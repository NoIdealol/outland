#pragma once

namespace outland
{
	using file_id = struct file_t*;
	using file_size_t = uint64_t;
}

namespace outland { namespace file 
{

	enum access_flags_t : uint8_t
	{
		access_read = 1 << 0,
		access_write = 1 << 1,
	};

	using access_mask_t = uint8_t;

	enum open_t : uint8_t
	{
		open_existing,	//fails if does not exist
		open_try_new,	//fails if exists
		open_overwrite,	//attempts to overwrite or creates new
		open_always,	//open or create
	};

	//////////////////////////////////////////////////
	// QUEUE
	//////////////////////////////////////////////////

	using queue_id = struct queue_t*;
	using request_id = struct request_t*;

	enum result_type_t
	{
		result_type_open,
		result_type_remove,
		result_type_get_size,
		result_type_set_size,
		result_type_request,
	};

	struct result_t
	{
		error_t			error;
		result_type_t	type;
		void*			user_ptr;

		union
		{
			file_id			file;
			file_size_t		size;
			char8_t const*	path;
			request_id		request;
		};
	};


	void	epoll(queue_id queue, result_t* result_array, size_t result_capacity, size_t& result_size);
	void	flush(queue_id queue);
	error_t open(queue_id queue, char8_t const* path, open_t open, access_mask_t access, void* user_ptr);
	error_t remove(queue_id queue, char8_t const* path, void* user_ptr);
	error_t	close(queue_id queue, file_id file);
	error_t get_size(queue_id queue, file_id file, void* user_ptr);
	error_t set_size(queue_id queue, file_id file, file_size_t size, void* user_ptr);
	error_t read(queue_id queue, file_id file, request_id request, file_size_t offset, void* buffer, size_t size, void* user_ptr);
	error_t write(queue_id queue, file_id file, request_id request, file_size_t offset, void* buffer, size_t size, void* user_ptr);

	using system_id = class system_t*;
	
	namespace queue
	{
		struct create_t
		{
			size_t	queue_capacity;
		};

		void	memory_info(memory_info_t& info, create_t const& create);
		void	create(queue_id& queue, create_t const& create, void* memory);
		void	destroy(queue_id queue);
	}

	namespace request
	{
		void	memory_info(memory_info_t& info);
		void	create(request_id& request, void* memory);
		void	destroy(request_id request);
	}

	namespace system
	{
		struct create_t
		{
			//allocator_t*	allocator;
		};

		void	memory_info(memory_info_t& info);
		error_t create(system_id& system, create_t const& create, void* memory);
		void	destroy(system_id system);
		void	update(system_id system);
		error_t queue_insert(system_id system, queue_id queue);
		void	queue_remove(system_id system, queue_id queue);

		//error_t junction_add(system_id system);

	}

	

} }