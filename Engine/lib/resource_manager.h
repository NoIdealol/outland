#pragma once
#include "file_system.h"

namespace outland
{
	enum resource_id : uint64_t {};
}

namespace outland { namespace resource
{
	enum type_uid : uint16_t {};
	enum package_uid : uint16_t {};
	using handle_id = void;
	using manager_id = class manager_t*;
	using package_id = class package_t*;
	class factory_t;

	struct acquire_result_t
	{
		error_t		error;
		void*		user_ptr;
	};

	struct type_desc_t
	{
		type_uid		uid;
		factory_t*		factory;
		memory_info_t	handle_info;
	};

	error_t	acquire(manager_id manager, resource_id resource, void* user_ptr);
	void	release(manager_id manager, resource_id resource);
	void	update(manager_id manager, acquire_result_t* result_array, size_t result_capacity, size_t& result_size);

	namespace manager
	{
		struct create_t
		{
			allocator_t*		manager_allocator;
			allocator_t*		resource_allocator;
			file::queue_id		file_queue;
			type_desc_t const*	type_array;
			size_t				type_size;
			size_t				queue_size;
		};

		error_t	create(manager_id& manager, create_t const& create);
		void	destroy(manager_id manager);
	}

	namespace package
	{
		struct create_t
		{
			package_uid		uid;
			allocator_t*	allocator;
		};

		struct resource_add_t
		{
			type_uid				type;
			char8_t const*			name;
			char8_t const*			path;
			char8_t const* const*	dep_array;
			size_t					dep_size;
		};

		error_t	create(manager_id manager, package_id& package, create_t const& create);
		void	destroy(manager_id manager, package_id package);
		error_t	resource_add(package_id package, resource_id& resource, resource_add_t const& add);

	}
	
} }
