#pragma once
#include "file_system.h"

namespace outland { namespace file 
{

	class device_t
	{
	public:
		virtual error_t	open(file_id file, char8_t const* path, open_t open, access_mask_t access) = 0;
		virtual error_t	remove(char8_t const* path) = 0;
		virtual void	close(file_id file) = 0;
		virtual error_t	get_size(file_id file, file_size_t& size) = 0;
		virtual error_t	set_size(file_id file, file_size_t size) = 0;
		virtual void	read(request_id request) = 0;
		virtual void	write(request_id request) = 0;
		virtual void	epoll(request_id* request_array, size_t request_capacity, size_t& request_size) = 0;
	};

} }
