#pragma once
#include "resource_manager.h"

namespace outland { namespace resource
{
	struct resource_create_t
	{
		resource_id const*	dep_array;
		size_t				dep_size;
		void const*			data_buffer;
		size_t				data_size;
		void*				user_ptr;
	};

	struct create_result_t
	{
		error_t		error;
		void*		user_ptr;
	};

	//creates resources
	class factory_t
	{
	public:
		virtual error_t	create(handle_id* handle, resource_create_t const& create) = 0;
		virtual	error_t	destroy(handle_id const* handle) = 0;
		virtual	void	epoll(create_result_t* result_array, size_t result_capacity, size_t& result_size) = 0;
	};
	
} }
