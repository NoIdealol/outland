#pragma once
#include "..\..\Graphics\lib\module.h"
#pragma comment(lib, "..\\..\\Graphics\\bin\\x64\\Debug\\Graphics.lib")
//#pragma comment(lib, "..\\..\\Graphics\\bin\\x64\\Release\\Graphics.lib")

namespace outland { namespace video 
{

	class renderer_t
	{
		allocator_t*			_allocator;
		gpu::module_id			_module;
		gpu::device_id			_device;
		
		gpu::cmd_queue_id		_cmd_queue;
		gpu::cmd_allocator_id	_cmd_allocator;
		gpu::cmd_ctx_id			_cmd_ctx;

		gpu::surface_id			_surface;
		gpu::swap_chain_id		_swap_chain;


		gpu::render_pass_id		_render_pass;
		gpu::descriptor_layout_id	_descriptor_layout;
		gpu::input_layout_id	_input_layout;
		gpu::pipeline_layout_id	_pipeline_layout;
		gpu::pipeline_id		_pipeline;
		gpu::shader_id			_vertex_shader;
		gpu::shader_id			_pixel_shader;
		gpu::render_set_id		_render_set_array[4];

		gpu::buffer_id			_buffer;
		gpu::texture_id			_texture;
		gpu::shader_view_id		_texture_sv;
		gpu::descriptor_set_id	_descriptor_set;
		gpu::sampler_id			_sampler;
		uint32_t				_upload_size;

	private:
		void _init_device(void* window_ptr, void* window_ctx, allocator_t* allocator, scratch_allocator_t* scratch);
		void _init_resource(scratch_allocator_t* scratch);
		void _update_device(scratch_allocator_t* scratch);

	public:
		void init(void* window_ptr, void* window_ctx, allocator_t* allocator, scratch_allocator_t* scratch);
		void render_frame(scratch_allocator_t* scratch);
	};

} }
