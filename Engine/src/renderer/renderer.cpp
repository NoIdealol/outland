#include "pch.h"
#include "renderer.h"

#include "system\file\file.h"
#include "system\thread\thread.h"
#include "system\application\time.h"
#include "system\debug\output.h"
#include "format\print.h"

#define SHADER_PATH "E:/Projects/Outland/Engine/src/shader/"
//#define SHADER_PATH "E:/Outland/Engine/src/shader/"

static outland::uint32_t texture_data[] = 
{
	0x00f000f0,
	0x0000f0f0,
	0x00f0f000,
	0x00f00000,
};

static const float vertex_buffer_data2[] = 
{ 
	0.6f, -0.3f,	1.0f, 
	0.3f, 1.0f,		1.0f,
	-0.9f, 0.7f,	1.0f,
};

static const float vertex_buffer_data[] = {
	-1.0f,-1.0f,-1.0f, // triangle 1 : begin
	-1.0f,-1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f, // triangle 1 : end
	1.0f, 1.0f,-1.0f, // triangle 2 : begin
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f, // triangle 2 : end
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f
};

#include "math\floating.h"
//--------------------------------------------------
using Matrix = float[4][4];
void setPerspectiveFrustrum(Matrix& out, const float fov, const float width, const float height, const float nearClip, const float farClip)
{
	float aspectRatio = width / height;
	
	//1 / tan
	float h = 1.0f / (outland::m::sin(fov / 2.0f) / outland::m::cos(fov / 2.0f));
	float w = h / aspectRatio;
	float d = farClip / (farClip - nearClip);

	Matrix proj =
	{
		//x		//y		Y		W
		{	w,		0.0f,	0.0f,	0.0f,			},
		{	0.0f,	h,		0.0f,	0.0f,			},
		{	0.0f,	0.0f,	d,		-nearClip * d,	},
		{	0.0f,	0.0f,	1.0f,	0.0f			},
	};

	/*{
		{ w,    0.0f, 0.0f, 0.0f },
		{ 0.0f, h   , 0.0f, 0.0f },
		{ 0.0f, 0.0f, 1.0f, 4.0f },
		{ 0.0f, 0.0f, 1.0f, 0.0f },
	};*/
	
	outland::u::mem_copy(&out, proj, sizeof(proj));
}

void MatRotY(Matrix& out, const float angle)
{
	float s = outland::m::sin(angle);
	float c = outland::m::cos(angle);

	Matrix rot =
	{
		{ c,    0.0f, s,    0.0f, },
		{ 0.0f, 1.0f, 0.0f, 0.0f, },
		{ -s  , 0.0f, c,    0.0f, },
		{ 0.0f, 0.0f, 0.0f, 1.0f, },
	};

	/*{
	{ w,    0.0f, 0.0f, 0.0f },
	{ 0.0f, h   , 0.0f, 0.0f },
	{ 0.0f, 0.0f, 1.0f, 4.0f },
	{ 0.0f, 0.0f, 1.0f, 0.0f },
	};*/

	outland::u::mem_copy(&out, rot, sizeof(rot));
}



namespace outland { namespace video 
{

	void load_file_blocking(array<byte_t>& data, char8_t const* path)
	{
		error_t err;
		data.clear();

		os::file::queue_id queue;
		err = os::file::queue_create(queue);
		O_ASSERT(err_ok == err);

		os::file::operation_id op;
		err = os::file::operation_create(op);
		O_ASSERT(err_ok == err);

		os::file_id file;
		err = os::file::open(file, queue, os::file::open_t::existing, os::file::access_read, path);
		O_ASSERT(err_ok == err);

		os::file::request_t req;
		req.file = file;
		req.user = nullptr;
		req.where = 0;
		err = os::file::size(file, req.size);
		O_ASSERT(err_ok == err);
		err = data.resize(req.size);
		O_ASSERT(err_ok == err);
		req.buffer = data.data();

		os::file::operation_set_request(op, req);
		err = os::file::read(op);
		O_ASSERT(err_ok == err);

		size_t results_size = 0;
		os::file::result_t results_array[1];

		while (0 == results_size)
		{
			os::file::queue_process(queue, results_array, 1, results_size);
			os::thread::sleep(1);
		}

		err = results_array[0].error;
		O_ASSERT(err_ok == err);

		os::file::close(file);
		os::file::operation_destroy(op);
		os::file::queue_destroy(queue);
	}

	void renderer_t::_init_device(void* window_ptr, void* window_ctx, allocator_t* allocator, scratch_allocator_t* scratch)
	{
		error_t err;

		gpu::module_create_info_t module_create;
		module_create.create_mask = gpu::module_create_debug;
		module_create.module_allocator = allocator;
		module_create.object_allocator = allocator;

		err = outland_gpu_module_create(&_module, &module_create, scratch);
		O_ASSERT(err_ok == err);

		gpu::surface_create_info_t surface_create;
		surface_create.window_ctx = window_ctx;
		surface_create.window_ptr = window_ptr;

		err = gpu::module::surface_create(_module, _surface, surface_create, scratch);
		O_ASSERT(err_ok == err);

		gpu::adapter_id adapter_array[8];
		size_t adapter_size;
		err = gpu::module::adapter_enumerate(_module, adapter_array, u::array_size(adapter_array), adapter_size, scratch);
		O_ASSERT(err_ok == err);
		O_ASSERT(adapter_size);

		gpu::device_create_t device_create;
		device_create.adapter = adapter_array[0];
		device_create.device_allocator = allocator;
		device_create.object_allocator = allocator;

		err = gpu::module::device_create(_module, _device, _cmd_queue, device_create, scratch);
		O_ASSERT(err_ok == err);

		gpu::swap_chain_create_t swap_chain_create;
		swap_chain_create.surface = _surface;
		swap_chain_create.present_mode = gpu::present_mode_fifo;
		swap_chain_create.back_buffer_min_count = 2;
		swap_chain_create.back_buffer_width = 800;
		swap_chain_create.back_buffer_height = 600;
		swap_chain_create.back_buffer_format = gpu::format_t::B8G8R8A8_UNORM;
		swap_chain_create.back_buffer_acquire_state.access = gpu::render_target_access_store;
		swap_chain_create.back_buffer_release_state.access = gpu::render_target_access_store;

		err = gpu::device::swap_chain_create(_device, _swap_chain, swap_chain_create, scratch);
		O_ASSERT(err_ok == err);

		gpu::cmd_allocator_create_t cmd_allocator_create;
		cmd_allocator_create.allocator = allocator;

		err = gpu::device::cmd_allocator_create(_device, _cmd_allocator, cmd_allocator_create, scratch);
		O_ASSERT(err_ok == err);

		err = gpu::cmd::ctx_alloc(_cmd_queue, _cmd_allocator, &_cmd_ctx, 1, gpu::cmd_type_primary);
		O_ASSERT(err_ok == err);

		gpu::sampler_create_t sampler_create;
		sampler_create.create_mask = 0;
		sampler_create.address_u = gpu::address_mode_clamp;
		sampler_create.address_v = gpu::address_mode_clamp;
		sampler_create.address_w = gpu::address_mode_clamp;
		sampler_create.min_filter = gpu::filter_nearest;
		sampler_create.mag_filter = gpu::filter_linear;
		sampler_create.mip_filter = gpu::filter_nearest;
		sampler_create.mip_bias = 0.0f;
		sampler_create.mip_min = 0.0f;
		sampler_create.mip_max = 16.0f;
		sampler_create.max_anisotropy = 0;
		sampler_create.compare_operation = gpu::compare_operation_never;
		sampler_create.border_color = gpu::border_color_R0G0B0A0_FLOAT;

		err = gpu::device::sampler_create(_device, _sampler, sampler_create, scratch);
		O_ASSERT(err_ok == err);
	}

	void renderer_t::_init_resource(scratch_allocator_t* scratch)
	{
		error_t err;

		_upload_size = 0;

		{
			gpu::render_target_desc_t rt_desc;
			rt_desc.format = gpu::format_t::B8G8R8A8_UNORM;
			rt_desc.color_load = gpu::attachment_load_clear;
			rt_desc.color_store = gpu::attachment_store_write;
			rt_desc.access_initial = gpu::render_target_access_store;
			rt_desc.stage_initial = 0;
			rt_desc.access_final = gpu::render_target_access_store;
			rt_desc.stage_final = 0;

			gpu::render_target_t subpass_0_rt_0;
			subpass_0_rt_0.access = gpu::render_target_access_store;
			subpass_0_rt_0.index = 0;
			subpass_0_rt_0.slot = 0;

			gpu::subpass_desc_t subpass_0;
			subpass_0.render_target_array = &subpass_0_rt_0;
			subpass_0.render_target_size = 1;
			subpass_0.depth_stencil_array = nullptr;
			subpass_0.depth_stencil_size = 0;
			subpass_0.render_target_preserve_array = nullptr;
			subpass_0.render_target_preserve_size = 0;
			subpass_0.depth_stencil_preserve_array = nullptr;
			subpass_0.depth_stencil_preserve_size = 0;
			subpass_0.subpass_dependency_array = nullptr;
			subpass_0.subpass_dependency_size = 0;

			gpu::render_pass_create_t render_pass_create;
			render_pass_create.render_target_array = &rt_desc;
			render_pass_create.render_target_size = 1;
			render_pass_create.depth_stencil_array = nullptr;
			render_pass_create.depth_stencil_size = 0;
			render_pass_create.subpass_array = &subpass_0;
			render_pass_create.subpass_size = 1;

			err = gpu::device::render_pass_create(_device, _render_pass, render_pass_create, scratch);
			O_ASSERT(err_ok == err);
		}

		{
			gpu::render_target_view_id bb_array[4];
			size_t bb_size;
			err = gpu::swap_chain::back_buffer_enumerate(_swap_chain, bb_array, u::array_size(bb_array), bb_size);
			O_ASSERT(err_ok == err);

			for (size_t i = 0; i < bb_size; ++i)
			{
				gpu::render_set_create_t set_create;
				set_create.render_pass = _render_pass;
				set_create.render_target_array = &bb_array[i];
				set_create.render_target_size = 1;
				set_create.depth_stencil_array = nullptr;
				set_create.depth_stencil_size = 0;
				set_create.width = 800;
				set_create.height = 600;
				
				err = gpu::device::render_set_create(_device, _render_set_array[i], set_create, scratch);
				O_ASSERT(err_ok == err);
			}
			//
		}

		{
			gpu::descriptor_desc_t descriptor_desc_array[2];
			descriptor_desc_array[0].binding = 0;
			descriptor_desc_array[0].type = gpu::descriptor_type_sampled_texture;
			descriptor_desc_array[0].size = 1;
			descriptor_desc_array[0].stage_mask = gpu::shader_stage_fragment;

			descriptor_desc_array[1].binding = 1;
			descriptor_desc_array[1].type = gpu::descriptor_type_sampler;
			descriptor_desc_array[1].size = 1;
			descriptor_desc_array[1].stage_mask = gpu::shader_stage_fragment;

			gpu::descriptor_layout_create_t descriptor_layout_create;
			descriptor_layout_create.descriptor_array = descriptor_desc_array;
			descriptor_layout_create.descriptor_size = 2;

			err = gpu::device::descriptor_layout_create(_device, _descriptor_layout, descriptor_layout_create, scratch);
			O_ASSERT(err_ok == err);

			/*gpu::uniform_desc_t uniform_desc;
			uniform_desc.size = 4;
			uniform_desc.stage_mask = gpu::shader_stage_vertex;*/
			gpu::transform_desc_t transform_desc_array[2];
			transform_desc_array[0].binding = 0;
			transform_desc_array[0].stage_mask = gpu::shader_stage_vertex;
			transform_desc_array[1].binding = 1;
			transform_desc_array[1].stage_mask = gpu::shader_stage_vertex;

			gpu::input_layout_create_t input_layout_create;
			input_layout_create.uniform_desc_array = nullptr;// &uniform_desc;
			input_layout_create.uniform_desc_size = 0;// 1;
			input_layout_create.transform_desc_array = transform_desc_array;// nullptr;
			input_layout_create.transform_desc_size = u::array_size(transform_desc_array);// 0;

			err = gpu::device::input_layout_create(_device, _input_layout, input_layout_create, scratch);
			O_ASSERT(err_ok == err);

			gpu::pipeline_layout_create_t pipeline_layout_create;
			pipeline_layout_create.descriptor_layout_array = &_descriptor_layout;
			pipeline_layout_create.descriptor_layout_size = 1;
			pipeline_layout_create.input_layout = _input_layout;

			err = gpu::device::pipeline_layout_create(_device, _pipeline_layout, pipeline_layout_create, scratch);
			O_ASSERT(err_ok == err);

			array<byte_t> data = { scratch };

			load_file_blocking(data, SHADER_PATH"shader_vertex.spirv");

			gpu::shader_create_t vs_create;
			vs_create.shader_data = data.data();
			vs_create.shader_size = data.size();
			//vs_create.stage = gpu::shader_stage_vertex;

			err = gpu::device::shader_create(_device, _vertex_shader, vs_create, scratch);
			O_ASSERT(err_ok == err);

		
			load_file_blocking(data, SHADER_PATH"shader_fragment.spirv");

			gpu::shader_create_t ps_create;
			ps_create.shader_data = data.data();
			ps_create.shader_size = data.size();
			//ps_create.stage = gpu::shader_stage_fragment;

			err = gpu::device::shader_create(_device, _pixel_shader, ps_create, scratch);
			O_ASSERT(err_ok == err);
		}

		{
			gpu::vertex_desc_t vertex_desc_array[1];
			vertex_desc_array[0].rate = gpu::vertex_input_rate_vertex;
			vertex_desc_array[0].stride = sizeof(float) * 3;

			gpu::attribute_desc_t attribute_desc_array[1];
			attribute_desc_array[0].binding = 0;
			attribute_desc_array[0].offset = 0;
			attribute_desc_array[0].slot = 0;
			attribute_desc_array[0].format = gpu::format_t::R32G32B32_FLOAT;

			gpu::input_assembly_state_t input_assembly_state;
			input_assembly_state.topology = gpu::primitive_topology_triangle_list;
			input_assembly_state.vertex_array = vertex_desc_array;
			input_assembly_state.vertex_size = 1;
			input_assembly_state.attribute_array = attribute_desc_array;
			input_assembly_state.attribute_size = 1;

			gpu::shader_state_t shader_state_array[2];
			shader_state_array[0].stage = gpu::shader_stage_vertex;
			shader_state_array[0].shader = _vertex_shader;
			shader_state_array[1].stage = gpu::shader_stage_fragment;
			shader_state_array[1].shader = _pixel_shader;

			gpu::viewport_t viewport_array[1];
			viewport_array[0].x = 0.0f;
			viewport_array[0].y = 0.0f;
			viewport_array[0].width = 800.0f;
			viewport_array[0].height = 600.0f;
			viewport_array[0].depth_min = 0.0f;
			viewport_array[0].depth_max = 1.0f;

			gpu::scissor_t scissor_array[1];
			scissor_array[0].x = 0;
			scissor_array[0].y = 0;
			scissor_array[0].width = 800;
			scissor_array[0].height = 600;

			gpu::viewport_state_t			viewport_state;
			viewport_state.viewport_array = viewport_array;
			viewport_state.scissor_array = scissor_array;
			viewport_state.size = u::array_size(viewport_array);

			gpu::depth_stencil_state_t		depth_stencil_state;
			depth_stencil_state.state_mask = 0;

			gpu::rasterizer_state_t			rasterizer_state;
			rasterizer_state.state_mask = gpu::rasterizer_state_front_face_counter_clockwise;
			rasterizer_state.fill_mode = gpu::fill_mode_solid;
			rasterizer_state.cull_mode = gpu::cull_mode_back;

			gpu::blend_desc_t blend_desc_array[1];
			blend_desc_array[0].desc_mask = 0;

			gpu::blend_state_t				blend_state;
			blend_state.blend_array = blend_desc_array;
			blend_state.blend_size = u::array_size(blend_desc_array);

			gpu::pipeline_state_t state;
			state.create_mask = 0;
			state.layout = _pipeline_layout;
			state.pass = _render_pass;
			state.subpass = 0;

			state.input_assembly_state = &input_assembly_state;
			state.shader_state_array = shader_state_array;
			state.shader_state_size = u::array_size(shader_state_array);
			state.viewport_state = &viewport_state;
			state.depth_stencil_state = &depth_stencil_state;
			state.rasterizer_state = &rasterizer_state;
			state.blend_state = &blend_state;

			gpu::pipeline_create_t pipeline_create;
			pipeline_create.pipeline_array = &state;
			pipeline_create.pipeline_size = 1;

			err = gpu::device::pipeline_create(_device, &_pipeline, pipeline_create, scratch);
			O_ASSERT(err_ok == err);
		}

		gpu::buffer_create_t bc;
		bc.size = sizeof(vertex_buffer_data);
		bc.type = gpu::buffer_type_vertex;
		bc.stage_mask = 0;
		err = gpu::device::buffer_create(_device, nullptr, bc, scratch);
		O_ASSERT(err_ok == err);
		_buffer = nullptr;

		gpu::texture_create_t tc;
		tc.usage_mask = gpu::texture_usage_shader_resource;
		tc.type = gpu::texture_type_sampled;
		tc.dimension = gpu::texture_dimension_2D;
		tc.format = gpu::format_t::R8G8B8A8_UNORM;
		tc.size.width = 2;
		tc.size.height = 2;
		tc.size.depth = 1;
		tc.mip_size = 1;
		tc.array_size = 1;
		tc.stage_mask = gpu::shader_stage_fragment;
		err = gpu::device::texture_create(_device, nullptr, tc, scratch);
		O_ASSERT(err_ok == err);
		_texture = nullptr;

	}

	void renderer_t::init(void* window_ptr, void* window_ctx, allocator_t* allocator, scratch_allocator_t* scratch)
	{
		_allocator = allocator;
		_init_device(window_ptr, window_ctx, allocator, scratch);
		_init_resource(scratch);
	}

	void renderer_t::_update_device(scratch_allocator_t* scratch)
	{
		error_t err;

		gpu::device_event_t device_event_array[8];
		size_t device_event_size;
		gpu::device::update(_device, device_event_array, u::array_size(device_event_array), device_event_size);
		for (size_t i = 0; i < device_event_size; ++i)
		{
			switch (device_event_array[i].type)
			{
			case gpu::device_event_type_buffer_create:
			{
				O_ASSERT(err_ok == device_event_array[i].buffer.error);
				_buffer = device_event_array[i].buffer.buffer;

				gpu::host_to_buffer_copy_t copy;
				copy.user_data = nullptr;
				copy.dst_buffer = _buffer;
				copy.src_data = vertex_buffer_data;
				err = gpu::device::buffer_copy(_device, copy);
				O_ASSERT(err_ok == err);

				break;
			}
			case gpu::device_event_type_buffer_copy:
			{
				++_upload_size;
				break;
			}
			case gpu::device_event_type_texture_create:
			{
				O_ASSERT(err_ok == device_event_array[i].texture.error);
				_texture = device_event_array[i].texture.texture;

				gpu::texture_mip_data_t mip_data;
				mip_data.byte_row_pitch = 8;
				mip_data.byte_slice_pitch = 16;
				mip_data.src_data = texture_data;

				gpu::host_to_texture_copy_t copy;
				copy.user_data = nullptr;
				copy.dst_texture = _texture;
				copy.mip_data_array = &mip_data;
				copy.mip_size = 1;
				copy.mip_level = 0;
				copy.array_level = 0;
				copy.array_size = 1;

				err = gpu::device::texture_copy(_device, copy);
				O_ASSERT(err_ok == err);

				gpu::texture_shader_view_create_t texture_shader_view_create;
				texture_shader_view_create.texture = _texture;
				texture_shader_view_create.format = gpu::format_t::R8G8B8A8_UNORM;
				texture_shader_view_create.type = gpu::texture_shader_view_type_2D;
				texture_shader_view_create.aspect_mask = gpu::texture_aspect_color;
				texture_shader_view_create.mip_level = 0;
				texture_shader_view_create.mip_size = 1;
				texture_shader_view_create.array_level = 0;
				texture_shader_view_create.array_size = 1;
				
				err = gpu::device::shader_view_create(_device, _texture_sv, texture_shader_view_create, scratch);
				O_ASSERT(err_ok == err);

				gpu::descriptor_state_t descriptor_state_array[2];
				descriptor_state_array[0].shader_view = _texture_sv;
				descriptor_state_array[1].sampler = _sampler;

				gpu::descriptor_set_state_t descriptor_set_state;
				descriptor_set_state.layout = _descriptor_layout;
				descriptor_set_state.descriptor_array = descriptor_state_array;
				descriptor_set_state.descriptor_size = 2;

				gpu::descriptor_set_create_t descriptor_set_create;
				descriptor_set_create.state_array = &descriptor_set_state;
				descriptor_set_create.state_size = 1;

				err = gpu::device::descriptor_set_create(_device, &_descriptor_set, descriptor_set_create, scratch);
				O_ASSERT(err_ok == err);

				break;
			}
			case gpu::device_event_type_texture_copy:
			{
				++_upload_size;
				break;
			}
			default:
				O_ASSERT(false);
				break;
			}
		}
	}

	void renderer_t::render_frame(scratch_allocator_t* scratch)
	{
		error_t err;

		_update_device(scratch);

		static int frame_sum = 0;
		//static os::time_t time_sum = os::time::now_high(os::time::micro_second);
		static os::time_t time_old = os::time::now_high(os::time::micro_second);

		frame_sum += 1;

		if (60 == frame_sum)
		{
			os::time_t time_now = os::time::now_high(os::time::micro_second);
			os::time_t time_sum = time_now - time_old;
			time_old = time_now;

			auto time_per_frame = (time_sum / frame_sum) / 1000;
			string8_t fps_str = { _allocator };
			err = f::print(fps_str, "%d \n", time_per_frame);
			O_ASSERT(err_ok == err);
			os::debug_string(fps_str.data());

			//time_sum = 0;
			frame_sum = 0;
		}	

		err = gpu::cmd::update(_cmd_queue);
		O_ASSERT(err_ok == err);

		err = gpu::cmd::record_primary(_cmd_ctx);
		O_ASSERT(err_ok == err);

		{
			size_t bb_index;
			gpu::cmd::back_buffer_acquire(_cmd_ctx, _swap_chain, bb_index);

			gpu::clear_color_t clear_color_array[1];
			clear_color_array[0].index = 0;
			clear_color_array[0].value.fmt_float32[0] = 0.3f;
			clear_color_array[0].value.fmt_float32[1] = 0.3f;
			clear_color_array[0].value.fmt_float32[2] = 0.3f;
			clear_color_array[0].value.fmt_float32[3] = 0.0f;

			gpu::render_state_t render_state;
			render_state.render_set = _render_set_array[bb_index];
			render_state.render_offset_x = 0;
			render_state.render_offset_y = 0;
			render_state.render_width = 800;
			render_state.render_height = 600;
			render_state.clear_color_array = clear_color_array;
			render_state.clear_color_size = u::array_size(clear_color_array);
			render_state.clear_depth_array = nullptr;
			render_state.clear_depth_size = 0;

			gpu::cmd::render_state(_cmd_ctx, _render_pass, render_state, gpu::cmd_type_primary);

			if (2 == _upload_size)
			{
				gpu::cmd::bind_pipeline(_cmd_ctx, _pipeline);
				gpu::cmd::bind_vertex(_cmd_ctx, &_buffer, 1, 0);
				gpu::cmd::bind_descriptor(_cmd_ctx, &_descriptor_set, 1, 0);

				Matrix proj;
				setPerspectiveFrustrum(proj, 3.14f * 0.45f, 800, 600, 1.0f, 100);
				
				Matrix rot;
				static float angle = 0;
				angle += 0.02f;
				MatRotY(rot, angle);

				//gpu::cmd::uniform(_cmd_ctx, &scale, sizeof(scale), 0, 0);
				void* transform;
				gpu::cmd::transform(_cmd_ctx, transform, sizeof(proj), 0);
				u::mem_copy(transform, &proj, sizeof(proj));

				gpu::cmd::transform(_cmd_ctx, transform, sizeof(rot), 1);
				u::mem_copy(transform, &rot, sizeof(rot));

				gpu::cmd::draw(_cmd_ctx, u::array_size(vertex_buffer_data) / 3, 0, 1, 0);

			}
			gpu::cmd::render_finish(_cmd_ctx, _render_pass);

			gpu::cmd::back_buffer_release(_cmd_ctx, _swap_chain);
		}

		err = gpu::cmd::record_finish(_cmd_ctx);
		O_ASSERT(err_ok == err);

		err = gpu::cmd::submit(_cmd_queue, &_cmd_ctx, 1, &_swap_chain, 1);
		O_ASSERT(err_ok == err);
	}

} }
