#pragma once
#include "..\..\Graphics\lib\module.h"

namespace outland { namespace video 
{
	namespace c
	{
		enum : size_t
		{
			max_vertex_buffer_attributes = 8,
			max_mesh_vertex_buffers = 3,
			max_mesh_attributes = max_vertex_buffer_attributes,
			max_transforms = 3,
			max_material_descriptors = 8,
			max_constant_descriptors = 4,
			max_pipeline_constants = 4,
		};
	}

	using semantics_id = char8_t const*;

	struct material_descriptor_desc_t
	{
		semantics_id			semantics;
		gpu::descriptor_type_t	type;
		uint32_t				size;
	};

	struct material_class_t
	{
		gpu::descriptor_layout_id	layout;
		material_descriptor_desc_t	descriptor_array[c::max_material_descriptors];
		uint32_t					descriptor_size;
	};

	struct mesh_class_t
	{
		semantics_id				semantics_array[c::max_mesh_vertex_buffers];
		gpu::vertex_desc_t			vertex_array[c::max_mesh_vertex_buffers];
		gpu::attribute_desc_t		attribute_array[c::max_mesh_attributes];
		gpu::input_assembly_state_t	layout;
	};

	struct transform_class_t
	{
		gpu::input_layout_id	input_layout;
		uint32_t				uniform_size;
		uint32_t				transform_size;
	};

	struct constant_descriptor_desc_t
	{
		semantics_id			semantics;
		gpu::descriptor_type_t	type;
		uint32_t				size;
	};

	struct constant_class_t
	{
		gpu::descriptor_layout_id	layout;
		constant_descriptor_desc_t	descriptor_array[c::max_constant_descriptors];
		uint32_t					descriptor_size;
	};

	struct pipeline_class_t
	{
		gpu::pipeline_layout_id		layout;

		material_class_t*			material_clazz;
		transform_class_t*			transform_clazz;
		constant_class_t*			constant_clazz_array[c::max_pipeline_constants];
		uint32_t					constant_clazz_size;
	};

} }
