#pragma once
#include "resource_class.h"

namespace outland { namespace video 
{

	struct pipeline_t
	{
		gpu::pipeline_id	pipeline;
		//render_pass_t*		render_pass;
		//uint32_t			subpass;
		
		uint8_t				vertex_buffer_size;
		uint8_t				transform_size;
		uint8_t				material_slot;
		uint8_t				uniform_size;
		//gpu::format_t		
		mesh_class_t*		mesh_clazz;
	};


	struct buffer_t
	{
		gpu::buffer_type_t	type;
		gpu::buffer_id		buffer;
	};

	struct vertex_buffer_t : buffer_t
	{
		uint32_t			vertex_size;
		gpu::format_t		attribute_array[c::max_vertex_buffer_attributes];
		uint8_t				attribute_size;
	};

	struct index_buffer_t : buffer_t
	{
		uint32_t			index_size;
		gpu::index_format_t	format;
	};

	struct uniform_buffer_t : buffer_t
	{
		uint32_t			vec4_size;
	};

	

	struct texture_t
	{
		gpu::texture_id		texture;
		gpu::shader_view_id	view;
	};


	struct material_t
	{
		material_class_t*			clazz;
		gpu::descriptor_set_id		descriptor_set;
	};

	struct mesh_t
	{
		mesh_class_t*		clazz;
		vertex_buffer_t*	vertex_array[c::max_mesh_vertex_buffers];
		index_buffer_t*		index_buffer;
	};

	using constant_t = gpu::descriptor_set_id;

} }
