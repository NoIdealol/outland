#include "pch.h"
#include "register.h"
#include "math\integer.h"

namespace outland { namespace video 
{
	void bind_pipeline_clazz(gpu::cmd_ctx_id ctx, pipeline_class_t* pipeline_clazz)
	{
		gpu::cmd::bind_pipeline_layout(ctx, pipeline_clazz->layout);
	}

	void bind_pipeline(gpu::cmd_ctx_id ctx, pipeline_t* pipeline)
	{
		gpu::cmd::bind_pipeline(ctx, pipeline->pipeline);
	}

	void bind_command(gpu::cmd_ctx_id ctx, pipeline_t* pipeline, command_t* cmd)
	{
		if (pipeline->vertex_buffer_size)
			gpu::cmd::bind_vertex(ctx, cmd->vertex_buffer_array, pipeline->vertex_buffer_size, 0);
		if (cmd->index_buffer)
			gpu::cmd::bind_index(ctx, cmd->index_buffer, cmd->index_format);
		if (cmd->material)
			gpu::cmd::bind_descriptor(ctx, &cmd->material, 1, pipeline->material_slot);
		if (pipeline->uniform_size)
			gpu::cmd::uniform(ctx, cmd->uniform, pipeline->uniform_size, 0, 0);
	}

	void draw_commands(gpu::cmd_ctx_id ctx, pipeline_t* pipeline, command_t* cmd, command_data_t const* data_array, size_t data_size)
	{
		while (data_size)
		{
			void* transform_data_array[c::max_transforms] = { 0 };
			uint32_t batch_size = m::min((uint32_t)cmd->max_instances, (uint32_t)data_size);

			//alloc transform buffers
			for (uint8_t i = 0; i < pipeline->transform_size; ++i)
			{
				uint32_t transform_size = cmd->transform_size_array[i] * batch_size;
				gpu::cmd::transform(ctx, transform_data_array[i], transform_size, i);
			}

			//fill transform buffers
			for (uint32_t i = 0; i < batch_size; ++i)
			{
				for (uint8_t j = 0; j < pipeline->transform_size; ++j)
				{
					u::mem_copy(transform_data_array[j], data_array[i].transform_array[j], cmd->transform_size_array[j]);
					transform_data_array[j] = (byte_t*)transform_data_array[j] + cmd->transform_size_array[j];
				}
			}

			if (cmd->index_buffer)
				gpu::cmd::draw_indexed(ctx, cmd->draw_size, 0, batch_size, 0, 0);
			else
				gpu::cmd::draw(ctx, cmd->draw_size, 0, batch_size, 0);

			data_array += batch_size;
			data_size -= batch_size;
		}
	}

} }
