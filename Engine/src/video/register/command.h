#pragma once
#include "resource_instance.h"

namespace outland { namespace video 
{
		
	//6		bucket == pass/subpass/viewport/layer/sort algo
	//6		pipeline layout
	//10	pipeline
	//10	constants 2:2:3:3
	//16	command = material + mesh + transform
	//16	order == depth? may be reverse-ordered and strict = after bucket

	using command_id = uint64_t;

	enum : command_id
	{
		sort_key_order_shift = 0,
		sort_key_order_bit_count = 16,

		sort_key_command_shift = sort_key_order_shift + sort_key_order_bit_count,
		sort_key_command_bit_count = 16,
		sort_key_command_capacity = (1 << sort_key_command_bit_count),
		sort_key_command_mask = sort_key_command_capacity - 1,
		sort_key_command_compare_mask = ~((command_id(1) << sort_key_command_shift) - 1),

		sort_key_constant3_shift = sort_key_command_shift + sort_key_command_bit_count,
		sort_key_constant3_bit_count = 3,
		sort_key_constant3_capacity = (1 << sort_key_constant3_bit_count),
		sort_key_constant3_mask = sort_key_constant3_capacity - 1,
		sort_key_constant3_compare_mask = ~((command_id(1) << sort_key_constant3_shift) - 1),

		sort_key_constant2_shift = sort_key_constant3_shift + sort_key_constant3_bit_count,
		sort_key_constant2_bit_count = 3,
		sort_key_constant2_capacity = (1 << sort_key_constant2_bit_count),
		sort_key_constant2_mask = sort_key_constant2_capacity - 1,
		sort_key_constant2_compare_mask = ~((command_id(1) << sort_key_constant2_shift) - 1),

		sort_key_constant1_shift = sort_key_constant2_shift + sort_key_constant2_bit_count,
		sort_key_constant1_bit_count = 2,
		sort_key_constant1_capacity = (1 << sort_key_constant1_bit_count),
		sort_key_constant1_mask = sort_key_constant1_capacity - 1,
		sort_key_constant1_compare_mask = ~((command_id(1) << sort_key_constant1_shift) - 1),

		sort_key_constant0_shift = sort_key_constant1_shift + sort_key_constant1_bit_count,
		sort_key_constant0_bit_count = 2,
		sort_key_constant0_capacity = (1 << sort_key_constant0_bit_count),
		sort_key_constant0_mask = sort_key_constant0_capacity - 1,
		sort_key_constant0_compare_mask = ~((command_id(1) << sort_key_constant0_shift) - 1),

		sort_key_pipeline_shift = sort_key_constant0_shift + sort_key_constant0_bit_count,
		sort_key_pipeline_bit_count = 10,
		sort_key_pipeline_capacity = (1 << sort_key_pipeline_bit_count),
		sort_key_pipeline_mask = sort_key_pipeline_capacity - 1,
		sort_key_pipeline_compare_mask = ~((command_id(1) << sort_key_pipeline_shift) - 1),

		sort_key_pipeline_class_shift = sort_key_pipeline_shift + sort_key_pipeline_bit_count,
		sort_key_pipeline_class_bit_count = 6,
		sort_key_pipeline_class_capacity = (1 << sort_key_pipeline_class_bit_count),
		sort_key_pipeline_class_mask = sort_key_pipeline_class_capacity - 1,
		sort_key_pipeline_class_compare_mask = ~((command_id(1) << sort_key_pipeline_class_shift) - 1),

		sort_key_bucket_shift = sort_key_pipeline_class_shift + sort_key_pipeline_class_bit_count,
		sort_key_bucket_bit_count = 6,
		sort_key_bucket_capacity = (1 << sort_key_bucket_bit_count),
		sort_key_bucket_mask = sort_key_bucket_capacity - 1,
		sort_key_bucket_compare_mask = ~((command_id(1) << sort_key_bucket_shift) - 1),
	};

	struct command_t
	{
		uint8_t						max_instances;
		gpu::index_format_t			index_format;
		uint16_t					transform_size_array[c::max_transforms];
		uint32_t					draw_size;
		uint32_t					uniform[2];

		gpu::buffer_id				index_buffer;
		gpu::buffer_id				vertex_buffer_array[c::max_mesh_vertex_buffers];
		gpu::descriptor_set_id		material;
	};
	
	struct command_data_t
	{
		command_id	key;
		void const*	transform_array[c::max_transforms];
	};
	

	class command_ctx_t
	{
		using command_queue_t = array<command_data_t>;

		array<command_queue_t>	_queue_array;


		//command_t*	_find_command(command_id id);

		size_t		_find_queue(command_id id);

	public:
		error_t render(command_id id, uint16_t order, void const* const* transform_array, size_t transform_size)
		{
			command_data_t data;
			data.key = id + order;
			for (size_t i = 0; i < transform_size; ++i)
				data.transform_array[i] = transform_array[i];
			for (size_t i = transform_size; i < c::max_transforms; ++i)
				data.transform_array[i] = nullptr;

			return _queue_array[_find_queue(id)].push_back(data);
		}
	};

	

} }
