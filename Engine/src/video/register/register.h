#pragma once
#include "resource_instance.h"
#include "command.h"

namespace outland { namespace video 
{

	class free_list_t
	{
		void*		_free_list;

	public:
		free_list_t()
			: _free_list(nullptr)
		{

		}

		void* pop()
		{
			if (nullptr == _free_list)
				return nullptr;

			void* memory = _free_list;
			_free_list = *((void**)_free_list);
			return memory;
		}

		void push(void* memory)
		{
			*((void**)memory) = _free_list;
			_free_list = memory;
		}
	};
	
	
	class data_register_t
	{
		free_list_t			_pipeline_clazz_free_list;
		pipeline_class_t	_pipeline_clazz_array[sort_key_pipeline_class_capacity];
		free_list_t			_pipeline_free_list;
		pipeline_t			_pipeline_array[sort_key_pipeline_capacity];
		free_list_t			_command_free_list;
		command_t			_command_array[sort_key_command_capacity];

	public:

		pipeline_class_t*	find_pipeline_clazz(command_id id)
		{
			return _pipeline_clazz_array + ((id >> sort_key_pipeline_class_shift) & sort_key_pipeline_class_mask);
		}

		pipeline_t*			find_pipeline(command_id id)
		{
			return _pipeline_array + ((id >> sort_key_pipeline_shift) & sort_key_pipeline_mask);
		}

		command_t*			find_command(command_id id)
		{
			return _command_array + ((id >> sort_key_command_shift) & sort_key_command_mask);
		}


		pipeline_class_t*	alloc_pipeline_clazz()
		{
			return (pipeline_class_t*)_pipeline_clazz_free_list.pop();
		}

		pipeline_t*			alloc_pipeline()
		{
			return (pipeline_t*)_pipeline_free_list.pop();
		}

		command_t*			alloc_command()
		{
			return (command_t*)_command_free_list.pop();
		}

		void	free_pipeline_clazz(pipeline_class_t* pipeline_class)
		{
			_pipeline_clazz_free_list.push(pipeline_class);
		}

		void	free_pipeline(pipeline_t* pipeline)
		{
			_pipeline_free_list.push(pipeline);
		}

		void	free_command(command_t* command)
		{
			_command_free_list.push(command);
		}
	};
	
	class constant_register_t
	{
		constant_t		constant_0_array[sort_key_constant0_capacity];
		constant_t		constant_1_array[sort_key_constant1_capacity];
		constant_t		constant_2_array[sort_key_constant2_capacity];
		constant_t		constant_3_array[sort_key_constant3_capacity];

	public:

		constant_t		find_constant_0(command_id id)
		{
			return constant_0_array[(id >> sort_key_constant0_shift) & sort_key_constant0_mask];
		}

		constant_t		find_constant_1(command_id id)
		{
			return constant_1_array[(id >> sort_key_constant1_shift) & sort_key_constant1_mask];
		}

		constant_t		find_constant_2(command_id id)
		{
			return constant_2_array[(id >> sort_key_constant2_shift) & sort_key_constant2_mask];
		}

		constant_t		find_constant_3(command_id id)
		{
			return constant_3_array[(id >> sort_key_constant3_shift) & sort_key_constant3_mask];
		}
	};

} }
