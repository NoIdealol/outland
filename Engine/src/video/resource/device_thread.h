#pragma once
#include "thread\lockfree_queue.h"
#include "system\thread\thread.h"
#include "..\..\Graphics\lib\module.h"

namespace outland { namespace video 
{
	using device_work_t = function<error_t(gpu::device_id, scratch_allocator_t*)>;
	using device_callback_t = function<void(error_t)>;

	struct device_result_t
	{
		error_t				error;
		device_callback_t	callback;
	};

	class device_thread_t
	{
		struct device_request_t
		{
			device_callback_t		callback;
			device_work_t			work;
		};

	private:
		lockfree_queue_id		_queue_request;
		lockfree_queue_id		_queue_result;
		lockfree_queue_id		_queue_event;
		
		size_t					_memory_request_size;
		size_t					_memory_result_size;
		size_t					_memory_event_size;
		scratch_allocator_t*	_scratch;
		size_t					_memory_scratch_size;
		os::thread_id			_thread;
		uint32_t				_refresh_ms;
		gpu::device_id			_device;
		allocator_t*			_allocator;
		bool					_run;

		gpu::device_event_t		_event_array[8];
		size_t					_event_size;

	private:
		uint8_t	_main();
		error_t	_stop(gpu::device_id, scratch_allocator_t*);
		void	_update_device();
		void	_update_requests();

	public:
		device_thread_t(gpu::device_id device, allocator_t* allocator);
		error_t	init(size_t scratch_size);
		void	clean();

		size_t	work_capacity();
		error_t	work_submit(device_work_t work, device_callback_t callback);
		void	work_flush();
		void	work_epoll(device_result_t* result_array, size_t result_capacity, size_t& result_size);
		void	event_epoll(gpu::device_event_t* event_array, size_t event_capacity, size_t& event_size);
	};


} }
