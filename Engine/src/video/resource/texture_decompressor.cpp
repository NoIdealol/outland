#include "pch.h"
#include "texture_decompressor.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ASSERT(x) O_ASSERT(x)
#define STBI_NO_STDIO



static outland::allocator_t*	stbi__g_allocator = nullptr;

static size_t	stbi__msize(void* memory, outland::allocator_t* allocator);
static void*	stbi__malloc(size_t size, outland::allocator_t* allocator);
static void		stbi__free(void* memory, outland::allocator_t* allocator);
static void*	stbi__realloc(void* old_memory, size_t size, outland::allocator_t* allocator);

#define STBI_MALLOC(size) stbi__malloc(size, stbi__g_allocator)
#define STBI_REALLOC(memory, size) stbi__realloc(memory, size, stbi__g_allocator)
#define STBI_FREE(memory) stbi__free(memory, stbi__g_allocator)

#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STBI_ONLY_BMP
//#define STBI_ONLY_PSD
#define STBI_ONLY_TGA
#define STBI_ONLY_GIF
//#define STBI_ONLY_HDR
//#define STBI_ONLY_PIC
//#define STBI_ONLY_PNM

#include "..\ext\stb\stb_image.h"

using stbi__memory_header_t = outland::uint64_t;

static jmp_buf					stbi__g_jump_buffer;

//--------------------------------------------------
static size_t stbi__msize(void* memory, outland::allocator_t* allocator)
{
	memory = (outland::byte_t*)memory - sizeof(stbi__memory_header_t);
	return size_t(*(stbi__memory_header_t*)memory);
}

//--------------------------------------------------
static void* stbi__malloc(size_t size, outland::allocator_t* allocator)
{
	void* memory = allocator->alloc(size + sizeof(stbi__memory_header_t), alignof(stbi__memory_header_t));
	if (memory)
	{
		*(stbi__memory_header_t*)memory = stbi__memory_header_t(size);
		memory = (outland::byte_t*)memory + sizeof(stbi__memory_header_t);
	}
	else
	{
		O_ASSERT(memory); //memory management not written properly, can crash when allocation fails
		longjmp(stbi__g_jump_buffer, 1);
	}

	return memory;
}

//--------------------------------------------------
static void stbi__free(void* memory, outland::allocator_t* allocator)
{
	memory = (outland::byte_t*)memory - sizeof(stbi__memory_header_t);
	allocator->free(memory, size_t(*(stbi__memory_header_t*)memory) + sizeof(stbi__memory_header_t));
}

//--------------------------------------------------
static void* stbi__realloc(void* old_memory, size_t size, outland::allocator_t* allocator)
{
	void* memory = stbi__malloc(size, allocator);

	size_t old_size = stbi__msize(old_memory, allocator);
	size_t min_size = old_size > size ? size : old_size;
	memcpy(memory, old_memory, min_size);
	stbi__free(old_memory, allocator);

	return memory;
}

namespace outland { namespace video 
{
	/*
	switch (ch)
			{
			case STBI_grey:
				info.format = gpu::format_t::R8_UNORM;
				break;
			case STBI_grey_alpha:
				info.format = gpu::format_t::R8G8_UNORM;
				break;
			case STBI_rgb:
				info.format = gpu::format_t::R8G8B8_UNORM;
				break;
			case STBI_rgb_alpha:
				info.format = gpu::format_t::R8G8B8A8_UNORM;
				break;
			default:
				return err_unknown;
				}
	*/

	
	//--------------------------------------------------
	static void get_texture_info(texture_info_t& info, int x, int y, int ch)
	{
		info.size.width = (uint32_t)x;
		info.size.height = (uint32_t)y;
		info.size.depth = 1;
		info.dimension = gpu::texture_dimension_2D;
		info.mip_size = 1;
		info.array_size = 1;
		info.channel_size = (uint32_t)ch;
	}

	//--------------------------------------------------
	error_t texture_get_info(texture_info_t& info, void const* data, size_t size, scratch_allocator_t* allocator)
	{
		byte_t const* in_buffer = (byte_t const*)data;
		int in_len = (int)size;

		int volatile x;
		int volatile y;
		int volatile ch;
		int volatile result;

		if (setjmp(stbi__g_jump_buffer) == 0)
		{
			result = stbi_info_from_memory(in_buffer, in_len, (int*)&x, (int*)&y, (int*)&ch);
		}
		else
		{
			return err_out_of_memory;
		}

		if (0 == result)
			return err_bad_data;
		
		get_texture_info(info, x, y, ch);
		return err_ok;		
	}

	//--------------------------------------------------
	error_t texture_decompress(texture_info_t& info, byte_t*& pixels, void const* data, size_t size, uint32_t channel_size, scratch_allocator_t* allocator)
	{
		stbi__g_allocator = allocator;

		byte_t const* in_buffer = (byte_t const*)data;
		int in_len = (int)size;

		int volatile x;
		int volatile y;
		int volatile ch;
		byte_t* volatile out_buffer = nullptr;

		if (setjmp(stbi__g_jump_buffer) == 0)
		{
			out_buffer = stbi_load_from_memory(in_buffer, in_len, (int*)&x, (int*)&y, (int*)&ch, 0);
		}
		else
		{
			return err_out_of_memory;
		}

		get_texture_info(info, x, y, ch);
		pixels = out_buffer;
		return err_ok;
	}


} }
