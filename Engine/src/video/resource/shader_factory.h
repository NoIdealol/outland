#pragma once
#include "..\lib\resource_factory.h"
#include "device_thread.h"

namespace outland { namespace video 
{
	class shader_factory_t : resource::factory_t
	{	 
		struct request_t
		{
			shader_factory_t*		factory;
			resource::handle_id*	handle;
			void*					user_ptr;
			gpu::shader_id			shader;
			void const*				data_buffer;
			size_t					data_size;
			error_t					error;
		};

	private:
		request_t*				_request_data;
		size_t					_request_size;
		array<request_t*>		_request_pool;
		deque<request_t*>		_done_queue;
		device_thread_t*		_device;
		//allocator_t*			_allocator;

		static error_t	_create(request_t* request, gpu::device_id device, scratch_allocator_t* scratch);
		static void		_on_create(request_t* request, error_t err);
		static error_t	_destroy(gpu::shader_id shader, gpu::device_id device, scratch_allocator_t* scratch);
		static void		_on_destroy(gpu::shader_id shader, error_t err);

	public:
		shader_factory_t(device_thread_t* device, allocator_t* object_allocator, allocator_t* factory_allocator);
		error_t	init(size_t queue_size);
		void	clean();

		error_t	create(resource::handle_id* handle, resource::resource_create_t const& create) final;
		error_t	destroy(resource::handle_id const* handle) final;
		void	epoll(resource::create_result_t* result_array, size_t result_capacity, size_t& result_size) final;

		void	update();
	};

} }