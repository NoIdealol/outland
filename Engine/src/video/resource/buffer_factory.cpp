#include "pch.h"
#include "buffer_factory.h"
#include "buffer_decompressor.h"
#include "math\integer.h"
namespace outland { namespace video 
{

	//--------------------------------------------------
	buffer_factory_t::buffer_factory_t(device_thread_t* device, allocator_t* object_allocator, allocator_t* factory_allocator)
		: _request_data(nullptr)
		, _request_size(0)
		, _request_pool(factory_allocator)
		, _copy_queue(factory_allocator)
		, _done_queue(factory_allocator)
		, _device(device)
		, _allocator(object_allocator)
	{
		_struct_info.align = m::max(alignof(vertex_buffer_t), m::max(alignof(index_buffer_t), alignof(uniform_buffer_t)));
		_struct_info.size = m::max(sizeof(vertex_buffer_t), m::max(sizeof(index_buffer_t), sizeof(uniform_buffer_t)));
	}

	//--------------------------------------------------
	error_t	buffer_factory_t::init(size_t queue_size)
	{
		error_t err;
		err = _request_pool.reserve(queue_size);
		if (err)
			return err;

		err = _copy_queue.reserve(queue_size);
		if (err)
			return err;

		err = _done_queue.reserve(queue_size);
		if (err)
			return err;

		err = u::mem_alloc(_request_pool.allocator(), _request_data, queue_size);
		if (err)
			return err;

		_request_size = queue_size;

		for (size_t i = 0; i < _request_size; ++i)
		{
			_request_pool.push_back(_request_data + i);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	buffer_factory_t::clean()
	{
		O_ASSERT(_request_size == _request_pool.size());
		O_ASSERT(0 == _copy_queue.size());
		O_ASSERT(0 == _done_queue.size());
		_request_pool.clear();
		u::mem_free(_request_pool.allocator(), _request_data, _request_size);
	}

	//--------------------------------------------------
	error_t	buffer_factory_t::_create(request_t* request, gpu::device_id device, scratch_allocator_t* scratch)
	{
		error_t err;
		scratch_lock_t lock = { scratch };
	
		buffer_info_t buffer_info;
		err = buffer_get_info(buffer_info, request->data_buffer, request->data_size, scratch);
		if (err)
			return err;

		request->buffer->type = buffer_info.type;
		switch (buffer_info.type)
		{
		case gpu::buffer_type_vertex:
		{
			auto* vb = static_cast<vertex_buffer_t*>(request->buffer);
			vb->vertex_size = buffer_info.entry_size;
			vb->attribute_size = buffer_info.format_size;
			for (size_t i = 0; i < buffer_info.format_size; ++i)
			{
				vb->attribute_array[i] = buffer_info.format_array[i];
			}
			break;
		}
		case gpu::buffer_type_index:
		{
			if (1 != buffer_info.format_size)
				return err_bad_data;

			auto* ib = static_cast<index_buffer_t*>(request->buffer);
			ib->index_size = buffer_info.entry_size;
			ib->format = buffer_info.format_array[0] == gpu::format_t::R16_UINT ? gpu::index_format_uint16 : gpu::index_format_uint32;
			break;
		}
		case gpu::buffer_type_uniform:
		{
			if (0 != (buffer_info.number_size & (4 - 1)))
				return err_bad_data;

			auto* ub = static_cast<uniform_buffer_t*>(request->buffer);
			ub->vec4_size = buffer_info.number_size >> 2; // div by 4
			
			break;
		}
		default:
			O_ASSERT(false);
			return err_bad_data;
		}

		gpu::buffer_create_t create;
		create.type = buffer_info.type;
		create.size = (uint32_t)buffer_info.data_size;
		create.stage_mask = gpu::buffer_type_uniform == buffer_info.type ? gpu::shader_stage_fragment : 0;
		
		return gpu::device::buffer_create(device, request, create, scratch);
	}

	//--------------------------------------------------
	void	buffer_factory_t::_on_create(request_t* request, error_t err)
	{
		if (err)
		{
			request->error = err;
			request->factory->_done_queue.push_back(request);
		}
	}

	//--------------------------------------------------
	void	buffer_factory_t::_on_create_done(request_t* request, gpu::buffer_id buffer, error_t err)
	{
		if (err)
		{
			request->error = err;
			request->factory->_done_queue.push_back(request);
		}
		else
		{
			request->buffer->buffer = buffer;
			request->factory->_copy_queue.push_back(request);
		}
	}

	//--------------------------------------------------
	error_t	buffer_factory_t::_copy(request_t* request, gpu::device_id device, scratch_allocator_t* scratch)
	{
		error_t err;	
		
		gpu::host_to_buffer_copy_t copy;
		copy.user_data = request;
		copy.dst_buffer = request->buffer->buffer;
		copy.src_data = nullptr; //dry run
		
		err = gpu::device::buffer_copy(device, copy); //dry run
		if (err)
			return err; //try again later

		scratch_lock_t lock = { scratch };

		void* data = nullptr;

		buffer_info_t buffer_info;
		err = buffer_decompress(buffer_info, data, request->data_buffer, request->data_size, scratch);
		if (err)
		{
			//terminating error
			//bad data or out of scratch memory
			gpu::device::buffer_destroy(device, request->buffer->buffer);
			return err;
		}
			
		copy.src_data = data;

		err = gpu::device::buffer_copy(device, copy); //submit copy
		O_ASSERT(err_ok == err);
		if (err)
		{
			//terminating error
			//should not happen
			gpu::device::buffer_destroy(device, request->buffer->buffer);
			return err;
		}
		
		return err_ok;
	}

	//--------------------------------------------------
	void	buffer_factory_t::_on_copy(request_t* request, error_t err)
	{
		switch (err)
		{
		case err_too_many_requests:
			request->factory->_copy_queue.push_front(request); //try again with priority
			break;
		case err_ok:
			break;
		default:
			request->error = err;
			request->factory->_done_queue.push_back(request); //buffer is clean now
			break;
		}
	}

	//--------------------------------------------------
	void	buffer_factory_t::_on_copy_done(request_t* request, gpu::buffer_id buffer, error_t err)
	{
		O_ASSERT(err_ok == err);
		request->error = err;
		request->factory->_done_queue.push_back(request);
	}

	//--------------------------------------------------
	error_t	buffer_factory_t::_destroy(gpu::buffer_id buffer, gpu::device_id device, scratch_allocator_t* scratch)
	{
		gpu::device::buffer_destroy(device, buffer);
		return err_ok;
	}

	//--------------------------------------------------
	void	buffer_factory_t::_on_destroy(gpu::buffer_id buffer, error_t err)
	{
		//buffer is invalid handle
		O_ASSERT(err_ok == err);
	}

	//--------------------------------------------------
	error_t	buffer_factory_t::create(resource::handle_id* handle, resource::resource_create_t const& create)
	{
		error_t err;
		if (0 == _request_pool.size())
			return err_too_many_requests;


		if (0 == _device->work_capacity())
			return err_too_many_requests;

		auto* request = _request_pool.last();
		request->factory = this;
		request->handle = handle;
		request->user_ptr = create.user_ptr;
		request->data_buffer = create.data_buffer;
		request->data_size = create.data_size;
		request->error = err_ok;
		request->buffer = (buffer_t*)_allocator->alloc(_struct_info.size, _struct_info.align);
		if (nullptr == request->buffer)
			return err_out_of_memory;

		_request_pool.pop_back();

		device_work_t work;
		work.bind<request_t, &buffer_factory_t::_create>(request);
		device_callback_t callback;
		callback.bind<request_t, &buffer_factory_t::_on_create>(request);

		err = _device->work_submit(work, callback);
		O_ASSERT(err_ok == err);
	
		return err_ok;
	}

	//--------------------------------------------------
	error_t	buffer_factory_t::destroy(resource::handle_id const* handle)
	{
		buffer_t* buffer = *((buffer_t* const*)handle);

		device_work_t work;
		work.bind<gpu::buffer_t, &buffer_factory_t::_destroy>(buffer->buffer);

		device_callback_t callback;
		callback.bind<gpu::buffer_t, &buffer_factory_t::_on_destroy>(buffer->buffer);

		error_t err = _device->work_submit(work, callback);
		if (err)
			return err;

		_allocator->free(buffer, _struct_info.size);

		return err_ok;
	}

	//--------------------------------------------------
	void	buffer_factory_t::epoll(resource::create_result_t* result_array, size_t result_capacity, size_t& result_size)
	{
		result_size = 0;
		while (_done_queue.size() && result_capacity > result_size)
		{
			auto* request = _done_queue.front();
			auto* result = result_array + result_size;
			result->error = request->error;
			result->user_ptr = request->user_ptr;
			if (request->error)
				_allocator->free(request->buffer, _struct_info.size);
			else
				*((buffer_t**)request->handle) = request->buffer;

			_request_pool.push_back(request);

			++result_size;
			_done_queue.pop_front();
		}
	}

	//--------------------------------------------------
	void	buffer_factory_t::update()
	{
		while (_copy_queue.size())
		{
			auto* copy = _copy_queue.front();

			device_work_t work;
			work.bind<request_t, &buffer_factory_t::_copy>(copy);
			device_callback_t callback;
			callback.bind<request_t, &buffer_factory_t::_on_copy>(copy);

			error_t err = _device->work_submit(work, callback);
			if (err)
				break;

			_copy_queue.pop_front();
		}
	}

	//--------------------------------------------------
	void	buffer_factory_t::on_event(gpu::device_event_t const& device_event)
	{
		switch (device_event.type)
		{
		case gpu::device_event_type_buffer_create:
			_on_create_done((request_t*)device_event.buffer.user_data, device_event.buffer.buffer, device_event.buffer.error);
			break;
		case gpu::device_event_type_buffer_copy:
			_on_copy_done((request_t*)device_event.buffer.user_data, device_event.buffer.buffer, device_event.buffer.error);
			break;
		default:
			O_ASSERT(false);
			break;
		}
	}

} }
