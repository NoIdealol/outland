#include "pch.h"
#include "shader_factory.h"

namespace outland { namespace video 
{

	//--------------------------------------------------
	shader_factory_t::shader_factory_t(device_thread_t* device, allocator_t* object_allocator, allocator_t* factory_allocator)
		: _request_data(nullptr)
		, _request_size(0)
		, _request_pool(factory_allocator)
		, _done_queue(factory_allocator)
		, _device(device)
		//, _allocator(object_allocator)
	{
		static_cast<void>(object_allocator);
	}

	//--------------------------------------------------
	error_t	shader_factory_t::init(size_t queue_size)
	{
		error_t err;
		err = _request_pool.reserve(queue_size);
		if (err)
			return err;

		err = _done_queue.reserve(queue_size);
		if (err)
			return err;

		err = u::mem_alloc(_request_pool.allocator(), _request_data, queue_size);
		if (err)
			return err;

		_request_size = queue_size;

		for (size_t i = 0; i < _request_size; ++i)
		{
			_request_pool.push_back(_request_data + i);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	shader_factory_t::clean()
	{
		O_ASSERT(_request_size == _request_pool.size());
		O_ASSERT(0 == _done_queue.size());
		_request_pool.clear();
		u::mem_free(_request_pool.allocator(), _request_data, _request_size);
	}

	//--------------------------------------------------
	error_t	shader_factory_t::_create(request_t* request, gpu::device_id device, scratch_allocator_t* scratch)
	{
		scratch_lock_t lock = { scratch };

		gpu::shader_create_t create;
		create.shader_data = request->data_buffer;
		create.shader_size = request->data_size;
		//create.stage = gpu::shader_stage_fragment;
			
		return gpu::device::shader_create(device, request->shader, create, scratch);
	}

	//--------------------------------------------------
	void	shader_factory_t::_on_create(request_t* request, error_t err)
	{
		request->error = err;
		request->factory->_done_queue.push_back(request);
	}

	//--------------------------------------------------
	error_t	shader_factory_t::_destroy(gpu::shader_id shader, gpu::device_id device, scratch_allocator_t* scratch)
	{
		gpu::device::shader_destroy(device, shader);
		return err_ok;
	}

	//--------------------------------------------------
	void	shader_factory_t::_on_destroy(gpu::shader_id shader, error_t err)
	{
		//shader is invalid handle
		O_ASSERT(err_ok == err);
	}

	//--------------------------------------------------
	error_t	shader_factory_t::create(resource::handle_id* handle, resource::resource_create_t const& create)
	{
		error_t err;
		if (0 == _request_pool.size())
			return err_too_many_requests;

		if (0 == _device->work_capacity())
			return err_too_many_requests;

		auto* request = _request_pool.last();
		request->factory = this;
		request->handle = handle;
		request->user_ptr = create.user_ptr;
		request->data_buffer = create.data_buffer;
		request->data_size = create.data_size;
		request->error = err_ok;
		request->shader = nullptr;
		
		_request_pool.pop_back();

		device_work_t work;
		work.bind<request_t, &shader_factory_t::_create>(request);
		device_callback_t callback;
		callback.bind<request_t, &shader_factory_t::_on_create>(request);

		err = _device->work_submit(work, callback);
		O_ASSERT(err_ok == err);
	
		return err_ok;
	}

	//--------------------------------------------------
	error_t	shader_factory_t::destroy(resource::handle_id const* handle)
	{
		gpu::shader_id shader = *((gpu::shader_id const*)handle);

		device_work_t work;
		work.bind<gpu::shader_t, &shader_factory_t::_destroy>(shader);

		device_callback_t callback;
		callback.bind<gpu::shader_t, &shader_factory_t::_on_destroy>(shader);

		error_t err = _device->work_submit(work, callback);
		if (err)
			return err;

		return err_ok;
	}

	//--------------------------------------------------
	void	shader_factory_t::epoll(resource::create_result_t* result_array, size_t result_capacity, size_t& result_size)
	{
		result_size = 0;
		while (_done_queue.size() && result_capacity > result_size)
		{
			auto* request = _done_queue.front();
			auto* result = result_array + result_size;
			result->error = request->error;
			result->user_ptr = request->user_ptr;
			if (request->error)
			{ 
				//do nothing
			}
			else
			{
				*((gpu::shader_id*)request->handle) = request->shader;
			}

			_request_pool.push_back(request);

			++result_size;
			_done_queue.pop_front();
		}
	}

	//--------------------------------------------------
	void	shader_factory_t::update()
	{
		
	}

	

} }
