#pragma once
#include "..\lib\resource_factory.h"
#include "device_thread.h"
#include "..\register\resource_instance.h"

namespace outland { namespace video 
{
	class texture_factory_t : resource::factory_t
	{	 
		struct request_t
		{
			texture_factory_t*		factory;
			resource::handle_id*	handle;
			void*					user_ptr;
			texture_t*				texture;
			void const*				data_buffer;
			size_t					data_size;
			error_t					error;
		};

	private:
		request_t*				_request_data;
		size_t					_request_size;
		array<request_t*>		_request_pool;
		deque<request_t*>		_copy_queue;
		deque<request_t*>		_done_queue;
		device_thread_t*		_device;
		allocator_t*			_allocator;

		static error_t	_create(request_t* request, gpu::device_id device, scratch_allocator_t* scratch);
		static void		_on_create(request_t* request, error_t err);
		static void		_on_create_done(request_t* request, gpu::texture_id texture, error_t err);
		static error_t	_copy(request_t* request, gpu::device_id device, scratch_allocator_t* scratch);
		static void		_on_copy(request_t* request, error_t err);
		static void		_on_copy_done(request_t* request, gpu::texture_id texture, error_t err);
		static error_t	_destroy_texture(gpu::texture_id texture, gpu::device_id device, scratch_allocator_t* scratch);
		static void		_on_destroy_texture(gpu::texture_id texture, error_t err);
		static error_t	_destroy_view(gpu::shader_view_id view, gpu::device_id device, scratch_allocator_t* scratch);
		static void		_on_destroy_view(gpu::shader_view_id view, error_t err);

		error_t			_channels_to_format(gpu::format_t& format, uint32_t& channel_size);

	public:
		texture_factory_t(device_thread_t* device, allocator_t* object_allocator, allocator_t* factory_allocator);
		error_t	init(size_t queue_size);
		void	clean();

		error_t	create(resource::handle_id* handle, resource::resource_create_t const& create) final;
		error_t	destroy(resource::handle_id const* handle) final;
		void	epoll(resource::create_result_t* result_array, size_t result_capacity, size_t& result_size) final;

		void	update();
		void	on_event(gpu::device_event_t const& device_event);
	};

} }
