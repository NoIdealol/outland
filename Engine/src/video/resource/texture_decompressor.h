#pragma once
#include "..\..\Graphics\lib\module.h"

namespace outland { namespace video 
{
	struct texture_info_t
	{
		uint32_t					channel_size;
		gpu::texture_size_t			size;
		gpu::texture_dimension_t	dimension;
		uint32_t					mip_size;
		uint32_t					array_size;
	};

	error_t texture_get_info(texture_info_t& info, void const* data, size_t size, scratch_allocator_t* allocator);
	error_t texture_decompress(texture_info_t& info, byte_t*& pixels, void const* data, size_t size, uint32_t channel_size, scratch_allocator_t* allocator);

} }
