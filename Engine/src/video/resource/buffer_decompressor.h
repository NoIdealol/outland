#pragma once
#include "..\..\Graphics\lib\module.h"

namespace outland { namespace video 
{

	struct buffer_info_t
	{
		gpu::buffer_type_t	type;
		gpu::format_t		format_array[8];
		size_t				format_size;
		size_t				data_size;
		size_t				entry_size;
		size_t				number_size;
	};

	error_t	buffer_get_info(buffer_info_t& info, void const* data, size_t size, scratch_allocator_t* allocator);
	error_t	buffer_decompress(buffer_info_t& info, void*& content, void const* data, size_t size, scratch_allocator_t* allocator);

} }
