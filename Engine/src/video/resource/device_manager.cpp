#include "pch.h"
#include "device_manager.h"

namespace outland { namespace video 
{

	device_manager_t::device_manager_t(gpu::device_id device, allocator_t* object_allocator, allocator_t* manager_allocator)
		: _thread(device, manager_allocator)
		, _texture_factory(&_thread, object_allocator, manager_allocator)
		, _buffer_factory(&_thread, object_allocator, manager_allocator)
		, _shader_factory(&_thread, object_allocator, manager_allocator)
	{

	}

	error_t	device_manager_t::init(size_t queue_size, size_t device_scratch_size)
	{
		error_t err;

		err = _texture_factory.init(queue_size);
		if (err)
			goto _err_texture;

		err = _buffer_factory.init(queue_size);
		if (err)
			goto _err_buffer;

		err = _shader_factory.init(queue_size);
		if(err)
			goto _err_shader;

		err = _thread.init(device_scratch_size);
		if (err)
			goto _err_thread;

		return err_ok;

	_err_thread:
		_shader_factory.clean();
	_err_shader:
		_buffer_factory.clean();
	_err_buffer:
		_texture_factory.clean();
	_err_texture:
		return err;
	}

	void	device_manager_t::clean()
	{
		_thread.clean();
		_buffer_factory.clean();
		_texture_factory.clean();

	}

	void	device_manager_t::update()
	{

		{
			_thread.work_flush();

			device_result_t result_array[32];
			size_t			result_size;

			_thread.work_epoll(result_array, u::array_size(result_array), result_size);

			for (size_t i = 0; i < result_size; ++i)
			{
				result_array[i].callback(result_array[i].error);
			}
		}

		{
			gpu::device_event_t event_array[32];
			size_t				event_size;

			_thread.event_epoll(event_array, u::array_size(event_array), event_size);

			for (size_t i = 0; i < event_size; ++i)
			{
				switch (event_array[i].type)
				{
				case gpu::device_event_type_texture_create:
				case gpu::device_event_type_texture_copy:
					_texture_factory.on_event(event_array[i]);
					break;
				case gpu::device_event_type_buffer_create:
				case gpu::device_event_type_buffer_copy:
					_buffer_factory.on_event(event_array[i]);
					break;
				default:
					O_ASSERT(false);
					break;
				}
			}
		}

		{
			_texture_factory.update();
			_buffer_factory.update();
		}
	}

} }
