#include "pch.h"
#include "texture_factory.h"
#include "texture_decompressor.h"

namespace outland { namespace video 
{

	//--------------------------------------------------
	error_t		texture_factory_t::_channels_to_format(gpu::format_t& format, uint32_t& channel_size)
	{
		switch (channel_size)
		{
		case 1:
			format = gpu::format_t::R8_UNORM;
			return err_ok;
		case 2:
			format = gpu::format_t::R8G8_UNORM;
			return err_ok;
		case 3:
			channel_size = 4;
			format = gpu::format_t::R8G8B8A8_UNORM;
			return err_ok;
		case 4:
			format = gpu::format_t::R8G8B8A8_UNORM;
			return err_ok;
		default:
			return err_bad_data;
		}
	}

	//--------------------------------------------------
	texture_factory_t::texture_factory_t(device_thread_t* device, allocator_t* object_allocator, allocator_t* factory_allocator)
		: _request_data(nullptr)
		, _request_size(0)
		, _request_pool(factory_allocator)
		, _copy_queue(factory_allocator)
		, _done_queue(factory_allocator)
		, _device(device)
		, _allocator(object_allocator)
	{

	}

	//--------------------------------------------------
	error_t	texture_factory_t::init(size_t queue_size)
	{
		error_t err;
		err = _request_pool.reserve(queue_size);
		if (err)
			return err;

		err = _copy_queue.reserve(queue_size);
		if (err)
			return err;

		err = _done_queue.reserve(queue_size);
		if (err)
			return err;

		err = u::mem_alloc(_request_pool.allocator(), _request_data, queue_size);
		if (err)
			return err;

		_request_size = queue_size;

		for (size_t i = 0; i < _request_size; ++i)
		{
			_request_pool.push_back(_request_data + i);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	texture_factory_t::clean()
	{
		O_ASSERT(_request_size == _request_pool.size());
		O_ASSERT(0 == _copy_queue.size());
		O_ASSERT(0 == _done_queue.size());
		_request_pool.clear();
		u::mem_free(_request_pool.allocator(), _request_data, _request_size);
	}

	//--------------------------------------------------
	error_t	texture_factory_t::_create(request_t* request, gpu::device_id device, scratch_allocator_t* scratch)
	{
		error_t err;
		scratch_lock_t lock = { scratch };

		texture_info_t texture_info;
		err = texture_get_info(texture_info, request->data_buffer, request->data_size, scratch);
		if (err)
			return err;

		gpu::texture_create_t create;
		create.usage_mask = gpu::texture_usage_shader_resource;
		create.type = gpu::texture_type_sampled;
		create.dimension = texture_info.dimension;
		err = request->factory->_channels_to_format(create.format, texture_info.channel_size);
		if (err)
			return err;
		
		create.size = texture_info.size;
		create.mip_size = texture_info.mip_size;
		create.array_size = texture_info.array_size;
		create.stage_mask = gpu::shader_stage_fragment;
		
		return gpu::device::texture_create(device, request, create, scratch);
	}

	//--------------------------------------------------
	void	texture_factory_t::_on_create(request_t* request, error_t err)
	{
		if (err)
		{
			request->error = err;
			request->factory->_done_queue.push_back(request);
		}
	}

	//--------------------------------------------------
	void	texture_factory_t::_on_create_done(request_t* request, gpu::texture_id texture, error_t err)
	{
		if (err)
		{
			request->error = err;
			request->factory->_done_queue.push_back(request);
		}
		else
		{
			request->texture->texture = texture;
			request->factory->_copy_queue.push_back(request);
		}
	}

	//--------------------------------------------------
	error_t	texture_factory_t::_copy(request_t* request, gpu::device_id device, scratch_allocator_t* scratch)
	{
		error_t err;

		texture_info_t texture_info;
		{
			scratch_lock_t lock = { scratch };
			err = texture_get_info(texture_info, request->data_buffer, request->data_size, scratch);
			O_ASSERT(err_ok == err);
			if (err)
			{
				//terminating error
				//should not happen
				gpu::device::texture_destroy(device, request->texture->texture);
				return err;
			}
		}

		gpu::host_to_texture_copy_t copy;
		copy.user_data = request;
		copy.dst_texture = request->texture->texture;
		copy.mip_data_array = nullptr; //dry run
		copy.mip_level = 0;
		copy.mip_size = texture_info.mip_size;
		copy.array_level = 0;
		copy.array_size = texture_info.array_size;

		err = gpu::device::texture_copy(device, copy); //dry run
		if (err)
			return err; //try again later

		scratch_lock_t lock = { scratch };

		//create view
		gpu::texture_shader_view_create_t create;
		create.texture = request->texture->texture;
		err = request->factory->_channels_to_format(create.format, texture_info.channel_size);
		O_ASSERT(err_ok == err);
		if (err)
		{
			//terminating error
			//should not happen
			gpu::device::texture_destroy(device, request->texture->texture);
			return err;
		}

		switch (texture_info.dimension)
		{
		case gpu::texture_dimension_1D:
			create.type = gpu::texture_shader_view_type_1D;
			break;
		case gpu::texture_dimension_2D:
			create.type = gpu::texture_shader_view_type_2D;
			break;
		case gpu::texture_dimension_3D:
			create.type = gpu::texture_shader_view_type_3D;
			break;
		}
		O_ASSERT(1 == texture_info.array_size);
		create.aspect_mask = gpu::texture_aspect_color;
		create.mip_level = 0;
		create.mip_size = texture_info.mip_size;
		create.array_level = 0;
		create.array_size = texture_info.array_size;

		err = gpu::device::shader_view_create(device, request->texture->view, create, scratch);
		if (err)
		{
			//terminating error
			gpu::device::texture_destroy(device, request->texture->texture);
			return err;
		}

		byte_t* pixels;
		err = texture_decompress(texture_info, pixels, request->data_buffer, request->data_size, texture_info.channel_size, scratch);
		if (err)
		{
			//terminating error
			//bad data or out of scratch memory
			gpu::device::shader_view_destroy(device, request->texture->view);
			gpu::device::texture_destroy(device, request->texture->texture);
			return err;
		}
		
		gpu::texture_mip_data_t mip_data_array[16];
		O_ASSERT(u::array_size(mip_data_array) >= texture_info.mip_size);
		for (uint32_t i = 0; i < texture_info.mip_size; ++i)
		{
			auto& mip_data = mip_data_array[i];
			mip_data.src_data = pixels;
			mip_data.byte_row_pitch = (texture_info.size.width >> i) * texture_info.channel_size;
			mip_data.byte_slice_pitch = (texture_info.size.height >> i) * mip_data.byte_row_pitch;

			pixels += mip_data.byte_slice_pitch * (texture_info.size.depth >> i) * texture_info.array_size;
		}
		copy.mip_data_array = mip_data_array;

		err = gpu::device::texture_copy(device, copy); //submit copy
		O_ASSERT(err_ok == err);
		if (err)
		{
			//terminating error
			//should not happen
			gpu::device::shader_view_destroy(device, request->texture->view);
			gpu::device::texture_destroy(device, request->texture->texture);
			return err;
		}
		
		return err_ok;
	}

	//--------------------------------------------------
	void	texture_factory_t::_on_copy(request_t* request, error_t err)
	{
		switch (err)
		{
		case err_too_many_requests:
			request->factory->_copy_queue.push_front(request); //try again with priority
			break;
		case err_ok:
			break;
		default:
			request->error = err;
			request->factory->_done_queue.push_back(request); //texture is clean now
			break;
		}
	}

	//--------------------------------------------------
	void	texture_factory_t::_on_copy_done(request_t* request, gpu::texture_id texture, error_t err)
	{
		O_ASSERT(err_ok == err);
		request->error = err;
		request->factory->_done_queue.push_back(request);
	}

	//--------------------------------------------------
	error_t	texture_factory_t::_destroy_texture(gpu::texture_id texture, gpu::device_id device, scratch_allocator_t* scratch)
	{
		gpu::device::texture_destroy(device, texture);
		return err_ok;
	}

	//--------------------------------------------------
	void	texture_factory_t::_on_destroy_texture(gpu::texture_id texture, error_t err)
	{
		//texture is invalid handle
		O_ASSERT(err_ok == err);
	}

	//--------------------------------------------------
	error_t	texture_factory_t::_destroy_view(gpu::shader_view_id view, gpu::device_id device, scratch_allocator_t* scratch)
	{
		gpu::device::shader_view_destroy(device, view);
		return err_ok;
	}

	//--------------------------------------------------
	void	texture_factory_t::_on_destroy_view(gpu::shader_view_id view, error_t err)
	{
		O_ASSERT(err_ok == err);
	}

	//--------------------------------------------------
	error_t	texture_factory_t::create(resource::handle_id* handle, resource::resource_create_t const& create)
	{
		error_t err;

		if (0 == _request_pool.size())
			return err_too_many_requests;

		if (0 == _device->work_capacity())
			return err_too_many_requests;

		auto* request = _request_pool.last();
		request->factory = this;
		request->handle = handle;
		request->user_ptr = create.user_ptr;
		request->data_buffer = create.data_buffer;
		request->data_size = create.data_size;
		request->error = err_ok;
		request->texture = (texture_t*)_allocator->alloc(sizeof(texture_t), alignof(texture_t));
		if (nullptr == request->texture)
			return err_out_of_memory;

		_request_pool.pop_back();

		device_work_t work;
		work.bind<request_t, &texture_factory_t::_create>(request);
		device_callback_t callback;
		callback.bind<request_t, &texture_factory_t::_on_create>(request);

		err = _device->work_submit(work, callback);
		O_ASSERT(err_ok == err);

		return err_ok;
	}

	//--------------------------------------------------
	error_t	texture_factory_t::destroy(resource::handle_id const* handle)
	{
		error_t err;

		if (2 > _device->work_capacity())
			return err_too_many_requests;

		texture_t* texture = *((texture_t* const*)handle);

		device_work_t work;
		device_callback_t callback;

		work.bind<gpu::texture_t, &texture_factory_t::_destroy_texture>(texture->texture);
		callback.bind<gpu::texture_t, &texture_factory_t::_on_destroy_texture>(texture->texture);

		err = _device->work_submit(work, callback);
		O_ASSERT(err_ok == err);

		work.bind<gpu::shader_view_t, &texture_factory_t::_destroy_view>(texture->view);
		callback.bind<gpu::shader_view_t, &texture_factory_t::_on_destroy_view>(texture->view);

		err = _device->work_submit(work, callback);
		O_ASSERT(err_ok == err);

		_allocator->free(texture, sizeof(texture_t));

		return err_ok;
	}

	//--------------------------------------------------
	void	texture_factory_t::epoll(resource::create_result_t* result_array, size_t result_capacity, size_t& result_size)
	{
		result_size = 0;
		while (_done_queue.size() && result_capacity > result_size)
		{
			auto* request = _done_queue.front();
			auto* result = result_array + result_size;
			result->error = request->error;
			result->user_ptr = request->user_ptr;
			if (request->error)
				_allocator->free(request->texture, sizeof(texture_t));
			else
				*((texture_t**)request->handle) = request->texture;

			_request_pool.push_back(request);

			++result_size;
			_done_queue.pop_front();
		}
	}

	//--------------------------------------------------
	void	texture_factory_t::update()
	{
		while (_copy_queue.size())
		{
			auto* copy = _copy_queue.front();

			device_work_t work;
			work.bind<request_t, &texture_factory_t::_copy>(copy);
			device_callback_t callback;
			callback.bind<request_t, &texture_factory_t::_on_copy>(copy);

			error_t err = _device->work_submit(work, callback);
			if (err)
				break;

			_copy_queue.pop_front();
		}
	}

	//--------------------------------------------------
	void	texture_factory_t::on_event(gpu::device_event_t const& device_event)
	{
		switch (device_event.type)
		{
		case gpu::device_event_type_texture_create:
			_on_create_done((request_t*)device_event.texture.user_data, device_event.texture.texture, device_event.texture.error);
			break;
		case gpu::device_event_type_texture_copy:
			_on_copy_done((request_t*)device_event.texture.user_data, device_event.texture.texture, device_event.texture.error);
			break;
		default:
			O_ASSERT(false);
			break;
		}
	}

} }
