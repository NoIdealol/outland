#pragma once
#include "device_thread.h"
#include "texture_factory.h"
#include "buffer_factory.h"
#include "shader_factory.h"

namespace outland { namespace video 
{

	class device_manager_t
	{
		device_thread_t		_thread;
		texture_factory_t	_texture_factory;
		buffer_factory_t	_buffer_factory;
		shader_factory_t	_shader_factory;

	public:
		device_manager_t(gpu::device_id device, allocator_t* object_allocator, allocator_t* manager_allocator);
		error_t	init(size_t queue_size, size_t device_scratch_size);
		void	clean();
		void	update();
	};

} }
