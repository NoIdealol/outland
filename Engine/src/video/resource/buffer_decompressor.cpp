#include "pch.h"
#include "buffer_decompressor.h"
#include "utility\json.h"

namespace outland { namespace video 
{
	namespace buffer_json
	{
		static error_t test_header(void const* data, size_t size)
		{
			size_t const min_json_size = 64;
			if (min_json_size > size)
				return err_bad_data;

			char8_t const* str = (char8_t const*)data;
			if ('{' != str[0])
				return err_bad_data;
			if (']' != str[size - 1])
				return err_bad_data;

			return err_ok;
		}

		static error_t validate_header_version(buffer_info_t& info, json::number_t& version)
		{
			int64_t const current_version = 0;
			if (version.exponent || version.mantissa != current_version)
				return err_bad_data;

			return err_ok;
		}

		static error_t validate_header_type(buffer_info_t& info, json::string_t& type)
		{
			if (u::str_cmp(type.str, "vertex", type.len))
			{
				info.type = gpu::buffer_type_vertex;
				return err_ok;
			}

			if (u::str_cmp(type.str, "index", type.len))
			{
				info.type = gpu::buffer_type_index;
				return err_ok;
			}

			if (u::str_cmp(type.str, "uniform", type.len))
			{
				info.type = gpu::buffer_type_uniform;
				return err_ok;
			}

			return err_bad_data;
		}

		static error_t validate_header_size(buffer_info_t& info, json::number_t& size, json::array_t& desc)
		{
			if (u::array_size(info.format_array) < desc.element_size)
				return err_bad_data;

			if (0 == desc.element_size)
				return err_bad_data;

			info.format_size = 0;
			size_t num_size = 0;
			for (auto& it : desc)
			{
				auto& format = it.string;

				if (u::str_cmp(format.str, "float", format.len))
				{
					info.format_array[info.format_size] = gpu::format_t::R32_FLOAT;
					++info.format_size;
					num_size += 1;
					continue;
				}

				if (u::str_cmp(format.str, "float2", format.len))
				{
					info.format_array[info.format_size] = gpu::format_t::R32G32_FLOAT;
					++info.format_size;
					num_size += 2;
					continue;
				}

				if (u::str_cmp(format.str, "float3", format.len))
				{
					info.format_array[info.format_size] = gpu::format_t::R32G32B32_FLOAT;
					++info.format_size;
					num_size += 3;
					continue;
				}

				if (u::str_cmp(format.str, "float4", format.len))
				{
					info.format_array[info.format_size] = gpu::format_t::R32G32B32A32_FLOAT;
					++info.format_size;
					num_size += 4;
					continue;
				}		

				if (u::str_cmp(format.str, "uint16", format.len))
				{
					info.format_array[info.format_size] = gpu::format_t::R16_UINT;
					++info.format_size;
					num_size += 1;
					continue;
				}

				if (u::str_cmp(format.str, "uint32", format.len))
				{
					info.format_array[info.format_size] = gpu::format_t::R32_UINT;
					++info.format_size;
					num_size += 1;
					continue;
				}

				return err_bad_data;
			}

			if (size.exponent || 0 > size.mantissa)
				return err_bad_data;

			info.entry_size = (size_t)size.mantissa;
			info.number_size = (size_t)size.mantissa * num_size;

			return err_ok;
		}

		static error_t validate_header(buffer_info_t& info, json::object_t& header, json::array_t& desc)
		{
			error_t err;

			json::value_t*	header_version = nullptr;
			json::value_t*	header_type = nullptr;
			json::value_t*	header_size = nullptr;

			if (3 != header.member_size)
				return err_bad_data;

			for (auto& it : header)
			{
				if (u::str_cmp(it.name.str, "version", it.name.len))
					header_version = &it.value;

				if (u::str_cmp(it.name.str, "type", it.name.len))
					header_type = &it.value;

				if (u::str_cmp(it.name.str, "size", it.name.len))
					header_size = &it.value;
			}

			if (!header_version || json::type_number != header_version->type)
				return err_bad_data;

			if (!header_type || json::type_string != header_type->type)
				return err_bad_data;

			if (!header_size || json::type_number != header_size->type)
				return err_bad_data;

			//check version
			err = validate_header_version(info, header_version->number);
			if (err)
				return err;

			//check type
			err = validate_header_type(info, header_type->string);
			if (err)
				return err;

			//check size
			err = validate_header_size(info, header_size->number, desc);
			if (err)
				return err;
			
			return err_ok;
		}

		static error_t decompress_data(buffer_info_t& info, void*& content, json::array_t& data, allocator_t* allocator)
		{
			if (gpu::format_t::R16_UINT == info.format_array[0])
			{
				if (gpu::buffer_type_index != info.type)
					return err_bad_data;

				if (1 != info.format_size)
					return err_bad_data;

				info.data_size = sizeof(uint16_t) * data.element_size;

				if (nullptr == allocator)
					return err_ok; //dry run;

				static_assert(alignof(void*) >= alignof(uint16_t), "");
				uint16_t* indices = (uint16_t*)allocator->alloc(sizeof(uint16_t) * data.element_size, alignof(void*) * 2);
				if (!indices)
					return err_out_of_memory;

				content = indices;

				for (auto& it : data)
				{
					if (json::type_number != it.type || it.number.exponent || c::max_uint16 < it.number.mantissa || 0 > it.number.mantissa)
					{
						allocator->free(content, sizeof(uint16_t) * data.element_size);
						content = nullptr;
						return err_bad_data;
					}

					*indices = (uint16_t)it.number.mantissa;
					++indices;
				}
	
				return err_ok;
			}
			else if(gpu::format_t::R32_UINT == info.format_array[0])
			{
				if (gpu::buffer_type_index != info.type)
					return err_bad_data;

				if (1 != info.format_size)
					return err_bad_data;

				info.data_size = sizeof(uint32_t) * data.element_size;

				if (nullptr == allocator)
					return err_ok; //dry run;

				static_assert(alignof(void*) >= alignof(uint32_t), "");
				uint32_t* indices = (uint32_t*)allocator->alloc(sizeof(uint32_t) * data.element_size, alignof(void*) * 2);
				if (!indices)
					return err_out_of_memory;

				content = indices;

				for (auto& it : data)
				{
					if (json::type_number != it.type || it.number.exponent || c::max_uint32 < it.number.mantissa || 0 > it.number.mantissa)
					{
						allocator->free(content, sizeof(uint32_t) * data.element_size);
						content = nullptr;
						return err_bad_data;
					}

					*indices = (uint32_t)it.number.mantissa;
					++indices;
				}

				return err_ok;
			}
			else
			{
				if (gpu::buffer_type_vertex != info.type && gpu::buffer_type_uniform != info.type)
					return err_bad_data;

				for (size_t i = 0; i < info.format_size; ++i)
				{
					if (gpu::format_t::R16_UINT == info.format_array[i] || gpu::format_t::R32_UINT == info.format_array[i])
						return err_bad_data;
				}

				info.data_size = sizeof(float) * data.element_size;

				if (nullptr == allocator)
					return err_ok; //dry run;

				static_assert(alignof(void*) >= alignof(float), "");
				float* vertices = (float*)allocator->alloc(sizeof(float) * data.element_size, alignof(void*) * 2);
				if (!vertices)
					return err_out_of_memory;

				content = vertices;

				for (auto& it : data)
				{
					if (json::type_number != it.type)
					{
						allocator->free(content, sizeof(float) * data.element_size);
						content = nullptr;
						return err_bad_data;
					}

					*vertices = json::to_float(it.number);
					++vertices;
				}

				return err_ok;
			}
		}

		static error_t parse_header(buffer_info_t& info, void const* data, size_t size, size_t* size_parsed, scratch_allocator_t* allocator)
		{
			error_t err;

			json::document_t doc;
			err = json::parse_document(doc, data, size, size_parsed, allocator, allocator);
			if (err)
				return err;

			if (json::type_object != doc.value.type)
				return err_bad_data;

			auto& root = doc.value.object;

			if (2 != root.member_size)
				return err_bad_data;

			json::value_t*	root_header = nullptr;
			json::value_t*	root_desc = nullptr;

			for (auto& it : root)
			{
				if (u::str_cmp(it.name.str, "header", it.name.len))
					root_header = &it.value;

				if (u::str_cmp(it.name.str, "desc", it.name.len))
					root_desc = &it.value;
			}

			if (!root_header || json::type_object != root_header->type)
				return err_bad_data;

			if (!root_desc || json::type_array != root_desc->type)
				return err_bad_data;

			err = validate_header(info, root_header->object, root_desc->array);
			if (err)
				return err;

			void* dummy_conent = nullptr;
			json::array_t dummy_data;
			dummy_data.element_array = nullptr;
			dummy_data.element_size = info.number_size;
			err = decompress_data(info, dummy_conent, dummy_data, nullptr); //dry run
			if (err)
				return err;

			return err_ok;
		}

		static error_t get_info(buffer_info_t& info, void const* data, size_t size, scratch_allocator_t* allocator)
		{
			return parse_header(info, data, size, nullptr, allocator);
		}

		static error_t decompress(buffer_info_t& info, void*& content, void const* data, size_t size, scratch_allocator_t* allocator)
		{
			error_t err;

			size_t header_size = 0;
			err = parse_header(info, data, size, &header_size, allocator);
			if (err)
				return err;

			json::document_t doc;
			err = json::parse_document(doc, (byte_t const*)data + header_size, size - header_size, nullptr, allocator, allocator);
			if (err)
				return err;

			if (json::type_array != doc.value.type)
				return err_bad_data;

			auto& root = doc.value.array;

			if (info.number_size != root.element_size)
				return err_bad_data;

			err = decompress_data(info, content, root, allocator);
			if (err)
				return err;

			return err_ok;
		}
	}

	namespace buffer_binary
	{
		static error_t test_header(void const* data, size_t size)
		{
			return err_not_implemented;
		}

		static error_t get_info(buffer_info_t& info, void const* data, size_t size, scratch_allocator_t* allocator)
		{
			return err_not_implemented;
		}

		static error_t decompress(buffer_info_t& info, void*& content, void const* data, size_t size, scratch_allocator_t* allocator)
		{
			return err_not_implemented;
		}
	}

	error_t	buffer_get_info(buffer_info_t& info, void const* data, size_t size, scratch_allocator_t* allocator)
	{
		error_t err;

		err = buffer_binary::test_header(data, size);
		if (err_ok == err)
			return buffer_binary::get_info(info, data, size, allocator);

		err = buffer_json::test_header(data, size);
		if (err_ok == err)
			return buffer_json::get_info(info, data, size, allocator);

		return err_bad_data;
	}

	error_t	buffer_decompress(buffer_info_t& info, void*& content, void const* data, size_t size, scratch_allocator_t* allocator)
	{
		error_t err;

		err = buffer_binary::test_header(data, size);
		if (err_ok == err)
			return buffer_binary::decompress(info, content, data, size, allocator);

		err = buffer_json::test_header(data, size);
		if (err_ok == err)
			return buffer_json::decompress(info, content, data, size, allocator);

		return err_bad_data;
	}

} }
