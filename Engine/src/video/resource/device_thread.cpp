#include "pch.h"
#include "device_thread.h"

namespace outland { namespace video 
{

	//--------------------------------------------------
	device_thread_t::device_thread_t(gpu::device_id device, allocator_t* allocator)
		: _refresh_ms(10)
		, _run(false)
		, _device(device)
		, _queue_request(nullptr)
		, _queue_result(nullptr)
		, _queue_event(nullptr)
		, _scratch(nullptr)
		, _event_size(0)
		, _allocator(allocator)
	{

	}

	//--------------------------------------------------
	void	device_thread_t::_update_device()
	{
		size_t event_size;
		gpu::device::update(_device, _event_array + _event_size, u::array_size(_event_array) - _event_size, event_size);
		_event_size += event_size;

		if (_event_size)
		{
			lockfree_queue::producer_evict(_queue_event);
			size_t event_capacity = lockfree_queue::producer_capacity(_queue_event);

			size_t event_sent = 0;
			while (event_sent < event_capacity && event_sent < _event_size)
			{
				gpu::device_event_t* dev = (gpu::device_event_t*)lockfree_queue::produce(_queue_event);
				*dev = _event_array[event_sent];
				++event_sent;
			}

			lockfree_queue::producer_flush(_queue_event);

			_event_size -= event_sent;
			if (_event_size)
			{
				u::array_move(_event_array, _event_array + event_sent, _event_size);
			}
		}
	}

	//--------------------------------------------------
	void	device_thread_t::_update_requests()
	{
		lockfree_queue::consumer_evict(_queue_request);
		lockfree_queue::producer_evict(_queue_result);

		size_t req_size = lockfree_queue::consumer_size(_queue_request);
		size_t res_capacity = lockfree_queue::producer_capacity(_queue_result);
		while (req_size && res_capacity)
		{
			device_request_t* req = (device_request_t*)lockfree_queue::consume(_queue_request);
			device_result_t* res = (device_result_t*)lockfree_queue::produce(_queue_result);
			res->error = req->work(_device, _scratch);
			res->callback = req->callback;

			--req_size;
			--res_capacity;
		}

		lockfree_queue::producer_flush(_queue_result);
		lockfree_queue::consumer_flush(_queue_request);
	}

	//--------------------------------------------------
	uint8_t	device_thread_t::_main()
	{
		while (_run)
		{
			_update_device();
			_update_requests();
			os::thread::sleep(_refresh_ms);
		}

		return 0;
	}

	//--------------------------------------------------
	error_t	device_thread_t::_stop(gpu::device_id, scratch_allocator_t*)
	{
		_run = false;
		return err_ok;
	}

	//--------------------------------------------------
	error_t	device_thread_t::init(size_t scratch_size)
	{
		error_t err;

		lockfree_queue::create_t create_request;
		memory_info_t info_request;
		create_request.element_info.align = alignof(device_request_t);
		create_request.element_info.size = sizeof(device_request_t);
		create_request.element_size = 8;
		lockfree_queue::memory_info(info_request, create_request);
		void* memory_request_queue = _allocator->alloc(info_request.size, info_request.align);
		if (nullptr == memory_request_queue)
			return err_out_of_memory;

		lockfree_queue::create_t create_result;
		memory_info_t info_result;
		create_result.element_info.align = alignof(device_result_t);
		create_result.element_info.size = sizeof(device_result_t);
		create_result.element_size = 8;
		lockfree_queue::memory_info(info_result, create_result);
		void* memory_result_queue = _allocator->alloc(info_result.size, info_result.align);
		if (nullptr == memory_result_queue)
		{
			_allocator->free(memory_request_queue, info_request.size);
			return err_out_of_memory;
		}

		lockfree_queue::create_t create_event;
		memory_info_t info_event;
		create_event.element_info.align = alignof(gpu::device_event_t);
		create_event.element_info.size = sizeof(gpu::device_event_t);
		create_event.element_size = 8;
		lockfree_queue::memory_info(info_event, create_event);
		void* memory_event_queue = _allocator->alloc(info_event.size, info_event.align);
		if (nullptr == memory_event_queue)
		{
			_allocator->free(memory_result_queue, info_result.size);
			_allocator->free(memory_request_queue, info_request.size);
			return err_out_of_memory;
		}

		memory_info_t info_scratch;
		scratch_allocator::memory_info(info_scratch);
		O_ASSERT(1024 > info_scratch.size); //sanity checks
		if (scratch_size < 1024 * 256)
			scratch_size = 1024 * 256;

		_memory_scratch_size = scratch_size;
		void* memory_scratch = _allocator->alloc(_memory_scratch_size, info_scratch.align);
		if (nullptr == memory_scratch)
		{
			_allocator->free(memory_event_queue, info_event.size);
			_allocator->free(memory_result_queue, info_result.size);
			_allocator->free(memory_request_queue, info_request.size);
			return err_out_of_memory;
		}

		scratch_allocator::create_t create_scratch;
		create_scratch.page_source = _allocator;
		create_scratch.page_size = 1024 * 256;
		create_scratch.init_buffer = (byte_t*)memory_scratch + info_scratch.size;
		create_scratch.init_size = _memory_scratch_size - info_scratch.size;
		scratch_allocator::create(_scratch, create_scratch, memory_scratch);

		lockfree_queue::create(_queue_request, create_request, memory_request_queue);
		lockfree_queue::create(_queue_result, create_result, memory_result_queue);
		lockfree_queue::create(_queue_event, create_event, memory_event_queue);

		_run = true;

		err = os::thread::create(
			_thread,
			os::thread::main_t().bind<device_thread_t, &device_thread_t::_main>(this),
			"gpu::device",
			1024 * 64);

		if (err)
		{
			lockfree_queue::destroy(_queue_event);
			lockfree_queue::destroy(_queue_result);
			lockfree_queue::destroy(_queue_request);
			scratch_allocator::destroy(_scratch);

			_allocator->free(memory_scratch, _memory_scratch_size);
			_allocator->free(memory_event_queue, info_event.size);
			_allocator->free(memory_result_queue, info_result.size);
			_allocator->free(memory_request_queue, info_request.size);

			return err;
		}

		_memory_event_size = info_event.size;
		_memory_result_size = info_result.size;
		_memory_request_size = info_request.size;

		return err_ok;
	}

	//--------------------------------------------------
	void	device_thread_t::clean()
	{
		while (true)
		{
			lockfree_queue::producer_evict(_queue_request);
			if (lockfree_queue::producer_capacity(_queue_request))
				break;

			os::thread::sleep(10);
		}

		device_request_t* req = (device_request_t*)lockfree_queue::produce(_queue_request);
		req->callback.clear();
		req->work.bind<device_thread_t, &device_thread_t::_stop>(this);
		lockfree_queue::producer_flush(_queue_request);

		error_t err = os::thread::join(_thread);
		O_ASSERT(err_ok == err);

		lockfree_queue::destroy(_queue_event);
		lockfree_queue::destroy(_queue_result);
		lockfree_queue::destroy(_queue_request);
		scratch_allocator::destroy(_scratch);
		_allocator->free(_scratch, _memory_scratch_size);
		_allocator->free(_queue_event, _memory_event_size);
		_allocator->free(_queue_result, _memory_result_size);
		_allocator->free(_queue_request, _memory_request_size);
	}

	//--------------------------------------------------
	size_t	device_thread_t::work_capacity()
	{
		return lockfree_queue::producer_capacity(_queue_request);
	}

	//--------------------------------------------------
	error_t	device_thread_t::work_submit(device_work_t work, device_callback_t callback)
	{
		if (0 == lockfree_queue::producer_capacity(_queue_request))
			return err_too_many_requests;

		device_request_t* req = (device_request_t*)lockfree_queue::produce(_queue_request);
		req->callback = callback;
		req->work = work;

		return err_ok;
	}

	//--------------------------------------------------
	void	device_thread_t::work_flush()
	{
		lockfree_queue::producer_flush(_queue_request);
		lockfree_queue::producer_evict(_queue_request);
	}

	//--------------------------------------------------
	void	device_thread_t::work_epoll(device_result_t* result_array, size_t result_capacity, size_t& result_size)
	{
		result_size = 0;
		lockfree_queue::consumer_evict(_queue_result);
		size_t result_ready = lockfree_queue::consumer_size(_queue_result);
		while (result_ready > result_size && result_capacity > result_size)
		{
			result_array[result_size] = *(device_result_t*)lockfree_queue::consume(_queue_result);
			++result_size;
		}

		lockfree_queue::consumer_flush(_queue_result);
	}

	//--------------------------------------------------
	void	device_thread_t::event_epoll(gpu::device_event_t* event_array, size_t event_capacity, size_t& event_size)
	{
		event_size = 0;
		lockfree_queue::consumer_evict(_queue_event);
		size_t event_ready = lockfree_queue::consumer_size(_queue_event);
		while (event_ready > event_size && event_capacity > event_size)
		{
			event_array[event_size] = *(gpu::device_event_t*)lockfree_queue::consume(_queue_event);
			++event_size;
		}

		lockfree_queue::consumer_flush(_queue_event);
	}

} }
