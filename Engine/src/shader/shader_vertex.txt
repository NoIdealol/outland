#version 450
//#extension GL_ARB_separate_shader_objects : enable

/*layout(push_constant) uniform push_constants
{
    float scale;
};*/

layout(set = 1, binding = 0) uniform Transform0
{
    mat4 proj;
};

layout(set = 1, binding = 1) uniform Transform1
{
	mat4 rot;
};



layout(location = 0) in vec3 position;
layout(location = 0) out vec2 uv;

void main()
{
	//vec2 pos = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
	//pos = pos * 0.5f + -0.5f;
	//pos *= scale2;
	//gl_Position = vec4(pos, 0.0f, 1.0f);

	uv = position.xy * 0.5f + 0.5f;

	vec4 pos = vec4(position, 1.0f);
	pos = pos * rot;
	
	pos.z += 5.0f;
	pos.y += 1.5f;
	gl_Position = pos  * proj;
}