#pragma once

namespace outland { namespace json
{
	
	//struct member_array_t;
	//struct element_array_t;
	struct member_t;
	struct value_t;

	enum type_t
	{
		type_object,
		type_array,
		type_string,
		type_number,
		type_true,
		type_false,
		type_null,
	};

	struct string_t
	{
		char8_t const*	str;
		size_t			len;
	};

	struct number_t
	{
		int64_t		mantissa;
		int64_t		exponent;
	};

	struct object_t
	{
		size_t		member_size;
		member_t*	member_array;
	};

	struct array_t
	{
		size_t		element_size;
		value_t*	element_array;
	};

	struct value_t
	{
		type_t			type;
		union 
		{
			string_t	string;
			number_t	number;
			object_t	object;
			array_t		array;
		};
	};


	struct member_t
	{
		string_t		name;	
		value_t			value;
	};

	/*struct member_array_t
	{
		member_t*	member_array;
		size_t		member_size;
	};*/

	/*struct element_t
	{
		value_t		value;
	};*/

	/*struct element_array_t
	{
		value_t*	element_array;
		size_t		element_size;
	};*/

	struct document_t
	{
		value_t			value;
		allocator_t*	allocator;
		size_t			line_no;
		size_t			char_no;
		char8_t const*	msg;
	};


	//string_t*	as_string(value_t* value);
	
	inline value_t*				begin(array_t& array) { return array.element_array; }
	inline value_t*				end(array_t& array) { return array.element_array + array.element_size; }
	inline member_t*			begin(object_t& object) { return object.member_array; }
	inline member_t*			end(object_t& object) { return object.member_array + object.member_size; }

	error_t parse_document(document_t& document, void const* data, size_t size, size_t* size_parsed, allocator_t* document_allocator, allocator_t* parse_allocator);
	void	free_document(document_t& document);
	float	to_float(number_t const& number);
	double	to_double(number_t const& number);
} }
