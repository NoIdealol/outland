#include "pch.h"
#include "json.h"
#include "math\integer.h"

namespace outland { namespace json
{
#define UNEXPECTED "unexpected "
#define EOF "end of file"
#define UNEXPECTED_EOF UNEXPECTED EOF
#define EXPECTED ", expected "
#define VALUE "value"
#define ALLOCATION_FAILED "allocation failed"
#define CONSTANT "constant"
#define TRUE "true"
#define FALSE "false"
#define NULL "null"
#define TECHNICAL_LIMITATION "technical limitation"
#define CHARACTER "character"
#define STRING "string"
#define ESCAPE_CHARACTER "escape " CHARACTER
#define DIGIT "digit"

	struct parser_t
	{
		allocator_t*	parse_allocator;
		allocator_t*	document_allocator;
		char8_t const*	input_data;
		char8_t const*	input_end;
		size_t			input_size;
		size_t			line_no;
		size_t			char_no;
		char8_t const*	msg;
	};

	static_assert(alignof(member_t) == alignof(value_t), "");

	static error_t	parse_value(parser_t* parser, value_t& value);
	static void		free_value(document_t* document, value_t& value);

	//--------------------------------------------------
	static void*	document_alloc(parser_t* parser, size_t size)
	{
		return parser->document_allocator->alloc(size, alignof(member_t));
	}

	//--------------------------------------------------
	static void document_free(document_t* document, void* memory, size_t size)
	{
		document->allocator->free(memory, size);
	}

	//--------------------------------------------------
	static void*	parse_realloc(parser_t* parser, void* memory, size_t old_size, size_t new_size)
	{
		size_t new_alloc_size = m::to_pow2(new_size);
		size_t old_alloc_size = m::to_pow2(old_size);

		if (new_alloc_size <= old_alloc_size)
			return memory;

		void* new_memory = parser->parse_allocator->alloc(new_alloc_size, alignof(member_t));
		if (nullptr == new_memory)
			return nullptr;

		u::mem_copy(new_memory, memory, old_size);
		parser->parse_allocator->free(memory, old_alloc_size);

		return new_memory;
	}

	//--------------------------------------------------
	static void*	parse_alloc(parser_t* parser, size_t size)
	{
		size_t alloc_size = m::to_pow2(size);
		return parser->parse_allocator->alloc(alloc_size, alignof(member_t));
	}

	//--------------------------------------------------
	static void	parse_free(parser_t* parser, void* memory,  size_t size)
	{
		size_t alloc_size = m::to_pow2(size);
		parser->parse_allocator->free(memory, alloc_size);
	}

	//--------------------------------------------------
	static void parse_ws(parser_t* parser)
	{
		while (parser->input_end != parser->input_data)
		{
			switch (*parser->input_data)
			{
			case ' ':
			case '\t':
				++parser->char_no;
				break;
			case '\r':
				break;
			case '\n':
				++parser->line_no;
				parser->char_no = 0;
				break;
			//case '\v':
			//case '\f':
			default:
				return;
			}

			++parser->input_data;
		}
	}

	//--------------------------------------------------
	static error_t parse_null(parser_t* parser)
	{
		O_ASSERT('n' == *parser->input_data);

		if (4 > (parser->input_end - parser->input_data))
		{
			parser->msg = UNEXPECTED_EOF EXPECTED NULL;
			return err_bad_data;
		}

		if(!u::str_cmp(parser->input_data, NULL, 4))
		{
			parser->msg = UNEXPECTED CONSTANT EXPECTED NULL;
			return err_bad_data;
		}

		parser->char_no += 4;
		parser->input_data += 4;

		return err_ok;
	}

	//--------------------------------------------------
	static error_t parse_true(parser_t* parser)
	{
		O_ASSERT('t' == *parser->input_data);

		if (4 > (parser->input_end - parser->input_data))
		{
			parser->msg = UNEXPECTED_EOF EXPECTED TRUE;
			return err_bad_data;
		}

		if (!u::str_cmp(parser->input_data, TRUE, 4))
		{
			parser->msg = UNEXPECTED CONSTANT EXPECTED TRUE;
			return err_bad_data;
		}

		parser->char_no += 4;
		parser->input_data += 4;

		return err_ok;
	}

	//--------------------------------------------------
	static error_t parse_false(parser_t* parser)
	{
		O_ASSERT('f' == *parser->input_data);

		if (5 > (parser->input_end - parser->input_data))
		{
			parser->msg = UNEXPECTED_EOF EXPECTED FALSE;
			return err_bad_data;
		}

		if (!u::str_cmp(parser->input_data, FALSE, 5))
		{
			parser->msg = UNEXPECTED CONSTANT EXPECTED FALSE;
			return err_bad_data;
		}

		parser->char_no += 5;
		parser->input_data += 5;

		return err_ok;
	}

	//--------------------------------------------------
	static error_t parse_string_escape(parser_t* parser)
	{
		O_ASSERT('\\' == *parser->input_data);
		
		++parser->input_data;
		parser->char_no += 1;

		if (parser->input_end == parser->input_data)
		{
			parser->msg = UNEXPECTED_EOF EXPECTED ESCAPE_CHARACTER;
			return err_bad_data;
		}

		switch (*parser->input_data)
		{
		case '\"':
		case '\\':
		case '/':
		case 'b':
		case 'f':
		case 'n':
		case 'r':
		case 't':
			++parser->input_data;
			parser->char_no += 1;
			return err_ok;
		case 'u': //not suporting 4 hexa digits
			parser->msg = ESCAPE_CHARACTER " 'u' not supported";
			return err_not_implemented;
		default:
			parser->msg = UNEXPECTED ESCAPE_CHARACTER;
			return err_bad_data;
		}
	}

	//--------------------------------------------------
	static error_t parse_string(parser_t* parser, string_t& string)
	{
		O_ASSERT('\"' == *parser->input_data);

		error_t err;

		++parser->input_data;
		parser->char_no += 1;
		string.str = parser->input_data;

		while (true)
		{
			if (parser->input_end == parser->input_data)
			{
				parser->msg = UNEXPECTED_EOF EXPECTED "\"";
				return err_bad_data;
			}

			switch (*parser->input_data)
			{
			case '\"':
				string.len = parser->input_data - string.str;
				++parser->input_data;
				parser->char_no += 1;
				return err_ok;
			case '\\':
				err = parse_string_escape(parser);
				if (err)
					return err;
				break;
			default:
				++parser->input_data;
				parser->char_no += 1;
				break;
			}
		}
	}

	//--------------------------------------------------
	static void parse_number_sign(parser_t* parser, int64_t& sign)
	{
		switch (*parser->input_data)
		{
		case '-':
			++parser->input_data;
			++parser->char_no;
			sign = -1;
			break;
		case '+':
			++parser->input_data;
			++parser->char_no;
			sign = 1;
			break;
		default:
			sign = 1;
			break;
		}
	}

	//--------------------------------------------------
	static void parse_number_int(parser_t* parser, int64_t& num)
	{
		//uint64_t num = 0;
		while (parser->input_end != parser->input_data && *parser->input_data >= '0' && *parser->input_data <= '9')
		{
			num *= 10;
			num += *parser->input_data - '0';
			++parser->input_data;
			++parser->char_no;
		}
	}

	//--------------------------------------------------
	static error_t parse_number_exponent(parser_t* parser, int64_t& exponent)
	{
		O_ASSERT('e' == *parser->input_data || 'E' == *parser->input_data);

		char8_t const* str_old;
		int64_t exp_sce = 0;
		int64_t exp_sce_sign;

		++parser->input_data;
		++parser->char_no;

		if (parser->input_end == parser->input_data)
		{
			parser->msg = UNEXPECTED_EOF EXPECTED DIGIT;
			return err_bad_data; //unexpected eof
		}

		parse_number_sign(parser, exp_sce_sign);

		str_old = parser->input_data;
		parse_number_int(parser, exp_sce);
		if (str_old == parser->input_data) //no digits in exponent
		{
			if (parser->input_end == parser->input_data)
				parser->msg = UNEXPECTED_EOF EXPECTED DIGIT;
			else
				parser->msg = UNEXPECTED CHARACTER EXPECTED DIGIT;

			return err_bad_data;
		}

		exponent += exp_sce_sign * exp_sce;
		return err_ok;
	}

	//--------------------------------------------------
	static error_t parse_number(parser_t* parser, number_t& number)
	{
		char8_t const* str_old;

		//parse mantissa
		int64_t sign;
		{
			parse_number_sign(parser, sign);

			number.mantissa = 0;
			str_old = parser->input_data;
			parse_number_int(parser, number.mantissa);
			if (str_old == parser->input_data) //no digits
			{
				if (parser->input_end == parser->input_data)
					parser->msg = UNEXPECTED_EOF EXPECTED DIGIT;
				else
					parser->msg = UNEXPECTED CHARACTER EXPECTED DIGIT;

				return err_bad_data;
			}
		}

		if (parser->input_end == parser->input_data)
		{
			number.mantissa *= sign;
			number.exponent = 0;
			return err_ok;
		}

		switch (*parser->input_data)
		{
		case '.':
			++parser->input_data;
			++parser->char_no;
			str_old = parser->input_data;
			parse_number_int(parser, number.mantissa);
			number.mantissa *= sign;
			number.exponent = str_old - parser->input_data;
			break;
		case 'e':
		case 'E':
		{
			number.mantissa *= sign;
			number.exponent = 0;
			return parse_number_exponent(parser, number.exponent);
		}
		default:
			number.mantissa *= sign;
			number.exponent = 0;
			return err_ok;
		}

		if (parser->input_end == parser->input_data) //after dot we can exit
		{
			return err_ok;
		}

		switch (*parser->input_data)
		{
		case 'e':
		case 'E':
		{
			return parse_number_exponent(parser, number.exponent);
		}
		default:
			return err_ok;
		}
	}

	//--------------------------------------------------
	static error_t parse_object(parser_t* parser, object_t& object)
	{
		error_t err;

		object.member_array = nullptr;
		object.member_size = 0;

		O_ASSERT('{' == *parser->input_data);
		++parser->input_data;
		parser->char_no += 1;

		parse_ws(parser);

		//check for empty array
		{
			if (parser->input_end == parser->input_data)
			{
				parser->msg = UNEXPECTED_EOF EXPECTED VALUE "or }";
				return err_bad_data;
			}

			if ('}' == *parser->input_data)
			{
				++parser->input_data;
				parser->char_no += 1;

				

				return err_ok;
			}
		}

		member_t*	ctx_array = nullptr;
		size_t		ctx_size = 0;
		size_t		ctx_capacity = 256;
		ctx_array = (member_t*)parse_alloc(parser, sizeof(member_t) * ctx_capacity);
		if (nullptr == ctx_array)
		{
			parser->msg = ALLOCATION_FAILED;
			return err_out_of_memory;
		}

		while (true)
		{
			//parse required name
			{
				if (parser->input_end == parser->input_data)
				{
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					parser->msg = UNEXPECTED_EOF EXPECTED STRING;
					return err_bad_data;
				}

				err = parse_string(parser, ctx_array[ctx_size].name);
				if (err)
				{
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					return err;
				}
			}

			parse_ws(parser);

			//parse required pairing
			{
				if (parser->input_end == parser->input_data)
				{
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					parser->msg = UNEXPECTED_EOF EXPECTED ":";
					return err_bad_data;
				}

				switch (*parser->input_data)
				{
				case ':':
					++parser->input_data;
					parser->char_no += 1;
					break;
				default:
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					parser->msg = UNEXPECTED CHARACTER EXPECTED ":";
					return err_bad_data;
				}
			}

			parse_ws(parser);

			//parse required value
			{
				if (parser->input_end == parser->input_data)
				{
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					parser->msg = UNEXPECTED_EOF EXPECTED VALUE;
					return err_bad_data;
				}

				if (ctx_capacity == ctx_size)
				{
					size_t increment = 256;
					void* mem = parse_realloc(parser, ctx_array, sizeof(member_t) * ctx_capacity, sizeof(member_t) * (ctx_capacity + increment));
					if (nullptr == mem)
					{
						parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
						parser->msg = ALLOCATION_FAILED;
						return err_out_of_memory;
					}
					ctx_capacity += increment;
					ctx_array = (member_t*)mem;
				}

				err = parse_value(parser, ctx_array[ctx_size].value);
				if (err)
				{
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					return err;
				}

				++ctx_size;
			}

			parse_ws(parser);

			//parse required next or end
			{
				if (parser->input_end == parser->input_data)
				{
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					parser->msg = UNEXPECTED_EOF EXPECTED ", or }";
					return err_bad_data;
				}

				switch (*parser->input_data)
				{
				case '}':
					++parser->input_data;
					parser->char_no += 1;

					object.member_size = ctx_size;
					object.member_array = (member_t*)document_alloc(parser, sizeof(member_t) * ctx_size);
					if (nullptr == object.member_array)
					{
						parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
						parser->msg = ALLOCATION_FAILED;
						return err_out_of_memory;
					}

					u::mem_copy(object.member_array, ctx_array, sizeof(member_t) * ctx_size);
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);

					return err_ok;
				case ',':
					++parser->input_data;
					parser->char_no += 1;

					break;
				default:
					parse_free(parser, ctx_array, sizeof(member_t) * ctx_capacity);
					parser->msg = UNEXPECTED CHARACTER EXPECTED ", or }";
					return err_bad_data;
				}
			}

			parse_ws(parser);
		}
	}

	//--------------------------------------------------
	static error_t parse_array(parser_t* parser, array_t& array)
	{
		error_t err;

		array.element_array = nullptr;
		array.element_size = 0;

		O_ASSERT('[' == *parser->input_data);
		++parser->input_data;
		parser->char_no += 1;

		parse_ws(parser);

		//check for empty array
		{
			if (parser->input_end == parser->input_data)
			{
				parser->msg = UNEXPECTED_EOF EXPECTED VALUE "or ]";
				return err_bad_data;
			}

			if (']' == *parser->input_data)
			{
				++parser->input_data;
				parser->char_no += 1;

				

				return err_ok;
			}
		}

		value_t*	ctx_array = nullptr;
		size_t		ctx_size = 0;
		size_t		ctx_capacity = 256;
		ctx_array = (value_t*)parse_alloc(parser, sizeof(value_t) * ctx_capacity);
		if (nullptr == ctx_array)
		{
			parser->msg = ALLOCATION_FAILED;
			return err_out_of_memory;
		}

		while (true)
		{
			//parse required value
			{
				if (parser->input_end == parser->input_data)
				{
					parse_free(parser, ctx_array, sizeof(value_t) * ctx_capacity);
					parser->msg = UNEXPECTED_EOF EXPECTED VALUE;
					return err_bad_data;
				}

				if (ctx_capacity == ctx_size)
				{
					size_t increment = 256;
					void* mem = parse_realloc(parser, ctx_array, sizeof(value_t) * ctx_capacity, sizeof(value_t) * (ctx_capacity + increment));
					if (nullptr == mem)
					{
						parse_free(parser, ctx_array, sizeof(value_t) * ctx_capacity);
						parser->msg = ALLOCATION_FAILED;
						return err_out_of_memory;
					}
					ctx_capacity += increment;
					ctx_array = (value_t*)mem;
				}

				err = parse_value(parser, ctx_array[ctx_size]);
				if (err)
				{
					parse_free(parser, ctx_array, sizeof(value_t) * ctx_capacity);
					return err;
				}

				++ctx_size;
			}

			parse_ws(parser);

			//parse required next or end
			{
				if (parser->input_end == parser->input_data)
				{
					parse_free(parser, ctx_array, sizeof(value_t) * ctx_capacity);
					parser->msg = UNEXPECTED_EOF EXPECTED ", or ]";
					return err_bad_data;
				}

				switch (*parser->input_data)
				{
				case ']':
					++parser->input_data;
					parser->char_no += 1;
			
					array.element_size = ctx_size;
					array.element_array = (value_t*)document_alloc(parser, sizeof(value_t) * ctx_size);
					if (nullptr == array.element_array)
					{
						parse_free(parser, ctx_array, sizeof(value_t) * ctx_capacity);
						parser->msg = ALLOCATION_FAILED;
						return err_out_of_memory;
					}

					u::mem_copy(array.element_array, ctx_array, sizeof(value_t) * ctx_size);
					parse_free(parser, ctx_array, sizeof(value_t) * ctx_capacity);

					return err_ok;
				case ',':
					++parser->input_data;
					parser->char_no += 1;

					break;
				default:
					parse_free(parser, ctx_array, sizeof(value_t) * ctx_capacity);
					parser->msg = UNEXPECTED CHARACTER EXPECTED ", or ]";
					return err_bad_data;
				}
			}

			parse_ws(parser);
		}
	}

	//--------------------------------------------------
	static error_t	parse_value(parser_t* parser, value_t& value)
	{
		switch (*parser->input_data)
		{
		case 'n':
			value.type = type_null;
			return parse_null(parser);
		case 't':
			value.type = type_true;
			return parse_true(parser);
		case 'f':
			value.type = type_false;
			return parse_false(parser);
		case '\"':
			value.type = type_string;
			return parse_string(parser, value.string);
		case '-':
		case '+':
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case '.':
			value.type = type_number;
			return parse_number(parser, value.number);
		case '{':
			value.type = type_object;
			return parse_object(parser, value.object);
		case '[':
			value.type = type_array;
			return parse_array(parser, value.array);
		default:
			parser->msg = UNEXPECTED CHARACTER EXPECTED VALUE;
			return err_bad_data;
		}
	}

	//--------------------------------------------------
	error_t parse_document(document_t& document, void const* data, size_t size, size_t* size_parsed, allocator_t* document_allocator, allocator_t* parse_allocator)
	{
		parser_t parser;
		parser.parse_allocator = parse_allocator;
		parser.document_allocator = document_allocator;
		parser.char_no = 0;
		parser.line_no = 0;
		parser.input_data = (char8_t const*)data;
		parser.input_size = size;
		parser.input_end = parser.input_data + size;
		parser.msg = nullptr;

		document.value.type = type_null;

		parse_ws(&parser);

		if (parser.input_end == parser.input_data)
		{
			parser.msg = UNEXPECTED_EOF EXPECTED VALUE;
			return err_bad_data;
		}

		error_t err = parse_value(&parser, document.value);
		document.line_no = parser.line_no;
		document.char_no = parser.char_no;
		document.msg = parser.msg;
		if (size_parsed)
			*size_parsed = parser.input_data - (char8_t const*)data;

		return err;
	}

	//--------------------------------------------------
	static void	free_object(document_t* document, object_t& object)
	{
		for (size_t i = 0; i < object.member_size; ++i)
		{
			free_value(document, object.member_array[i].value);
		}
		document_free(document, object.member_array, sizeof(member_t) * object.member_size);
	}

	//--------------------------------------------------
	static void	free_array(document_t* document, array_t& array)
	{
		for (size_t i = 0; i < array.element_size; ++i)
		{
			free_value(document, array.element_array[i]);
		}
		document_free(document, array.element_array, sizeof(value_t) * array.element_size);
	}

	//--------------------------------------------------
	static void	free_value(document_t* document, value_t& value)
	{
		switch (value.type)
		{
		case type_array:
			free_array(document, value.array);
			break;
		case type_object:
			free_object(document, value.object);
			break;
		default:
			break;
		}
	}

	//--------------------------------------------------
	void	free_document(document_t& document)
	{
		free_value(&document, document.value);
	}

	//--------------------------------------------------
	double	to_double(number_t const& number)
	{
		double result = (double)number.mantissa;
		double exp_result = 1;
		double base = 10;

		if (0 == number.exponent)
		{
			return result;
		}
		else if (0 < number.exponent)
		{
			auto exp = number.exponent;
			while (exp)
			{
				if (1 & exp)
					exp_result *= base;

				exp >>= 1; // divide by 2
				base *= base;
			}

			return result * exp_result;
		}
		else
		{
			auto uexp = -number.exponent;
			while (uexp)
			{
				if (1 & uexp)
					exp_result *= base;

				uexp >>= 1; // divide by 2
				base *= base;
			}

			return result / exp_result;
		}
	}

	//--------------------------------------------------
	float	to_float(number_t const& number)
	{
		return (float)to_double(number);
	}

} }
