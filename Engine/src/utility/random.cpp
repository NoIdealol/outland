#include "pch.h"
#include "random.h"

namespace outland
{
	uint32_t rand(uint64_t& ctx)
	{
		enum : uint64_t
		{
			_mul = 25214903917_u64,
			_inc = 11_u64,
			_mod = (1_u64 << 48) - 1,

			_res = 15_u64,
		};

		ctx = (ctx * _mul + _inc) & _mod;
		return uint32_t(ctx >> _res);
	}
}
