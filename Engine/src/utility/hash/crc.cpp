#include "pch.h"
#include "crc.h"

namespace outland { namespace hash
{
	namespace g
	{
		enum : uint8_t { POLY8 = 0xd5 }; // x8 + x7 + x6 + x4 + x2 + 1
		
		//(Ethernet, ZIP, etc.) polynomial in reversed bit order.
		enum : uint32_t { POLY32 = 0xedb88320 };
	}

	//////////////////////////////////////////////////
	// EVIL POPULATE MACROS
	//////////////////////////////////////////////////

#define A(function, counter) B(function, counter) B(function, counter + 128)
#define B(function, counter) C(function, counter) C(function, counter +  64)
#define C(function, counter) D(function, counter) D(function, counter +  32)
#define D(function, counter) E(function, counter) E(function, counter +  16)
#define E(function, counter) F(function, counter) F(function, counter +   8)
#define F(function, counter) G(function, counter) G(function, counter +   4)
#define G(function, counter) H(function, counter) H(function, counter +   2)
#define H(function, counter) I(function, counter) I(function, counter +   1)
#define I(function, counter) function(counter) ,
#define POPULATE_TABLE(function) A(function, 0)

	//////////////////////////////////////////////////
	// CRC8 - INTERNALS
	//////////////////////////////////////////////////

	//--------------------------------------------------
	template<size_t j>
	static constexpr uint8_t initCrc8Loop(uint8_t temp)
	{
		return initCrc8Loop<j + 1>((((temp & 0x80) != 0) ? ((temp << 1) ^ g::POLY8) : (temp << 1)));
	}

	//--------------------------------------------------
	template<>
	static constexpr uint8_t initCrc8Loop<8>(uint8_t temp)
	{
		return temp;
	}

	//--------------------------------------------------
	constexpr static const uint8_t initCrc8(uint8_t i)
	{
		return initCrc8Loop<0>(i);
	}

	namespace g
	{
		//--------------------------------------------------
		constexpr uint8_t table8[256] = { POPULATE_TABLE(initCrc8) };
	}

	//////////////////////////////////////////////////
	// CRC32 - INTERNALS
	//////////////////////////////////////////////////

	//--------------------------------------------------
	template<size_t j>
	constexpr uint32_t initCrc32Loop(uint32_t temp)
	{
		return initCrc32Loop<j + 1>((((temp & 1) != 0) ? ((temp >> 1) ^ g::POLY32) : (temp >> 1)));
	}

	//--------------------------------------------------
	template<>
	constexpr uint32_t initCrc32Loop<8>(uint32_t temp)
	{
		return temp;
	}

	//--------------------------------------------------
	constexpr uint32_t initCrc32(uint32_t i)
	{
		return initCrc32Loop<0>(i);
	}

	namespace g
	{
		//--------------------------------------------------
		constexpr uint32_t table32[256] = { POPULATE_TABLE(initCrc32) };
	}

	//--------------------------------------------------
	uint8_t crc8(char8_t const* string, uint8_t seed)
	{
		byte_t const* bytes = reinterpret_cast<byte_t const*>(string);
		uint8_t crc = seed;
		while (*bytes)
			crc = g::table8[crc ^ *bytes++];
		return crc;
	}

	//--------------------------------------------------
	uint8_t crc8(void const* data, size_t size, uint8_t seed)
	{
		byte_t const* bytes = reinterpret_cast<byte_t const*>(data);
		uint8_t crc = seed;
		while (--size)
			crc = g::table8[crc ^ *bytes++];
		return crc;
	}

	//--------------------------------------------------
	uint32_t crc32(char8_t const* string, uint32_t seed)
	{
		byte_t const* bytes = reinterpret_cast<byte_t const*>(string);
		uint32_t crc = ~seed;
		while (*bytes)
			crc = (crc >> 8) ^ g::table32[(crc ^ *bytes++) & 0xFF];
		return ~crc;
	}

	//--------------------------------------------------
	uint32_t crc32(void const* data, size_t size, uint32_t seed)
	{
		byte_t const* bytes = reinterpret_cast<byte_t const*>(data);
		uint32_t crc = ~seed;
		while (--size)
			crc = (crc >> 8) ^ g::table32[(crc ^ *bytes++) & 0xFF];
		return ~crc;
	}

} }
