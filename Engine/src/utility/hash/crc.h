#pragma once

namespace outland { namespace hash
{

	uint8_t crc8(char8_t const* string, uint8_t seed = 0);
	uint8_t crc8(void const* data, size_t size, uint8_t seed = 0);

	uint32_t crc32(char8_t const* string, uint32_t seed);
	uint32_t crc32(void const* data, size_t size, uint32_t seed);

	O_INLINE uint32_t crc32(char8_t const* string)					{ return crc32(string, 0); }
	O_INLINE uint32_t crc32(void const* data, size_t size)			{ return crc32(data, size, 0); }

} }
