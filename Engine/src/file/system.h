#pragma once
#include "common.h"

namespace outland { namespace file 
{
	struct file_storage_t : file_t
	{
		uintptr_t			reserved[4];
	};

	//////////////////////////////////////////////////
	// CMD
	//////////////////////////////////////////////////

	enum cmd_type_t
	{
		cmd_type_open,
		cmd_type_remove,
		cmd_type_close,
		cmd_type_get_size,
		cmd_type_set_size,
		cmd_type_read,
		cmd_type_write,
	};

	struct cmd_open_t
	{
		char8_t const*	path;
		open_t			open;
		access_mask_t	access;
		void*			user_ptr;
	};

	struct cmd_remove_t
	{
		char8_t const*	path;
		void*			user_ptr;
	};

	struct cmd_close_t
	{
		file_id			file;
	};

	struct cmd_get_size_t
	{
		file_id			file;
		void*			user_ptr;
	};

	struct cmd_set_size_t
	{
		file_id			file;
		file_size_t		size;
		void*			user_ptr;
	};

	struct cmd_data_t
	{
		request_id		request;
	};

	struct command_t
	{
		cmd_type_t			type;
		union
		{
			cmd_open_t		open;
			cmd_remove_t	remove;
			cmd_close_t		close;
			cmd_get_size_t	get_size;
			cmd_set_size_t	set_size;
			cmd_data_t		data;
		};
	};

	//////////////////////////////////////////////////
	// SYSTEM
	//////////////////////////////////////////////////

	class system_t
	{
		enum : size_t
		{
			_max_queue_size = 32,
			_max_device_size = 32,
			_max_file_size = 64,
		};

		struct device_info_t
		{
			device_t*	device;
		};

		queue_t*		_queue_array[_max_queue_size];
		size_t			_queue_size;
		device_info_t	_device_array[_max_device_size];
		size_t			_device_size;

		file_storage_t	_file_array[_max_file_size];
		file_t*			_file_pool[_max_file_size];
		size_t			_file_size;

	private:
		error_t			_alloc_file(file_id& file);
		void			_free_file(file_id file);
		error_t			_find_device(device_t*& device, char8_t const* path);
		void			_execute_command(command_t const* cmd, result_t* res);
		void			_write_io_result(queue_id queue, result_t* res);
		bool			_is_io_result_ready(queue_id queue);
		void			_insert_io_result(request_id request);
		void			_update_queue(queue_id queue);

	public:
		system_t(device_t** device_array, size_t device_size);
		~system_t();
		void	update();
		error_t	queue_insert(queue_id queue);
		void	queue_remove(queue_id queue);
	};


} }
