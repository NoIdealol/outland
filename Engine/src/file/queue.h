#pragma once
#include "system\thread\atomic.h"
#include "system.h"

namespace outland { namespace file 
{

	struct queue_cache_t
	{
		uint32_t	cached;
		uint32_t	submited;
		uint32_t	finished;

		uint32_t	consumable;
		uint32_t	consuming;
		uint32_t	consumed;
	};

	struct queue_t
	{
		queue_cache_t*			command_cache;			//cmd
		os::atomic_uint32_id	command_produced;		//cmd write, res read 
		os::atomic_uint32_id	command_consumed;		//cmd read, res write
		command_t*				command_array;			//cmd write, res read
		queue_cache_t*			result_cache;			//res
		os::atomic_uint32_id	result_produced;		//res write, cmd read
		os::atomic_uint32_id	result_consumed;		//res read, cmd write
		result_t*				result_array;			//res write, cmd read
		uint32_t				capacity;				//const
		request_list_t*			result_queue;			//res
	};

	namespace queue
	{
		enum memory_layout_t : size_t
		{
			memory_layout_queue,

			memory_layout_command_cache,

			memory_layout_result_cache,
			memory_layout_result_queue,

			memory_layout_command_produced,
			memory_layout_command_array,
			memory_layout_result_consumed,

			memory_layout_result_produced,
			memory_layout_result_array,
			memory_layout_command_consumed,

			memory_layout_size,
		};

		enum memory_cache_t : size_t
		{
			memory_cache_0 = memory_layout_command_cache,
			memory_cache_1 = memory_layout_result_cache,
			memory_cache_2 = memory_layout_command_produced,
			memory_cache_3 = memory_layout_result_produced,
		};

		void memory_offsets(create_t const& create, memory_info_t& info, size_t(&offset_arr)[memory_layout_size]);

		bool cmd_can_produce(queue_t* queue);
		void cmd_produce(queue_t* queue, command_t*& cmd);
		void cmd_flush_producer(queue_t* queue);
		void cmd_evict_producer(queue_t* queue);
		void cmd_evict_consumer(queue_t* queue);
		void cmd_flush_consumer(queue_t* queue);
		bool cmd_consume(queue_t* queue, command_t const*& cmd);
		bool res_can_produce(queue_t* queue);
		void res_produce(queue_t* queue, result_t*& res);
		void res_flush_producer(queue_t* queue);
		void res_evict_producer(queue_t* queue);
		void res_evict_consumer(queue_t* queue);
		void res_flush_consumer(queue_t* queue);
		bool res_consume(queue_t* queue, result_t const*& res);
		request_list_t* res_request_list(queue_t* queue);
	}

} }
