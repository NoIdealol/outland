#pragma once
#include "..\lib\file_system.h"
#include "..\lib\file_device.h"

namespace outland
{
	struct file_t
	{
		file::device_t*		device;
	};


}
namespace outland { namespace file 
{

	struct request_t
	{
		file_id		file;
		file_size_t offset;
		void*		buffer;
		size_t		size;
		void*		user_ptr;

		error_t		error;
		queue_id	queue;
		request_t*	next;
	};

	//////////////////////////////////////////////////
	// REQUEST LIST
	//////////////////////////////////////////////////

	class request_list_t
	{
		request_t*	_begin;
		request_t*	_end;
	public:
		request_list_t();

		void		move_back(request_list_t& resource_list);
		void		push_back(request_t* resource);
		request_t*	pop_front();
		request_t*	begin();
		request_t*	end();
		void		clear();
	};

} }
