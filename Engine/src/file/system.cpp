#include "pch.h"
#include "system.h"
#include "queue.h"

#include "device_disk.h" //todo remove this

namespace outland { namespace file 
{
	//////////////////////////////////////////////////
	// REQUEST LIST
	//////////////////////////////////////////////////

	//--------------------------------------------------
	request_list_t::request_list_t()
		: _begin(nullptr)
		, _end(nullptr)
	{

	}

	//--------------------------------------------------
	void	request_list_t::move_back(request_list_t& resource_list)
	{
		if (nullptr == resource_list._begin)
			return;

		if (nullptr == _begin)
		{
			_begin = resource_list._begin;
			_end = resource_list._end;
			resource_list._begin = nullptr;
			resource_list._end = nullptr;
			return;
		}

		_end->next = resource_list._begin;
		_end = resource_list._end;
		resource_list._begin = nullptr;
		resource_list._end = nullptr;
	}

	//--------------------------------------------------
	void	request_list_t::push_back(request_t* resource)
	{
		if (_begin)
		{
			_end->next = resource;
			_end = resource;
			resource->next = nullptr;
		}
		else
		{
			_begin = resource;
			_end = resource;
			resource->next = nullptr;
		}
	}

	//--------------------------------------------------
	request_t*	request_list_t::pop_front()
	{
		if (_begin)
		{
			request_t* ret = _begin;
			_begin = _begin->next;
			if (ret == _end)
				_end = nullptr;

			return ret;
		}
		else
		{
			return nullptr;
		}
	}

	//--------------------------------------------------
	request_t*	request_list_t::begin()
	{
		return _begin;
	}

	//--------------------------------------------------
	request_t*	request_list_t::end()
	{
		return _end;
	}

	//--------------------------------------------------
	void			request_list_t::clear()
	{
		_begin = nullptr;
		_end = nullptr;
	}

	//////////////////////////////////////////////////
	// REQUEST
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void	request::memory_info(memory_info_t& info)
	{
		info.size = sizeof(request_t);
		info.align = alignof(request_t);
	}

	//--------------------------------------------------
	void	request::create(request_id& request, void* memory)
	{
		request = O_CREATE(request_t, memory);
	}

	//--------------------------------------------------
	void	request::destroy(request_id request)
	{
		O_DESTROY(request_t, request);
	}

	//////////////////////////////////////////////////
	// CMD
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void	epoll(queue_id queue, result_t* result_array, size_t result_capacity, size_t& result_size)
	{
		result_size = 0;
		queue::res_evict_consumer(queue);
		result_t const* res;
		while (result_capacity > result_size && queue::res_consume(queue, res))
		{
			u::mem_copy(result_array + result_size, res, sizeof(result_t));
			++result_size;
		}
		queue::res_flush_consumer(queue);
	}

	//--------------------------------------------------
	void	flush(queue_id queue)
	{
		queue::cmd_flush_producer(queue);
		queue::cmd_evict_producer(queue);
	}

	//--------------------------------------------------
	error_t open(queue_id queue, char8_t const* path, open_t open, access_mask_t access, void* user_ptr)
	{
		if (!queue::cmd_can_produce(queue))
			return err_too_many_requests;

		command_t* cmd;
		queue::cmd_produce(queue, cmd);
		cmd->type = cmd_type_open;
		cmd->open.path = path;
		cmd->open.open = open;
		cmd->open.access = access;
		cmd->open.user_ptr = user_ptr;

		return err_ok;
	}

	//--------------------------------------------------
	error_t remove(queue_id queue, char8_t const* path, void* user_ptr)
	{
		if (!queue::cmd_can_produce(queue))
			return err_too_many_requests;

		command_t* cmd;
		queue::cmd_produce(queue, cmd);
		cmd->type = cmd_type_remove;
		cmd->remove.path = path;
		cmd->remove.user_ptr = user_ptr;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	close(queue_id queue, file_id file)
	{
		if (!queue::cmd_can_produce(queue))
			return err_too_many_requests;

		command_t* cmd;
		queue::cmd_produce(queue, cmd);
		cmd->type = cmd_type_close;
		cmd->close.file = file;

		return err_ok;
	}

	//--------------------------------------------------
	error_t get_size(queue_id queue, file_id file, void* user_ptr)
	{
		if (!queue::cmd_can_produce(queue))
			return err_too_many_requests;

		command_t* cmd;
		queue::cmd_produce(queue, cmd);
		cmd->type = cmd_type_get_size;
		cmd->get_size.file = file;
		cmd->get_size.user_ptr = user_ptr;

		return err_ok;
	}

	//--------------------------------------------------
	error_t set_size(queue_id queue, file_id file, file_size_t size, void* user_ptr)
	{
		if (!queue::cmd_can_produce(queue))
			return err_too_many_requests;

		command_t* cmd;
		queue::cmd_produce(queue, cmd);
		cmd->type = cmd_type_set_size;
		cmd->set_size.file = file;
		cmd->set_size.size = size;
		cmd->set_size.user_ptr = user_ptr;

		return err_ok;
	}

	//--------------------------------------------------
	error_t read(queue_id queue, file_id file, request_id request, file_size_t offset, void* buffer, size_t size, void* user_ptr)
	{
		if (!queue::cmd_can_produce(queue))
			return err_too_many_requests;

		request->file = file;
		request->offset = offset;
		request->buffer = buffer;
		request->size = size;
		request->user_ptr = user_ptr;
		request->error = err_ok;
		request->queue = queue;
		request->next = nullptr;

		command_t* cmd;
		queue::cmd_produce(queue, cmd);
		cmd->type = cmd_type_read;
		cmd->data.request = request;


		return err_ok;
	}

	//--------------------------------------------------
	error_t write(queue_id queue, file_id file, request_id request, file_size_t offset, void* buffer, size_t size, void* user_ptr)
	{
		if (!queue::cmd_can_produce(queue))
			return err_too_many_requests;

		request->file = file;
		request->offset = offset;
		request->buffer = buffer;
		request->size = size;
		request->user_ptr = user_ptr;
		request->error = err_ok;
		request->queue = queue;
		request->next = nullptr;

		command_t* cmd;
		queue::cmd_produce(queue, cmd);
		cmd->type = cmd_type_write;
		cmd->data.request = request;

		return err_ok;
	}

	//////////////////////////////////////////////////
	// SYSTEM
	//////////////////////////////////////////////////

	//--------------------------------------------------
	error_t	system_t::_alloc_file(file_id& file)
	{
		if (0 == _file_size)
			return err_out_of_handles;

		file = _file_pool[--_file_size];
		u::mem_clr(file, sizeof(file_t));

		return err_ok;
	}

	//--------------------------------------------------
	void	system_t::_free_file(file_id file)
	{
		_file_pool[_file_size] = file;
		++_file_size;
	}

	//--------------------------------------------------
	error_t	system_t::_find_device(device_t*& device, char8_t const* path)
	{
		if (_device_size)
		{
			device =  _device_array[0].device;
			return err_ok;
		}

		return err_not_found;
	}
	
	//--------------------------------------------------
	void system_t::_execute_command(command_t const* cmd, result_t* res)
	{
		error_t err;
		switch (cmd->type)
		{
		case cmd_type_open:
		{
			device_t* device = nullptr;
			file_id file = nullptr; //todo alloc file
			err = _find_device(device, cmd->open.path);
			if (!err)
			{
				err = _alloc_file(file);
				if (!err)
				{
					err = device->open(file, cmd->open.path, cmd->open.open, cmd->open.access);
					if (!err)
					{
						file->device = device;
					}
					else
					{
						_free_file(file);
						file = nullptr;
					}
				}
			}
			
			res->error = err;
			res->type = result_type_open;
			res->user_ptr = cmd->open.user_ptr;
			res->file = file;

			break;
		}
		case cmd_type_remove:
		{
			device_t* device = nullptr;
			err = _find_device(device, cmd->remove.path);
			if (!err)
			{
				err = device->remove(cmd->remove.path);
			}

			res->error = err;
			res->type = result_type_remove;
			res->user_ptr = cmd->remove.user_ptr;
			res->path = cmd->remove.path;

			break;
		}
		case cmd_type_close:
		{
			file_id file = cmd->close.file;
			file->device->close(file);
			_free_file(file);

			O_ASSERT(nullptr == res);
			break;
		}
		case cmd_type_get_size:
		{
			file_id file = cmd->get_size.file;
			err = file->device->get_size(file, res->size);

			res->error = err;
			res->type = result_type_get_size;
			res->user_ptr = cmd->get_size.user_ptr;

			break;
		}
		case cmd_type_set_size:
		{
			file_id file = cmd->set_size.file;
			err = file->device->set_size(file, cmd->set_size.size);

			res->error = err;
			res->type = result_type_set_size;
			res->user_ptr = cmd->set_size.user_ptr;

			break;
		}
		case cmd_type_read:
		{
			request_id request = cmd->data.request;
			file_id file = request->file;
			file->device->read(request);

			O_ASSERT(nullptr == res);
			break;
		}
		case cmd_type_write:
		{
			request_id request = cmd->data.request;
			file_id file = request->file;
			file->device->write(request);

			O_ASSERT(nullptr == res);
			break;
		}
		default:
			O_ASSERT(false);
			break;
		}
	}

	//--------------------------------------------------
	void system_t::_write_io_result(queue_id queue, result_t* res)
	{
		auto* list = queue::res_request_list(queue);
		auto* request = list->pop_front();
		res->error = request->error;
		res->type = result_type_request;
		res->user_ptr = request->user_ptr;
		res->request = request;
	}

	//--------------------------------------------------
	bool system_t::_is_io_result_ready(queue_id queue)
	{
		auto* list = queue::res_request_list(queue);
		return nullptr != list->begin();
	}

	//--------------------------------------------------
	void system_t::_insert_io_result(request_id request)
	{
		auto* list = queue::res_request_list(request->queue);
		list->push_back(request);
	}

	//--------------------------------------------------
	void system_t::_update_queue(queue_id queue)
	{
		result_t* res;
		command_t const* cmd;

		queue::res_evict_producer(queue);

		//fill queue with ready io results
		while (_is_io_result_ready(queue) && queue::res_can_produce(queue))
		{
			queue::res_produce(queue, res);
			_write_io_result(queue, res);
		}

		//process new commands
		if (!queue::res_can_produce(queue))
		{
			queue::res_flush_producer(queue);
			return; //cannot store consume result
		}

		queue::cmd_evict_consumer(queue);

		while (queue::res_can_produce(queue) && queue::cmd_consume(queue, cmd))
		{	
			if (cmd->type == cmd_type_read || cmd->type == cmd_type_write || cmd->type == cmd_type_close)
				res = nullptr;
			else
				queue::res_produce(queue, res);

			_execute_command(cmd, res);
		}

		queue::cmd_flush_consumer(queue);
		queue::res_flush_producer(queue);
	}

	//--------------------------------------------------
	system_t::system_t(device_t** device_array, size_t device_size)
		: _queue_size(0)
		, _device_size(device_size)
	{
		for (size_t i = 0; i < _max_file_size; ++i)
		{
			_file_pool[i] = _file_array + i;
		}
		_file_size = _max_file_size;

		for (size_t i = 0; i < device_size; ++i)
		{
			_device_array[i].device = device_array[i];
		}
	}

	//--------------------------------------------------
	system_t::~system_t()
	{
	}

	//--------------------------------------------------
	void system_t::update()
	{
		request_id	request_array[32];
		size_t		request_size;

		for (size_t i = 0; i < _device_size; ++i)
		{
			_device_array[i].device->epoll(request_array, u::array_size(request_array), request_size);
			for (size_t j = 0; j < request_size; ++j)
			{
				_insert_io_result(request_array[j]);
			}
		}

		for (size_t i = 0; i < _queue_size; ++i)
		{
			_update_queue(_queue_array[i]);
		}
	}

	//--------------------------------------------------
	error_t	system_t::queue_insert(queue_id queue)
	{
		if (_max_queue_size == _queue_size)
			return err_out_of_memory;

		_queue_array[_queue_size] = queue;
		++_queue_size;

		return err_ok;
	}

	//--------------------------------------------------
	void	system_t::queue_remove(queue_id queue)
	{
		for (size_t i = 0; i < _queue_size; ++i)
		{
			if (_queue_array[i] == queue)
			{
				--_queue_size;
				u::array_move(_queue_array + i, _queue_array + i + 1, _queue_size - i);
				return;
			}
		}
	}

	//--------------------------------------------------
	void	system::memory_info(memory_info_t& info)
	{
		size_t align_arr[] = { alignof(system_t), alignof(device_disk_t) };
		size_t size_arr[] = { sizeof(system_t), sizeof(device_disk_t) };
		size_t offset_arr[2];
		u::mem_offsets(align_arr, size_arr, offset_arr, info.align, info.size, 2);
	}

	//--------------------------------------------------
	error_t system::create(system_id& system, create_t const& create, void* memory)
	{
		error_t err;

		memory_info_t info;
		size_t align_arr[] = { alignof(system_t), alignof(device_disk_t) };
		size_t size_arr[] = { sizeof(system_t), sizeof(device_disk_t) };
		size_t offset_arr[2];
		u::mem_offsets(align_arr, size_arr, offset_arr, info.align, info.size, 2);

		auto* disk = O_CREATE(device_disk_t, (byte_t*)(memory) + offset_arr[1]);
		err = disk->init();
		if (err)
		{
			O_DESTROY(device_disk_t, disk);
			return err;
		}

		device_t* dev_arr[] = { disk };

		system = O_CREATE(system_t, memory)(dev_arr, u::array_size(dev_arr));
		return err_ok;
	}

	//--------------------------------------------------
	void system::destroy(system_id system)
	{
		memory_info_t info;
		size_t align_arr[] = { alignof(system_t), alignof(device_disk_t) };
		size_t size_arr[] = { sizeof(system_t), sizeof(device_disk_t) };
		size_t offset_arr[2];
		u::mem_offsets(align_arr, size_arr, offset_arr, info.align, info.size, 2);

		O_DESTROY(system_t, system);
		auto* disk = (device_disk_t*)((byte_t*)(system)+offset_arr[1]);
		disk->clean();
		O_DESTROY(device_disk_t, disk);
	}

	//--------------------------------------------------
	void	system::update(system_id system)
	{
		system->update();
	}

	//--------------------------------------------------
	error_t system::queue_insert(system_id system, queue_id queue)
	{
		return system->queue_insert(queue);
	}

	//--------------------------------------------------
	void	system::queue_remove(system_id system, queue_id queue)
	{
		system->queue_remove(queue);
	}

} }
