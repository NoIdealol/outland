#pragma once
#include "common.h"
#include "system\file\file.h"

namespace outland { namespace file 
{
	struct file_disk_t : file_t
	{
		os::file_id		file;
	};

	class device_disk_t : public device_t
	{
		os::file::queue_id		_queue;
		os::file::operation_id	_operation_pool[32];
		size_t					_operation_size;
		request_list_t			_read_queue;
		request_list_t			_write_queue;

	private:
		static file_disk_t*			_cast(file_t* file);
		static os::file::access_t	_to_os_access(access_mask_t access);
		static os::file::open_t		_to_os_open(open_t open);

	public:
		device_disk_t();
		~device_disk_t();

		error_t	init();
		void	clean();

		error_t	open(file_id file, char8_t const* path, open_t open, access_mask_t access) final;
		error_t	remove(char8_t const* path) final;
		void	close(file_id file) final;
		error_t	get_size(file_id file, file_size_t& size) final;
		error_t	set_size(file_id file, file_size_t size) final;
		void	read(request_id request) final;
		void	write(request_id request) final;
		void	epoll(request_id* request_array, size_t request_capacity, size_t& request_size) final;
	};

} }
