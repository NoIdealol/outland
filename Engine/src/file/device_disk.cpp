#include "pch.h"
#include "device_disk.h"
#include "math\integer.h"

namespace outland { namespace file 
{

	//--------------------------------------------------
	device_disk_t::device_disk_t()
		: _operation_size(0)
	{

	}

	//--------------------------------------------------
	device_disk_t::~device_disk_t()
	{

	}

	//--------------------------------------------------
	error_t	device_disk_t::init()
	{
		error_t err;

		err = os::file::queue_create(_queue);
		if (err)
			return err;

		for (size_t i = 0; i < u::array_size(_operation_pool); ++i)
		{
			err = os::file::operation_create(_operation_pool[i]);
			if (err)
			{
				for (size_t j = 0; j < i; ++j)
					os::file::operation_destroy(_operation_pool[j]);

				os::file::queue_destroy(_queue);
				return err;
			}
		}

		_operation_size = u::array_size(_operation_pool);

		return err_ok;
	}

	//--------------------------------------------------
	void	device_disk_t::clean()
	{
		for (size_t i = 0; i < u::array_size(_operation_pool); ++i)
			os::file::operation_destroy(_operation_pool[i]);

		_operation_size = 0;

		os::file::queue_destroy(_queue);
	}

	//--------------------------------------------------
	file_disk_t* device_disk_t::_cast(file_t* file)
	{
		return static_cast<file_disk_t*>(file);
	}

	//--------------------------------------------------
	os::file::access_t device_disk_t::_to_os_access(access_mask_t access)
	{
		os::file::access_t os_access = 0;
		if (access & access_read)
			os_access |= os::file::access_read;
		if (access & access_write)
			os_access |= os::file::access_write;

		return os_access;
	}

	//--------------------------------------------------
	os::file::open_t device_disk_t::_to_os_open(open_t open)
	{
		switch (open)
		{
		case outland::file::open_existing:
			return os::file::open_t::existing;
		case outland::file::open_try_new:
			return os::file::open_t::try_new;
		case outland::file::open_overwrite:
			return os::file::open_t::overwrite;
		case outland::file::open_always:
			return os::file::open_t::always;
		default:
			O_ASSERT(false);
			return os::file::open_t::existing;
		}
	}

	//--------------------------------------------------
	error_t	device_disk_t::open(file_id file, char8_t const* path, open_t open, access_mask_t access)
	{
		error_t err;
		auto* df = _cast(file);
		err = os::file::open(df->file, _queue, _to_os_open(open), _to_os_access(access), path);
		if (err)
			return err;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	device_disk_t::remove(char8_t const* path)
	{
		return os::file::remove(path);
	}

	//--------------------------------------------------
	void	device_disk_t::close(file_id file)
	{
		auto* df = _cast(file);
		os::file::close(df->file);
		df->file = nullptr;
	}

	//--------------------------------------------------
	error_t	device_disk_t::get_size(file_id file, file_size_t& size)
	{
		error_t err;
		auto* df = _cast(file);
		err = os::file::size(df->file, size);
		if (err)
			return err;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	device_disk_t::set_size(file_id file, file_size_t size)
	{
		error_t err;
		auto* df = _cast(file);
		err = os::file::resize(df->file, size);
		if (err)
			return err;

		return err_ok;
	}

	//--------------------------------------------------
	void	device_disk_t::read(request_id request)
	{
		_read_queue.push_back(request);
	}

	//--------------------------------------------------
	void	device_disk_t::write(request_id request)
	{
		_write_queue.push_back(request);
	}

	//--------------------------------------------------
	void	device_disk_t::epoll(request_id* request_array, size_t request_capacity, size_t& request_size)
	{
		error_t err;

		os::file::result_t		result_array[32];
		size_t					result_size;

		os::file::queue_process(_queue, result_array, m::min(u::array_size(result_array), request_capacity), result_size);
		for (size_t i = 0; i < result_size; ++i)
		{
			os::file::request_t req;
			os::file::operation_get_request(result_array[i].operation, req);
			request_array[i] = (request_id)req.user;
			request_array[i]->error = result_array[i].error;

			_operation_pool[_operation_size] = result_array[i].operation;
			++_operation_size;
		}
		request_size = result_size;

		while (_operation_size && _write_queue.begin())
		{
			auto* wr = _write_queue.begin();
			auto op = _operation_pool[_operation_size - 1];
			auto* df = _cast(wr->file);
			os::file::request_t req;
			req.file = df->file;
			req.buffer = wr->buffer;
			req.size = wr->size;
			req.where = wr->offset;
			req.user = wr;

			os::file::operation_set_request(op, req);
			err = os::file::write(op);
			if (err)
			{
				O_ASSERT(false);
				break;
			}

			--_operation_size;
			_write_queue.pop_front();
		}

		while (_operation_size && _read_queue.begin())
		{
			auto* rd = _read_queue.begin();
			auto op = _operation_pool[_operation_size - 1];
			auto* df = _cast(rd->file);
			os::file::request_t req;
			req.file = df->file;
			req.buffer = rd->buffer;
			req.size = rd->size;
			req.where = rd->offset;
			req.user = rd;

			os::file::operation_set_request(op, req);
			err = os::file::read(op);
			if (err)
			{
				O_ASSERT(false);
				break;
			}

			--_operation_size;
			_read_queue.pop_front();
		}
	}

} }
