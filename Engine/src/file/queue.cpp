#include "pch.h"
#include "queue.h"
#include "math\integer.h"

namespace outland { namespace file 
{
	//--------------------------------------------------
	void queue::memory_offsets(create_t const& create, memory_info_t& info, size_t (&offset_arr)[memory_layout_size])
	{
		size_t cache_size = 64;

		size_t align_arr[memory_layout_size];
		size_t size_arr[memory_layout_size];

		memory_info_t mem_atom;
		os::atomic::memory_info_uint32(mem_atom);

		align_arr[memory_layout_queue] = alignof(queue_t);
		size_arr[memory_layout_queue] = sizeof(queue_t);
		align_arr[memory_layout_command_cache] = alignof(queue_cache_t);
		size_arr[memory_layout_command_cache] = sizeof(queue_cache_t);
		align_arr[memory_layout_result_cache] = alignof(queue_cache_t);
		size_arr[memory_layout_result_cache] = sizeof(queue_cache_t);
		align_arr[memory_layout_result_queue] = alignof(request_list_t);
		size_arr[memory_layout_result_queue] = sizeof(request_list_t);
		align_arr[memory_layout_command_produced] = mem_atom.align;
		size_arr[memory_layout_command_produced] = mem_atom.size;
		align_arr[memory_layout_command_array] = alignof(command_t);
		size_arr[memory_layout_command_array] = sizeof(command_t) * create.queue_capacity;
		align_arr[memory_layout_result_consumed] = mem_atom.align;
		size_arr[memory_layout_result_consumed] = mem_atom.size;
		align_arr[memory_layout_result_produced] = mem_atom.align;
		size_arr[memory_layout_result_produced] = mem_atom.size;
		align_arr[memory_layout_result_array] = alignof(result_t);
		size_arr[memory_layout_result_array] = sizeof(result_t) * create.queue_capacity;
		align_arr[memory_layout_command_consumed] = mem_atom.align;
		size_arr[memory_layout_command_consumed] = mem_atom.size;

		align_arr[memory_cache_0] = m::max(align_arr[memory_cache_0], cache_size);
		align_arr[memory_cache_1] = m::max(align_arr[memory_cache_1], cache_size);
		align_arr[memory_cache_2] = m::max(align_arr[memory_cache_2], cache_size);
		align_arr[memory_cache_3] = m::max(align_arr[memory_cache_3], cache_size);

		u::mem_offsets(align_arr, size_arr, offset_arr, info.align, info.size, memory_layout_size, cache_size);
	}

	//--------------------------------------------------
	void	queue::memory_info(memory_info_t& info, create_t const& create)
	{
		size_t offset_arr[memory_layout_size];
		memory_offsets(create, info, offset_arr);
	}

	//--------------------------------------------------
	void	queue::create(queue_id& queue, create_t const& create, void* memory)
	{
		memory_info_t info;
		size_t offset_arr[memory_layout_size];
		memory_offsets(create, info, offset_arr);
		
		byte_t* buffer = (byte_t*)memory;

		queue = O_CREATE(queue_t, buffer + offset_arr[memory_layout_queue]);
		queue->command_cache = O_CREATE(queue_cache_t, buffer + offset_arr[memory_layout_command_cache]);
		queue->result_cache = O_CREATE(queue_cache_t, buffer + offset_arr[memory_layout_result_cache]);
		queue->result_queue = O_CREATE(request_list_t, buffer + offset_arr[memory_layout_result_queue]);
		os::atomic::create(queue->command_produced, 0, buffer + offset_arr[memory_layout_command_produced]);
		queue->command_array = (command_t*)(buffer + offset_arr[memory_layout_command_array]);
		os::atomic::create(queue->result_consumed, 0, buffer + offset_arr[memory_layout_result_consumed]);
		os::atomic::create(queue->result_produced, 0, buffer + offset_arr[memory_layout_result_produced]);
		queue->result_array = (result_t*)(buffer + offset_arr[memory_layout_result_array]);
		os::atomic::create(queue->command_consumed, 0, buffer + offset_arr[memory_layout_command_consumed]);
		queue->capacity = (uint32_t)create.queue_capacity;

		u::mem_clr(queue->command_cache, sizeof(queue_cache_t));
		u::mem_clr(queue->result_cache, sizeof(queue_cache_t));
	}

	//--------------------------------------------------
	void	queue::destroy(queue_id queue)
	{
		O_DESTROY(queue_cache_t, queue->command_cache);
		O_DESTROY(queue_cache_t, queue->result_cache);
		O_DESTROY(request_list_t, queue->result_queue);
		os::atomic::destroy(queue->command_produced);
		os::atomic::destroy(queue->result_consumed);
		os::atomic::destroy(queue->result_produced);		
		os::atomic::destroy(queue->command_consumed);

		O_DESTROY(queue_t, queue);
	}

	//--------------------------------------------------
	bool queue::cmd_can_produce(queue_t* queue)
	{
		return queue->capacity != (queue->command_cache->cached - queue->command_cache->finished);
	}

	//--------------------------------------------------
	void queue::cmd_produce(queue_t* queue, command_t*& cmd)
	{
		O_ASSERT(queue::cmd_can_produce(queue));

		cmd = queue->command_array + (queue->command_cache->cached % queue->capacity);
		++queue->command_cache->cached;
	}

	//--------------------------------------------------
	void queue::cmd_flush_producer(queue_t* queue)
	{
		if (queue->command_cache->cached != queue->command_cache->submited)
		{
			os::atomic::write(queue->command_produced, queue->command_cache->cached, os::memory::order_flush);
			queue->command_cache->submited = queue->command_cache->cached;
		}
	}

	//--------------------------------------------------
	void queue::cmd_evict_producer(queue_t* queue)
	{
		queue->command_cache->finished = os::atomic::read(queue->command_consumed, os::memory::order_relaxed);
	}

	//--------------------------------------------------
	void queue::cmd_evict_consumer(queue_t* queue)
	{
		queue->result_cache->consumable = os::atomic::read(queue->command_produced, os::memory::order_evict);
	}

	//--------------------------------------------------
	void queue::cmd_flush_consumer(queue_t* queue)
	{
		if (queue->result_cache->consuming != queue->result_cache->consumed)
		{
			os::atomic::write(queue->command_consumed, queue->result_cache->consuming, os::memory::order_flush);
			queue->result_cache->consumed = queue->result_cache->consuming;
		}
	}

	//--------------------------------------------------
	bool queue::cmd_consume(queue_t* queue, command_t const*& cmd)
	{
		if ((queue->result_cache->consumable == queue->result_cache->consuming))
			return false;

		cmd = queue->command_array + (queue->result_cache->consuming % queue->capacity);
		++queue->result_cache->consuming;
		return true;
	}

	//--------------------------------------------------
	bool queue::res_can_produce(queue_t* queue)
	{
		return queue->capacity != (queue->result_cache->cached - queue->result_cache->finished);
	}

	//--------------------------------------------------
	void queue::res_produce(queue_t* queue, result_t*& res)
	{
		O_ASSERT(queue::res_can_produce(queue));

		res = queue->result_array + (queue->result_cache->cached % queue->capacity);
		++queue->result_cache->cached;
	}

	//--------------------------------------------------
	void queue::res_flush_producer(queue_t* queue)
	{
		if (queue->result_cache->cached != queue->result_cache->submited)
		{
			os::atomic::write(queue->result_produced, queue->result_cache->cached, os::memory::order_flush);
			queue->result_cache->submited = queue->result_cache->cached;
		}
	}

	//--------------------------------------------------
	void queue::res_evict_producer(queue_t* queue)
	{
		queue->result_cache->finished = os::atomic::read(queue->result_consumed, os::memory::order_relaxed);
	}

	//--------------------------------------------------
	void queue::res_evict_consumer(queue_t* queue)
	{
		queue->command_cache->consumable = os::atomic::read(queue->result_produced, os::memory::order_evict);
	}

	//--------------------------------------------------
	void queue::res_flush_consumer(queue_t* queue)
	{
		if (queue->command_cache->consuming != queue->command_cache->consumed)
		{
			os::atomic::write(queue->result_consumed, queue->command_cache->consuming, os::memory::order_flush);
			queue->command_cache->consumed = queue->command_cache->consuming;
		}
	}

	//--------------------------------------------------
	bool queue::res_consume(queue_t* queue, result_t const*& res)
	{
		if ((queue->command_cache->consumable == queue->command_cache->consuming))
			return false;

		res = queue->result_array + (queue->command_cache->consuming % queue->capacity);
		++queue->command_cache->consuming;
		return true;
	}

	//--------------------------------------------------
	request_list_t* queue::res_request_list(queue_t* queue)
	{
		return queue->result_queue;
	}

} }
