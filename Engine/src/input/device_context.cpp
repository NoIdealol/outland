#include "pch.h"
#include "device_context.h"
#include "math\integer.h"

namespace outland { namespace input
{

	error_t create_device_context(device_context_t*& ctx, allocator_t* allocator, os::hid::device_id dev, os::hid::usage_t type)
	{
		error_t err;

		ctx = mem<device_context_t>::alloc(allocator, allocator, dev, type);
		if (nullptr == ctx)
			return err_out_of_memory;
		
		err = ctx->init();
		if (err)
		{
			mem<device_context_t>::free(allocator, ctx);
			ctx = nullptr;
			return err;
		}

		return err_ok;
	}

	void destroy_device_context(device_context_t* ctx, allocator_t* allocator)
	{
		ctx->clean();
		mem<device_context_t>::free(allocator, ctx);
	}


	device_context_t::device_context_t(allocator_t* allocator, os::hid::device_id dev, os::hid::usage_t type)
		: _index_array(nullptr)
		, _reserved_count(0)
		, _ctx_info_array(allocator)
		, _allocator(allocator)
		, _device_id(dev)
		, _device_type(type)
	{
		u::mem_clr(&_device_caps, sizeof(_device_caps));
		_device_caps.feature_array = nullptr;
	}

	device_context_t::~device_context_t()
	{
		
	}

	error_t device_context_t::init()
	{
		error_t err;

		err = os::input::get_hid_caps(_device_id, _device_caps);
		if (err)
			return err;

		_device_caps.feature_array = mem<os::hid::feature_usage_t[]>::alloc(_allocator, _device_caps.feature_count);
		if (nullptr == _device_caps.feature_array)
		{
			clean();
			return err_out_of_memory;
		}

		_index_array = mem<device_binding_t[]>::alloc(_allocator, _device_caps.feature_count, _allocator);
		if (nullptr == _index_array)
		{
			clean();
			return err_out_of_memory;
		}

		err = _reserve(8);
		if (err)
		{
			clean();
			return err;
		}

		return err_ok;
	}

	void	device_context_t::clean()
	{
		if (nullptr != _device_caps.feature_array)
		{
			mem<os::hid::feature_usage_t[]>::free(_allocator, _device_caps.feature_array, _device_caps.feature_count);
			_device_caps.feature_array = nullptr;
		}

		if (nullptr != _index_array)
		{
			mem<device_binding_t[]>::free(_allocator, _index_array, _device_caps.feature_count);
			_index_array = nullptr;
		}
	}

	error_t device_context_t::_reserve(size_t size)
	{
		error_t err;
		auto* beg = _index_array;
		auto* end = _index_array + _device_caps.feature_count;
		for (auto* it = beg; it != end; ++it)
		{
			err = it->reserve(size);
			if (err)
				return err;
		}

		err = _ctx_info_array.reserve(size);
		if (err)
			return err;

		_reserved_count = size;
		return err_ok;
	}

	error_t	device_context_t::_find_feature(os::hid::feature_usage_t& feature_usage, os::hid::usage_t usage)
	{
		auto* beg = _device_caps.feature_array;
		auto* end = _device_caps.feature_array + _device_caps.feature_count;

		for (auto* it = beg; it != end; ++it)
		{
			if (it->usage == usage)
			{
				feature_usage = *it;
				return err_ok;
			}
		}

		return err_not_found;
	}

	error_t device_context_t::bind_context(action_context_t* ctx, priority_t priority)
	{
		error_t err;
		size_t ctx_idx;

		//ensure memory is allocated
		if (_reserved_count == _ctx_info_array.size())
		{
			size_t reserve_try = m::max(_reserved_count << 1, size_t(4));
			err = _reserve(reserve_try);
			if (err)
				return err;

			_reserved_count = reserve_try;
		}

		//insert new ctx
		{
			//find insert index
			auto it = _ctx_info_array.begin();
			while ((it != _ctx_info_array.end()) && (priority <= it->priority))
			{
				++it;
			}
			ctx_idx = it - _ctx_info_array.begin();

			//insert new ctx
			_ctx_info_array.insert(ctx_idx);
			ctx_info_t& info = _ctx_info_array[ctx_idx];
			info.priority = priority;
			info.context = ctx;

			auto* beg = _index_array;
			auto* end = _index_array + _device_caps.feature_count;
			for (auto* it = beg; it != end; ++it)
			{
				it->insert(ctx_idx);
			}
		}

		//bind new ctx
		{
			auto* action_beg = ctx->action_array;
			auto* action_end = ctx->action_array + ctx->action_count;
			for (auto* action_it = action_beg; action_it != action_end; ++action_it)
			{
				if (action_it->binding.device_type != _device_type)
					continue;
				
				auto* binding_beg = action_it->binding.binding_array;
				auto* binding_end = action_it->binding.binding_array + action_it->binding.state_count;
				for (auto* binding_it = binding_beg; binding_it != binding_end; ++binding_it)
				{
					os::hid::feature_usage_t feature_usage;
					err = _find_feature(feature_usage, *binding_it);
					if (err)
					{
						//todo: log bad binding
						continue;
					}

					//todo: input type validation
					_index_array[feature_usage.index].bind(action_it, ctx_idx);
				}


			}
		}
		
	}

} }