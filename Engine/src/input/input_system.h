#pragma once
#include "system\input\input.h"

namespace outland { namespace input
{
	using priority_t = uint32_t;
	using callback_t = function<void(os::hid::value_t const*, size_t)>;

	class user_context_t;
	using user_ctx_id = user_context_t*;

	class device_context_t;
	using device_ctx_id = device_context_t*;

	struct action_context_t;
	using action_ctx_id = action_context_t*;

	struct hid_info_t
	{
		device_ctx_id			device_ctx;
		os::hid::device_id		device;
		os::hid::usage_t		device_usage;
		os::hid::device_caps_t	device_caps;
	};

	class system_t
	{
	private:
		virtual ~system_t() = default;

	public:

		virtual void update() = 0;

		virtual error_t bind_device_ctx(user_ctx_id user, device_ctx_id device_ctx) = 0;
		virtual error_t unbind_device_ctx(user_ctx_id user, device_ctx_id device_ctx) = 0;

		virtual error_t bind_action_ctx(user_ctx_id user, action_ctx_id action_ctx, priority_t priority) = 0;
		virtual error_t unbind_action_ctx(user_ctx_id user, action_ctx_id action_ctx) = 0;

		virtual error_t get_hid_info_array(array<hid_info_t>& info_array) = 0;
	};
	
} }