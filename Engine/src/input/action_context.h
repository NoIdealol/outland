#pragma once
#include "input_system.h"

namespace outland { namespace input
{
	
	struct binding_t
	{
		os::hid::usage_t	device_type;

		size_t				state_count;
		os::hid::usage_t*	binding_array;
		os::hid::value_t*	value_array;
	};

	
	

	//action can have multiple inputs bound to it
	struct action_t
	{
		char8_t*		name;
		priority_t		priority;
		callback_t		callback;
		binding_t		binding;
	};

	//this is one layer with the list of actions bindings
	struct action_context_t
	{
		action_t*	action_array;
		size_t		action_count;
	};

} }


namespace outland { namespace input
{


} }