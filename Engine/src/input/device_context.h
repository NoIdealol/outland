#pragma once
#include "action_context.h"

namespace outland { namespace input
{
	class device_binding_t
	{
		size_t				_binding_count;
		array<action_t*>	_binding_array;

	public:
		device_binding_t(allocator_t* allocator) : _binding_array(allocator), _binding_count(0) {}

		error_t reserve(size_t size) { return _binding_array.reserve(size); }
		bool is_bound() { return 0 != _binding_count; }
		void insert(size_t index) { _binding_array.insert(index, nullptr); }
		void remove(size_t index) { _binding_array.remove(index); }
		void bind(action_t* action, size_t index)
		{
			if (nullptr != action && nullptr == _binding_array[index])
				++_binding_count;

			if (nullptr == action && nullptr != _binding_array[index])
				--_binding_count;

			_binding_array[index] = action;
		}
	};

	//here we can bind action contexts
	class device_context_t
	{
	public:
		

		struct ctx_info_t
		{
			priority_t			priority;
			action_context_t*	context;
		};

	private:
		allocator_t*		_allocator;

		device_binding_t*	_index_array; //size is state_count * num context bound
		size_t				_reserved_count;

		array<ctx_info_t>	_ctx_info_array;

		os::hid::device_id		_device_id;
		os::hid::device_caps_t	_device_caps;
		os::hid::usage_t		_device_type;

	private:
		error_t _reserve(size_t size);
		error_t	_find_feature(os::hid::feature_usage_t& feature_usage, os::hid::usage_t usage);

	public:
		device_context_t(allocator_t* allocator, os::hid::device_id dev, os::hid::usage_t type);
		~device_context_t();

		error_t init();
		void	clean();

		//size_t		get_state_count() const { return _state_count; }
		//binding_t*	get_index_array() const { return _index_array; }

		error_t		bind_context(action_context_t* ctx, priority_t priority);
	};

} }
