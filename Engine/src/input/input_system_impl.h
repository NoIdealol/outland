#pragma once
#include "input_system.h"
#include "device_context.h"
#include "action_context.h"

namespace outland { namespace input
{

	//here we bind devices with action contexts
	class user_context_t
	{
		array<action_context_t*>	action_context_array; //active action contexts
		array<device_context_t*>	device_context_array; //active devices for the contexts

	public:
		user_context_t(allocator_t* allocator);
	};

	class system_impl_t 
		: public system_t
		, public os::input_t
	{


	public:
		system_impl_t();
		~system_impl_t();
	};

} }