#pragma once

#include "..\lib\resource_manager.h"
#include "..\lib\resource_factory.h"

namespace outland { namespace resource 
{
	//////////////////////////////////////////////////
	// RESOURCE
	//////////////////////////////////////////////////

	enum resource_state_t : uint16_t
	{
		resource_state_depending,	//waiting for dependencies
		resource_state_loading,		//waiting for data
		resource_state_creating,	//waiting for factory
		resource_state_ready,		//ready

		resource_state_failed,		//something failed
	};

	struct resource_t
	{
		uintptr_t			handle;

		struct alive_t
		{
			uint16_t			count;
			resource_state_t	state;
			error_t				error;
		};

		union
		{
			alive_t		alive;
			resource_t*	release;
		};
	};

	struct resource_desc_t
	{
		char8_t*		name;
		char8_t*		path;
		resource_id*	dep_array;
		size_t			dep_size;
	};

	using find_resource_t = function<resource_t*(resource_id uid)>;
	using find_desc_t = function<resource_desc_t const*(resource_id uid)>;
	using free_resource_t = function<void(resource_id uid)>;
	using load_resource_t = function<bool(resource_desc_t const* desc, resource_id uid)>;
	using load_data_t = function<bool(resource_desc_t const* desc, resource_t* resource)>;
	using done_resource_t = function<void(resource_desc_t const* desc, resource_t* resource)>;
	using create_resource_t = function<bool(resource_desc_t const*, resource_t*, void*, size_t)>;
	using free_data_t = function<void(void*, size_t)>;
	using done_file_t = function<void(void*, size_t, void*, error_t)>;

	package_uid	package_get_id(resource_id uid);
	uint32_t	package_hash(package_uid uid);
	bool		package_cmp(package_uid uid1, package_uid uid2);
	type_uid	type_get_id(resource_id uid);
	uint32_t	type_hash(type_uid uid);
	bool		type_cmp(type_uid uid1, type_uid uid2);
	uint32_t	resource_hash(resource_id uid);
	bool		resource_cmp(resource_id uid1, resource_id uid2);
	uint32_t	name_hash(uint32_t const* seed, char8_t const* name);
	bool		name_cmp(char8_t const* name1, char8_t const* name2);
	resource_id	resource_make_id(type_uid type, package_uid package, uint32_t hash);
	
} }
