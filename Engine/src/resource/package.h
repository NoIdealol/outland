#pragma once
#include "resource.h"

namespace outland { namespace resource
{
	//////////////////////////////////////////////////
	// PACKAGE
	//////////////////////////////////////////////////

	class package_t
	{
		package_uid											_uid;
		uint32_t											_hash_seed;
		uint32_t											_uid_counter;
		hash_map<resource_desc_t const*, resource_id>		_desc_map;
		hash_map<resource_id, char8_t const*>				_name_map;
		allocator_t*										_allocator;

	public:
		package_t(package_uid uid, allocator_t* allocator);
		~package_t();

		package_uid		uid() { return _uid; }
		allocator_t*	allocator() { return _allocator; };
		error_t			find(resource_id uid, resource_desc_t const*& desc);
		error_t			find(char8_t const* name, resource_id& uid);
		error_t			resource_add(resource_id& resource, package::resource_add_t const& add);
	};

} }
