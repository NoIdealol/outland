#include "pch.h"
#include "data_loader.h"
#include "memory\circular.h"

namespace outland { namespace resource
{
	//////////////////////////////////////////////////
	// FILE LOADER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	file_loader_t::file_loader_t(file::queue_id queue, allocator_t* allocator)
		: _allocator(allocator)
		, _load_memory(nullptr)
		, _load_size(0)
		, _load_pool(allocator)
		, _update_queue(allocator)
		, _queue(queue)
		, _data_allocator(nullptr)
		, _data_buffer(nullptr)
		, _data_size(0)
	{

	}

	//--------------------------------------------------
	error_t	file_loader_t::init(size_t queue_size)
	{
		error_t err;
		err = _load_pool.reserve(queue_size);
		if (err)
			return err;

		err = _update_queue.reserve(queue_size);
		if (err)
			return err;

		memory_info_t info;

		{
			circular_allocator::get_memory_info(info);

			/*size_t align_arr[] = { info.align, 16 };
			size_t size_arr[] = { info.size, 1024 * 1024 * 4 };
			size_t offset_arr[2];
			size_t circ_align;
			size_t circ_size;
			u::mem_offsets(align_arr, size_arr, offset_arr, circ_align, circ_size, 2);*/
			void* memory_allocator = _allocator->alloc(info.size, info.align);
			if(nullptr == memory_allocator)
				return err_out_of_memory;

			_data_size = 1024 * 1024 * 4;
			void* _data_buffer = _allocator->alloc(_data_size, 16);
			if (nullptr == _data_buffer)
			{
				_allocator->free(memory_allocator, info.size);
				return err_out_of_memory;
			}


			circular_allocator::create_info_t create;
			create.buffer = _data_buffer;
			create.size = _data_size;

			err = circular_allocator::create(_data_allocator, create, memory_allocator);
			if (err)
			{
				_allocator->free(memory_allocator, info.size);
				_allocator->free(_data_buffer, _data_size);
				return err;
			}
		}

		{
			file::request::memory_info(info);

			size_t align_arr[] = { alignof(load_t), info.align };
			size_t size_arr[] = { sizeof(load_t), info.size };
			size_t offset_arr[2];
			size_t load_align;
			size_t load_size;
			u::mem_offsets(align_arr, size_arr, offset_arr, load_align, load_size, 2);

			_load_memory = _allocator->alloc(load_size * queue_size, load_align);
			if (nullptr == _load_memory)
			{
				circular_allocator::get_memory_info(info);
				circular_allocator::destroy(_data_allocator);
				_allocator->free(_data_allocator, info.size);
				_allocator->free(_data_buffer, _data_size);
				return err_out_of_memory;
			}

			_load_size = load_size * queue_size;

			byte_t* memory = (byte_t*)_load_memory;
			for (size_t i = 0; i < queue_size; ++i)
			{
				load_t* load = O_CREATE(load_t, memory + offset_arr[0]);
				file::request::create(load->file_request, memory + offset_arr[1]);
				memory += load_size;

				_load_pool.push_back(load);
			}
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	file_loader_t::clean()
	{
		_allocator->free(_load_memory, _load_size);
		_load_pool.clear();
		O_ASSERT(0 == _update_queue.size());

		memory_info_t info;
		circular_allocator::get_memory_info(info);
		circular_allocator::destroy(_data_allocator);
		_allocator->free(_data_allocator, info.size);
		_allocator->free(_data_buffer, _data_size);
	}

	//--------------------------------------------------
	bool	file_loader_t::_load_update(load_t* load)
	{
		switch (load->state)
		{
		case load_state_size_get:
		{
			if (file::get_size(_queue, load->file, load))
				return false;
			
			load->state = load_state_size_wait;
			return true;
		}
		case load_state_memory_get:
		{
			load->data_size = static_cast<size_t>(load->file_size);
			load->data_buffer = _data_allocator->alloc(load->data_size, 16);
			if (nullptr == load->data_buffer)
				return false;

			load->state = load_state_data_get;
			return _load_update(load);
		}
		case load_state_data_get:
		{
			if (file::read(_queue, load->file, load->file_request, 0, load->data_buffer, load->data_size, load))
				return false;

			load->state = load_state_data_wait;
			return true;
		}
		case load_state_close:
		{
			if(file::close(_queue, load->file))
				return false;

			_load_finish(load);
			return true;
		}
		case load_state_open: //open failed
		{
			_load_finish(load);
			return true;
		}
		case load_state_size_wait: //get size failed
		{
			if (file::close(_queue, load->file))
				return false;

			_load_finish(load);
			return true;
		}
		case load_state_data_wait: //read failed
		{
			if (file::close(_queue, load->file))
				return false;

			_data_allocator->free(load->data_buffer, load->data_size);

			_load_finish(load);
			return true;
		}
		default:
			O_ASSERT(false);
			load->error = err_unknown;
			_load_finish(load);
			return true;
		}
	}

	//--------------------------------------------------
	void	file_loader_t::_load_finish(load_t* load)
	{
		load->done(load->data_buffer, load->data_size, load->user_ptr, load->error);
		_load_pool.push_back(load);
	}

	//--------------------------------------------------
	bool	file_loader_t::load_file(char8_t const* path, done_file_t const& done, void* user_ptr)
	{
		if (0 == _load_pool.size())
			return false;

		load_t* load = _load_pool.last();
		if (file::open(_queue, path, file::open_existing, file::access_read, load))
			return false;

		load->state = load_state_open;
		load->done = done;
		load->data_buffer = nullptr;
		load->data_size = 0;
		load->user_ptr = user_ptr;
		load->file = nullptr;
		load->file_size = 0;
		//load->file_request;

		_load_pool.pop_back();

		return true;
	}

	//--------------------------------------------------
	void	file_loader_t::free_file(void* data, size_t size)
	{
		_data_allocator->free(data, size);
	}

	//--------------------------------------------------
	void	file_loader_t::update()
	{
		file::result_t	result_array[32];
		size_t			result_size;

		do
		{
			file::epoll(_queue, result_array, u::array_size(result_array), result_size);
			for (size_t i = 0; i < result_size; ++i)
			{
				auto& result = result_array[i];
				load_t* load = (load_t*)(result.user_ptr);
				load->error = result.error;
				if (err_ok == result.error)
				{
					switch (result.type)
					{
					case file::result_type_open:
						load->file = result.file;
						break;
					case file::result_type_get_size:
						load->file_size = result.size;
						break;
					case file::result_type_request:
						break;
					default:
						O_ASSERT(false);
						continue;
					}

					load->state = (load_state_t)(load->state + 1);
				}

				_update_queue.push_back(load);
			}

		} while (u::array_size(result_array) == result_size);

		while (_update_queue.size() && _load_update(_update_queue.front()))
		{
			_update_queue.pop_front();
		}

		file::flush(_queue);
	}

	//////////////////////////////////////////////////
	// DATA LOADER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	data_loader_t::data_loader_t(file_loader_t* file_loader, allocator_t* allocator)
		: _file_loader(file_loader)
		, _load_pool(allocator)
		, _load_array(nullptr)
		, _load_size(0)
		, _create_queue(allocator)
	{

	}

	//--------------------------------------------------
	/*data_loader_t::~data_loader_t()
	{

	}*/

	//--------------------------------------------------
	void	data_loader_t::_on_done_data(void* data, size_t size, void* user_ptr, error_t err)
	{
		load_t* load = (load_t*)user_ptr;
		if (err)
		{
			load->inst->alive.error = err;
			load->inst->alive.state = resource_state_failed;

			for (size_t i = 0; i < load->desc->dep_size; ++i)
			{
				_free_resource(load->desc->dep_array[i]);
			}

			_done_resource(load->desc, load->inst);
			_load_pool.push_back(load);
		}
		else
		{
			load->data = data;
			load->size = size;
			_create_queue.push_back(load);
		}
	}

	//--------------------------------------------------
	bool	data_loader_t::_create_alive(load_t* it)
	{
		if (it->inst->alive.count)
			return true;

		//chain the error
		it->inst->alive.error = err_aborted;
		it->inst->alive.state = resource_state_failed;

		for (size_t i = 0; i < it->desc->dep_size; ++i)
		{
			_free_resource(it->desc->dep_array[i]);
		}

		_done_resource(it->desc, it->inst);
		_create_queue.pop_front();
		_load_pool.push_back(it);

		return false;
	}

	//--------------------------------------------------
	error_t	data_loader_t::init(size_t queue_size)
	{
		error_t err;
		err = _load_pool.reserve(queue_size);
		if (err)
			return err;

		err = _create_queue.reserve(queue_size);
		if (err)
			return err;

		err = u::mem_alloc(_load_pool.allocator(), _load_array, queue_size);
		if (err)
			return err;

		_load_size = queue_size;
		for (size_t i = 0; i < _load_size; ++i)
		{
			_load_pool.push_back(_load_array + i);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	data_loader_t::clean()
	{
		u::mem_free(_load_pool.allocator(), _load_array, _load_size);
		O_ASSERT(_load_size == _load_pool.size());
		_load_pool.clear();
		O_ASSERT(0 == _create_queue.size());
	}

	//--------------------------------------------------
	bool	data_loader_t::load_data(resource_desc_t const* desc, resource_t* resource)
	{
		if (0 == _load_pool.size())
			return false;

		auto* load = _load_pool.last();
		load->data = nullptr;
		load->size = 0;
		load->desc = desc;
		load->inst = resource;

		done_file_t done;
		done.bind<data_loader_t, &data_loader_t::_on_done_data>(this);
		
		if (_file_loader->load_file(desc->path, done, load))
		{
			_load_pool.pop_back();
			resource->alive.state = resource_state_loading;
			return true;
		}
		else
		{
			return false;
		}
	}

	//--------------------------------------------------
	void	data_loader_t::free_data(void* data, size_t size)
	{
		_file_loader->free_file(data, size);
	}

	//--------------------------------------------------
	void	data_loader_t::update()
	{
		while (_create_queue.size())
		{
			auto* it = _create_queue.front();
			if (!_create_alive(it))
				continue;

			if (_create_resource(it->desc, it->inst, it->data, it->size))
			{
				_create_queue.pop_front();
				_load_pool.push_back(it);
				continue;
			}
			else
			{
				break;
			}
		}
	}


} }
