#include "pch.h"
#include "manager.h"

namespace outland { namespace resource
{
	//////////////////////////////////////////////////
	// MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	manager_t::manager_t(file::queue_id queue, allocator_t* allocator, allocator_t* resource_allocator)
		: _package_map(resource_allocator, decltype(_package_map)::fnc_hash_t().bind<&package_hash>(), decltype(_package_map)::fnc_cmp_t().bind<&package_cmp>())
		, _type_map(allocator, decltype(_type_map)::fnc_hash_t().bind<&type_hash>(), decltype(_type_map)::fnc_cmp_t().bind<&type_cmp>())
		, _resource_map(resource_allocator, decltype(_resource_map)::fnc_hash_t().bind<&resource_hash>(), decltype(_resource_map)::fnc_cmp_t().bind<&resource_cmp>())
		, _type_array(allocator)
		, _acquire_queue(allocator)
		, _resource_allocator(resource_allocator)
		, _file_loader(queue, allocator)
	{

	}

	//--------------------------------------------------
	error_t	manager_t::init(type_desc_t const* desc_array, size_t desc_size, size_t queue_size)
	{
		error_t err;
		err = _type_map.reserve(desc_size);
		if (err)
			return err;

		err = _type_array.reserve(desc_size);
		if (err)
			return err;

		err = _acquire_queue.reserve(queue_size * 4);
		if (err)
			return err;

		err = _file_loader.init(queue_size * 4);
		if (err)
			return err;

		for (size_t i = 0; i < desc_size; ++i)
		{
			_type_array.push_back(desc_array[i].factory, desc_array[i].handle_info, &_file_loader, _type_array.allocator());
			auto& it = _type_array.last();

			err = it.dependency.init(queue_size);
			if (err)
				goto _clean_dependency;
			err = it.type.init(queue_size);
			if (err)
				goto _clean_type;
			err = it.data.init(queue_size);
			if (err)
				goto _clean_data;

			continue;
		
		_clean_data:
			it.type.clean();
		_clean_type:
		_clean_dependency:

			for (size_t j = 0; j < i; ++j)
			{
				auto& it = _type_array[j];
				it.type.clean();
				it.data.clean();
			}
			
			_file_loader.clean();
			return err;
		}

		for (size_t i = 0; i < desc_size; ++i)
		{
			_type_map.insert(desc_array[i].uid, &_type_array[i]);

			auto& it = _type_array[i];
			it.data._create_resource.bind<type_loader_t, &type_loader_t::create>(&it.type);
			it.data._done_resource.bind<manager_t, &manager_t::_done_resource>(this);
			it.data._free_resource.bind<manager_t, &manager_t::release>(this);

			it.type._free_resource.bind<manager_t, &manager_t::release>(this);
			it.type._free_data.bind<data_loader_t, &data_loader_t::free_data>(&it.data);
			it.type._done_resource.bind<manager_t, &manager_t::_done_resource>(this);
			
			it.dependency._find_resource.bind<manager_t, &manager_t::_find_resource>(this);
			it.dependency._find_desc.bind<manager_t, &manager_t::_find_desc>(this);
			it.dependency._free_resource.bind<manager_t, &manager_t::release>(this);
			it.dependency._load_resource.bind<manager_t, &manager_t::_load_resource>(this);
			it.dependency._load_data.bind<data_loader_t, &data_loader_t::load_data>(&it.data);
			it.dependency._done_resource.bind<manager_t, &manager_t::_done_resource>(this);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	manager_t::clean()
	{
		_type_map.clear();

		for (auto& it : _type_array)
		{
			it.type.clean();
			it.data.clean();
		}

		_type_array.clear();
	}

	//--------------------------------------------------
	bool	manager_t::_load_resource(resource_desc_t const* desc, resource_id uid)
	{
		error_t err;
		resource_t* inst = nullptr;
		if (_resource_map.find(uid, inst))
		{
			auto* type = _find_type(uid);
			O_ASSERT(type);
			
			err = type->dependency.can_load_dependencies();
			if (err)
				return false;

			err = u::mem_alloc(_resource_allocator, inst);
			if (err)
				return false;

			err = _resource_map.insert(uid, inst);
			if (err)
			{
				u::mem_free(_resource_allocator, inst);
				return false;
			}

			inst->alive.count = 1;
			inst->alive.error = err_ok;
			inst->handle = 0;
			inst->alive.state = resource_state_depending;

			type->dependency.load_dependencies(desc, inst);
			return true;
		}
		else
		{
			++inst->alive.count;
			return true;
		}
	}

	//--------------------------------------------------
	error_t	manager_t::acquire(resource_id uid, void* user_ptr)
	{
		error_t err;
		if (0 == _acquire_queue.space())
			return err_too_many_requests;

		resource_t* inst = nullptr;
		if (_resource_map.find(uid, inst))
		{
			auto* desc = _find_desc(uid);
			if (nullptr == desc)
				return err_not_found;

			auto* type = _find_type(uid);
			if (nullptr == type)
				return err_not_found;

			err = type->dependency.can_load_dependencies();
			if (err)
				return err;

			err = u::mem_alloc(_resource_allocator, inst);
			if (err)
				return err;

			err = _resource_map.insert(uid, inst);
			if (err)
			{
				u::mem_free(_resource_allocator, inst);
				return err;
			}

			inst->alive.count = 1;
			inst->alive.error = err_ok;
			inst->handle = 0;
			inst->alive.state = resource_state_depending;

			type->dependency.load_dependencies(desc, inst);

			acquire_t ac;
			ac.uid = uid;
			ac.resource = inst;
			ac.user_ptr = user_ptr;
			_acquire_queue.push_back(ac);

			return err_ok;
		}
		else
		{
			++inst->alive.count;

			acquire_t ac;
			ac.uid = uid;
			ac.resource = inst;
			ac.user_ptr = user_ptr;

			switch (inst->alive.state)
			{
			case resource_state_ready:
			case resource_state_failed:
				_acquire_queue.push_front(ac);
				return err_ok;
			default:
				_acquire_queue.push_back(ac);
				return err_ok;
			}
		}

	}

	//--------------------------------------------------
	resource_desc_t const*	manager_t::_find_desc(resource_id uid)
	{
		package_t* package;
		if (_package_map.find(package_get_id(uid), package))
			return nullptr;

		resource_desc_t const* desc;
		if (package->find(uid, desc))
			return nullptr;

		return desc;
	}

	//--------------------------------------------------
	resource_t*			manager_t::_find_resource(resource_id uid)
	{
		resource_t* resource;
		if (_resource_map.find(uid, resource))
			return nullptr;

		return resource;
	}

	//--------------------------------------------------
	manager_t::type_t*	manager_t::_find_type(resource_id uid)
	{
		type_t* type;
		if (_type_map.find(type_get_id(uid), type))
			return nullptr;

		return type;
	}

	//--------------------------------------------------
	void	manager_t::_done_resource(resource_desc_t const* desc, resource_t* inst)
	{
		O_ASSERT(resource_state_ready == inst->alive.state || resource_state_failed == inst->alive.state);

		if (inst->alive.count)
			return;

		//cleanup unreferenced resources

		error_t err;
		resource_id uid;
		resource_desc_t const* desc_cmp;
		for (auto it : _package_map)
		{
			err = it->find(desc->name, uid);
			if (err)
				continue;

			err = it->find(uid, desc_cmp);
			O_ASSERT(err_ok == err);

			if (desc == desc_cmp)
			{
				++inst->alive.count; //hack to not duplicate code,
				release(uid);
				return;
			}
		}

		O_ASSERT(false);
	}

	//--------------------------------------------------
	void	manager_t::release(resource_id uid)
	{
		error_t err;
		resource_t* inst = nullptr;
		err = _resource_map.find(uid, inst);
		O_ASSERT(err_ok == err);
		if (err)
			return;

		if (--inst->alive.count)
			return;

		if (resource_state_ready == inst->alive.state || resource_state_failed == inst->alive.state)
		{
			err = _resource_map.remove(uid);
			O_ASSERT(err_ok == err);

			
		}

		if (resource_state_ready == inst->alive.state)
		{
			auto* desc = _find_desc(uid);
			O_ASSERT(desc);

			auto* type = _find_type(uid);
			O_ASSERT(type);

			if (type->type.destroy(&inst->handle))
			{
				u::mem_free(_resource_allocator, inst);
			}
			else
			{
				//defer release, insert into list
				if (type->release_front)
				{
					type->release_back->release = inst;
					type->release_back = inst;
					inst->release = nullptr;
				}
				else
				{
					type->release_front = inst;
					type->release_back = inst;
					inst->release = nullptr;
				}
			}

			for (size_t i = 0; i < desc->dep_size; ++i)
				release(desc->dep_array[i]);
		}	
	}

	//--------------------------------------------------
	void	manager_t::update(acquire_result_t* result_array, size_t result_capacity, size_t& result_size)
	{
		_file_loader.update();

		for (auto& it : _type_array)
		{
			it.type.update();
			//try release
			while (it.release_front)
			{
				if (it.type.destroy(&it.release_front->handle))
				{
					//pop front
					if (it.release_front == it.release_back)
						it.release_back = nullptr;

					auto* inst = it.release_front;
					it.release_front = it.release_front->release;

					u::mem_free(_resource_allocator, inst);
				}
				else
				{
					break;
				}
			}

			it.data.update();
			it.dependency.update();
		}

		result_size = 0;
		while (result_capacity > result_size && _acquire_queue.size())
		{
			auto* ac = &_acquire_queue.front();
			error_t err = ac->resource->alive.error;

			switch (ac->resource->alive.state)
			{
			case resource_state_failed:
				release(ac->uid);
			case resource_state_ready:
				result_array[result_size].error = err;
				result_array[result_size].user_ptr = ac->user_ptr;
				++result_size;
				_acquire_queue.pop_front();
				break;
			default:
				return;
			}
		}
	}

	//--------------------------------------------------
	error_t	manager_t::package_add(package_id package, package_uid uid)
	{
		return _package_map.insert(uid, package);
	}

	//--------------------------------------------------
	void	manager_t::package_remove(package_uid uid)
	{
		error_t err;
		err = _package_map.remove(uid);
		O_ASSERT(err_ok == err);
	}

	//--------------------------------------------------
	error_t	acquire(manager_id manager, resource_id resource, void* user_ptr)
	{
		return manager->acquire(resource, user_ptr);
	}

	//--------------------------------------------------
	void	release(manager_id manager, resource_id resource)
	{
		manager->release(resource);
	}

	//--------------------------------------------------
	void	update(manager_id manager, acquire_result_t* result_array, size_t result_capacity, size_t& result_size)
	{
		manager->update(result_array, result_capacity, result_size);
	}

	//--------------------------------------------------
	error_t	manager::create(manager_id& manager, create_t const& create)
	{
		error_t err;
		manager_t* man;
		err = u::mem_alloc(create.manager_allocator, man);
		if (err)
			return err;

		O_CREATE(manager_t, man)(create.file_queue, create.manager_allocator, create.resource_allocator);
		err = man->init(create.type_array, create.type_size, create.queue_size);
		if (err)
		{
			O_DESTROY(manager_t, man);
			u::mem_free(create.manager_allocator, man);
			return err;
		}

		manager = man;

		return err_ok;
	}

	//--------------------------------------------------
	void	manager::destroy(manager_id manager)
	{
		auto* allocator = manager->allocator();
		manager->clean();
		O_DESTROY(manager_t, manager);
		u::mem_free(allocator, manager);
	}

} }
