#pragma once
#include "resource.h"
#include "math\integer.h"

#include "..\lib\file_system.h"

namespace outland { namespace resource
{
	//////////////////////////////////////////////////
	// FILE LOADER
	//////////////////////////////////////////////////

	class file_loader_t
	{
		enum load_state_t
		{
			load_state_open,
			load_state_size_get,
			load_state_size_wait,
			load_state_memory_get,
			load_state_data_get,
			load_state_data_wait,
			load_state_close,
		};

		struct load_t
		{
			error_t				error;
			load_state_t		state;
			done_file_t			done;
			void*				data_buffer;
			size_t				data_size;
			void*				user_ptr;
			file_id				file;
			file_size_t			file_size;
			file::request_id	file_request;
		};

	private:
		allocator_t*	_allocator;
		void*			_load_memory;
		size_t			_load_size;
		array<load_t*>	_load_pool;
		deque<load_t*>	_update_queue;
		file::queue_id	_queue;
		allocator_t*	_data_allocator;
		void*			_data_buffer;
		size_t			_data_size;

	private:
		bool	_load_update(load_t* load);
		void	_load_finish(load_t* load);

	public:
		file_loader_t(file::queue_id queue, allocator_t* allocator);
		error_t	init(size_t queue_size);
		void	clean();
		bool	load_file(char8_t const* path, done_file_t const& done, void* user_ptr);
		void	free_file(void* data, size_t size);
		void	update();
	};

	//////////////////////////////////////////////////
	// DATA LOADER
	//////////////////////////////////////////////////

	class data_loader_t
	{
		friend manager_t;

		struct load_t
		{
			resource_desc_t const*	desc;
			resource_t*				inst;
			void*					data;
			size_t					size;
		};

		free_resource_t		_free_resource;
		done_resource_t		_done_resource;
		create_resource_t	_create_resource;
		file_loader_t*		_file_loader;

		array<load_t*>		_load_pool;
		load_t*				_load_array;
		size_t				_load_size;
		deque<load_t*>		_create_queue;

	private:
		void	_on_done_data(void* data, size_t size, void* user_ptr, error_t err);
		bool	_create_alive(load_t* it);

	public:
		data_loader_t(file_loader_t* file_loader, allocator_t* allocator);
		//~data_loader_t();

		error_t	init(size_t queue_size);
		void	clean();
		bool load_data(resource_desc_t const* desc, resource_t* resource);
		void free_data(void* data, size_t size);
		void update();
	};

} }
