#include "pch.h"
#include "resource.h"
#include "utility\hash\crc.h"

namespace outland { namespace resource
{
	//////////////////////////////////////////////////
	// RESOURCE
	//////////////////////////////////////////////////

	//--------------------------------------------------
	package_uid	package_get_id(resource_id uid)
	{
		return package_uid((uid >> 32) & c::max_uint16);
	}

	//--------------------------------------------------
	uint32_t package_hash(package_uid uid)
	{
		return uid;
	}

	//--------------------------------------------------
	bool package_cmp(package_uid uid1, package_uid uid2)
	{
		return uid1 == uid2;
	}

	//--------------------------------------------------
	type_uid	type_get_id(resource_id uid)
	{
		return type_uid((uid >> 48) & c::max_uint16);
	}

	//--------------------------------------------------
	uint32_t type_hash(type_uid uid)
	{
		return uid;
	}

	//--------------------------------------------------
	bool type_cmp(type_uid uid1, type_uid uid2)
	{
		return uid1 == uid2;
	}

	//--------------------------------------------------
	uint32_t resource_hash(resource_id uid)
	{
		return uint32_t(uid);
	}

	//--------------------------------------------------
	bool resource_cmp(resource_id uid1, resource_id uid2)
	{
		return uid1 == uid2;
	}

	//--------------------------------------------------
	uint32_t	name_hash(uint32_t const* seed, char8_t const* name)
	{
		return hash::crc32(name, *seed);
	}

	//--------------------------------------------------
	bool		name_cmp(char8_t const* name1, char8_t const* name2)
	{
		return u::str_cmp(name1, name2);
	}

	//--------------------------------------------------
	resource_id	resource_make_id(type_uid type, package_uid package, uint32_t hash)
	{
		return resource_id((resource_id(type) << 48) | (resource_id(package) << 32) | hash);
	}

} }
