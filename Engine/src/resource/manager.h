#pragma once

#include "resource.h"
#include "type_loader.h"
#include "dependency_loader.h"
#include "data_loader.h"
#include "package.h"

namespace outland { namespace resource
{
	
	//////////////////////////////////////////////////
	// MANAGER
	//////////////////////////////////////////////////

	class manager_t
	{
		struct acquire_t
		{
			resource_id uid;
			resource_t* resource;
			void*		user_ptr;
		};

		struct type_t
		{
			type_loader_t			type;
			data_loader_t			data;
			dependency_loader_t		dependency;

			resource_t*				release_front;
			resource_t*				release_back;

			type_t(factory_t* factory, memory_info_t const& handle_info, file_loader_t* file_loader, allocator_t* allocator)
				: type(factory, handle_info, allocator)
				, dependency(allocator)
				, data(file_loader, allocator)
				, release_front(nullptr)
				, release_back(nullptr)
			{
			}
		};

	private:
		hash_map<package_t*, package_uid>			_package_map;
		hash_map<type_t*, type_uid>					_type_map;
		hash_map<resource_t*, resource_id>			_resource_map;
		array<type_t>								_type_array;
		deque<acquire_t>							_acquire_queue;
		allocator_t*								_resource_allocator;
		file_loader_t								_file_loader;

	private:
		resource_desc_t const*	_find_desc(resource_id uid);
		resource_t*				_find_resource(resource_id uid);
		type_t*					_find_type(resource_id uid);
		bool					_load_resource(resource_desc_t const* desc, resource_id uid);
		void					_done_resource(resource_desc_t const* desc, resource_t* inst);

	public:
		manager_t(file::queue_id queue, allocator_t* allocator, allocator_t* resource_allocator);
		allocator_t* allocator() { return _type_array.allocator(); }
		error_t	init(type_desc_t const* desc_array, size_t desc_size, size_t queue_size);
		void	clean();
		error_t	acquire(resource_id uid, void* user_ptr);
		void	release(resource_id uid);
		void	update(acquire_result_t* result_array, size_t result_capacity, size_t& result_size);
		error_t	package_add(package_id package, package_uid uid);
		void	package_remove(package_uid uid);

		//error_t	find_by_handle(resource_id& resource, handle_id const* handle, type_id type);
		//error_t	find_by_name(resource_id& uid, char8_t const* name, package_id package);
	};

	

	
} }