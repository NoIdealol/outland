#include "pch.h"
#include "dependency_loader.h"

namespace outland { namespace resource 
{
	//////////////////////////////////////////////////
	// DEPENDENCY LOADER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	dependency_loader_t::dependency_loader_t(allocator_t* allocator)
		: _check_queue(allocator)
		, _check_index(0)
		, _depend_queue(allocator)
		, _depend_index(0)
	{

	}

	//--------------------------------------------------
	error_t	dependency_loader_t::init(size_t queue_capacity)
	{
		error_t err;
		err = _check_queue.reserve(queue_capacity);
		if (err)
			return err;
		err = _depend_queue.reserve(queue_capacity);
		if (err)
			return err;

		return err_ok;
	}

	//--------------------------------------------------
	bool	dependency_loader_t::_request_alive(resource_info_t* it)
	{
		if (it->inst->alive.count)
			return true;

		//chain the error
		it->inst->alive.error = err_aborted;
		it->inst->alive.state = resource_state_failed;

		for (size_t i = 0; i < _depend_index; ++i)
		{
			_free_resource(it->desc->dep_array[i]);
		}

		_depend_queue.pop_front();
		_depend_index = 0;
		_done_resource(it->desc, it->inst);
		return false;
	}

	//--------------------------------------------------
	uint8_t	dependency_loader_t::_request_dependency(resource_info_t* it)
	{
		resource_t* dep = _find_resource(it->desc->dep_array[_depend_index]);
		if (dep)
		{
			if (resource_state_failed == dep->alive.state)
			{
				//chain the error
				it->inst->alive.error = dep->alive.error;
				it->inst->alive.state = resource_state_failed;

				for (size_t i = 0; i < _depend_index; ++i)
				{
					_free_resource(it->desc->dep_array[i]);
				}

				_depend_queue.pop_front();
				_depend_index = 0;
				_done_resource(it->desc, it->inst);
				return 1;
			}
			else
			{
				++dep->alive.count;
				++_depend_index;
				return 0;
			}
		}
		else
		{
			resource_desc_t const* desc = _find_desc(it->desc->dep_array[_depend_index]);
			if (desc)
			{
				//try to load
				if (_load_resource(desc, it->desc->dep_array[_depend_index]))
				{
					++_depend_index;
					return 0;
				}
				else
				{
					return 2;
				}
			}
			else
			{
				it->inst->alive.error = err_not_found;
				it->inst->alive.state = resource_state_failed;

				for (size_t i = 0; i < _depend_index; ++i)
				{
					_free_resource(it->desc->dep_array[i]);
				}

				_depend_queue.pop_front();
				_depend_index = 0;
				_done_resource(it->desc, it->inst);
				return 1;
			}
		}
	}

	//--------------------------------------------------
	bool	dependency_loader_t::_request_dependency_done(resource_info_t* it)
	{
		if (_check_queue.space())
		{
			_check_queue.push_back(*it);
			_depend_queue.pop_front();
			_depend_index = 0;
			return true;
		}
		else
		{
			return false;
		}
	}

	//--------------------------------------------------
	bool	dependency_loader_t::_check_alive(resource_info_t* it)
	{
		if (it->inst->alive.count)
			return true;

		//chain the error
		it->inst->alive.error = err_aborted;
		it->inst->alive.state = resource_state_failed;

		for (size_t i = 0; i < it->desc->dep_size; ++i)
		{
			_free_resource(it->desc->dep_array[i]);
		}

		_check_queue.pop_front();
		_check_index = 0;
		_done_resource(it->desc, it->inst);
		return false;
	}

	//--------------------------------------------------
	uint8_t	dependency_loader_t::_check_dependency(resource_info_t* it)
	{
		resource_t* dep = _find_resource(it->desc->dep_array[_check_index]);
		O_ASSERT(dep);

		//check status
		switch (dep->alive.state)
		{
		case resource_state_failed: //abort
		{
			//chain the error
			it->inst->alive.error = dep->alive.error;
			it->inst->alive.state = resource_state_failed;

			for (size_t i = 0; i < it->desc->dep_size; ++i)
			{
				_free_resource(it->desc->dep_array[i]);
			}

			_check_queue.pop_front();
			_check_index = 0;
			_done_resource(it->desc, it->inst);
			return 1;
		}
		case resource_state_ready:
		{
			++_check_index;
			return 0;
		}
		default: //it is ok
			return 2;
		}
	}

	//--------------------------------------------------
	bool	dependency_loader_t::_check_dependency_done(resource_info_t* it)
	{
		if (_load_data(it->desc, it->inst))
		{
			_check_queue.pop_front();
			_check_index = 0;
			return true;
		}
		else
		{
			return false;
		}
	}

	//--------------------------------------------------
	void dependency_loader_t::update()
	{
		//request dependencies
		while (_depend_queue.size())
		{
			auto* it = &_depend_queue.front();
			if (!_request_alive(it))
				goto _continue_depend_queue;

			while (_depend_index != it->desc->dep_size)
			{
				switch (_request_dependency(it))
				{
				case 0:
					continue;
				case 1:
					goto _continue_depend_queue;
				case 2:
					goto _break_depend_queue;
				default:
					O_ASSERT(false);
				}
			}

			O_ASSERT(_depend_index == it->desc->dep_size);
			if (_request_dependency_done(it))
				goto _continue_depend_queue;
			else
				goto _break_depend_queue;

		_continue_depend_queue:;
		}
	_break_depend_queue:;

		//check dependencies and try load
		while (_check_queue.size())
		{
			auto* it = &_check_queue.front();
			if (!_check_alive(it))
				goto _continue_load_queue;

			while (_check_index != it->desc->dep_size)
			{
				switch (_check_dependency(it))
				{
				case 0:
					continue;
				case 1:
					goto _continue_load_queue;
				case 2:
					goto _break_load_queue;
				default:
					O_ASSERT(false);
				}
			}

			O_ASSERT(_check_index == it->desc->dep_size);
			if (_check_dependency_done(it))
				goto _continue_load_queue;
			else
				goto _break_load_queue;

		_continue_load_queue:;
		}
	_break_load_queue:;

	}

	//--------------------------------------------------
	void	dependency_loader_t::load_dependencies(resource_desc_t const* desc, resource_t* resource)
	{
		O_ASSERT(_depend_queue.space());

		resource_info_t info;
		info.desc = desc;
		info.inst = resource;
		_depend_queue.push_back(info);

		resource->alive.state = resource_state_depending;
	}

	//--------------------------------------------------
	error_t	dependency_loader_t::can_load_dependencies()
	{
		if (0 == _depend_queue.space())
			return err_too_many_requests;

		return err_ok;
	}

} }
