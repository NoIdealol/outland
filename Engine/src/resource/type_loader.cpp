#include "pch.h"
#include "type_loader.h"

namespace outland { namespace resource
{
	//////////////////////////////////////////////////
	// TYPE LOADER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	type_loader_t::type_loader_t(factory_t* factory, memory_info_t const& handle_info, allocator_t* allocator)
		: _factory(factory)
		, _handle_info(handle_info)
		, _create_pool(allocator)
		, _create_array(nullptr)
		, _create_size(0)
	{

	}

	//--------------------------------------------------
	error_t	type_loader_t::init(size_t queue_size)
	{
		error_t err;
		err = _create_pool.reserve(queue_size);
		if (err)
			return err;

		err = u::mem_alloc(_create_pool.allocator(), _create_array, queue_size);
		if (err)
			return err;

		_create_size = queue_size;

		for (size_t i = 0; i < _create_size; ++i)
		{
			_create_pool.push_back(_create_array + i);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	type_loader_t::clean()
	{
		if (_create_array)
		{
			u::mem_free(_create_pool.allocator(), _create_array, _create_size);
			_create_array = nullptr;
			_create_size = 0;
			_create_pool.clear();
		}
	}

	//--------------------------------------------------
	bool	type_loader_t::create(resource_desc_t const* desc, resource_t* resource, void* data, size_t size)
	{
		error_t err;
		if (0 == _create_pool.size())
			return false;

		resource_create_t create;
		create.data_buffer = data;
		create.data_size = size;
		create.dep_array = desc->dep_array;
		create.dep_size = desc->dep_size;
		create.user_ptr = _create_pool.last();

		err = _factory->create(&resource->handle, create);
		switch (err)
		{
		case err_too_many_requests:
			return false;
		case err_ok:
		{
			resource->alive.state = resource_state_creating;
			auto* info = _create_pool.last();
			info->data_buffer = data;
			info->data_size = size;
			info->resource = resource;
			info->desc = desc;
			_create_pool.pop_back();
			return true;
		}
		default:
		{
			resource->alive.error = err;
			resource->alive.state = resource_state_failed;
			for (size_t i = 0; i < desc->dep_size; ++i)
			{
				_free_resource(desc->dep_array[i]);
			}
			_free_data(data, size);
			_done_resource(desc, resource);
			return true;
		}
		}
	}

	//--------------------------------------------------
	bool	type_loader_t::destroy(handle_id const* handle)
	{
		error_t err = _factory->destroy(handle);
		if (err)
		{
			O_ASSERT(err_too_many_requests == err);
			return false;
		}

		return true;
	}

	//--------------------------------------------------
	void	type_loader_t::update()
	{
		create_result_t result_array[32];
		size_t result_size;
		_factory->epoll(result_array, u::array_size(result_array), result_size);

		for (size_t i = 0; i < result_size; ++i)
		{
			auto& result = result_array[i];
			auto* info = static_cast<create_info_t*>(result.user_ptr);

			info->resource->alive.error = result.error;
			_free_data(info->data_buffer, info->data_size);

			if (result.error)
			{
				info->resource->alive.state = resource_state_failed;
				for (size_t i = 0; i < info->desc->dep_size; ++i)
				{
					_free_resource(info->desc->dep_array[i]);
				}
			}
			else
			{
				info->resource->alive.state = resource_state_ready;
				//_handle_map.insert(&info->resource->handle, )
			}

			_done_resource(info->desc, info->resource);
			_create_pool.push_back(info);
		}
	}

} }
