#include "pch.h"
#include "package.h"
#include "manager.h"

namespace outland { namespace resource
{
	enum desc_layout_t
	{
		desc_layout_desc,
		desc_layout_name,
		desc_layout_path,
		desc_layout_dep_array,

		desc_layout_SIZE,
	};

	//--------------------------------------------------
	static void desc_memory_info(char8_t const* name, char8_t const* path, size_t dep_size, memory_info_t& info, size_t (&offsets)[desc_layout_SIZE])
	{
		size_t name_size = u::str_len(name) + 1;
		size_t path_size = u::str_len(path) + 1;

		size_t align_arr[] = { alignof(resource_desc_t), alignof(char8_t) , alignof(char8_t), alignof(resource_id) };
		size_t size_arr[] = { sizeof(resource_desc_t), sizeof(char8_t) * name_size, sizeof(char8_t) * path_size, sizeof(resource_id) * dep_size };
		u::mem_offsets(align_arr, size_arr, offsets, info.align, info.size, desc_layout_SIZE);
	}

	//////////////////////////////////////////////////
	// PACKAGE
	//////////////////////////////////////////////////

	//--------------------------------------------------
	package_t::package_t(package_uid uid, allocator_t* allocator)
		: _desc_map(allocator, decltype(_desc_map)::fnc_hash_t().bind<&resource_hash>(), decltype(_desc_map)::fnc_cmp_t().bind<&resource_cmp>())
		, _name_map(allocator, decltype(_name_map)::fnc_hash_t().bind<uint32_t, &name_hash>(&this->_hash_seed), decltype(_name_map)::fnc_cmp_t().bind<&name_cmp>())
		, _hash_seed(0)
		, _uid_counter(0)
		, _uid(uid)
		, _allocator(allocator)
	{

	}

	//--------------------------------------------------
	package_t::~package_t()
	{
		memory_info_t info;
		size_t offsets[desc_layout_SIZE];

		for (auto* it : _desc_map)
		{
			_name_map.remove(it->name);
			desc_memory_info(it->name, it->path, it->dep_size, info, offsets);
			_allocator->free(const_cast<resource_desc_t*>(it), info.size);
		}

		_desc_map.clear();
	}

	//--------------------------------------------------
	error_t	package_t::find(resource_id uid, resource_desc_t const*& desc)
	{
		return _desc_map.find(uid, desc);
	}

	//--------------------------------------------------
	error_t	package_t::find(char8_t const* name, resource_id& uid)
	{
		return _name_map.find(name, uid);
	}

	//--------------------------------------------------
	error_t	package_t::resource_add(resource_id& resource, package::resource_add_t const& add)
	{
		error_t err;

		auto uid = resource_make_id(add.type, _uid, _uid_counter);
		memory_info_t info;
		size_t offsets[desc_layout_SIZE];
		desc_memory_info(add.name, add.path, add.dep_size, info, offsets);

		void* memory = _allocator->alloc(info.size, info.align);
		if (nullptr == memory)
			return err_out_of_memory;

		byte_t* bytes = (byte_t*)memory;
		auto* desc = (resource_desc_t*)(bytes + offsets[desc_layout_desc]);
		desc->name = (char8_t*)(bytes + offsets[desc_layout_name]);
		desc->path = (char8_t*)(bytes + offsets[desc_layout_path]);
		desc->dep_size = add.dep_size;
		desc->dep_array = (resource_id*)(bytes + offsets[desc_layout_dep_array]);
		
		u::str_copy(desc->name, add.name);
		u::str_copy(desc->path, add.path);

		for (size_t i = 0; i < add.dep_size; ++i)
		{
			err = find(add.dep_array[i], desc->dep_array[i]);
			if (err)
			{
				_allocator->free(memory, info.size);
				return err;
			}
		}

		err = _desc_map.insert(uid, desc);
		if (err)
		{
			_allocator->free(memory, info.size);
			return err;
		}

		err = _name_map.insert(desc->name, uid);
		if (err)
		{
			_desc_map.remove(uid);
			_allocator->free(memory, info.size);
			return err;
		}

		++_uid_counter;
		resource = uid;
		return err_ok;
	}

	//--------------------------------------------------
	error_t	package::create(manager_id manager, package_id& package, create_t const& create)
	{
		error_t err;
		package_t* pack;
		err = u::mem_alloc(create.allocator, pack);
		if (err)
			return err;

		O_CREATE(package_t, pack)(create.uid, create.allocator);
		
		err = manager->package_add(pack, create.uid);
		if (err)
		{
			O_DESTROY(package_t, pack);
			u::mem_free(create.allocator, pack);
			return err;
		}

		package = pack;

		return err_ok;
	}

	//--------------------------------------------------
	void	package::destroy(manager_id manager, package_id package)
	{
		manager->package_remove(package->uid());
		auto* allocator = package->allocator();
		O_DESTROY(package_t, package);
		u::mem_free(allocator, package);
	}

	//--------------------------------------------------
	error_t	package::resource_add(package_id package, resource_id& resource, resource_add_t const& add)
	{
		return package->resource_add(resource, add);
	}

} }
