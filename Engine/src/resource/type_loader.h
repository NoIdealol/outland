#pragma once
#include "resource.h"

namespace outland { namespace resource
{
	//////////////////////////////////////////////////
	// TYPE LOADER
	//////////////////////////////////////////////////

	class type_loader_t
	{
		friend manager_t;

		struct create_info_t
		{
			void*					data_buffer;
			size_t					data_size;
			resource_t*				resource;
			resource_desc_t const*	desc;
		};

	private:
		factory_t*								_factory;
		memory_info_t							_handle_info;

		array<create_info_t*>					_create_pool;
		create_info_t*							_create_array;
		size_t									_create_size;

		free_resource_t							_free_resource;
		free_data_t								_free_data;
		done_resource_t							_done_resource;

	public:
		type_loader_t(factory_t* factory, memory_info_t const& handle_info, allocator_t* allocator);
		error_t	init(size_t queue_size);
		void	clean();

		bool	create(resource_desc_t const* desc, resource_t* resource, void* data, size_t size);
		bool	destroy(handle_id const* handle);
		void	update();
	};

} }
