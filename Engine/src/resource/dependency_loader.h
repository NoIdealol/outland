#pragma once
#include "resource.h"

namespace outland { namespace resource 
{

	//////////////////////////////////////////////////
	// DEPENDENCY LOADER
	//////////////////////////////////////////////////

	class dependency_loader_t
	{
		friend manager_t;

		struct resource_info_t
		{
			resource_desc_t const*	desc;
			resource_t*				inst;
		};

	private:
		deque<resource_info_t>	_check_queue;
		size_t					_check_index;
		deque<resource_info_t>	_depend_queue;
		size_t					_depend_index;

		//external functionality
		find_resource_t		_find_resource;
		find_desc_t			_find_desc;
		load_resource_t		_load_resource;
		free_resource_t		_free_resource;
		done_resource_t		_done_resource;
		load_data_t			_load_data;

	private:
		bool		_request_alive(resource_info_t* it);
		uint8_t		_request_dependency(resource_info_t* it);
		bool		_request_dependency_done(resource_info_t* it);
		bool		_check_alive(resource_info_t* it);
		uint8_t		_check_dependency(resource_info_t* it);
		bool		_check_dependency_done(resource_info_t* it);

	public:
		dependency_loader_t(allocator_t* allocator);
		error_t	init(size_t queue_capacity);
		void	update();
		void	load_dependencies(resource_desc_t const* desc, resource_t* resource);
		error_t	can_load_dependencies();
	};

} }
