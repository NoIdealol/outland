#include "pch.h"
//#include "package.h"
//#include "memory\circular.h"

namespace outland { namespace data
{
	enum type_t : uint32_t
	{
		TYPE_TEXTURE,
		TYPE_MESH,
		TYPE_SHADER,
		TYPE_SOUND,

		TYPE_COUNT,
	};

	struct name_t
	{
		type_t					type;
		char8_t const*			name;
	};

	struct descriptor_t
	{
		name_t					name;
		char8_t const*			path;
	};

	//array/map of resources
	using package_id = class package_t*;
	namespace package
	{
		struct create_t
		{
			allocator_t*	allocator;
		};

		error_t	create(package_id& package, create_t const& create);
		void	destroy(package_id package);
		error_t	parse(package_id package, void* data, size_t size);
		error_t	serialize(package_id package, void* data, size_t capacity, size_t& size);
		error_t	insert(package_id package, descriptor_t const& desc);
		error_t	remove(package_id package, name_t const& name);
		error_t	find(package_id package, name_t const& name, descriptor_t*& desc);
	}

	using loader_id = struct loader_t*;
	namespace loader
	{

	}

	struct register_t
	{

	};


} }
