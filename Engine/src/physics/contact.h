#pragma once
#include "config.h"
#include "shape.h"

namespace outland { namespace shape2d
{
	struct feature_t
	{
		uint16_t	type;
		uint16_t	index;
	};

	struct contact_t
	{
		vector_t	normal;
		vector_t	point;
		scalar_t	depth;
		feature_t	feature1;
		feature_t	feature2;
	};

	struct collision_t
	{
		contact_t	contact_array[2];
		size_t		contact_count;
	};

	struct context_t
	{
		scalar_t	zero_tolerance;
		vector_t	singularity_normal;
	};

	bool collide(
		const context_t* context, collision_t& collision, 
		const transform_t& t1, const circle_t& s1, 
		const transform_t& t2, const circle_t& s2);

	bool collide(
		const context_t* context, collision_t& collision, 
		const transform_t& t1, const circle_t& s1, 
		const transform_t& t2, const capsule_t& s2);

	bool collide(
		const context_t* context, collision_t& collision,
		const transform_t& t1, const circle_t& s1,
		const transform_t& t2, const rect_t& s2);
	
	bool collide(
		const context_t* context, collision_t& collision, 
		const transform_t& t1, const circle_t& s1, 
		const transform_t& t2, const polygon_t& s2);
	//bool collide(const context_t* context, collision_t& collision, const transform_t& t1, const circle_t& s1, const transform_t& t2, const compound_t& s2);

} }