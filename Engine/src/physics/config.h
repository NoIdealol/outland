#pragma once
#include "math\vector2.h"
#include "math\fixed.h"
#include "math\floating.h"

namespace outland { namespace shape2d
{

} }

namespace outland { namespace shape2d
{
	//using scalar_t = float;//fixed_t;
	using scalar_t = fixed_t;
	using vector_t = vector2<scalar_t>;
	using matrix_t = matrix2<scalar_t>;

	struct transform_t
	{
		matrix_t	rot;
		vector_t	pos;
	};

} }