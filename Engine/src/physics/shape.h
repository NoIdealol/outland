#pragma once
#include "config.h"

namespace outland { namespace shape2d
{
	struct circle_t
	{
		scalar_t radius;
	};

	struct capsule_t
	{
		scalar_t radius;
		scalar_t lenght;
	};

	struct rect_t
	{
		vector_t size;
	};

	struct polygon_t //convex
	{
		vector_t*	vertex_array;
		size_t		vertex_count;
	};

	struct compound_t
	{
		transform_t*	transform_array;

		circle_t*		circle_array;
		size_t			circle_count;

		capsule_t*		capsule_array;
		size_t			capsule_count;

		rect_t*			rect_array;
		size_t			rect_count;

		polygon_t*		polygon_array;
		size_t			polygon_count;
	};

} }