#include "pch.h"
#include "contact.h"

namespace outland { namespace shape2d
{
	namespace c
	{
		static const scalar_t scalar_min = scalar_t(-32000);
	}

	bool collide(
		const context_t* context, collision_t& collision, 
		const transform_t& t1, const circle_t& s1, 
		const transform_t& t2, const circle_t& s2)
	{
		//collision testing
		vector_t position_rel = t2.pos - t1.pos;
		scalar_t distance_sq = m::dot(position_rel, position_rel);
		scalar_t radius_sum = s1.radius + s2.radius;
		scalar_t radius_sum_sq = radius_sum * radius_sum;

		if(radius_sum_sq > distance_sq) //radius check
			return false;
		
		//contact calculation
		scalar_t distance = m::len(position_rel);

		collision.contact_count = 1;
		auto& contact = collision.contact_array[0];
		
		if(distance > context->zero_tolerance)
			//can be div by zero
			contact.normal = position_rel / distance; 
		else
			contact.normal = context->singularity_normal;

		contact.depth = radius_sum - distance;
		contact.point = t1.pos + contact.normal * (s1.radius - contact.depth / scalar_t(2));
		
		//there is only one feature on circle
		contact.feature1.type = 0;
		contact.feature1.index = 0;
		contact.feature2.type = 0;
		contact.feature2.index = 0;
		
		return true;
	}

	bool collide(
		const context_t* context, collision_t& collision, 
		const transform_t& t1, const circle_t& s1, 
		const transform_t& t2, const capsule_t& s2)
	{
		//calc point on capsule segment
		vector_t segment1 = m::mul(t2.rot, vector_t(+s2.lenght / scalar_t(2), scalar_t(0))) + t2.pos;
		vector_t segment2 = m::mul(t2.rot, vector_t(-s2.lenght / scalar_t(2), scalar_t(0))) + t2.pos;
		vector_t direction_scaled = segment2 - segment1;
		scalar_t projection_scaled = m::dot(t1.pos - segment1, direction_scaled);
		scalar_t direction_len_sq = m::dot(direction_scaled, direction_scaled);

		//clamp to capsule segment range
		projection_scaled = m::max(projection_scaled, scalar_t(0));
		projection_scaled = m::min(projection_scaled, direction_len_sq);

		//capsule len must be nonzero
		vector_t point = segment1 + (direction_scaled * projection_scaled) / direction_len_sq;

		//collision testing
		vector_t position_rel = point - t1.pos;
		scalar_t distance_sq = m::dot(position_rel, position_rel);
		scalar_t radius_sum = s1.radius + s2.radius;
		scalar_t radius_sum_sq = radius_sum * radius_sum;

		if (radius_sum_sq > distance_sq) //radius check
			return false;

		//contact calculation
		scalar_t distance = m::len(position_rel);

		collision.contact_count = 1;
		auto& contact = collision.contact_array[0];

		if (distance > context->zero_tolerance)
			//can be div by zero
			contact.normal = position_rel / distance;
		else
			contact.normal = context->singularity_normal;

		contact.depth = radius_sum - distance;
		contact.point = t1.pos + contact.normal * (s1.radius - contact.depth / scalar_t(2));

		//there is only one feature on circle and capsule
		contact.feature1.type = 0;
		contact.feature1.index = 0;
		contact.feature2.type = 0;
		contact.feature2.index = 0;

		return true;
	}

	bool collide(
		const context_t* context, collision_t& collision,
		const transform_t& t1, const circle_t& s1,
		const transform_t& t2, const rect_t& s2)
	{
		vector_t vertex_array[4];
		vertex_array[0] = s2.size / scalar_t(2);
		vertex_array[1] = vertex_array[0] - vector_t(scalar_t(0), s2.size[1]);
		vertex_array[2] = vertex_array[0] - s2.size;
		vertex_array[3] = vertex_array[0] - vector_t(s2.size[0], scalar_t(0));

		polygon_t polygon;
		polygon.vertex_array = vertex_array;
		polygon.vertex_count = u::array_size(vertex_array);

		return collide(context, collision, t1, s1, t2, polygon);
	}

	bool collide(
		const context_t* context, collision_t& collision,
		const transform_t& t1, const circle_t& s1,
		const transform_t& t2, const polygon_t& s2)
	{
		//transform to polygon space so we dont need to transform vertices
		const vector_t position_rel = m::mul(m::transp(t2.rot), t1.pos - t2.pos);
		const scalar_t radius_sq = s1.radius * s1.radius;

		//size_t vertex_index = s2.vertex_count;
		scalar_t max_distance_sq = c::scalar_min;
		vector_t max_normal_scaled;
		feature_t max_feature;

		vector_t edge1 = s2.vertex_array[0] - s2.vertex_array[s2.vertex_count - 1];
		vector_t edge2;
		vector_t normal_scaled;
		scalar_t distance_sq;

		size_t i = 0;
		for (i = 0; i < s2.vertex_count - 1; ++i)
		{
			edge2 = s2.vertex_array[i + 1] - s2.vertex_array[i];
		
		_loop:
			//vertex check
			{
				normal_scaled = position_rel - s2.vertex_array[i];
				//voronoi regoin check
				if (m::dot(normal_scaled, edge1) > scalar_t(0) && m::dot(normal_scaled, edge2) < scalar_t(0))
				{
					distance_sq = m::dot(normal_scaled, normal_scaled);
					
					if (distance_sq > radius_sq)
						return false;

					if (distance_sq > max_distance_sq)
					{
						max_distance_sq = distance_sq;
						max_normal_scaled = normal_scaled;
						
						max_feature.type = 0;
						max_feature.index = static_cast<uint16_t>(i);
					}
				}
			}

			//edge check
			{
				//anti clock-wise normal
				normal_scaled = vector_t(-edge2[1], edge2[0]);
				scalar_t plane_d = m::dot(normal_scaled, s2.vertex_array[i]);
				scalar_t center_d = m::dot(normal_scaled, position_rel);

				scalar_t distance_scaled = center_d - plane_d;
				scalar_t scale_sq = m::dot(normal_scaled, normal_scaled);
				//divide first to avoid fixed point overflow
				distance_sq = m::abs(distance_scaled) * (distance_scaled / scale_sq);
				
				if (distance_sq > radius_sq)
					return false;

				if (distance_sq > max_distance_sq)
				{
					max_distance_sq = distance_sq;
					max_normal_scaled = normal_scaled;

					max_feature.type = 1;
					max_feature.index = static_cast<uint16_t>(i);
				}
			}

			//update edge
			edge1 = edge2;
		}
		
		//check if the loop exited for the first time
		if (i < s2.vertex_count)
		{
			//do one more iteration for last vertex/edge
			edge2 = s2.vertex_array[0] - s2.vertex_array[i];
			goto _loop;
		}

		collision.contact_count = 1;
		auto& contact = collision.contact_array[0];

		contact.depth = s1.radius - m::sign(max_distance_sq) * m::sqrt(m::abs(max_distance_sq));
		contact.normal = -m::norm(m::mul(t2.rot, max_normal_scaled));
		contact.point = t1.pos + contact.normal * (s1.radius - contact.depth / scalar_t(2));

		contact.feature1.type = 0;
		contact.feature1.index = 0;
		contact.feature2 = max_feature;

		return true;
	}



} }
