#include "pch.h"
#include "main\main.h"

//#include "..\..\Graphics\src\vulkan\swap_chain.h"
//#include "..\..\Graphics\src\vulkan\library_vk.h"

//#include "..\..\Graphics\lib\module.h"

#include "system\gui\gui.h"
#include "system\application\application.h"
#include "system\application\library.h"

//#include "system\file\file.h"

#include "system\thread\thread.h"
#include "renderer\renderer.h"
#include "..\lib\file_system.h"
#include "..\lib\resource_manager.h"
#include "..\lib\resource_factory.h"

#include "system\debug\output.h"
#include "utility\json.h"
#include "video\resource\buffer_decompressor.h"

namespace outland { namespace os
{
	namespace app
	{
		//extern allocator_t* const	allocator;
		//extern process_id const&	process;
		extern library_id const&	library;
	}

} }

namespace outland
{

	class game_window_t : public os::window_t
	{
	private:
		void on_key_down(os::window_id window, os::hid::usage_t key) final { os::app::exit(0); }
		void on_frame_paint(os::window_id window, os::surface_id surface) const final {}
		void on_paint(os::window_id window, os::surface_id surface) const final { /*os::surface::clear(surface, 0x303030);*/ }
	public:
	};

	class application_t : public os::application_t
	{
		video::renderer_t*		_renderer;
		scratch_allocator_t*	_scratch;

	public:
		application_t(video::renderer_t* renderer, scratch_allocator_t* scratch)
			: _renderer(renderer)
			, _scratch(scratch)
		{
		}

		bool on_idle() final
		{
			_renderer->render_frame(_scratch);
			return false;
		}
	};

	byte_t	scratch_init[1024 * 8];
	byte_t	scratch_buff[128];

	uint8_t fs_thread(file::system_id sys)
	{
		while (true)
		{
			file::system::update(sys);
			os::thread::sleep(10);
		}

		return 0;
	}

	file::result_t wait_for_file(file::queue_id que)
	{
		file::result_t	result_array[16];
		size_t			result_size = 0;

		while (0 == result_size)
		{
			os::thread::sleep(10);
			file::epoll(que, result_array, u::array_size(result_array), result_size);
		}

		O_ASSERT(1 == result_size);
		O_ASSERT(err_ok == result_array[0].error);
		return result_array[0];
	}

	void file_test(allocator_t* allocator)
	{
		error_t err;
		file::system_id sys;
		file::queue_id que;
		file::request_id req;
		memory_info_t mem_info;

		{
			file::system::memory_info(mem_info);
			file::system::create_t create;

			err = file::system::create(sys, create, allocator->alloc(mem_info.size, mem_info.align));
			O_ASSERT(err_ok == err);
		}

		{
			file::queue::create_t create;
			create.queue_capacity = 64;
			file::queue::memory_info(mem_info, create);
			file::queue::create(que, create, allocator->alloc(mem_info.size, mem_info.align));
			err = file::system::queue_insert(sys, que);
			O_ASSERT(err_ok == err);
		}

		{
			file::request::memory_info(mem_info);
			file::request::create(req, allocator->alloc(mem_info.size, mem_info.align));
		}

		{
			os::thread::main_t main;
			main.bind<file::system_t, &fs_thread>(sys);
			os::thread_id thr;
			err = os::thread::create(thr, main, "fs_thread", 1024);
			O_ASSERT(err_ok == err);
		}

		file::result_t res;

		auto path = "C:/Users/Lukas/Desktop/spell queue.txt";
		err = file::open(que, path, file::open_existing, file::access_read, nullptr);
		O_ASSERT(err_ok == err);
		file::flush(que);
		res = wait_for_file(que);
		
		auto file = res.file;
		err = file::get_size(que, file, nullptr);
		O_ASSERT(err_ok == err);
		file::flush(que);
		res = wait_for_file(que);
		auto file_size = res.size;

		err = file::read(que, file, req, 0, allocator->alloc(file_size, 16), file_size, nullptr);
		O_ASSERT(err_ok == err);
		file::flush(que);
		res = wait_for_file(que);

		O_ASSERT(err_ok == err);
	}

	class txt_factory_t : public resource::factory_t
	{
		struct req_t
		{
			resource::handle_id* handle;
			void*				user_ptr;
		};

		array<req_t>		_arr;
		
		
		allocator_t*			_allocator;

	public:
		txt_factory_t(allocator_t* allocator) : _allocator(allocator), _arr(allocator) {}

		error_t	create(resource::handle_id* handle, resource::resource_create_t const& create) final
		{
			void* mem = _allocator->alloc(create.data_size + 1, 1);
			u::mem_clr(mem, create.data_size + 1);
			u::mem_copy(mem, create.data_buffer, create.data_size);
			u::mem_copy(handle, &mem, sizeof(mem));

			req_t req;

			req.user_ptr = create.user_ptr;
			req.handle = mem;
			_arr.push_back(req);
			return err_ok;
		}

		error_t	destroy(resource::handle_id const* handle) final
		{
			return err_ok;
		}

		void	epoll(resource::create_result_t* result_array, size_t result_capacity, size_t& result_size) final
		{
			result_size = 0;
			while (_arr.size() && result_capacity > result_size)
			{
				auto& res = result_array[result_size];
				res.error = err_ok;
				res.user_ptr = _arr.last().user_ptr;
				_arr.pop_back();
				++result_size;
			}
		}
	};

	void resource_test(allocator_t* allocator)
	{
		error_t err;
		file::system_id sys;
		file::queue_id que;
		resource::manager_id man;
		resource::package_id pack;
		resource_id res;
		txt_factory_t factory1 = { allocator };
		txt_factory_t factory2 = { allocator };
		memory_info_t mem_info;
		resource_id res_array[7];
		{
			file::system::memory_info(mem_info);
			file::system::create_t create;

			err = file::system::create(sys, create, allocator->alloc(mem_info.size, mem_info.align));
			O_ASSERT(err_ok == err);
		}

		{
			file::queue::create_t create;
			create.queue_capacity = 64;
			file::queue::memory_info(mem_info, create);
			file::queue::create(que, create, allocator->alloc(mem_info.size, mem_info.align));
			err = file::system::queue_insert(sys, que);
			O_ASSERT(err_ok == err);
		}

		{
			os::thread::main_t main;
			main.bind<file::system_t, &fs_thread>(sys);
			os::thread_id thr;
			err = os::thread::create(thr, main, "fs_thread", 1024);
			O_ASSERT(err_ok == err);
		}

		{
			resource::type_desc_t type_array[2];
			type_array[0].factory = &factory1;
			type_array[0].handle_info.align = alignof(char8_t*);
			type_array[0].handle_info.size = sizeof(char8_t*);
			type_array[0].uid = resource::type_uid(7);
			type_array[1].factory = &factory2;
			type_array[1].handle_info.align = alignof(char8_t*);
			type_array[1].handle_info.size = sizeof(char8_t*);
			type_array[1].uid = resource::type_uid(11);

			resource::manager::create_t create;
			create.file_queue = que;
			create.manager_allocator = allocator;
			create.resource_allocator = allocator;
			create.queue_size = 2;
			create.type_array = type_array;
			create.type_size = u::array_size(type_array);

			err = resource::manager::create(man, create);
			O_ASSERT(err_ok == err);
		}

		{
			resource::package::create_t create;
			create.uid = resource::package_uid(35);
			create.allocator = allocator;

			err = resource::package::create(man, pack, create);
			O_ASSERT(err_ok == err);

			resource::package::resource_add_t add;
			{
				add.name = "MytestFile";
				add.path = "C:/Users/Lukas/Desktop/spell queue.txt";
				add.type = resource::type_uid(7);
				add.dep_array = nullptr;
				add.dep_size = 0;

				err = resource::package::resource_add(pack, res, add);
				O_ASSERT(err_ok == err);
			}

			{
				char8_t const* dep[] = { "MytestFile" };

				add.name = "GDD";
				add.path = "C:/Users/Lukas/Desktop/gdd.txt";
				add.type = resource::type_uid(11);
				add.dep_array = dep;
				add.dep_size = u::array_size(dep);

				err = resource::package::resource_add(pack, res, add);
				O_ASSERT(err_ok == err);

				add.name = "test1";
				add.path = "C:/Users/Lukas/Desktop/data/test1.txt";
				err = resource::package::resource_add(pack, res_array[0], add);
				O_ASSERT(err_ok == err);
				add.name = "test2";
				add.path = "C:/Users/Lukas/Desktop/data/test2.txt";
				err = resource::package::resource_add(pack, res_array[1], add);
				O_ASSERT(err_ok == err);
				add.name = "test3";
				add.path = "C:/Users/Lukas/Desktop/data/test3.txt";
				err = resource::package::resource_add(pack, res_array[2], add);
				O_ASSERT(err_ok == err);
				add.name = "test4";
				add.path = "C:/Users/Lukas/Desktop/data/test4.txt";
				err = resource::package::resource_add(pack, res_array[3], add);
				O_ASSERT(err_ok == err);
				add.name = "test5";
				add.path = "C:/Users/Lukas/Desktop/data/test5.txt";
				err = resource::package::resource_add(pack, res_array[4], add);
				O_ASSERT(err_ok == err);
				add.name = "test6";
				add.path = "C:/Users/Lukas/Desktop/data/test6.txt";
				err = resource::package::resource_add(pack, res_array[5], add);
				O_ASSERT(err_ok == err);
				add.name = "test7";
				add.path = "C:/Users/Lukas/Desktop/data/test7.txt";
				err = resource::package::resource_add(pack, res_array[6], add);
				O_ASSERT(err_ok == err);
				
			}
		}

		err = resource::acquire(man, res, (void*)16);
		O_ASSERT(err_ok == err);



		resource::acquire_result_t	result_array[32];
		size_t						result_size = 0;
		while (0 == result_size)
		{
			os::thread::sleep(10);
			resource::update(man, result_array, u::array_size(result_array), result_size);
		}

		int a = 1;
	}

	void json_test(allocator_t* allocator)
	{
		char8_t str[] = "[ { \"asd\" : true }, false, [ +14.987e3 ], [null, true] ]";
		json::document_t doc;
		auto err = json::parse_document(doc, str, u::str_len(str), nullptr, allocator, allocator);
		O_ASSERT(err_ok == err);

		auto& memb = doc.value.array.element_array[0].object.member_array[0];

		float val = json::to_float(doc.value.array.element_array[2].array.element_array[0].number);

		size_t el_size = doc.value.array.element_size;
		auto& ar = doc.value.array.element_array[3];
		for (auto& ita : ar.array)
		{
			auto type = ita.type;
			//json::member_size(it_e.object);
		}
	}


	uint8_t main(main_args_t const& main_args)
	{
		
		//json_test(main_args.allocator);

		scratch_allocator_t* scratch;

		error_t err;

		{
			scratch_allocator::create_t create_info;
			create_info.init_buffer = scratch_init;
			create_info.init_size = sizeof(scratch_init);
			create_info.page_size = sizeof(scratch_init);
			create_info.page_source = main_args.allocator;

			scratch_allocator::create(scratch, create_info, scratch_buff);
		}

		/*{
			"{\"header\":{\"version\":0,\"type\":\"vertex\",\"size\":2},\"desc\":[\"float\" , \"float\" ] } [6, 3, 6, 8]";
			char const* json = "{ \"header\" : { \"version\" : 0 , \"type\" : \"vertex\" , \"size\" : 2 } , \"desc\" : [ \"float\" , \"float\" ] } [6, 3, 6, 8]";
			error_t err;
			video::buffer_info_t buffer_info;
			void* content;
			err = video::buffer_decompress(buffer_info, content, json, u::str_len(json), scratch);
			O_ASSERT(err_ok == err);
		}*/


		err = os::gui::create();
		O_ASSERT(err_ok == err);

		os::window_id os_wnd;
		game_window_t game_wnd;

		{
			os::rect_t rc;
			rc.base.x = 200;
			rc.base.y = 200;
			rc.size.x = 800;
			rc.size.y = 600;

			err = os::window::calc_window_rect(rc, os::window::null, os::window::st_standard);
			O_ASSERT(err_ok == err);
			err = os::window::create(os_wnd, &game_wnd, rc, "game window", os::window::null, os::window::st_standard);
			O_ASSERT(err_ok == err);
		}

		video::renderer_t renderer;
		renderer.init(&os_wnd, (void*)&os::app::library, main_args.allocator, scratch);

		application_t application(&renderer, scratch);
		uint8_t ret = os::app::run(&application, main_args.cmd_count, main_args.cmd_line);

		

		os::gui::destroy();

		scratch_allocator::destroy(scratch);

		return 0;
	}


}
