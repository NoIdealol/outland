#include "pch.h"
#include "thread\triple_buffer_queue.h"
#include "system\thread\atomic.h"

namespace outland
{
	struct triple_buffer_queue_t
	{
		os::atomic_uint32_id	swapper;
		uint32_t				consumer;
		uint32_t				producer;
	};

	//--------------------------------------------------
	void	triple_buffer_queue::memory_info(memory_info_t& memory_info)
	{
		memory_info_t atom_mem;
		os::atomic::memory_info_uint32(atom_mem);

		size_t align_arr[] = { alignof(triple_buffer_queue_t), atom_mem.align };
		size_t size_arr[] = { sizeof(triple_buffer_queue_t), atom_mem.size };
		size_t offset_arr[2];
		u::mem_offsets(align_arr, size_arr, offset_arr, memory_info.align, memory_info.size, 2);
	}

	//--------------------------------------------------
	void	triple_buffer_queue::create(triple_buffer_queue_id& queue, create_info_t const& info, void* memory)
	{
		memory_info_t atom_mem;
		os::atomic::memory_info_uint32(atom_mem);

		size_t align_arr[] = { alignof(triple_buffer_queue_t), atom_mem.align };
		size_t size_arr[] = { sizeof(triple_buffer_queue_t), atom_mem.size };
		size_t offset_arr[2];

		memory_info_t memory_info;
		u::mem_offsets(align_arr, size_arr, offset_arr, memory_info.align, memory_info.size, 2);

		queue = reinterpret_cast<triple_buffer_queue_t*>(memory);
		os::atomic::create(queue->swapper, 0x0FffFFff & info.initial_unused_index, reinterpret_cast<byte_t*>(memory) + offset_arr[1]);
		queue->consumer = 0x0FffFFff & info.initial_consumer_index;
		queue->producer = 0x0FffFFff & info.initial_producer_index;
	}

	//--------------------------------------------------
	void	triple_buffer_queue::destroy(triple_buffer_queue_id queue)
	{
		os::atomic::destroy(queue->swapper);
	}

	//--------------------------------------------------
	void	triple_buffer_queue::produce(triple_buffer_queue_id queue, size_t& index)
	{
		queue->producer = 0x0FffFFff & os::atomic::exchange(queue->swapper, queue->producer | 0x10000000, os::memory::order_flush);
		index = queue->producer;
	}

	//--------------------------------------------------
	bool	triple_buffer_queue::consume(triple_buffer_queue_id queue, size_t& index)
	{
		uint32_t swapper = os::atomic::read(queue->swapper, os::memory::order_evict);
		if (0x10000000 & swapper)
		{
			queue->consumer = 0x0FffFFff & os::atomic::exchange(queue->swapper, queue->consumer, os::memory::order_evict);
			index = queue->consumer;
			return true;
		}
		else
		{
			index = queue->consumer;
			return false;
		}
	}
}
