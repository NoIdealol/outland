#pragma once
#include "..\lib\thread\lockfree_queue.h"
#include "system\thread\atomic.h"
namespace outland
{
	namespace lockfree_queue
	{
		struct producer_t
		{
			uint32_t	producing;
			uint32_t	produced;
			uint32_t	consumed;
		};

		struct consumer_t
		{
			uint32_t	produced;
			uint32_t	consuming;
			uint32_t	consumed;
		};

		enum memory_layout_t : size_t
		{
			memory_layout_queue,

			//producer write
			memory_layout_producer,
			memory_layout_atom_produced,
			memory_layout_element_data,

			//consumer write
			memory_layout_consumer,
			memory_layout_atom_consumer,

			memory_layout_size,
		};

		enum memory_cache_t : size_t
		{
			memory_cache_0 = memory_layout_queue,
			memory_cache_1 = memory_layout_producer,
			memory_cache_2 = memory_layout_consumer,
		};

		void memory_offsets(create_t const& create, memory_info_t& info, size_t(&offset_arr)[memory_layout_size]);
	}

	struct lockfree_queue_t
	{
		lockfree_queue::producer_t*		producer;			//producer read write
		lockfree_queue::consumer_t*		consumer;			//consumer read write
		os::atomic_uint32_id			atom_produced;		//producer write consumer read
		os::atomic_uint32_id			atom_consumed;		//consumer write producer read
		uint32_t						element_capacity;	//const
		size_t							element_size;		//const
		byte_t*							element_data;		//producer write consumer read
	};

}