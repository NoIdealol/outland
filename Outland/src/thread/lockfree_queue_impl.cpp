#include "pch.h"
#include "lockfree_queue_impl.h"
#include "math\integer.h"

namespace outland
{

	//--------------------------------------------------
	void	lockfree_queue::memory_offsets(create_t const& create, memory_info_t& info, size_t(&offset_arr)[memory_layout_size])
	{
		size_t cache_size = 64;

		size_t align_arr[memory_layout_size];
		size_t size_arr[memory_layout_size];

		memory_info_t mem_atom;
		os::atomic::memory_info_uint32(mem_atom);

		align_arr[memory_layout_queue] = alignof(lockfree_queue_t);
		size_arr[memory_layout_queue] = sizeof(lockfree_queue_t);
		align_arr[memory_layout_producer] = alignof(producer_t);
		size_arr[memory_layout_producer] = sizeof(producer_t);
		align_arr[memory_layout_atom_produced] = mem_atom.align;
		size_arr[memory_layout_atom_produced] = mem_atom.size;
		align_arr[memory_layout_element_data] = create.element_info.align;
		size_arr[memory_layout_element_data] = create.element_info.size * create.element_size;
		align_arr[memory_layout_consumer] = alignof(consumer_t);
		size_arr[memory_layout_consumer] = sizeof(consumer_t);
		align_arr[memory_layout_atom_consumer] = mem_atom.align;
		size_arr[memory_layout_atom_consumer] = mem_atom.size;

		align_arr[memory_cache_0] = m::max(align_arr[memory_cache_0], cache_size);
		align_arr[memory_cache_1] = m::max(align_arr[memory_cache_1], cache_size);
		align_arr[memory_cache_2] = m::max(align_arr[memory_cache_2], cache_size);

		u::mem_offsets(align_arr, size_arr, offset_arr, info.align, info.size, memory_layout_size, cache_size);
	}

	//--------------------------------------------------
	void	lockfree_queue::memory_info(memory_info_t& memory_info, create_t const& create)
	{
		size_t offset_arr[memory_layout_size];
		memory_offsets(create, memory_info, offset_arr);
	}

	//--------------------------------------------------
	void	lockfree_queue::create(lockfree_queue_id& queue, create_t const& create, void* memory)
	{
		memory_info_t info;
		size_t offset_arr[memory_layout_size];
		memory_offsets(create, info, offset_arr);

		byte_t* bytes = (byte_t*)memory;
		queue = O_CREATE(lockfree_queue_t, bytes + offset_arr[memory_layout_queue]);
		queue->producer = O_CREATE(producer_t, bytes + offset_arr[memory_layout_producer]);
		os::atomic::create(queue->atom_produced, 0, bytes + offset_arr[memory_layout_atom_produced]);
		queue->element_data = bytes + offset_arr[memory_layout_element_data];
		queue->element_capacity = (uint32_t)create.element_size;
		queue->consumer = O_CREATE(consumer_t, bytes + offset_arr[memory_layout_consumer]);
		os::atomic::create(queue->atom_consumed, 0, bytes + offset_arr[memory_layout_atom_consumer]);
		queue->element_size = create.element_info.size;
	}

	//--------------------------------------------------
	void	lockfree_queue::destroy(lockfree_queue_id queue)
	{
		O_DESTROY(lockfree_queue_t, queue);
		O_DESTROY(producer_t, queue->producer);
		os::atomic::destroy(queue->atom_produced);
		O_DESTROY(consumer_t, queue->consumer);
		os::atomic::destroy(queue->atom_consumed);
	}

	//--------------------------------------------------
	size_t	lockfree_queue::producer_capacity(lockfree_queue_id queue)
	{
		return queue->element_capacity - (queue->producer->producing - queue->producer->consumed);
	}

	//--------------------------------------------------
	void*	lockfree_queue::produce(lockfree_queue_id queue)
	{
		O_ASSERT(producer_capacity(queue));

		void* ret = queue->element_data + (queue->producer->producing % queue->element_capacity) * queue->element_size;
		++queue->producer->producing;
		return ret;
	}

	//--------------------------------------------------
	void	lockfree_queue::producer_flush(lockfree_queue_id queue)
	{
		if (queue->producer->producing != queue->producer->produced)
		{
			os::atomic::write(queue->atom_produced, queue->producer->producing, os::memory::order_flush);
			queue->producer->produced = queue->producer->producing;
		}
	}

	//--------------------------------------------------
	void	lockfree_queue::producer_evict(lockfree_queue_id queue)
	{
		queue->producer->consumed = os::atomic::read(queue->atom_consumed, os::memory::order_evict);
	}

	//--------------------------------------------------
	size_t	lockfree_queue::consumer_size(lockfree_queue_id queue)
	{
		return queue->consumer->produced - queue->consumer->consuming;
	}

	//--------------------------------------------------
	void*	lockfree_queue::consume(lockfree_queue_id queue)
	{
		O_ASSERT(consumer_size(queue));

		void* ret = queue->element_data + (queue->consumer->consuming % queue->element_capacity) * queue->element_size;
		++queue->consumer->consuming;
		return ret;
	}

	//--------------------------------------------------
	void	lockfree_queue::consumer_flush(lockfree_queue_id queue)
	{
		if (queue->consumer->consuming != queue->consumer->consumed)
		{
			//ensure reading is done
			os::atomic::write(queue->atom_consumed, queue->consumer->consuming, os::memory::order_flush);
			queue->consumer->consumed = queue->consumer->consuming;
		}
	}

	//--------------------------------------------------
	void	lockfree_queue::consumer_evict(lockfree_queue_id queue)
	{
		queue->consumer->produced = os::atomic::read(queue->atom_produced, os::memory::order_evict);
	}

}