#include "pch.h"
#include "memory.h"
#include "math\integer.h"
#include <memory.h>

namespace outland { namespace u
{
	//////////////////////////////////////////////////
	// ALIGNMENT
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void* mem_align(void* ptr, size_t alignment)
	{
		uintptr_t pointer = reinterpret_cast<uintptr_t>(ptr);
		uintptr_t addition = alignment - 1; //alignment is power of 2, invert bits to get mask
		pointer = (pointer + addition) & (~addition); //& mask
		O_ASSERT(pointer % alignment == 0); //invalid alignment, not power of 2
		return reinterpret_cast<void*>(pointer);
	}

	//--------------------------------------------------
	void const* mem_align(void const* ptr, size_t alignment)
	{
		uintptr_t pointer = reinterpret_cast<uintptr_t>(ptr);
		uintptr_t addition = alignment - 1; //alignment is power of 2, invert bits to get mask
		pointer = (pointer + addition) & (~addition); //& mask
		O_ASSERT(pointer % alignment == 0); //invalid alignment, not power of 2
		return reinterpret_cast<void const*>(pointer);
	}

	//--------------------------------------------------
	uintptr_t mem_align(uintptr_t ptr, size_t alignment)
	{
		uintptr_t addition = alignment - 1; //alignment is power of 2, invert bits to get mask
		ptr = (ptr + addition) & (~addition); //& mask
		O_ASSERT(ptr % alignment == 0); //invalid alignment, not power of 2
		return ptr;
	}

	//--------------------------------------------------
	bool mem_is_aligned(void const* ptr, size_t alignment)
	{
		uintptr_t pointer = reinterpret_cast<uintptr_t>(ptr);
		uintptr_t mask = alignment - 1; //alignment is power of 2
		O_ASSERT(reinterpret_cast<uintptr_t>(ptr) % alignment == (pointer & mask));
		return (pointer & mask) == 0;
	}

	//--------------------------------------------------
	size_t mem_align_offset(void const* ptr, size_t alignment)
	{
		uintptr_t addition = alignment - 1; //alignment is power of 2, invert bits to get mask
		return (alignment - (reinterpret_cast<uintptr_t>(ptr) & addition)) & (addition);
	}

	//--------------------------------------------------
	void mem_offsets(
		size_t const* alignment_array,
		size_t const* size_array,
		size_t* offset_array,
		size_t& max_alignment,
		size_t& mem_size,
		size_t array_size,
		size_t alignment
	)
	{
		size_t align_max = alignment;
		size_t size_sum = 0;
		for (size_t i = 0; i < array_size; ++i)
		{
			size_t aligner = alignment_array[i] - 1; //alignment is power of 2, invert bits to get mask
			size_sum = (size_sum + aligner) & (~aligner); //& mask				 
			offset_array[i] = size_sum;
			size_sum += size_array[i];
			align_max = m::max(alignment_array[i], align_max);
		}

		max_alignment = align_max;
		mem_size = size_sum;
	}

	//--------------------------------------------------
	size_t size_align(size_t size, size_t alignment)
	{
		size_t addition = alignment - 1; //alignment is power of 2, invert bits to get mask
		size = (size + addition) & (~addition); //& mask
		O_ASSERT(size % alignment == 0); //invalid alignment, not power of 2
		return size;
	}

	//////////////////////////////////////////////////
	// MEMORY
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void mem_copy(void* dst, void const* src, size_t size)
	{
		memcpy(dst, src, size);
	}

	//--------------------------------------------------
	void mem_move(void* dst, void const* src, size_t size)
	{
		memmove(dst, src, size);
	}

	//--------------------------------------------------
	void mem_clr(void* dst, size_t size)
	{
		memset(dst, 0, size);
	}

	//--------------------------------------------------
	bool mem_cmp(void const* mem1, void const* mem2, size_t size)
	{
		return 0 == memcmp(mem1, mem2, size);
	}

	//--------------------------------------------------
	bool mem_cmp32(void const* dst, size_t size, uint32_t value)
	{
		byte_t const* aligner = reinterpret_cast<byte_t const*>(dst);
		dst = u::mem_align(dst, alignof(uint32_t));

		while ((aligner != dst) && (size >= sizeof(uint8_t)) && (*aligner == (value & 0xFF)))
		{
			size -= sizeof(uint8_t);
			++aligner;
			value = (value >> 8) | (value << 24);
		}
		if (aligner != dst)
			return false;

		while ((size >= sizeof(uint32_t)) && (*reinterpret_cast<uint32_t const*>(dst) == value))
		{
			size -= sizeof(uint32_t);
			dst = reinterpret_cast<byte_t const*>(dst) + sizeof(uint32_t);
		}
		if (size >= sizeof(uint32_t))
			return false;

		while ((size >= sizeof(uint8_t)) && (*reinterpret_cast<uint8_t const*>(dst) == (value & 0xFF)))
		{
			size -= sizeof(uint8_t);
			value >>= 8;
			dst = reinterpret_cast<byte_t const*>(dst) + sizeof(uint8_t);
		}

		return 0 == size;
	}

	//--------------------------------------------------
	void mem_set8(void* dst, size_t size, uint8_t value)
	{
		memset(dst, value, size);
	}

	//--------------------------------------------------
	void mem_set32(void* dst, size_t size, uint32_t value)
	{
		byte_t* aligner = reinterpret_cast<byte_t*>(dst);
		dst = u::mem_align(dst, alignof(uint32_t));

		while ((aligner != dst) && (size >= sizeof(uint8_t)))
		{
			size -= sizeof(uint8_t);
			*aligner = value & 0xFF;
			++aligner;
			value = (value >> 8) | (value << 24);
		}

		while (size >= sizeof(uint32_t))
		{
			size -= sizeof(uint32_t);
			*reinterpret_cast<uint32_t*>(dst) = value;
			dst = reinterpret_cast<byte_t*>(dst) + sizeof(uint32_t);
		}

		while (size >= sizeof(uint8_t))
		{
			size -= sizeof(uint8_t);
			*reinterpret_cast<uint8_t*>(dst) = value & 0xFF;
			value >>= 8;
			dst = reinterpret_cast<byte_t*>(dst) + sizeof(uint8_t);
		}
	}

} }