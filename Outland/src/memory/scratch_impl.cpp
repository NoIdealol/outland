#include "pch.h"
#include "scratch_impl.h"
#include "math\integer.h"

namespace outland
{
	//--------------------------------------------------
	void	scratch_allocator::memory_info(memory_info_t& memory_info)
	{
		memory_info.align = alignof(scratch_allocator_impl_t);
		memory_info.size = sizeof(scratch_allocator_impl_t);
	}

	//--------------------------------------------------
	void	scratch_allocator::create(scratch_allocator_t*& scratch, create_t const& create, void* memory)
	{
		scratch = O_CREATE(scratch_allocator_impl_t, memory)(create.page_source, create.page_size, create.init_buffer, create.init_size);
	}

	//--------------------------------------------------
	void	scratch_allocator::destroy(scratch_allocator_t* scratch)
	{
		O_DESTROY(scratch_allocator_impl_t, scratch);
	}

	//--------------------------------------------------
	scratch_allocator_impl_t::scratch_allocator_impl_t(allocator_t* allocator, size_t page_size)
		: scratch_allocator_impl_t(allocator, page_size, nullptr, 0)
	{

	}

	//--------------------------------------------------
	scratch_allocator_impl_t::scratch_allocator_impl_t(allocator_t* allocator, size_t page_size, void* buffer, size_t buffer_size)
		: _allocator(allocator)
		, _head(buffer)
		, _page(&_begin)
		, _page_size(page_size)
	{
		_begin._next = nullptr;
		_begin._begin = buffer;
		_begin._end = reinterpret_cast<byte_t*>(buffer) + buffer_size;
	}

	//--------------------------------------------------
	scratch_allocator_impl_t::~scratch_allocator_impl_t()
	{
		scratch_page_t* page = _begin._next;
		while (page)
		{
			scratch_page_t* next = page->_next;

			{
				using mem_t = mem<scratch_page_t, byte_t[]>;

				mem_t::array_size_t sizes;
				sizes.at<1>() = reinterpret_cast<byte_t*>(page->_end) - reinterpret_cast<byte_t*>(page->_begin);

				mem_t::alloc_t allocation;
				allocation.at<0>() = page;
				allocation.at<1>() = reinterpret_cast<byte_t*>(page->_begin);
				
				mem<scratch_page_t, byte_t[]>::free(_allocator, allocation, sizes);
			}
			page = next;
		}
	}

	//--------------------------------------------------
	void	scratch_allocator_impl_t::push_frame(scratch_frame_t& frame)
	{
		frame.page = _page;
		frame.head = _head;
	}

	//--------------------------------------------------
	void	scratch_allocator_impl_t::pop_frame(scratch_frame_t const& frame)
	{
		_page = frame.page;
		_head = frame.head;
	}

	//--------------------------------------------------
	void*	scratch_allocator_impl_t::alloc(size_t size, size_t align, size_t* capacity)
	{

		if ((reinterpret_cast<uintptr_t>(_page->_end) - reinterpret_cast<uintptr_t>(_head)) > 
			(u::mem_align_offset(_head, align) + size))
		{
			//we have memory
			void* memory = u::mem_align(_head, align);

			_head = reinterpret_cast<byte_t*>(memory) + size;
			
			if (capacity)
				*capacity = size;

			return memory;
		}
		else
		{
			//get more memory

			if (nullptr == _page->_next)
			{
				//get a new page, we are at the end
				goto _alloc_page;
			}
			else
			{
				if ((reinterpret_cast<uintptr_t>(_page->_next->_end) - reinterpret_cast<uintptr_t>(_page->_next->_begin)) > 
					(u::mem_align_offset(_page->_next->_begin, align) + size))
				{
					//next page is sufficient
					void* memory = u::mem_align(_page->_next->_begin, align);

					_head = reinterpret_cast<byte_t*>(memory) + size;
					_page = _page->_next;
					
					if (capacity)
						*capacity = size;

					return memory;
				}
				else
				{
					//try alloc a new page and insert it as next
					goto _alloc_page;
				}
			}
		}

		_alloc_page:
		{
			//get a new page, we are at the end
			using mem_t = mem<scratch_page_t, byte_t[]>;

			mem_t::array_size_t sizes;
			sizes.at<1>() = m::max(_page_size, size + align);

			mem_t::alloc_t allocation = mem<scratch_page_t, byte_t[]>::alloc(_allocator, sizes);

			if (nullptr == allocation.at<0>())
				return nullptr;

			allocation.at<0>()->_next = _page->_next;
			allocation.at<0>()->_begin = allocation.at<1>();
			allocation.at<0>()->_end = allocation.at<1>() + sizes.at<1>();

			_page->_next = allocation.at<0>();

			void* memory = u::mem_align(allocation.at<1>(), align);

			_head = reinterpret_cast<byte_t*>(memory) + size;
			_page = _page->_next;

			if (capacity)
				*capacity = size;

			return memory;
		}
	}

	//--------------------------------------------------
	void	scratch_allocator_impl_t::free(void* memory, size_t size)
	{
		static_cast<void>(memory);
		static_cast<void>(size);
	}

}