#pragma once
#include "memory\circular.h"

namespace outland
{

	class circular_allocator_t : public allocator_t
	{
		//ALLOCATION LAYOUT
		//HEADER
		//1 used bit, 7 pading bits (not including this byte)
		//padding(optional), used for alignment, last byte has pading copy
		//DATA
		//user data or size_t(free size) if block is free

		enum : size_t
		{
			_min_alloc_size = sizeof(size_t),
			_max_align = 1 << 7,
		};

		enum : byte_t
		{
			_header_used_bit = 1 << 7,
			_header_padding_mask = _header_used_bit - 1,
		};

		byte_t*	_buffer;
		size_t	_size;

		size_t	_head;
		size_t	_tail;

	private:
		size_t	_capacity();
		size_t	_head_offset();
		size_t	_tail_offset();
		void	_collect();

	public:
		circular_allocator_t(void* buffer, size_t size);
		~circular_allocator_t();
		void*	alloc(size_t size, size_t align, size_t* capacity) final;
		void	free(void* memory, size_t size) final;
	};

}