#include "pch.h"
#include "memory\block_pow2.h"
#include "math\integer.h"

#if O_DBG_BLOCK_POW2_ALLOCATOR_T
#define O_ASSERT_BLOCK_POW2_ALLOCATOR_T(condition) if(!(condition)) O_BREAKPOINT
#else
#define O_ASSERT_BLOCK_POW2_ALLOCATOR_T(condition)
#endif

namespace outland
{
	namespace g
	{
		enum : uint32_t
		{
			clearing_pattern_32 = 0xbad0beef,
			overwrite_guard_size = sizeof(void*),
		};
	}

	//--------------------------------------------------
	block_pow2_allocator_t::block_pow2_allocator_t(allocator_t* allocator, size_t page_power, size_t page_count)
		: _chunk_count(0)
		, _chunk_page_count(m::max(page_count, size_t(1)))
		, _page_power(0)
		, _page_size(0)
		, _page_list(nullptr)
		, _allocator(allocator)
	{
		static_assert(sizeof(block_header_t) <= min_alloc_size, "minimal block size too small");
		static_assert(min_page_power <= max_page_power, "invalid page power range");
		static_assert(sizeof(page_header_t) <= min_page_size, "minimal page size too small");
		static_assert(0 < max_grow_count, "cannot allocate anything");
		static_assert(0 == (sizeof(block_header_t) % sizeof(uint32_t)), "clearing pattern misaligned");

		_page_power = m::min(m::max(page_power, (size_t)min_page_power), (size_t)max_page_power);
		_page_size = size_t(1) << _page_power;

		for (auto& pool : _block_pool_array)
		{
			pool.begin = nullptr;

			#if O_DBG_BLOCK_POW2_ALLOCATOR_T
			{
				pool.dbg_alloc_count = 0;
				pool.dbg_page_count = 0;
			}
			#endif
		}

		for (auto& chunk : _chunk_metadata_array)
		{
			chunk.page_begin = nullptr;
			chunk.page_end = nullptr;
			chunk.page_meta_array = nullptr;
		}
	}

	//--------------------------------------------------
	block_pow2_allocator_t::~block_pow2_allocator_t()
	{
		using chunk_mem_t = mem<byte_t[], page_metadata_t[]>;

		#if O_DBG_BLOCK_POW2_ALLOCATOR_T
		{
			for (auto& pool : _block_pool_array)
			{
				O_ASSERT_BLOCK_POW2_ALLOCATOR_T(0 == pool.dbg_alloc_count);
				O_ASSERT_BLOCK_POW2_ALLOCATOR_T(0 == pool.dbg_page_count);
			}
		}
		#endif
		
		while (_chunk_count)
		{
			--_chunk_count;
			chunk_metadata_t& chunk = _chunk_metadata_array[_chunk_count];

			size_t chunk_size = (chunk.page_end - chunk.page_begin);
			size_t meta_count = (chunk.page_end - chunk.page_begin) >> _page_power;

			chunk_mem_t::array_size_t arr_size;
			arr_size.at<0>() = chunk_size;
			arr_size.at<1>() = meta_count;

			chunk_mem_t::alloc_t alloc;
			alloc.at<0>() = chunk.page_begin;
			alloc.at<1>() = chunk.page_meta_array;

			chunk_mem_t::free(_allocator, alloc, arr_size);
		}
	}

	//--------------------------------------------------
	block_pow2_allocator_t::chunk_metadata_t*
		block_pow2_allocator_t::_find_chunk_meta(void* memory)
	{
		for (auto& chunk : _chunk_metadata_array)
		{
			if (chunk.page_begin <= reinterpret_cast<byte_t*>(memory) && chunk.page_end > reinterpret_cast<byte_t*>(memory))
				return &chunk;
		}

		return nullptr;
	}

	//--------------------------------------------------
	size_t	block_pow2_allocator_t::_find_page_index(chunk_metadata_t* chunk, void* memory)
	{
		return (reinterpret_cast<byte_t*>(memory) - chunk->page_begin) >> _page_power;
	}

	//--------------------------------------------------
	block_pow2_allocator_t::page_metadata_t*
		block_pow2_allocator_t::_find_page_meta(chunk_metadata_t* chunk, size_t page_index)
	{
		return chunk->page_meta_array + page_index;
	}

	//--------------------------------------------------
	byte_t*	block_pow2_allocator_t::_find_page_data(chunk_metadata_t* chunk, size_t page_index)
	{
		return chunk->page_begin + (page_index << _page_power);
	}

	//--------------------------------------------------
	void	block_pow2_allocator_t::_push_block(block_pool_t* pool, page_metadata_t* page_meta, byte_t* page_data, void* memory)
	{
		block_header_t* block = reinterpret_cast<block_header_t*>(memory);
		block_header_t** prev = nullptr;

		//adjust meta
		{
			if ((static_cast<size_t>(page_meta->alloc_count) << page_meta->block_power) == _page_size) //if is empty
			{
				prev = &pool->begin;
				page_meta->first = _find_block_index(page_meta, page_data, memory);
				page_meta->last = page_meta->first;
			}
			else
			{
				prev = _find_block(page_meta, page_data, page_meta->first)->prev;
				page_meta->first = _find_block_index(page_meta, page_data, memory);
			}

			--page_meta->alloc_count;
		}

		//insert
		{
			block->next = *prev;
			block->prev = prev;
			if (nullptr != block->next)
				block->next->prev = &block->next;
			*prev = block;
		}
	}

	//--------------------------------------------------
	void*	block_pow2_allocator_t::_pop_block(block_pool_t* pool, page_metadata_t* page_meta, byte_t* page_data)
	{
		block_header_t* block = pool->begin;

		//adjust meta
		{
			if (page_meta->first != page_meta->last) //if not last
			{
				page_meta->first = _find_block_index(page_meta, page_data, block->next);
			}

			++page_meta->alloc_count;
		}

		//remove
		{
			if (nullptr != block->next)
				block->next->prev = &pool->begin;
			pool->begin = block->next;
		}

		return block;
	}

	//--------------------------------------------------
	block_pow2_allocator_t::block_size_t		
		block_pow2_allocator_t::_find_block_index(page_metadata_t* page_meta, byte_t* page_data, void* memory)
	{
		return static_cast<block_size_t>((reinterpret_cast<byte_t*>(memory) - page_data) >> page_meta->block_power);
	}

	//--------------------------------------------------
	block_pow2_allocator_t::block_header_t*		
		block_pow2_allocator_t::_find_block(page_metadata_t* page_meta, byte_t* page_data, block_size_t index)
	{
		return reinterpret_cast<block_header_t*>(page_data + (static_cast<size_t>(index) << page_meta->block_power));
	}

	//--------------------------------------------------
	void	block_pow2_allocator_t::_make_blocks(block_pool_t* pool, size_t block_power)
	{
		page_metadata_t* page_meta = _page_list->page_meta;
		byte_t* page_data = reinterpret_cast<byte_t*>(_page_list);
		_page_list = _page_list->next;

		size_t block_count = _page_size >> block_power;
		size_t const block_size = size_t(1) << block_power;

		page_meta->block_power = static_cast<block_size_t>(block_power);
		page_meta->alloc_count = 0;
		page_meta->first = 0;
		page_meta->last = static_cast<block_size_t>(block_count - 1);

		block_header_t* begin = reinterpret_cast<block_header_t*>(page_data);
		block_header_t* block = begin;

		#if O_DBG_BLOCK_POW2_ALLOCATOR_T
		{
			//clear the memory after header
			u::mem_set32(block + 1, block_size - sizeof(block_header_t), g::clearing_pattern_32);
		}
		#endif

		while (--block_count)
		{
			page_data += block_size;
			block->next = reinterpret_cast<block_header_t*>(page_data);
			block->next->prev = &block->next;
			block = block->next;

			#if O_DBG_BLOCK_POW2_ALLOCATOR_T
			{
				//clear the memory after header
				u::mem_set32(block + 1, block_size - sizeof(block_header_t), g::clearing_pattern_32);
			}
			#endif
		}

		//insert
		{
			block->next = pool->begin;
			if (nullptr != block->next)
				block->next->prev = &block->next;
			begin->prev = &pool->begin;
			pool->begin = begin;
		}

		#if O_DBG_BLOCK_POW2_ALLOCATOR_T
		{
			++(pool->dbg_page_count);
		}
		#endif
	}

	//--------------------------------------------------
	void	block_pow2_allocator_t::_take_blocks(block_pool_t* pool, page_metadata_t* page_meta, byte_t* page_data)
	{
		page_header_t* page = reinterpret_cast<page_header_t*>(page_data);

		block_header_t* first = _find_block(page_meta, page_data, page_meta->first);
		block_header_t* last = _find_block(page_meta, page_data, page_meta->last);

		//remove
		{
			*first->prev = last->next;
			if (nullptr != last->next)
				last->next->prev = first->prev;
		}

		page->page_meta = page_meta;
		page->next = _page_list;
		_page_list = page;

		#if O_DBG_BLOCK_POW2_ALLOCATOR_T
		{
			--(pool->dbg_page_count);
		}
		#endif
	}

	//--------------------------------------------------
	void	block_pow2_allocator_t::_make_pages()
	{
		using chunk_mem_t = mem<byte_t[], page_metadata_t[]>;

		if (max_grow_count == _chunk_count)
			return;

		size_t chunk_size = _chunk_page_count << _page_power;
		size_t meta_count = _chunk_page_count;
		_chunk_page_count <<= 1;

		chunk_mem_t::array_size_t arr_size;
		arr_size.at<0>() = chunk_size;
		arr_size.at<1>() = meta_count;

		chunk_mem_t::alloc_t alloc = chunk_mem_t::alloc(max_alignment, _allocator, arr_size);
		if (nullptr == alloc.at<0>())
			return;

		chunk_metadata_t& chunk_meta = _chunk_metadata_array[_chunk_count];
		++_chunk_count;

		chunk_meta.page_begin = alloc.at<0>();
		chunk_meta.page_end = alloc.at<0>() + chunk_size;
		chunk_meta.page_meta_array = alloc.at<1>();

		page_header_t* first = reinterpret_cast<page_header_t*>(chunk_meta.page_begin);
		page_header_t* last = reinterpret_cast<page_header_t*>(chunk_meta.page_end - _page_size);

		page_header_t* page = first;
		page_metadata_t* meta = chunk_meta.page_meta_array;

		while (page != last)
		{
			page->page_meta = meta;
			page->next = reinterpret_cast<page_header_t*>(reinterpret_cast<byte_t*>(page) + _page_size);
			
			page = page->next;
			++meta;
		}
		page->page_meta = meta;

		last->next = _page_list;
		_page_list = first;
	}

	//--------------------------------------------------
	void*	block_pow2_allocator_t::alloc(size_t size, size_t align, size_t* capacity)
	{
		if (max_alignment < align)
			return nullptr;

		#if O_DBG_BLOCK_POW2_ALLOCATOR_T
		{
			//add out of bounds guard buffer
			size += g::overwrite_guard_size;
		}
		#endif

		size = m::max(size, align); //size must be equal or greater than alignment

		if (max_alloc_size < size)
			return nullptr;


		size = m::to_pow2(size, min_alloc_power);
		size_t size_power = m::bit_index(size);
		size_t pool_index = size_power - min_alloc_power;
		block_pool_t* pool = &(_block_pool_array[pool_index]);

		//ensure pool blocks
		if (nullptr == pool->begin)
		{
			//ensure pages
			if (nullptr == _page_list)
			{
				_make_pages();

				if (nullptr == _page_list)
					return nullptr;
			}

			_make_blocks(pool, size_power);
		}

		chunk_metadata_t*	chunk		= _find_chunk_meta(pool->begin);
		size_t				page_index	= _find_page_index(chunk, pool->begin);
		page_metadata_t*	page_meta	= _find_page_meta(chunk, page_index);
		byte_t*				page_data	= _find_page_data(chunk, page_index);

		void* memory = _pop_block(pool, page_meta, page_data);

		#if O_DBG_BLOCK_POW2_ALLOCATOR_T
		{
			++(pool->dbg_alloc_count);
			//dont give out the out of bounds buffer
			size -= g::overwrite_guard_size;
			//clear the header that was used by the allocator
			u::mem_set32(memory, sizeof(block_header_t), g::clearing_pattern_32);
			//check if the memory was tampered with while free
			O_ASSERT_BLOCK_POW2_ALLOCATOR_T(u::mem_cmp32(
				reinterpret_cast<byte_t*>(memory) + sizeof(block_header_t), 
				(size_t(1) << size_power) - sizeof(block_header_t),
				g::clearing_pattern_32));
		}
		#endif

		if (capacity)
			*capacity = size;
		return memory;
	}

	//--------------------------------------------------
	void	block_pow2_allocator_t::free(void* memory, size_t size)
	{
		chunk_metadata_t*	chunk = _find_chunk_meta(memory);
		if (nullptr == chunk)
			return; //err invalid memory

		size_t				page_index = _find_page_index(chunk, memory);
		page_metadata_t*	page_meta = _find_page_meta(chunk, page_index);
		byte_t*				page_data = _find_page_data(chunk, page_index);

		size_t size_power = page_meta->block_power;
		size_t pool_index = size_power - min_alloc_power;
		block_pool_t* pool = &(_block_pool_array[pool_index]);

		#if O_DBG_BLOCK_POW2_ALLOCATOR_T
		{
			size_t const block_size = size_t(1) << size_power;
			size_t const pattern_offset = size % sizeof(uint32_t);
			uint32_t const pattern_check = (g::clearing_pattern_32 >> (pattern_offset * 8)) | (g::clearing_pattern_32 << ((sizeof(uint32_t) - pattern_offset) * 8));

			--(pool->dbg_alloc_count);
			//check if the correct size was returned
			O_ASSERT_BLOCK_POW2_ALLOCATOR_T(block_size >= (size + g::overwrite_guard_size));
			//clear the memory that was used by the user
			u::mem_set32(memory, size, g::clearing_pattern_32);
			//check out of bounds buffer
			O_ASSERT_BLOCK_POW2_ALLOCATOR_T(u::mem_cmp32(reinterpret_cast<byte_t*>(memory) + size, block_size - size, pattern_check));
		}
		#endif

		_push_block(pool, page_meta, page_data, memory);

		if (0 == page_meta->alloc_count)
			_take_blocks(pool, page_meta, page_data);
	}

}