#include "pch.h"
#include "circular_impl.h"
#include "math\integer.h"

namespace outland
{

	//--------------------------------------------------
	void		circular_allocator::get_memory_info(memory_info_t& memory_info)
	{
		memory_info.align = alignof(circular_allocator_t);
		memory_info.size = sizeof(circular_allocator_t);
	}

	//--------------------------------------------------
	error_t		circular_allocator::create(allocator_t*& circular, create_info_t const& create_info, void* memory)
	{
		if (create_info.size != m::to_pow2(create_info.size))
			return err_invalid_parameter;

		circular = O_CREATE(circular_allocator_t, memory)(create_info.buffer, create_info.size);
		return err_ok;
	}

	//--------------------------------------------------
	void		circular_allocator::destroy(allocator_t* circular)
	{
		O_DESTROY(circular_allocator_t, circular);
	}

	//--------------------------------------------------
	size_t circular_allocator_t::_capacity()
	{
		return _size - (_head - _tail);
	}

	//--------------------------------------------------
	size_t circular_allocator_t::_head_offset()
	{
		return _head & (_size - 1);
	}

	//--------------------------------------------------
	size_t circular_allocator_t::_tail_offset()
	{
		return _tail & (_size - 1);
	}

	//--------------------------------------------------
	void	circular_allocator_t::_collect()
	{
		while (_tail != _head)
		{
			size_t header = _tail_offset();

			if ((header + _min_alloc_size + 1) > _size)
			{
				_tail += _size - header;
				continue;
			}

			if (_buffer[header] & _header_used_bit)
				return;

			byte_t padding = _buffer[header] & _header_padding_mask;
			byte_t* data = _buffer + header + (padding + 1);
			size_t size;
			u::mem_copy(&size, data, sizeof(size_t));

			_tail += padding + 1;
			_tail += size;
		}
	}

	//--------------------------------------------------
	circular_allocator_t::circular_allocator_t(void* buffer, size_t size)
		: _buffer((byte_t*)buffer)
		, _size(size)
		, _head(0)
		, _tail(0)
	{
	}

	//--------------------------------------------------
	circular_allocator_t::~circular_allocator_t()
	{
		O_ASSERT(_head == _tail);
	}

	//--------------------------------------------------
	void*	circular_allocator_t::alloc(size_t size, size_t align, size_t* capacity)
	{
		O_ASSERT(align);
		size = m::max(size, (size_t)_min_alloc_size);
		if (_max_align < align)
		{
			O_ASSERT(false);
			return nullptr;
		}

		size_t header = _head_offset();
		size_t data = u::mem_align_offset(_buffer + header, align) + header;
		if (header == data)
			data += align;
		size_t next = data + size;

		//do we have total size?
		if (_capacity() < (next - header))
			return nullptr;

		//do we wrap?
		if (next > _size)
		{
			size_t wrap_next = u::mem_align_offset(_buffer, align);
			if (0 == wrap_next)
				wrap_next += align;
			wrap_next += size;
			size_t wrap_size = _size - header;

			if (_capacity() < (wrap_next + wrap_size))
				return nullptr;

			//insert dummy wrap allocation
			if ((header + _min_alloc_size + 1) <= _size)
			{
				_buffer[header] = 0;
				size_t dummy_size = _size - (header + 1);
				u::mem_copy(_buffer + header + 1, &dummy_size, sizeof(size_t));
			}

			_head += _size - header;

			header = _head_offset();
			O_ASSERT(0 == header);
			data = u::mem_align_offset(_buffer, align);
			if (header == data)
				data += align;
			next = data + size;
			O_ASSERT(next == wrap_next);
		}

		byte_t padding = byte_t(data - header - 1);
		_buffer[header] = _header_used_bit | padding;
		if (padding)
		{
			u::mem_clr(_buffer + header + 1, padding - 1);
			_buffer[header + padding] = padding;
		}

		if (capacity)
		{
			*capacity = next - data;
		}

		_head += next - header;

		return _buffer + data;
	}

	//--------------------------------------------------
	void	circular_allocator_t::free(void* memory, size_t size)
	{
		size = m::max(size, (size_t)_min_alloc_size);

		byte_t* mem = reinterpret_cast<byte_t*>(memory);
		byte_t padding = *(mem - 1) & _header_padding_mask;
		byte_t* header = mem - (padding + 1);
		u::mem_copy(memory, &size, sizeof(size_t));
		*header = *header & _header_padding_mask;

		_collect();
	}

}