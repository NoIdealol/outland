#pragma once
#include "memory\scratch.h"

namespace outland
{
	struct scratch_page_t
	{
		scratch_page_t*	_next;
		void*			_begin;
		void*			_end;
	};

	class scratch_allocator_impl_t : public scratch_allocator_t
	{
	private:
		allocator_t*	_allocator;
		size_t			_page_size;

		scratch_page_t	_begin;
		void*			_head;
		scratch_page_t*	_page;

	public:
		scratch_allocator_impl_t(allocator_t* allocator, size_t page_size);
		scratch_allocator_impl_t(allocator_t* allocator, size_t page_size, void* buffer, size_t buffer_size);
		scratch_allocator_impl_t(scratch_allocator_t const&) = delete;
		scratch_allocator_impl_t(scratch_allocator_t&&) = delete;
		~scratch_allocator_impl_t();

		void	push_frame(scratch_frame_t& frame) final;
		void	pop_frame(scratch_frame_t const& frame) final;

		void*	alloc(size_t size, size_t align, size_t* capacity) final;
		void	free(void* memory, size_t size) final;
	};

}
