#include "pch.h"
#include "format\parse.h"

namespace outland { namespace f
{
	//--------------------------------------------------
	uint32_t parse_uint32_d(char8_t const*& str)
	{
		uint32_t value = 0;
		char8_t const* it = str;

		while ((*it >= '0') && (*it <= '9'))
		{
			value = value * 10 + (*it - '0');
			++it;
		}

		str = it;
		return value;
	}

} }