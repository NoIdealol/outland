#include "pch.h"
#include "format\print.h"
#include "format\parse.h"

namespace outland { namespace f
{
	//////////////////////////////////////////////////
	// TYPES
	//////////////////////////////////////////////////

	using flags_t = size_t;
	enum : flags_t
	{
		f_index = 1 << 0,
		f_sign = 1 << 1,
		f_decorate = 1 << 2,
		f_left = 1 << 3,
		f_width = 1 << 4,
		f_whole = 1 << 5,
		f_decimal = 1 << 6,
		f_format = 1 << 7,
	};

	struct format_t
	{
		flags_t	flags;
		size_t	index;
		size_t	width;
		size_t	whole;
		size_t	decimal;
		char8_t	sign;
		char8_t	format;
	};

	using parse_format_t = error_t(*)(char8_t const*& str, format_t& f);

	//////////////////////////////////////////////////
	// FUNCTIONS
	//////////////////////////////////////////////////

	error_t parse_format_(char8_t const*& str, format_t& f);
	error_t parse_bad_data(char8_t const*& str, format_t& f) { return err_bad_data; }

	parse_format_t parse_format_index(char8_t const*& str, format_t& f);
	parse_format_t parse_format_decimal(char8_t const*& str, format_t& f);
	parse_format_t parse_format_left(char8_t const*& str, format_t& f);
	parse_format_t parse_format_decorate(char8_t const*& str, format_t& f);
	parse_format_t parse_format_sign(char8_t const*& str, format_t& f);
	parse_format_t parse_format_width_or_whole(char8_t const*& str, format_t& f);

	error_t print_variant(string8_t& stream, format_t& format, variant_t const& variant);
	error_t print_fixed(string8_t& stream, format_t& format, fixed_t value);
	error_t print_pointer(string8_t& stream, format_t& format, void const* ptr);
	error_t print_string(string8_t& stream, format_t& format, char8_t const* string);
	error_t print_sint(string8_t& stream, format_t& format, int64_t value);
	error_t print_uint(string8_t& stream, format_t& format, uint64_t value);
	
	error_t print_digits(
		char8_t format,
		uint64_t value,
		char8_t* buffer,
		size_t& size);
	error_t print_prefix(
		char8_t format,
		char8_t* buffer,
		size_t& written);
	error_t print_prefixed_whole(
		string8_t& stream,
		format_t& format,
		char8_t const* digit_buffer,
		size_t digit_size,
		char8_t const* prefix_buffer,
		size_t prefix_size);

	//////////////////////////////////////////////////
	// IMPLEMENTATION
	//////////////////////////////////////////////////

	//////////////////////////////////////////////////
	// PARSING FORMAT
	//////////////////////////////////////////////////

	//--------------------------------------------------
	parse_format_t parse_format_index(char8_t const*& str, format_t& f)
	{
		if (f_index <= f.flags)
			return &parse_bad_data;

		char8_t const* it = str;
		++it; //skip '['

		f.index = f::parse_uint32_d(it);

		if (']' != *it)
			return &parse_bad_data;

		++it;
		str = it;
		f.flags |= f_index;
		return &parse_format_;
	}

	//--------------------------------------------------
	parse_format_t parse_format_decimal(char8_t const*& str, format_t& f)
	{
		if (f_decimal <= f.flags)
			return &parse_bad_data;

		char8_t const* it = str;
		++it; //skip '.'

		f.decimal = f::parse_uint32_d(it);

		str = it;

		f.flags |= f_decimal;
		return &parse_format_;
	}

	//--------------------------------------------------
	parse_format_t parse_format_left(char8_t const*& str, format_t& f)
	{
		if (f_left <= f.flags)
			return &parse_bad_data;

		++str;

		f.flags |= f_left;
		return &parse_format_;
	}

	//--------------------------------------------------
	parse_format_t parse_format_decorate(char8_t const*& str, format_t& f)
	{
		if (f_decorate <= f.flags)
			return &parse_bad_data;

		++str;

		f.flags |= f_decorate;
		return &parse_format_;
	}

	//--------------------------------------------------
	parse_format_t parse_format_sign(char8_t const*& str, format_t& f)
	{
		if (f_sign <= f.flags)
			return &parse_bad_data;

		f.sign = *str;
		++str;

		f.flags |= f_sign;
		return &parse_format_;
	}

	//--------------------------------------------------
	parse_format_t parse_format_width_or_whole(char8_t const*& str, format_t& f)
	{
		uint32_t value = f::parse_uint32_d(str);
		if (':' == *str)
		{
			if (f_width <= f.flags)
				return &parse_bad_data;

			++str;
			f.width = value;
			f.flags |= f_width;
		}
		else
		{
			if (f_whole <= f.flags)
				return &parse_bad_data;

			f.whole = value;
			f.flags |= f_whole;
		}

		return &parse_format_;
	}

	//--------------------------------------------------
	error_t parse_format_(char8_t const*& str, format_t& f)
	{
		parse_format_t parse;

		switch (*str)
		{
		case '[':
		{
			parse = parse_format_index(str, f);
			break;
		}
		case '-':
		{
			parse = parse_format_left(str, f);
			break;
		}
		case '#':
		{
			parse = parse_format_decorate(str, f);
			break;
		}
		case '+':
		case ' ':
		{
			parse = parse_format_sign(str, f);
			break;
		}
		case '.':
		{
			parse = parse_format_decimal(str, f);
			break;
		}
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		{
			parse = parse_format_width_or_whole(str, f);
			break;
		}
		case 'd':
		case 'b':
		case 'o':
		case 'x':
		case 'X':
		case 'p':
		case 's':
		{
			f.format = *str;
			++str;
			return err_ok;
		}
		case 'f':
			return err_not_implemented;
		default:
			return err_bad_data;
		}

		return parse(str, f);
	}

	//////////////////////////////////////////////////
	// PRINT
	//////////////////////////////////////////////////

	//--------------------------------------------------
	error_t print_va(string8_t& stream, char8_t const* string, variant_t* args, size_t arg_count)
	{
		error_t err;
		char8_t const* begin;
		size_t arg = 0;

		while (true)
		{
			begin = string;

			//find formating
			while ('\0' != *string && '%' != *string)
				++string;

			//print normal string to output
			err = stream.append(begin, string);
			if (err)
				return err;

			//check formating
			switch (*string)
			{
			case '\0':
				return err_ok;
			case '%':
			{
				++string;
				if ('%' == *string)
				{

					err = stream.append('%');
					if (err)
						return err;

					++string;
				}
				else
				{
					format_t format = { 0 };
					format.index = arg;
					err = parse_format_(string, format);
					if (err)
						return err;

					if (format.index >= arg_count)
						return err_bad_data;

					err = print_variant(stream, format, args[format.index]);
					if (err)
						return err;

					++arg;
				}

				break;
			}
			default:
				return err_unknown;
			}
		}
	}

	//--------------------------------------------------
	error_t print_prefixed_whole(
		string8_t& stream,
		format_t& format,
		char8_t const* digit_buffer,
		size_t digit_size,
		char8_t const* prefix_buffer,
		size_t prefix_size)
	{
		error_t err;

		if (0 == (format.flags & f_whole))
			format.whole = 1;

		size_t zero_padding = m::max(format.whole, digit_size) - digit_size;
		size_t num_size = prefix_size + zero_padding + digit_size;
		size_t space_padding = m::max(format.width, num_size) - num_size;

		err = stream.resize(stream.size() + num_size + space_padding);
		if (err)
			return err;

		char8_t* out = stream.end() - (num_size + space_padding);

		//print space padding
		if (format.flags & f_left)
		{
			u::mem_set8(out + num_size, space_padding, ' ');
		}
		else
		{
			u::mem_set8(out, space_padding, ' ');
			out += space_padding;
		}

		//print decoration
		u::mem_copy(out, prefix_buffer, prefix_size);
		out += prefix_size;

		//print zero padding
		u::mem_set8(out, zero_padding, '0');
		out += zero_padding;

		//print digits
		u::mem_copy(out, digit_buffer, digit_size);

		return err_ok;
	}

	//--------------------------------------------------
	error_t print_prefix(
		char8_t format,
		char8_t* buffer,
		size_t& written)
	{
		switch (format)
		{
		case 'X':
		case 'x':
		{
			buffer[0] = '0';
			buffer[1] = 'x';
			written += 2;
			break;
		}
		case 'o':
		{
			buffer[0] = '0';
			buffer[1] = 'o';
			written += 2;
			break;
		}
		case 'b':
		{
			buffer[0] = '0';
			buffer[1] = 'b';
			written += 2;
			break;
		}
		case 'd':
		{
			break;
		}
		default:
			return err_bad_data;
		}

		return err_ok;
	}

	error_t print_digits(
		char8_t format,
		uint64_t value,
		char8_t* buffer,
		size_t& size)
	{
		static char8_t const table_x[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		static char8_t const table_X[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		switch (format)
		{
		case 'X':
		{
			while (value)
			{
				buffer[--size] = table_X[value & 0xF];
				value >>= 4;
			}
			break;
		}
		case 'x':
		{
			while (value)
			{
				buffer[--size] = table_x[value & 0xF];
				value >>= 4;
			}
			break;
		}
		case 'o':
		{
			while (value)
			{
				buffer[--size] = table_X[value & 0x7];
				value >>= 3;
			}
			break;
		}
		case 'b':
		{
			while (value)
			{
				buffer[--size] = table_X[value & 0x1];
				value >>= 1;
			}
			break;
		}
		case 'd':
		{
			while (value)
			{
				buffer[--size] = table_X[value % 10];
				value /= 10;
			}

			break;
		}
		default:
			return err_bad_data;
		}

		return err_ok;
	}

	//--------------------------------------------------
	error_t print_uint(string8_t& stream, format_t& format, uint64_t value)
	{
		error_t err;

		char8_t	digits_buffer[32];
		size_t	digits_size = u::array_size(digits_buffer);
		char8_t prefix_buffer[4];
		size_t	prefix_size = 0;

		if (format.flags & f_decorate)
		{
			err = print_prefix(format.format, prefix_buffer, prefix_size);
		
			if (err)
				return err;
		}

		err = print_digits(
			format.format, 
			value, 
			digits_buffer, 
			digits_size);

		if (err)
			return err;

		return print_prefixed_whole(
			stream, 
			format, 
			digits_buffer + digits_size, 
			u::array_size(digits_buffer) - digits_size, 
			prefix_buffer,
			prefix_size);
	}

	//--------------------------------------------------
	error_t print_sint(string8_t& stream, format_t& format, int64_t value)
	{
		error_t err;

		char8_t	digits_buffer[32];
		size_t	digits_size = u::array_size(digits_buffer);
		char8_t prefix_buffer[4];
		size_t	prefix_size = 0;

		uint64_t uvalue;

		//correct value and add sign decoration
		if (value < 0)
		{
			uvalue = -value;

			prefix_buffer[prefix_size++] = '-';
		}
		else
		{
			uvalue = value;

			if (format.flags & f_sign)
			{
				switch (format.sign)
				{
				case '+':
					prefix_buffer[prefix_size++] = '+';
					break;
				case ' ':
					prefix_buffer[prefix_size++] = ' ';
					break;
				default:
					return err_unknown;
				}
			}
		}

		if (format.flags & f_decorate)
		{
			err = print_prefix(format.format, prefix_buffer, prefix_size);

			if (err)
				return err;
		}

		err = print_digits(
			format.format,
			uvalue,
			digits_buffer,
			digits_size);

		if (err)
			return err;

		return print_prefixed_whole(
			stream,
			format,
			digits_buffer + digits_size,
			u::array_size(digits_buffer) - digits_size,
			prefix_buffer,
			prefix_size);
	}

	//--------------------------------------------------
	error_t print_string(string8_t& stream, format_t& format, char8_t const* string)
	{
		error_t err;

		if ('s' != format.format)
			return err_bad_data;

		size_t char_count = 0;
		if (0 == (format.flags & f_whole))
			format.whole = c::max_uint32;
		
		while (string[char_count] && (char_count < format.whole))
			++char_count;

		size_t space_padding = m::max(char_count, format.width) - char_count;

		err = stream.resize(stream.size() + char_count + space_padding);
		if (err)
			return err;

		char8_t* out = stream.end() - (char_count + space_padding);

		//print space padding
		if (format.flags & f_left)
		{
			u::mem_set8(out + char_count, space_padding, ' ');
		}
		else
		{
			u::mem_set8(out, space_padding, ' ');
			out += space_padding;
		}

		u::mem_copy(out, string, char_count * sizeof(char8_t));

		return err_ok;
	}

	//--------------------------------------------------
	error_t print_pointer(string8_t& stream, format_t& format, void const* ptr)
	{
		if ('p' != format.format)
			return err_bad_data;

		format.format = 'x';
		format.flags |= f_whole;
		format.whole = sizeof(ptr) * 2;

		return print_uint(stream, format, reinterpret_cast<uintptr_t>(ptr));
	}

	//--------------------------------------------------
	error_t print_fixed(string8_t& stream, format_t& format, fixed_t value)
	{
		error_t err;

		char8_t	digits_buffer[32];
		size_t	digits_size = u::array_size(digits_buffer);
		char8_t prefix_buffer[4];
		size_t	prefix_size = 0;

		int16_t uwhole;
		uint64_t ufrac;

		//correct value and add sign decoration
		if (value < fixed_t(0))
		{
			uwhole = -value;
			ufrac = m::decimal(-value);

			prefix_buffer[prefix_size++] = '-';
		}
		else
		{
			uwhole = value;
			ufrac = m::decimal(value);

			if (format.flags & f_sign)
			{
				switch (format.sign)
				{
				case '+':
					prefix_buffer[prefix_size++] = '+';
					break;
				case ' ':
					prefix_buffer[prefix_size++] = ' ';
					break;
				default:
					return err_unknown;
				}
			}
		}

		//print prefix
		if (format.flags & f_decorate)
		{
			err = print_prefix(format.format, prefix_buffer, prefix_size);

			if (err)
				return err;
		}
	
		if (format.flags & f_decimal)
			format.decimal = m::min(format.decimal, size_t(12));
		else
			format.decimal = 4;

		//print frac
		{
			uint64_t base;
			switch (format.format)
			{
			case 'X':
			case 'x':
				base = 16;
				break;
			case 'o':
				base = 8;
				break;
			case 'b':
				base = 2;
				break;
			case 'd':
				base = 10;
				break;
			default:
				return err_bad_data;
			}

			for (size_t i = format.decimal; i > 0; --i)
				ufrac *= base;

			ufrac /= (uint64_t(1) << 16);

			err = print_digits(format.format, ufrac, digits_buffer, digits_size);

			if (err)
				return err;

			//pad frac with zeros
			size_t frac_digits = u::array_size(digits_buffer) - digits_size;
			while (frac_digits < format.decimal)
			{
				digits_buffer[--digits_size] = '0';
				++frac_digits;
			}
		}

		//print point
		{
			digits_buffer[--digits_size] = '.';
		}

		//print whole
		{
			err = print_digits(format.format, uwhole, digits_buffer, digits_size);

			if (err)
				return err;
		}

		//hack whole zero padding
		format.whole += format.decimal + 1;

		return print_prefixed_whole(
			stream,
			format,
			digits_buffer + digits_size,
			u::array_size(digits_buffer) - digits_size,
			prefix_buffer,
			prefix_size);
	}

	//--------------------------------------------------
	error_t print_variant(string8_t& stream, format_t& format, variant_t const& variant)
	{
		switch (variant.type())
		{
		case variant_t::t_uint32:
		{
			return print_uint(stream, format, variant.get_uint32());
		}
		case variant_t::t_uint64:
		{
			return print_uint(stream, format, variant.get_uint64());
		}
		case variant_t::t_int32:
		{
			return print_sint(stream, format, variant.get_int32());
		}
		case variant_t::t_int64:
		{
			return print_sint(stream, format, variant.get_int64());
		}
		case variant_t::t_pointer:
		{
			return print_pointer(stream, format, variant.get_pointer());
		}
		case variant_t::t_string8:
		{
			return print_string(stream, format, variant.get_string8());
		}
		case variant_t::t_fixed:
		{
			return print_fixed(stream, format, variant.get_fixed());
		}
		case variant_t::t_float32:
			return err_not_implemented;
		default:
			return err_bad_data;
		}
	}

} }