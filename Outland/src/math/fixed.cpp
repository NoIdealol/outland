#include "pch.h"
#include "math\fixed.h"
#include "math\integer.h"

namespace outland { 
	
	namespace c
	{
		enum : int32_t
		{
			pi = 205887,		//bit value of pi
			e = 178145,			//bit value of e
			one = 0x00010000,	//bit value of 1
		};
	}

namespace m
{

	static const uint64_t DEBRUJIN64 = 0x0218A392CD3D5DBFL;
	static const uint32_t INV_SQRT[64] =
	{
		3611622602,
		2553802833,
		1805811301,
		319225354,
		1276901416,
		39903169,
		225726412,
		4987896,
		902905650,
		623487,
		28215801,
		220435,
		159612677,
		27554,
		3526975,
		3444,
		638450708,
		9975792,
		440871,
		6888,
		19951584,
		430,
		155871,
		215,
		112863206,
		77935,
		19483,
		26,
		2493948,
		107,
		2435,
		9,
		1,
		451452825,
		56431603,
		7053950,
		881743,
		311743,
		38967,
		4870,
		14107900,
		9741,
		608,
		304,
		110217,
		38,
		152,
		13,
		1,
		79806338,
		1246974,
		55108,
		13777,
		861,
		53,
		19,
		2,
		1763487,
		1217,
		76,
		3,
		1722,
		4,
		6,
	};
	static const uint32_t SQRT[64] =
	{
		1,
		1,
		2,
		13,
		3,
		107,
		19,
		861,
		4,
		6888,
		152,
		19483,
		26,
		155871,
		1217,
		1246974,
		6,
		430,
		9741,
		623487,
		215,
		9975792,
		27554,
		19951584,
		38,
		55108,
		220435,
		159612677,
		1722,
		39903169,
		1763487,
		451452825,
		3611622602,
		9,
		76,
		608,
		4870,
		13777,
		110217,
		881743,
		304,
		440871,
		7053950,
		14107900,
		38967,
		112863206,
		28215801,
		319225354,
		2553802833,
		53,
		3444,
		77935,
		311743,
		4987896,
		79806338,
		225726412,
		1805811301,
		2435,
		3526975,
		56431603,
		1276901416,
		2493948,
		902905650,
		638450708
	};

	static const uint32_t PRE_INV_SQRT[64] =
	{
		15057090,
		15057090,
		10646970,
		4346607,
		8693215,
		1536757,
		3764272,
		543325,
		7528545,
		192094,
		1330871,
		117633,
		3073515,
		41589,
		470534,
		14704,
		6147031,
		768378,
		166358,
		20794,
		1086651,
		5198,
		96047,
		3676,
		2661742,
		67915,
		33957,
		1299,
		384189,
		2599,
		12005,
		750,
		265,
		5323485,
		1882136,
		665435,
		235267,
		135831,
		48023,
		16978,
		941068,
		24011,
		6002,
		4244,
		83179,
		1500,
		3001,
		919,
		324,
		2173303,
		271662,
		58816,
		29408,
		7352,
		1838,
		1061,
		375,
		332717,
		8489,
		2122,
		459,
		10397,
		530,
		649,
	};

	//--------------------------------------------------------------------------------
	fixed_t	abs(fixed_t value)
	{
		value._data = m::abs(value._data);
		return value;
	}

	//--------------------------------------------------------------------------------
	fixed_t	sign(fixed_t value)
	{
		return (static_cast<int32_t>(value._data > 0) - static_cast<int32_t>(value._data < 0));
	}

	//--------------------------------------------------------------------------------
	fixed_t fast_inv_sqrt(fixed_t value)
	{
		/*//table generation for precomputed invSqrt
		size_t testSQRT[64];

		uint64_t DeBruijnArray[64];
		for (int x = 0; x < 64; ++x)
		{
		uint64_t v = (uint64_t)(1UL) << x;
		uint64_t triggeringNumber = ( ((uint64_t)(1) << (x >> 1)) + (((uint64_t)(1) << (x >> 1)) >> 1) * (x & 1) );
		DeBruijnArray[(v*debruijn64) >> 58] = triggeringNumber << PRECISION;
		}
		for (int x = 0; x < 64; ++x)
		testSQRT[x] = (size_t)((double)((uint64_t)(1U) << 32) / ::sqrt((double)(DeBruijnArray[x]) * ::sqrt(1.5414)));
		*/
		//*
		uint32_t v = value._data;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v = (v + 1) >> 1;
		uint64_t data64 = v;
		data64 = data64 * (data64 + (v & (value._data << 1)));
		data64 = PRE_INV_SQRT[(data64 * DEBRUJIN64) >> 58];

		value._data = static_cast<int32_t>(((((3 << fixed_t::precision) - ((data64 * data64 * value._data) >> (fixed_t::precision << 1))) * data64) >> (fixed_t::precision + 1)));
		return value;
	}

	//--------------------------------------------------------------------------------
	fixed_t fast_sqrt(fixed_t value)
	{
		uint64_t num = value._data;
		num = num << fixed_t::precision;

		uint64_t v = num;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v |= v >> 32;
		v = (v + 1) >> 1;

		uint32_t y = SQRT[(v*DEBRUJIN64) >> 58];
		uint32_t iSqrt = INV_SQRT[(v*DEBRUJIN64) >> 58];
		
		uint32_t divAprox = (uint32_t)(((num >> 32) * iSqrt) + (((num & c::max_uint32) * iSqrt) >> 32));

		y = (y + divAprox) >> 1;
		//y = (y + num / y) >> 1;

		value._data = static_cast<int32_t>(y);
		return value;
	}

	//--------------------------------------------------------------------------------
	fixed_t sqrt(fixed_t value)
	{
/*
#define BITSPERLONG 32
#define TOP2BITS(x) ((x & (3L << (BITSPERLONG-2))) >> (BITSPERLONG-2))
*/
		enum : uint32_t
		{
			value_bits = 32,
			top2_bits_shift = value_bits - 2,
			top2_bits_mask = uint32_t(3) << top2_bits_shift,

			output_bits = ((value_bits - fixed_t::precision) >> 1) + fixed_t::precision,
		};

		/* usqrt:
		ENTRY x: unsigned long
		EXIT  returns floor(sqrt(x) * pow(2, BITSPERLONG/2))

		Since the square root never uses more than half the bits
		of the input, we use the other half of the bits to contain
		extra bits of precision after the binary point.

		EXAMPLE
		suppose BITSPERLONG = 32
		then    usqrt(144) = 786432 = 12 * 65536
		usqrt(32) = 370727 = 5.66 * 65536

		NOTES
		(1) change BITSPERLONG to BITSPERLONG/2 if you do not want
		the answer scaled.  Indeed, if you want n bits of
		precision after the binary point, use BITSPERLONG/2+n.
		The code assumes that BITSPERLONG is even.
		(2) This is really better off being written in assembly.
		The line marked below is really a "arithmetic shift left"
		on the double-long value with r in the upper half
		and x in the lower half.  This operation is typically
		expressible in only one or two assembly instructions.
		(3) Unrolling this loop is probably not a bad idea.

		ALGORITHM
		The calculations are the base-two analogue of the square
		root algorithm we all learned in grammar school.  Since we're
		in base 2, there is only one nontrivial trial multiplier.

		Notice that absolutely no multiplications or divisions are performed.
		This means it'll be fast on a wide range of processors.
		*/

		uint32_t x = value._data;
		uint32_t a = 0;                   /* accumulator      */
		uint32_t r = 0;                   /* remainder        */
		//uint32_t e = 0;                   /* trial product    */

		for (uint32_t i = 0; i < output_bits; ++i)   // NOTE 1
		{
			r = (r << 2) + ((x & top2_bits_mask) >> top2_bits_shift); x <<= 2; // NOTE 2
			a <<= 1;
			uint32_t e = (a << 1) + 1; //hope for a register

			if (r >= e)
			{
				r -= e;
				++a;
			}
		}
	
		value._data = a;
		return value;
	}

	//--------------------------------------------------------------------------------
	fixed_t sin(fixed_t inAngle)
	{
		fixed_t tempAngle;
		/*
		tempAngle._data = inAngle._data % (c::pi << 1);

		if (tempAngle._data > c::pi)
			tempAngle._data -= (c::pi << 1);
		else if (tempAngle._data < -c::pi)
			tempAngle._data += (c::pi << 1);
		*/

		tempAngle._data = abs((abs(inAngle._data - (c::pi >> 1)) % (c::pi << 1)) - c::pi) - (c::pi >> 1);


		fixed_t tempAngleSq = tempAngle * tempAngle;

		// Most accurate version, accurate to ~2.1%

		fixed_t tempOut = tempAngle;
		tempAngle = tempAngle * tempAngleSq;
		tempOut._data -= (tempAngle._data / 6);
		tempAngle = tempAngle * tempAngleSq;
		tempOut._data += (tempAngle._data / 120);
		tempAngle = tempAngle * tempAngleSq;
		tempOut._data -= (tempAngle._data / 5040);
		tempAngle = tempAngle * tempAngleSq;
		tempOut._data += (tempAngle._data / 362880);
		tempAngle = tempAngle * tempAngleSq;
		tempOut._data -= ((tempAngle._data + (((tempAngle._data >= 0) * ((39916800 - 9413677) * 2)) - (39916800 - 9413677))) / 39916800);

		return tempOut;
	}

	//--------------------------------------------------------------------------------
	fixed_t fast_sin(fixed_t inAngle)
	{
		fixed_t tempAngle;
		tempAngle._data = inAngle._data % (c::pi << 1);

		if (tempAngle._data > c::pi)
			tempAngle._data -= (c::pi << 1);
		else if (tempAngle._data < -c::pi)
			tempAngle._data += (c::pi << 1);

		fixed_t tempAngleSq = tempAngle * tempAngle;

		// Fast implementation, runs at 159% the speed of above 'accurate' version
		// with an slightly lower accuracy of ~2.3%

		fixed_t tempOut;
		tempOut ._data = -13;
		(tempOut *= tempAngleSq)._data += 546;
		(tempOut *= tempAngleSq)._data -= 10923;
		(tempOut *= tempAngleSq)._data += 65536;
		(tempOut *= tempAngle);

		return tempOut;
	}

	//--------------------------------------------------------------------------------
	fixed_t cos(fixed_t inAngle)
	{
		inAngle._data += c::pi >> 1;
		return sin(inAngle);
	}

	//--------------------------------------------------------------------------------
	fixed_t fast_cos(fixed_t inAngle)
	{
		inAngle._data += c::pi >> 1;
		return fast_sin(inAngle);
	}

} }