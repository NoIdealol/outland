#include "pch.h"
#include "math\floating.h"

#include <math.h>

namespace outland
{
	namespace m
	{
		//--------------------------------------------------------------------------------
		float32_t trunc(float32_t value)
		{
			return ::truncf(value);
		}

		//--------------------------------------------------------------------------------
		float32_t floor(float32_t value)
		{
			return ::floorf(value);
		}

		//--------------------------------------------------------------------------------
		float32_t frac(float32_t value)
		{
			return value - ::floorf(value);
		}

		//--------------------------------------------------------------------------------
		float32_t abs(float32_t value)
		{
			return ::fabsf(value);
		}

		//--------------------------------------------------------------------------------
		float32_t sign(float32_t value)
		{
			return (float32_t)((0.0f < value) - (value < 0.0f));
		}

		//--------------------------------------------------------------------------------
		float32_t fast_inv_sqrt(float32_t value)
		{
			return 1.0f / ::sqrtf(value);
		}

		//--------------------------------------------------------------------------------
		float32_t fast_sqrt(float32_t value)
		{
			return ::sqrtf(value);
		}

		//--------------------------------------------------------------------------------
		float32_t sqrt(float32_t value)
		{
			return ::sqrtf(value);
		}

		//--------------------------------------------------------------------------------
		float32_t fast_sin(float32_t rad)
		{
			return ::sinf(rad);
		}

		//--------------------------------------------------------------------------------
		float32_t sin(float32_t rad)
		{
			return ::sinf(rad);
		}

		//--------------------------------------------------------------------------------
		float32_t fast_cos(float32_t rad)
		{
			return ::cosf(rad);
		}

		//--------------------------------------------------------------------------------
		float32_t cos(float32_t rad)
		{
			return ::cosf(rad);
		}
	}
}