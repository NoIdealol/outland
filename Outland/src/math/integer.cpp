#include "pch.h"
#include "math\integer.h"

namespace outland { namespace m
{
	//////////////////////////////////////////////////
	// INTEGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	uint32_t align(uint32_t value, uint32_t align)
	{
		uint32_t mask = align - 1;
		return (value + mask) & (~mask);
	}

	//--------------------------------------------------
	uint32_t to_pow2(uint32_t value)
	{
		value = value - 1;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		return value + 1;
	}

	//--------------------------------------------------
	uint32_t to_pow2(uint32_t value, size_t min_power)
	{
		value = value - 1;
		value |= ((1 << min_power) - 1);
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		return value + 1;
	}

	//--------------------------------------------------
	uint32_t bit_top(uint32_t value)
	{
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		return value & (~(value >> 1));
	}

	//--------------------------------------------------
	size_t bit_index(uint32_t value)
	{
		enum : uint32_t { debruijn_32 = 0x077CB531Ui32 };
		static const uint8_t debruijn_32_table[32] =
		{
			0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
			31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
		};
		return debruijn_32_table[(value * debruijn_32) >> 27];
	}

	//--------------------------------------------------
	size_t bit_lowest_index(uint32_t value)
	{
		enum : uint32_t { debruijn_32 = 0x077CB531Ui32 };
		static const uint8_t debruijn_32_table[32] =
		{
			0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
			31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
		};
		return debruijn_32_table[((value & (0 - value)) * debruijn_32) >> 27];
	}

	//--------------------------------------------------
	size_t bit_count(uint32_t value)
	{
		value = value - ((value >> 1) & 0x55555555);
		value = (value & 0x33333333) + ((value >> 2) & 0x33333333);
		return (((value + (value >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
	}

	//--------------------------------------------------
	uint64_t align(uint64_t value, uint64_t align)
	{
		uint64_t mask = align - 1;
		return (value + mask) & (~mask);
	}

	//--------------------------------------------------
	uint64_t to_pow2(uint64_t value)
	{
		value = value - 1;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		value |= value >> 32;
		return value + 1;
	}

	//--------------------------------------------------
	uint64_t to_pow2(uint64_t value, size_t min_power)
	{
		value = value - 1;
		value |= ((1 << min_power) - 1);
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		value |= value >> 32;
		return value + 1;
	}

	//--------------------------------------------------
	uint64_t bit_top(uint64_t value)
	{
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		value |= value >> 32;
		return value & (~(value >> 1));
	}

	//--------------------------------------------------
	size_t bit_index(uint64_t value)
	{
		enum : uint64_t { debruijn_64 = 0x0218A392CD3D5DBFUi64 };
		static const uint8_t debruijn_64_table[64] =
		{
			0, 1, 2, 7, 3, 13, 8, 19, 4, 25, 14, 28, 9, 34, 20,
			40, 5, 17, 26, 38, 15, 46, 29, 48, 10, 31, 35, 54,
			21, 50, 41, 57, 63, 6, 12, 18, 24, 27, 33, 39, 16,
			37, 45, 47, 30, 53, 49, 56, 62, 11, 23, 32, 36, 44,
			52, 55, 61, 22, 43, 51, 60, 42, 59, 58,
		};

		return debruijn_64_table[(value * debruijn_64) >> 58];
	}

	//--------------------------------------------------
	int8_t abs(int8_t value)
	{
		const int8_t mask = static_cast<int8_t>(value >= 0) - 1;
		return (mask & (~value + 1)) + (~mask & value);
	}

	//--------------------------------------------------
	int16_t abs(int16_t value)
	{
		const int16_t mask = static_cast<int16_t>(value >= 0) - 1;
		return (mask & (~value + 1)) + (~mask & value);
	}

	//--------------------------------------------------
	int32_t abs(int32_t value)
	{
		const int32_t mask = static_cast<int32_t>(value >= 0) - 1;
		return (mask & (~value + 1)) + (~mask & value);
	}

	//--------------------------------------------------
	int64_t abs(int64_t value)
	{
		const int64_t mask = static_cast<int64_t>(value >= 0) - 1;
		return (mask & (~value + 1)) + (~mask & value);
	}

	//--------------------------------------------------
	int8_t sign(int8_t value)
	{
		return static_cast<int8_t>(value > 0) - static_cast<int8_t>(value < 0);
	}

	//--------------------------------------------------
	int16_t sign(int16_t value)
	{
		return static_cast<int16_t>(value > 0) - static_cast<int16_t>(value < 0);
	}

	//--------------------------------------------------
	int32_t sign(int32_t value)
	{
		return static_cast<int32_t>(value > 0) - static_cast<int32_t>(value < 0);
	}

	//--------------------------------------------------
	int64_t sign(int64_t value)
	{
		return static_cast<int64_t>(value > 0) - static_cast<int64_t>(value < 0);
	}

} }
