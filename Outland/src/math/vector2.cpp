#include "pch.h"
#include "math\vector2.h"
#include "math\fixed.h"

namespace outland
{
	//////////////////////////////////////////////////////////////////////////////////
	// VECTOR 2
	//////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>::vector2(scalar_t x, scalar_t y)
		: _data{ x, y }
	{
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator = (const vector2& rhs)
	{
		vector2::_data[0] = rhs._data[0];
		vector2::_data[1] = rhs._data[1];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator += (const vector2& rhs)
	{
		vector2::_data[0] += rhs._data[0];
		vector2::_data[1] += rhs._data[1];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator += (const scalar_t rhs)
	{
		vector2::_data[0] += rhs;
		vector2::_data[1] += rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator -= (const vector2& rhs)
	{
		vector2::_data[0] -= rhs._data[0];
		vector2::_data[1] -= rhs._data[1];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator -= (const scalar_t rhs)
	{
		vector2::_data[0] -= rhs;
		vector2::_data[1] -= rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator *= (const vector2& rhs)
	{
		vector2::_data[0] *= rhs._data[0];
		vector2::_data[1] *= rhs._data[1];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator *= (const scalar_t rhs)
	{
		vector2::_data[0] *= rhs;
		vector2::_data[1] *= rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator /= (const vector2& rhs)
	{
		vector2::_data[0] /= rhs._data[0];
		vector2::_data[1] /= rhs._data[1];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t>& vector2<scalar_t>::operator /= (const scalar_t rhs)
	{
		vector2::_data[0] /= rhs;
		vector2::_data[1] /= rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator + (const vector2& rhs) const
	{
		return vector2(
			vector2::_data[0] + rhs._data[0],
			vector2::_data[1] + rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator + (const scalar_t rhs) const
	{
		return vector2(
			vector2::_data[0] + rhs,
			vector2::_data[1] + rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator - (const vector2& rhs) const
	{
		return vector2(
			vector2::_data[0] - rhs._data[0],
			vector2::_data[1] - rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator - (const scalar_t rhs) const
	{
		return vector2(
			vector2::_data[0] - rhs,
			vector2::_data[1] - rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator - () const//unary.. invert
	{
		return vector2(
			-vector2::_data[0],
			-vector2::_data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator * (const vector2& rhs) const
	{
		return vector2(
			vector2::_data[0] * rhs._data[0],
			vector2::_data[1] * rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator * (const scalar_t rhs) const
	{
		return vector2(
			vector2::_data[0] * rhs,
			vector2::_data[1] * rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator / (const scalar_t rhs) const
	{
		return vector2(
			vector2::_data[0] / rhs,
			vector2::_data[1] / rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> vector2<scalar_t>::operator / (const vector2& rhs) const
	{
		return vector2(
			vector2::_data[0] / rhs._data[0],
			vector2::_data[1] / rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector2<scalar_t>::operator == (const vector2& rhs) const
	{
		return (vector2::_data[0] == rhs._data[0] && vector2::_data[1] == rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector2<scalar_t>::operator != (const vector2& rhs) const
	{
		return ((vector2::_data[0] != rhs._data[0]) || (vector2::_data[1] != rhs._data[1]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector2<scalar_t>::operator < (const vector2& rhs) const
	{
		return ((vector2::_data[0] * vector2::_data[0] + vector2::_data[1] * vector2::_data[1]) < (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector2<scalar_t>::operator <= (const vector2& rhs) const
	{
		return ((vector2::_data[0] * vector2::_data[0] + vector2::_data[1] * vector2::_data[1]) <= (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector2<scalar_t>::operator > (const vector2& rhs) const
	{
		return ((vector2::_data[0] * vector2::_data[0] + vector2::_data[1] * vector2::_data[1]) > (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector2<scalar_t>::operator >= (const vector2& rhs) const
	{
		return ((vector2::_data[0] * vector2::_data[0] + vector2::_data[1] * vector2::_data[1]) >= (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	scalar_t& vector2<scalar_t>::operator [] (size_t index)
	{
		return _data[index];
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	const scalar_t& vector2<scalar_t>::operator [] (size_t index) const
	{
		return _data[index];
	}

	//////////////////////////////////////////////////
	// NON MEMBER FUNCTIONS
	//////////////////////////////////////////////////

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> operator + (const scalar_t lhs, const vector2<scalar_t>& rhs)
	{
		return rhs + lhs;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> operator - (const scalar_t lhs, const vector2<scalar_t>& rhs)
	{
		return -rhs + lhs;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector2<scalar_t> operator * (const scalar_t lhs, const vector2<scalar_t>& rhs)
	{
		return rhs * lhs;
	}

	namespace m
	{
		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		scalar_t dot(const vector2<scalar_t>& lhs, const vector2<scalar_t>& rhs)
		{
			return lhs._data[0] * rhs._data[0] + lhs._data[1] * rhs._data[1];
		}

		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		vector2<scalar_t> norm(const vector2<scalar_t>& val)
		{
			return val / sqrt(val._data[0] * val._data[0] + val._data[1] * val._data[1]);
		}

		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		scalar_t len(const vector2<scalar_t>& val)
		{
			return sqrt(val._data[0] * val._data[0] + val._data[1] * val._data[1]);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	// MATRIX 2
	//////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t> matrix2<scalar_t>::zero(scalar_t(0) , scalar_t(0) , scalar_t(0) , scalar_t(0));

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t> matrix2<scalar_t>::eye(scalar_t(1), scalar_t(0), scalar_t(0), scalar_t(1));

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t> matrix2<scalar_t>::one(scalar_t(1), scalar_t(1), scalar_t(1), scalar_t(1));

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>::matrix2(scalar_t rad)
	{
		scalar_t sin = m::sin(rad);
		scalar_t cos = m::cos(rad);
		_data[0] = vector2<scalar_t>(cos, -sin);
		_data[1] = vector2<scalar_t>(sin, cos);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>::matrix2(scalar_t c00, scalar_t c01, scalar_t c10, scalar_t c11)
	{
		_data[0] = vector2<scalar_t>(c00, c01);
		_data[1] = vector2<scalar_t>(c10, c11);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>::matrix2(const vector2<scalar_t>& r0, const vector2<scalar_t>& r1)
	{
		_data[0] = r0;
		_data[1] = r1;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator =  (const matrix2& rhs)
	{
		_data[0] = rhs._data[0];
		_data[1] = rhs._data[1];
		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator += (const matrix2& rhs)
	{
		_data[0] += rhs._data[0];
		_data[1] += rhs._data[1];
		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator += (const scalar_t rhs)
	{
		_data[0] += rhs;
		_data[1] += rhs;
		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator -= (const matrix2& rhs)
	{
		_data[0] -= rhs._data[0];
		_data[1] -= rhs._data[1];
		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator -= (const scalar_t rhs)
	{
		_data[0] -= rhs;
		_data[1] -= rhs;
		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator *= (const matrix2& rhs)
	{
		vector2<scalar_t> col0 = m::col(rhs, 0);
		vector2<scalar_t> col1 = m::col(rhs, 1);

		_data[0] = vector2<scalar_t>(m::dot(_data[0], col0), m::dot(_data[0], col1));
		_data[1] = vector2<scalar_t>(m::dot(_data[1], col0), m::dot(_data[1], col1));

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator *= (const scalar_t rhs)
	{
		_data[0] *= rhs;
		_data[1] *= rhs;
		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator /= (const matrix2& rhs)
	{
		_data[0] = vector2<scalar_t>(m::dot(_data[0], rhs._data[0]), m::dot(_data[0], rhs._data[1]));
		_data[1] = vector2<scalar_t>(m::dot(_data[1], rhs._data[0]), m::dot(_data[1], rhs._data[1]));

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>&	matrix2<scalar_t>::operator /= (const scalar_t rhs)
	{
		_data[0] /= rhs;
		_data[1] /= rhs;
		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator + (const matrix2& rhs) const
	{
		matrix2 ret = *this;
		ret += rhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator + (const scalar_t rhs) const
	{
		matrix2 ret = *this;
		ret += rhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator - (const matrix2& rhs) const
	{
		matrix2 ret = *this;
		ret -= rhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator - (const scalar_t rhs) const
	{
		matrix2 ret = *this;
		ret -= rhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator * (const matrix2& rhs) const
	{
		matrix2 ret = *this;
		ret *= rhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator * (const scalar_t rhs) const
	{
		matrix2 ret = *this;
		ret *= rhs;
		return ret;
	}
	
	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator / (const matrix2& rhs) const
	{
		matrix2 ret = *this;
		ret /= rhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	matrix2<scalar_t>::operator / (const scalar_t rhs) const
	{
		matrix2 ret = *this;
		ret /= rhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t> matrix2<scalar_t>::operator - () const//unary.. inverse
	{
		return matrix2(-_data[0], -_data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool matrix2<scalar_t>::operator == (const matrix2& rhs) const
	{
		return (_data[0] == rhs._data[0]) && (_data[1] == rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool matrix2<scalar_t>::operator != (const matrix2& rhs) const
	{
		return (_data[0] != rhs._data[0]) || (_data[1] != rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	const vector2<scalar_t>&	matrix2<scalar_t>::operator [] (size_t row) const
	{
		return _data[row];
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	operator + (const scalar_t lhs, const matrix2<scalar_t>& rhs)
	{
		matrix2<scalar_t> ret = rhs;
		ret += lhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	operator - (const scalar_t lhs, const matrix2<scalar_t>& rhs)
	{
		matrix2<scalar_t> ret = rhs;
		ret -= lhs;
		return ret;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	matrix2<scalar_t>	operator * (const scalar_t lhs, const matrix2<scalar_t>& rhs)
	{
		matrix2<scalar_t> ret = rhs;
		ret *= lhs;
		return ret;
	}

	namespace m
	{
		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		vector2<scalar_t>	mul(const matrix2<scalar_t>& lhs, const vector2<scalar_t>& rhs)
		{
			return vector2<scalar_t>(m::dot(lhs._data[0], rhs), m::dot(lhs._data[1], rhs));
		}

		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		vector2<scalar_t>	col(const matrix2<scalar_t>& val, size_t index)
		{
			return vector2<scalar_t>(val._data[0][index], val._data[1][index]);
		}

		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		const vector2<scalar_t>&	row(const matrix2<scalar_t>& val, size_t index)
		{
			return val._data[index];
		}

		template<typename scalar_t>
		matrix2<scalar_t>	transp(const matrix2<scalar_t>& val)
		{
			return matrix2<scalar_t>(
				val._data[0][0], val._data[1][0],
				val._data[0][1], val._data[1][1]);
		}
		
	}




#define O_INSTANTIATE(scalar_t)																	\
	template class vector2<scalar_t>;															\
	template vector2<scalar_t>	operator + (const scalar_t, const vector2<scalar_t>&);			\
	template vector2<scalar_t>	operator - (const scalar_t, const vector2<scalar_t>&);			\
	template vector2<scalar_t>	operator * (const scalar_t, const vector2<scalar_t>&);			\
	template scalar_t			m::dot(const vector2<scalar_t>&, const vector2<scalar_t>&);		\
	template vector2<scalar_t>	m::norm(const vector2<scalar_t>&);								\
	template scalar_t			m::len(const vector2<scalar_t>&);								\
	template class matrix2<scalar_t>;															\
	template matrix2<scalar_t>	operator + (const scalar_t, const matrix2<scalar_t>&);			\
	template matrix2<scalar_t>	operator - (const scalar_t, const matrix2<scalar_t>&);			\
	template matrix2<scalar_t>	operator * (const scalar_t, const matrix2<scalar_t>&);			\
	template vector2<scalar_t>	m::mul(const matrix2<scalar_t>&, const vector2<scalar_t>&);		\
	template vector2<scalar_t>	m::col(const matrix2<scalar_t>&, size_t index);					\
	template const vector2<scalar_t>&	m::row(const matrix2<scalar_t>&, size_t index);			\
	template matrix2<scalar_t>	m::transp(const matrix2<scalar_t>& val);

	O_INSTANTIATE(float)
	O_INSTANTIATE(double)
	O_INSTANTIATE(fixed_t)

}