#include "pch.h"
#include "math\vector3.h"
#include "math\fixed.h"

namespace outland
{
	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>::vector3(scalar_t x, scalar_t y, scalar_t z) : 
		_data{ x, y, z }
	{
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator = (const vector3& rhs)
	{
		vector3::_data[0] = rhs._data[0];
		vector3::_data[1] = rhs._data[1];
		vector3::_data[2] = rhs._data[2];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator += (const vector3& rhs)
	{
		vector3::_data[0] += rhs._data[0];
		vector3::_data[1] += rhs._data[1];
		vector3::_data[2] += rhs._data[2];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator += (const scalar_t rhs)
	{
		vector3::_data[0] += rhs;
		vector3::_data[1] += rhs;
		vector3::_data[2] += rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator -= (const vector3& rhs)
	{
		vector3::_data[0] -= rhs._data[0];
		vector3::_data[1] -= rhs._data[1];
		vector3::_data[2] -= rhs._data[2];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator -= (const scalar_t rhs)
	{
		vector3::_data[0] -= rhs;
		vector3::_data[1] -= rhs;
		vector3::_data[2] -= rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator *= (const vector3& rhs)
	{
		vector3::_data[0] *= rhs._data[0];
		vector3::_data[1] *= rhs._data[1];
		vector3::_data[2] *= rhs._data[2];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator *= (const scalar_t rhs)
	{
		vector3::_data[0] *= rhs;
		vector3::_data[1] *= rhs;
		vector3::_data[2] *= rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator /= (const vector3& rhs)
	{
		vector3::_data[0] /= rhs._data[0];
		vector3::_data[1] /= rhs._data[1];
		vector3::_data[2] /= rhs._data[2];

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t>& vector3<scalar_t>::operator /= (const scalar_t rhs)
	{
		vector3::_data[0] /= rhs;
		vector3::_data[1] /= rhs;
		vector3::_data[2] /= rhs;

		return *this;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator + (const vector3& rhs) const
	{
		return vector3(
			vector3::_data[0] + rhs._data[0],
			vector3::_data[1] + rhs._data[1],
			vector3::_data[2] + rhs._data[2]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator + (const scalar_t rhs) const
	{
		return vector3(
			vector3::_data[0] + rhs,
			vector3::_data[1] + rhs,
			vector3::_data[2] + rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator - (const vector3& rhs) const
	{
		return vector3(
			vector3::_data[0] - rhs._data[0],
			vector3::_data[1] - rhs._data[1],
			vector3::_data[2] - rhs._data[2]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator - (const scalar_t rhs) const
	{
		return vector3(
			vector3::_data[0] - rhs,
			vector3::_data[1] - rhs,
			vector3::_data[2] - rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator - () const//unary.. invert
	{
		return vector3(
			-vector3::_data[0],
			-vector3::_data[1],
			-vector3::_data[2]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator * (const vector3& rhs) const
	{
		return vector3(
			vector3::_data[0] * rhs._data[0],
			vector3::_data[1] * rhs._data[1],
			vector3::_data[2] * rhs._data[2]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator * (const scalar_t rhs) const
	{
		return vector3(
			vector3::_data[0] * rhs,
			vector3::_data[1] * rhs,
			vector3::_data[2] * rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator / (const scalar_t rhs) const
	{
		return vector3(
			vector3::_data[0] / rhs,
			vector3::_data[1] / rhs,
			vector3::_data[2] / rhs);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> vector3<scalar_t>::operator / (const vector3& rhs) const
	{
		return vector3(
			vector3::_data[0] / rhs._data[0],
			vector3::_data[1] / rhs._data[1],
			vector3::_data[2] / rhs._data[1]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector3<scalar_t>::operator == (const vector3& rhs) const
	{
		return (vector3::_data[0] == rhs._data[0] && vector3::_data[1] == rhs._data[1] && vector3::_data[2] == rhs._data[2]);
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector3<scalar_t>::operator != (const vector3& rhs) const
	{
		return ((vector3::_data[0] != rhs._data[0]) || (vector3::_data[1] != rhs._data[1]) || (vector3::_data[2] != rhs._data[2]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector3<scalar_t>::operator < (const vector3& rhs) const
	{
		return ((vector3::_data[0] * vector3::_data[0] + vector3::_data[1] * vector3::_data[1] + vector3::_data[2] * vector3::_data[2]) < (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1] + rhs._data[2] * rhs._data[2]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector3<scalar_t>::operator <= (const vector3& rhs) const
	{
		return ((vector3::_data[0] * vector3::_data[0] + vector3::_data[1] * vector3::_data[1] + vector3::_data[2] * vector3::_data[2]) <= (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1] + rhs._data[2] * rhs._data[2]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector3<scalar_t>::operator > (const vector3& rhs) const
	{
		return ((vector3::_data[0] * vector3::_data[0] + vector3::_data[1] * vector3::_data[1] + vector3::_data[2] * vector3::_data[2]) > (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1] + rhs._data[2] * rhs._data[2]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	bool vector3<scalar_t>::operator >= (const vector3& rhs) const
	{
		return ((vector3::_data[0] * vector3::_data[0] + vector3::_data[1] * vector3::_data[1] + vector3::_data[2] * vector3::_data[2]) >= (rhs._data[0] * rhs._data[0] + rhs._data[1] * rhs._data[1] + rhs._data[2] * rhs._data[2]));
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	scalar_t& vector3<scalar_t>::operator [] (size_t index)
	{
		return _data[index];
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	const scalar_t& vector3<scalar_t>::operator [] (size_t index) const
	{
		return _data[index];
	}

	//////////////////////////////////////////////////
	// NON MEMBER FUNCTIONS
	//////////////////////////////////////////////////

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> operator + (const scalar_t lhs, const vector3<scalar_t>& rhs)
	{
		return rhs + lhs;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> operator - (const scalar_t lhs, const vector3<scalar_t>& rhs)
	{
		return -rhs + lhs;
	}

	//--------------------------------------------------------------------------------
	template<typename scalar_t>
	vector3<scalar_t> operator * (const scalar_t lhs, const vector3<scalar_t>& rhs)
	{
		return rhs * lhs;
	}

	namespace m
	{
		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		scalar_t dot(const vector3<scalar_t>& lhs, const vector3<scalar_t>& rhs)
		{
			return lhs._data[0] * rhs._data[0] + lhs._data[1] * rhs._data[1] + lhs._data[2] * rhs._data[2];
		}

		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		vector3<scalar_t> cross(const vector3<scalar_t>& lhs, const vector3<scalar_t>& rhs)
		{
			return vector3<scalar_t>(
				lhs._data[1] * rhs._data[2] - lhs._data[2] * rhs._data[1],
				lhs._data[2] * rhs._data[0] - lhs._data[0] * rhs._data[2],
				lhs._data[0] * rhs._data[1] - lhs._data[1] * rhs._data[0]);
		}

		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		vector3<scalar_t> norm(const vector3<scalar_t>& val)
		{
			return val / sqrt(val._data[0] * val._data[0] + val._data[1] * val._data[1] + val._data[2] * val._data[2]);
		}

		//--------------------------------------------------------------------------------
		template<typename scalar_t>
		scalar_t len(const vector3<scalar_t>& val)
		{
			return sqrt(val._data[0] * val._data[0] + val._data[1] * val._data[1] + val._data[2] * val._data[2]);
		}
	}

#define O_INSTANTIATE(scalar_t)																	\
	template class vector3<scalar_t>;															\
	template vector3<scalar_t>	operator + (const scalar_t, const vector3<scalar_t>&);			\
	template vector3<scalar_t>	operator - (const scalar_t, const vector3<scalar_t>&);			\
	template vector3<scalar_t>	operator * (const scalar_t, const vector3<scalar_t>&);			\
	template scalar_t			m::dot(const vector3<scalar_t>&, const vector3<scalar_t>&);		\
	template vector3<scalar_t>	m::cross(const vector3<scalar_t>&, const vector3<scalar_t>&);	\
	template vector3<scalar_t>	m::norm(const vector3<scalar_t>&);								\
	template scalar_t			m::len(const vector3<scalar_t>&);


	O_INSTANTIATE(float)
	O_INSTANTIATE(double)
	O_INSTANTIATE(fixed_t)

}