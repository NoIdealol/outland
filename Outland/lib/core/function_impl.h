#pragma once
#include "function.h"

namespace outland
{

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<ret_t(*fnc_free)(arg_t...)>
	inline static ret_t function<ret_t(arg_t...)>::free_call(instance_t instance, arg_t... args)
	{
		return (*fnc_free)(u::forward<arg_t>(args)...);
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class obj_t, ret_t(*fnc_object)(obj_t*, arg_t...)>
	inline static ret_t function<ret_t(arg_t...)>::object_call(instance_t instance, arg_t... args)
	{
		return (*fnc_object)(static_cast<obj_t*>(instance), u::forward<arg_t>(args)...);
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class obj_t, ret_t(*fnc_object)(obj_t const*, arg_t...)>
	inline static ret_t function<ret_t(arg_t...)>::object_call(instance_t instance, arg_t... args)
	{
		return (*fnc_object)(static_cast<obj_t const*>(instance), u::forward<arg_t>(args)...);
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class cls_t, ret_t(cls_t::*fnc_class)(arg_t...)>
	inline static ret_t function<ret_t(arg_t...)>::class_call(instance_t instance, arg_t... args)
	{
		return (static_cast<cls_t*>(instance)->*fnc_class)(u::forward<arg_t>(args)...);
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class cls_t, ret_t(cls_t::*fnc_class)(arg_t...) const>
	inline static ret_t function<ret_t(arg_t...)>::class_call(instance_t instance, arg_t... args)
	{
		return (static_cast<cls_t const*>(instance)->*fnc_class)(u::forward<arg_t>(args)...);
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	function<ret_t(arg_t...)>::function()
		: _function(nullptr)
		, _instance(nullptr)
	{
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	function<ret_t(arg_t...)>::function(const function& other)
		: _function(other._function)
		, _instance(other._instance)
	{
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	function<ret_t(arg_t...)>::function(function&& other)
		: _function(other._function)
		, _instance(other._instance)
	{
		other._function = nullptr;
		other._instance = nullptr;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	function<ret_t(arg_t...)>::~function()
	{
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::operator = (const function& other)
	{
		_function = other._function;
		_instance = other._instance;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::operator = (function&& other)
	{
		_function = other._function;
		_instance = other._instance;
		other._function = nullptr;
		other._instance = nullptr;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	inline bool function<ret_t(arg_t...)>::operator == (const function& rhs) const
	{
		return _function == other._function && _instance == other._instance;
	}

	template<typename ret_t, typename... arg_t>
	inline bool function<ret_t(arg_t...)>::operator != (const function& rhs) const
	{
		return _function != other._function || _instance != other._instance;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	inline ret_t function<ret_t(arg_t...)>::operator() (arg_t... args) const
	{
		return _function(_instance, u::forward<arg_t>(args)...);
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<ret_t(*fnc)(arg_t...)>
	inline function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::bind()
	{
		_instance = nullptr;
		_function = &function::free_call<fnc>;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class obj_t, ret_t(*fnc)(obj_t*, arg_t...)>
	inline function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::bind(obj_t* obj)
	{
		_instance = obj;
		_function = &function::object_call<obj_t, fnc>;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class obj_t, ret_t(*fnc)(obj_t const*, arg_t...)>
	inline function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::bind(obj_t const* obj)
	{
		_instance = const_cast<obj_t*>(obj);
		_function = &function::object_call<obj_t, fnc>;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class cls_t, ret_t(cls_t::*fnc)(arg_t...)>
	inline function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::bind(cls_t* obj)
	{
		_instance = obj;
		_function = &function::class_call<cls_t, fnc>;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	template<class cls_t, ret_t(cls_t::*fnc)(arg_t...) const>
	inline function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::bind(cls_t const* obj)
	{
		_instance = const_cast<cls_t*>(obj);
		_function = &function::class_call<cls_t, fnc>;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	inline  function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::bind(instance_t* instance, function_t function)
	{
		_instance = instance;
		_function = function;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	inline function<ret_t(arg_t...)>& function<ret_t(arg_t...)>::clear()
	{
		_instance = nullptr;
		_function = nullptr;
		return *this;
	}

	//--------------------------------------------------
	template<typename ret_t, typename... arg_t>
	inline bool function<ret_t(arg_t...)>::is_bound() const
	{
		return _function != nullptr; 
	}
	
}
