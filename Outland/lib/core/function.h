#pragma once

namespace outland
{
	//////////////////////////////////////////////////
	// FUNCTION
	//////////////////////////////////////////////////

	template <typename sig_t> 
	class function;

	template<typename ret_t, typename... arg_t>
	class function<ret_t(arg_t...)>
	{
	public:
		using instance_t = void*;
		using function_t = ret_t(*)(instance_t instance, arg_t... args);

	private:
		instance_t	_instance;
		function_t	_function;

	//private:
	public:
		//--------------------------------------------------
		template<ret_t(*fnc_free)(arg_t...)>
		inline static ret_t free_call(instance_t instance, arg_t... args);
		//--------------------------------------------------
		template<class obj_t, ret_t(*fnc_object)(obj_t*, arg_t...)>
		inline static ret_t object_call(instance_t instance, arg_t... args);
		//--------------------------------------------------
		template<class obj_t, ret_t(*fnc_object)(obj_t const*, arg_t...)>
		inline static ret_t object_call(instance_t instance, arg_t... args);
		//--------------------------------------------------
		template<class cls_t, ret_t(cls_t::*fnc_class)(arg_t...)>
		inline static ret_t class_call(instance_t instance, arg_t... args);
		//--------------------------------------------------
		template<class cls_t, ret_t(cls_t::*fnc_class)(arg_t...) const>
		inline static ret_t class_call(instance_t instance, arg_t... args);

	public:

		function();
		function(const function& other);
		function(function&& other);
		~function();

		function&			operator = (const function& other);
		function&			operator = (function&& other);
		
		inline bool			operator == (const function& rhs) const;
		inline bool			operator != (const function& rhs) const;
		inline ret_t		operator() (arg_t... args) const;

		inline function&	clear();
		inline bool			is_bound() const;

		//--------------------------------------------------
		template<ret_t(*fnc)(arg_t...)>
		inline function& bind();
		//--------------------------------------------------
		template<class obj_t, ret_t(*fnc)(obj_t*, arg_t...)>
		inline function& bind(obj_t* obj);
		//--------------------------------------------------
		template<class obj_t, ret_t(*fnc)(obj_t const*, arg_t...)>
		inline function& bind(obj_t const* obj);
		//--------------------------------------------------
		template<class cls_t, ret_t(cls_t::*fnc)(arg_t...)>
		inline function& bind(cls_t* obj);
		//--------------------------------------------------
		template<class cls_t, ret_t(cls_t::*fnc)(arg_t...) const>
		inline function& bind(cls_t const* obj);
		//--------------------------------------------------
		inline function& bind(instance_t* instance, function_t function);

	
		inline instance_t	get_instance() const { return _instance; }
		inline function_t	get_function() const { return _function; }
	};
}

#include"function_impl.h"
