#pragma once
#include "array.h"

namespace outland
{
	//////////////////////////////////////////////////
	// STRING
	//////////////////////////////////////////////////

	template<typename char_t>
	class string;

	using stringw_t = string<wchar_t>;
	using string8_t = string<char8_t>;
	using string16_t = string<char16_t>;
	using string32_t = string<char32_t>;

	template<typename char_t>
	class string
	{
	public:
		using iterator_t = typename array<char_t>::iterator_t;
		using iterator_const_t = typename array<char_t>::iterator_const_t;

	private:
		array<char_t>	_buffer;

	public:
		string(class allocator_t* allocator);
		string(class allocator_t* allocator, char_t const* str, error_t& err);
		string(class allocator_t* allocator, size_t reserve, error_t& err);
		string(class allocator_t* allocator, string const& other, error_t& err);
		string(string const& other, error_t& err);
		string(string&& other);		
		~string();

		error_t operator = (char_t const* other);
		error_t operator = (string const& other);
		string& operator = (string&& other);
		
		allocator_t*		allocator() const { return _buffer.allocator(); }

		iterator_t		begin() { return _buffer.begin(); }
		iterator_t		end() { return _buffer.end() - 1; }
		char_t&			first() { return *_buffer.begin(); }
		char_t&			last() { return *(_buffer.end() - 2); }
		char_t*			data() { return _buffer.data(); }
		char_t&			operator [] (size_t index) { return _buffer[index]; }
		array<char_t>&	buffer() { return _buffer; }

		iterator_const_t		begin() const { return _buffer.begin(); }
		iterator_const_t		end() const { return _buffer.end() - 1; }
		char_t const&			first() const { return *_buffer.begin(); }
		char_t const&			last() const { return *(_buffer.end() - 2); }
		char_t const*			data() const { return _buffer.data(); }
		char_t const&			operator [] (size_t index) const { return _buffer[index]; }
		array<char_t> const&	buffer() const { return _buffer; }

		size_t			capacity() const { return _buffer.capacity() - 1; }
		size_t			size() const { return _buffer.size() - 1; }


		error_t			compact() { return _buffer.compact(); }
		error_t			resize(size_t size);
		error_t			reserve(size_t size);
		void			clear();

		void			swap(size_t first, size_t second) { _buffer.swap(first, second); }
		void			swap(iterator_t first, iterator_t second) { _buffer.swap(first, second); }

		size_t				find(size_t index, char_t const* str);
		size_t				find(size_t index, char_t const* str, size_t size);
		size_t				find(size_t index, iterator_const_t begin, iterator_const_t end);
		iterator_t			find(iterator_t at, char_t const* str);
		iterator_t			find(iterator_t at, char_t const* str, size_t size);
		iterator_t			find(iterator_t at, iterator_const_t begin, iterator_const_t end);

		size_t				find(size_t index, char_t const* str) const;
		size_t				find(size_t index, char_t const* str, size_t size) const;
		size_t				find(size_t index, iterator_const_t begin, iterator_const_t end) const;
		iterator_const_t	find(iterator_const_t at, char_t const* str) const;
		iterator_const_t	find(iterator_const_t at, char_t const* str, size_t size) const;
		iterator_const_t	find(iterator_const_t at, iterator_const_t begin, iterator_const_t end) const;

		size_t			replace(char_t what, char_t with);

		error_t			append(char_t ch);
		error_t			append(char_t const* str);
		error_t			append(char_t const* str, size_t size);
		error_t			append(iterator_const_t begin, iterator_const_t end);
	
		error_t			insert(size_t index, char_t ch);
		error_t			insert(iterator_t at, char_t ch);
		error_t			insert(size_t index, char_t const* str);
		error_t			insert(iterator_t at, char_t const* str);
		error_t			insert(size_t index, char_t const* str, size_t size);
		error_t			insert(iterator_t at, char_t const* str, size_t size);
		error_t			insert(size_t index, iterator_const_t begin, iterator_const_t end);
		error_t			insert(iterator_t at, iterator_const_t begin, iterator_const_t end);

		void			erase();
		void			erase(size_t size);

		void			remove(size_t index);
		void			remove(iterator_t at);
		void			remove(size_t index, size_t size);
		void			remove(iterator_t at, size_t size);
		void			remove(iterator_t begin, iterator_t end);
	};

	namespace u
	{
		template<typename char_t> void		str_copy(char_t* dst, char_t const* src);
		template<typename char_t> bool		str_cmp(char_t const* str1, char_t const* str2);
		template<typename char_t> bool		str_cmp(char_t const* str1, char_t const* str2, size_t max_chars);
		template<typename char_t> size_t	str_len(char_t const* str);
	}

}

#include "string_impl.h"
