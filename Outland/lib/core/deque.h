#pragma once
#include "allocator.h"

namespace outland
{
	//////////////////////////////////////////////////
	// DEQUE
	//////////////////////////////////////////////////

	template<typename element_t>
	class deque
	{
	private:
		allocator_t*	_allocator;
		size_t			_begin;
		size_t			_end;
		element_t*		_data;
		size_t			_capacity;

	private:
		element_t*	_alloc(size_t size, size_t& capacity);
		void		_free(element_t* memory, size_t size);

		void		_get_iterators(element_t*& beg1, element_t*& end1, element_t*& beg2, element_t*& end2);
		void		_move(element_t* to);
		size_t		_free_capacity();
		size_t		_expand();
		size_t		_last_index();

	public:
		deque(allocator_t* allocator);
		deque(deque const& other) = delete;
		deque(deque&& other);
		~deque();

		error_t operator = (deque const& other) = delete;
		deque& operator = (deque&& other);

		allocator_t*	allocator() const;

		size_t			size() const;
		size_t			capacity() const;
		size_t			space() const;
		error_t			reserve(size_t size);
		void			clear();

		element_t&		front();
		element_t&		back();

		template<typename... args_t>
		error_t			push_front(args_t&&... args);

		template<typename... args_t>
		error_t			push_back(args_t&&... args);

		void			pop_front();
		void			pop_back();
	};

}

#include "deque_impl.h"
