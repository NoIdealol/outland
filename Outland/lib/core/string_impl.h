#pragma once

namespace outland
{
	//--------------------------------------------------
	template<typename char_t>
	string<char_t>::string(class allocator_t* allocator)
		: _buffer(allocator)
	{
		_buffer.push_back('\0');
	}

	//--------------------------------------------------
	template<typename char_t>
	string<char_t>::string(class allocator_t* allocator, char_t const* str, error_t& err)
		: _buffer(allocator)
	{
		size_t len = u::str_len(str);
		err = _buffer.reserve(len + 1);
		
		if (!err)
			_buffer.push_back_array(str, len);
		
		//always null terminate
		_buffer.push_back('\0');
	}

	//--------------------------------------------------
	template<typename char_t>
	string<char_t>::string(class allocator_t* allocator, size_t reserve, error_t& err)
		: _buffer(allocator, reserve + 1, err)
	{
		//always null terminate
		_buffer.push_back('\0');
	}

	//--------------------------------------------------
	template<typename char_t>
	string<char_t>::string(class allocator_t* allocator, string const& other, error_t& err)
		: _buffer(allocator, other._buffer, err)
	{

	}

	//--------------------------------------------------
	template<typename char_t>
	string<char_t>::string(string const& other, error_t& err)
		: _buffer(other._buffer, err)
	{

	}

	//--------------------------------------------------
	template<typename char_t>
	string<char_t>::string(string&& other)
		: _buffer(u::move(other._buffer))
	{
		other._buffer.push_back('\0');
	}

	//--------------------------------------------------
	template<typename char_t>
	string<char_t>::~string()
	{

	}

	//--------------------------------------------------
	template<typename char_t>
	error_t string<char_t>::operator = (char_t const* other)
	{
		_buffer.clear();

		size_t len = u::str_len(other);
		error_t err = _buffer.reserve(len + 1);

		if (!err)
		{
			_buffer.push_back_array(other, len);
			_buffer.push_back('\0');
		}

		return err;
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t string<char_t>::operator = (string const& other)
	{
		return _buffer = str._buffer;
	}

	//--------------------------------------------------
	template<typename char_t>
	string<char_t>& string<char_t>::operator = (string&& str)
	{
		_buffer = u::move(str._buffer);
		str._buffer.push_back('\0');

		return *this;
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::resize(size_t size)
	{
		error_t err = _buffer.resize(size + 1);

		_buffer.last() = '\0'; //this is safe, must be allocated

		return err;
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::reserve(size_t size)
	{
		return _buffer.reserve(size + 1);
	}

	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::clear()
	{
		_buffer.clear();
		_buffer.push_back('\0');
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	string<char_t>::find(size_t index, char_t const* str)
	{
		return find(index, str, u::str_len(str));
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	string<char_t>::find(size_t index, char_t const* str, size_t size)
	{
		iterator_t it = this->begin() + index;
		iterator_t const it_end = this->end();
		while ((it_end != it) && !u::str_cmp(it, str, size))
		{
			++it;
		}

		return u::array_size(this->begin(), it);
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	string<char_t>::find(size_t index, iterator_const_t begin, iterator_const_t end)
	{
		return find(index, begin, u::array_size(begin, end));
	}

	//--------------------------------------------------
	template<typename char_t>
	typename string<char_t>::iterator_t	string<char_t>::find(iterator_t at, char_t const* str)
	{
		return find(at, str, u::str_len(str));
	}

	//--------------------------------------------------
	template<typename char_t>
	typename string<char_t>::iterator_t	string<char_t>::find(iterator_t at, char_t const* str, size_t size)
	{
		iterator_t it = at;
		iterator_t const it_end = this->end();
		while ((it_end != it) && !u::str_cmp(it, str, size))
		{
			++it;
		}

		return it;
	}

	//--------------------------------------------------
	template<typename char_t>
	typename string<char_t>::iterator_t	string<char_t>::find(iterator_t at, iterator_const_t begin, iterator_const_t end)
	{
		return find(at, begin, u::array_size(begin, end));
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	string<char_t>::find(size_t index, char_t const* str) const
	{
		return find(index, str, u::str_len(str));
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	string<char_t>::find(size_t index, char_t const* str, size_t size) const
	{
		iterator_const_t it = this->begin() + index;
		iterator_const_t const it_end = this->end();
		while ((it_end != it) && !u::str_cmp(it, str, size))
		{
			++it;
		}

		return u::array_size(this->begin(), it);
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	string<char_t>::find(size_t index, iterator_const_t begin, iterator_const_t end) const
	{
		return find(index, begin, u::array_size(begin, end));
	}

	//--------------------------------------------------
	template<typename char_t>
	typename string<char_t>::iterator_const_t	string<char_t>::find(iterator_const_t at, char_t const* str) const
	{
		return find(at, str, u::str_len(str));
	}

	//--------------------------------------------------
	template<typename char_t>
	typename string<char_t>::iterator_const_t	string<char_t>::find(iterator_const_t at, char_t const* str, size_t size) const
	{
		iterator_const_t it = at;
		iterator_const_t const it_end = this->end();
		while ((it_end != it) && !u::str_cmp(it, str, size))
		{
			++it;
		}

		return it;
	}

	//--------------------------------------------------
	template<typename char_t>
	typename string<char_t>::iterator_const_t	string<char_t>::find(iterator_const_t at, iterator_const_t begin, iterator_const_t end) const
	{
		return find(at, begin, u::array_size(begin, end));
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	string<char_t>::replace(char_t what, char_t with)
	{
		size_t count = 0;
		for (char_t& v : _buffer)
		{
			if (v == what)
			{
				v = with;
				++count;
			}
		}

		return count;
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::append(char_t ch)
	{
		return _buffer.insert(_buffer.end() - 1, ch);
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::append(char_t const* str)
	{
		return _buffer.insert_array(_buffer.end() - 1, str, u::str_len(str));
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::append(char_t const* str, size_t size)
	{
		return _buffer.insert_array(_buffer.end() - 1, str, size);
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::append(iterator_const_t begin, iterator_const_t end)
	{
		return _buffer.insert_array(_buffer.end() - 1, begin, end);
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::insert(size_t index, char_t ch)
	{
		return _buffer.insert(index, ch);
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::insert(iterator_t at, char_t ch)
	{
		return _buffer.insert(at, ch);
	}
	
	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::insert(size_t index, char_t const* str)
	{
		return _buffer.insert_array(_buffer.begin() + index, str, u::str_len(str));
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t string<char_t>::insert(iterator_t at, char_t const* str)
	{
		return _buffer.insert_array(at, str, u::str_len(str));
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::insert(size_t index, char_t const* str, size_t size)
	{
		return _buffer.insert_array(index, str, size);
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t string<char_t>::insert(iterator_t at, char_t const* str, size_t size)
	{
		return _buffer.insert_array(at, str, size);
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t	string<char_t>::insert(size_t index, iterator_const_t begin, iterator_const_t end)
	{
		return _buffer.insert_array(index, begin, end);
	}

	//--------------------------------------------------
	template<typename char_t>
	error_t string<char_t>::insert(iterator_t at, iterator_const_t begin, iterator_const_t end)
	{
		return _buffer.insert_array(at, begin, end);
	}
		
	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::erase()
	{
		_buffer.pop_back();
		_buffer.last() = '\0';
	}

	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::erase(size_t size)
	{
		_buffer.pop_back_array(size);
		_buffer.last() = '\0';
	}
	
	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::remove(size_t index)
	{
		_buffer.remove(index);
	}
	
	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::remove(iterator_t at)
	{
		_buffer.remove(at);
	}

	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::remove(size_t index, size_t size)
	{
		_buffer.remove_array(index, size);
	}

	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::remove(iterator_t at, size_t size)
	{
		_buffer.remove_array(at, size);
	}
	
	//--------------------------------------------------
	template<typename char_t>
	void	string<char_t>::remove(iterator_t begin, iterator_t end)
	{
		_buffer.remove_array(begin, end);
	}

	//--------------------------------------------------
	template<typename char_t> 
	void	u::str_copy(char_t* dst, char_t const* src)
	{
		while(*dst = *src)
		{
			++dst;
			++src;
		}
	}

	//--------------------------------------------------
	template<typename char_t> 
	bool	u::str_cmp(char_t const* str1, char_t const* str2)
	{
		while (*str1 == *str2 && *str1) 
		{
			++str1;
			++str2;
		}

		return *str1 == *str2;
	}

	//--------------------------------------------------
	template<typename char_t> 
	bool	u::str_cmp(char_t const* str1, char_t const* str2, size_t max_chars)
	{
		if (0 == max_chars)
			return true;

		while (*str1 == *str2 && *str1 && --max_chars)
		{
			++str1;
			++str2;
		}

		return *str1 == *str2;
	}

	//--------------------------------------------------
	template<typename char_t>
	size_t	u::str_len(const char_t* str)
	{
		const char_t* it = str;
		while (*it)
			++it;

		return static_cast<size_t>(it - str);
	}

}