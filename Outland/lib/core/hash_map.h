#pragma once
#include "array.h"

namespace outland
{
	//////////////////////////////////////////////////
	// HASH MAP
	//////////////////////////////////////////////////

	template<typename value_t, typename key_t>
	class hash_map
	{
	private:
		struct data_t
		{
			key_t		_key;
			value_t		_value;
			data_t*		_next;
		};

	public:
		using fnc_hash_t = function<uint32_t(key_t)>;
		using fnc_cmp_t = function<bool(key_t, key_t)>;

		class iterator_const_t
		{
		private:
			typename array<data_t>::iterator_const_t	_iterator;

		public:
			iterator_const_t() = default;
			iterator_const_t(typename array<data_t>::iterator_const_t it) : _iterator(it) {}

			value_t&	operator * () { return _iterator->_value; }
			value_t*	operator -> () { return &_iterator->_value; }

			iterator_const_t&	operator ++ () { ++_iterator; return *this; }
			iterator_const_t	operator ++ (int) { return iterator_const_t(_iterator++); }

			bool		operator == (iterator_const_t other) const { return _iterator == other._iterator; }
			bool		operator != (iterator_const_t other) const { return _iterator != other._iterator; }
		};

		class iterator_t
		{
		private:
			typename array<data_t>::iterator_t	_iterator;

		public:
			iterator_t() = default;
			iterator_t(typename array<data_t>::iterator_t it) : _iterator(it) {}

			value_t&	operator * () { return _iterator->_value; }
			value_t*	operator -> () { return &_iterator->_value; }

			iterator_t&	operator ++ () { ++_iterator; return *this; }
			iterator_t	operator ++ (int) { return iterator_t(_iterator++); }

			bool		operator == (iterator_t other) const { return _iterator == other._iterator; }
			bool		operator != (iterator_t other) const { return _iterator != other._iterator; }

			operator iterator_const_t () { return iterator_const_t(_iterator); }
		};
		
	private:
		array<data_t>	_data;
		array<data_t*>	_buckets; //always power of 2, rounded up from _data size.
		fnc_hash_t		_hash;
		fnc_cmp_t		_cmp;
		
	private:
		error_t	_make_space(size_t count);
		void	_rehash();
		size_t	_index(key_t key) const;

	public:
		hash_map(allocator_t* allocator, fnc_hash_t hash, fnc_cmp_t cmp);
		~hash_map();

		error_t				find(key_t key, value_t& val) const;
		error_t				remove(key_t key);
		error_t				insert(key_t key, value_t value);
		error_t				reserve(size_t size);
		void				clear();
		size_t				size() const { return _data.size(); }

		iterator_t			begin() { return iterator_t(_data.begin()); }
		iterator_t			end() { return iterator_t(_data.end()); }

		iterator_const_t	begin() const { return iterator_const_t(_data.begin()); }
		iterator_const_t	end() const { return iterator_const_t(_data.end()); }
	};
}

#include "hash_map_impl.h"
