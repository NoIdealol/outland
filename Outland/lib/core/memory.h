#pragma once
#include "tuple.h"
#include "allocator.h"

namespace outland
{
	struct memory_info_t
	{
		size_t align;
		size_t size;
	};

	namespace u
	{
		template<typename type_t>
		error_t	mem_alloc(allocator_t* allocator, type_t*& ptr)
		{
			void* buffer = allocator->alloc(sizeof(type_t), alignof(type_t));
			if (nullptr == buffer)
				return err_out_of_memory;

			ptr = reinterpret_cast<type_t*>(buffer);

			return err_ok;
		}

		template<typename type_t>
		void	mem_free(allocator_t* allocator, type_t* ptr)
		{
			allocator->free(ptr, sizeof(type_t));
		}

		template<typename type_t>
		error_t	mem_alloc(allocator_t* allocator, type_t*& ptr, size_t size)
		{
			void* buffer = allocator->alloc(sizeof(type_t) * size, alignof(type_t));
			if (nullptr == buffer)
				return err_out_of_memory;

			ptr = reinterpret_cast<type_t*>(buffer);

			return err_ok;
		}

		template<typename type_t>
		void	mem_free(allocator_t* allocator, type_t* ptr, size_t size)
		{
			allocator->free(ptr, sizeof(type_t) * size);
		}

		template<typename... types_t>
		error_t	mem_alloc(allocator_t* allocator, void*(&ptr_array)[sizeof...(types_t)], size_t(&size_array)[sizeof...(types_t)])
		{
			size_t align_arr[] = { alignof(types_t)... };
			size_t size_arr[] = { sizeof(types_t)... };
			size_t offset_arr[sizeof...(types_t)];
			for (size_t i = 0; i < sizeof...(types_t); ++i)
				size_arr[i] *= size_array[i];

			size_t size_sum;
			size_t align_max;
			u::mem_offsets(align_arr, size_arr, offset_arr, align_max, size_sum, sizeof...(types_t), 0);

			void* buffer = allocator->alloc(size_sum, align_max);
			if (nullptr == buffer)
				return err_out_of_memory;

			for (size_t i = 0; i < sizeof...(types_t); ++i)
				ptr_array[i] = reinterpret_cast<byte_t*>(buffer) + offset_arr[i];

			return err_ok;
		}

		template<typename... types_t>
		void	mem_free(allocator_t* allocator, void* ptr, size_t(&size_array)[sizeof...(types_t)])
		{
			size_t align_arr[] = { alignof(types_t)... };
			size_t size_arr[] = { sizeof(types_t)... };
			size_t offset_arr[sizeof...(types_t)];
			for (size_t i = 0; i < sizeof...(types_t); ++i)
				size_arr[i] *= size_array[i];

			size_t size_sum;
			size_t align_max;
			u::mem_offsets(align_arr, size_arr, offset_arr, align_max, size_sum, sizeof...(types_t), 0);

			allocator->free(ptr, size_sum);
		}

		
		O_INLINE error_t	mem_alloc(allocator_t* allocator, void*& ptr, memory_info_t const& info)
		{
			void* buffer = allocator->alloc(info.size, info.align);
			if (nullptr == buffer)
				return err_out_of_memory;

			ptr = buffer;

			return err_ok;
		}

		O_INLINE void	mem_free(allocator_t* allocator, void* ptr, memory_info_t const& info)
		{
			allocator->free(ptr, info.size);
		}
	}


	template<typename... types_t>
	class mem;

	//////////////////////////////////////////////////
	// MEM OBJECT
	//////////////////////////////////////////////////

	template<typename type_t>
	class mem<type_t>
	{
	public:
		using element_t = type_t;
		using alloc_t = type_t*;
		using array_size_t = void;

		template<typename... args_t>
		static alloc_t	alloc(outland::allocator_t* allocator, args_t&&... args);
		template<typename... args_t>
		static alloc_t	alloc(size_t align, outland::allocator_t* allocator, args_t&&... args);

		static void		free(outland::allocator_t* allocator, alloc_t allocation);
	};

	//////////////////////////////////////////////////
	// MEM ARRAY
	//////////////////////////////////////////////////

	template<typename type_t>
	class mem<type_t[]>
	{
	public:
		using element_t = type_t;
		using alloc_t = type_t*;
		using array_size_t = size_t;

		template<typename... args_t>
		static alloc_t	alloc(outland::allocator_t* allocator, array_size_t size, args_t&&... args);
		template<typename... args_t>
		static alloc_t	alloc(size_t align, outland::allocator_t* allocator, array_size_t size, args_t&&... args);
		static void		free(outland::allocator_t* allocator, alloc_t allocation, array_size_t size);
	};

	//////////////////////////////////////////////////
	// MEM DYNAMIC
	//////////////////////////////////////////////////

	template<typename... types_t>
	class mem
	{
	private:
		template<typename element_array_size_t, size_t i>
		struct _array_helper_t;

	public:
		using alloc_t = tuple<typename mem<types_t>::alloc_t...>;
		using array_size_t = tuple<typename mem<types_t>::array_size_t...>;

	private:
		template<size_t... i>
		static alloc_t	_alloc(size_t align, outland::allocator_t* allocator, array_size_t const& size, u::index_pack_t<i...>);
		
		template<size_t... i>
		static void		_free(outland::allocator_t* allocator, alloc_t const& allocation, array_size_t const& size, u::index_pack_t<i...>);

	public:
		static alloc_t	alloc(outland::allocator_t* allocator, array_size_t size);
		static alloc_t	alloc(size_t align, outland::allocator_t* allocator, array_size_t size);
		static void		free(outland::allocator_t* allocator, alloc_t const& allocation, array_size_t size);
	};

	//////////////////////////////////////////////////
	// MEM SIZED
	//////////////////////////////////////////////////

	//template<typename... types_t>
	//using mem_sized_t = mem<types_t..., tuple<typename mem<types_t>::array_size_t..., void>>;
}

#include "memory_impl.h"
