#pragma once
#include "deque.h"
#include "utility.h"
#include "placement_new.h"
#include "error.h"

namespace outland
{
	//--------------------------------------------------
	template<typename element_t>
	element_t*	deque<element_t>::_alloc(size_t size, size_t& capacity)
	{
		void* mem = _allocator->alloc(size * sizeof(element_t), alignof(element_t), &capacity);
		if (nullptr == mem)
			return nullptr;
		
		capacity = capacity / sizeof(element_t);
		return reinterpret_cast<element_t*>(mem);
	}

	//--------------------------------------------------
	template<typename element_t>
	void		deque<element_t>::_free(element_t* memory, size_t size)
	{
		_allocator->free(memory, size * sizeof(element_t));
	}

	//--------------------------------------------------
	template<typename element_t>
	void		deque<element_t>::_get_iterators(element_t*& beg1, element_t*& end1, element_t*& beg2, element_t*& end2)
	{
		if (_end <= _capacity)
		{
			end1 = _data + _end;
			end2 = _data;
		}
		else
		{
			end1 = _data + _capacity;
			end2 = _data + (_end - _capacity);
		}

		beg1 = _data + _begin;
		beg2 = _data;
	}

	//--------------------------------------------------
	template<typename element_t>
	void		deque<element_t>::_move(element_t* to)
	{
		element_t* it1;
		element_t* it2;
		element_t* end1;
		element_t* end2;
		_get_iterators(it1, end1, it2, end2);

		while (it1 != end1)
		{
			new(to, alignof(element_t)) element_t(u::move(*it1));
			it1->~element_t();

			++to;
			++it1;
		}

		while (it2 != end2)
		{
			new(to, alignof(element_t)) element_t(u::move(*it2));
			it2->~element_t();

			++to;
			++it2;
		}
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t		deque<element_t>::_free_capacity()
	{
		return _capacity - size();
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t		deque<element_t>::_expand()
	{
		//double the size, or request init size
		return size() ? (size() << 1) : (4);
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t		deque<element_t>::_last_index()
	{
		O_ASSERT(0 != size());

		return (_end <= _capacity) ? (_end - 1) : (_end - 1 - _capacity);
	}

	//--------------------------------------------------
	template<typename element_t>
	deque<element_t>::deque(allocator_t* allocator)
		: _allocator(allocator)
		, _begin(0)
		, _end(0)
		, _data(nullptr)
		, _capacity(0)
	{

	}

	//--------------------------------------------------
	template<typename element_t>
	deque<element_t>::deque(deque&& other)
		: _allocator(other._allocator)
		, _begin(other._begin)
		, _end(other._end)
		, _data(other._data)
		, _capacity(other._capacity)
	{
		other._begin = 0;
		other._end = 0;
		other._data = nullptr;
		other._capacity = 0;
	}

	//--------------------------------------------------
	template<typename element_t>
	deque<element_t>::~deque()
	{
		_free(_data, _capacity);
	}

	//--------------------------------------------------
	template<typename element_t>
	deque<element_t>& deque<element_t>::operator = (deque&& other)
	{
		_free(_data, _capacity);

		_allocator = other.allocator;

		_begin = other._begin;
		_end = other._end;
		_data = other._data;
		_capacity = other._capacity;

		other._begin = 0;
		other._end = 0;
		other._data = nullptr;
		other._capacity = 0;

		return *this;
	}

	//--------------------------------------------------
	template<typename element_t>
	allocator_t*	deque<element_t>::allocator() const
	{
		return _allocator;
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t			deque<element_t>::size() const
	{
		return _end - _begin;
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t			deque<element_t>::capacity() const
	{
		return _capacity;
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t			deque<element_t>::space() const
	{
		return capacity() - size();
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t			deque<element_t>::reserve(size_t size)
	{
		if (size > _capacity)
		{
			size_t cap;
			element_t* data = _alloc(size, cap);
			if (nullptr == data)
				return err_out_of_memory;

			_move(data);

			//store
			_free(_data, _capacity);

			_capacity = cap;
			_data = data;
			_end = this->size();
			_begin = 0;
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	void			deque<element_t>::clear()
	{
		element_t* it1;
		element_t* it2;
		element_t* end1;
		element_t* end2;
		_get_iterators(it1, end1, it2, end2);

		while (it1 != end1)
		{
			it1->~element_t();
			++it1;
		}

		while (it2 != end2)
		{
			it2->~element_t();
			++it2;
		}

		_begin = 0;
		_end = 0;
	}

	//--------------------------------------------------
	template<typename element_t>
	element_t&		deque<element_t>::front()
	{
		O_ASSERT(0 != size());

		return _data[_begin];
	}

	//--------------------------------------------------
	template<typename element_t>
	element_t&		deque<element_t>::back()
	{
		O_ASSERT(0 != size());

		return _data[_last_index()];
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t			deque<element_t>::push_front(args_t&&... args)
	{
		if (_free_capacity())
		{
			if (0 == _begin)
			{
				_begin += _capacity;
				_end += _capacity;
			}

			--_begin;
		}
		else
		{
			size_t cap;
			element_t* data = _alloc(_expand(), cap);
			if (nullptr == data)
				return err_out_of_memory;

			_move(data + 1);

			//store
			_free(_data, _capacity);

			_capacity = cap;
			_data = data;
			_end = this->size() + 1;
			_begin = 0;
		}

		new(_data + _begin, alignof(element_t)) element_t(u::forward<args_t>(args)...);

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t			deque<element_t>::push_back(args_t&&... args)
	{
		if (_free_capacity())
		{		
		}
		else
		{
			size_t cap;
			element_t* data = _alloc(_expand(), cap);
			if (nullptr == data)
				return err_out_of_memory;

			_move(data);

			//store
			_free(_data, _capacity);

			_capacity = cap;
			_data = data;
			_end = this->size();
			_begin = 0;
		}

		++_end;
		new(_data + _last_index(), alignof(element_t)) element_t(u::forward<args_t>(args)...);

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	void			deque<element_t>::pop_front()
	{
		O_ASSERT(0 != size());

		_data[_begin].~element_t();

		if (++_begin == _capacity)
		{
			_begin -= _capacity;
			_end -= _capacity;
		}
	}

	//--------------------------------------------------
	template<typename element_t>
	void			deque<element_t>::pop_back()
	{
		O_ASSERT(0 != size());

		_data[_last_index()].~element_t();
		--_end;
	}

}
