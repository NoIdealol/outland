#pragma once
#include "hash_map.h"

namespace outland
{

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	error_t	hash_map<value_t, key_t>::_make_space(size_t count)
	{
		enum : size_t 
		{
			max_load_factor = 70,
			precision = 100,
			init_size_half = 2,
		};

		if ((precision * (_data.size() + count)) > (max_load_factor * _buckets.size()))
		{
			error_t err;

			size_t bucket_space = (_buckets.size() != 0) ? (_buckets.size()) : init_size_half;
			size_t data_space;

			//lazy calculation of required resize
			do
			{
				bucket_space *= 2;
				data_space = (bucket_space * max_load_factor + (precision - 1)) / precision;
			} while ((_data.size() + count) > data_space);

			err = _data.reserve(data_space);
			if (err)
				return err;

			err = _buckets.resize(bucket_space);

			_rehash(); //data ptrs are invalid, need to rehash regardless of error

			if (err)
				return err;
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	void	hash_map<value_t, key_t>::_rehash()
	{
		for (data_t*& v : _buckets)
		{
			v = nullptr;
		}

		for (data_t& v : _data)
		{
			size_t index = _index(v._key);
			v._next = _buckets[index];
			_buckets[index] = &v;
		}
	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	error_t	hash_map<value_t, key_t>::reserve(size_t size)
	{
		if (_data.size() >= size)
			return err_ok;

		return _make_space(size - _data.size());
	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	void	hash_map<value_t, key_t>::clear()
	{
		_data.clear();
		_buckets.clear();
	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	size_t	hash_map<value_t, key_t>::_index(key_t key) const
	{
		return _hash(key) % _buckets.size();
	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	hash_map<value_t, key_t>::hash_map(class allocator_t* allocator, fnc_hash_t hash, fnc_cmp_t cmp)
		: _data(allocator)
		, _buckets(allocator)
		, _hash(hash)
		, _cmp(cmp)
	{

	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	hash_map<value_t, key_t>::~hash_map()
	{

	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	error_t	hash_map<value_t, key_t>::find(key_t key, value_t& val) const
	{
		if (0 == _buckets.size())
			return err_not_found;

		size_t index = _index(key);
		const data_t* entry = _buckets[index];
		while (entry && !_cmp(entry->_key, key))
			entry = entry->_next;

		if (entry)
		{
			val = entry->_value;
			return err_ok;
		}
		else
		{
			return err_not_found;
		}
	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	error_t		hash_map<value_t, key_t>::remove(key_t key)
	{
		size_t index = _index(key);

		data_t* entry = _buckets[index];
		data_t** prev = &_buckets[index];

		while (entry && !_cmp(entry->_key, key))
		{
			prev = &(entry->_next);
			entry = entry->_next;
		}

		if (entry)
		{
			//release the chain
			*prev = entry->_next;

			//re-link the last one
			if (_data.size() > 1 && entry != (_data.end() - 1))
			{
				size_t index_last = _index(_data.last()._key);

				data_t* entry_last = _buckets[index_last];
				data_t** prev_last = &_buckets[index_last];

				while (entry_last != (_data.end() - 1))
				{
					prev_last = &(entry_last->_next);
					entry_last = entry_last->_next;
				}

				*prev_last = entry;

				_data.remove_swap(entry);
			}
			else
			{
				_data.pop_back();
			}

			return err_ok;
		}
		else
		{
			return err_not_found;
		}
	}

	//--------------------------------------------------
	template<typename value_t, typename key_t>
	error_t		hash_map<value_t, key_t>::insert(key_t key, value_t value)
	{
		error_t err;

		err = _make_space(1);

		if (err)
			return err;

		size_t index = _index(key);

		if (_buckets[index] == nullptr)
		{
			_data.push_back();
			_buckets[index] = &_data.last();
			_buckets[index]->_key = key;
			_buckets[index]->_value = value;
			_buckets[index]->_next = nullptr;

			return err_ok;
		}
		else
		{
			data_t* entry = _buckets[index];
			while (entry)
			{
				if (_cmp(entry->_key, key))
				{
					return err_already_exists;
				}
				entry = entry->_next;
			}

			entry = _buckets[index];

			_data.push_back();
			_buckets[index] = &_data.last();
			_buckets[index]->_key = key;
			_buckets[index]->_value = value;
			_buckets[index]->_next = entry;
			
			return err_ok;
		}
	}

}
