#pragma once
#include "array.h"
#include "utility.h"
#include "error.h"

namespace outland
{
	//--------------------------------------------------
	template<typename element_t>
	array<element_t>::array(class allocator_t* allocator)
		: _allocator(allocator)
		, _begin(nullptr)
		, _size(0)
		, _capacity(0)
	{

	}

	//--------------------------------------------------
	template<typename element_t>
	array<element_t>::array(class allocator_t* allocator, size_t reserve, error_t& err)
		: array(allocator)
	{
		_begin = _alloc(reserve, _capacity);
		err = (nullptr == _begin) ? err_out_of_memory : err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	array<element_t>::array(class allocator_t* allocator, array const& other, error_t& err)
		: array(allocator, other.size(), err)
	{
		if (err)
			return;

		u::array_copy(_begin, other._begin, other._size);
		_size = other._size;
	}

	//--------------------------------------------------
	template<typename element_t>
	array<element_t>::array(array const& other, error_t& err)
		: array(other._allocator, other, err)
	{
	}

	//--------------------------------------------------
	template<typename element_t>
	array<element_t>::array(array&& other)
		: _allocator(other._allocator)
		, _begin(nullptr)
		, _size(0)
		, _capacity(0)
	{
		_store(other._begin, other._size, other._capacity);
	
		other._begin = nullptr;
		other._size = 0;
		other._capacity = 0;
	}

	//--------------------------------------------------
	template<typename element_t>
	array<element_t>::~array()
	{
		u::array_destruct(_begin, _size);

		_free();
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t array<element_t>::operator = (array const& other)
	{	
		//make sure we have memory
		if (capacity() < other._size)
		{
			element_t* begin;
			size_t capacity;

			begin = _alloc(other._size, capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_destruct(_begin, _size);
			_free();

			_store(begin, other._size, capacity);
		}
		else
		{
			u::array_destruct(_begin, _size);
			_size = other._size;
		}
		
		u::array_copy(_begin, other._begin, other._size);
	
		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	array<element_t>& array<element_t>::operator = (array&& other)
	{
		u::array_destruct(_begin, _size);
		_free();

		_allocator = other._allocator;
		_store(other._begin, other._size, other._capacity);
	
		other._begin = nullptr;
		other._size = 0;
		other._capacity = 0;

		return *this;
	}

	//--------------------------------------------------
	template<typename element_t>
	element_t*	array<element_t>::_alloc(size_t size, size_t& capacity)
	{
		if (0 == size)
		{
			capacity = 0;
			return nullptr;
		}

		if (size <= _soo_capacity())
		{
			return _soo_buffer(capacity);
		}
		else
		{
			void* begin_v = _allocator->alloc(sizeof(element_t) * size, alignof(element_t),  &capacity);
			capacity = capacity / sizeof(element_t);
			return reinterpret_cast<element_t*>(begin_v);
		}
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::_free()
	{
		if (!_soo_enabled())
			_allocator->free(_begin, sizeof(element_t) * _capacity);
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::_store(element_t* begin, size_t size, size_t& capacity)
	{
		if (begin == _soo_buffer(capacity))
		{
			_begin = _soo_buffer(_capacity);
			_capacity = 0; //clear before write

			u::array_move(_begin, begin, size);
		}
		else
		{
			_begin = begin;
			_capacity = capacity;
		}

		_size = size;
	}

	//--------------------------------------------------
	template<typename element_t>
	bool	array<element_t>::_soo_enabled() const
	{
		return _begin == _soo_buffer(_capacity);
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t	array<element_t>::_soo_capacity() const
	{
		return sizeof(_capacity) / sizeof(element_t);
	}

	//--------------------------------------------------
	template<typename element_t>
	element_t*	array<element_t>::_soo_buffer(size_t& capacity) const
	{
		return reinterpret_cast<element_t*>(reinterpret_cast<void*>(&capacity));
	}

	//--------------------------------------------------
	template<typename element_t>
	element_t const*	array<element_t>::_soo_buffer(size_t const& capacity) const
	{
		return reinterpret_cast<element_t const*>(reinterpret_cast<void const*>(&capacity));
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t array<element_t>::_free_capacity() const
	{
		return capacity() - _size;
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t	array<element_t>::_expand() const
	{
		//double the size, or request one on first
		return size() ? (size() << 1) : (1);
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t	array<element_t>::_expand(size_t min_size) const
	{
		size_t cur_size = _expand();
		//double the size until we are at our goal
		while (min_size > cur_size)
			cur_size <<= 1;
		
		return cur_size;
	}

	//--------------------------------------------------
	template<typename element_t>
	size_t	array<element_t>::capacity() const
	{
		return _soo_enabled() ? _soo_capacity() : _capacity;
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t	array<element_t>::compact()
	{
		if (_size)
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_size, capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, _size);
			_free();

			_store(begin, _size, capacity);			
		}
		else if (capacity())
		{
			_free();

			_begin = nullptr;
			_capacity = 0;
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t	array<element_t>::resize(size_t size)
	{
		if (_size == size)
			return err_ok;

		if (_size > size)
		{
			u::array_destruct(_begin + size, _size - size);
			_size = size;
		}
		else
		{
			error_t err = reserve(size);
			if (err)
				return err;

			u::array_construct(_begin + _size, size - _size);
			_size = size;
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t	array<element_t>::reserve(size_t size)
	{
		if (capacity() < size)
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_expand(size), capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, _size);
			_free();

			_store(begin, _size, capacity);
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::clear()
	{
		u::array_destruct(_begin, _size);
		_size = 0;
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::swap(size_t first, size_t second)
	{
		swap(_begin + first, _begin + second);
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::swap(iterator_t first, iterator_t second)
	{
		element_t temp = u::move(*first);
		*first = u::move(*second);
		*second = u::move(temp);
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t	array<element_t>::push_back(args_t&&... args)
	{
		if (!_free_capacity())
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_expand(), capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, _size);
			_free();

			_store(begin, _size, capacity);
		}

		new(_begin + _size, alignof(element_t)) element_t(u::forward<args_t>(args)...);
		++_size;

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t	array<element_t>::insert(size_t index, args_t&&... args)
	{
		if (_free_capacity())
		{
			u::array_move_reverse(_begin + index + 1, _begin + index, _size - index);
				
			new(_begin + index, alignof(element_t)) element_t(u::forward<args_t>(args)...);
			++_size;
		}
		else
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_expand(), capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, index);
			u::array_move(begin + index + 1, _begin + index, _size - index);


			new(begin + index, alignof(element_t)) element_t(u::forward<args_t>(args)...);

			_free();
			_store(begin, _size + 1, capacity);
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t	array<element_t>::insert(iterator_t at, args_t&&... args)
	{
		return insert(u::array_size(_begin, at), u::forward<args_t>(args)...);
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t	array<element_t>::push_back_array(size_t size, args_t&&... args)
	{
		if (_free_capacity() < size)
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_expand(_size + size), capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, _size);
			_free();

			_store(begin, _size, capacity);
		}

		u::array_construct(_begin + _size, size, u::forward<args_t>(args)...);
		_size += size;	

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t	array<element_t>::push_back_array(iterator_const_t begin, iterator_const_t end)
	{
		size_t size = u::array_size(begin, end);
		return push_back_array(begin, size);
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t	array<element_t>::push_back_array(const element_t* data, size_t size)
	{
		if (_free_capacity() < size)
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_expand(_size + size), capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, _size);
			_free();

			_store(begin, _size, capacity);
		}

		u::array_copy(_begin + _size, data, size);
		_size += size;

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t	array<element_t>::insert_array(iterator_t at, size_t size, args_t&&... args)
	{
		size_t index = u::array_size(_begin, at);
		return insert_array(index, size, u::forward<args_t>(args)...);
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t array<element_t>::insert_array(iterator_t at, iterator_const_t begin, iterator_const_t end)
	{
		size_t index = u::array_size(_begin, at);
		size_t size = u::array_size(begin, end);
		return insert_array(index, begin, size);
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t array<element_t>::insert_array(iterator_t at, const element_t* data, size_t size)
	{
		size_t index = u::array_size(_begin, at);
		return insert_array(index, data, size);
	}

	//--------------------------------------------------
	template<typename element_t>
	template<typename... args_t>
	error_t	array<element_t>::insert_array(size_t index, size_t size, args_t&&... args)
	{
		if (_free_capacity() < size)
		{
			u::array_move_reverse(_begin + index + size, _begin + index, _size - index);
			u::array_construct(_begin + index, size, u::forward<args_t>(args)...);
			_size += size;
		}
		else
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_expand(_size + size), capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, index);
			u::array_move(begin + index + size, _begin + index, _size - index);

			u::array_construct(begin + index, size, u::forward<args_t>(args)...);

			_free();
			_store(begin, _size + size, capacity);
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t array<element_t>::insert_array(size_t index, iterator_const_t begin, iterator_const_t end)
	{
		size_t size = u::array_size(begin, end);
		return insert_array(index, begin, size);
	}

	//--------------------------------------------------
	template<typename element_t>
	error_t array<element_t>::insert_array(size_t index, const element_t* data, size_t size)
	{
		if (_free_capacity() < size)
		{
			u::array_move_reverse(_begin + index + size, _begin + index, _size - index);
			u::array_copy(_begin + index, data, size);
			_size += size;
		}
		else
		{
			element_t* begin;
			size_t capacity;
			begin = _alloc(_expand(_size + size), capacity);

			if (nullptr == begin)
				return err_out_of_memory;

			u::array_move(begin, _begin, index);
			u::array_move(begin + index + size, _begin + index, _size - index);

			u::array_copy(begin + index, data, size);

			_free();
			_store(begin, _size + size, capacity);
		}

		return err_ok;
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::pop_back()
	{
		_begin[--_size].~element_t();
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove(size_t index)
	{
		element_t* at = _begin + index;
		at->~element_t();
		u::array_move(at, at + 1, _size - index);
		--_size;
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove(iterator_t at)
	{
		size_t index = u::array_size(_begin, at);
		at->~element_t();
		u::array_move(at, at + 1, _size - index);
		--_size;
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove_swap(size_t index)
	{
		remove_swap(_begin + index);
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove_swap(iterator_t at)
	{
		--_size;
		element_t* end = _begin + _size;
		if(at != end)
			*at = u::move(*end);

		end->~element_t();
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::pop_back_array(size_t size)
	{
		u::array_destruct(_begin + (_size - size), size);
		_size -= size;
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::pop_back_array(iterator_t begin)
	{
		size_t size = _size - u::array_size(_begin, begin);
		pop_back_array(size);
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove_array(iterator_t at, size_t size)
	{
		size_t index = u::array_size(_begin, at);
		remove_array(index, size);
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove_array(iterator_t at, iterator_t end)
	{
		size_t index = u::array_size(_begin, at);
		size_t size = u::array_size(_begin + index, end);
		remove_array(index, size);
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove_array(size_t index, size_t size)
	{
		u::array_destruct(_begin + index, size);
		u::array_move(_begin + index, _begin + index + size, _size - (index + size));
		_size -= size;
	}

	//--------------------------------------------------
	template<typename element_t>
	void	array<element_t>::remove_array(size_t index, iterator_t end)
	{
		size_t size = u::array_size(_begin + index, end);
		remove_array(index, size);
	}

}
