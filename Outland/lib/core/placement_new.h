#pragma once
#include "allocator.h"

//if the standard <new.h> is not included, it defines the void* version

////////////////////////////////////////////////////
// PLACEMENT NEW
////////////////////////////////////////////////////

//--------------------------------------------------
inline void* operator new(size_t size, void* allocator, size_t align)
{
	static_cast<void>(size);
	return allocator;
}

//--------------------------------------------------
inline void operator delete(void* memory, void* allocator, size_t align)
{
	static_cast<void>(memory);
	static_cast<void>(allocator);
	static_cast<void>(align);
}

#define O_CREATE(type_t, ptr) new(ptr, alignof(type_t)) type_t
#define O_DESTROY(type_t, ptr) static_cast<type_t*>(ptr)->~type_t();
