#pragma once

namespace outland
{
	enum : error_t
	{
		err_ok, //0

		//memory
		err_out_of_memory,
		err_out_of_handles,


		//file
		err_not_found,
		err_access_denied,
		err_already_exists,

		//async
		err_operation_pending,
		err_too_many_requests,
		err_aborted,

		//logic err
		err_bad_data,
		err_invalid_handle,
		err_invalid_parameter,
		err_not_implemented,

		//last error, use this + 1 as a starting index for custom errors
		err_unknown,
	};
}
