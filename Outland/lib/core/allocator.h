#pragma once

namespace outland
{
	//////////////////////////////////////////////////
	// ALLOCATOR
	//////////////////////////////////////////////////

	class allocator_t
	{
	public:
		virtual void*	alloc(size_t size, size_t align, size_t* capacity = nullptr) = 0;
		virtual void	free(void* memory, size_t size) = 0;
	};

}
