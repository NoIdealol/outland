#pragma once
#include "memory.h"

namespace outland
{
	//////////////////////////////////////////////////
	// MEM OBJECT
	//////////////////////////////////////////////////

	//--------------------------------------------------
	template<typename type_t>
	template<typename... args_t>
	typename mem<type_t>::alloc_t		mem<type_t>::alloc(outland::allocator_t* allocator, args_t&&... args)
	{
		return new(allocator->alloc(sizeof(type_t), alignof(type_t)), alignof(type_t)) type_t(u::forward<args_t>(args)...);
	}

	//--------------------------------------------------
	template<typename type_t>
	template<typename... args_t>
	typename mem<type_t>::alloc_t		mem<type_t>::alloc(size_t align, outland::allocator_t* allocator, args_t&&... args)
	{
		return new(allocator->alloc(sizeof(type_t), align), align) type_t(u::forward<args_t>(args)...);
	}

	//--------------------------------------------------
	template<typename type_t>
	void mem<type_t>::free(outland::allocator_t* allocator, alloc_t allocation)
	{
		allocation->~type_t();
		allocator->free(allocation, sizeof(type_t));
	}

	//////////////////////////////////////////////////
	// MEM ARRAY
	//////////////////////////////////////////////////

	//--------------------------------------------------
	template<typename type_t>
	template<typename... args_t>
	typename mem<type_t[]>::alloc_t		mem<type_t[]>::alloc(outland::allocator_t* allocator, array_size_t size, args_t&&... args)
	{
		type_t* ptr = reinterpret_cast<type_t*>(allocator->alloc(size * sizeof(type_t), alignof(type_t)));
		if (ptr)
		{
			while (size)
			{
				--size;
				new(ptr + size, alignof(type_t)) type_t(u::forward<args_t>(args)...);
			}
		}

		return ptr;
	}

	//--------------------------------------------------
	template<typename type_t>
	template<typename... args_t>
	typename mem<type_t[]>::alloc_t		mem<type_t[]>::alloc(size_t align, outland::allocator_t* allocator, array_size_t size, args_t&&... args)
	{
		type_t* ptr = reinterpret_cast<type_t*>(allocator->alloc(size * sizeof(type_t), align));
		if (ptr)
		{
			while (size)
			{
				--size;
				new(ptr + size, alignof(type_t)) type_t(u::forward<args_t>(args)...);
			}
		}

		return ptr;
	}

	//--------------------------------------------------
	template<typename type_t>
	void	mem<type_t[]>::free(outland::allocator_t* allocator, alloc_t allocation, array_size_t size)
	{
		while (size)
		{
			--size;
			allocation[size].~type_t();
		}

		allocator->free(allocation, size * sizeof(type_t));
	}

	//////////////////////////////////////////////////
	// MEM DYNAMIC
	//////////////////////////////////////////////////

	//--------------------------------------------------
	template<typename... types_t>
	template<typename element_array_size_t, size_t i>
	struct mem<types_t...>::_array_helper_t
	{
		using type_t = typename u::pack_type_at_t<i, typename mem<types_t>::element_t...>::type_t;

		//--------------------------------------------------
		static size_t size(typename mem::array_size_t const& size)
		{
			return sizeof(type_t) * size.at<i>();
		}

		//--------------------------------------------------
		static void construct(void* buffer, size_t* offset, typename mem::alloc_t& value, typename mem::array_size_t const& size)
		{
			type_t* ptr = reinterpret_cast<type_t*>(reinterpret_cast<byte_t*>(buffer) + offset[i]);

			for (size_t j = 0, count = size.at<i>(); j < count; ++j)
			{
				new(ptr + j, alignof(type_t)) type_t;
			}

			value.at<i>() = ptr;
		}

		//--------------------------------------------------
		static void destruct(typename mem::alloc_t const& value, typename mem::array_size_t const& size)
		{
			type_t* ptr = value.at<i>();

			for (size_t j = 0, count = size.at<i>(); j < count; ++j)
			{
				ptr[j].~type_t();
			}
		}

		//--------------------------------------------------
		static void make_null(typename mem::alloc_t& value)
		{
			value.at<i>() = nullptr;
		}
	};

	//--------------------------------------------------
	template<typename... types_t>
	template<size_t i>
	struct mem<types_t...>::_array_helper_t<void, i>
	{
		using type_t = typename u::pack_type_at_t<i, typename mem<types_t>::element_t...>::type_t;

		//--------------------------------------------------
		static size_t size(typename mem::array_size_t const& size)
		{
			return sizeof(type_t);
		}

		//--------------------------------------------------
		static void construct(void* buffer, size_t* offset, typename mem::alloc_t& value, typename mem::array_size_t const& size)
		{
			type_t* ptr = reinterpret_cast<type_t*>(reinterpret_cast<byte_t*>(buffer) + offset[i]);

			new(ptr, alignof(type_t)) type_t;

			value.at<i>() = ptr;
		}

		//--------------------------------------------------
		static void destruct(typename mem::alloc_t const& value, typename mem::array_size_t const& size)
		{
			type_t* ptr = value.at<i>();

			ptr->~type_t();
		}

		//--------------------------------------------------
		static void make_null(typename mem::alloc_t& value)
		{
			value.at<i>() = nullptr;
		}
	};

	//--------------------------------------------------
	template<typename... types_t>
	template<size_t... i>
	typename mem<types_t...>::alloc_t		mem<types_t...>::_alloc(
		size_t align,
		outland::allocator_t* allocator, 
		array_size_t const& size, 
		u::index_pack_t<i...>)
	{
		size_t align_arr[] = { alignof(typename mem<types_t>::element_t)... };
		size_t size_arr[] = { _array_helper_t<typename mem<types_t>::array_size_t, i>::size(size)... };
		size_t offset_arr[sizeof...(types_t)];
		size_t size_sum;
		size_t align_max;
		u::mem_offsets(align_arr, size_arr, offset_arr, align_max, size_sum, sizeof...(types_t), align);
		
		void* buffer = allocator->alloc(size_sum, align_max);
		alloc_t value;

		using dummy_t = int[];

		if (nullptr == buffer)
		{
			static_cast<void>(dummy_t{ (_array_helper_t<typename mem<types_t>::array_size_t, i>::make_null(value), 0)... });
		}
		else
		{
			static_cast<void>(dummy_t{ (_array_helper_t<typename mem<types_t>::array_size_t, i>::construct(buffer, offset_arr, value, size), 0)... });
		}

		return value;
	}

	//--------------------------------------------------
	template<typename... types_t>
	template<size_t... i>
	void									mem<types_t...>::_free(
		outland::allocator_t* allocator, 
		alloc_t const& allocation,
		array_size_t const& size,
		u::index_pack_t<i...>)
	{
		void* buffer = allocation.at<0>();

		{
			int dummy[] = { (_array_helper_t<typename mem<types_t>::array_size_t, i>::destruct(allocation, size), 0)... };
			static_cast<void>(dummy);
		}

		size_t buffer_size = reinterpret_cast<byte_t const*>(allocation.at<sizeof...(types_t) - 1>()) - reinterpret_cast<byte_t const*>(buffer);
		buffer_size += _array_helper_t<typename array_size_t::type_at<sizeof...(types_t) - 1>, sizeof...(types_t) - 1>::size(size);

		allocator->free(buffer, buffer_size);
	}

	//--------------------------------------------------
	template<typename... types_t>
	typename mem<types_t...>::alloc_t		mem<types_t...>::alloc(
		outland::allocator_t* allocator, 
		array_size_t size)
	{
		return _alloc(0, allocator, size, typename u::index_builder_t<sizeof...(types_t)>::type_t());
	}

	//--------------------------------------------------
	template<typename... types_t>
	typename mem<types_t...>::alloc_t		mem<types_t...>::alloc(
		size_t align, 
		outland::allocator_t* allocator, 
		array_size_t size)
	{
		return _alloc(align, allocator, size, typename u::index_builder_t<sizeof...(types_t)>::type_t());
	}

	//--------------------------------------------------
	template<typename... types_t>
	void									mem<types_t...>::free(
		outland::allocator_t* allocator,
		alloc_t const& allocation,
		array_size_t size)
	{	
		_free(allocator, allocation, size, typename u::index_builder_t<sizeof...(types_t)>::type_t());
	}

}
