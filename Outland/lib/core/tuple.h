#pragma once
#include "utility.h"

namespace outland
{

	template<typename... args_t>
	class tuple
	{
	public:
		template<size_t i>
		using type_at = typename u::pack_type_at_t<i, args_t...>::type_t;

	private:
		template<size_t i, typename element_t>
		struct _indexed_element { element_t _element; };

		template<size_t i>
		struct _indexed_element<i, void> {};

		template<size_t i>
		struct _container : _container<i + 1>, _indexed_element<i, type_at<i>> {};
		
		template<>
		struct _container<sizeof...(args_t)> {};
	
	private:
		_container<0> _container;

	public:

		template<size_t i>
		typename type_at<i>& at() { return _container._indexed_element<i, type_at<i>>::_element; }

		template<size_t i>
		typename type_at<i> const& at() const { return _container._indexed_element<i, type_at<i>>::_element; }
	};



}