#pragma once
#include "platform\types.h"

namespace outland
{
	namespace u
	{
		template<size_t size>
		struct uint_type_t;

		template<> struct uint_type_t<1> { using type_t = uint8_t; };
		template<> struct uint_type_t<2> { using type_t = uint16_t; };
		template<> struct uint_type_t<4> { using type_t = uint32_t; };
		template<> struct uint_type_t<8> { using type_t = uint64_t; };

		template<size_t size>
		struct int_type_t;

		template<> struct int_type_t<1> { using type_t = int8_t; };
		template<> struct int_type_t<2> { using type_t = int16_t; };
		template<> struct int_type_t<4> { using type_t = int32_t; };
		template<> struct int_type_t<8> { using type_t = int64_t; };
	}

	using char8_t = char;
	using byte_t = unsigned char;
	using size_t = decltype(sizeof(void*));
	using ptrdiff_t = decltype((char*)0 - (char*)0);


	using nullptr_t = decltype(nullptr);
	using uintptr_t = u::uint_type_t<sizeof(nullptr_t)>::type_t;
	using intptr_t = u::int_type_t<sizeof(nullptr_t)>::type_t;

	using error_t = uint32_t;

	//////////////////////////////////////////////////
	// LITERAL FUNCTIONS
	//////////////////////////////////////////////////

	inline constexpr const uint8_t operator "" _u8(unsigned long long int value) { return uint8_t(value); }
	inline constexpr const uint16_t operator "" _u16(unsigned long long int value) { return uint16_t(value); }
	inline constexpr const uint32_t operator "" _u32(unsigned long long int value) { return uint32_t(value); }
	inline constexpr const uint64_t operator "" _u64(unsigned long long int value) { return uint64_t(value); }

	inline constexpr const int8_t operator "" _i8(unsigned long long int value) { return int8_t(value); }
	inline constexpr const int16_t operator "" _i16(unsigned long long int value) { return int16_t(value); }
	inline constexpr const int32_t operator "" _i32(unsigned long long int value) { return int32_t(value); }
	inline constexpr const int64_t operator "" _i64(unsigned long long int value) { return int64_t(value); }

	//////////////////////////////////////////////////
	// CONSTANTS
	//////////////////////////////////////////////////

	namespace c
	{
		enum : size_t { max_size = size_t(-1) };
		enum : byte_t { max_byte = 0xFF_u8 };

		enum : uint8_t { max_uint8 = 0xFF_u8 };
		enum : int8_t { max_int8 = 0x7F_i8 };
		enum : int8_t { min_int8 = 0x80_i8 };

		enum : uint16_t { max_uint16 = 0xFFFF_u16 };
		enum : int16_t { max_int16 = 0x7FFF_i16 };
		enum : int16_t { min_int16 = 0x8000_i16 };

		enum : uint32_t { max_uint32 = 0xFFFFFFFF_u32 };
		enum : int32_t { max_int32 = 0x7FFFFFFF_i32 };
		enum : int32_t { min_int32 = 0x80000000_i32 };

		enum : uint64_t { max_uint64 = 0xFFFFFFFFFFFFFFFF_u64 };
		enum : int64_t { max_int64 = 0x7FFFFFFFFFFFFFFF_i64 };
		enum : int64_t { min_int64 = 0x8000000000000000_i64 };
	}
}