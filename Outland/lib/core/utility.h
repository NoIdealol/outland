#pragma once
#include "types.h"
#include "placement_new.h"

namespace outland { namespace u
{
	//////////////////////////////////////////////////
	// ARRAY UTILITY
	//////////////////////////////////////////////////

	//--------------------------------------------------
	template<typename type_t, size_t n>
	O_INLINE constexpr size_t array_size(type_t const(&)[n])
	{
		return n;
	}

	//--------------------------------------------------
	template<typename type_t>
	O_INLINE constexpr size_t array_size(type_t const* begin, type_t const* end)
	{
		return (end - begin);
	}

	//--------------------------------------------------
	template<typename element_t>
	void array_copy(element_t* dst, element_t const* begin, size_t size)
	{
		element_t const* const end = begin + size;
		while (begin != end)
		{
			new (dst, alignof(element_t)) element_t(*begin);
			++begin;
			++dst;
		}
	}

	//--------------------------------------------------
	template<typename element_t>
	void array_move(element_t* dst, element_t* begin, size_t size)
	{
		element_t* const end = begin + size;
		while (begin != end)
		{
			new(dst, alignof(element_t)) element_t(u::move(*begin));
			begin->~element_t();
			++begin;
			++dst;
		}
	}

	//--------------------------------------------------
	template<typename element_t>
	void array_move_reverse(element_t* dst, element_t* begin, size_t size)
	{
		element_t* end = begin + size;
		dst += size;

		while (begin != end)
		{
			--end;
			--dst;
			new(dst, alignof(element_t)) element_t(u::move(*end));
			end->~element_t();
		}
	}

	//--------------------------------------------------
	template<typename element_t>
	void array_destruct(element_t* begin, size_t size)
	{
		element_t* const end = begin + size;
		while (begin != end)
		{
			begin->~element_t();
			++begin;
		}
	}

	//--------------------------------------------------
	template<typename element_t>
	void array_construct(element_t* begin, size_t size)
	{
		element_t* const end = begin + size;
		while (begin != end)
		{
			new(begin, alignof(element_t)) element_t();
			++begin;
		}
	}

	//--------------------------------------------------
	template<typename element_t, typename... args_t>
	void array_construct(element_t* begin, size_t size, args_t&&... args)
	{
		element_t* const end = begin + size;
		while (begin != end)
		{
			new(begin, alignof(element_t)) element_t(u::forward<args_t>(args)...);
			++begin;
		}
	}

	//////////////////////////////////////////////////
	// BOOL TYPES
	//////////////////////////////////////////////////

	struct true_t
	{
		constexpr static const bool value = true;
	};

	struct false_t
	{
		constexpr static const bool value = false;
	};

	//////////////////////////////////////////////////
	// IS SAME
	//////////////////////////////////////////////////

	template<typename T1, typename T2>
	struct is_same_t : false_t
	{
		using type_t = false_t;
	};

	template<typename T>
	struct is_same_t<T, T> : true_t
	{
		using type_t = true_t;
	};


	//////////////////////////////////////////////////
	// IS POINTER
	//////////////////////////////////////////////////

	template<typename T>
	struct is_pointer_t : false_t
	{
		using type_t = false_t;
	};

	template<typename T>
	struct is_pointer_t<T*> : true_t
	{
		using type_t = true_t;
	};

	//////////////////////////////////////////////////
	// REMOVE POINTER
	//////////////////////////////////////////////////

	template<typename T>
	struct remove_pointer_t
	{
		using type_t = T;
	};

	template<typename T>
	struct remove_pointer_t<T*>
	{
		using type_t = T;
	};

	//////////////////////////////////////////////////
	// IS REFERENCE
	//////////////////////////////////////////////////

	// TEMPLATE CLASS is_lvalue_reference
	template<typename T>
	struct is_lvalue_reference_t : false_t
	{	// determine whether _Ty is an lvalue reference
		using type_t = false_t;
	};

	template<typename T>
	struct is_lvalue_reference_t<T&> : true_t
	{	// determine whether _Ty is an lvalue reference
		using type_t = true_t;
	};

	// TEMPLATE CLASS is_rvalue_reference
	template<typename T>
	struct is_rvalue_reference_t : false_t
	{	// determine whether _Ty is an rvalue reference
		using type_t = false_t;
	};

	template<typename T>
	struct is_rvalue_reference_t<T&&> : true_t
	{	// determine whether _Ty is an rvalue reference
		using type_t = true_t;
	};

	//////////////////////////////////////////////////
	// REMOVE REFERENCE
	//////////////////////////////////////////////////

	template<typename T>
	struct remove_reference_t
	{	// remove reference
		using type_t = T;
	};

	template<typename T>
	struct remove_reference_t<T&>
	{	// remove reference
		using type_t = T;
	};

	template<typename T>
	struct remove_reference_t<T&&>
	{	// remove rvalue reference
		using type_t = T;
	};

	//////////////////////////////////////////////////
	// MOVE
	//////////////////////////////////////////////////

	template<typename T>
	constexpr typename remove_reference_t<T>::type_t&& move(T&& arg)
	{	// forward arg as movable
		return (static_cast<typename remove_reference_t<T>::type_t&&>(arg));
	}

	//////////////////////////////////////////////////
	// FORWARD
	//////////////////////////////////////////////////

	// TEMPLATE FUNCTION forward
	template<typename T> inline
	constexpr T&& forward(typename remove_reference_t<T>::type_t& arg)
	{	// forward an lvalue as either an lvalue or an rvalue
		return (static_cast<T&&>(arg));
	}

	template<typename T> inline
	constexpr T&& forward(typename remove_reference_t<T>::type_t&& arg)
	{	// forward an rvalue as an rvalue
		static_assert(!is_lvalue_reference_t<T>::value, "bad forward call");
		return (static_cast<T&&>(arg));
	}

	//////////////////////////////////////////////////
	// PACK TYPE AT
	//////////////////////////////////////////////////

	template<size_t index, typename arg0_t, typename... args_t>
	struct pack_type_at_t
	{
		using type_t = typename pack_type_at_t<index - 1, args_t...>::type_t;
	};

	template<typename arg0_t, typename... args_t>
	struct pack_type_at_t<0, arg0_t, args_t...>
	{
		using type_t = arg0_t;
	};

	//////////////////////////////////////////////////
	// indices
	//////////////////////////////////////////////////

	template<size_t... i>
	struct index_pack_t
	{
	};


	template<size_t count, size_t... i>
	struct index_builder_t
	{
		using type_t = typename index_builder_t<count - 1, count - 1, i...>::type_t;
	};

	template<size_t... i>
	struct index_builder_t<0, i...>
	{
		using type_t = typename index_pack_t<i...>;
	};

} }