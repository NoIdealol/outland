#pragma once
#include "allocator.h"

namespace outland
{
	//////////////////////////////////////////////////
	// ARRAY
	//////////////////////////////////////////////////

	template<typename element_t>
	class array
	{
	public:
		using iterator_t = element_t*;
		using iterator_const_t = element_t const*;

	private:
		allocator_t*	_allocator;
		element_t*		_begin;
		size_t			_size;
		size_t			_capacity;

	private:
		element_t*	_alloc(size_t size, size_t& capacity);
		void		_free();
		void		_store(element_t* begin, size_t size, size_t& capacity);
		bool		_soo_enabled() const;
		size_t		_soo_capacity() const;
		element_t*			_soo_buffer(size_t& capacity) const;
		element_t const*	_soo_buffer(size_t const& capacity) const;

		size_t		_free_capacity() const;
		size_t		_expand() const;
		size_t		_expand(size_t min_size) const;

	public:
		array(class allocator_t* allocator);
		array(class allocator_t* allocator, size_t reserve, error_t& err);
		array(class allocator_t* allocator, array const& other, error_t& err);
		array(array const& other, error_t& err);
		array(array&& other);
		~array();

		error_t operator = (array const& other);
		array& operator = (array&& other);

		allocator_t*	allocator() const { return _allocator; }

		iterator_t		begin() { return _begin; }
		iterator_t		end() { return _begin + _size; }
		element_t&		first() { return *_begin; }
		element_t&		last() { return *(_begin + _size - 1); }
		element_t*		data() { return _begin; }
		element_t&		operator [] (size_t index) { return _begin[index]; }

		iterator_const_t	begin() const { return _begin; }
		iterator_const_t	end() const { return _begin + _size; }
		element_t const&	first() const { return *_begin; }
		element_t const&	last() const { return *(_begin + _size - 1); }
		element_t const*	data() const { return _begin; }
		element_t const&	operator [] (size_t index) const { return _begin[index]; }

		size_t			capacity() const;
		size_t			size() const { return _size; }


		error_t			compact();
		error_t			resize(size_t size);
		error_t			reserve(size_t size);
		void			clear();

		void			swap(size_t first, size_t second);
		void			swap(iterator_t first, iterator_t second);

		template<typename... args_t>
		error_t			push_back(args_t&&... args);
		template<typename... args_t>
		error_t			insert(size_t index, args_t&&... args);
		template<typename... args_t>
		error_t			insert(iterator_t at, args_t&&... args);

		template<typename... args_t>
		error_t			push_back_array(size_t size, args_t&&... args);
		error_t			push_back_array(iterator_const_t begin, iterator_const_t end);
		error_t			push_back_array(const element_t* data, size_t size);

		template<typename... args_t>
		error_t			insert_array(iterator_t at, size_t size, args_t&&... args);
		error_t			insert_array(iterator_t at, iterator_const_t begin, iterator_const_t end);
		error_t			insert_array(iterator_t at, const element_t* data, size_t size);

		template<typename... args_t>
		error_t			insert_array(size_t index, size_t size, args_t&&... args);
		error_t			insert_array(size_t index, iterator_const_t begin, iterator_const_t end);
		error_t			insert_array(size_t index, const element_t* data, size_t size);

		void			pop_back();
		void			remove(size_t index);
		void			remove(iterator_t at);

		void			remove_swap(size_t index);
		void			remove_swap(iterator_t at);

		void			pop_back_array(size_t size);
		void			pop_back_array(iterator_t begin);

		void			remove_array(iterator_t at, size_t size);
		void			remove_array(iterator_t at, iterator_t end);

		void			remove_array(size_t index, size_t size);
		void			remove_array(size_t index, iterator_t end);
	};

}
#include "array_impl.h"
