#pragma once

namespace outland { namespace m
{
	//////////////////////////////////////////////////
	// INTEGER
	//////////////////////////////////////////////////

	uint32_t align(uint32_t value, uint32_t align);
	uint32_t to_pow2(uint32_t value);
	uint32_t to_pow2(uint32_t value, size_t min_power);
	uint32_t bit_top(uint32_t value);
	size_t bit_index(uint32_t value);
	size_t bit_count(uint32_t value);
	size_t bit_lowest_index(uint32_t value);

	uint64_t align(uint64_t value, uint64_t align);
	uint64_t to_pow2(uint64_t value);
	uint64_t to_pow2(uint64_t value, size_t min_power);
	uint64_t bit_top(uint64_t value);
	size_t bit_index(uint64_t value);

	int8_t abs(int8_t value);
	int16_t abs(int16_t value);
	int32_t abs(int32_t value);
	int64_t abs(int64_t value);

	int8_t  sign(int8_t value);
	int16_t sign(int16_t value);
	int32_t sign(int32_t value);
	int64_t sign(int64_t value);

	O_INLINE int32_t min(int32_t v1, int32_t v2) { return v1 < v2 ? v1 : v2; }
	O_INLINE uint32_t min(uint32_t v1, uint32_t v2) { return v1 < v2 ? v1 : v2; }
	O_INLINE int64_t min(int64_t v1, int64_t v2) { return v1 < v2 ? v1 : v2; }
	O_INLINE uint64_t min(uint64_t v1, uint64_t v2) { return v1 < v2 ? v1 : v2; }

	O_INLINE int32_t max(int32_t v1, int32_t v2) { return v1 > v2 ? v1 : v2; }
	O_INLINE uint32_t max(uint32_t v1, uint32_t v2) { return v1 > v2 ? v1 : v2; }
	O_INLINE int64_t max(int64_t v1, int64_t v2) { return v1 > v2 ? v1 : v2; }
	O_INLINE uint64_t max(uint64_t v1, uint64_t v2) { return v1 > v2 ? v1 : v2; }


	O_INLINE int32_t sq(int32_t v) { return v * v; }
	O_INLINE uint32_t sq(uint32_t v) { return v * v; }
	O_INLINE int64_t sq(int64_t v) { return v * v; }
	O_INLINE uint64_t sq(uint64_t v) { return v * v; }

} }