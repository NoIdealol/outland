#pragma once

namespace outland
{
	using float32_t = float;

	namespace m
	{
		inline float32_t	min(float32_t v1, float32_t v2);
		inline float32_t	max(float32_t v1, float32_t v2);
		
		float32_t trunc(float32_t value);
		float32_t floor(float32_t value);
		float32_t frac(float32_t value);
		float32_t abs(float32_t value);
		float32_t sign(float32_t value);
		float32_t fast_inv_sqrt(float32_t value);
		float32_t fast_sqrt(float32_t value);
		float32_t sqrt(float32_t value);
		float32_t fast_sin(float32_t rad);
		float32_t sin(float32_t rad);
		float32_t fast_cos(float32_t rad);
		float32_t cos(float32_t rad);
	}

	namespace m
	{
		inline float32_t	min(float32_t v1, float32_t v2) { return v1 > v2 ? v2 : v1; }
		inline float32_t	max(float32_t v1, float32_t v2) { return v1 < v2 ? v2 : v1; }
	}
}