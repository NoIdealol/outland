#pragma once

namespace outland
{
	template<typename scalar_t>
	class vector3;

	namespace m
	{
		template<typename scalar_t>
		scalar_t			dot(const vector3<scalar_t>& lhs, const vector3<scalar_t>& rhs);
		template<typename scalar_t>
		vector3<scalar_t>	cross(const vector3<scalar_t>& lhs, const vector3<scalar_t>& rhs);
		template<typename scalar_t>
		vector3<scalar_t>	norm(const vector3<scalar_t>& val);
		template<typename scalar_t>
		scalar_t			len(const vector3<scalar_t>& val);
	}

	template<typename scalar_t>
	class vector3
	{
		friend scalar_t				m::dot(const vector3<scalar_t>& lhs, const vector3<scalar_t>& rhs);
		friend vector3<scalar_t>	m::cross(const vector3<scalar_t>& lhs, const vector3<scalar_t>& rhs);
		friend vector3<scalar_t>	m::norm(const vector3<scalar_t>& val);
		friend scalar_t				m::len(const vector3<scalar_t>& val);

	private:
		scalar_t _data[3];

	public:
		vector3(scalar_t, scalar_t, scalar_t);
		vector3() = default;

		//operators
		vector3&	operator =  (const vector3&);
		vector3&	operator += (const vector3&);
		vector3&	operator += (const scalar_t);
		vector3&	operator -= (const vector3&);
		vector3&	operator -= (const scalar_t);
		vector3&	operator *= (const vector3&);
		vector3&	operator *= (const scalar_t);
		vector3&	operator /= (const vector3&);
		vector3&	operator /= (const scalar_t);


		vector3	operator + (const vector3&) const;
		vector3	operator + (const scalar_t) const;
		vector3	operator - (const vector3&) const;
		vector3	operator - (const scalar_t) const;
		vector3	operator * (const vector3&) const;
		vector3	operator * (const scalar_t) const;
		vector3	operator / (const vector3&) const;
		vector3	operator / (const scalar_t) const;

		inline const vector3& operator + () const { return *this; }//unary.. do nothing
		vector3	operator - () const;//unary.. inverse

		//comparison
		bool		operator == (const vector3&) const;
		bool		operator != (const vector3&) const;
		bool		operator <  (const vector3&) const;
		bool		operator <= (const vector3&) const;
		bool		operator >  (const vector3&) const;
		bool		operator >= (const vector3&) const;

		scalar_t&		operator [] (size_t index);
		const scalar_t&	operator [] (size_t index) const;
	};

	template<typename scalar_t>
	vector3<scalar_t>	operator + (const scalar_t, const vector3<scalar_t>&);
	template<typename scalar_t>
	vector3<scalar_t>	operator - (const scalar_t, const vector3<scalar_t>&);
	template<typename scalar_t>
	vector3<scalar_t>	operator * (const scalar_t, const vector3<scalar_t>&);

	
}
