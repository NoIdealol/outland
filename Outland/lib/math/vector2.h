#pragma once

namespace outland
{
	//////////////////////////////////////////////////////////////////////////////////
	// VECTOR 2
	//////////////////////////////////////////////////////////////////////////////////

	template<typename scalar_t>
	class vector2;

	namespace m
	{
		template<typename scalar_t>
		scalar_t			dot(const vector2<scalar_t>& lhs, const vector2<scalar_t>& rhs);
		template<typename scalar_t>
		vector2<scalar_t>	norm(const vector2<scalar_t>& val);
		template<typename scalar_t>
		scalar_t			len(const vector2<scalar_t>& val);
	}

	template<typename scalar_t>
	class vector2
	{
		friend scalar_t				m::dot(const vector2<scalar_t>& lhs, const vector2<scalar_t>& rhs);
		friend vector2<scalar_t>	m::norm(const vector2<scalar_t>& val);
		friend scalar_t				m::len(const vector2<scalar_t>& val);

	private:
		scalar_t _data[2];

	public:

		vector2(scalar_t, scalar_t);
		vector2() = default;

		//operators
		vector2&	operator =  (const vector2&);
		vector2&	operator += (const vector2&);
		vector2&	operator += (const scalar_t);
		vector2&	operator -= (const vector2&);
		vector2&	operator -= (const scalar_t);
		vector2&	operator *= (const vector2&);
		vector2&	operator *= (const scalar_t);
		vector2&	operator /= (const vector2&);
		vector2&	operator /= (const scalar_t);


		vector2	operator + (const vector2&) const;
		vector2	operator + (const scalar_t) const;
		vector2	operator - (const vector2&) const;
		vector2	operator - (const scalar_t) const;
		vector2	operator * (const vector2&) const;
		vector2	operator * (const scalar_t) const;
		vector2	operator / (const vector2&) const;
		vector2	operator / (const scalar_t) const;

		inline const vector2& operator + () const { return *this; }//unary.. do nothing
		vector2	operator - () const;//unary.. inverse

		//comparison
		bool		operator == (const vector2&) const;
		bool		operator != (const vector2&) const;
		bool		operator <  (const vector2&) const;
		bool		operator <= (const vector2&) const;
		bool		operator >  (const vector2&) const;
		bool		operator >= (const vector2&) const;

		scalar_t&		operator [] (size_t index);
		const scalar_t&	operator [] (size_t index) const;
	};

	template<typename scalar_t>
	vector2<scalar_t>	operator + (const scalar_t, const vector2<scalar_t>&);
	template<typename scalar_t>
	vector2<scalar_t>	operator - (const scalar_t, const vector2<scalar_t>&);
	template<typename scalar_t>
	vector2<scalar_t>	operator * (const scalar_t, const vector2<scalar_t>&);

	//////////////////////////////////////////////////////////////////////////////////
	// MATRIX 2
	//////////////////////////////////////////////////////////////////////////////////

	template<typename scalar_t>
	class matrix2;

	namespace m
	{
		template<typename scalar_t>
		vector2<scalar_t>	mul(const matrix2<scalar_t>& lhs, const vector2<scalar_t>& rhs);

		template<typename scalar_t>
		vector2<scalar_t>	col(const matrix2<scalar_t>& val, size_t index);

		template<typename scalar_t>
		const vector2<scalar_t>&	row(const matrix2<scalar_t>& val, size_t index);

		template<typename scalar_t>
		matrix2<scalar_t>	transp(const matrix2<scalar_t>& val);
	}

	template<typename scalar_t>
	class matrix2
	{
		friend vector2<scalar_t>			m::mul<scalar_t>(const matrix2<scalar_t>& lhs, const vector2<scalar_t>& rhs);
		friend vector2<scalar_t>			m::col<scalar_t>(const matrix2<scalar_t>& val, size_t index);
		friend const vector2<scalar_t>&		m::row<scalar_t>(const matrix2<scalar_t>& val, size_t index);
		friend matrix2<scalar_t>			m::transp<scalar_t>(const matrix2<scalar_t>& val);
	public:
		static matrix2 zero;
		static matrix2 eye;
		static matrix2 one;

	private:
		vector2<scalar_t> _data[2];

	public:
		matrix2(scalar_t rad);
		matrix2(scalar_t c00, scalar_t c01, scalar_t c10, scalar_t c11);
		matrix2(const vector2<scalar_t>& r0, const vector2<scalar_t>& r1);
		matrix2() = default;

		//operators
		matrix2&	operator =  (const matrix2&);
		matrix2&	operator += (const matrix2&);
		matrix2&	operator += (const scalar_t);
		matrix2&	operator -= (const matrix2&);
		matrix2&	operator -= (const scalar_t);
		matrix2&	operator *= (const matrix2&);
		matrix2&	operator *= (const scalar_t);
		matrix2&	operator /= (const matrix2&);
		matrix2&	operator /= (const scalar_t);


		matrix2	operator + (const matrix2&) const;
		matrix2	operator + (const scalar_t) const;
		matrix2	operator - (const matrix2&) const;
		matrix2	operator - (const scalar_t) const;
		matrix2	operator * (const matrix2&) const;
		matrix2	operator * (const scalar_t) const;
		matrix2	operator / (const matrix2&) const;
		matrix2	operator / (const scalar_t) const;

		inline const matrix2& operator + () const { return *this; }//unary.. do nothing
		matrix2	operator - () const;//unary.. inverse

		//comparison
		bool		operator == (const matrix2&) const;
		bool		operator != (const matrix2&) const;
		
		const vector2<scalar_t>&	operator [] (size_t row) const;
	};

	template<typename scalar_t>
	matrix2<scalar_t>	operator + (const scalar_t, const matrix2<scalar_t>&);
	template<typename scalar_t>
	matrix2<scalar_t>	operator - (const scalar_t, const matrix2<scalar_t>&);
	template<typename scalar_t>
	matrix2<scalar_t>	operator * (const scalar_t, const matrix2<scalar_t>&);

}
