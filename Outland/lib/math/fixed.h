#pragma once

namespace outland
{
	class fixed_t;

	namespace m
	{
		O_INLINE fixed_t	min(fixed_t v1, fixed_t v2);
		O_INLINE fixed_t	max(fixed_t v1, fixed_t v2);
		O_INLINE uint16_t	decimal(fixed_t value);
		O_INLINE int32_t	representation(fixed_t value);
		O_INLINE fixed_t	floor(fixed_t value);
		O_INLINE fixed_t	frac(fixed_t value);

		fixed_t	abs(fixed_t value);
		fixed_t	sign(fixed_t value);
		fixed_t fast_inv_sqrt(fixed_t value);
		fixed_t fast_sqrt(fixed_t value);
		fixed_t sqrt(fixed_t value);
		fixed_t fast_sin(fixed_t rad);
		fixed_t sin(fixed_t rad);
		fixed_t fast_cos(fixed_t rad);
		fixed_t cos(fixed_t rad);
	}

	//////////////////////////////////////////////////
	// FIXED POINT TYPE
	//////////////////////////////////////////////////

	class fixed_t
	{
	public:
		friend fixed_t			m::floor(fixed_t value);
		friend fixed_t			m::frac(fixed_t value);
		friend uint16_t			m::decimal(fixed_t value);
		friend int32_t			m::representation(fixed_t value);
		friend fixed_t			m::abs(fixed_t value);
		friend fixed_t			m::sign(fixed_t value);
		friend fixed_t			m::fast_inv_sqrt(fixed_t value);
		friend fixed_t			m::fast_sqrt(fixed_t value);
		friend fixed_t			m::sqrt(fixed_t value);
		friend fixed_t			m::fast_sin(fixed_t rad);
		friend fixed_t			m::sin(fixed_t rad);
		friend fixed_t			m::fast_cos(fixed_t rad);
		friend fixed_t			m::cos(fixed_t rad);

		enum : int32_t 
		{ 
			precision = 16,
			shift = 1 << precision,
			frac = (1 << precision) - 1,
			floor = ~frac,
		};

	private:
		int32_t _data;

	public:
		O_INLINE constexpr fixed_t(long double value)				: _data((int32_t)(value * shift)) {}
		O_INLINE constexpr fixed_t(unsigned long long int value)	: _data((int32_t)(value << precision)) {}
		O_INLINE constexpr fixed_t(int32_t value)					: _data(value << precision) {}
		O_INLINE constexpr fixed_t(double value)					: _data((int32_t)(value * shift)) {}
		O_INLINE constexpr fixed_t(float value)						: _data((int32_t)(value * shift)) {}
		O_INLINE constexpr fixed_t(int16_t value, uint16_t frac)	: _data(((int32_t)(value) << precision) + frac) {}

		fixed_t() = default;

		O_INLINE fixed_t& operator += (const fixed_t rhs) { _data += rhs._data; return *this; }
		O_INLINE fixed_t& operator -= (const fixed_t rhs) { _data -= rhs._data; return *this; }
		O_INLINE fixed_t& operator *= (const fixed_t rhs) { _data = static_cast<int32_t>((static_cast<int64_t>(_data) * static_cast<int64_t>(rhs._data)) >> precision); return *this; }
		O_INLINE fixed_t& operator /= (const fixed_t rhs) { _data = static_cast<int32_t>((static_cast<int64_t>(_data) << precision) / static_cast<int64_t>(rhs._data)); return *this; }
		O_INLINE fixed_t& operator %= (const fixed_t rhs) { _data = _data % rhs._data; return *this; }
		O_INLINE fixed_t& operator >>= (const size_t rhs) { _data >>= rhs; return *this; }
		O_INLINE fixed_t& operator <<= (const size_t rhs) { _data <<= rhs; return *this; }

		O_INLINE fixed_t operator + (const fixed_t rhs) const { return fixed_t(*this) += rhs; }
		O_INLINE fixed_t operator - (const fixed_t rhs) const { return fixed_t(*this) -= rhs; }
		O_INLINE fixed_t operator * (const fixed_t rhs) const { return fixed_t(*this) *= rhs; }
		O_INLINE fixed_t operator / (const fixed_t rhs) const { return fixed_t(*this) /= rhs; }
		O_INLINE fixed_t operator % (const fixed_t rhs) const { return fixed_t(*this) %= rhs; }
		O_INLINE fixed_t operator >> (const size_t rhs) const { return fixed_t(*this) >>= rhs; }
		O_INLINE fixed_t operator << (const size_t rhs) const { return fixed_t(*this) <<= rhs; }

		O_INLINE fixed_t operator + () const { return *this; }
		O_INLINE fixed_t operator - () const { fixed_t ret; ret._data = -_data; return ret; };

		O_INLINE bool operator == (const fixed_t rhs) const { return _data == rhs._data; }
		O_INLINE bool operator != (const fixed_t rhs) const { return _data != rhs._data; }
		O_INLINE bool operator <= (const fixed_t rhs) const { return _data <= rhs._data; }
		O_INLINE bool operator >= (const fixed_t rhs) const { return _data >= rhs._data; }
		O_INLINE bool operator <  (const fixed_t rhs) const { return _data <  rhs._data; }
		O_INLINE bool operator >  (const fixed_t rhs) const { return _data >  rhs._data; }

		O_INLINE operator int16_t()	const { return (_data >> precision) & frac; }
		O_INLINE operator float()	const { return static_cast<float>(_data) / (shift); }
		O_INLINE operator double()	const { return static_cast<double>(_data) / (shift); }
	};

	namespace m
	{
		O_INLINE fixed_t	floor(fixed_t value) { value._data &= fixed_t::floor; return value; }
		O_INLINE fixed_t	frac(fixed_t value) { value._data &= fixed_t::frac; return value; }
		O_INLINE fixed_t	min(fixed_t v1, fixed_t v2) { return v1 > v2 ? v2 : v1; }
		O_INLINE fixed_t	max(fixed_t v1, fixed_t v2) { return v1 < v2 ? v2 : v1; }
		O_INLINE uint16_t	decimal(fixed_t value) { return value._data & fixed_t::frac; }
		O_INLINE int32_t	representation(fixed_t value) { return value._data; }
	}
}

//////////////////////////////////////////////////
// LITERAL FUNCTIONS
//////////////////////////////////////////////////

inline constexpr const outland::fixed_t operator "" _p(long double value) { return outland::fixed_t(value); }
inline constexpr const outland::fixed_t operator "" _p(unsigned long long int value) { return outland::fixed_t(value); }
