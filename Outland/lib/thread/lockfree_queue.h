#pragma once

namespace outland
{

	using lockfree_queue_id = struct lockfree_queue_t*;

	namespace lockfree_queue
	{
		struct create_t
		{
			memory_info_t	element_info;
			size_t			element_size;
		};

		void	memory_info(memory_info_t& memory_info, create_t const& create);
		void	create(lockfree_queue_id& queue, create_t const& create, void* memory);
		void	destroy(lockfree_queue_id queue);

		size_t	producer_capacity(lockfree_queue_id queue);
		void*	produce(lockfree_queue_id queue);
		void	producer_flush(lockfree_queue_id queue);
		void	producer_evict(lockfree_queue_id queue);

		size_t	consumer_size(lockfree_queue_id queue);
		void*	consume(lockfree_queue_id queue);
		void	consumer_flush(lockfree_queue_id queue);
		void	consumer_evict(lockfree_queue_id queue);
	}
}