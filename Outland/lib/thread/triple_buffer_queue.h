#pragma once

namespace outland
{

	using triple_buffer_queue_id = struct triple_buffer_queue_t*;

	namespace triple_buffer_queue
	{
		struct create_info_t
		{
			size_t initial_producer_index;
			size_t initial_consumer_index;
			size_t initial_unused_index;
		};

		void	memory_info(memory_info_t& memory_info);
		void	create(triple_buffer_queue_id& queue, create_info_t const& info, void* memory);
		void	destroy(triple_buffer_queue_id queue);
		void	produce(triple_buffer_queue_id queue, size_t& index);
		bool	consume(triple_buffer_queue_id queue, size_t& index);
	}
}
