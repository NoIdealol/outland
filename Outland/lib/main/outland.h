#pragma once

#include "platform\platform.h"
#include "platform\debugger.h"

#include "core\types.h"
#include "core\error.h"
#include "core\array.h"
#include "core\string.h"
#include "core\deque.h"
#include "core\function.h"
#include "core\hash_map.h"
#include "core\utility.h"
#include "core\tuple.h"
#include "core\memory.h"
#include "memory\memory.h"
#include "memory\scratch.h"
