#pragma once

namespace outland
{	
	struct main_args_t
	{
		allocator_t*	allocator;
		size_t			cmd_count;
		char8_t**		cmd_line;
	};

	uint8_t main(main_args_t const& main_args);
}