#pragma once
#include "variant.h"

namespace outland { namespace f
{


	O_INLINE error_t print(string8_t& stream, char8_t const* string);
	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0);
	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1);
	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1, variant_t arg2);
	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1, variant_t arg2, variant_t arg3);
	template<typename... args_t>
	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1, variant_t arg2, variant_t arg3, args_t... args);

	error_t print_va(string8_t& stream, char8_t const* string, variant_t* args, size_t arg_count);


	O_INLINE error_t print(string8_t& stream, char8_t const* string)
	{
		return print_va(stream, string, nullptr, 0);
	}

	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0)
	{
		variant_t va_args[] = { arg0 };
		return print_va(stream, string, va_args, u::array_size(va_args));
	}

	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1)
	{
		variant_t va_args[] = { arg0, arg1 };
		return print_va(stream, string, va_args, u::array_size(va_args));
	}

	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1, variant_t arg2)
	{
		variant_t va_args[] = { arg0, arg1, arg2 };
		return print_va(stream, string, va_args, u::array_size(va_args));
	}

	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1, variant_t arg2, variant_t arg3)
	{
		variant_t va_args[] = { arg0, arg1, arg2, arg3 };
		return print_va(stream, string, va_args, u::array_size(va_args));
	}

	template<typename... args_t>
	O_INLINE error_t print(string8_t& stream, char8_t const* string, variant_t arg0, variant_t arg1, variant_t arg2, variant_t arg3, args_t... args)
	{
		variant_t va_args[] = { arg0, arg1, arg2, arg3, args... };
		return print_va(stream, string, va_args, u::array_size(va_args));
	}

} }
