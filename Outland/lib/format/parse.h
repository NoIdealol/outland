#pragma once

namespace outland { namespace f
{

	uint32_t parse_uint32_d(char8_t const*& str);

} }