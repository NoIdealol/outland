#pragma once
#include "math\fixed.h"
#include "math\floating.h"
#include "math\integer.h"

namespace outland
{
	class variant_t
	{
	public:
		enum type_t
		{
			t_int32,
			t_int64,
			t_uint32,
			t_uint64,
			t_fixed,
			t_float32,
			t_string8,
			t_pointer,
		};

	private:
		union
		{
			int32_t			_i32;
			int64_t			_i64;
			uint32_t		_u32;
			uint64_t		_u64;
			fixed_t			_fix;
			float32_t		_f32;
			void const*		_ptr;
			char8_t const*	_str8;
		};

		type_t	_type;
	public:

		variant_t(int32_t			i32) : _i32(i32), _type(t_int32) {}
		variant_t(int64_t			i64) : _i64(i64), _type(t_int64) {}
		variant_t(uint32_t			u32) : _u32(u32), _type(t_uint32) {}
		variant_t(uint64_t			u64) : _u64(u64), _type(t_uint64) {}
		variant_t(fixed_t			fix) : _fix(fix), _type(t_fixed) {}
		variant_t(float32_t			f32) : _f32(f32), _type(t_float32) {}
		variant_t(void const*		ptr) : _ptr(ptr), _type(t_pointer) {}
		variant_t(char8_t const*	str8) : _str8(str8), _type(t_string8) {}

		type_t type() const { return _type; }

		inline int32_t			get_int32() const { return _i32; };
		inline int64_t			get_int64() const { return _i64; };
		inline uint32_t			get_uint32() const { return _u32; };
		inline uint64_t			get_uint64() const { return _u64; };
		inline float32_t		get_float32() const { return _f32; }
		inline fixed_t			get_fixed() const { return _fix; }
		inline void const*		get_pointer() const { return _ptr; }
		inline char8_t const*	get_string8() const { return _str8; };
	};
}