#pragma once

namespace outland { namespace  os
{

	struct processor_t
	{
		size_t	core_count_logical;
		size_t	core_count_physical;
		size_t	cache_line_size;
	};

	namespace processor
	{
		error_t querry_info(processor_t& processor);
	}

} }
