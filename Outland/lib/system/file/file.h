#pragma once

namespace outland { namespace  os
{

	struct file_t;
	using file_id = file_t*;

	namespace file
	{
		using filesize_t = uint64_t;
		using attributes_t = uint32_t;
		using access_t = uint32_t;

		struct queue_t;
		using queue_id = queue_t*;

		struct operation_t;
		using operation_id = operation_t*;

		struct request_t;
		struct result_t
		{
			error_t			error;
			operation_id	operation;
			size_t			transfered;
		};

		struct request_t
		{
			file_id			file;
			filesize_t		where;
			void*			buffer;
			size_t			size;
			void*			user;
		};

		enum : attributes_t
		{
			att_folder,
			att_read_only,
			att_system,
			att_hidden,
		};

		enum class open_t
		{
			existing,	//fails if does not exist
			try_new,	//fails if exists
			overwrite,	//attempts to overwrite or creates new
			always,		//open or create
		};

		enum : access_t
		{
			access_read = 1 << 0,
			access_write = 1 << 1,
		};

		error_t	open(file_id& file, queue_id q, open_t open, access_t access, const char8_t* path);
		error_t	close(file_id file);
		error_t	remove(const char8_t* path);
		error_t size(file_id file, filesize_t& size);
		error_t resize(file_id file, filesize_t size);

		error_t	read(operation_id operation);
		error_t	write(operation_id operation);

		error_t	folder_create(const char8_t* path);
		error_t	folder_remove(const char8_t* path);

		error_t	queue_create(queue_id& q);
		error_t	queue_destroy(queue_id q);
		void	queue_process(queue_id q, result_t* results_data, size_t results_size, size_t& results_processed);

		error_t	operation_create(operation_id& op);
		error_t	operation_destroy(operation_id op);
		void	operation_set_request(operation_id op, request_t const& request);
		void	operation_get_request(operation_id op, request_t& request);

		error_t	attributes_set(attributes_t attr, const char8_t* path);
		error_t	attributes_get(attributes_t& attr, const char8_t* path);		
	}
	
} }
