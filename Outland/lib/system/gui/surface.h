#pragma once

#if O_PLATFORM_DESKTOP

namespace outland { namespace  os
{
	enum window_id : uintptr_t;

	//////////////////////////////////////////////////
	// COORD
	//////////////////////////////////////////////////

	using coord_t = int32_t;

	//////////////////////////////////////////////////
	// POINT
	//////////////////////////////////////////////////

	struct point_t
	{
		coord_t	x;
		coord_t	y;

		point_t() = default;
		point_t(coord_t x, coord_t y) : x(x), y(y) {}

		//operators
		point_t&		operator += (const point_t& rhs) { x += rhs.x; y += rhs.y; return *this; }
		point_t&		operator += (const coord_t rhs) { x += rhs; y += rhs; return *this; }
		point_t&		operator -= (const point_t& rhs) { x -= rhs.x; y -= rhs.y; return *this; }
		point_t&		operator -= (const coord_t rhs) { x -= rhs; y -= rhs; return *this; }
		point_t&		operator *= (const coord_t rhs) { x *= rhs; y *= rhs; return *this; }
		point_t&		operator /= (const coord_t rhs) { x /= rhs; y /= rhs; return *this; }

		point_t		operator + (const point_t& rhs) const { return point_t(x + rhs.x, y + rhs.y); }
		point_t		operator + (const coord_t rhs) const { return point_t(x + rhs, y + rhs); }
		point_t		operator - (const point_t& rhs) const { return point_t(x - rhs.x, y - rhs.y); }
		point_t		operator - (const coord_t rhs) const { return point_t(x - rhs, y - rhs); }
		point_t		operator * (const coord_t rhs) const { return point_t(x * rhs, y * rhs); }
		point_t		operator / (const coord_t rhs) const { return point_t(x / rhs, y / rhs); }

		point_t		operator + () const { return *this; }
		point_t		operator - () const { return point_t(-x, -y); }

		bool		operator == (const point_t& rhs) const { return x == rhs.x && y == rhs.y; }
		bool		operator != (const point_t& rhs) const { return x != rhs.x || y != rhs.y; }
	};

	//--------------------------------------------------
	inline point_t	operator + (const coord_t lhs, const point_t& rhs) { return point_t(lhs + rhs.x, lhs + rhs.y); }
	inline point_t	operator - (const coord_t lhs, const point_t& rhs) { return point_t(lhs - rhs.x, lhs - rhs.y); }
	inline point_t	operator * (const coord_t lhs, const point_t& rhs) { return point_t(lhs * rhs.x, lhs * rhs.y); }
	
	//////////////////////////////////////////////////
	// RECT
	//////////////////////////////////////////////////

	//--------------------------------------------------
	struct rect_t
	{
		point_t	base;
		point_t	size;
	};

	//////////////////////////////////////////////////
	// CURSOR
	//////////////////////////////////////////////////
	
	struct cursor_t;
	using cursor_id = cursor_t*;

	//////////////////////////////////////////////////
	// COLOR
	//////////////////////////////////////////////////

	using color_t = uint32_t;
	
	//////////////////////////////////////////////////
	// BITMAP
	//////////////////////////////////////////////////

	struct bitmap_t
	{
		enum format_t
		{
			device_dependant_bitmap,	
			b8_g8_r8_x8,
			b8_g8_r8,
		};

		size_t			width;
		size_t			height;
		size_t			scanline; //must be a multiple of 4. padding filled with zeros
		byte_t*			pixels;
		format_t		format;
	};

	//////////////////////////////////////////////////
	// TEXTURE
	//////////////////////////////////////////////////

	struct texture_t;
	using texture_id = texture_t*;

	namespace texture
	{
		extern texture_id const null;

		error_t		create_ddb(texture_id& texture, bitmap_t const& bitmap);
		error_t		create_dib(texture_id& texture, bitmap_t const& bitmap);
		void		destroy(texture_id texture);

		void		get_bitmap(texture_id texture, bitmap_t& bitmap);
		error_t		lock(texture_id texture, bitmap_t& bitmap);
		void		unlock(texture_id texture);
	}

	//////////////////////////////////////////////////
	// SURFACE
	//////////////////////////////////////////////////

	struct surface_t;
	using surface_id = surface_t*;
	
	namespace surface
	{
		error_t		draw_begin(surface_id& surface, texture_id texture);
		error_t		draw_begin(surface_id& surface, window_id window);
		void		draw_end(surface_id surface);

		void		clear(surface_id surface, color_t color);
		void		draw_pixel(surface_id surface, const point_t& position, color_t color);
		void		draw_line(surface_id surface, const point_t& p1, const point_t& p2, color_t color);
		void		draw_ellipse(surface_id surface, const rect_t& dst_rect, color_t color);
		void		draw_rect(surface_id surface, const rect_t& dst_rect, color_t color);
		void		fill_rect(surface_id surface, const rect_t& dst_rect, color_t color);
		void		draw_bitmap(surface_id surface, const rect_t& dst_rect, const point_t& src_base, const bitmap_t& bitmap);
		void		draw_bitmap(surface_id surface, const rect_t& dst_rect, const rect_t& src_rect, const bitmap_t& bitmap);
		void		draw_texture(surface_id surface, const rect_t& dst_rect, const point_t& src_base, texture_id texture);
		void		draw_texture(surface_id surface, const rect_t& dst_rect, const rect_t& src_rect, texture_id texture);
	}

} }

#endif
