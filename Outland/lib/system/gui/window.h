#pragma once
#include "surface.h"
#include "system\input\input.h"

#if O_PLATFORM_DESKTOP

namespace outland { namespace  os
{
	class window_manager_t;
	class window_t;
	enum window_id : uintptr_t {};

	//////////////////////////////////////////////////
	// WINDOW FUNCTIONS
	//////////////////////////////////////////////////

	namespace window
	{
		enum class frame_t
		{
			//resize areas
			top,
			left,
			right,
			bottom,

			top_left,
			top_right,

			bottom_left,
			bottom_right,

			caption, //for window movement
			client,
		};

		using style_t = uint32_t;
		enum : style_t
		{
			st_basic = 0, //top level window, cannot be child, nonclient is not managed
			st_standard = 1 << 0, //standart system top level window, has a title bar and a frame, nonclient is managed
			st_child = 1 << 1, //nonclient is managed only if a border is specified

			//for top level window
			st_top_tool = 1 << 2, //not on taskbar
			st_top_app = 1 << 3, //forced on taskbar
			st_top_most = 1 << 4, //topmost - stays on top with other topomost windows even when deactivated

			
			st_file_drop = 1 << 5, //accepts file drops

			//for standard and child windows
			st_border = 1 << 6, //a thin line border
			st_client_border = 1 << 7, //the client has a sunken border, visual

			//for standard windows
			st_caption = 1 << 8,
			st_size_frame = 1 << 9, //a thick sizing border

			st_system_menu = 1 << 10, //enables buttons
			st_menu_minimize = 1 << 11,
			st_menu_maximize = 1 << 12,

			//for popup
			st_shadow = 1 << 13,
		};

		extern window_id const null;

		error_t get_client_rect(window_id window, rect_t& client_rect);
		error_t get_client_size(window_id window, point_t& client_size);
		error_t set_text(window_id window, char8_t const* utf8);
		error_t set_cursor(window_id window, cursor_id cursor);
		error_t resize(window_id window, point_t const& size);
		error_t move(window_id window, point_t const& base);
		error_t repaint(window_id window, rect_t const& client_rect);
		error_t repaint_frame(window_id window);
		error_t minimize(window_id window);
		error_t maximize(window_id window);
		error_t restore(window_id window);
		error_t show(window_id window);
		error_t hide(window_id window);
		error_t activate(window_id window);
		error_t focus(window_id window);

		error_t drag(window_id window); //left MB must be down
		error_t parent_set(window_id window, window_id parent);
		window_id window_at(point_t const& point); //return null if it fails
		
		error_t mouse_leave_register(window_id window);
		error_t mouse_leave_frame_register(window_id window);
		error_t mouse_leave_cancel(window_id window);
		error_t mouse_leave_frame_cancel(window_id window);
		error_t mouse_hover_register(window_id window);
		error_t mouse_hover_frame_register(window_id window);
		error_t mouse_hover_cancel(window_id window);
		error_t mouse_hover_frame_cancel(window_id window);

		error_t	mouse_capture(window_id window); //must be within window
		void	mouse_release();

		error_t calc_window_rect(rect_t& rect, window_id parent, style_t style);
		error_t create(window_id& window, window_t* callback,  rect_t const& window_rect, char8_t const* name, window_id parent, style_t style);
		error_t destroy(window_id window);
		error_t destroy_later(window_id window);
	}

	//////////////////////////////////////////////////
	// WINDOW CALLBACK
	//////////////////////////////////////////////////

	class window_t
	{
		friend window_manager_t;

	protected:
		//lifetime
		virtual void on_create(os::window_id window) {}
		virtual void on_close(os::window_id window) {}
		virtual void on_destroy(os::window_id window) {}
		//position and size
		virtual void on_resize(os::window_id window, const os::point_t& client_size) {}
		virtual void on_move(os::window_id window, const os::point_t& base) {}
		virtual void on_tracking_begin(os::window_id window) {}
		virtual void on_tracking(os::window_id window, os::rect_t& window_rect) {}
		virtual void on_tracking_end(os::window_id window) {}
		//client state
		virtual void on_activate(os::window_id window, window_id other_window) {}
		virtual void on_deactivate(os::window_id window, window_id other_window) {}
		virtual void on_minimize(os::window_id window) {}
		virtual void on_maximize(os::window_id window, const os::point_t& client_size) {}
		virtual void on_paint(os::window_id window, os::surface_id surface) const = 0;
		//frame related maangement
		virtual void on_frame_activate(os::window_id window) {}
		virtual void on_frame_deactivate(os::window_id window) {}
		virtual void on_frame_size_move(os::window_id window, os::rect_t& window_rect) const {}
		virtual void on_frame_maximized(os::window_id window, os::rect_t& max_size_pos) const {}
		virtual void on_frame_client_rect(os::window_id window, os::rect_t& client_rect) const {}
		virtual void on_frame_hit(os::window_id window, const os::point_t& position, os::window::frame_t& frame) const {}
		virtual void on_frame_paint(os::window_id window, os::surface_id surface) const = 0;
		//mouse input
		virtual void on_mouse_capture_lost(os::window_id window) {}
		virtual void on_mouse_cursor(os::window_id window, os::cursor_id& cursor) {}
		//client
		virtual void on_mouse_leave(os::window_id window) {}
		virtual void on_mouse_hover(os::window_id window, os::point_t position) {}
		virtual void on_mouse_move(os::window_id window, os::point_t position) {}
		virtual void on_mouse_wheel(os::window_id window, int32_t wheel, const os::point_t& position) {}
		virtual void on_mouse_down(os::window_id window, os::hid::usage_t button, const os::point_t& position) {}
		virtual void on_mouse_up(os::window_id window, os::hid::usage_t button, const os::point_t& position) {}
		//frame
		virtual void on_frame_mouse_leave(os::window_id window) {}
		virtual void on_frame_mouse_hover(os::window_id window, os::point_t position) {}
		virtual void on_frame_mouse_move(os::window_id window, const os::point_t& position) {}
		virtual void on_frame_mouse_wheel(os::window_id window, int32_t wheel, const os::point_t& position) {}
		virtual void on_frame_mouse_down(os::window_id window, os::hid::usage_t button, const os::point_t& position) {}
		virtual void on_frame_mouse_up(os::window_id window, os::hid::usage_t button, const os::point_t& position) {}
		//keyboard input
		virtual void on_focus_gained(os::window_id window, window_id other_window) {}
		virtual void on_focus_lost(os::window_id window, window_id other_window) {}
		virtual void on_key_down(os::window_id window, os::hid::usage_t key) {}
		virtual void on_key_up(os::window_id window, os::hid::usage_t key) {}
		virtual void on_write(os::window_id window, const char8_t* utf8) {}

	public:
		virtual ~window_t() = default;
		//base functions
	};

	

} }

#endif
