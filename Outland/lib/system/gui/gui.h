#pragma once

#include "system\gui\window.h"

namespace outland { namespace  os
{
	namespace gui
	{
		error_t		create();
		void		destroy();
	}

	namespace cursor
	{
		void		hide();
		void		show();

		error_t		create(cursor_id& cursor, size_t width, size_t height, color_t* pixels, size_t hotspot_x, size_t hotspot_y);
		void		destroy(cursor_id cursor);
	}
	
} }