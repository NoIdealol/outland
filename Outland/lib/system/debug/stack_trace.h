#pragma once

namespace outland { namespace  os
{
	using stack_trace_id = void*;

	namespace stack_trace
	{
		error_t init();
		size_t	capture(stack_trace_id* trace, size_t count, size_t skip, uint32_t& hash);
		error_t info(stack_trace_id trace, string8_t& name, size_t& line);
		error_t clean();
	}

} }