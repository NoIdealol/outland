#pragma once
#include "system\application\time.h"

namespace outland { namespace  os
{
	namespace hid
	{
		using usage_t = uint32_t; //usage_page_t << 16 | usage_type_t
		using index_t = uint32_t;
		using value_t = int32_t;
		enum class feature_t
		{
			button,	//<0,1>,		binary
			range,	//<-min,+max>,	absolute,
			stick,	//<-min,+max>,	normalized
			lever,	//<0, +max>,	normalized
			hat,	//<dir flags>	binary
		};

		enum device_id : uintptr_t {};

		struct device_t
		{
			device_id	device;
			usage_t		usage;
			void*		appdata;
		};

		struct report_t
		{
			value_t		value_new;
			value_t		value_old;
			index_t		index;
		};

		struct feature_usage_t
		{
			usage_t		usage;
			index_t		index;
			feature_t	feature;
		};

		struct device_caps_t
		{
			size_t			button_count;
			size_t			range_count;
			size_t			stick_count;
			size_t			lever_count;
			size_t			hat_count;

			feature_usage_t*	feature_array;
			index_t				feature_count;
		};

		constexpr inline usage_t make_usage(uint16_t page, uint16_t usage) { return page << 16 | usage; }
		
		namespace hat_switch
		{
			enum : uint8_t
			{
				dir_up = 1 << 0,
				dir_right = 1 << 1,
				dir_down = 1 << 2,
				dir_left = 1 << 3,

				//clockwise, 0 is north (up)
				angle_0 = dir_up,
				angle_45 = dir_up | dir_right,
				angle_90 = dir_right,
				angle_135 = dir_right | dir_down,
				angle_180 = dir_down,
				angle_225 = dir_down | dir_left,
				angle_270 = dir_left,
				angle_315 = dir_left | dir_up,
				angle_none = 0,
			};
		};

		namespace usage_page
		{
			enum : uint16_t
			{
				generic_desktop = 0x01,
				keyboard = 0x07,
				button = 0x09,
			};
		}

		namespace usage_generic_desktop
		{
			enum : int16_t
			{
				undefined = 0x00,
				pointer = 0x01,
				mouse = 0x02,
				reserved = 0x03,
				joystick = 0x04,
				gamepad = 0x05,
				keyboard = 0x06,
				keypad = 0x07,
				multi_axis_controller = 0x08,
				x = 0x30,
				y = 0x31,
				z = 0x32,
				r_x = 0x33,
				r_y = 0x34,
				r_z = 0x35,
				slider = 0x36,
				dial = 0x37,
				wheel = 0x38,
				hat_switch = 0x39,
				counted_buffer = 0x3A,
				byte_count = 0x3B,
				motion_wakeup = 0x3C,
				v_x = 0x40,
				v_y = 0x41,
				v_z = 0x42,
				vbrx = 0x43,
				vbry = 0x44,
				vbrz = 0x45,
				vno = 0x46,
				system_control = 0x80,
				system_power_down = 0x81,
				system_sleep = 0x82,
				system_wake_up = 0x83,
				system_context_menu = 0x84,
				system_main_menu = 0x85,
				system_app_menu = 0x86,
				system_menu_help = 0x87,
				system_menu_exit = 0x88,
				system_menu_select = 0x89,
				system_menu_right = 0x8A,
				system_menu_left = 0x8B,
				system_menu_up = 0x8C,
				system_menu_down = 0x8D,
				d_pad_up = 0x90,
				d_pad_down = 0x91,
				d_pad_right = 0x92,
				d_pad_left = 0x93,
			};
		}

		namespace usage_button
		{
			enum : uint16_t
			{
				button_none = 0x00,
				button_1,
				button_2,
				button_3,
				button_4,
				button_5,
				button_6,
				button_7,
				button_8,
				button_9,
				button_10,
				button_11,
				button_12,
				button_13,
				button_14,
				button_15,
				button_16,
				button_17,
				button_18,
				button_19,
				button_20,
			};
		}
		
	}

} }
