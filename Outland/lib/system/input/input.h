#pragma once
#include "keyboard.h"
#include "mouse.h"
#include "gamepad.h"

namespace outland { namespace  os
{	
	class input_t
	{
	public:
		virtual ~input_t() = default;

		virtual void* on_device_added(hid::device_t const& device) = 0;
		virtual void on_device_removed(hid::device_t const& device) = 0;

		virtual void on_input(hid::report_t const* report_array, size_t report_count, hid::device_t const& device) = 0;
	};

	namespace input
	{
		error_t	create(input_t* input);
		void	destroy();

		bool register_hid(hid::usage_t usage);
		void unregister_hid(hid::usage_t usage);

		error_t get_hid_caps(hid::device_id device, hid::device_caps_t& caps);
	}

} }