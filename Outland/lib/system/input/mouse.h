#pragma once
#include "device.h"

namespace outland { namespace  os
{
	
	namespace hid { namespace mouse
	{		
		enum : hid::usage_t
		{
			device = make_usage(usage_page::generic_desktop, usage_generic_desktop::mouse),

			button_1 = make_usage(usage_page::button, usage_button::button_1), //left
			button_2 = make_usage(usage_page::button, usage_button::button_2), //right
			button_3 = make_usage(usage_page::button, usage_button::button_3), //middle
			button_4 = make_usage(usage_page::button, usage_button::button_4),
			button_5 = make_usage(usage_page::button, usage_button::button_5),

			position_x = make_usage(usage_page::generic_desktop, usage_generic_desktop::x),
			position_y = make_usage(usage_page::generic_desktop, usage_generic_desktop::y),
			wheel = make_usage(usage_page::generic_desktop, usage_generic_desktop::z),
		};

	} }

} }