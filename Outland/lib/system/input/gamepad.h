#pragma once
#include "device.h"

namespace outland { namespace  os
{
	namespace hid { namespace gamepad
	{		
		
		enum : hid::usage_t
		{
			device = make_usage(usage_page::generic_desktop, usage_generic_desktop::gamepad),

			button_A = make_usage(usage_page::button, usage_button::button_1),
			button_B = make_usage(usage_page::button, usage_button::button_2),
			button_X = make_usage(usage_page::button, usage_button::button_3),
			button_Y = make_usage(usage_page::button, usage_button::button_4),
			button_bumper_left = make_usage(usage_page::button, usage_button::button_5),
			button_bumper_right = make_usage(usage_page::button, usage_button::button_6),
			button_start = make_usage(usage_page::button, usage_button::button_7),
			button_back = make_usage(usage_page::button, usage_button::button_8),
			button_stick_left = make_usage(usage_page::button, usage_button::button_9),
			button_stick_right = make_usage(usage_page::button, usage_button::button_10),

			stick_left_x = make_usage(usage_page::generic_desktop, usage_generic_desktop::x),
			stick_left_y = make_usage(usage_page::generic_desktop, usage_generic_desktop::y),
			trigger_left = make_usage(usage_page::generic_desktop, usage_generic_desktop::z),

			stick_right_x = make_usage(usage_page::generic_desktop, usage_generic_desktop::r_x),
			stick_right_y = make_usage(usage_page::generic_desktop, usage_generic_desktop::r_y),
			trigger_right = make_usage(usage_page::generic_desktop, usage_generic_desktop::r_z),

			hat_switch = make_usage(usage_page::generic_desktop, usage_generic_desktop::hat_switch),
		};

	} }

} }