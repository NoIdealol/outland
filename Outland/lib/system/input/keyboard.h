#pragma once
#include "device.h"

namespace outland { namespace  os
{
	namespace hid { namespace keyboard
	{		
		enum : hid::usage_t
		{
			device = make_usage(usage_page::generic_desktop, usage_generic_desktop::keyboard),

			key_none = make_usage(usage_page::keyboard, 0),

			key_A = make_usage(usage_page::keyboard, 4),
			key_B = make_usage(usage_page::keyboard, 5),
			key_C = make_usage(usage_page::keyboard, 6),
			key_D = make_usage(usage_page::keyboard, 7),
			key_E = make_usage(usage_page::keyboard, 8),
			key_F = make_usage(usage_page::keyboard, 9),
			key_G = make_usage(usage_page::keyboard, 10),
			key_H = make_usage(usage_page::keyboard, 11),
			key_I = make_usage(usage_page::keyboard, 12),
			key_J = make_usage(usage_page::keyboard, 13),
			key_K = make_usage(usage_page::keyboard, 14),
			key_L = make_usage(usage_page::keyboard, 15),
			key_M = make_usage(usage_page::keyboard, 16),
			key_N = make_usage(usage_page::keyboard, 17),
			key_O = make_usage(usage_page::keyboard, 18),
			key_P = make_usage(usage_page::keyboard, 19),
			key_Q = make_usage(usage_page::keyboard, 20),
			key_R = make_usage(usage_page::keyboard, 21),
			key_S = make_usage(usage_page::keyboard, 22),
			key_T = make_usage(usage_page::keyboard, 23),
			key_U = make_usage(usage_page::keyboard, 24),
			key_V = make_usage(usage_page::keyboard, 25),
			key_W = make_usage(usage_page::keyboard, 26),
			key_X = make_usage(usage_page::keyboard, 27),
			key_Y = make_usage(usage_page::keyboard, 28),
			key_Z = make_usage(usage_page::keyboard, 29),
			key_1 = make_usage(usage_page::keyboard, 30),
			key_2 = make_usage(usage_page::keyboard, 31),
			key_3 = make_usage(usage_page::keyboard, 32),
			key_4 = make_usage(usage_page::keyboard, 33),
			key_5 = make_usage(usage_page::keyboard, 34),
			key_6 = make_usage(usage_page::keyboard, 35),
			key_7 = make_usage(usage_page::keyboard, 36),
			key_8 = make_usage(usage_page::keyboard, 37),
			key_9 = make_usage(usage_page::keyboard, 38),
			key_0 = make_usage(usage_page::keyboard, 39),
			key_enter = make_usage(usage_page::keyboard, 40),
			key_escape = make_usage(usage_page::keyboard, 41),
			key_backspace = make_usage(usage_page::keyboard, 42),
			key_tab = make_usage(usage_page::keyboard, 43),
			key_space = make_usage(usage_page::keyboard, 44),
			key_minus = make_usage(usage_page::keyboard, 45),
			key_equals = make_usage(usage_page::keyboard, 46),
			key_bracket_left = make_usage(usage_page::keyboard, 47),
			key_bracket_right = make_usage(usage_page::keyboard, 48),
			key_backslash = make_usage(usage_page::keyboard, 49),
			key_semicolon = make_usage(usage_page::keyboard, 51),
			key_quote = make_usage(usage_page::keyboard, 52),
			key_tilde = make_usage(usage_page::keyboard, 53),
			key_comma = make_usage(usage_page::keyboard, 54),
			key_period = make_usage(usage_page::keyboard, 55),
			key_slash = make_usage(usage_page::keyboard, 56),
			key_caps_lock = make_usage(usage_page::keyboard, 57),
			key_F1 = make_usage(usage_page::keyboard, 58),
			key_F2 = make_usage(usage_page::keyboard, 59),
			key_F3 = make_usage(usage_page::keyboard, 60),
			key_F4 = make_usage(usage_page::keyboard, 61),
			key_F5 = make_usage(usage_page::keyboard, 62),
			key_F6 = make_usage(usage_page::keyboard, 63),
			key_F7 = make_usage(usage_page::keyboard, 64),
			key_F8 = make_usage(usage_page::keyboard, 65),
			key_F9 = make_usage(usage_page::keyboard, 66),
			key_F10 = make_usage(usage_page::keyboard, 67),
			key_F11 = make_usage(usage_page::keyboard, 68),
			key_F12 = make_usage(usage_page::keyboard, 69),
			key_print_screen = make_usage(usage_page::keyboard, 70),
			key_scroll_lock = make_usage(usage_page::keyboard, 71),
			key_pause = make_usage(usage_page::keyboard, 72),
			key_insert = make_usage(usage_page::keyboard, 73),
			key_home = make_usage(usage_page::keyboard, 74),
			key_page_up = make_usage(usage_page::keyboard, 75),
			key_delete = make_usage(usage_page::keyboard, 76),
			key_end = make_usage(usage_page::keyboard, 77),
			key_page_down = make_usage(usage_page::keyboard, 78),
			key_right = make_usage(usage_page::keyboard, 79),
			key_left = make_usage(usage_page::keyboard, 80),
			key_down = make_usage(usage_page::keyboard, 81),
			key_up = make_usage(usage_page::keyboard, 82),
			key_num_lock = make_usage(usage_page::keyboard, 83),
			key_num_divide = make_usage(usage_page::keyboard, 84),
			key_num_multiply = make_usage(usage_page::keyboard, 85),
			key_num_minus = make_usage(usage_page::keyboard, 86),
			key_num_plus = make_usage(usage_page::keyboard, 87),
			key_num_enter = make_usage(usage_page::keyboard, 88),
			key_num_1 = make_usage(usage_page::keyboard, 89),
			key_num_2 = make_usage(usage_page::keyboard, 90),
			key_num_3 = make_usage(usage_page::keyboard, 91),
			key_num_4 = make_usage(usage_page::keyboard, 92),
			key_num_5 = make_usage(usage_page::keyboard, 93),
			key_num_6 = make_usage(usage_page::keyboard, 94),
			key_num_7 = make_usage(usage_page::keyboard, 95),
			key_num_8 = make_usage(usage_page::keyboard, 96),
			key_num_9 = make_usage(usage_page::keyboard, 97),
			key_num_0 = make_usage(usage_page::keyboard, 98),
			key_num_period = make_usage(usage_page::keyboard, 99),
			key_backslash_102 = make_usage(usage_page::keyboard, 100),
			key_F13 = make_usage(usage_page::keyboard, 104),
			key_F14 = make_usage(usage_page::keyboard, 105),
			key_F15 = make_usage(usage_page::keyboard, 106),
			key_F16 = make_usage(usage_page::keyboard, 107),
			key_F17 = make_usage(usage_page::keyboard, 108),
			key_F18 = make_usage(usage_page::keyboard, 109),
			key_F19 = make_usage(usage_page::keyboard, 110),
			key_F20 = make_usage(usage_page::keyboard, 111),
			key_F21 = make_usage(usage_page::keyboard, 112),
			key_F22 = make_usage(usage_page::keyboard, 113),
			key_F23 = make_usage(usage_page::keyboard, 114),
			key_F24 = make_usage(usage_page::keyboard, 115),
			key_help = make_usage(usage_page::keyboard, 117),
			key_menu = make_usage(usage_page::keyboard, 118),
			key_control_left = make_usage(usage_page::keyboard, 224),
			key_shift_left = make_usage(usage_page::keyboard, 225),
			key_alt_left = make_usage(usage_page::keyboard, 226),
			key_win_left = make_usage(usage_page::keyboard, 227),
			key_control_right = make_usage(usage_page::keyboard, 228),
			key_shift_right = make_usage(usage_page::keyboard, 229),
			key_alt_right = make_usage(usage_page::keyboard, 230),
			key_win_right = make_usage(usage_page::keyboard, 231),
		};

		error_t get_key_name(hid::usage_t key, string8_t& utf8);

	} }

} }