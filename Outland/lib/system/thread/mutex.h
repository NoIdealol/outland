#pragma once

namespace outland { namespace  os
{
	struct mutex_t;
	using mutex_id = mutex_t*;

	namespace mutex
	{
		error_t	create(mutex_id& mutex, size_t spin_count);
		void	destroy(mutex_id mutex);
		void	lock(mutex_id mutex);
		void	unlock(mutex_id mutex);
		bool	try_lock(mutex_id mutex);
	}

	class mutex_lock_t
	{
		os::mutex_id	_mutex;
	public:
		mutex_lock_t(os::mutex_id mutex) : _mutex(mutex) { os::mutex::lock(_mutex); }
		~mutex_lock_t() { os::mutex::unlock(_mutex); }
	};

} }