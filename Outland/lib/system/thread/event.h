#pragma once

namespace outland { namespace  os
{
	struct event_t;
	using event_id = event_t*;

	namespace event
	{
		error_t	create(event_id& event);
		void	destroy(event_id event);
		void	set(event_id event);
		void	reset(event_id event);
		void	wait(event_id event);
	}

} }
