#pragma once

namespace outland { namespace  os
{
	namespace memory
	{
		enum order
		{
			order_flush,
			order_evict,
			order_strict,
			order_relaxed,
		};

		void barrier(memory::order order);
	}

	using atomic_int32_id = struct atomic_int32_t*;
	using atomic_uint32_id = struct atomic_uint32_t*;
	using atomic_int64_id = struct atomic_int64_t*;
	using atomic_uint64_id = struct atomic_uint64_t*;
	//using atomic_ptr_id = struct atomic_ptr_t*;
	
	namespace atomic
	{
		int32_t	read(atomic_int32_id atom, memory::order order);
		int32_t	exchange(atomic_int32_id atom, int32_t value, memory::order order);
		int32_t	increment(atomic_int32_id atom, int32_t value, memory::order order);
		int32_t	decrement(atomic_int32_id atom, int32_t value, memory::order order);
		void	write(atomic_int32_id atom, int32_t value, memory::order order);

		uint32_t	read(atomic_uint32_id atom, memory::order order);
		uint32_t	exchange(atomic_uint32_id atom, uint32_t value, memory::order order);
		uint32_t	increment(atomic_uint32_id atom, uint32_t value, memory::order order);
		uint32_t	decrement(atomic_uint32_id atom, uint32_t value, memory::order order);
		void		write(atomic_uint32_id atom, uint32_t value, memory::order order);

		int64_t	read(atomic_int64_id atom, memory::order order);
		int64_t	exchange(atomic_int64_id atom, int64_t value, memory::order order);
		int64_t	increment(atomic_int64_id atom, int64_t value, memory::order order);
		int64_t	decrement(atomic_int64_id atom, int64_t value, memory::order order);
		void	write(atomic_int64_id atom, int64_t value, memory::order order);

		uint64_t	read(atomic_uint64_id atom, memory::order order);
		uint64_t	exchange(atomic_uint64_id atom, uint64_t value, memory::order order);
		uint64_t	increment(atomic_uint64_id atom, uint64_t value, memory::order order);
		uint64_t	decrement(atomic_uint64_id atom, uint64_t value, memory::order order);
		void		write(atomic_uint64_id atom, uint64_t value, memory::order order);

		void	memory_info_int32(memory_info_t& info);
		void	create(atomic_int32_id& atom, int32_t value, void* memory);
		void	destroy(atomic_int32_id& atom);
		void	memory_info_uint32(memory_info_t& info);
		void	create(atomic_uint32_id& atom, uint32_t value, void* memory);
		void	destroy(atomic_uint32_id& atom);
		void	memory_info_int64(memory_info_t& info);
		void	create(atomic_int64_id& atom, int64_t value, void* memory);
		void	destroy(atomic_int64_id& atom);
		void	memory_info_uint64(memory_info_t& info);
		void	create(atomic_uint64_id& atom, uint64_t value, void* memory);
		void	destroy(atomic_uint64_id& atom);
	}

} }
