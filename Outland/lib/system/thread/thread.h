#pragma once

#include "system\application\time.h"

namespace outland { namespace  os
{
	struct thread_t;
	using thread_id = thread_t*;

	namespace thread
	{
		using id_t = uint32_t;
		using main_t = function<uint8_t()>;

		id_t	id();
		id_t	id(thread_id thread);
		error_t	create(thread_id& thread, main_t main, char8_t const* name, size_t stack_size);
		error_t join(thread_id thread);
		error_t	detach(thread_id thread);
		error_t	terminate(thread_id thread);

		void	sleep(time_t time_ms);
		void	yield();
	};

} }
