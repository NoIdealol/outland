#pragma once

namespace outland { namespace  os
{
	using time_t = int64_t;

	namespace time
	{
		using resolution_t = uint64_t;
		enum : resolution_t
		{
			seconds = 1,
			milli_second = 1000,
			micro_second = 1000 * 1000,
			nano_second = 1000 * 1000 * 1000,

			binary_32 = (1ui64 << 32),
			binary_16 = (1ui64 << 16),
		};

		time_t	now_high(resolution_t resolution = binary_32);
		time_t	now_low(resolution_t resolution = milli_second);
		time_t	convert(time_t time, resolution_t resolution_from, resolution_t resolution_to);
	}
} }
