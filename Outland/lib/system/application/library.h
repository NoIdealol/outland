#pragma once

namespace outland { namespace  os
{
	struct library_t;
	using library_id = library_t*;
	namespace library
	{
		using function_t = void(*)();
	
		error_t		load(library_id& lib, const char8_t* name);
		error_t		free(library_id lib);
		function_t	find(library_id lib, const char8_t* name);
	};

} }
