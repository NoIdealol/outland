#pragma once

namespace outland { namespace  os
{
	struct process_t;
	using process_id = process_t*;

	namespace process
	{
		using id_t = uint32_t;

		id_t	id();
		id_t	id(process_id process);
		error_t	create(process_id& process, const char8_t* name, const char8_t* cmd_line);
		error_t join(process_id process);
		error_t	detach(process_id process);
		error_t	terminate(process_id process);
	}

} }
	
