#pragma once

#include "process.h"
#include "library.h"

namespace outland { namespace  os
{

	class application_t
	{
	public:
		virtual ~application_t() = default;

		virtual bool on_init(size_t arg_count, char8_t* cmd_line[]) { return true; }
		virtual bool on_idle() { return true; }
		virtual uint8_t on_exit(uint8_t code) { return code; }

	};

	namespace app
	{
		uint8_t run(application_t* application, size_t arg_count, char8_t* cmd_line[]);
		void exit(uint8_t code);
	}

} }