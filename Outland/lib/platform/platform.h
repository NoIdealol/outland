#pragma once

//////////////////////////////////////////////////
// PLATFORM DEFINES
//////////////////////////////////////////////////

#if defined(_WIN32)
#define O_OSYSTEM_WINDOWS	1
#else
#define O_OSYSTEM_WINDOWS	0
#endif

#if defined(__linux__) || defined(linux) || defined(__linux)
#define O_OSYSTEM_LINUX	1
#else
#define O_OSYSTEM_LINUX	0
#endif

#if defined(__APPLE__) || defined(__MACH__)
#define O_OSYSTEM_MAC	1
#else
#define O_OSYSTEM_MAC	0
#endif

static_assert((O_OSYSTEM_WINDOWS + O_OSYSTEM_LINUX + O_OSYSTEM_MAC) == 1, "operating system define error");

#if O_OSYSTEM_WINDOWS && defined(_MSC_VER)
#define O_INLINE __forceinline
#else
#define O_INLINE inline
#endif

#if O_OSYSTEM_WINDOWS || O_OSYSTEM_LINUX || O_OSYSTEM_MAC
#define O_PLATFORM_DESKTOP	1
#define O_PLATFORM_CONSOLE	0
#define O_PLATFORM_MOBILE	0
#else
#define O_PLATFORM_DESKTOP	0
#define O_PLATFORM_CONSOLE	0
#define O_PLATFORM_MOBILE	0
#endif

static_assert((O_OSYSTEM_WINDOWS + O_OSYSTEM_LINUX + O_OSYSTEM_MAC) == 1, "platform define error");