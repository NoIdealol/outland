#pragma once

//////////////////////////////////////////////////
// DEBUGGER
//////////////////////////////////////////////////

#if defined(_MSC_VER)
#define O_BREAKPOINT __debugbreak()
#else
static_assert(false, "not implemented");
#endif

#define O_ASSERT(condition) if(!(condition)) O_BREAKPOINT;
