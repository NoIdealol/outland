#pragma once

namespace outland
{

	//////////////////////////////////////////////////
	// INTEGRAL TYPES
	//////////////////////////////////////////////////

#if defined(_MSC_VER) && defined(_WIN32)

	using int8_t = __int8;
	using int16_t = __int16;
	using int32_t = __int32;
	using int64_t = __int64;

	using uint8_t = unsigned __int8;
	using uint16_t = unsigned __int16;
	using uint32_t = unsigned __int32;
	using uint64_t = unsigned __int64;

	using float32_t = float;
	using float64_t = double;
#else
	static_assert(false, "not implemented");
#endif

}