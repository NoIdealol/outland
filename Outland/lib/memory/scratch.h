#pragma once

namespace outland
{
	//////////////////////////////////////////////////
	// SCRATCH FRAME
	//////////////////////////////////////////////////

	struct scratch_page_t;

	struct scratch_frame_t
	{
		scratch_page_t*	page;
		void*			head;
	};

	//////////////////////////////////////////////////
	// SCRATCH ALLOCATOR
	//////////////////////////////////////////////////

	class scratch_allocator_t : public allocator_t
	{
	public:
		virtual void push_frame(scratch_frame_t& frame) = 0;
		virtual void pop_frame(scratch_frame_t const& frame) = 0;

		void*	alloc(size_t size, size_t align, size_t* capacity) override = 0;
		void	free(void* memory, size_t size) override = 0;
	};

	namespace scratch_allocator
	{
		struct create_t
		{
			allocator_t*	page_source;
			size_t			page_size;
			void*			init_buffer;
			size_t			init_size;
		};

		void		memory_info(memory_info_t& memory_info);
		void		create(scratch_allocator_t*& scratch, create_t const& create, void* memory);
		void		destroy(scratch_allocator_t* scratch);
	}

	//////////////////////////////////////////////////
	// SCRATCH LOCK
	//////////////////////////////////////////////////

	class scratch_lock_t
	{
	private:
		scratch_allocator_t*	_allocator;
		scratch_frame_t			_frame;

	public:
		scratch_lock_t(scratch_allocator_t* allocator) : _allocator(allocator)	{ _allocator->push_frame(_frame); }
		~scratch_lock_t()														{ _allocator->pop_frame(_frame); }
		scratch_lock_t(scratch_lock_t const&) = delete;
		scratch_lock_t(scratch_lock_t&&) = delete;
		scratch_lock_t& operator = (scratch_lock_t const&) = delete;
		scratch_lock_t& operator = (scratch_lock_t&&) = delete;
	};

}