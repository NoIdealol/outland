#pragma once

namespace outland
{
#define O_DBG_BLOCK_POW2_ALLOCATOR_T 1

	class block_pow2_allocator_t : public allocator_t
	{
	private:

		//--------------------------------------------------
		using block_size_t = uint16_t;
		struct page_metadata_t;
		struct chunk_metadata_t;

		//////////////////////////////////////////////////
		// CONSTANTS
		//////////////////////////////////////////////////

		//--------------------------------------------------
		enum : size_t
		{
			min_alloc_power = 4, // 16 Bytes
			max_alloc_power = 14, // 16 KBytes
			max_grow_count = 8, // grow doubles capacity
			max_alignment = 1024,

			min_page_power = max_alloc_power,
			max_page_power = sizeof(block_size_t) * 8 + min_alloc_power,
			min_alloc_size = 1 << min_alloc_power,
			max_alloc_size = 1 << max_alloc_power,
			min_page_size = 1 << min_page_power,
			max_page_size = 1 << max_page_power,
			max_pool_count = max_alloc_power - min_alloc_power + 1,

		};

		//////////////////////////////////////////////////
		// HEADERS
		//////////////////////////////////////////////////

		//--------------------------------------------------
		struct block_header_t
		{
			block_header_t*		next; //byte offset
			block_header_t**	prev; //byte offset
		};

		//--------------------------------------------------
		struct page_header_t
		{
			page_header_t*		next;
			page_metadata_t*	page_meta;
		};

		//////////////////////////////////////////////////
		// DATA STRUCTURES
		//////////////////////////////////////////////////
		
		//--------------------------------------------------
		struct block_pool_t
		{
			block_header_t* begin;
#if O_DBG_BLOCK_POW2_ALLOCATOR_T
			size_t			dbg_alloc_count;
			size_t			dbg_page_count;
#endif
		};
	
		//--------------------------------------------------
		struct page_metadata_t
		{
			block_size_t	first;	//index of block in page
			block_size_t	last;	//index of block in page
			block_size_t	block_power;	//power of 2
			block_size_t 	alloc_count;	//allocated count
		};

		//--------------------------------------------------
		struct chunk_metadata_t
		{		
			page_metadata_t*	page_meta_array;
			byte_t*				page_begin;
			byte_t*				page_end;
		};

	private:
		block_pool_t		_block_pool_array[max_pool_count];
		chunk_metadata_t	_chunk_metadata_array[max_grow_count];
		size_t				_chunk_count;
		size_t				_chunk_page_count;
		size_t				_page_power;
		size_t				_page_size;
		page_header_t*		_page_list;
		allocator_t*		_allocator;
	
	private:
		chunk_metadata_t*	_find_chunk_meta(void* memory);
		size_t				_find_page_index(chunk_metadata_t* chunk, void* memory);
		page_metadata_t*	_find_page_meta(chunk_metadata_t* chunk, size_t page_index);
		byte_t*				_find_page_data(chunk_metadata_t* chunk, size_t page_index);
		block_size_t		_find_block_index(page_metadata_t* page_meta, byte_t* page_data, void* memory);
		block_header_t*		_find_block(page_metadata_t* page_meta, byte_t* page_data, block_size_t index);

		void				_push_block(block_pool_t* pool, page_metadata_t* page_meta, byte_t* page_data, void* memory);
		void*				_pop_block(block_pool_t* pool, page_metadata_t* page_meta, byte_t* page_data);

		void				_make_blocks(block_pool_t* pool, size_t block_power);
		void				_take_blocks(block_pool_t* pool, page_metadata_t* page_meta, byte_t* page_data);

		void				_make_pages();

	public:
		block_pow2_allocator_t(allocator_t* allocator, size_t page_power, size_t page_count);
		block_pow2_allocator_t(block_pow2_allocator_t const&) = delete;
		~block_pow2_allocator_t();

		void*	alloc(size_t size, size_t align, size_t* capacity) final;
		void	free(void* memory, size_t size) final;
	};

}