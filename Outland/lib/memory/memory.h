#pragma once

namespace outland { namespace u
{
	//////////////////////////////////////////////////
	// ALIGNMENT
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void* mem_align(void* ptr, size_t alignment);
	//--------------------------------------------------
	void const* mem_align(void const* ptr, size_t alignment);
	//--------------------------------------------------
	uintptr_t mem_align(uintptr_t ptr, size_t alignment);
	//--------------------------------------------------
	bool mem_is_aligned(void const* ptr, size_t alignment);
	//--------------------------------------------------
	size_t mem_align_offset(void const* ptr, size_t alignment);
	//--------------------------------------------------
	void mem_offsets(
		size_t const* alignment_array, 
		size_t const* size_array, 
		size_t* offset_array, 
		size_t& max_alignment, 
		size_t& mem_size, 
		size_t array_size, 
		size_t alignment = 0
	);

	//--------------------------------------------------
	size_t size_align(size_t size, size_t alignment);

	//////////////////////////////////////////////////
	// MEMORY
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void mem_copy(void* dst, void const* src, size_t size);

	//--------------------------------------------------
	void mem_move(void* dst, void const* src, size_t size);

	//--------------------------------------------------
	void mem_clr(void* dst, size_t size);

	//--------------------------------------------------
	bool mem_cmp(void const* mem1, void const* mem2, size_t size); //returns if the mem is same

	//--------------------------------------------------
	bool mem_cmp32(void const* dst, size_t size, uint32_t value);

	//--------------------------------------------------
	void mem_set8(void* dst, size_t size, uint8_t value);

	//--------------------------------------------------
	void mem_set32(void* dst, size_t size, uint32_t value);


} }