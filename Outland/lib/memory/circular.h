#pragma once

namespace outland
{
	
	//////////////////////////////////////////////////
	// CIRCULAR ALLOCATOR
	//////////////////////////////////////////////////

	namespace circular_allocator
	{
		struct create_info_t
		{
			void*			buffer;
			size_t			size;
		};

		void		get_memory_info(memory_info_t& memory_info);
		error_t		create(allocator_t*& circular, create_info_t const& create_info, void* memory);
		void		destroy(allocator_t* circular);
	}

}