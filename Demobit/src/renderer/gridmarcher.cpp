#include "pch.h"
#include "gridmarcher.h"



namespace demobit
{

	os::color_t pixel_color(camera_t* cam, size_t x, size_t y, vector3_t const sphere_center)
	{
		//vector3_t const sphere_center = { coord_t(3.0), coord_t(0.0), coord_t(0.0) };
		coord_t const sphere_radius = coord_t(1.3);
		coord_t const sphere_inv_radius = coord_t(0.7692);
		coord_t const sphere_radius_sq = sphere_radius * sphere_radius;

		vector3_t ray = cam->get_ray(x, y);
		vector3_t pos = cam->get_position();

		vector3_t const cam_dist = sphere_center - pos;
		coord_t const cam_dist_proj = m::dot(ray, cam_dist);

		vector3_t const ray_dist = ray * cam_dist_proj - cam_dist;
		coord_t ray_dist_sq = m::dot(ray_dist, ray_dist);

		if (sphere_radius_sq > ray_dist_sq)
		{
			coord_t const tangent_half = m::fast_sqrt(sphere_radius_sq - ray_dist_sq);

			vector3_t const normal = (ray_dist - ray * tangent_half) * sphere_inv_radius;

			vector3_t const light_dir = { coord_t(0.707), coord_t(0.0), coord_t(-0.707) };

			return (int16_t)(coord_t(0x70) * m::max(-m::dot(light_dir, normal), coord_t(0.0)));
			//return (int16_t)(ray_dist_sq / sphere_radius_sq * coord_t(0xFF));
			//return (int16_t)(ray_dist_sq * coord_t(0.59) * coord_t(0xFF));
			//return (int16_t)(ray_dist_sq * coord_t(150.45));
			//return (int16_t)(ray_dist_sq * (coord_t(0xFF) / sphere_radius_sq));
			//return 0xFF00;
		}

		return 0;
		//return 0x700070;
	}

	os::color_t sphere_grid_pixel_color(camera_t* cam, size_t x, size_t y)
	{
		vector3_t spheres[] =
		{
			{ coord_t(3.0), coord_t(0.0), coord_t(0.0) },
			{ coord_t(4.0), coord_t(2.0), coord_t(3.0) },
			{ coord_t(4.0), coord_t(3.0), coord_t(0.0) },
			{ coord_t(4.0), coord_t(-3.0), coord_t(2.0) },
			{ coord_t(4.0), coord_t(-2.0), coord_t(0.0) },
			{ coord_t(4.0), coord_t(-1.0), coord_t(-1.0) },
		};

		os::color_t col = 0;

		for (auto& s : spheres)
		{
			col += pixel_color(cam, x, y, s);
		}

		return col;
	}

	O_INLINE bool ray_march3(vector3_t ray_pos, vector3_t ray_dir)
	{
		coord_t const t_scale = coord_t(0, 1024); //scale to avoid division make dt zero

												  //coord_t const t_infinity = coord_t(30000);
		coord_t const t_clip = coord_t(32) * t_scale; //max ray length

		vector3_t dt;
		coord_t t = coord_t(0);
		vector3_t t_next;
		int inc[3];
		size_t pos[3];

		for (size_t i = 0; i < 3; ++i)
		{
			pos[i] = (int16_t)m::floor(ray_pos[i]);

			switch (m::sign(ray_dir[i]))
			{
			case 0:
			{
				inc[i] = 0;
				dt[i] = coord_t(0);
				t_next[i] = t_clip;
				break;
			}
			case 1:
			{
				inc[i] = 1;
				dt[i] = coord_t(t_scale) / ray_dir[i];
				t_next[i] = (coord_t(1) - m::frac(ray_pos[i])) * dt[i];
				break;
			}
			case -1:
			{
				inc[i] = -1;
				dt[i] = coord_t(-t_scale) / ray_dir[i];
				t_next[i] = m::frac(ray_pos[i]) * dt[i];
				break;
			}
			}
		}

		while (t < t_clip)
		{
			if (ray_visit(pos))
				return true;

			size_t c_min = 0;
			{
				/*for (size_t i = 1; i < 3; ++i)
				{
				if (t_next[c_min] > t_next[i])
				c_min = i;
				}*/

				c_min = t_next[c_min] > t_next[1] ? 1 : c_min;
				c_min = t_next[c_min] > t_next[2] ? 2 : c_min;
			}

			pos[c_min] += inc[c_min];
			t = t_next[c_min];
			t_next[c_min] += dt[c_min];
		}

		return false;
	}

	O_INLINE os::color_t ray_march4(grid_t* grid, vector3_t ray_pos, vector3_t ray_dir)
	{
		enum : int64_t
		{
			precision = 16,
			frac = (1 << precision) - 1,
		};

		//near clip
		ray_pos += ray_dir * coord_t(16);

		int64_t const t_scale = 256 << precision; //scale to avoid division make dt zero
		int64_t const t_clip = ((64 << precision) * t_scale) >> precision; //max ray length

		material_t* mat;
		int64_t t = 0;
		int64_t dt[3];
		int64_t t_next[3];
		int64_t inc[3];
		size_t pos[3];

		for (size_t i = 0; i < 3; ++i)
		{
			pos[i] = m::representation(ray_pos[i]) >> precision;

			switch (m::sign(m::representation(ray_dir[i])))
			{
			case 0:
			{
				inc[i] = 0;
				dt[i] = 0;
				t_next[i] = t_clip;
				break;
			}
			case 1:
			{
				inc[i] = 1;
				dt[i] = (t_scale << precision) / m::representation(ray_dir[i]);
				t_next[i] = (static_cast<int64_t>(65536 - (m::representation(ray_pos[i]) & frac)) * dt[i]) >> precision;
				break;
			}
			case -1:
			{
				inc[i] = -1;
				dt[i] = -(t_scale << precision) / m::representation(ray_dir[i]);
				t_next[i] = (static_cast<int64_t>(m::representation(ray_pos[i]) & frac) * dt[i]) >> precision;
				break;
			}
			}
		}

		while (t < t_clip)
		{
			size_t c_min = 0;
			{
				c_min = t_next[c_min] > t_next[1] ? 1 : c_min;
				c_min = t_next[c_min] > t_next[2] ? 2 : c_min;
			}

			pos[c_min] += inc[c_min];
			t = t_next[c_min];
			t_next[c_min] += dt[c_min];

			mat = ray_visit(grid, pos);
			if (mat)
			{
				//vector3_t const light_dir = { coord_t(0.47), coord_t(0.3), coord_t(-0.707) };

				//dot product
				coord_t dot = grid->light_dir[c_min] * coord_t((int32_t)inc[c_min]);
				dot = m::max(dot, coord_t(0));

				//add ambient
				dot = dot * coord_t(0.7) + coord_t(0.3);

				auto b = coord_t((int32_t)((mat->_color >> 0) & 0xFF)) * dot;
				auto g = coord_t((int32_t)((mat->_color >> 8) & 0xFF)) * dot;
				auto r = coord_t((int32_t)((mat->_color >> 16) & 0xFF)) * dot;

				return
					((os::color_t)((int16_t)(b)) << 0) |
					((os::color_t)((int16_t)(g)) << 8) |
					((os::color_t)((int16_t)(r)) << 16);
			}
		}

		return ray_sky(grid)->_color;
	}

	O_INLINE os::color_t grid_pixel_color(grid_t* grid, camera_t* cam, size_t x, size_t y)
	{
		vector3_t const ray = cam->get_ray(x, y);
		vector3_t const pos = cam->get_position();

		//no shading, just do color
		return ray_march4(grid, pos, ray);
	}

	struct draw_grid_job_t
	{
		grid_t*		_grid;
		camera_t*	_camera;
		size_t		_size_x;
		size_t		_size_y_beg;
		size_t		_size_y_end;
		size_t		_scanline;
		byte_t*		_buffer;

		void work()
		{
			_buffer += _scanline * _size_y_beg;
			os::color_t* pixel;

			for (size_t y = _size_y_beg; y < _size_y_end; ++y)
			{
				pixel = reinterpret_cast<os::color_t*>(_buffer);
				_buffer += _scanline;

				for (size_t x = 0; x < _size_x; ++x)
				{
					pixel[x] = grid_pixel_color(_grid, _camera, x, y);
				}
			}
		}
	};

	void draw_grid(job_system_t* js, grid_t* grid, camera_t* camera, size_t size_x, size_t size_y, size_t scanline, byte_t* buffer)
	{
		draw_grid_job_t	jobs[32];
		size_t size_y_per_job[u::array_size(jobs)];

		size_t size_y_div = size_y / u::array_size(jobs);
		size_t size_y_mod = size_y % u::array_size(jobs);
		for (auto& it : size_y_per_job)
		{
			it = size_y_div;
			if (size_y_mod)
			{
				++it;
				--size_y_mod;
			}
		}
		size_t size_y_sum = 0;

		for (size_t i = 0; i < u::array_size(jobs); ++i)
		{
			jobs[i]._buffer = buffer;
			jobs[i]._camera = camera;
			jobs[i]._grid = grid;
			jobs[i]._scanline = scanline;
			jobs[i]._size_x = size_x;
			jobs[i]._size_y_beg = size_y_sum;
			jobs[i]._size_y_end = size_y_sum + size_y_per_job[i];

			size_y_sum += size_y_per_job[i];

			work_t work;
			work.bind<draw_grid_job_t, &draw_grid_job_t::work>(jobs + i);

			js->add_work(work);
		}

		js->do_work();

		/*os::color_t* pixel;

		for (size_t y = 0; y < size_y; ++y)
		{
			pixel = reinterpret_cast<os::color_t*>(buffer);
			buffer += scanline;

			for (size_t x = 0; x < size_x; ++x)
			{
				//pixel[x] = pixel_color(camera, x, y);
				//pixel[x] = sphere_grid_pixel_color(camera, x, y);
				pixel[x] = grid_pixel_color(grid, camera, x, y);

			}
			
		}*/
	}

}
