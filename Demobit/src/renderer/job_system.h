#pragma once
#include "system\thread\thread.h"
#include "system\thread\event.h"
#include "system\thread\mutex.h"

namespace demobit
{
	using work_t = function<void()>;

	class work_queue_t
	{
	private:
		os::mutex_id	_work_lock;
		array<work_t>	_work_array;

		size_t			_work_thread_count;
		os::event_id	_work_done_event;

	public:
		work_queue_t(allocator_t* allocator);
		error_t init();
		void	clean();

		error_t add_work(work_t work);
		bool get_work(work_t& work);
		
		void main_begin(size_t thread_count);
		void main_wait();
	};

	class worker_t
	{
	private:
		os::thread_id	_thread;
		os::event_id	_wake_event;
		work_queue_t*	_work_queue;
		volatile bool	_run;

	private:
		uint8_t work();

	public:
		worker_t(work_queue_t* q);
		~worker_t();

		error_t start();
		void wake();
		void stop();
	};

	class job_system_t
	{
	private:
		work_queue_t		_work_queue;
		array<worker_t*>	_worker_array;
		allocator_t*		_allocator;

	public:
		job_system_t(allocator_t* allocator);
		error_t init(size_t thread_count);
		void	clean();

		error_t add_work(work_t work);
		void	do_work();
	};
}
