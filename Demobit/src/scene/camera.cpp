#include "pch.h"
#include "camera.h"

namespace demobit
{
	using camera_mem_t = mem<camera_t, vector3_t[]>;

	camera_t::camera_t()
		: _rays(nullptr)
		, _fov(coord_t(0))
		, _width(0)
		, _height(0)
		, _rot0(coord_t(1), coord_t(0), coord_t(0))
		, _rot1(coord_t(0), coord_t(1), coord_t(0))
		, _rot2(coord_t(0), coord_t(0), coord_t(1))
		, _pos(coord_t(0), coord_t(0), coord_t(0))
	{

	}

	error_t camera_t::init(size_t x, size_t y, coord_t fov, vector3_t* rays)
	{
		if ((0 == x) || (0 == y) || (coord_t(0) == fov) || (nullptr == rays))
			return err_invalid_parameter;

		_width = x;
		_height = y;
		_fov = fov;
		_rays = rays;

		coord_t const fov_half = fov * coord_t(0.5);
		coord_t const tri_side = coord_t(1) / m::sin(fov_half);
		coord_t const tri_depth = tri_side *  m::cos(fov_half);

		coord_t const aspect_ratio = coord_t(y) / coord_t(x);

		coord_t const ray_z = tri_depth;
		coord_t const ray_x_beg = coord_t(-1);
		coord_t const ray_x_end = coord_t(+1);
		coord_t const ray_x_len = ray_x_end - ray_x_beg;
		coord_t const ray_y_beg = coord_t(-aspect_ratio);
		coord_t const ray_y_end = coord_t(+aspect_ratio);
		coord_t const ray_y_len = ray_y_end - ray_y_beg;

		coord_t const scale_x = coord_t(x - 1);
		coord_t const scale_y = coord_t(y - 1);

		for (size_t h = 0; h < y; ++h)
		{
			for (size_t w = 0; w < x; ++w)
			{
				vector3_t ray =
				{
					ray_x_beg + (ray_x_len * coord_t(w)) / scale_x,
					ray_y_beg + (ray_y_len * coord_t(h)) / scale_y,
					ray_z
				};

				_rays[w + h * _width] = m::norm(ray);
			}
		}

		return err_ok;
	}

	void camera_t::set_view(vector3_t dir, vector3_t up)
	{
		vector3_t view_z = m::norm(dir);
		vector3_t view_x = m::norm(m::cross(view_z, up));
		vector3_t view_y = m::norm(m::cross(view_z, view_x));

		_rot0[0] = view_x[0];
		_rot0[1] = view_y[0];
		_rot0[2] = view_z[0];

		_rot1[0] = view_x[1];
		_rot1[1] = view_y[1];
		_rot1[2] = view_z[1];

		_rot2[0] = view_x[2];
		_rot2[1] = view_y[2];
		_rot2[2] = view_z[2];
	}

	vector3_t camera_t::get_ray(size_t x, size_t y) const
	{
		vector3_t const& ray = _rays[x + y * _width];
		return vector3_t
		(
			m::dot(ray, _rot0),
			m::dot(ray, _rot1),
			m::dot(ray, _rot2)
		);

		//return _rays[x + y * _width];
	}

	error_t create_camera(camera_t*& camera, allocator_t* allocator, size_t x, size_t y, coord_t fov)
	{
		error_t err;

		camera_mem_t::array_size_t size;
		size.at<1>() = x * y;

		camera_mem_t::alloc_t alloc = camera_mem_t::alloc(allocator, size);

		camera_t* cam = alloc.at<0>();
		vector3_t* rays = alloc.at<1>();

		if (nullptr == cam)
			return err_out_of_memory;

		err = cam->init(x, y, fov, rays);
		if (err)
		{
			camera_mem_t::free(allocator, alloc, size);
			return err;
		}

		camera = cam;

		return err;
	}

	void destroy_camera(camera_t* camera, allocator_t* allocator)
	{
		camera_mem_t::array_size_t size;
		size.at<1>() = camera->get_width() * camera->get_height();

		camera_mem_t::alloc_t alloc;
		alloc.at<0>() = camera;
		alloc.at<1>() = camera->get_rays();

		camera_mem_t::free(allocator, alloc, size);
	}


}
