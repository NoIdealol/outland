#pragma once
#include "types.h"

namespace demobit
{

	class demobit_controller_t
	{
	private:
		bool	_forward;
		bool	_back;
		bool	_left;
		bool	_right;

		vector3_t	_position;
		vector3_t	_look_dir;

	public:
		demobit_controller_t();

		void update(coord_t dt);

		void on_forward_down() { _forward = true; }
		void on_forward_up() { _forward = false; }
		void on_back_down() { _back = true; }
		void on_back_up() { _back = false; }
		void on_left_down() { _left = true; }
		void on_left_up() { _left = false; }
		void on_right_down() { _right = true; }
		void on_right_up() { _right = false; }

		vector3_t get_pos() { return _position; }
	};

}
