#pragma once

#include "system\gui\surface.h"
#include "system\application\time.h"

#include "renderer\stretch.h"
#include "scene\camera.h"
#include "scene\grid.h"
#include "renderer\job_system.h"
#include "scene\controller.h"

namespace demobit
{
	
	class demobit_t
	{
	private:
		allocator_t*	_allocator;
		camera_t*		_camera;
		stretch_t*		_stretch;
		grid_t*			_grid;
		job_system_t*	_job_system;
		demobit_controller_t*	_controller;

		size_t			_buffer_width;
		size_t			_buffer_height;
		byte_t*			_buffer_data;

		//os::texture_id	_buffer_dib;

	public:
		demobit_t();
		~demobit_t();

		error_t init(allocator_t* allocator, demobit_controller_t* controller, size_t render_width, size_t render_height, size_t window_width, size_t window_height);
		void update(os::texture_id back_buffer, os::time_t dt_micro);
	};

}