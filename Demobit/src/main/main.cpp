#include "pch.h"
#include "main\main.h"
#include "application.h"

namespace outland
{
	uint8_t main(size_t arg_count, char8_t* cmd_line[])
	{
		demobit::demobit_application_t app;
		return os::app::run(&app, arg_count, cmd_line);
	}
}