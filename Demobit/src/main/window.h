#pragma once
#include "system\gui\gui.h"
#include "scene\controller.h"

namespace demobit
{
	


	class demobit_view_t
	{
	public:
		virtual ~demobit_view_t() = default;

		virtual void on_view_destroy() = 0;
		virtual void on_view_close_request() = 0;
		virtual void on_view_active() = 0;
		virtual void on_view_background() = 0;
		virtual void on_view_hidden() = 0;
		virtual void on_view_resize(size_t width, size_t height) = 0;
	};

	class demobit_window_t : public os::window_t
	{
	private:
		os::texture_id		_back_buffer;
		demobit_view_t*		_demobit_view;

		bool				_is_tracking;
		bool				_is_active;
		bool				_is_minimized;

		demobit_controller_t _controller;

	private:
		void on_create(os::window_id window) final;
		void on_close(os::window_id window) final;
		void on_destroy(os::window_id window) final;

		void on_activate(os::window_id window, os::window_id other_window);
		void on_deactivate(os::window_id window, os::window_id other_window);
		void on_minimize(os::window_id window);
		void on_maximize(os::window_id window, const os::point_t& client_size) final;
		void on_resize(os::window_id window, const os::point_t& client_size) final;
		void on_tracking_begin(os::window_id window) final;
		void on_tracking_end(os::window_id window) final;

		void on_key_down(os::window_id window, os::hid::usage_t key) final;
		void on_key_up(os::window_id window, os::hid::usage_t key) final;

		void on_mouse_down(os::window_id window, os::hid::usage_t button, const  os::point_t& position) final;
		void on_frame_paint(os::window_id window, os::surface_id surface) const final;
		void on_paint(os::window_id window, os::surface_id surface) const final;

	public:
		demobit_window_t(demobit_view_t* game_view);
		void present(os::window_id window, os::texture_id back_buffer);

		demobit_controller_t* get_controller() { return &_controller; }
	};

}
