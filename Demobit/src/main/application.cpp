#include "pch.h"
#include "application.h"
#include "system\thread\thread.h"

namespace demobit
{
	
	//--------------------------------------------------
	demobit_application_t::demobit_application_t()
		: _system_window(os::window::null)
		, _back_buffer(os::texture::null)
		, _demobit_window(this)
		, _present_time(0)
		, _status(status_t::init)
	{

	}

	//--------------------------------------------------
	error_t demobit_application_t::_back_buffer_create(size_t width, size_t height)
	{
		os::bitmap_t bitmap;
		bitmap.format = os::bitmap_t::b8_g8_r8_x8;
		bitmap.pixels = nullptr;
		bitmap.width = width;
		bitmap.height = height;
		bitmap.scanline = width * 4;

		return os::texture::create_dib(_back_buffer, bitmap);
	}

	//--------------------------------------------------
	void demobit_application_t::_back_buffer_destroy()
	{
		if (os::texture::null != _back_buffer)
		{
			os::texture::destroy(_back_buffer);
			_back_buffer = os::texture::null;
		}
	}

	//--------------------------------------------------
	error_t	demobit_application_t::_window_create(size_t width, size_t height)
	{
		if (os::window::null != _system_window)
			return err_already_exists;

		error_t err;

		os::window::style_t style = 0;

		style |= os::window::st_basic;

		//style |= os::window::st_standard;
		//style |= os::window::st_caption;
		//style |= os::window::st_size_frame;

		//style |= os::window::st_system_menu;
		//style |= os::window::st_menu_maximize;
		//style |= os::window::st_menu_minimize;

		os::rect_t rect;
		rect.base.x = 0;
		rect.base.y = 0;
		rect.size.x = static_cast<os::coord_t>(width);
		rect.size.y = static_cast<os::coord_t>(height);

		err = os::window::calc_window_rect(rect, os::window::null, style);
		if (err)
			return false;

		err = os::window::create(_system_window, &_demobit_window, rect, "game window", os::window::null, style);
		if (err)
			return false;

		return err;
	}

	//--------------------------------------------------
	void	demobit_application_t::_window_destroy()
	{
		if (os::window::null != _system_window)
		{
			os::window::destroy(_system_window);
			_system_window = os::window::null;
		}
	}

	//--------------------------------------------------
	void	demobit_application_t::_exit()
	{
		if (status_t::exit != _status)
		{
			_status = status_t::exit;
			os::app::exit(0);
		}
	}

	//--------------------------------------------------
	bool demobit_application_t::on_init(size_t arg_count, char8_t* cmd_line[])
	{	
		error_t err;
		
		//size_t render_width = 1280;
		//size_t render_height = 720;
		size_t render_width = 1024;
		size_t render_height = 576;
		//size_t render_width = 640;
		//size_t render_height = 360;
		//size_t render_width = 320;
		//size_t render_height = 180;
		//size_t window_width = 640;
		//size_t window_height = 360;
		size_t window_width = 1920;
		size_t window_height = 1080;
		//size_t window_width = 1280;
		//size_t window_height = 720;

		_status = status_t::init;
		_present_time = os::time::now_high(os::time::micro_second);

		err = os::gui::create();
		if (err)
			return false;

		err = _demobit.init(os::app::allocator, _demobit_window.get_controller(), render_width, render_height, window_width, window_height);
		if (err)
			goto _error;

		err = _back_buffer_create(window_width, window_height);
		if (err)
			goto _error;

		//finally try spawning the window
		_status = status_t::hidden;
		err = _window_create(window_width, window_height);
		if (err)
			goto _error;

		os::cursor::hide();

		return true;

	_error:
		_status = status_t::exit;

		_window_destroy();
		_back_buffer_destroy();

		os::gui::destroy();
		return false;
	}

	//--------------------------------------------------
	bool demobit_application_t::on_idle()
	{
		if (status_t::exit == _status)
			return true;

		static os::time_t const time_pre_frame_milli_table[] = 
		{
			1000,
			10,	//~10fps
			250,	//~5fps
			1000,	//~1fps
		};

		os::time_t stamp = os::time::now_high(os::time::micro_second);
		os::time_t elapsed_micro = stamp - _present_time;
		os::time_t elapsed_milli = os::time::convert(elapsed_micro, os::time::micro_second, os::time::milli_second);
		os::time_t const time_per_frame_milli = time_pre_frame_milli_table[static_cast<size_t>(_status)];

		if (time_per_frame_milli > elapsed_milli)
		{
			os::thread::sleep(time_per_frame_milli - elapsed_milli);
		}
		else
		{
			_present_time = stamp;

			if ((status_t::hidden != _status) && (os::texture::null != _back_buffer)) //resize may fail
			{
				_demobit.update(_back_buffer, elapsed_micro);
				_demobit_window.present(_system_window, _back_buffer);
			}
		}
		
		return false;
	}

	//--------------------------------------------------
	uint8_t demobit_application_t::on_exit(uint8_t code)
	{
		_status = status_t::exit;

		_window_destroy();
		_back_buffer_destroy();

		os::cursor::show();

		os::gui::destroy();

		return code;
	}

	//--------------------------------------------------
	void demobit_application_t::on_view_destroy()
	{
		_system_window = os::window::null;
		_exit();
	}

	//--------------------------------------------------
	void demobit_application_t::on_view_close_request()
	{
		_window_destroy();
	}

	//--------------------------------------------------
	void demobit_application_t::on_view_active()
	{
		if (status_t::exit == _status)
			return;

		_status = status_t::active;
	}

	//--------------------------------------------------
	void demobit_application_t::on_view_background()
	{
		if (status_t::exit == _status)
			return;

		_status = status_t::background;
	}

	//--------------------------------------------------
	void demobit_application_t::on_view_hidden()
	{
		if (status_t::exit == _status)
			return;

		_status = status_t::hidden;
	}

	//--------------------------------------------------
	void demobit_application_t::on_view_resize(size_t width, size_t height)
	{
		//no buffer resize
		//_back_buffer_destroy();
		//ignore resize fail
		//_back_buffer_create(width, height);
	}

}