#include "pch.h"
#include "demobit.h"

#include "math\fixed.h"
#include "math\floating.h"
#include "math\integer.h"
#include "math\vector3.h"

#include "system\debug\output.h"
#include "format\print.h"

#include "renderer\gridmarcher.h"
#include "scene\camera.h"

#include "system\hardware\processor.h"

#include "system\application\application.h"

namespace demobit
{

	

	

	

	demobit_t::demobit_t()
		: _allocator(nullptr)
		, _camera(nullptr)
		, _stretch(nullptr)
		, _buffer_data(nullptr)
		, _grid(nullptr)
		, _job_system(nullptr)
	{

	}

	demobit_t::~demobit_t()
	{
		if (nullptr != _camera)
			destroy_camera(_camera, _allocator);

		if (nullptr != _buffer_data)
			mem<byte_t[]>::free(_allocator, _buffer_data, _buffer_width * _buffer_height * sizeof(os::coord_t));

		if (nullptr != _stretch)
			stretch_destroy(_stretch, _allocator);

		if (nullptr != _grid)
			grid_destroy(_grid, _allocator);

		if (nullptr != _job_system)
		{
			_job_system->clean();
			mem<job_system_t>::free(_allocator, _job_system);
		}
	}

	error_t demobit_t::init(allocator_t* allocator, demobit_controller_t* controller, size_t render_width, size_t render_height, size_t window_width, size_t window_height)
	{
		error_t err;
		_allocator = allocator;
		_controller = controller;

		_job_system = mem<job_system_t>::alloc(_allocator, _allocator);
		if (nullptr == _job_system)
			return err_out_of_memory;

		os::processor_t processor;
		/*err = os::processor::querry_info(processor);
		if (err)
		{
			processor.core_count_logical = 4;
		}
		processor.core_count_logical = m::max(processor.core_count_logical, size_t(4));*/
		processor.core_count_logical = 8;

		err = _job_system->init(processor.core_count_logical - 1);
		if (err)
		{
			mem<job_system_t>::free(_allocator, _job_system);
			_job_system = nullptr;
			return err;
		}

		err = grid_create(_grid, _allocator);
		if (err)
			return err;

		err = create_camera(_camera, _allocator, render_width, render_height, coord_t(3.14 * 70.0 / 180.0));
		if (err)
			return err;

		buffer_info_t src;
		src.width = render_width;
		src.height = render_height;
		src.scanline = render_width * sizeof(os::color_t);

		buffer_info_t dst;
		dst.width = window_width;
		dst.height = window_height;
		dst.scanline = window_width * sizeof(os::color_t);

		err = stretch_create(_stretch, _allocator, dst, src);
		if (err)
			return err;

		/*os::bitmap_t bitmap;
		bitmap.format = os::bitmap_t::b8_g8_r8_x8;
		bitmap.pixels = nullptr;
		bitmap.width = width;
		bitmap.height = height;
		bitmap.scanline = width * sizeof(os::coord_t);

		err = os::texture::create_dib(_buffer_dib, bitmap);*/

		_buffer_width = render_width;
		_buffer_height = render_height;
		_buffer_data = mem<byte_t[]>::alloc(_allocator, _buffer_width * _buffer_height * sizeof(os::color_t));
		if (nullptr == _buffer_data)
			return err_out_of_memory;

		return err;
	}

	void demobit_t::update(os::texture_id back_buffer, os::time_t dt_micro)
	{
		error_t err;
#if !O_CRT
		{
			static os::time_t fps_time = 0;
			static size_t fps_count = 0;
			fps_time += dt_micro;
			++fps_count;

			if (5000000 <= fps_time)
			{
				size_t fps = (fps_count * 1000000 * 100) / fps_time;
				fps_count = 0;
				fps_time = 0;

				string8_t str = { _allocator };
				err = f::print(str, "fps * 100: %d \n", fps);
				if (!err)
					os::debug_string(str.data());
			}
		}
#endif	
		static coord_t time = coord_t(0);
		os::time_t dt_16 = os::time::convert(dt_micro, os::time::micro_second, os::time::binary_16);
		time += coord_t(dt_16 >> 16, dt_16 & 0xFFff);
		//time += (coord_t)(dt_16 >> 16) + ((coord_t)(dt_16 & 0xFFff) / 65536.0);
		
		//land switch
		{
			enum class landscape_t
			{
				pillars,
				height,
				drugs,
			};

			static landscape_t ls = landscape_t::pillars;

			switch (ls)
			{
			case landscape_t::pillars:
			{
				if (coord_t(15) < time)
				{
					ls = landscape_t::height;
					grid_generate_terrain_height(_grid, _allocator);
				}
				break;
			}
			case landscape_t::height:
			{
				if (coord_t(30) < time)
				{
					ls = landscape_t::drugs;
					grid_generate_terrain_drugs(_grid, _allocator);
				}
				break;
			}
			case landscape_t::drugs:
			{
				if (coord_t(50) < time)
				{
					os::app::exit(0);
				}
				break;
			}
			default:
				break;
			}
		}

		//rotate
		{
			vector3_t dir = { coord_t(3), m::sin(time * coord_t(0.4)), coord_t(-3.0) };
			vector3_t const up = { coord_t(0), coord_t(0), coord_t(1) };
			_camera->set_view(dir, up);
		}

		//light
		{
			coord_t time_scale_rot(0.5);
			coord_t time_scale_height(0.53);

			coord_t height_variance(0.3);
			coord_t height_offset(-0.7);

			_grid->light_dir = m::norm(vector3_t(
			{
				m::sin(time * time_scale_rot),
				m::cos(time * time_scale_rot),
				height_offset + m::sin(time * time_scale_height) * height_variance
			}));
		}

		//move
		{
			coord_t dt_s = coord_t((int32_t)os::time::convert(dt_micro, os::time::micro_second, os::time::milli_second)) / coord_t(1000);
			_controller->update(dt_s);
			vector3_t pos = _controller->get_pos();
			_camera->set_position(pos);
		}

		//draw
		{
			os::bitmap_t bitmap;
			err = os::texture::lock(back_buffer, bitmap);
			if (err)
				return;

			//draw(_camera, bitmap.width, bitmap.height, bitmap.scanline, bitmap.pixels);
			draw_grid(_job_system, _grid, _camera, _buffer_width, _buffer_height, _buffer_width * sizeof(os::color_t), _buffer_data);
			stretch_buffer(_stretch, bitmap.pixels, _buffer_data);

			os::texture::unlock(back_buffer);
		}
		/*os::surface_id surface;
		err = os::surface::draw_begin(surface, back_buffer);
		if (err)
			return;

		os::rect_t src_rect;
		src_rect.base.x = 0;
		src_rect.base.y = 0;
		src_rect.size.x = bitmap.width;
		src_rect.size.y = bitmap.height;

		os::bitmap_t bb_bitmap;
		os::texture::get_bitmap(back_buffer, bb_bitmap);

		os::rect_t dst_rect;
		dst_rect.base = src_rect.base;
		//dst_rect.size.x = src_rect.size.x * 2;
		//dst_rect.size.y = src_rect.size.y * 2;
		dst_rect.size.x = bb_bitmap.width;
		dst_rect.size.y = bb_bitmap.height;

		os::surface::draw_texture(surface, dst_rect, src_rect, _buffer_dib);

		os::surface::draw_end(surface);*/

		/*os::bitmap_t bitmap;
		err = os::texture::lock(back_buffer, bitmap);
		if (err)
			return;

		draw(_camera, bitmap.width, bitmap.height, bitmap.scanline, bitmap.pixels);

		os::texture::unlock(back_buffer);*/

		/*os::bitmap_t bb_bitmap;
		os::texture::get_bitmap(back_buffer, bb_bitmap);

		os::bitmap_t buffer_bitmap;
		buffer_bitmap.format = os::bitmap_t::b8_g8_r8_x8;
		buffer_bitmap.height = _buffer_height;
		buffer_bitmap.width = _buffer_width;
		buffer_bitmap.pixels = _buffer_data;
		buffer_bitmap.scanline = _buffer_width * sizeof(os::color_t);

		os::surface_id surface;
		err = os::surface::draw_begin(surface, back_buffer);
		if (err)
			return;

		draw(_camera, buffer_bitmap.width, buffer_bitmap.height, buffer_bitmap.scanline, buffer_bitmap.pixels);

		os::rect_t src_rect;
		src_rect.base.x = 0;
		src_rect.base.y = 0;
		src_rect.size.x = _buffer_width;
		src_rect.size.y = _buffer_height;

		os::rect_t dst_rect;
		dst_rect.base = src_rect.base;
		dst_rect.size.x = bb_bitmap.width;
		dst_rect.size.y = bb_bitmap.height;

		os::surface::draw_bitmap(surface, dst_rect, src_rect, buffer_bitmap);
		//os::surface::draw_bitmap(surface, src_rect, src_rect.base, buffer_bitmap);

		os::surface::draw_end(surface);*/



	}

}