#include "pch.h"
#include "xml.h"

#define O_ASSERT(par)
#define O_OUTLAND 1
#define RAPIDXML_STATIC_POOL_SIZE (1 * 1024)
#define RAPIDXML_DYNAMIC_POOL_SIZE (16 * 1024)

#include "rapidxml.hpp"

#define RAPIDXML_NO_STREAMS
#include "rapidxml_print.hpp"


namespace outland { namespace common { namespace xml
{

	//--------------------------------------------------
	inline rapidxml::xml_document<char8_t>* to_native(document_id doc)
	{
		return reinterpret_cast<rapidxml::xml_document<char8_t>*>(doc);
	}

	//--------------------------------------------------
	inline rapidxml::xml_node<char8_t>* to_native(node_id node)
	{
		return reinterpret_cast<rapidxml::xml_node<char8_t>*>(node);
	}

	//--------------------------------------------------
	inline rapidxml::xml_attribute<char8_t>* to_native(attribute_id attr)
	{
		return reinterpret_cast<rapidxml::xml_attribute<char8_t>*>(attr);
	}

	//--------------------------------------------------
	inline rapidxml::xml_node<char8_t>* to_native(attr_list_id list)
	{
		return reinterpret_cast<rapidxml::xml_node<char8_t>*>(list);
	}

	//--------------------------------------------------
	inline rapidxml::xml_node<char8_t>* to_native(node_list_id list)
	{
		return reinterpret_cast<rapidxml::xml_node<char8_t>*>(list);
	}

	//--------------------------------------------------
	inline rapidxml::xml_attribute<char8_t>* to_native(attr_iterator_id it)
	{
		return reinterpret_cast<rapidxml::xml_attribute<char8_t>*>(it);
	}

	//--------------------------------------------------
	inline rapidxml::xml_node<char8_t>* to_native(node_iterator_id it)
	{
		return reinterpret_cast<rapidxml::xml_node<char8_t>*>(it);
	}

	////////////////////////////////////////////////////
	// allocation
	////////////////////////////////////////////////////

	//--------------------------------------------------
	error_t create(allocator_t* allocator, document_id& doc, node_id& root)
	{
		rapidxml::xml_document<char8_t>* xml_doc = new(new_allocator, allocator) rapidxml::xml_document<char8_t>();
		if (nullptr == xml_doc)
			return err_out_of_memory;

		xml_doc->set_allocator(allocator);
		rapidxml::xml_node<char8_t>* xml_root = xml_doc;

		doc = static_cast<document_id>(reinterpret_cast<uintptr_t>(xml_doc));
		root = static_cast<node_id>(reinterpret_cast<uintptr_t>(xml_root));

		return err_ok;
	}

	//--------------------------------------------------
	void	destroy(document_id doc)
	{
		rapidxml::xml_document<char8_t>* xml_doc = to_native(doc);
		del(new_allocator, xml_doc->get_allocator(), xml_doc);
	}

	//--------------------------------------------------
	error_t create(document_id doc, node_id& node, node_type_t type, char8_t const* name, size_t name_size, char8_t const* value, size_t value_size)
	{
		auto* xml_doc = to_native(doc);
		
		try
		{
			rapidxml::xml_node<char8_t>* xml_node = xml_doc->allocate_node(static_cast<rapidxml::node_type>(type), name, value, name_size, value_size);
			node = static_cast<node_id>(reinterpret_cast<uintptr_t>(xml_node));
		}
		catch (std::bad_alloc&)
		{
			return err_out_of_memory;
		}

		return err_ok;
	}

	//--------------------------------------------------
	error_t create(document_id doc, attribute_id& attr, char8_t const* name, size_t name_size, char8_t const* value, size_t value_size)
	{
		auto* xml_doc = to_native(doc);
		try
		{
			rapidxml::xml_attribute<char8_t>* xml_attr = xml_doc->allocate_attribute(name, value, name_size, value_size);
			attr = static_cast<attribute_id>(reinterpret_cast<uintptr_t>(xml_attr));
		}
		catch (std::bad_alloc&)
		{
			return err_out_of_memory;
		}

		return err_ok;
	}

	//--------------------------------------------------
	error_t create(document_id doc, attribute_id& attr, char8_t const* name, char8_t const* value, size_t value_size)
	{
		return create(doc, attr, name, 0, value, value_size);
	}

	//--------------------------------------------------
	error_t	create(document_id doc, char8_t*& str, size_t size)
	{
		auto* xml_doc = to_native(doc);
		try
		{
			str = xml_doc->allocate_string(nullptr, size);
		}
		catch (std::bad_alloc&)
		{
			return err_out_of_memory;
		}

		return err_ok;
	}

	//--------------------------------------------------
	error_t	create(document_id doc, char8_t*& str, char8_t* value, size_t value_size)
	{
		if (0 == value_size)
			value_size = u::str_len(value);

		error_t err = create(doc, str, value_size);
		if (err)
			return err;

		u::mem_cpy(str, value, value_size);

		return err_ok;
	}

	////////////////////////////////////////////////////
	// usage
	////////////////////////////////////////////////////

	//--------------------------------------------------
	error_t print(document_id doc, string8_t& out, error_msg* msg)
	{
		//interator helper
		class print_iterator_t
		{
		private:
			string8_t*	_string;
			size_t		_index;

		public:
			print_iterator_t(string8_t& string) :
				_string(&string),
				_index(0)
			{}

			print_iterator_t(string8_t& string, size_t index) :
				_string(&string),
				_index(index)
			{}

			print_iterator_t(const print_iterator_t& other) :
				_string(other._string),
				_index(other._index)
			{}

			print_iterator_t& operator = (const print_iterator_t& other)
			{
				_string = other._string;
				_index = other._index;
				return *this;
			}

			print_iterator_t operator ++ (int)
			{
				return print_iterator_t(*_string, _index++);
			}

			print_iterator_t& operator ++ ()
			{
				++_index;
				return *this;
			}

			char8_t& operator * ()
			{
				if (_index >= _string->size())
					_string->resize(_index + 1);
				return (*_string)[_index];
			}
		};

		print_iterator_t it(out);

		try
		{
			rapidxml::print(it, *reinterpret_cast<rapidxml::xml_document<char8_t>*>(doc));
		}
		catch(const std::bad_alloc&)
		{
			return err_out_of_memory;
		}
		catch (const rapidxml::parse_error& e)
		{
			if (msg)
			{
				msg->what = e.what();
				msg->where = e.where<char8_t>();
			}
			return err_bad_data;
		}

		return err_ok;
	}

	//--------------------------------------------------
	error_t	parse(document_id doc, char8_t* data, error_msg* msg)
	{
		auto* xml_doc = to_native(doc);
		try
		{
			xml_doc->parse<rapidxml::parse_comment_nodes>(data);
		}
		catch (const rapidxml::parse_error& e)
		{
			if (msg)
			{
				msg->what = e.what();
				msg->where = e.where<char8_t>();
			}
			return err_bad_data;
		}

		return err_ok;
	}

	////////////////////////////////////////////////////
	// node
	////////////////////////////////////////////////////

	//--------------------------------------------------
	node_type_t		type(node_id node)
	{
		auto* xml_node = to_native(node);
		return static_cast<node_type_t>(xml_node->type());
	}

	//--------------------------------------------------
	char8_t const*	name_data(node_id node)
	{
		auto* xml_node = to_native(node);
		return xml_node->name();
	}

	//--------------------------------------------------
	size_t			name_size(node_id node)
	{
		auto* xml_node = to_native(node);
		return xml_node->name_size();
	}

	//--------------------------------------------------
	char8_t const*	value_data(node_id node)
	{
		auto* xml_node = to_native(node);
		return xml_node->value();
	}

	//--------------------------------------------------
	size_t			value_size(node_id node)
	{
		auto* xml_node = to_native(node);
		return xml_node->value_size();
	}

	//--------------------------------------------------
	node_list_id	child_list(node_id node)
	{
		auto* xml_node = to_native(node);
		return static_cast<node_list_id>(node);
	}

	//--------------------------------------------------
	attr_list_id	attr_list(node_id node)
	{
		auto* xml_node = to_native(node);
		return static_cast<attr_list_id>(node);
	}

	////////////////////////////////////////////////////
	// attribute
	////////////////////////////////////////////////////

	//--------------------------------------------------
	char8_t const*	name_data(attribute_id attr)
	{
		auto* xml_attr = to_native(attr);
		return xml_attr->name();
	}

	//--------------------------------------------------
	size_t			name_size(attribute_id attr)
	{
		auto* xml_attr = to_native(attr);
		return xml_attr->name_size();
	}

	//--------------------------------------------------
	char8_t const*	value_data(attribute_id attr)
	{
		auto* xml_attr = to_native(attr);
		return xml_attr->value();
	}

	//--------------------------------------------------
	size_t			value_size(attribute_id attr)
	{
		auto* xml_attr = to_native(attr);
		return xml_attr->value_size();
	}

	////////////////////////////////////////////////////
	// node list
	////////////////////////////////////////////////////

	//--------------------------------------------------
	node_iterator_id begin(node_list_id list)
	{
		auto* xml_list = to_native(list);
		return static_cast<node_iterator_id>(reinterpret_cast<uintptr_t>(xml_list->first_node()));
	}

	//--------------------------------------------------
	node_iterator_id end(node_list_id list)
	{
		return static_cast<node_iterator_id>(reinterpret_cast<uintptr_t>(nullptr));
	}

	//--------------------------------------------------
	node_iterator_id& operator ++(node_iterator_id& it)
	{
		auto* xml_it = to_native(it);
		xml_it = xml_it->next_sibling();
		it = static_cast<node_iterator_id>(reinterpret_cast<uintptr_t>(xml_it));
		return it;
	}

	//--------------------------------------------------
	node_id operator *(node_iterator_id it)
	{
		auto* xml_it = to_native(it);
		return static_cast<node_id>(reinterpret_cast<uintptr_t>(xml_it));
	}

	//--------------------------------------------------
	/*bool operator == (node_iterator_id it1, node_iterator_id it2)
	{
		auto* xml_it1 = to_native(it1);
		auto* xml_it2 = to_native(it2);
		return xml_it1 == xml_it2;
	}

	//--------------------------------------------------
	bool operator != (node_iterator_id it1, node_iterator_id it2)
	{
		auto* xml_it1 = to_native(it1);
		auto* xml_it2 = to_native(it2);
		return xml_it1 != xml_it2;
	}*/

	//--------------------------------------------------
	void	push_back(node_list_id list, node_id value)
	{
		auto* xml_list = to_native(list);
		auto* xml_value = to_native(value);
		xml_list->append_node(xml_value);
	}

	//--------------------------------------------------
	void	push_front(node_list_id list, node_id value)
	{
		auto* xml_list = to_native(list);
		auto* xml_value = to_native(value);
		xml_list->prepend_node(xml_value);
	}

	//--------------------------------------------------
	void	pop_back(node_list_id list)
	{
		auto* xml_list = to_native(list);
		xml_list->remove_last_node();
	}

	//--------------------------------------------------
	void	pop_front(node_list_id list)
	{
		auto* xml_list = to_native(list);
		xml_list->remove_first_node();
	}

	//--------------------------------------------------
	void	insert(node_list_id list, node_iterator_id at, node_id value)
	{
		auto* xml_list = to_native(list);
		auto* xml_it = to_native(at);
		auto* xml_value = to_native(value);
		xml_list->insert_node(xml_it, xml_value);
	}

	//--------------------------------------------------
	void	remove(node_list_id list, node_iterator_id at)
	{
		auto* xml_list = to_native(list);
		auto* xml_it = to_native(at);
		xml_list->remove_node(xml_it);
	}

	////////////////////////////////////////////////////
	// attr list
	////////////////////////////////////////////////////

	//--------------------------------------------------
	attr_iterator_id begin(attr_list_id list)
	{
		auto* xml_list = to_native(list);
		return static_cast<attr_iterator_id>(reinterpret_cast<uintptr_t>(xml_list->first_node()));
	}

	//--------------------------------------------------
	attr_iterator_id end(attr_list_id list)
	{
		return static_cast<attr_iterator_id>(reinterpret_cast<uintptr_t>(nullptr));
	}

	//--------------------------------------------------
	attr_iterator_id& operator ++(attr_iterator_id& it)
	{
		auto* xml_it = to_native(it);
		xml_it = xml_it->next_attribute();
		it = static_cast<attr_iterator_id>(reinterpret_cast<uintptr_t>(xml_it));
		return it;
	}

	//--------------------------------------------------
	attribute_id operator *(attr_iterator_id it)
	{
		auto* xml_it = to_native(it);
		return static_cast<attribute_id>(reinterpret_cast<uintptr_t>(xml_it));
	}

	//--------------------------------------------------
	/*bool operator == (attr_iterator_id it1, attr_iterator_id it2)
	{
		auto* xml_it1 = to_native(it1);
		auto* xml_it2 = to_native(it2);
		return xml_it1 == xml_it2;
	}

	//--------------------------------------------------
	bool operator != (attr_iterator_id it1, attr_iterator_id it2)
	{
		auto* xml_it1 = to_native(it1);
		auto* xml_it2 = to_native(it2);
		return xml_it1 != xml_it2;
	}*/

	//--------------------------------------------------
	void	push_back(attr_list_id list, attribute_id value)
	{
		auto* xml_list = to_native(list);
		auto* xml_value = to_native(value);
		xml_list->append_attribute(xml_value);
	}

	//--------------------------------------------------
	void	push_front(attr_list_id list, attribute_id value)
	{
		auto* xml_list = to_native(list);
		auto* xml_value = to_native(value);
		xml_list->prepend_attribute(xml_value);
	}

	//--------------------------------------------------
	void	pop_back(attr_list_id list)
	{
		auto* xml_list = to_native(list);
		xml_list->remove_last_attribute();
	}

	//--------------------------------------------------
	void	pop_front(attr_list_id list)
	{
		auto* xml_list = to_native(list);
		xml_list->remove_first_attribute();
	}

	//--------------------------------------------------
	void	insert(attr_list_id list, attr_iterator_id at, attribute_id value)
	{
		auto* xml_list = to_native(list);
		auto* xml_it = to_native(at);
		auto* xml_value = to_native(value);
		xml_list->insert_attribute(xml_it, xml_value);
	}

	//--------------------------------------------------
	void	remove(attr_list_id list, attr_iterator_id at)
	{
		auto* xml_list = to_native(list);
		auto* xml_it = to_native(at);
		xml_list->remove_attribute(xml_it);
	}

} } }

