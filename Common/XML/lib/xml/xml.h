#pragma once

namespace outland { namespace common { namespace xml 
{
	////////////////////////////////////////////////////
	// types
	////////////////////////////////////////////////////

	enum node_type_t
	{
		node_document,      //!< A document node. Name and value are empty.
		node_element,       //!< An element node. Name contains element name. Value contains text of first data node.
		node_data,          //!< A data node. Name is empty. Value contains data text.
		node_cdata,         //!< A CDATA node. Name is empty. Value contains data text.
		node_comment,       //!< A comment node. Name is empty. Value contains comment text.
		node_declaration,   //!< A declaration node. Name and value are empty. Declaration parameters (version, encoding and standalone) are in node attributes.
		node_doctype,       //!< A DOCTYPE node. Name is empty. Value contains DOCTYPE text.
		node_pi             //!< A PI node. Name contains target. Value contains instructions.
	};

	// Forward declarations
	enum class node_id : uintptr_t {};
	enum class attribute_id : uintptr_t {};
	enum class document_id : uintptr_t {};

	enum class node_list_id : uintptr_t {};
	enum class node_iterator_id : uintptr_t {};

	enum class attr_list_id : uintptr_t {};
	enum class attr_iterator_id : uintptr_t {};

	struct error_msg
	{
		char8_t const* what;
		char8_t const* where;
	};
	
	////////////////////////////////////////////////////
	// allocation
	////////////////////////////////////////////////////

	error_t create(allocator_t* allocator, document_id& doc, node_id& root);
	void	destroy(document_id doc);

	//automatic destruction with doc
	error_t create(document_id doc, node_id& node, node_type_t type, char8_t const* name = nullptr, size_t name_size = 0, char8_t const* value = nullptr, size_t value_size = 0);
	error_t create(document_id doc, attribute_id& attr, char8_t const* name, size_t name_size, char8_t const* value, size_t value_size = 0);
	error_t create(document_id doc, attribute_id& attr, char8_t const* name, char8_t const* value, size_t value_size = 0);
	error_t	create(document_id doc, char8_t*& str, size_t size);
	error_t	create(document_id doc, char8_t*& str, char8_t const* value, size_t value_size = 0);

	////////////////////////////////////////////////////
	// usage
	////////////////////////////////////////////////////

	error_t print(document_id doc, string8_t& out, error_msg* msg = nullptr);
	error_t	parse(document_id doc, char8_t* data, error_msg* msg = nullptr);

	////////////////////////////////////////////////////
	// node
	////////////////////////////////////////////////////

	node_type_t		type(node_id node);
	char8_t const*	name_data(node_id node);
	size_t			name_size(node_id node);
	char8_t const*	value_data(node_id node);
	size_t			value_size(node_id node);
	node_list_id	child_list(node_id node);
	attr_list_id	attr_list(node_id node);
	
	////////////////////////////////////////////////////
	// attribute
	////////////////////////////////////////////////////

	char8_t const*	name_data(attribute_id attr);
	size_t			name_size(attribute_id attr);
	char8_t const*	value_data(attribute_id attr);
	size_t			value_size(attribute_id attr);

	////////////////////////////////////////////////////
	// node list
	////////////////////////////////////////////////////

	node_iterator_id begin(node_list_id list);
	node_iterator_id end(node_list_id list);
	node_iterator_id& operator ++(node_iterator_id& it);
	node_id operator *(node_iterator_id it);
	//bool operator == (node_iterator_id it1, node_iterator_id it2);
	//bool operator != (node_iterator_id it1, node_iterator_id it2);
	void	push_back(node_list_id list, node_id value);
	void	push_front(node_list_id list, node_id value);
	void	pop_back(node_list_id list);
	void	pop_front(node_list_id list);
	void	insert(node_list_id list, node_iterator_id at, node_id value);
	void	remove(node_list_id list, node_iterator_id at);

	////////////////////////////////////////////////////
	// attr list
	////////////////////////////////////////////////////

	attr_iterator_id begin(attr_list_id list);
	attr_iterator_id end(attr_list_id list);
	attr_iterator_id& operator ++(attr_iterator_id& it);
	attribute_id operator *(attr_iterator_id it);
	//bool operator == (attr_iterator_id it1, attr_iterator_id it2);
	//bool operator != (attr_iterator_id it1, attr_iterator_id it2);
	void	push_back(attr_list_id list, attribute_id value);
	void	push_front(attr_list_id list, attribute_id value);
	void	pop_back(attr_list_id list);
	void	pop_front(attr_list_id list);
	void	insert(attr_list_id list, attr_iterator_id at, attribute_id value);
	void	remove(attr_list_id list, attr_iterator_id at);

} } }







/*
#define RAPIDXML_NO_STDLIB
#define RAPIDXML_NO_EXCEPTIONS

#include "external\rapidxml-1.13\rapidxml.hpp"

#include "Utility\String.h"
#include "Utility\Exception.h"

namespace rapidxml
{
	class xml_exception : public outland::Invalid_data
	{
	private:
		//void*		m_where;

	public:

		//! Constructs parse error
		xml_exception(const char *what, void *where)
			: Invalid_data(what)
			//, m_where(where)
		{
		}

		//! Gets pointer to character data where error happened.
		//! Ch should be the same as char type of xml_document that produced the error.
		//! \return Pointer to location within the parsed string where error occured.
		
		//template<class Ch>
		//Ch *where() const
		//{
		//	return reinterpret_cast<Ch *>(m_where);
		//}
	};

}

namespace outland { namespace xml
{
	using Document = rapidxml::xml_document<char>;
	using Node = rapidxml::xml_node<char>;
	using Attribute = rapidxml::xml_attribute<char>;
	using Exception = rapidxml::xml_exception;
	using NodeType = rapidxml::node_type;

	outland::String serialize(Node& node);
	void serialize(outland::String& out, Node&node);
} }
*/
