#pragma once

#include "system\application\application.h"
#include "window.h"
#include "renderer\renderer.h"
#include "renderer\scene.h"

namespace demotracer
{
	
	class demobit_application_t 
		: public os::application_t
		, private demobit_view_t
	{
		friend demobit_window_t;
	private:
		enum class status_t
		{
			init,
			active,
			background,
			hidden,
			exit,
		};

	private:
		allocator_t*		_allocator;
		status_t			_status;
		demobit_window_t	_demobit_window;
		os::window_id		_system_window;
		os::time_t			_present_time;
		renderer_t*			_renderer;
		scene_t*			_scene_array[scene::scene_count];

	private:
		error_t	_back_buffer_create(size_t width, size_t height);
		void	_back_buffer_destroy();
		error_t	_window_create(size_t width, size_t height);
		void	_window_destroy();
		void	_exit();

	public:
		demobit_application_t(allocator_t* allocator);

		bool	on_init(size_t arg_count, char8_t* cmd_line[]) final;
		bool	on_idle() final;
		uint8_t	on_exit(uint8_t code) final;

		void	on_view_destroy() final;
		void	on_view_close_request() final;
		void	on_view_active() final;
		void	on_view_background() final;
		void	on_view_hidden() final;
		void	on_view_resize(size_t width, size_t height) final;

	};

}