#include "vertex_inc.hlsl"

static const float SCREEN_FOV = 3.14 * 70.0f / 180.0f;
static const float SCREEN_ASPECT_RATIO = 1920.0f / 1080.0f;

vertex_t main(uint vertex_id : SV_VERTEXID)
{
	/*float4 positions[6] = 
	{ 
		float4(-1.0f, -1.0f, 0.0f, 1.0f),	//bot left
		float4(1.0f, -1.0f, 0.0f, 1.0f),	//bot right
		float4(-1.0f, 1.0f, 0.0f, 1.0f),	//top left

		float4(1.0f, 1.0f, 0.0f, 1.0f),		//top right
		float4(1.0f, -1.0f, 0.0f, 1.0f),
		float4(-1.0f, 1.0f, 0.0f, 1.0f),
	};
	
	float3 directions[6] =
	{
		float3(1.0f, 1.0f, -1.0f),
		float3(1.0f, -1.0f, -1.0f),
		float3(1.0f, 1.0f, 1.0f),
		
		float3(1.0f, -1.0f, 1.0f),
		float3(1.0f, -1.0f, -1.0f),
		float3(1.0f, 1.0f, 1.0f),
	};*/

	float4 positions[3] = 
	{ 
		float4(-1.0f, -1.0f, 0.0f, 1.0f),	//bot left
		float4(3.0f, -1.0f, 0.0f, 1.0f),	//bot right
		float4(-1.0f, 3.0f, 0.0f, 1.0f),	//top left
	};
	
	float fov_half = SCREEN_FOV * 0.5f;
	float tri_len = 1.0f / cos(fov_half);
	float tri_side = tri_len * sin(fov_half);
	float tri_up = tri_side / SCREEN_ASPECT_RATIO;

	float3 directions[3] =
	{
		float3(1.0f, tri_side, -tri_up),
		float3(1.0f, -tri_side * 3.0f, -tri_up),
		float3(1.0f, tri_side, tri_up * 3.0f),
	};


	vertex_t v;
	v.position = positions[vertex_id];
	v.ray_dir = directions[vertex_id];
	v.ray_pos = float3(0.0f, 0.0f, 0.0f);

	return v;
}
