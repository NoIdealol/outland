

struct vertex_t
{
	float4 position : SV_POSITION;
	float3 ray_pos : NORMAL0;
	float3 ray_dir : NORMAL1;
};