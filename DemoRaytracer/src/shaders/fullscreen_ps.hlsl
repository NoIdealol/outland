#include "vertex_inc.hlsl"

/*static const float	HIT_DELTA = 0.01f;
static const float	MIN_STEP = 0.005f;
static const uint	MAX_STEPS = 70;
static const float	NORMAL_DELTA = 0.001f;*/

static const float	NEAR_HIT_DELTA = 0.0005f;
static const float	NEAR_MIN_STEP = 0.05f;
static const uint	NEAR_MAX_STEPS = 100;
static const float	DIFFERENCIAL = 0.001f;

static const uint OBJECT_COUNT = 6;

struct scene_test_t
{
	float	dist;
	uint	object;
};

struct material_t
{
	float4	color;
	float	reflectivity;
	float	specularity;
};

float object_sphere(float3 position, float3 sphere_position, float sphere_radius)
{
	return length(position - sphere_position) - sphere_radius;
}

float object_sky_dome(float3 position, float3 sphere_position, float sphere_radius)
{
	return sphere_radius - length(position - sphere_position);
}

float object_metaballs(float3 position, float3 meta_position, float meta_radius)
{
	const uint BALL_COUNT = 12;
	float3 balls[BALL_COUNT] =
	{
		float3(0.3f, 0.4f, 0.5f),
		float3(-0.1f, 0.5f, 0.1f),
		float3(0.4f, -0.2f, -0.3f),
		float3(-0.3f, -0.7f, -0.4f),

		float3(0.8f, -0.7f, -0.4f),
		float3(-0.3f, -0.9f, 0.4f),
		float3(0.3f, 0.3f, -0.4f),
		float3(-0.7f, -0.7f, 0.4f),

		float3(0.8f, -0.5f, -0.9f),
		float3(-0.3f, -0.9f, +0.4f),
		float3(0.3f, 1.3f, -0.7f),
		float3(-0.7f, -1.1f, -0.3f),
	};

	/*float field = 0;
	for (uint i = 0; i < BALL_COUNT; ++i)
	{
		float3 dist = (position - (balls[i] * 0.6f + meta_position));
		field += (meta_radius * meta_radius / dot(dist, dist)) - 0.25f * meta_radius * meta_radius;
	}

	return sqrt(meta_radius * meta_radius / field) - 1.0f * 0.2f;*/

	const float falloff = 0.1f;
	float field = falloff;
	for (uint i = 0; i < BALL_COUNT; ++i)
	{
		float3 dist = (position - (balls[i] * 1.2f + meta_position));
		field += max((1.0f / dot(dist, dist)) - falloff, 0.0f);
	}

	return sqrt(1.0f / field) - 1.0f * 0.3f;
}

float get_object_dist(float3 position, uint object)
{
	float offset = -0.5f;
	switch (object)
	{
	case 1:
		return object_sphere(position, float3(5.0f, 2.0f + offset, 0.5f), 0.7f);
	case 2:
		return object_sphere(position, float3(7.5f, 0.65f + offset, 1.1f), 0.7f);
	case 3:
		return object_sphere(position, float3(6.5f, -0.95f + offset, 0.7f), 0.6f);
	case 4:
		return object_metaballs(position, float3(7.5f, -2.4f + offset, -0.1f), 0.6f);
		//return object_sphere(position, float3(7.5f, -0.4f + offset, -0.1f), 0.8f);
	case 5:
		return object_sphere(position, float3(4.5f, -0.1f + offset, -0.2f), 0.9f);
	case 0:
	default:
		return object_sky_dome(position, float3(0.5f, 2.0f, 0.0f), 10.0f);
	}
}

float3 get_object_normal(float3 position, uint object)
{
	return normalize(float3(
		get_object_dist(float3(position.x + DIFFERENCIAL, position.y, position.z), object),
		get_object_dist(float3(position.x, position.y + DIFFERENCIAL, position.z), object),
		get_object_dist(float3(position.x, position.y, position.z + DIFFERENCIAL), object))
	- get_object_dist(position, object));
}

scene_test_t get_scene(float3 position)
{
	uint	index_min = 0;

	float dist_array[OBJECT_COUNT];
	dist_array[0] = get_object_dist(position, 0);

	for (uint i = 1; i < OBJECT_COUNT; ++i)
	{
		dist_array[i] = get_object_dist(position, i);
		index_min = dist_array[i] < dist_array[index_min] ? i : index_min;
	}

	scene_test_t scene_test;
	scene_test.dist = dist_array[index_min];
	scene_test.object = index_min;

	return scene_test;
}

material_t get_object_material(float3 position, uint object)
{
	material_t materials[OBJECT_COUNT] =
	{
		{ float4(0.3f, 0.3f, 0.3f, 1.0f), 0.0f, 0.0f },
		{ float4(0.5f, 0.0f, 0.0f, 1.0f), 0.3f, 1.5f },
		{ float4(0.0f, 0.0f, 0.5f, 1.0f), 0.5f, 0.7f },
		{ float4(0.0f, 0.3f, 0.0f, 1.0f), 0.1f, 0.7f },
		{ float4(0.5f, 0.5f, 0.0f, 1.0f), 0.1f, 0.7f },
		//{ float4(0.0f, 0.5f, 0.5f, 1.0f), 0.1f, 0.7f },
		{ float4(0.18f, 0.18f, 0.5f, 1.0f), 0.05, 0.7f },
	};

	if (0 == object)
	{
		uint col =
			((uint)(abs(position.x)) +
			(uint)(abs(position.y)) +
			(uint)(abs(position.z)) & 1);

		float4 colors[] =
		{
			float4(0.7f, 0.7f, 0.7f, 1.0f),
			float4(0.0f, 0.0f, 0.0f, 1.0f),
		};

		materials[object].color = colors[col];
		//materials[object].color.xyz = normalize(position) * 0.5f + 0.5f;
	}

	return materials[object];
}

struct refraction_t
{
	float ni;
	float c1;
	float c2_sq;
};

refraction_t refraction_init(float n1, float n2, float3 ray, float3 normal)
{
	refraction_t ref;
	ref.ni = n1 / n2;
	ref.c1 = -dot(ray, normal);
	ref.c2_sq = 1.0f - (ref.ni * ref.ni) * (1.0 - ref.c1 * ref.c1);

	return ref;
}

bool refraction_is_internal_reflection(refraction_t ref)
{
	return ref.c2_sq < 0.0f;
}

float3 refraction_get_ray(refraction_t ref, float3 ray, float3 normal)
{
	float c2 = sqrt(ref.c2_sq);
	return ref.ni * ray + (ref.ni * ref.c1 - c2) * normal;
}

float3 get_ray_refraction(float3 ray, float3 normal, float n1, float n2)
{
	float ni = n1 / n2; //air-glass
	float c1 = -dot(ray, normal);
	float c2_sq = 1.0f - (ni * ni) * (1.0 - c1 * c1);
	float c2 = sqrt(c2_sq);
	float3 refraction = ni * ray + (ni * c1 - c2) * normal;

	return refraction;
}

float get_reflection_coef(float n1, float n2, float3 ray, float3 normal)
{
	float ni = n1 / n2;
	float c1 = -dot(ray, normal);
	float s1_sq = 1.0f - (c1 * c1);
	float c2 = sqrt(1.0f - ni * ni * s1_sq);

	float fr_rt = (n1 * c1 - n2 * c2) / (n1 * c1 + n2 * c2);
	float fr = fr_rt * fr_rt;

	float fp_rt = (n1 * c2 - n2 * c1) / (n1 * c2 + n2 * c1);
	float fp = fp_rt * fp_rt;

	return min((fr + fp) * 0.5f, 1.0f);
}

float4 material_shade(material_t material, float3 normal, float3 ray_dir)
{
	float3 light_dir = normalize(float3(1.0f, 1.0f, -1.0f));
	float4 light_color = float4(1.0f, 1.0f, 1.0f, 1.0f);
	float specular = material.specularity * pow(saturate(-dot(ray_dir, reflect(light_dir, normal))), 2048);

	float ambient = 0.7f;
	float4 color = material.color * (ambient + (1.0f - ambient) * dot(-light_dir, normal));

	//return specular;// *0.5f + 0.5f;
	//return light_color * specular;
	return color + light_color * specular;
}



struct ray_t
{
	float3 dir;
	float3 pos;
};

struct ray_test_t
{
	ray_t			ray;
	scene_test_t	scene_test;
	uint			step;
};

ray_test_t	get_ray_test(ray_t ray, uint step)
{
	scene_test_t scene_test = get_scene(ray.pos);

	float step_dist = max(scene_test.dist, NEAR_MIN_STEP);
	while (step < NEAR_MAX_STEPS && scene_test.dist >= 0.0f)
	{
		step_dist = max(scene_test.dist, NEAR_MIN_STEP);

		ray.pos += ray.dir * step_dist;
		scene_test = get_scene(ray.pos);
		++step;
	}

	//binary search
	while (step < NEAR_MAX_STEPS && abs(scene_test.dist) >= NEAR_HIT_DELTA)
	{
		step_dist *= 0.5f;

		ray.pos += ray.dir * step_dist * sign(scene_test.dist);
		scene_test.dist = get_object_dist(ray.pos, scene_test.object);
		++step;
	}

	ray_test_t ray_test;
	ray_test.ray = ray;
	ray_test.scene_test = scene_test;
	ray_test.step = step;

	return ray_test;
}

ray_t	get_ray_refraction(ray_t ray, uint step, uint object, float n1, float n2)
{
	ray.dir = get_ray_refraction(ray.dir, get_object_normal(ray.pos, object), n1, n2);

	ray.pos += ray.dir * 20.0f; //todo max object size 20

	scene_test_t scene_test;
	scene_test.object = object;
	scene_test.dist = get_object_dist(ray.pos, scene_test.object);

	float step_dist = max(scene_test.dist, NEAR_MIN_STEP);
	while (step < NEAR_MAX_STEPS && scene_test.dist >= 0.0f)
	{
		step_dist = max(scene_test.dist, NEAR_MIN_STEP);

		ray.pos -= ray.dir * step_dist;
		scene_test.dist = get_object_dist(ray.pos, scene_test.object);
		++step;
	}

	//binary search
	while (step < NEAR_MAX_STEPS && abs(scene_test.dist) >= NEAR_HIT_DELTA)
	{
		step_dist *= 0.5f;

		ray.pos -= ray.dir * step_dist * sign(scene_test.dist);
		scene_test.dist = get_object_dist(ray.pos, scene_test.object);
		++step;
	}

	ray.pos += ray.dir * NEAR_MIN_STEP * 2;
	ray.dir = get_ray_refraction(ray.dir, -get_object_normal(ray.pos, object), n2, n1);

	return ray;
}

float4 main(vertex_t v) : SV_TARGET
{
	ray_t ray;
	ray.dir = normalize(v.ray_dir);
	ray.pos = v.ray_pos;

	uint step = 0;

	ray_test_t ray_test = get_ray_test(ray, step);
	material_t material = get_object_material(ray_test.ray.pos, ray_test.scene_test.object);
	float3 normal = get_object_normal(ray_test.ray.pos, ray_test.scene_test.object);
	//float4 color = material_shade(material, normal);

	//reflection
	//step = 0;
	ray = ray_test.ray;
	ray.pos += normal * NEAR_HIT_DELTA; //factor 3 to avoid numerical errors
	ray.dir = reflect(ray.dir, normal);
	ray.pos += ray.dir * NEAR_MIN_STEP * 2;
	//ray.dir = normalize(ray.dir - 2.0f * dot(ray.dir, normal) * normal);

	ray_test_t reflection_ray_test = get_ray_test(ray, step);
	material_t reflection_material = get_object_material(reflection_ray_test.ray.pos, reflection_ray_test.scene_test.object);
	float3 reflection_normal = get_object_normal(reflection_ray_test.ray.pos, reflection_ray_test.scene_test.object);

	/*if (-dot(material_diffuse.normal, ray_dir) < 0.12f)
	{
		return material_shade(material_diffuse) * 0.5f;
	}*/
	//return (float)ray_test.step / (float)NEAR_MAX_STEPS;
	//return (float)reflection_ray_test.step / (float)NEAR_MAX_STEPS;
	//return ray_test.scene_test.dist / NEAR_HIT_DELTA * 0.25f + 0.5f;
	//return -dot(ray.dir, normal);
	//return float4(normal * 0.5f + 0.5f, 1.0f);
	//return reflection_material.color;
	//return material.color;
	//return material_shade(reflection_material, normal) *0.5f + material_shade(material, normal) * 0.5f;

	float brightness = 1.0f;

	float4 color = material_shade(material, normal, ray_test.ray.dir);

	//refraction
	if (5 == ray_test.scene_test.object)
	{
		ray = get_ray_refraction(ray_test.ray, step, ray_test.scene_test.object, 1.0f, 1.5f);

		ray_test_t refraction_ray_test = get_ray_test(ray, step);
		material_t refraction_material = get_object_material(refraction_ray_test.ray.pos, refraction_ray_test.scene_test.object);
		float3 refraction_normal = get_object_normal(refraction_ray_test.ray.pos, refraction_ray_test.scene_test.object);
		//return refraction_material.color;
		float4 refraction_color = material_shade(refraction_material, refraction_normal, refraction_ray_test.ray.dir);
		float4 absorb = exp((1.0f - material.color) * -length(ray.pos - ray_test.ray.pos) * 0.25f);
		color = refraction_color * absorb;
	}

	//return color;
	float4 reflection_color = material_shade(reflection_material, reflection_normal, reflection_ray_test.ray.dir);
	//float reflectivity = material.reflectivity;// *get_reflection_coef(1.5f, 1.0f, ray_test.ray.dir, normal);

	float reflectivity = material.reflectivity + (1.0f - material.reflectivity) * get_reflection_coef(1.0f, 1.5f, ray_test.ray.dir, normal);
	//float reflectivity = get_reflection_coef(1.0f, 1.5f, ray_test.ray.dir, normal);
	//return reflectivity;
	return lerp(color, reflection_color, reflectivity) * brightness;
	//return (reflection_material.color * material.reflectivity + (1.0f - material.reflectivity) * ) * brightness;
}

/*float4 main2(vertex_t v) : SV_TARGET
{
	float3 ray_dir = normalize(v.ray_dir);
	float3 ray_pos = v.ray_pos;

	uint step = 0;

	scene_test_t scene_test;
	scene_test.dist = 1.0f;
	scene_test.index = 0;

	while ((step < MAX_STEPS) && (scene_test.dist >= HIT_DELTA))
	{
		ray_pos += ray_dir * max(scene_test.dist, MIN_STEP);
		scene_test = get_scene(ray_pos);
		++step;
	}

	material_t material_diffuse = get_material(ray_pos, scene_test);
	float4 color_diffuse = material_shade(material_diffuse);
	float4 color_refraction;
	float refraction_factor = 0.0f;

	if (1 == scene_test.index || 5 == scene_test.index)
	{
		color_diffuse = material_diffuse.color;

		//reset interations
		step = 0;
		//move inside object
		material_t material_enter = get_material(ray_pos, scene_test);
		ray_pos -= material_enter.normal * HIT_DELTA;
		scene_test.dist = get_scene_object(ray_pos, scene_test.index);

		refraction_factor = 1.0f - get_reflection_coef(1.0f, 1.5f, ray_dir, material_enter.normal);

		//always refract
		refraction_t ref_enter = refraction_init(1.0f, 1.5f, ray_dir, material_enter.normal);
		ray_dir = refraction_get_ray(ref_enter, ray_dir, material_enter.normal);
		//ray_dir = get_ray_refraction(ray_dir, material_enter.normal);


		//find exit from object
		while ((step < MAX_STEPS) && (scene_test.dist <= 0.0f))
		{
			ray_pos += ray_dir * max(-scene_test.dist, MIN_STEP);
			scene_test.dist = get_scene_object(ray_pos, scene_test.index);
			++step;
		}

		//reset interations
		step = 0;

		material_t material_leave = get_material(ray_pos, scene_test);
		refraction_t ref_leave = refraction_init(1.5f, 1.0f, ray_dir, -material_leave.normal);

		if (refraction_is_internal_reflection(ref_leave))
		{
			return 1;
			ray_pos -= material_leave.normal * HIT_DELTA;
			ray_dir = ray_dir - 2.0f * dot(ray_dir, material_leave.normal) * material_leave.normal;
		}
		else
		{
			ray_pos += material_leave.normal * HIT_DELTA;
			ray_dir = refraction_get_ray(ref_leave, ray_dir, -material_leave.normal);
		}

		//move outside object
		//ray_pos += ray_dir * HIT_DELTA;
		scene_test = get_scene(ray_pos);

		//find refraction object
		while ((step < MAX_STEPS) && (scene_test.dist >= HIT_DELTA))
		{
			ray_pos += ray_dir * max(scene_test.dist, MIN_STEP);
			scene_test = get_scene(ray_pos);
			++step;
		}

		material_t material_refraction = get_material(ray_pos, scene_test);
		color_refraction = material_shade(material_refraction);
	}

	
	float4 color = color_diffuse * (1.0f - refraction_factor) + color_refraction * refraction_factor;
	return color;
}*/