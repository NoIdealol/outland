#include "pch.h"
#include "renderer_dx11.h"

#pragma comment (lib, "D3D11.lib")
#pragma comment (lib, "DXGI.lib")

//#include <D3Dcompiler.h>
//#pragma comment (lib, "D3dCompiler.lib")
//#pragma comment (lib, "D3dCompiler_47.lib")

namespace demotracer
{
	void	renderer::get_memory_info(memory_info_t& memory_info)
	{
		memory_info.align = alignof(renderer_d3d11_t);
		memory_info.size = sizeof(renderer_d3d11_t);
	}

	error_t renderer::create(renderer_t*& renderer, void* memory)
	{
		renderer = O_CREATE(renderer_d3d11_t, memory);
		return err_ok;
	}

	void	renderer::destroy(renderer_t* renderer)
	{
		O_DESTROY(renderer_d3d11_t, renderer);
	}
	
	error_t renderer_d3d11_t::init(os::window_id window, size_t width, size_t height)
	{
		_width = width;
		_height = height;

		HRESULT hr;

		hr = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)&_factory.init());
		if (FAILED(hr))
		{
			return err_unknown;
		}

		hr = _factory->EnumAdapters1(0, &_adapter.init());
		if (FAILED(hr))
		{
			return err_unknown;
		}
		
		DXGI_SWAP_CHAIN_DESC scd;
		ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));
		scd.BufferCount = 2;
		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferDesc.Width = (UINT)width;
		scd.BufferDesc.Height = (UINT)height;
		scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		scd.BufferDesc.RefreshRate.Numerator = 60;
		scd.BufferDesc.RefreshRate.Denominator = 1;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.Flags = 0; //DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH
		scd.OutputWindow = os::get_native_handle(window);
		scd.SampleDesc.Count = 1;//turn off multisample antialias
		scd.SampleDesc.Quality = 0;// -||-
		scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		scd.Windowed = TRUE;

		// create a device, device context and swap chain using the information in the scd struct
		D3D_FEATURE_LEVEL feature_level;
		UINT flags = 0; //D3D11_CREATE_DEVICE_DEBUG;
		
		hr = D3D11CreateDeviceAndSwapChain(_adapter, //adapter
			D3D_DRIVER_TYPE_UNKNOWN, //D3D_DRIVER_TYPE_HARDWARE ak nezadam adapter //D3D_DRIVER_TYPE_UNKNOWN ak zadam adapter
			NULL, //software rasterizer
			flags, //Flags =  0 ..nic specialne
			NULL, //array of req feature levels, .. null= all up to dx 11.0
			0, //num of features prev array
			D3D11_SDK_VERSION,
			&scd,
			&_swap_chain.init(),
			&_device.init(),
			&feature_level,
			&_ctx.init());

		if (hr != S_OK)
		{
			return err_unknown;
		}

		if (feature_level < D3D_FEATURE_LEVEL_11_0)
		{
			return err_unknown;
		}

		hr = _swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&_back_buffer.init());
		if (FAILED(hr))
		{
			return err_unknown;
		}

		hr = _device->CreateRenderTargetView(_back_buffer, NULL, &_render_target.init());
		if (FAILED(hr))
		{
			return err_unknown;
		}

		return err_ok;
	}

	error_t renderer_d3d11_t::init(ID3D11RenderTargetView* view, ID3D11Device* dev, ID3D11DeviceContext* ctx, IDXGISwapChain* sc, size_t width, size_t height)
	{
		_width = width;
		_height = height;
		_render_target = view;
		_device = dev;
		_ctx = ctx;
		_swap_chain = sc;

		return err_ok;
	}

	void	renderer_d3d11_t::render(double time)
	{
		if (_scene)
		{
			_scene->render(time, _render_target, _ctx, _width, _height);
		}
		else
		{
			FLOAT clear_col[] = { 0.5f, 0.5f, 0.5f, 0.5f };
			_ctx->ClearRenderTargetView(_render_target, clear_col);
		}

		_swap_chain->Present(1, 0);
	}

	error_t renderer_d3d11_t::scene_load(scene_t* scene)
	{
		return scene->load(_device, _ctx);
	}

	void	renderer_d3d11_t::scene_unload(scene_t* scene)
	{
		scene->unload();
	}

	void	renderer_d3d11_t::scene_set(scene_t* scene)
	{
		_scene = scene;
	}
}