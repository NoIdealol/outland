#pragma once
#include "renderer.h"

#include "internal_dx11.h"

#include "..\..\System\src\gui\window_win.h"
#include "com_ptr.h"

namespace demotracer
{
	class renderer_d3d11_t : public renderer_t
	{
		Ptr<IDXGIFactory1>			_factory;
		Ptr<IDXGIAdapter1>			_adapter;
		Ptr<ID3D11Device>			_device;
		Ptr<ID3D11DeviceContext>	_ctx;
		Ptr<IDXGISwapChain>			_swap_chain;
		Ptr<ID3D11Texture2D>		_back_buffer;
		Ptr<ID3D11RenderTargetView>	_render_target;

		scene_t*					_scene = nullptr;
		size_t						_width;
		size_t						_height;
	private:

		error_t init(os::window_id window, size_t width, size_t height) final;
		error_t init(ID3D11RenderTargetView* view, ID3D11Device* dev, ID3D11DeviceContext* ctx, IDXGISwapChain* sc, size_t width, size_t height) final;
		void	render(double time) final;

		error_t scene_load(scene_t* scene) final;
		void	scene_unload(scene_t* scene) final;
		void	scene_set(scene_t* scene) final;
	};

}