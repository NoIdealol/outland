#pragma once


namespace demotracer
{
	template<typename type_t>
	class Ptr
	{
	private:
		type_t*	_ptr;

	public:
		Ptr() : _ptr(nullptr) {}
		Ptr(type_t* ptr) : _ptr(ptr) {}
		~Ptr()
		{
			if (_ptr)
				_ptr->Release();
		}

		Ptr& operator = (type_t* ptr)
		{
			clear();
			_ptr = ptr;
			_ptr->AddRef();
			return *this;
		}

		void clear()
		{
			if (_ptr)
			{
				_ptr->Release();
				_ptr = nullptr;
			}
		}

		type_t*& init() { clear(); return _ptr; }
		type_t* operator -> () { return _ptr; }

		operator type_t* () { return _ptr; }
	};
}