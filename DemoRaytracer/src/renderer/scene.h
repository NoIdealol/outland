#pragma once
#include "types_dx11.h"

namespace demotracer
{
	class scene_t
	{
	public:
		virtual error_t	load(ID3D11Device* dev, ID3D11DeviceContext* ctx) = 0;
		virtual void	unload() = 0;

		virtual void	render(double time, ID3D11RenderTargetView* rt, ID3D11DeviceContext* ctx, size_t width, size_t height) = 0;
	};

	namespace scene
	{
		enum : size_t
		{
			scene_count = 1,
		};

		error_t	create_scenes(scene_t* (&scene_array)[scene_count], allocator_t* allocator);
		void	destroy_scenes(scene_t* const (&scene_array)[scene_count], allocator_t* allocator);
	}
}