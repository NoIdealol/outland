#pragma once

#include "scene\types.h"

namespace demotracer
{
	struct buffer_info_t
	{
		size_t width;
		size_t height;
		size_t scanline;
	};

	struct stretch_t
	{
		struct block_t
		{
			size_t block[4]; //byte offsets in src
		};

		buffer_info_t	info;
		block_t*		filter;		//filter for every dst pixel
	};

	void stretch_buffer(stretch_t* stretch, byte_t* dst, byte_t* src);
	void stretch_destroy(stretch_t* stretch, allocator_t* allocator);
	error_t stretch_create(stretch_t*& stretch, allocator_t* allocator, buffer_info_t const& dst, buffer_info_t const& src);
}
