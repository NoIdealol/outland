#pragma once
#include "scene.h"
#include "system\gui\window.h"

namespace demotracer
{
	class renderer_t
	{
	public:
		virtual error_t init(os::window_id window, size_t width, size_t height) = 0;
		virtual error_t init(ID3D11RenderTargetView* view, ID3D11Device* dev, ID3D11DeviceContext* ctx, IDXGISwapChain* sc, size_t width, size_t height) = 0;
		virtual void	render(double time) = 0;

		virtual error_t scene_load(scene_t* scene) = 0;
		virtual void	scene_unload(scene_t* scene) = 0;
		virtual void	scene_set(scene_t* scene) = 0;
	};

	namespace renderer
	{
		void	get_memory_info(memory_info_t& memory_info);
		error_t create(renderer_t*& renderer, void* memory);
		void	destroy(renderer_t* renderer);
	}
}