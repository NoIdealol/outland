#pragma once

#include "scene\camera.h"
#include "scene\grid.h"
#include "job_system.h"

namespace demotracer
{
	void draw_grid(job_system_t* js, grid_t* grid, camera_t* camera, size_t size_x, size_t size_y, size_t scanline, byte_t* buffer);

}
