#include "pch.h"
#include "job_system.h"

namespace demotracer
{
	work_queue_t::work_queue_t(allocator_t* allocator)
		: _work_array(allocator)
		, _work_thread_count(0)
	{

	}

	error_t work_queue_t::init()
	{
		error_t err;

		err = _work_array.reserve(256);
		if (err)
			return err;

		err = os::event::create(_work_done_event);
		if (err)
			return err;

		err = os::mutex::create(_work_lock, 256);
		if (err)
		{
			os::event::destroy(_work_done_event);
			return err;
		}

		return err_ok;
	}

	void	work_queue_t::clean()
	{
		os::event::destroy(_work_done_event);
		os::mutex::destroy(_work_lock);
	}

	error_t work_queue_t::add_work(work_t work)
	{
		return _work_array.push_back(work);
	}

	bool work_queue_t::get_work(work_t& work)
	{
		os::mutex::lock(_work_lock);

		if (0 != _work_array.size())
		{
			work = _work_array.last();
			_work_array.pop_back();

			os::mutex::unlock(_work_lock);
			return true;
		}
		else
		{
			if (0 == --_work_thread_count)
				os::event::set(_work_done_event);

			os::mutex::unlock(_work_lock);
			return false;
		}
	}

	void work_queue_t::main_begin(size_t thread_count)
	{
		_work_thread_count = thread_count + 1;
	}

	void work_queue_t::main_wait()
	{
		work_t job;
		while (get_work(job))
		{
			job();
		}

		os::event::wait(_work_done_event);
		os::event::reset(_work_done_event);
	}




	worker_t::worker_t(work_queue_t* q)
		: _work_queue(q)
		, _run(false)
	{

	}

	worker_t::~worker_t()
	{

	}

	error_t worker_t::start()
	{
		error_t err;

		err = os::event::create(_wake_event);
		if (err)
			return err;

		_run = true;

		os::thread::main_t thread_main;
		thread_main.bind<worker_t, &worker_t::work>(this);
		err = os::thread::create(_thread, thread_main, "worker", 1024 * 4);
		if (err)
		{
			_run = false;
			os::event::destroy(_wake_event);
			return err;
		}

		return err_ok;
	}

	uint8_t worker_t::work()
	{
		os::event::wait(_wake_event);

		while (_run)
		{
			os::event::reset(_wake_event);

			{
				work_t job;
				while (_work_queue->get_work(job))
				{
					job();
				}
			}

			os::event::wait(_wake_event);
		}

		return 0;
	}

	void worker_t::wake()
	{
		os::event::set(_wake_event);
	}

	void worker_t::stop()
	{
		_run = false;
		os::event::set(_wake_event);
		os::thread::join(_thread);
		os::event::destroy(_wake_event);
	}



	job_system_t::job_system_t(allocator_t* allocator)
		: _work_queue(allocator)
		, _worker_array(allocator)
		, _allocator(allocator)
	{

	}

	error_t job_system_t::init(size_t thread_count)
	{
		error_t err;

		err = _worker_array.reserve(thread_count);
		if (err)
			return err;

		err = _work_queue.init();
		if (err)
			return err;

		{
			for (size_t i = 0; i < thread_count; ++i)
			{
				worker_t* w = mem<worker_t>::alloc(_allocator, &_work_queue);
				if (nullptr == w)
				{
					err = err_out_of_memory;
					goto _cleanup;
				}

				err = w->start();
				if (err)
				{
					mem<worker_t>::free(_allocator, w);
					goto _cleanup;
				}

				_worker_array.push_back(w);
			}
		}

		return err_ok;

	_cleanup:
		for (worker_t* w : _worker_array)
		{
			w->stop();
			mem<worker_t>::free(_allocator, w);
		}

		_work_queue.clean();
		return err;
	}

	void	job_system_t::clean()
	{
		for (worker_t* w : _worker_array)
		{
			w->stop();
			mem<worker_t>::free(_allocator, w);
		}
		_worker_array.clear();

		_work_queue.clean();
	}

	error_t job_system_t::add_work(work_t work)
	{
		return _work_queue.add_work(work);
	}

	void job_system_t::do_work()
	{
		_work_queue.main_begin(_worker_array.size());
		
		for (worker_t* w : _worker_array)
		{
			w->wake();
		}

		_work_queue.main_wait();
	}

}