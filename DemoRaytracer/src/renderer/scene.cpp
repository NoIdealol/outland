#include "pch.h"
#include "scene.h"

#include "scenes\scene_1.h"

namespace demotracer
{
	error_t	scene::create_scenes(scene_t* (&scene_array)[scene_count], allocator_t* allocator)
	{
		scene_array[0] = mem<scene_1_t>::alloc(allocator, allocator);

		return err_ok;
	}

	void	scene::destroy_scenes(scene_t* const (&scene_array)[scene_count], allocator_t* allocator)
	{
		mem<scene_1_t>::free(allocator, static_cast<scene_1_t*>(scene_array[0]));
	}
	
}