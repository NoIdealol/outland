#include "pch.h"
#include "stretch.h"


namespace demotracer
{
	void stretch_buffer(stretch_t* stretch, byte_t* dst, byte_t* src)
	{
		auto* filter = stretch->filter;

		for (size_t y = 0; y < stretch->info.height; ++y)
		{
			os::color_t* dst_line = reinterpret_cast<os::color_t*>(dst + stretch->info.scanline * y);

			for (size_t x = 0; x < stretch->info.width; ++x)
			{
				auto& block = filter[x].block;
				dst_line[x] =
					*reinterpret_cast<os::color_t*>(src + block[3]) +
					*reinterpret_cast<os::color_t*>(src + block[2]) +
					*reinterpret_cast<os::color_t*>(src + block[1]) +
					*reinterpret_cast<os::color_t*>(src + block[0]);
			}

			filter += stretch->info.width;
		}
	}

	void stretch_destroy(stretch_t* stretch, allocator_t* allocator)
	{
		mem<stretch_t::block_t[]>::free(allocator, stretch->filter, stretch->info.width * stretch->info.height);
		mem<stretch_t>::free(allocator, stretch);
	}

	error_t stretch_create(stretch_t*& stretch, allocator_t* allocator, buffer_info_t const& dst, buffer_info_t const& src)
	{
		stretch = mem<stretch_t>::alloc(allocator);
		if (nullptr == stretch)
		{
			return err_out_of_memory;
		}

		stretch->filter = mem<stretch_t::block_t[]>::alloc(allocator, dst.width * dst.height);
		if (nullptr == stretch->filter)
		{
			mem<stretch_t>::free(allocator, stretch);
			stretch = nullptr;
			return err_out_of_memory;
		}

		stretch->info.width = dst.width;
		stretch->info.height = dst.height;
		stretch->info.scanline = dst.scanline;

		//create
		{
			double const dbl_err = 0.000001;
			double const src_x = src.width - dbl_err - 1;
			double const src_y = src.height - dbl_err - 1;
			double const dst_x = dst.width - 1;
			double const dst_y = dst.height - 1;

			auto* filter = stretch->filter;

			for (size_t y = 0; y < stretch->info.height; ++y)
			{
				for (size_t x = 0; x < stretch->info.width; ++x)
				{
					auto& block = filter[x].block;

					size_t src_pixel_x = static_cast<size_t>((static_cast<double>(x) / dst_x) * src_x);
					size_t src_pixel_y = static_cast<size_t>((static_cast<double>(y) / dst_y) * src_y);

					block[0] = src.scanline * (src_pixel_y + 0) + (src_pixel_x + 0) * sizeof(os::color_t);
					block[1] = src.scanline * (src_pixel_y + 0) + (src_pixel_x + 1) * sizeof(os::color_t);
					block[2] = src.scanline * (src_pixel_y + 1) + (src_pixel_x + 0) * sizeof(os::color_t);
					block[3] = src.scanline * (src_pixel_y + 1) + (src_pixel_x + 1) * sizeof(os::color_t);
				}

				filter += stretch->info.width;
			}
		}

		return err_ok;
	}
}