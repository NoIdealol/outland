#include "pch.h"
#include "scene_1.h"

namespace demotracer
{
	bool file_read(char8_t const* name, array<byte_t>& data)
	{
		DWORD acces = GENERIC_READ;		
		DWORD flagsAndAttributes = FILE_ATTRIBUTE_NORMAL;
		DWORD creation = OPEN_EXISTING;
		
		HANDLE hFile = CreateFileA(name, acces, FILE_SHARE_READ, NULL, creation, flagsAndAttributes, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		LARGE_INTEGER li;
		if (FALSE == GetFileSizeEx(hFile, &li))
		{
			CloseHandle(hFile);
			return false;
		}

		if (data.resize(li.QuadPart))
		{
			CloseHandle(hFile);
			return false;
		}

		BOOL read = ReadFile(hFile, data.data(), data.size(), 0, NULL);
		CloseHandle(hFile);

		return FALSE != read;
	}



	scene_1_t::scene_1_t(allocator_t* allocator)
		: _allocator(allocator)
	{

	}

	error_t	scene_1_t::load(ID3D11Device* dev, ID3D11DeviceContext* ctx)
	{
		HRESULT hr;
		array<byte_t> vs_data = { _allocator };

		if (!file_read("fullscreen_vs.cso", vs_data))
			return err_unknown;

		hr = dev->CreateVertexShader(vs_data.data(), vs_data.size(), NULL, &_vs.init());
		if (!SUCCEEDED(hr))
			return err_unknown;

		if (!file_read("fullscreen_ps.cso", vs_data))
			return err_unknown;

		hr = dev->CreatePixelShader(vs_data.data(), vs_data.size(), NULL, &_ps.init());
		if (!SUCCEEDED(hr))
			return err_unknown;

		D3D11_RASTERIZER_DESC rasterizer_desc;
		ZeroMemory(&rasterizer_desc, sizeof(D3D11_RASTERIZER_DESC));
		rasterizer_desc.FillMode = D3D11_FILL_SOLID;
		rasterizer_desc.CullMode = D3D11_CULL_NONE;
		rasterizer_desc.FrontCounterClockwise = TRUE;
		rasterizer_desc.DepthClipEnable = FALSE;//TRUE;

		hr = dev->CreateRasterizerState(&rasterizer_desc, &_rs.init());
		if (!SUCCEEDED(hr))
			return err_unknown;

		return err_ok;
	}

	void	scene_1_t::unload()
	{

	}

	void	scene_1_t::render(double time, ID3D11RenderTargetView* rt, ID3D11DeviceContext* ctx, size_t width, size_t height)
	{
		FLOAT clear_col[] = { 0.0f, 0.25f, 0.5f, 0.5f };
		ctx->ClearRenderTargetView(rt, clear_col);

		D3D11_VIEWPORT viewport;
		ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		viewport.Width = (FLOAT)width;
		viewport.Height = (FLOAT)height;

		ctx->RSSetViewports(1, &viewport);

		ctx->RSSetState(_rs);
		ctx->OMSetRenderTargets(1, &rt, NULL);

		ctx->VSSetShader(_vs, NULL, 0);
		ctx->PSSetShader(_ps, NULL, 0);

		//D3D11_PRIMITIVE_TOPOLOGY
		ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		ctx->Draw(3, 0);
	}

}