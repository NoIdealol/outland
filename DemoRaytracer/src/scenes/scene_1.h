#pragma once
#include "scene_dx11.h"

namespace demotracer
{

	class scene_1_t : public scene_t
	{
		allocator_t*				_allocator;
		Ptr<ID3D11VertexShader>		_vs;
		Ptr<ID3D11PixelShader>		_ps;
		Ptr<ID3D11RasterizerState>	_rs;
	public:
		scene_1_t(allocator_t* allocator);

		error_t	load(ID3D11Device* dev, ID3D11DeviceContext* ctx) final;
		void	unload() final;
		void	render(double time, ID3D11RenderTargetView* rt, ID3D11DeviceContext* ctx, size_t width, size_t height) final;

	};

}