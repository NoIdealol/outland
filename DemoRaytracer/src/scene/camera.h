#pragma once
#include "scene\types.h"

namespace demotracer
{
	class camera_t
	{
	private:
		size_t	_width;
		size_t	_height;
		coord_t	_fov;

		vector3_t*	_rays; //normalized rays prepared to be transformed

						   //x points right
						   //y points down
						   //z points forward

		vector3_t	_rot0;
		vector3_t	_rot1;
		vector3_t	_rot2;
		vector3_t	_pos;

	public:
		camera_t();

		error_t		init(size_t x, size_t y, coord_t fov, vector3_t* rays);
		void		set_view(vector3_t dir, vector3_t up);
		void		set_position(vector3_t pos) { _pos = pos; }

		size_t		get_width() const { return _width; }
		size_t		get_height() const { return _height; }
		vector3_t*	get_rays() const { return _rays; }

		vector3_t	get_position() const { return _pos; }
		vector3_t	get_ray(size_t x, size_t y) const;
	};

	error_t create_camera(camera_t*& camera, allocator_t* allocator, size_t x, size_t y, coord_t fov);
	void destroy_camera(camera_t* camera, allocator_t* allocator);

}
