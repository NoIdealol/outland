#include "pch.h"
#include "controller.h"

namespace demotracer
{
	demobit_controller_t::demobit_controller_t()
		: _position(coord_t(0), coord_t(0), coord_t(0))
		, _look_dir(coord_t(1), coord_t(0), coord_t(0))
		, _forward(false)
		, _back(false)
		, _left(false)
		, _right(false)
	{

	}

	void demobit_controller_t::update(coord_t dt)
	{
		coord_t const speed = coord_t(7.0);

		vector3_t fwd = m::norm(_look_dir);
		vector3_t up = { coord_t(0), coord_t(0), coord_t(1) };
		vector3_t left = m::norm(m::cross(up, fwd));

		vector3_t dir = { coord_t(0), coord_t(0), coord_t(0) };
#if O_CRT
		dir[0] = coord_t(1);
		dir[1] = coord_t(0.6);
#else
		if (_forward)
		{
			dir += fwd;
		}

		if (_back)
		{
			dir -= fwd;
		}

		if (_left)
		{
			dir += left;
		}

		if (_right)
		{
			dir -= left;
		}
#endif

		if(coord_t(0) != m::dot(dir, dir))
			dir = m::norm(dir);

		_position += dir * speed * dt;
	}

}
