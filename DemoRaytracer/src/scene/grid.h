#pragma once
#include "scene\types.h"

namespace demotracer
{
	struct material_t
	{
		os::color_t	_color;
	};

	struct grid_t
	{
		enum : size_t
		{
			length = 1 << 5,
			mask = length - 1,
			variance_count = 32,
			variance_mask = variance_count - 1,
		};

		material_t* grid[length][length][length];
		material_t	sky;
		material_t	grass;
		material_t	dirt;
		material_t	dirt_variance[variance_count];
		material_t	grass_variance[variance_count];
		material_t  height_variance[variance_count];
		material_t  drugs_variance[variance_count];

		vector3_t	light_dir;
	};


	bool ray_visit(size_t(&pos)[3]);

	material_t* ray_visit(grid_t* grid, size_t(&pos)[3]);
	O_INLINE material_t* ray_sky(grid_t* grid) { return &grid->sky; }

	error_t grid_create(grid_t*& grid, allocator_t* allocator);
	void grid_destroy(grid_t* grid, allocator_t* allocator);

	void grid_generate_terrain_pillars(grid_t* grid, allocator_t* allocator);
	void grid_generate_terrain_drugs(grid_t* grid, allocator_t* allocator);
	void grid_generate_terrain_height(grid_t* grid, allocator_t* allocator);
}

