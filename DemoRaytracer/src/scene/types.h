#pragma once

#include "math\fixed.h"
#include "math\floating.h"
#include "math\integer.h"
#include "math\vector3.h"

#include "system\gui\surface.h"

namespace demotracer
{
	using coord_t = fixed_t;
	//using coord_t = float32_t;
	using vector3_t = vector3<coord_t>;
}
