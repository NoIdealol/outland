#include "pch.h"
#include "grid.h"

namespace demotracer
{
	class rand_generator_t
	{
	private:
		enum : uint64_t
		{
			MUL = 25214903917ull,
			INC = 11ull,
			MOD_MASK = (1ull << 48) - 1,

			RES_BIT_SHIFT = 15,
			RES_BIT_MASK_BIT_COUNT = 32,
			RES_BIT_MASK = (1ull << RES_BIT_MASK_BIT_COUNT) - 1,
		};

	public:
		enum : uint32_t
		{
			MAX_NUMBER = RES_BIT_MASK,
		};

	private:
		uint64_t	_last_number;

	private:
		inline uint64_t _generate()
		{
			_last_number = (_last_number * MUL + INC) & MOD_MASK;
			return (_last_number >> RES_BIT_SHIFT) & RES_BIT_MASK;
		}

	public:
		rand_generator_t(uint32_t seed = 0) : _last_number(seed)
		{
			_generate();
		}

		O_INLINE uint32_t generate() { return static_cast<uint32_t>(_generate()); }
	};

	void grid_clear(grid_t* grid)
	{
		for (auto& z : grid->grid)
		{
			for (auto& y : z)
			{
				for (auto& x : y)
				{
					x = nullptr;
				}
			}
		}
	}

	void grid_generate_terrain_pillars(grid_t* grid, allocator_t* allocator)
	{
		grid_clear(grid);

		struct helper_t
		{
			coord_t hf[grid_t::length][grid_t::length];
		};

		helper_t* hlp = mem<helper_t>::alloc(allocator);
		rand_generator_t gen;
		for (auto& it_y : hlp->hf)
		{
			for (auto& it_x : it_y)
			{
				it_x = coord_t((int16_t)(gen.generate() & (grid_t::mask >> 1)));
			}
		}

		for (size_t x = 0; x < grid_t::length; ++x)
		{
			for (size_t y = 0; y < grid_t::length; ++y)
			{
				size_t variance_index = gen.generate() & grid_t::variance_mask;
				material_t* mat_dirt_variance = &grid->dirt_variance[variance_index];
				material_t* mat_grass_variance = &grid->grass_variance[variance_index];

				size_t z = 0;
				while (coord_t((int16_t)z) < hlp->hf[x][y])
				{
					grid->grid[x][y][z] = mat_dirt_variance;
					++z;
				}
				grid->grid[x][y][z] = mat_grass_variance;
			}
		}

		mem<helper_t>::free(allocator, hlp);
	}

	void grid_generate_terrain_drugs(grid_t* grid, allocator_t* allocator)
	{
		grid_clear(grid);

		struct helper_t
		{
			coord_t hf[grid_t::length][grid_t::length];
		};

		helper_t* hlp = mem<helper_t>::alloc(allocator);
		rand_generator_t gen;
		for (auto& it_y : hlp->hf)
		{
			for (auto& it_x : it_y)
			{
				it_x = coord_t((int16_t)(gen.generate() & (grid_t::mask >> 1)));
			}
		}

		//do relaxation
		{
			size_t relaxation_count = 4;

			for (size_t r = 0; r < relaxation_count; ++r)
			{
				for (size_t y = 0; y < grid_t::length; ++y)
				{
					for (size_t x = 0; x < grid_t::length; ++x)
					{
						coord_t sum =
							hlp->hf[(x + 0) & grid_t::mask][(y + 0) & grid_t::mask] +
							hlp->hf[(x + 1) & grid_t::mask][(y + 0) & grid_t::mask] +
							hlp->hf[(x + 0) & grid_t::mask][(y + 1) & grid_t::mask] +
							hlp->hf[(x + 1) & grid_t::mask][(y + 1) & grid_t::mask];
						hlp->hf[x][y] = sum / coord_t(4);
					}
				}
			}
		}

		for (size_t x = 0; x < grid_t::length; ++x)
		{
			for (size_t y = 0; y < grid_t::length; ++y)
			{
				size_t variance_index = gen.generate() & grid_t::variance_mask;
				material_t* mat_dirt_variance = &grid->dirt_variance[variance_index];
				material_t* mat_grass_variance = &grid->grass_variance[variance_index];

				size_t z = 0;
				while (coord_t((int16_t)z) <= hlp->hf[x][y])
				{
					//grid->grid[x][y][z] = mat_dirt_variance;
					grid->grid[x][y][z] = &grid->drugs_variance[z & grid_t::mask];
					++z;
				}
				//grid->grid[x][y][z] = mat_grass_variance;
			}
		}

		mem<helper_t>::free(allocator, hlp);
	}

	void grid_generate_terrain_height(grid_t* grid, allocator_t* allocator)
	{
		grid_clear(grid);

		struct helper_t
		{
			coord_t hf[grid_t::length][grid_t::length];
		};

		helper_t* hlp = mem<helper_t>::alloc(allocator);
		rand_generator_t gen;
		for (auto& it_y : hlp->hf)
		{
			for (auto& it_x : it_y)
			{
				it_x = coord_t((int16_t)(gen.generate() & (grid_t::mask >> 1)));
			}
		}

		//do relaxation
		{
			size_t relaxation_count = 2;

			for (size_t r = 0; r < relaxation_count; ++r)
			{
				for (size_t y = 0; y < grid_t::length; ++y)
				{
					for (size_t x = 0; x < grid_t::length; ++x)
					{
						coord_t sum =
							hlp->hf[(x + 0) & grid_t::mask][(y + 0) & grid_t::mask] +
							hlp->hf[(x + 1) & grid_t::mask][(y + 0) & grid_t::mask] +
							hlp->hf[(x + 0) & grid_t::mask][(y + 1) & grid_t::mask] +
							hlp->hf[(x + 1) & grid_t::mask][(y + 1) & grid_t::mask];
						hlp->hf[x][y] = sum / coord_t(4);
					}
				}
			}
		}

		for (size_t x = 0; x < grid_t::length; ++x)
		{
			for (size_t y = 0; y < grid_t::length; ++y)
			{
				size_t variance_index = gen.generate() & grid_t::variance_mask;
				material_t* mat_dirt_variance = &grid->dirt_variance[variance_index];
				material_t* mat_grass_variance = &grid->grass_variance[variance_index];

				size_t z = 0;
				while (coord_t((int16_t)z) <= hlp->hf[x][y])
				{
					//grid->grid[x][y][z] = mat_dirt_variance;
					grid->grid[x][y][z] = &grid->height_variance[z & grid_t::mask];
					++z;
				}
				//grid->grid[x][y][z] = mat_grass_variance;
			}
		}

		mem<helper_t>::free(allocator, hlp);
	}


	material_t* ray_visit(grid_t* grid, size_t(&pos)[3])
	{
		return grid->grid
			[pos[0] & grid_t::mask]
			[pos[1] & grid_t::mask]
			[pos[2] & grid_t::mask];
	}

	error_t grid_create(grid_t*& grid, allocator_t* allocator)
	{
		grid = mem<grid_t>::alloc(allocator);
		if (nullptr == grid)
			return err_out_of_memory;

		grid->light_dir = { coord_t(0.47), coord_t(0.3), coord_t(-0.707) };

		//init materials
		grid->sky._color = 0x003F3F3F;
		grid->dirt._color = 0x001F1F00;
		grid->grass._color = 0x00003F00;

		coord_t const variance_scale = coord_t(0.3);
		coord_t const variance_offset = coord_t(1) - variance_scale;

		rand_generator_t gen;
		for (auto& it : grid->dirt_variance)
		{
			auto b = coord_t((int16_t)((grid->dirt._color >> 0) & 0xFF));
			auto g = coord_t((int16_t)((grid->dirt._color >> 8) & 0xFF));
			auto r = coord_t((int16_t)((grid->dirt._color >> 16) & 0xFF));
			auto rand = coord_t((int16_t)(gen.generate() & 0xFF)) / coord_t(0xFF);
			//scale rand
			rand = rand * variance_scale + variance_offset;
			
			b *= rand;
			g *= rand;
			r *= rand;

			it._color =
				((((int16_t)b) & 0xFF) << 0) |
				((((int16_t)g) & 0xFF) << 8) |
				((((int16_t)r) & 0xFF) << 16);
		}

		for (auto& it : grid->grass_variance)
		{
			auto b = coord_t((int16_t)((grid->grass._color >> 0) & 0xFF));
			auto g = coord_t((int16_t)((grid->grass._color >> 8) & 0xFF));
			auto r = coord_t((int16_t)((grid->grass._color >> 16) & 0xFF));
			auto rand = coord_t((int16_t)(gen.generate() & 0xFF)) / coord_t(0xFF);
			//scale rand
			rand = rand * variance_scale + variance_offset;

			b *= rand;
			g *= rand;
			r *= rand;

			it._color =
				((((int16_t)b) & 0xFF) << 0) |
				((((int16_t)g) & 0xFF) << 8) |
				((((int16_t)r) & 0xFF) << 16);
		}

		for (size_t i = 0; i < u::array_size(grid->height_variance); ++i)
		{
			auto& it = grid->height_variance[i];
			//it._color = gen.generate() & 0xFFffFF;
			it._color = gen.generate() & 0x3F3f3F;
		}

		for (size_t i = 0; i < u::array_size(grid->drugs_variance); ++i)
		{
			auto& it = grid->drugs_variance[i];
			it._color = gen.generate() & 0xFFffFF;
			//it._color = gen.generate() & 0x3F3f3F;
		}

		/*for (size_t i = 0; i < u::array_size(grid->height_variance); ++i)
		{
			auto& it = grid->height_variance[i];

			auto b = coord_t((int16_t)((grid->grass._color >> 0) & 0xFF));
			auto g = coord_t((int16_t)((grid->grass._color >> 8) & 0xFF));
			auto r = coord_t((int16_t)((grid->grass._color >> 16) & 0xFF));
			auto rand = coord_t((int16_t)(i + 1)) / coord_t((int16_t)(u::array_size(grid->height_variance)));
			//scale rand
			//rand = rand * variance_scale + variance_offset;

			b *= rand;
			g *= rand;
			r *= rand;

			it._color =
				((((int16_t)b) & 0xFF) << 0) |
				((((int16_t)g) & 0xFF) << 8) |
				((((int16_t)r) & 0xFF) << 16);
		}*/


		//init grid
		grid_clear(grid);

		//generate
		grid_generate_terrain_pillars(grid, allocator);

		/*grid->grid[4][2][1] = &grid->grass;
		grid->grid[4][2][2] = &grid->dirt;
		grid->grid[7][2][30] = &grid->dirt;*/

		return err_ok;
	}

	void grid_destroy(grid_t* grid, allocator_t* allocator)
	{
		mem<grid_t>::free(allocator, grid);
	}

	

}
