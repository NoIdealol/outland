#pragma once

#include "raw_mouse_win.h"
#include "raw_keyboard_win.h"

typedef struct RawGamepadModule
{
	Allocator	allocator;

} RawGamepadModule;

typedef struct RawJoystickModule
{
	Allocator	allocator;

} RawJoystickModule;

typedef struct RawInput
{
	Allocator			allocator;
	Input				input;

	HWND				wnd;
	ATOM				cls;

	flags_t				modules;
	RawMouseModule		mouse;
	RawKeyboardModule	keyboard;
	RawGamepadModule	gamepad;
	RawJoystickModule	joystick;

} RawInput;


bool_t	raw_input_init(RawInput* raw_input, Input input, Allocator allocator);
void	raw_input_clean(RawInput* raw_input);
void	raw_input_update(RawInput* raw_input);
bool_t	raw_input_init_mouse(RawInput* raw_input);
bool_t	raw_input_init_keyboard(RawInput* raw_input);
bool_t	raw_input_init_gamepad(RawInput* raw_input);
bool_t	raw_input_init_joystick(RawInput* raw_input);
void	raw_input_clean_mouse(RawInput* raw_input);
void	raw_input_clean_keyboard(RawInput* raw_input);
void	raw_input_clean_gamepad(RawInput* raw_input);
void	raw_input_clean_joystick(RawInput* raw_input);