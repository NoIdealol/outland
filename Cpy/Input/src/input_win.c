#include "core.h"
#if OS_WINDOWS
#include "input_win.h"
#include <Windows.h>
#include "raw_input_win.h"
#include <stdio.h>

struct Input
{
	Allocator	allocator;
	InputEvent* ev_array;
	size_t		ev_head;
	size_t		ev_size;
	size_t		ev_capacity;
	RawInput	raw_input;
};

//--------------------------------------------------
void	input_log_error(Input input, char8_t const* format, ...)
{
	va_list va;
	va_start(va, format);
	char8_t buffer[256] = { 0 };
	int res = vsnprintf(buffer, lengthof(buffer), format, va);
	va_end(va);
	if (res >= 0 && res < (lengthof(buffer) - 1))
	{
		buffer[res] = '\n';
		OutputDebugStringA(buffer);
	}
}

//--------------------------------------------------
bool_t	input_queue_event(Input input, InputEvent const* ev)
{
	if (input->ev_size == input->ev_capacity)
		return false;

	memcpy(input->ev_array + input->ev_size, ev, sizeof(InputEvent));
	++input->ev_size;
	return true;
}

//--------------------------------------------------
bool_t	input_init(Input* input, Allocator allocator, size_t ev_capacity)
{
	Input in = mem_alloc(allocator, sizeof(struct Input));
	if (!in)
	{
		return false;
	}

	in->allocator = allocator;
	in->ev_array = mem_alloc(allocator, sizeof(InputEvent) * ev_capacity);
	if (!in->ev_array)
	{
		mem_free(allocator, in, sizeof(struct Input));
		return false;
	}
	in->ev_capacity = ev_capacity;
	in->ev_head = 0;
	in->ev_size = 0;
	if (!raw_input_init(&in->raw_input, in, allocator))
	{
		mem_free(allocator, in->ev_array, sizeof(InputEvent) * ev_capacity);
		mem_free(allocator, in, sizeof(struct Input));
		return false;
	}

	*input = in;
	return true;
}

//--------------------------------------------------
void	input_clean(Input input)
{
	raw_input_clean(&input->raw_input);
	mem_free(input->allocator, input->ev_array, sizeof(InputEvent) * input->ev_capacity);
	mem_free(input->allocator, input, sizeof(struct Input));
}

//--------------------------------------------------
bool_t	input_module_init(Input input, InputDeviceType type)
{
	switch (type)
	{
	case INPUT_DEVICE_TYPE_MOUSE:
		return raw_input_init_mouse(&input->raw_input);
	case INPUT_DEVICE_TYPE_KEYBOARD:
		return raw_input_init_keyboard(&input->raw_input);
	case INPUT_DEVICE_TYPE_GAMEPAD:
		return raw_input_init_gamepad(&input->raw_input);
	case INPUT_DEVICE_TYPE_JOYSTICK:
		return raw_input_init_joystick(&input->raw_input);
	default:
		return false;
	}
}

//--------------------------------------------------
void	input_module_clean(Input input, InputDeviceType type)
{
	switch (type)
	{
	case INPUT_DEVICE_TYPE_MOUSE:
		raw_input_clean_mouse(&input->raw_input);
		return;
	case INPUT_DEVICE_TYPE_KEYBOARD:
		raw_input_clean_keyboard(&input->raw_input);
		return;
	case INPUT_DEVICE_TYPE_GAMEPAD:
		 raw_input_clean_gamepad(&input->raw_input);
		 return;
	default:
		return;
	}
}

//--------------------------------------------------
void	input_update(Input input)
{
	input->ev_size = 0;
	input->ev_head = 0;
	raw_input_update(&input->raw_input);
}

//--------------------------------------------------
size_t	input_epoll(Input input, InputEvent* ev_array, size_t ev_capacity)
{
	size_t ev_size = m_min_size(input->ev_size - input->ev_head, ev_capacity);
	memcpy(ev_array, input->ev_array + input->ev_head, sizeof(InputEvent) * ev_size);
	input->ev_head += ev_size;
	return ev_size;
}

#endif