#pragma once
#include "input_win.h"


typedef struct RawMouseDevice
{
	HANDLE	device;
	uint8_t	buttons;
	bool_t	is_present;
	bool_t	is_signaled;
	bool_t	is_reported;
} RawMouseDevice;

typedef struct RawMouseModule
{
	Input			input;
	Allocator		allocator;
	RawMouseDevice*	device_array;
	size_t			device_capacity;
	size_t			device_size;
} RawMouseModule;

bool_t	raw_mouse_init(RawMouseModule* module, Input input, Allocator allocator);
void	raw_mouse_clean(RawMouseModule* module);
void	raw_mouse_update(RawMouseModule* module);
void	raw_mouse_input(RawMouseModule* module, HANDLE device, HRAWINPUT hInput);
void	raw_mouse_added(RawMouseModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo);
void	raw_mouse_removed(RawMouseModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo);