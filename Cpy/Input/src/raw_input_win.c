#include "core.h"
#if OS_WINDOWS
#include <Windows.h>
#include <hidusage.h>
#include "raw_input_win.h"

bool_t	raw_gamepad_init(RawGamepadModule* module, Input input, Allocator allocator) { return false; }
void	raw_gamepad_clean(RawGamepadModule* module) {}
void	raw_gamepad_update(RawGamepadModule* module) {}
void	raw_gamepad_input(RawGamepadModule* module, HANDLE device, HRAWINPUT hInput) {}
void	raw_gamepad_added(RawGamepadModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo) {}
void	raw_gamepad_removed(RawGamepadModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo) {}

bool_t	raw_joystick_init(RawJoystickModule* module, Input input, Allocator allocator) { return false; }
void	raw_joystick_clean(RawJoystickModule* module) {}
void	raw_joystick_update(RawJoystickModule* module) {}
void	raw_joystick_input(RawJoystickModule* module, HANDLE device, HRAWINPUT hInput) {}
void	raw_joystick_added(RawJoystickModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo) {}
void	raw_joystick_removed(RawJoystickModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo) {}

//--------------------------------------------------
static LRESULT CALLBACK raw_input_wnd_callback(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CREATE:
	{
		CREATESTRUCT* param = (CREATESTRUCT*)lParam;
		SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)param->lpCreateParams);
		break;
	}
	case WM_INPUT:
	{
		//if (RIM_INPUT == wParam)
		{
			RawInput* const raw_input = (RawInput*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
			HRAWINPUT const hInput = (HRAWINPUT)lParam;
			RAWINPUTHEADER inputHeader;
			UINT inputHeaderSize = sizeof(RAWINPUTHEADER);
			UINT res = GetRawInputData(hInput, RID_HEADER, &inputHeader, &inputHeaderSize, inputHeaderSize);
			if (res != (UINT)-1 && res != 0)
			{
				switch (inputHeader.dwType)
				{
				case RIM_TYPEMOUSE:
					raw_mouse_input(&raw_input->mouse, inputHeader.hDevice, hInput);
					break;
				case RIM_TYPEKEYBOARD:
					raw_keyboard_input(&raw_input->keyboard, inputHeader.hDevice, hInput);
					break;
				case RIM_TYPEHID:
				{
					RID_DEVICE_INFO deviceInfo;
					deviceInfo.cbSize = sizeof(RID_DEVICE_INFO);
					UINT deviceInfoSize = sizeof(RID_DEVICE_INFO);
					UINT res = GetRawInputDeviceInfo(inputHeader.hDevice, RIDI_DEVICEINFO, &deviceInfo, &deviceInfoSize);
					if (res != (UINT)-1 && res != 0)
					{
						switch (deviceInfo.hid.usUsage)
						{
						case HID_USAGE_GENERIC_GAMEPAD:
							raw_gamepad_input(&raw_input->gamepad, inputHeader.hDevice, hInput);
							break;
						case HID_USAGE_GENERIC_JOYSTICK:
							raw_joystick_input(&raw_input->joystick, inputHeader.hDevice, hInput);
							break;
						default:
							break;
						}
					}
					break;
				}
				default:
					break; //return def proc to cleanup
				}
			}
		}

		DefWindowProcW(hWnd, msg, wParam, lParam); //clean the msg
		return 0;
	}
	case WM_INPUT_DEVICE_CHANGE:
	{
		RawInput* const raw_input = (RawInput*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		HANDLE const hDevice = (HANDLE)lParam;

		RID_DEVICE_INFO deviceInfo;
		deviceInfo.cbSize = sizeof(RID_DEVICE_INFO);
		UINT deviceInfoSize = sizeof(RID_DEVICE_INFO);
		UINT res = GetRawInputDeviceInfo(hDevice, RIDI_DEVICEINFO, &deviceInfo, &deviceInfoSize);
		if (res != (UINT)-1 && res != 0)
		{
			switch (wParam)
			{
			case GIDC_ARRIVAL:
			{
				switch (deviceInfo.dwType)
				{
				case RIM_TYPEMOUSE:
				{
					raw_mouse_added(&raw_input->mouse, hDevice, &deviceInfo);
					break;
				}
				case RIM_TYPEKEYBOARD:
				{
					raw_keyboard_added(&raw_input->keyboard, hDevice, &deviceInfo);
					break;
				}
				case RIM_TYPEHID:
				{
					switch (deviceInfo.hid.usUsage)
					{
					case HID_USAGE_GENERIC_GAMEPAD:
						raw_gamepad_added(&raw_input->gamepad, hDevice, &deviceInfo);
						break;
					case HID_USAGE_GENERIC_JOYSTICK:
						raw_joystick_added(&raw_input->joystick, hDevice, &deviceInfo);
						break;
					default:
						break;
					}

					break;
				}
				default:
					break;
				}

				break;
			}
			case GIDC_REMOVAL:
			{
				switch (deviceInfo.dwType)
				{
				case RIM_TYPEMOUSE:
				{
					raw_mouse_removed(&raw_input->mouse, hDevice, &deviceInfo);
					break;
				}
				case RIM_TYPEKEYBOARD:
				{
					raw_keyboard_removed(&raw_input->keyboard, hDevice, &deviceInfo);
					break;
				}
				case RIM_TYPEHID:
				{
					switch (deviceInfo.hid.usUsage)
					{
					case HID_USAGE_GENERIC_GAMEPAD:
						raw_gamepad_removed(&raw_input->gamepad, hDevice, &deviceInfo);
						break;
					case HID_USAGE_GENERIC_JOYSTICK:
						raw_joystick_removed(&raw_input->joystick, hDevice, &deviceInfo);
						break;
					default:
						break;
					}

					break;
				}
				default:
					break;
				}

				break;
			}
			default:
				break;
			}
		}

		DefWindowProcW(hWnd, msg, wParam, lParam);
		return 0;
	}
	default:
		break;
	}

	return DefWindowProcW(hWnd, msg, wParam, lParam);
}

//--------------------------------------------------
bool_t raw_input_init(RawInput* raw_input, Input input, Allocator allocator)
{
	SetLastError(0);
	
	HICON hIcon = NULL;
	HICON hIconSm = NULL;
	HBRUSH hbrBackground = (HBRUSH)(COLOR_3DDKSHADOW + 1);
	HINSTANCE hInstance = GetModuleHandle(NULL);

	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof(WNDCLASSEXW);
	wcex.style = 0;
	wcex.lpfnWndProc = &raw_input_wnd_callback;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = hIcon;
	wcex.hIconSm = hIconSm;
	wcex.hCursor = NULL;
	wcex.hbrBackground = hbrBackground;
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"RAW_INPUT_WND_CLS";

	ATOM cls = RegisterClassExW(&wcex);
	if (!cls)
	{
		input_log_error(input, "RegisterClassExW : CLS = %S, E = %ul", wcex.lpszClassName, GetLastError());
		return false;
	}

	HWND wnd = CreateWindowExW(
		WS_EX_TOOLWINDOW,
		(LPCWSTR)cls,
		NULL,
		WS_POPUP,
		0, 0,
		100, 100,
		HWND_MESSAGE,
		NULL,
		hInstance,
		raw_input);

	if (NULL == wnd)
	{
		UnregisterClassW((LPCWSTR)cls, hInstance);
		input_log_error(input, "CreateWindowExW : CLS = %S, E = %ul", wcex.lpszClassName, GetLastError());
		return false;
	}

	raw_input->allocator = allocator;
	raw_input->input = input;
	raw_input->cls = cls;
	raw_input->wnd = wnd;
	raw_input->modules = 0;
	return true;
}


//--------------------------------------------------
void raw_input_clean(RawInput* raw_input)
{
	HINSTANCE hInstance = GetModuleHandle(NULL);
	DestroyWindow(raw_input->wnd);
	UnregisterClassW((LPCWSTR)raw_input->cls, hInstance);
}

//--------------------------------------------------
void	raw_input_update(RawInput* raw_input)
{
	if ((1 << INPUT_DEVICE_TYPE_MOUSE) & raw_input->modules)
		raw_mouse_update(&raw_input->mouse);
	if ((1 << INPUT_DEVICE_TYPE_KEYBOARD) & raw_input->modules)
		raw_keyboard_update(&raw_input->keyboard);
	if ((1 << INPUT_DEVICE_TYPE_GAMEPAD) & raw_input->modules)
		raw_gamepad_update(&raw_input->gamepad);
	if ((1 << INPUT_DEVICE_TYPE_JOYSTICK) & raw_input->modules)
		raw_joystick_update(&raw_input->joystick);
	
	MSG msg;
	while (PeekMessageW(&msg, raw_input->wnd, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessageW(&msg);
	}
}

//--------------------------------------------------
bool_t raw_input_init_mouse(RawInput* raw_input)
{
	if (raw_input->modules & (1 << INPUT_DEVICE_TYPE_MOUSE))
		return true;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
	device.usUsage = HID_USAGE_GENERIC_MOUSE;
	device.hwndTarget = raw_input->wnd;

	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_MOUSE, E = %ul", GetLastError());
		return false;
	}

	if (!raw_mouse_init(&raw_input->mouse, raw_input->input, raw_input->allocator))
	{
		return false;
	}

	raw_input->modules |= 1 << INPUT_DEVICE_TYPE_MOUSE;
	return true;
}

//--------------------------------------------------
bool_t raw_input_init_keyboard(RawInput* raw_input)
{
	if (raw_input->modules & (1 << INPUT_DEVICE_TYPE_KEYBOARD))
		return true;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
	device.usUsage = HID_USAGE_GENERIC_KEYBOARD;
	device.hwndTarget = raw_input->wnd;

	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_KEYBOARD, E = %ul", GetLastError());
		return false;
	}

	if (!raw_keyboard_init(&raw_input->keyboard, raw_input->input, raw_input->allocator))
	{
		return false;
	}

	raw_input->modules |= 1 << INPUT_DEVICE_TYPE_KEYBOARD;
	return true;
}

//--------------------------------------------------
bool_t raw_input_init_gamepad(RawInput* raw_input)
{
	if (raw_input->modules & (1 << INPUT_DEVICE_TYPE_GAMEPAD))
		return true;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
	device.usUsage = HID_USAGE_GENERIC_GAMEPAD;
	device.hwndTarget = raw_input->wnd;

	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_GAMEPAD, E = %ul", GetLastError());
		return false;
	}

	if (!raw_gamepad_init(&raw_input->gamepad, raw_input->input, raw_input->allocator))
	{
		return false;
	}

	raw_input->modules |= 1 << INPUT_DEVICE_TYPE_GAMEPAD;
	return true;
}

//--------------------------------------------------
bool_t raw_input_init_joystick(RawInput* raw_input)
{
	if (raw_input->modules & (1 << INPUT_DEVICE_TYPE_JOYSTICK))
		return true;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
	device.usUsage = HID_USAGE_GENERIC_JOYSTICK;
	device.hwndTarget = raw_input->wnd;

	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_JOYSTICK, E = %ul", GetLastError());
		return false;
	}

	if (!raw_joystick_init(&raw_input->joystick, raw_input->input, raw_input->allocator))
	{
		return false;
	}

	raw_input->modules |= 1 << INPUT_DEVICE_TYPE_JOYSTICK;
	return true;
}

//--------------------------------------------------
void raw_input_clean_mouse(RawInput* raw_input)
{
	if (0 == (raw_input->modules & (1 << INPUT_DEVICE_TYPE_MOUSE)))
		return;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_REMOVE;
	device.usUsage = HID_USAGE_GENERIC_MOUSE;
	device.hwndTarget = NULL;
	
	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_MOUSE, E = %ul", GetLastError());
	}

	raw_mouse_clean(&raw_input->mouse);
	raw_input->modules ^= 1 << INPUT_DEVICE_TYPE_MOUSE;
}

//--------------------------------------------------
void raw_input_clean_keyboard(RawInput* raw_input)
{
	if (0 == (raw_input->modules & (1 << INPUT_DEVICE_TYPE_KEYBOARD)))
		return;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_REMOVE;
	device.usUsage = HID_USAGE_GENERIC_KEYBOARD;
	device.hwndTarget = NULL;

	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_KEYBOARD, E = %ul", GetLastError());
	}

	raw_keyboard_clean(&raw_input->keyboard);
	raw_input->modules ^= 1 << INPUT_DEVICE_TYPE_KEYBOARD;
}


//--------------------------------------------------
void raw_input_clean_gamepad(RawInput* raw_input)
{
	if (0 == (raw_input->modules & (1 << INPUT_DEVICE_TYPE_GAMEPAD)))
		return;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_REMOVE;
	device.usUsage = HID_USAGE_GENERIC_GAMEPAD;
	device.hwndTarget = NULL;

	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_GAMEPAD, E = %ul", GetLastError());
	}

	raw_gamepad_clean(&raw_input->gamepad);
	raw_input->modules ^= 1 << INPUT_DEVICE_TYPE_GAMEPAD;
}

//--------------------------------------------------
void raw_input_clean_joystick(RawInput* raw_input)
{
	if (0 == (raw_input->modules & (1 << INPUT_DEVICE_TYPE_JOYSTICK)))
		return;

	RAWINPUTDEVICE device;
	device.usUsagePage = HID_USAGE_PAGE_GENERIC;
	device.dwFlags = RIDEV_REMOVE;
	device.usUsage = HID_USAGE_GENERIC_JOYSTICK;
	device.hwndTarget = NULL;

	if (FALSE == RegisterRawInputDevices(&device, 1, sizeof(device)))
	{
		input_log_error(raw_input->input, "RegisterRawInputDevices : USAGE = HID_USAGE_GENERIC_JOYSTICK, E = %ul", GetLastError());
	}

	raw_joystick_clean(&raw_input->joystick);
	raw_input->modules ^= 1 << INPUT_DEVICE_TYPE_JOYSTICK;
}

#endif