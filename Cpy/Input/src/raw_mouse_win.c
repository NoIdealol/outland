#include "core.h"
#if OS_WINDOWS
#include <Windows.h>
#include "raw_mouse_win.h"

enum
{
	DEVICE_ARRAY_CHUNK = 16,
};

//--------------------------------------------------
static RawMouseDevice* raw_mouse_find(RawMouseModule* module, HANDLE device)
{
	RawMouseDevice* const beg = module->device_array;
	RawMouseDevice* const end = module->device_array + module->device_size;
	for (RawMouseDevice* it = beg; beg != end; ++it)
	{
		if (it->device == device && it->is_present)
			return it;
	}

	return null;
}

//--------------------------------------------------
bool_t	raw_mouse_init(RawMouseModule* module, Input input, Allocator allocator)
{
	module->input = input;
	module->allocator = allocator;
	module->device_size = 0;
	module->device_capacity = DEVICE_ARRAY_CHUNK;
	module->device_array = mem_alloc(allocator, sizeof(RawMouseDevice) * module->device_capacity);
	if (null == module->device_array)
	{
		input_log_error(input, "raw_mouse_init : mem_alloc");
		return false;
	}

	return true;
}

//--------------------------------------------------
void	raw_mouse_clean(RawMouseModule* module)
{
	mem_free(module->allocator, module->device_array, sizeof(RawMouseDevice) * module->device_capacity);
}

//--------------------------------------------------
void	raw_mouse_update(RawMouseModule* module)
{
	RawMouseDevice* beg = module->device_array;
	RawMouseDevice* end = module->device_array + module->device_size;
	RawMouseDevice* it;

	it = beg;
	while (it != end)
	{
		if (it->is_present)
		{
			++it;
		}
		else
		{
			InputEvent ev;
			ev.event_type = INPUT_EVENT_TYPE_DEVICE_REMOVED;
			ev.device_type = INPUT_DEVICE_TYPE_MOUSE;
			ev.device_id = (InputDeviceId)it->device;
			memset(&ev.device_state.mouse, 0, sizeof(MouseState));
			ev.device_state.mouse.buttons = it->buttons;

			if (input_queue_event(module->input, &ev))
			{
				--end;
				--module->device_size;
				if (end != it)
					memcpy(it, end, sizeof(RawMouseDevice));
			}
			else
			{
				return;
			}
		}
	}

	it = beg;
	while (it != end)
	{
		if (it->is_present)
		{
			if (it->is_signaled)
			{
				if (false == it->is_reported)
				{
					InputEvent ev;
					ev.event_type = INPUT_EVENT_TYPE_DEVICE_READING;
					ev.device_type = INPUT_DEVICE_TYPE_MOUSE;
					ev.device_id = (InputDeviceId)it->device;
					memset(&ev.device_state.mouse, 0, sizeof(MouseState));
					ev.device_state.mouse.buttons = it->buttons;

					it->is_reported = input_queue_event(module->input, &ev);
				}
			}
			else
			{
				InputEvent ev;
				ev.event_type = INPUT_EVENT_TYPE_DEVICE_ADDED;
				ev.device_type = INPUT_DEVICE_TYPE_MOUSE;
				ev.device_id = (InputDeviceId)it->device;
				memset(&ev.device_state.mouse, 0, sizeof(MouseState));

				it->is_signaled = input_queue_event(module->input, &ev);
			}
		}
		
		++it;
	}
}

//--------------------------------------------------
void	raw_mouse_input(RawMouseModule* module, HANDLE device, HRAWINPUT hInput)
{
	RawMouseDevice* raw_mouse = raw_mouse_find(module, device);
	if (null == raw_mouse)
		return;

	RAWINPUT input;
	UINT size = sizeof(RAWINPUT);
	UINT res = GetRawInputData(hInput, RID_INPUT, &input, &size, sizeof(RAWINPUTHEADER));
	if (res == ((UINT)-1) || (res == 0) || input.header.dwType != RIM_TYPEMOUSE)
		return;

	RAWMOUSE* mouse = &input.data.mouse;
	int16_t axis[3];
	memset(axis, 0, sizeof(axis));

	bool_t axis_changed = false;
	if (MOUSE_MOVE_RELATIVE == mouse->usFlags) //flag is zero!!!
	{
		axis[MOUSE_AXIS_X] = (int16_t)mouse->lLastX;
		axis[MOUSE_AXIS_Y] = (int16_t)mouse->lLastY;
		axis_changed = true;
	}
	
	if (RI_MOUSE_WHEEL & mouse->usButtonFlags)
	{
		axis[MOUSE_AXIS_WHEEL] = (int16_t)mouse->usButtonData;
		axis_changed = true;
	}
	
	uint8_t buttons = raw_mouse->buttons;
	if (RI_MOUSE_BUTTON_1_DOWN & mouse->usButtonFlags)
		buttons |= 1 << MOUSE_BUTTON_LEFT;
	if (RI_MOUSE_BUTTON_2_DOWN & mouse->usButtonFlags)
		buttons |= 1 << MOUSE_BUTTON_RIGHT;
	if (RI_MOUSE_BUTTON_3_DOWN & mouse->usButtonFlags)
		buttons |= 1 << MOUSE_BUTTON_MIDDLE;
	if (RI_MOUSE_BUTTON_4_DOWN & mouse->usButtonFlags)
		buttons |= 1 << MOUSE_BUTTON_4;
	if (RI_MOUSE_BUTTON_5_DOWN & mouse->usButtonFlags)
		buttons |= 1 << MOUSE_BUTTON_5;

	if (RI_MOUSE_BUTTON_1_UP & mouse->usButtonFlags)
		buttons &= ~(1 << MOUSE_BUTTON_LEFT);
	if (RI_MOUSE_BUTTON_2_UP & mouse->usButtonFlags)
		buttons &= ~(1 << MOUSE_BUTTON_RIGHT);
	if (RI_MOUSE_BUTTON_3_UP & mouse->usButtonFlags)
		buttons &= ~(1 << MOUSE_BUTTON_MIDDLE);
	if (RI_MOUSE_BUTTON_4_UP & mouse->usButtonFlags)
		buttons &= ~(1 << MOUSE_BUTTON_4);
	if (RI_MOUSE_BUTTON_5_UP & mouse->usButtonFlags)
		buttons &= ~(1 << MOUSE_BUTTON_5);

	bool_t buttons_changed = raw_mouse->buttons == buttons ? false : true;
	raw_mouse->buttons = buttons;

	if ((buttons_changed || axis_changed) && raw_mouse->is_signaled)
	{
		InputEvent ev;
		ev.event_type = INPUT_EVENT_TYPE_DEVICE_READING;
		ev.device_type = INPUT_DEVICE_TYPE_MOUSE;
		ev.device_id = (InputDeviceId)raw_mouse->device;
		memset(&ev.device_state.mouse, 0, sizeof(MouseState));
		ev.device_state.mouse.axis[0] = axis[0];
		ev.device_state.mouse.axis[1] = axis[1];
		ev.device_state.mouse.axis[2] = axis[2];
		ev.device_state.mouse.buttons = buttons;

		raw_mouse->is_reported = input_queue_event(module->input, &ev);
	}
}

//--------------------------------------------------
void	raw_mouse_added(RawMouseModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo)
{
	if (module->device_capacity == module->device_size)
	{
		size_t new_device_capacity = module->device_capacity + DEVICE_ARRAY_CHUNK;
		RawMouseDevice* new_device_array = mem_alloc(module->allocator, sizeof(RawMouseDevice) * new_device_capacity);
		if (null == new_device_array)
		{
			input_log_error(module->input, "raw_mouse_added : mem_alloc");
			return;
		}
		
		memcpy(new_device_array, module->device_array, sizeof(RawMouseDevice) * module->device_capacity);
		mem_free(module->allocator, module->device_array, sizeof(RawMouseDevice) * module->device_capacity);
		module->device_array = new_device_array;
		module->device_capacity = new_device_capacity;
	}

	RawMouseDevice* raw_mouse = module->device_array + module->device_size;
	module->device_size += 1;
	raw_mouse->device = device;
	raw_mouse->buttons = 0;
	raw_mouse->is_present = true;
	raw_mouse->is_signaled = false;
	raw_mouse->is_reported = true;
	
	
	InputEvent ev;
	ev.event_type = INPUT_EVENT_TYPE_DEVICE_ADDED;
	ev.device_type = INPUT_DEVICE_TYPE_MOUSE;
	ev.device_id = (InputDeviceId)raw_mouse->device;
	memset(&ev.device_state.mouse, 0, sizeof(MouseState));

	raw_mouse->is_signaled = input_queue_event(module->input, &ev);
}

//--------------------------------------------------
void	raw_mouse_removed(RawMouseModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo)
{
	RawMouseDevice* raw_mouse = raw_mouse_find(module, device);
	if (null == raw_mouse)
		return;

	InputEvent ev;
	ev.event_type = INPUT_EVENT_TYPE_DEVICE_REMOVED;
	ev.device_type = INPUT_DEVICE_TYPE_MOUSE;
	ev.device_id = (InputDeviceId)raw_mouse->device;
	memset(&ev.device_state.mouse, 0, sizeof(MouseState));
	ev.device_state.mouse.buttons = raw_mouse->buttons;

	if (raw_mouse->is_signaled && false == input_queue_event(module->input, &ev))
	{
		raw_mouse->is_present = false;
		return;
	}
	
	--module->device_size;
	RawMouseDevice* const end = module->device_array + module->device_size;
	if (end != raw_mouse)
		memcpy(raw_mouse, end, sizeof(RawMouseDevice));
}

#endif