#pragma once
#include "input.h"

void	input_log_error(Input input, char8_t const* format, ...);
bool_t	input_queue_event(Input input, InputEvent const* ev);
