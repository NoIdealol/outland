#include "core.h"
#if OS_WINDOWS
#include <Windows.h>
#include "raw_keyboard_win.h"

//--------------------------------------------------
enum
{
	DEVICE_ARRAY_CHUNK = 16,
};

//--------------------------------------------------
enum ScanCode
{
	SCAN_CODE_escape = 0x01,
	SCAN_CODE_1 = 0x02,
	SCAN_CODE_2 = 0x03,
	SCAN_CODE_3 = 0x04,
	SCAN_CODE_4 = 0x05,
	SCAN_CODE_5 = 0x06,
	SCAN_CODE_6 = 0x07,
	SCAN_CODE_7 = 0x08,
	SCAN_CODE_8 = 0x09,
	SCAN_CODE_9 = 0x0A,
	SCAN_CODE_0 = 0x0B,
	SCAN_CODE_minus = 0x0C,
	SCAN_CODE_equals = 0x0D,
	SCAN_CODE_backspace = 0x0E,
	SCAN_CODE_tab = 0x0F,
	SCAN_CODE_q = 0x10,
	SCAN_CODE_w = 0x11,
	SCAN_CODE_e = 0x12,
	SCAN_CODE_r = 0x13,
	SCAN_CODE_t = 0x14,
	SCAN_CODE_y = 0x15,
	SCAN_CODE_u = 0x16,
	SCAN_CODE_i = 0x17,
	SCAN_CODE_o = 0x18,
	SCAN_CODE_p = 0x19,
	SCAN_CODE_bracket_left = 0x1A,
	SCAN_CODE_bracket_right = 0x1B,
	SCAN_CODE_enter = 0x1C,
	SCAN_CODE_control_left = 0x1D,
	SCAN_CODE_a = 0x1E,
	SCAN_CODE_s = 0x1F,
	SCAN_CODE_d = 0x20,
	SCAN_CODE_f = 0x21,
	SCAN_CODE_g = 0x22,
	SCAN_CODE_h = 0x23,
	SCAN_CODE_j = 0x24,
	SCAN_CODE_k = 0x25,
	SCAN_CODE_l = 0x26,
	SCAN_CODE_semicolon = 0x27,
	SCAN_CODE_quote = 0x28,
	SCAN_CODE_tilde = 0x29,
	SCAN_CODE_shift_left = 0x2A,
	SCAN_CODE_backslash = 0x2B,
	SCAN_CODE_z = 0x2C,
	SCAN_CODE_x = 0x2D,
	SCAN_CODE_c = 0x2E,
	SCAN_CODE_v = 0x2F,
	SCAN_CODE_b = 0x30,
	SCAN_CODE_n = 0x31,
	SCAN_CODE_m = 0x32,
	SCAN_CODE_comma = 0x33,
	SCAN_CODE_preiod = 0x34,
	SCAN_CODE_slash = 0x35,
	SCAN_CODE_shift_right = 0x36,
	SCAN_CODE_num_multiply = 0x37,
	SCAN_CODE_alt_left = 0x38,
	SCAN_CODE_space = 0x39,
	SCAN_CODE_capslock = 0x3A,
	SCAN_CODE_F1 = 0x3B,
	SCAN_CODE_F2 = 0x3C,
	SCAN_CODE_F3 = 0x3D,
	SCAN_CODE_F4 = 0x3E,
	SCAN_CODE_F5 = 0x3F,
	SCAN_CODE_F6 = 0x40,
	SCAN_CODE_F7 = 0x41,
	SCAN_CODE_F8 = 0x42,
	SCAN_CODE_F9 = 0x43,
	SCAN_CODE_F10 = 0x44,
	SCAN_CODE_numlock = 0x45, //should be 0xE045, in raw input 0x45
	SCAN_CODE_scrolllock = 0x46,
	SCAN_CODE_num_7 = 0x47,
	SCAN_CODE_num_8 = 0x48,
	SCAN_CODE_num_9 = 0x49,
	SCAN_CODE_num_minus = 0x4A,
	SCAN_CODE_num_4 = 0x4B,
	SCAN_CODE_num_5 = 0x4C,
	SCAN_CODE_num_6 = 0x4D,
	SCAN_CODE_num_plus = 0x4E,
	SCAN_CODE_num_1 = 0x4F,
	SCAN_CODE_num_2 = 0x50,
	SCAN_CODE_num_3 = 0x51,
	SCAN_CODE_num_0 = 0x52,
	SCAN_CODE_num_period = 0x53,
	SCAN_CODE_alt_printscreen = 0x54, //ALT + PrintScreen
	SCAN_CODE_backslash_102 = 0x56, //Key between the left shift and Z
	SCAN_CODE_F11 = 0x57,
	SCAN_CODE_F12 = 0x58,
	SCAN_CODE_help = 0x63,
	SCAN_CODE_F13 = 0x64,
	SCAN_CODE_F14 = 0x65,
	SCAN_CODE_F15 = 0x66,
	SCAN_CODE_F16 = 0x67,
	SCAN_CODE_F17 = 0x68,
	SCAN_CODE_F18 = 0x69,
	SCAN_CODE_F19 = 0x6a,
	SCAN_CODE_F20 = 0x6b,
	SCAN_CODE_F21 = 0x6c,
	SCAN_CODE_F22 = 0x6d,
	SCAN_CODE_F23 = 0x6e,
	SCAN_CODE_F24 = 0x76,
	SCAN_CODE_numpad_enter = 0xE01C,
	SCAN_CODE_control_right = 0xE01D,
	SCAN_CODE_numpad_divide = 0xE035,
	SCAN_CODE_printscreen = 0xE037, //sends two codes, 0xE02A, 0xE037, the first can be ignored
	SCAN_CODE_alt_right = 0xE038,
	SCAN_CODE_cancel = 0xE046, //CTRL + Pause
	SCAN_CODE_home = 0xE047,
	SCAN_CODE_arrow_up = 0xE048,
	SCAN_CODE_page_up = 0xE049,
	SCAN_CODE_arrow_left = 0xE04B,
	SCAN_CODE_arrow_right = 0xE04D,
	SCAN_CODE_end = 0xE04F,
	SCAN_CODE_arrow_down = 0xE050,
	SCAN_CODE_page_down = 0xE051,
	SCAN_CODE_insert = 0xE052,
	SCAN_CODE_delete = 0xE053,
	SCAN_CODE_win_left = 0xE05B, //Windows
	SCAN_CODE_win_right = 0xE05C,
	SCAN_CODE_menu = 0xE05D, //Menu, near Windows	
	SCAN_CODE_pause = 0xE11D45, //0xE11D 45 0xE19D C5, in raw input: 0xE11D 0x45, CTRL + Pause generates sc_cancel
};

//--------------------------------------------------
static bool_t	raw_keyboard_map_state(RawKeyboardDevice* device, RAWKEYBOARD const* keyboard)
{
	uint8_t key_id;
	uint32_t scancode = keyboard->MakeCode;
	if (0xFF <= scancode) //handle unexpected scancodes
		return false;

	if (RI_KEY_E0 & keyboard->Flags)
		scancode |= 0xE000;
	else if (RI_KEY_E1 & keyboard->Flags)
		scancode |= 0xE100;

	bool_t is_down;
	if (keyboard->Flags & RI_KEY_BREAK) //RI_KEY_MAKE flag is zero!!!
		is_down = false;
	else
		is_down = true;

	if (device->is_0xE11D)
	{
		//pause is in 2 parts: 0xE11D, 0x45
		device->is_0xE11D = false;
		if (0x45 == scancode)
			scancode = SCAN_CODE_pause;
	}

	switch (scancode)
	{
	case 0xE11D: device->is_0xE11D = true; return false;
		//0xE11D: first part of the Pause
		//0xE02A: first part of the PrintScreen scancode if no Shift, Control or Alt keys are pressed
		//0xE02A, 0xE0AA, 0xE036, 0xE0B6: generated in addition of Insert, Delete, Home, End, Page Up, Page Down, Up, Down, Left, Right when num lock is on; or when num lock is off but one or both shift keys are pressed
		//0xE02A, 0xE0AA, 0xE036, 0xE0B6: generated in addition of Numpad Divide and one or both Shift keys are pressed
		//When holding a key down, the pre/postfix (0xE02A) is not repeated!
	case 0xE02A: return false;
	case 0xE0AA: return false;
	case 0xE0B6: return false;
	case 0xE036: return false;
	default: return false;

	case SCAN_CODE_escape: key_id = KEYBOARD_KEY_ESCAPE; break;
	case SCAN_CODE_1: key_id = KEYBOARD_KEY_1; break;
	case SCAN_CODE_2: key_id = KEYBOARD_KEY_2; break;
	case SCAN_CODE_3: key_id = KEYBOARD_KEY_3; break;
	case SCAN_CODE_4: key_id = KEYBOARD_KEY_4; break;
	case SCAN_CODE_5: key_id = KEYBOARD_KEY_5; break;
	case SCAN_CODE_6: key_id = KEYBOARD_KEY_6; break;
	case SCAN_CODE_7: key_id = KEYBOARD_KEY_7; break;
	case SCAN_CODE_8: key_id = KEYBOARD_KEY_8; break;
	case SCAN_CODE_9: key_id = KEYBOARD_KEY_9; break;
	case SCAN_CODE_0: key_id = KEYBOARD_KEY_0; break;
	case SCAN_CODE_minus: key_id = KEYBOARD_KEY_MINUS; break;
	case SCAN_CODE_equals: key_id = KEYBOARD_KEY_EQUALS; break;
	case SCAN_CODE_backspace: key_id = KEYBOARD_KEY_BACKSPACE; break;
	case SCAN_CODE_tab: key_id = KEYBOARD_KEY_TAB; break;
	case SCAN_CODE_q: key_id = KEYBOARD_KEY_Q; break;
	case SCAN_CODE_w: key_id = KEYBOARD_KEY_W; break;
	case SCAN_CODE_e: key_id = KEYBOARD_KEY_E; break;
	case SCAN_CODE_r: key_id = KEYBOARD_KEY_R; break;
	case SCAN_CODE_t: key_id = KEYBOARD_KEY_T; break;
	case SCAN_CODE_y: key_id = KEYBOARD_KEY_Y; break;
	case SCAN_CODE_u: key_id = KEYBOARD_KEY_U; break;
	case SCAN_CODE_i: key_id = KEYBOARD_KEY_I; break;
	case SCAN_CODE_o: key_id = KEYBOARD_KEY_O; break;
	case SCAN_CODE_p: key_id = KEYBOARD_KEY_P; break;
	case SCAN_CODE_bracket_left: key_id = KEYBOARD_KEY_BRACE_LEFT; break;
	case SCAN_CODE_bracket_right: key_id = KEYBOARD_KEY_BRACE_RIGHT; break;
	case SCAN_CODE_enter: key_id = KEYBOARD_KEY_ENTER; break;
	case SCAN_CODE_control_left: key_id = KEYBOARD_KEY_CTRL_LEFT; break;
	case SCAN_CODE_a: key_id = KEYBOARD_KEY_A; break;
	case SCAN_CODE_s: key_id = KEYBOARD_KEY_S; break;
	case SCAN_CODE_d: key_id = KEYBOARD_KEY_D; break;
	case SCAN_CODE_f: key_id = KEYBOARD_KEY_F; break;
	case SCAN_CODE_g: key_id = KEYBOARD_KEY_G; break;
	case SCAN_CODE_h: key_id = KEYBOARD_KEY_H; break;
	case SCAN_CODE_j: key_id = KEYBOARD_KEY_J; break;
	case SCAN_CODE_k: key_id = KEYBOARD_KEY_K; break;
	case SCAN_CODE_l: key_id = KEYBOARD_KEY_L; break;
	case SCAN_CODE_semicolon: key_id = KEYBOARD_KEY_SEMICOLON; break;
	case SCAN_CODE_quote: key_id = KEYBOARD_KEY_QUOTE; break;
	case SCAN_CODE_tilde: key_id = KEYBOARD_KEY_TILDE; break;
	case SCAN_CODE_shift_left: key_id = KEYBOARD_KEY_SHIFT_LEFT; break;
	case SCAN_CODE_backslash: key_id = KEYBOARD_KEY_BACKSLASH; break;
	case SCAN_CODE_z: key_id = KEYBOARD_KEY_Z; break;
	case SCAN_CODE_x: key_id = KEYBOARD_KEY_X; break;
	case SCAN_CODE_c: key_id = KEYBOARD_KEY_C; break;
	case SCAN_CODE_v: key_id = KEYBOARD_KEY_V; break;
	case SCAN_CODE_b: key_id = KEYBOARD_KEY_B; break;
	case SCAN_CODE_n: key_id = KEYBOARD_KEY_N; break;
	case SCAN_CODE_m: key_id = KEYBOARD_KEY_M; break;
	case SCAN_CODE_comma: key_id = KEYBOARD_KEY_COMMA; break;
	case SCAN_CODE_preiod: key_id = KEYBOARD_KEY_PERIOD; break;
	case SCAN_CODE_slash: key_id = KEYBOARD_KEY_SLASH; break;
	case SCAN_CODE_shift_right: key_id = KEYBOARD_KEY_SHIFT_RIGHT; break;
	case SCAN_CODE_num_multiply: key_id = KEYBOARD_KEY_NUM_MULTIPLY; break;
	case SCAN_CODE_alt_left: key_id = KEYBOARD_KEY_ALT_LEFT; break;
	case SCAN_CODE_space: key_id = KEYBOARD_KEY_SPACE; break;
	case SCAN_CODE_capslock: key_id = KEYBOARD_KEY_CAPS_LOCK; break;
	case SCAN_CODE_F1: key_id = KEYBOARD_KEY_F1; break;
	case SCAN_CODE_F2: key_id = KEYBOARD_KEY_F2; break;
	case SCAN_CODE_F3: key_id = KEYBOARD_KEY_F3; break;
	case SCAN_CODE_F4: key_id = KEYBOARD_KEY_F4; break;
	case SCAN_CODE_F5: key_id = KEYBOARD_KEY_F5; break;
	case SCAN_CODE_F6: key_id = KEYBOARD_KEY_F6; break;
	case SCAN_CODE_F7: key_id = KEYBOARD_KEY_F7; break;
	case SCAN_CODE_F8: key_id = KEYBOARD_KEY_F8; break;
	case SCAN_CODE_F9: key_id = KEYBOARD_KEY_F9; break;
	case SCAN_CODE_F10: key_id = KEYBOARD_KEY_F10; break;
	case SCAN_CODE_numlock: key_id = KEYBOARD_KEY_NUM_LOCK; break;
	case SCAN_CODE_scrolllock: key_id = KEYBOARD_KEY_SCROLL_LOCK; break;
	case SCAN_CODE_num_7: key_id = KEYBOARD_KEY_7; break;
	case SCAN_CODE_num_8: key_id = KEYBOARD_KEY_8; break;
	case SCAN_CODE_num_9: key_id = KEYBOARD_KEY_9; break;
	case SCAN_CODE_num_minus: key_id = KEYBOARD_KEY_NUM_MINUS; break;
	case SCAN_CODE_num_4: key_id = KEYBOARD_KEY_NUM_4; break;
	case SCAN_CODE_num_5: key_id = KEYBOARD_KEY_NUM_5; break;
	case SCAN_CODE_num_6: key_id = KEYBOARD_KEY_NUM_6; break;
	case SCAN_CODE_num_plus: key_id = KEYBOARD_KEY_NUM_PLUS; break;
	case SCAN_CODE_num_1: key_id = KEYBOARD_KEY_NUM_1; break;
	case SCAN_CODE_num_2: key_id = KEYBOARD_KEY_NUM_2; break;
	case SCAN_CODE_num_3: key_id = KEYBOARD_KEY_NUM_3; break;
	case SCAN_CODE_num_0: key_id = KEYBOARD_KEY_NUM_0; break;
	case SCAN_CODE_num_period: key_id = KEYBOARD_KEY_NUM_PERIOD; break;
		//ALT + PrintScreen generates 0x54, mapping it to sc_printscreen
	case SCAN_CODE_alt_printscreen: key_id = KEYBOARD_KEY_PRINT_SCREEN; break;
	case SCAN_CODE_backslash_102: key_id = KEYBOARD_KEY_BACKSLASH_102; break;
	case SCAN_CODE_F11: key_id = KEYBOARD_KEY_F11; break;
	case SCAN_CODE_F12: key_id = KEYBOARD_KEY_F12; break;
	case SCAN_CODE_help: key_id = KEYBOARD_KEY_HELP; break;
	case SCAN_CODE_F13: key_id = KEYBOARD_KEY_F13; break;
	case SCAN_CODE_F14: key_id = KEYBOARD_KEY_F14; break;
	case SCAN_CODE_F15: key_id = KEYBOARD_KEY_F15; break;
	case SCAN_CODE_F16: key_id = KEYBOARD_KEY_F16; break;
	case SCAN_CODE_F17: key_id = KEYBOARD_KEY_F17; break;
	case SCAN_CODE_F18: key_id = KEYBOARD_KEY_F18; break;
	case SCAN_CODE_F19: key_id = KEYBOARD_KEY_F19; break;
	case SCAN_CODE_F20: key_id = KEYBOARD_KEY_F20; break;
	case SCAN_CODE_F21: key_id = KEYBOARD_KEY_F21; break;
	case SCAN_CODE_F22: key_id = KEYBOARD_KEY_F22; break;
	case SCAN_CODE_F23: key_id = KEYBOARD_KEY_F23; break;
	case SCAN_CODE_F24: key_id = KEYBOARD_KEY_F24; break;
	case SCAN_CODE_numpad_enter: key_id = KEYBOARD_KEY_NUM_ENTER; break;
	case SCAN_CODE_control_right: key_id = KEYBOARD_KEY_CTRL_RIGHT; break;
	case SCAN_CODE_numpad_divide: key_id = KEYBOARD_KEY_NUM_DIVIDE; break;
	case SCAN_CODE_printscreen: key_id = KEYBOARD_KEY_PRINT_SCREEN; break;
	case SCAN_CODE_alt_right: key_id = KEYBOARD_KEY_ALT_RIGHT; break;
		//CTRL + Pause generates sc_cancel mapping it to sc_pause
	case SCAN_CODE_cancel: key_id = KEYBOARD_KEY_PAUSE; break;
	case SCAN_CODE_home: key_id = KEYBOARD_KEY_HOME; break;
	case SCAN_CODE_arrow_up: key_id = KEYBOARD_KEY_UP; break;
	case SCAN_CODE_page_up: key_id = KEYBOARD_KEY_PAGE_UP; break;
	case SCAN_CODE_arrow_left: key_id = KEYBOARD_KEY_LEFT; break;
	case SCAN_CODE_arrow_right: key_id = KEYBOARD_KEY_RIGHT; break;
	case SCAN_CODE_end: key_id = KEYBOARD_KEY_END; break;
	case SCAN_CODE_arrow_down: key_id = KEYBOARD_KEY_DOWN; break;
	case SCAN_CODE_page_down: key_id = KEYBOARD_KEY_PAGE_DOWN; break;
	case SCAN_CODE_insert: key_id = KEYBOARD_KEY_INSERT; break;
	case SCAN_CODE_delete: key_id = KEYBOARD_KEY_DELETE; break;
	case SCAN_CODE_win_left: key_id = KEYBOARD_KEY_WIN_LEFT; break;
	case SCAN_CODE_win_right: key_id = KEYBOARD_KEY_WIN_RIGHT; break;
	case SCAN_CODE_menu: key_id = KEYBOARD_KEY_MENU; break;
	case SCAN_CODE_pause: key_id = KEYBOARD_KEY_PAUSE; break;
	}

	uint8_t const button_idx = key_id >> 6;
	uint64_t const button_bit = ((uint64_t)1) << (63 & key_id);

	if (is_down)
	{
		if (device->buttons[button_idx] & button_bit)
			return false;
		else
			device->buttons[button_idx] |= button_bit;
	}
	else
	{
		if (device->buttons[button_idx] & button_bit)
			device->buttons[button_idx] ^= button_bit;
		else
			return false;
	}

	return true;
}

//--------------------------------------------------
static RawKeyboardDevice* raw_keyboard_find(RawKeyboardModule* module, HANDLE device)
{
	RawKeyboardDevice* const beg = module->device_array;
	RawKeyboardDevice* const end = module->device_array + module->device_size;
	for (RawKeyboardDevice* it = beg; beg != end; ++it)
	{
		if (it->device == device && it->is_present)
			return it;
	}

	return null;
}

//--------------------------------------------------
bool_t	raw_keyboard_init(RawKeyboardModule* module, Input input, Allocator allocator)
{
	module->input = input;
	module->allocator = allocator;
	module->device_size = 0;
	module->device_capacity = DEVICE_ARRAY_CHUNK;
	module->device_array = mem_alloc(allocator, sizeof(RawKeyboardDevice) * module->device_capacity);
	if (null == module->device_array)
	{
		input_log_error(input, "raw_keyboard_init : mem_alloc");
		return false;
	}

	return true;
}

//--------------------------------------------------
void	raw_keyboard_clean(RawKeyboardModule* module)
{
	mem_free(module->allocator, module->device_array, sizeof(RawKeyboardDevice) * module->device_capacity);
}

//--------------------------------------------------
void	raw_keyboard_update(RawKeyboardModule* module)
{
	RawKeyboardDevice* beg = module->device_array;
	RawKeyboardDevice* end = module->device_array + module->device_size;
	RawKeyboardDevice* it;

	it = beg;
	while (it != end)
	{
		if (it->is_present)
		{
			++it;
		}
		else
		{
			InputEvent ev;
			ev.event_type = INPUT_EVENT_TYPE_DEVICE_REMOVED;
			ev.device_type = INPUT_DEVICE_TYPE_KEYBOARD;
			ev.device_id = (InputDeviceId)it->device;
			memset(&ev.device_state.keyboard, 0, sizeof(KeyboardState));
			ev.device_state.keyboard.buttons[0] = it->buttons[0];
			ev.device_state.keyboard.buttons[1] = it->buttons[1];

			if (input_queue_event(module->input, &ev))
			{
				--end;
				--module->device_size;
				if (end != it)
					memcpy(it, end, sizeof(RawKeyboardDevice));
			}
			else
			{
				return;
			}
		}
	}

	it = beg;
	while (it != end)
	{
		if (it->is_present)
		{
			if (it->is_signaled)
			{
				if (false == it->is_reported)
				{
					InputEvent ev;
					ev.event_type = INPUT_EVENT_TYPE_DEVICE_READING;
					ev.device_type = INPUT_DEVICE_TYPE_KEYBOARD;
					ev.device_id = (InputDeviceId)it->device;
					memset(&ev.device_state.keyboard, 0, sizeof(KeyboardState));
					ev.device_state.keyboard.buttons[0] = it->buttons[0];
					ev.device_state.keyboard.buttons[1] = it->buttons[1];

					it->is_reported = input_queue_event(module->input, &ev);
				}
			}
			else
			{
				InputEvent ev;
				ev.event_type = INPUT_EVENT_TYPE_DEVICE_ADDED;
				ev.device_type = INPUT_DEVICE_TYPE_KEYBOARD;
				ev.device_id = (InputDeviceId)it->device;
				memset(&ev.device_state.keyboard, 0, sizeof(KeyboardState));

				it->is_signaled = input_queue_event(module->input, &ev);
			}
		}

		++it;
	}
}

//--------------------------------------------------
void	raw_keyboard_input(RawKeyboardModule* module, HANDLE device, HRAWINPUT hInput)
{
	RawKeyboardDevice* raw_keyboard = raw_keyboard_find(module, device);
	if (null == raw_keyboard)
		return;

	RAWINPUT input;
	UINT size = sizeof(RAWINPUT);
	UINT res = GetRawInputData(hInput, RID_INPUT, &input, &size, sizeof(RAWINPUTHEADER));
	if (res == ((UINT)-1) || (res == 0) || input.header.dwType != RIM_TYPEKEYBOARD)
		return;

	if (raw_keyboard_map_state(raw_keyboard, &input.data.keyboard) && raw_keyboard->is_signaled)
	{
		InputEvent ev;
		ev.event_type = INPUT_EVENT_TYPE_DEVICE_READING;
		ev.device_type = INPUT_DEVICE_TYPE_KEYBOARD;
		ev.device_id = (InputDeviceId)raw_keyboard->device;
		memset(&ev.device_state.keyboard, 0, sizeof(KeyboardState));
		ev.device_state.keyboard.buttons[0] = raw_keyboard->buttons[0];
		ev.device_state.keyboard.buttons[1] = raw_keyboard->buttons[1];

		raw_keyboard->is_reported = input_queue_event(module->input, &ev);
	}
}

//--------------------------------------------------
void	raw_keyboard_added(RawKeyboardModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo)
{
	if (module->device_capacity == module->device_size)
	{
		size_t new_device_capacity = module->device_capacity + DEVICE_ARRAY_CHUNK;
		RawKeyboardDevice* new_device_array = mem_alloc(module->allocator, sizeof(RawKeyboardDevice) * new_device_capacity);
		if (null == new_device_array)
		{
			input_log_error(module->input, "raw_keyboard_added : mem_alloc");
			return;
		}

		memcpy(new_device_array, module->device_array, sizeof(RawKeyboardDevice) * module->device_capacity);
		mem_free(module->allocator, module->device_array, sizeof(RawKeyboardDevice) * module->device_capacity);
		module->device_array = new_device_array;
		module->device_capacity = new_device_capacity;
	}

	RawKeyboardDevice* raw_keyboard = module->device_array + module->device_size;
	module->device_size += 1;
	raw_keyboard->device = device;
	raw_keyboard->buttons[0] = 0;
	raw_keyboard->buttons[1] = 0;
	raw_keyboard->is_0xE11D = false;
	raw_keyboard->is_present = true;
	raw_keyboard->is_signaled = false;
	raw_keyboard->is_reported = true;

	InputEvent ev;
	ev.event_type = INPUT_EVENT_TYPE_DEVICE_ADDED;
	ev.device_type = INPUT_DEVICE_TYPE_KEYBOARD;
	ev.device_id = (InputDeviceId)raw_keyboard->device;
	memset(&ev.device_state.keyboard, 0, sizeof(KeyboardState));

	raw_keyboard->is_signaled = input_queue_event(module->input, &ev);
}

//--------------------------------------------------
void	raw_keyboard_removed(RawKeyboardModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo)
{
	RawKeyboardDevice* raw_keyboard = raw_keyboard_find(module, device);
	if (null == raw_keyboard)
		return;

	InputEvent ev;
	ev.event_type = INPUT_EVENT_TYPE_DEVICE_REMOVED;
	ev.device_type = INPUT_DEVICE_TYPE_KEYBOARD;
	ev.device_id = (InputDeviceId)raw_keyboard->device;
	memset(&ev.device_state.keyboard, 0, sizeof(KeyboardState));
	ev.device_state.keyboard.buttons[0] = raw_keyboard->buttons[0];
	ev.device_state.keyboard.buttons[1] = raw_keyboard->buttons[1];

	if (raw_keyboard->is_signaled && false == input_queue_event(module->input, &ev))
	{
		raw_keyboard->is_present = false;
		return;
	}

	--module->device_size;
	RawKeyboardDevice* const end = module->device_array + module->device_size;
	if (end != raw_keyboard)
		memcpy(raw_keyboard, end, sizeof(RawKeyboardDevice));
}

#endif