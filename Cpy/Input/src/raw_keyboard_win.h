#pragma once
#include "input_win.h"

typedef struct RawKeyboardDevice
{
	HANDLE		device;
	uint64_t	buttons[2];
	bool_t		is_0xE11D;
	bool_t		is_present;
	bool_t		is_signaled;
	bool_t		is_reported;
} RawKeyboardDevice;

typedef struct RawKeyboardModule
{
	Input				input;
	Allocator			allocator;
	RawKeyboardDevice*	device_array;
	size_t				device_capacity;
	size_t				device_size;
} RawKeyboardModule;

bool_t	raw_keyboard_init(RawKeyboardModule* module, Input input, Allocator allocator);
void	raw_keyboard_clean(RawKeyboardModule* module);
void	raw_keyboard_update(RawKeyboardModule* module);
void	raw_keyboard_input(RawKeyboardModule* module, HANDLE device, HRAWINPUT hInput);
void	raw_keyboard_added(RawKeyboardModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo);
void	raw_keyboard_removed(RawKeyboardModule* module, HANDLE device, RID_DEVICE_INFO const* deviceInfo);