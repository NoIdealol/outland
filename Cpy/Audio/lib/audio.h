#pragma once

typedef enum AudioChannel
{
	AUDIO_CHANNEL_MONO,
	AUDIO_CHANNEL_FRONT_LEFT,
	AUDIO_CHANNEL_FRONT_RIGHT,
	AUDIO_CHANNEL_FRONT_CENTER,
	AUDIO_CHANNEL_LOW_FREQUENCY,
	AUDIO_CHANNEL_REAR_LEFT,
	AUDIO_CHANNEL_REAR_RIGHT,
	AUDIO_CHANNEL_REAR_CENTER,
	AUDIO_CHANNEL_SIDE_LEFT,
	AUDIO_CHANNEL_SIDE_RIGHT,

	AUDIO_CHANNEL_COUNT,
} AudioChannel;

typedef struct AudioFormat
{
	uint32_t			frequency;
	uint32_t			channel_count;
	uint32_t			channel_mask;
} AudioFormat;

typedef enum AudioEvent
{
	AUDIO_EVENT_INIT,
	AUDIO_EVENT_START,
	AUDIO_EVENT_DATA,
	AUDIO_EVENT_STOP,
	AUDIO_EVENT_IDLE,
	AUDIO_EVENT_EXIT,
} AudioEvent;

typedef struct AudioPacket
{
	void*		buffer;
	uint32_t	frames;
	int64_t		mix_time;
} AudioPacket;

typedef struct AudioStream* AudioStream;

typedef void AudioCallback(void* ctx, AudioStream stream, AudioEvent event, AudioPacket const* packet);

bool_t	audio_render(AudioCallback* callback_fnc, void* callback_ctx); //blocking call
void	audio_stream_exit(AudioStream stream); //release loop
void	audio_stream_write(AudioStream stream, void* buffer, flt32_t const** data, uint32_t frames, uint32_t offset);
bool_t	audio_stream_map_channels(AudioStream stream, AudioChannel* ch_map, uint32_t ch_count);
void	audio_stream_format(AudioStream stream, AudioFormat* format);
