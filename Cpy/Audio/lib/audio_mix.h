#pragma once
#include "audio_include.h"

/*
	src:
		aligned to CT_AUDIO_ALIGN
	dst:
		aligned to CT_AUDIO_ALIGN
	size:
		multiple of 4
*/

CT_AUDIO_API void	ct_audio_mix2(float* dst, float const* src1, float const* src2, size_t size);
CT_AUDIO_API void	ct_audio_mix3(float* dst, float const* src1, float const* src2, float const* src3, size_t size);
CT_AUDIO_API void	ct_audio_mix4(float* dst, float const* src1, float const* src2, float const* src3, float const* src4, size_t size);
CT_AUDIO_API void	ct_audio_mix5(float* dst, float const* const* src_array, size_t size);
CT_AUDIO_API void	ct_audio_mix6(float* dst, float const* const* src_array, size_t size);
CT_AUDIO_API void	ct_audio_mix7(float* dst, float const* const* src_array, size_t size);
CT_AUDIO_API void	ct_audio_mix8(float* dst, float const* const* src_array, size_t size);
CT_AUDIO_API void	ct_audio_mixa(float* dst, float const* const* src_array, size_t src_count, size_t size);
