#pragma once
#include "audio_include.h"

/*	TABLE
	Any type memory alignment required, or CT_AUDIO_ALIGN.
	Power determines the "transform size" = 1 << power.
	Minimal power is 4 (transform size of 16).
*/

/*	TRANSFORM
	The transform uses two or three buffers of transform size.
	Buffers data and src can point to the same memory, dst must be a different buffer.
	Buffer minimal alignment is CT_AUDIO_ALIGN.
	Returns the last written buffer (dst if power is even, src otherwise).
*/

/*	CONVOLUTION
	Performs convolution of buffers x and y and stores/adds the result to z.
	Buffers cant point to the same memory.
	Buffers are of transform size.
	Buffer minimal alignment is CT_AUDIO_ALIGN.
*/

typedef struct CtAudioHartleyTable*	CtAudioHartleyTable;

CT_AUDIO_API size_t					ct_audio_hartley_table_size(uint32_t power);
CT_AUDIO_API CtAudioHartleyTable	ct_audio_hartley_table_create(uint32_t power, void* memory);
CT_AUDIO_API uint32_t				ct_audio_hartley_table_power(CtAudioHartleyTable table);
CT_AUDIO_API float*					ct_audio_hartley_transform(CtAudioHartleyTable table, float const* data, float* src, float* dst, float scale);
CT_AUDIO_API void					ct_audio_hartley_convolve(CtAudioHartleyTable table, float* z, float const* x, float const* y);
CT_AUDIO_API void					ct_audio_hartley_convolve_add(CtAudioHartleyTable table, float* z, float const* x, float const* y);
