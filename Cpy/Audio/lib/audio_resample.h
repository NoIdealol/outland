#pragma once
#include "audio_include.h"

typedef float CtAudioButterworthFilter[8];
typedef float CtAudioCubicSpline[4];

/*
	CtAudioButterworthFilter and CtAudioCubicSpline:
		aligned to CT_AUDIO_ALIGN
		zero init

	size:
		multiple of 4 for decimate4
		multiple of 4 for decimate2
		multiple of 16 for upsample4
		multiple of 8 for upsample2
		multiple of 4 for resample

	dst:
		aligned to CT_AUDIO_ALIGN
		min length of size floats

	src:
		aligned to CT_AUDIO_ALIGN
		min length of size * 4 floats for decimate4
		min length of size * 2 floats for decimate2
		min length of size / 4 floats for upsample4
		min length of size / 2 floats for upsample2
		aligned to float
		min length of size + drift + 2 floats for resample

	drift:
		within interval (-0.75 * size, 1.33 * size)

	offset:
		may be null

	return:
		consumed src size
*/

CT_AUDIO_API void	ct_audio_decimate4(CtAudioButterworthFilter filter, float* dst, float const* src, size_t size);
CT_AUDIO_API void	ct_audio_decimate2(CtAudioButterworthFilter filter, float* dst, float const* src, size_t size);
CT_AUDIO_API void	ct_audio_upsample4(CtAudioButterworthFilter filter, CtAudioCubicSpline interpolator, float* dst, float const* src, size_t size);
CT_AUDIO_API void	ct_audio_upsample2(CtAudioButterworthFilter filter, CtAudioCubicSpline interpolator, float* dst, float const* src, size_t size);
CT_AUDIO_API size_t	ct_audio_resample(CtAudioCubicSpline interpolator, float* dst, float const* src, size_t size, float drift);
CT_AUDIO_API float	ct_audio_resample_offset(CtAudioCubicSpline interpolator);

