#pragma once
#include "audio_include.h"

#define CT_AUDIO_REVERB_BLOCK_SIZE_6000		16
#define CT_AUDIO_REVERB_BLOCK_SIZE_12000	32
#define CT_AUDIO_REVERB_BLOCK_SIZE_24000	64
#define CT_AUDIO_REVERB_BLOCK_SIZE_48000	128
#define CT_AUDIO_REVERB_BLOCK_SIZE_96000	256

typedef struct CtAudioReverbContext*	CtAudioReverbContext;
typedef struct CtAudioReverbImpulse*	CtAudioReverbImpulse;
typedef struct CtAudioReverb*			CtAudioReverb;

typedef enum CtAudioReverbRate
{
	CT_AUDIO_REVERB_RATE_6000,
	CT_AUDIO_REVERB_RATE_12000,
	CT_AUDIO_REVERB_RATE_24000,
	CT_AUDIO_REVERB_RATE_48000,
	CT_AUDIO_REVERB_RATE_96000,
} CtAudioReverbRate;

typedef enum CtAudioReverbSize
{
	CT_AUDIO_REVERB_SIZE_1_SECONDS,
	CT_AUDIO_REVERB_SIZE_2_SECONDS,
	CT_AUDIO_REVERB_SIZE_4_SECONDS,
	CT_AUDIO_REVERB_SIZE_8_SECONDS,
	CT_AUDIO_REVERB_SIZE_16_SECONDS,
} CtAudioReverbSize;

typedef enum CtAudioReverbCompression
{
	CT_AUDIO_REVERB_COMPRESSION_NONE,
	CT_AUDIO_REVERB_COMPRESSION_LOW,
	CT_AUDIO_REVERB_COMPRESSION_MEDIUM,
	CT_AUDIO_REVERB_COMPRESSION_HIGH,
} CtAudioReverbCompression;

CT_AUDIO_API size_t					ct_audio_reverb_context_size(CtAudioReverbRate rate, CtAudioReverbSize size, CtAudioReverbCompression compression);
CT_AUDIO_API CtAudioReverbContext	ct_audio_reverb_context_create(CtAudioReverbRate rate, CtAudioReverbSize size, CtAudioReverbCompression compression, void* memory);
CT_AUDIO_API size_t					ct_audio_reverb_context_scratch_size(CtAudioReverbContext ctx);
CT_AUDIO_API size_t					ct_audio_reverb_impulse_size(CtAudioReverbContext ctx, uint32_t data_frames);
CT_AUDIO_API CtAudioReverbImpulse	ct_audio_reverb_impulse_create(CtAudioReverbContext ctx, flt32_t const* data_array, uint32_t data_frames, void* memory, void* scratch);
CT_AUDIO_API size_t					ct_audio_reverb_size(CtAudioReverbContext ctx, CtAudioReverbImpulse impulse);
CT_AUDIO_API CtAudioReverb			ct_audio_reverb_create(CtAudioReverbContext ctx, CtAudioReverbImpulse impulse, uint32_t perf_index, void* memory);
CT_AUDIO_API void					ct_audio_reverb_process(CtAudioReverb reverb, flt32_t const* signal, flt32_t* output, flt32_t gain, void* scratch);
