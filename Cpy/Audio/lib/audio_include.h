#pragma once
#include <stdint.h>

#ifdef __cplusplus
#define CT_EXTERN_C extern "C" 
#else
#define CT_EXTERN_C
#endif

#if defined(_MSC_VER)
//  Microsoft 
#define CT_API_EXPORT __declspec(dllexport)
#define CT_API_IMPORT __declspec(dllimport)
#elif defined(__GNUC__)
//  GCC
#define CT_API_EXPORT __attribute__((visibility("default")))
#define CT_API_IMPORT
#else
#error Unknown compiler
#endif

#if CT_AUDIO_LIB_EXPORT
#define CT_AUDIO_API CT_EXTERN_C CT_API_EXPORT
#elif CT_AUDIO_LIB_STATIC
#define CT_AUDIO_API CT_EXTERN_C
#else
#define CT_AUDIO_API CT_EXTERN_C CT_API_IMPORT
#endif

#define CT_AUDIO_ALIGN 16
