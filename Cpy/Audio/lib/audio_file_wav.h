#pragma once
#include "audio_include.h"

typedef enum CtAudioChannel
{
	CT_AUDIO_CHANNEL_FRONT_LEFT				= 0x1,
	CT_AUDIO_CHANNEL_FRONT_RIGHT			= 0x2,
	CT_AUDIO_CHANNEL_FRONT_CENTER			= 0x4,
	CT_AUDIO_CHANNEL_LOW_FREQUENCY			= 0x8,
	CT_AUDIO_CHANNEL_BACK_LEFT				= 0x10,
	CT_AUDIO_CHANNEL_BACK_RIGHT				= 0x20,
	CT_AUDIO_CHANNEL_FRONT_LEFT_OF_CENTER	= 0x40,
	CT_AUDIO_CHANNEL_FRONT_RIGHT_OF_CENTER	= 0x80,
	CT_AUDIO_CHANNEL_BACK_CENTER			= 0x100,
	CT_AUDIO_CHANNEL_SIDE_LEFT				= 0x200,
	CT_AUDIO_CHANNEL_SIDE_RIGHT				= 0x400,
	CT_AUDIO_CHANNEL_TOP_CENTER				= 0x800,
	CT_AUDIO_CHANNEL_TOP_FRONT_LEFT			= 0x1000,
	CT_AUDIO_CHANNEL_TOP_FRONT_CENTER		= 0x2000,
	CT_AUDIO_CHANNEL_TOP_FRONT_RIGHT		= 0x4000,
	CT_AUDIO_CHANNEL_TOP_BACK_LEFT			= 0x8000,
	CT_AUDIO_CHANNEL_TOP_BACK_CENTER		= 0x10000,
	CT_AUDIO_CHANNEL_TOP_BACK_RIGHT			= 0x20000,
} CtAudioChannel;

typedef enum CtAudioFormat
{
	CT_AUDIO_FORMAT_UINT8,
	CT_AUDIO_FORMAT_INT16,
	CT_AUDIO_FORMAT_INT24,
	CT_AUDIO_FORMAT_INT32,
	CT_AUDIO_FORMAT_FLOAT,
} CtAudioFormat;

typedef struct CtAudioFileWav
{
	CtAudioFormat	format;
	uint32_t		channel_count;
	uint32_t		channel_mask;
	uint32_t		sample_rate;
	uint32_t		frame_count;
} CtAudioFileWav;

#define CT_AUDIO_FILE_WAV_HEADER_SIZE 68

CT_AUDIO_API void const*	ct_audio_file_wav(CtAudioFileWav* wav, void const* file, size_t size);
CT_AUDIO_API void*			ct_audio_file_wav_header(CtAudioFileWav const* wav, void* memory);
