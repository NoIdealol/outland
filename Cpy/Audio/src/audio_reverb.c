#include "pch.h"
#include "audio_reverb.h"
#include "audio_hartley.h"
#include "audio_resample.h"
#include <immintrin.h>

#define CONV_POWER_COUNT 3
#define CONV_WORK_COUNT 16

static uint32_t convolution_power(CtAudioReverbRate rate, CtAudioReverbSize size, size_t index)
{
	/*
		layer0 and layer2 must be atleast 2 powers apart
		after layer2 compression the power must be atleast
		layer0 power before compression
	*/
	uint32_t const power_array[] = { 5, 7, 9 };
	return rate + power_array[index] + ((((uint32_t)index) * ((uint32_t)size) + 1) >> 1);
}

static uint32_t convolution_compress(CtAudioReverbCompression compression, size_t index)
{
	uint32_t const compress_array[][3] = 
	{ 
		{ 0, 0, 0 },
		{ 0, 0, 1 },
		{ 1, 1, 1 },
		{ 1, 1, 2 },
	};

	return compress_array[compression][index];
}

struct CtAudioReverbContext
{
	CtAudioHartleyTable	table_array[CONV_POWER_COUNT];
	uint32_t			compress_array[CONV_POWER_COUNT];
};

typedef struct Impulse
{
	uint32_t	blocks;
	flt32_t*	impulse;
} Impulse;

struct CtAudioReverbImpulse
{
	Impulse	impulse_array[CONV_POWER_COUNT];
};

typedef struct Reverb
{
	Impulse				impulse;
	flt32_t*			signal;
	flt32_t*			save;
	uint32_t			head;
	CtAudioHartleyTable	table;
} Reverb;

struct CtAudioReverb
{
	//keep this first to ensure alignment
	CtAudioButterworthFilter	filter_decimate_array[CONV_POWER_COUNT];
	CtAudioButterworthFilter	filter_spline_array[CONV_POWER_COUNT];
	CtAudioCubicSpline			interpolator_array[CONV_POWER_COUNT];

	flt32_t*				signal_array[CONV_POWER_COUNT - 1];
	Reverb					reverb_array[CONV_POWER_COUNT];
	flt32_t*				output_array[CONV_POWER_COUNT - 1];
	uint32_t				compress_array[CONV_POWER_COUNT];
	uint32_t				convolver_work_offset;
	uint32_t				convolver_work_array[CONV_WORK_COUNT];
	uint32_t				head;
};

static size_t convolution_context_struct(CtAudioReverbRate crate, CtAudioReverbSize csize, CtAudioReverbCompression ccompression, size_t* offsets)
{
	size_t align[CONV_POWER_COUNT + 1] = { alignof(struct CtAudioReverbContext) };
	for (size_t i = 1; i < lengthof(align); ++i)
		align[i] = CT_AUDIO_ALIGN;

	size_t size[CONV_POWER_COUNT + 1] = { sizeof(struct CtAudioReverbContext) };
	for (size_t i = 1; i < lengthof(size); ++i)
		size[i] = ct_audio_hartley_table_size(convolution_power(crate, csize, i - 1) - convolution_compress(ccompression, i - 1));

	return mem_struct(offsets, align, size, CONV_POWER_COUNT + 1);
}

size_t ct_audio_reverb_context_size(CtAudioReverbRate rate, CtAudioReverbSize size, CtAudioReverbCompression compression)
{
	size_t offsets[CONV_POWER_COUNT + 1];
	return convolution_context_struct(rate, size, compression, offsets);
}

CtAudioReverbContext	ct_audio_reverb_context_create(CtAudioReverbRate rate, CtAudioReverbSize size, CtAudioReverbCompression compression, void* memory)
{
	size_t offsets[CONV_POWER_COUNT + 1];
	convolution_context_struct(rate, size, compression, offsets);

	CtAudioReverbContext ctx = memory;
	for (size_t i = 0; i < CONV_POWER_COUNT; ++i)
	{
		ctx->table_array[i] = ct_audio_hartley_table_create(convolution_power(rate, size, i) - convolution_compress(compression, i), mem_offset(memory, offsets[i + 1]));
		ctx->compress_array[i] = convolution_compress(compression, i);
	}

	return ctx;
}

size_t ct_audio_reverb_context_scratch_size(CtAudioReverbContext ctx)
{
	return sizeof(flt32_t) << (ct_audio_hartley_table_power(ctx->table_array[CONV_POWER_COUNT - 1]));
}

static size_t impulse_size(CtAudioHartleyTable table, uint32_t compress, uint32_t frames, uint32_t* blocks)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(table);
	uint32_t const L = N >> 1;

	*blocks = ((frames >> compress) + L - 1) / L;
	return sizeof(flt32_t) * (*blocks) * N;
}

static void impulse_compress(CtAudioButterworthFilter filter, uint32_t compress, float* dst, float const* src, uint32_t frames)
{
	switch (compress)
	{
	case 1:
		ct_audio_decimate2(filter, dst, src, frames);
		return;
	case 2:
		ct_audio_decimate4(filter, dst, src, frames);
		return;
	default:
		memcpy(dst, src, sizeof(flt32_t) * frames);
		return;
	}
}

static void impulse_create(Impulse* impulse, void* memory, CtAudioHartleyTable table, uint32_t compress, flt32_t const* data, uint32_t frames, uint32_t blocks, flt32_t* work)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(table);
	uint32_t const L = N >> 1;

	impulse->blocks = blocks;
	impulse->impulse = memory;
	alignas(CT_AUDIO_ALIGN)CtAudioButterworthFilter filter;
	memset(filter, 0, sizeof(filter));
	for (uint32_t i = 0; i < impulse->blocks - 1; ++i)
	{
		memset(work, 0, sizeof(flt32_t) * N);
		impulse_compress(filter, compress, work, data + i * (L << compress), L);
		//memcpy(work, data + i * L, sizeof(flt32_t) * L);
		flt32_t* imp_block = impulse->impulse + i * N;
		flt32_t* imp_fht = ct_audio_hartley_transform(table, work, work, imp_block, 1.0f);
		if (imp_block != imp_fht)
			memcpy(imp_block, imp_fht, sizeof(flt32_t) * N);
	}

	{
		uint32_t i = (impulse->blocks - 1);
		memset(work, 0, sizeof(flt32_t) * N);
		impulse_compress(filter, compress, work, data + i * (L << compress), (frames >> compress) - i * L);
		//memcpy(work, data + i * L, sizeof(flt32_t) * (frames - i * L));
		flt32_t* imp_block = impulse->impulse + i * N;
		flt32_t* imp_fht = ct_audio_hartley_transform(table, work, work, imp_block, 1.0f);
		if (imp_block != imp_fht)
			memcpy(imp_block, imp_fht, sizeof(flt32_t) * N);
	}
}



static size_t convolution_impulse_struct(CtAudioReverbContext ctx, uint32_t data_frames, size_t* offsets, uint32_t* frames, uint32_t* blocks)
{
	frames[0] = 1 << (ct_audio_hartley_table_power(ctx->table_array[1]) + ctx->compress_array[1] - 1); //-1 to make second one immediate
	frames[1] = (1 << (ct_audio_hartley_table_power(ctx->table_array[2]) + ctx->compress_array[2])) - frames[0];  //double of the next one to give it processing time
	frames[2] = data_frames - (frames[0] + frames[1]);

	size_t align[CONV_POWER_COUNT + 1] = { alignof(struct CtAudioReverbImpulse) };
	for (size_t i = 1; i < lengthof(align); ++i)
		align[i] = CT_AUDIO_ALIGN;

	size_t size[CONV_POWER_COUNT + 1] = { sizeof(struct CtAudioReverbImpulse) };
	for (size_t i = 0; i < CONV_POWER_COUNT; ++i)
		size[i + 1] = impulse_size(ctx->table_array[i], ctx->compress_array[i], frames[i], &blocks[i]);

	return mem_struct(offsets, align, size, CONV_POWER_COUNT + 1);
}

size_t ct_audio_reverb_impulse_size(CtAudioReverbContext ctx, uint32_t data_frames)
{
	size_t offsets[CONV_POWER_COUNT + 1];
	uint32_t frames[CONV_POWER_COUNT];
	uint32_t blocks[CONV_POWER_COUNT];
	return convolution_impulse_struct(ctx, data_frames, offsets, frames, blocks);
}

CtAudioReverbImpulse ct_audio_reverb_impulse_create(CtAudioReverbContext ctx, flt32_t const* data_array, uint32_t data_frames, void* memory, void* scratch)
{
	size_t offsets[CONV_POWER_COUNT + 1];
	uint32_t frames[CONV_POWER_COUNT];
	uint32_t blocks[CONV_POWER_COUNT];
	convolution_impulse_struct(ctx, data_frames, offsets, frames, blocks);

	uint32_t frames_offset = 0;
	CtAudioReverbImpulse const impulse = memory;
	for (size_t i = 0; i < CONV_POWER_COUNT; ++i)
	{
		impulse_create(
			&impulse->impulse_array[i], 
			mem_offset(memory, offsets[i + 1]), 
			ctx->table_array[i], 
			ctx->compress_array[i],
			data_array + frames_offset, 
			frames[i],
			blocks[i],
			scratch);

		frames_offset += frames[i];
	}

	return impulse;
}

static size_t reverb_struct(Impulse const* impulse, CtAudioHartleyTable table, size_t* offsets)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(table);
	uint32_t const L = N >> 1;

	size_t align[2] = { CT_AUDIO_ALIGN, CT_AUDIO_ALIGN };
	size_t size[2];
	size[0] = sizeof(flt32_t) * impulse->blocks * N;
	size[1] = sizeof(flt32_t) * L;

	return mem_struct(offsets, align, size, 2);
}

static void reverb_create(Reverb* reverb, Impulse const* impulse, CtAudioHartleyTable table, void* memory)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(table);
	uint32_t const L = N >> 1;

	size_t offsets[2];
	reverb_struct(impulse, table, offsets);

	reverb->table = table;
	reverb->impulse = *impulse;
	reverb->signal = mem_offset(memory, offsets[0]);
	reverb->save = mem_offset(memory, offsets[1]);
	reverb->head = 0;

	//memset(reverb->signal, 0, sizeof(flt32_t) * reverb->impulse.blocks * N);
	//memset(reverb->save, 0, sizeof(flt32_t) * L);
}

static void reverb_process(Reverb* reverb, flt32_t const* signal, flt32_t* output, flt32_t gain, flt32_t* work)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(reverb->table);
	uint32_t const L = N >> 1;

	flt32_t* sig_fht = reverb->signal + reverb->head * N;
	memcpy(sig_fht, reverb->save, sizeof(flt32_t) * L);
	memcpy(sig_fht + L, signal, sizeof(flt32_t) * L);
	memcpy(reverb->save, sig_fht + L, sizeof(flt32_t) * L);

	flt32_t* tmp_fht = ct_audio_hartley_transform(reverb->table, sig_fht, sig_fht, work, 1.0f);
	if (tmp_fht != sig_fht)
		memcpy(sig_fht, tmp_fht, sizeof(flt32_t) * N);


	flt32_t* imp_fht = reverb->impulse.impulse;
	ct_audio_hartley_convolve(reverb->table, work, reverb->signal, imp_fht + reverb->head * N);
	for (uint32_t i = reverb->head; 0 < i; --i)
	{
		ct_audio_hartley_convolve_add(reverb->table, work, reverb->signal + i * N, imp_fht);
		imp_fht += N;
	}

	for (uint32_t i = reverb->impulse.blocks - 1; reverb->head < i; --i)
	{
		imp_fht += N; //this is first to skip the first convolution
		ct_audio_hartley_convolve_add(reverb->table, work, reverb->signal + i * N, imp_fht);
	}

	if (reverb->impulse.blocks == ++reverb->head)
		reverb->head = 0;

	tmp_fht = reverb->signal + reverb->head * N; //use memory from next signal block
	flt32_t* res_fht = ct_audio_hartley_transform(reverb->table, work, work, tmp_fht, gain * (0.5f / N));
	memcpy(output, res_fht + L, sizeof(flt32_t) * L);
}

void reverb_process_signal(Reverb* reverb, flt32_t const* signal, flt32_t* work)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(reverb->table);
	uint32_t const L = N >> 1;

	flt32_t* sig_fht = reverb->signal + reverb->head * N;
	memcpy(sig_fht, reverb->save, sizeof(flt32_t) * L);
	memcpy(sig_fht + L, signal, sizeof(flt32_t) * L);
	memcpy(reverb->save, sig_fht + L, sizeof(flt32_t) * L);

	flt32_t* tmp_fht = ct_audio_hartley_transform(reverb->table, sig_fht, sig_fht, work, 1.0f);
	if (tmp_fht != sig_fht)
		memcpy(sig_fht, tmp_fht, sizeof(flt32_t) * N);

	if (reverb->impulse.blocks == ++reverb->head)
		reverb->head = 0;
}

void reverb_process_output(Reverb* reverb, flt32_t* output, flt32_t* work, flt32_t gain)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(reverb->table);
	uint32_t const L = N >> 1;

	flt32_t* res_fht = ct_audio_hartley_transform(reverb->table, reverb->signal + reverb->head * N, reverb->signal + reverb->head * N, work, gain * (0.5f / N));
	memcpy(output, res_fht + L, sizeof(flt32_t) * L);
}

void reverb_process_convolve_first(Reverb* reverb, flt32_t* work)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(reverb->table);
	uint32_t const L = N >> 1;

	ct_audio_hartley_convolve(
		reverb->table, 
		work, 
		reverb->signal + reverb->head * N, 
		reverb->impulse.impulse + (reverb->impulse.blocks - 1) * N);

	memcpy(reverb->signal + reverb->head * N, work, sizeof(flt32_t) * N);
}

void reverb_process_convolve_next(Reverb* reverb, uint32_t offset, uint32_t count)
{
	uint32_t const N = 1 << ct_audio_hartley_table_power(reverb->table);
	uint32_t const L = N >> 1;

	flt32_t* const fht_sum = reverb->signal + reverb->head * N;
	flt32_t* fht_imp = reverb->impulse.impulse + (reverb->impulse.blocks - offset) * N;

	uint32_t i;
	for (i = reverb->head + offset; i < reverb->impulse.blocks && count; ++i)
	{
		--count;
		fht_imp -= N;
		ct_audio_hartley_convolve_add(
			reverb->table,
			fht_sum,
			reverb->signal + i * N,
			fht_imp);
	}

	if (i >= reverb->impulse.blocks)
	{
		for (i = i - reverb->impulse.blocks; i < reverb->head && count; ++i)
		{
			--count;
			fht_imp -= N;
			ct_audio_hartley_convolve_add(
				reverb->table,
				fht_sum,
				reverb->signal + i * N,
				fht_imp);
		}
	}
}

#define CONV_REVERB_STRUCT_COUNT 1 + (CONV_POWER_COUNT - 1) + CONV_POWER_COUNT + (CONV_POWER_COUNT - 1)

static size_t convolution_reverb_struct(CtAudioReverbContext ctx, CtAudioReverbImpulse impulse, size_t* offsets, size_t* output_sizes)
{
	size_t align[CONV_REVERB_STRUCT_COUNT] = { alignof(struct CtAudioReverb ) };
	for (uint32_t i = 1; i < lengthof(align); ++i)
		align[i] = CT_AUDIO_ALIGN;

	size_t size[CONV_REVERB_STRUCT_COUNT] = { 0 };
	size[0] = sizeof(struct CtAudioReverb);
	for (uint32_t i = 0; i < CONV_POWER_COUNT - 1; ++i)
	{
		size[i + 1] = sizeof(flt32_t) * ((size_t)1 << (ct_audio_hartley_table_power(ctx->table_array[i + 1]) - 1));
	}
	
	for (uint32_t i = 0; i < CONV_POWER_COUNT; ++i)
	{
		size_t reverb_offsets[2];
		size[i + 3] = reverb_struct(&impulse->impulse_array[i], ctx->table_array[i], reverb_offsets);
	}

	for (uint32_t i = 0; i < CONV_POWER_COUNT - 1; ++i)
	{
		output_sizes[i] = sizeof(flt32_t) * ((size_t)1 << (ct_audio_hartley_table_power(ctx->table_array[i + 1]) - 1));
		size[i + 6] = output_sizes[i];
	}

	return mem_struct(offsets, align, size, CONV_REVERB_STRUCT_COUNT);
}

size_t ct_audio_reverb_size(CtAudioReverbContext ctx, CtAudioReverbImpulse impulse)
{
	size_t offsets[CONV_REVERB_STRUCT_COUNT];
	size_t output_sizes[CONV_POWER_COUNT - 1];
	return convolution_reverb_struct(ctx, impulse, offsets, output_sizes);
}

static uint32_t bit_reverse(uint32_t value, uint32_t power)
{
	uint32_t out = 0;
	uint32_t const max_bit = 1 << (power - 1);
	for (uint32_t i = 0; i < power; ++i)
	{
		if ((1 << i) & value)
			out |= max_bit >> i;
	}

	return out;
}

CtAudioReverb ct_audio_reverb_create(CtAudioReverbContext ctx, CtAudioReverbImpulse impulse, uint32_t perf_index, void* memory)
{
	CtAudioReverb reverb = memory;
	
	size_t offsets[CONV_REVERB_STRUCT_COUNT];
	size_t output_sizes[CONV_POWER_COUNT - 1]; //todo: output sizes are useless, we memset 0 
	memset(reverb, 0, convolution_reverb_struct(ctx, impulse, offsets, output_sizes));

	uint32_t const head_power =
		(ct_audio_hartley_table_power(ctx->table_array[2]) + ctx->compress_array[2]) -
		(ct_audio_hartley_table_power(ctx->table_array[0]) + ctx->compress_array[0]);

	reverb->head = bit_reverse(perf_index & ((1 << head_power) - 1), head_power);
	for (uint32_t i = 0; i < CONV_POWER_COUNT - 1; ++i)
	{
		reverb->signal_array[i] = mem_offset(memory, offsets[i + 1]);
		//memset(reverb->signal_array[i], 0, output_sizes[i]); //output and signal sizes have the same size
	}
	
	for (uint32_t i = 0; i < CONV_POWER_COUNT; ++i)
	{
		reverb->compress_array[i] = ctx->compress_array[i];

		reverb_create(
			&reverb->reverb_array[i], 
			&impulse->impulse_array[i], 
			ctx->table_array[i], 
			mem_offset(memory, offsets[i + 3]));
	}
	
	for (uint32_t i = 0; i < CONV_POWER_COUNT - 1; ++i)
	{
		reverb->output_array[i] = mem_offset(memory, offsets[i + 6]);
		//memset(reverb->output_array[i], 0, output_sizes[i]);
	}

	for (uint32_t i = 0; i < CONV_WORK_COUNT; ++i)
	{
		uint32_t work_blocks = reverb->reverb_array[2].impulse.blocks / CONV_WORK_COUNT;
		uint32_t work_remain_blocks = reverb->reverb_array[2].impulse.blocks % CONV_WORK_COUNT;
		if (i < work_remain_blocks)
			++work_blocks;

		reverb->convolver_work_array[i] = work_blocks;	
	}
	--reverb->convolver_work_array[0];

	return reverb;
}


void ct_audio_reverb_process(CtAudioReverb reverb, flt32_t const* signal, flt32_t* output, flt32_t gain, void* scratch)
{
	//size in floats
	uint32_t const block_size = 1 << (ct_audio_hartley_table_power(reverb->reverb_array[0].table) + reverb->compress_array[0] - 1);
	uint32_t const scratch_size = 1 << (ct_audio_hartley_table_power(reverb->reverb_array[2].table));
	uint32_t const head_mask1 = (1 << (
		+ct_audio_hartley_table_power(reverb->reverb_array[1].table) + reverb->compress_array[1]
		- ct_audio_hartley_table_power(reverb->reverb_array[0].table) - reverb->compress_array[0]
		)) - 1;
	uint32_t const head_mask2 = (1 << (
		+ct_audio_hartley_table_power(reverb->reverb_array[2].table) + reverb->compress_array[2]
		- ct_audio_hartley_table_power(reverb->reverb_array[0].table) - reverb->compress_array[0]
		)) - 1;

	//layer0: do processing with compression
	if (reverb->compress_array[0])
	{
		float* const signal_compress = mem_offset(scratch, sizeof(float) * block_size); //block_size is scratch for reverb 0 with compression 1
		float* const signal_output = mem_offset(scratch, sizeof(float) * (block_size + (block_size >> 1)));

		ct_audio_decimate2(reverb->filter_decimate_array[0], signal_compress, signal, block_size >> 1);
		reverb_process(&reverb->reverb_array[0], signal_compress, signal_output, gain * (1 << reverb->compress_array[0]), scratch);
		ct_audio_upsample2(reverb->filter_spline_array[0], reverb->interpolator_array[0], output, signal_output, block_size);
	}
	else
	{
		reverb_process(&reverb->reverb_array[0], signal, output, gain, scratch);
	}

	//layer1 & layer2: decompress and mix with output from layer0
	{
		//layer1: copy/decompress signal block
		float* output1;
		if (reverb->compress_array[1])
		{
			output1 = scratch;
			ct_audio_upsample2(
				reverb->filter_spline_array[1],
				reverb->interpolator_array[1],
				output1,
				reverb->output_array[0] + (reverb->head & head_mask1) * (block_size >> reverb->compress_array[1]),
				block_size
			);
		}
		else
		{
			output1 = reverb->output_array[0] + (reverb->head & head_mask1) * (block_size);
		}

		//layer2: copy/decompress signal block
		float* output2;
		if (1 == reverb->compress_array[2])
		{
			output2 = mem_offset(scratch, sizeof(float) * block_size);
			ct_audio_upsample2(
				reverb->filter_spline_array[2],
				reverb->interpolator_array[2],
				output2,
				reverb->output_array[1] + (reverb->head & head_mask2) * (block_size >> reverb->compress_array[2]),
				block_size
			);
		}
		else if (2 == reverb->compress_array[2])
		{
			output2 = mem_offset(scratch, sizeof(float) * block_size);
			ct_audio_upsample4(
				reverb->filter_spline_array[2],
				reverb->interpolator_array[2],
				output2,
				reverb->output_array[1] + (reverb->head & head_mask2) * (block_size >> reverb->compress_array[2]),
				block_size
			);
		}
		else
		{
			output2 = reverb->output_array[1] + (reverb->head & head_mask2) * (block_size);
		}

		//do the mix
		for (uint32_t i = 0; i < block_size; i += 4)
		{
			_mm_store_ps(output + i, _mm_add_ps(_mm_add_ps(
				_mm_load_ps(output + i),
				_mm_load_ps(output1 + i)),
				_mm_load_ps(output2 + i)));
		}
	}

	//layer1: copy/compress new signal block
	if (reverb->compress_array[1])
	{
		ct_audio_decimate2(
			reverb->filter_decimate_array[1], 
			reverb->signal_array[0] + (reverb->head & head_mask1) * (block_size >> reverb->compress_array[1]),
			signal, 
			(block_size >> reverb->compress_array[1]));
	}
	else
	{
		memcpy(reverb->signal_array[0] + (reverb->head & head_mask1) * block_size, signal, sizeof(flt32_t) * block_size);
	}

	//layer1: proces signal before output is needed
	if (head_mask1 == (reverb->head & head_mask1))
	{
		reverb_process(&reverb->reverb_array[1],
			reverb->signal_array[0],
			reverb->output_array[0],
			gain * (1 << reverb->compress_array[1]),
			scratch);
	}

	//layer2: proces signal before overwriting it
	uint32_t const head2 = reverb->head & head_mask2;
	if (0 == head2)
	{
		reverb_process_signal(&reverb->reverb_array[2], reverb->signal_array[1], scratch);		
	}
	else if (1 == head2)
	{
		reverb_process_convolve_first(&reverb->reverb_array[2], scratch);
		reverb->convolver_work_offset = 1;
		reverb_process_convolve_next(&reverb->reverb_array[2], reverb->convolver_work_offset, reverb->convolver_work_array[head2 - 1]);
		reverb->convolver_work_offset += reverb->convolver_work_array[head2 - 1];
	}
	else if (CONV_WORK_COUNT > (head2 - 1))
	{
		reverb_process_convolve_next(&reverb->reverb_array[2], reverb->convolver_work_offset, reverb->convolver_work_array[head2 - 1]);
		reverb->convolver_work_offset += reverb->convolver_work_array[head2 - 1];
	}

	//layer2: output
	if (head_mask2 == head2)
	{
		reverb_process_output(&reverb->reverb_array[2], reverb->output_array[1], scratch, gain * (1 << reverb->compress_array[2]));
	}

	//layer2: copy/compress new signal block
	if (1 == reverb->compress_array[2])
	{
		ct_audio_decimate2(
			reverb->filter_decimate_array[2],
			reverb->signal_array[1] + head2 * (block_size >> reverb->compress_array[2]),
			signal,
			(block_size >> reverb->compress_array[2]));
	}
	else if (2 == reverb->compress_array[2])
	{
		ct_audio_decimate4(
			reverb->filter_decimate_array[2],
			reverb->signal_array[1] + head2 * (block_size >> reverb->compress_array[2]),
			signal,
			(block_size >> reverb->compress_array[2]));
	}
	else
	{
		memcpy(reverb->signal_array[1] + head2 * block_size, signal, sizeof(flt32_t) * block_size);
	}

	

	//inc head
	++reverb->head;
}


