#include "pch.h"
#include "audio_resample.h"
#include <immintrin.h>

/*	
	for (int i = 0; i < size; ++i)
	{
		ctx->xv[0] = ctx->xv[1]; ctx->xv[1] = ctx->xv[2]; ctx->xv[2] = ctx->xv[3];
		ctx->xv[3] = buff[i] / GAIN;
		ctx->yv[0] = ctx->yv[1]; ctx->yv[1] = ctx->yv[2]; ctx->yv[2] = ctx->yv[3];
		ctx->yv[3] = (ctx->xv[0] + ctx->xv[3]) + 3 * (ctx->xv[1] + ctx->xv[2])
			+ (0.1978251873 * ctx->yv[0]) + (-0.9103690003 * ctx->yv[1])
			+ (1.4590290622 * ctx->yv[2]);
		buff[i] = ctx->yv[3];
	}
	return;

	dot		X0			X1			X2			X3			X4			X5			X6			Y0			Y1			Y2			(Y)3	(Y4)	(Y5)
	Y3=		a0			a1			a2			a3			0			0			0			b0			b1			b2

	Y4=		0			a0			a1			a2			a3			0			0			0			b0			b1			b2
			b2a0		b2a1		b2a2		b2a3		0			0			0			b2b0		b2b1		b2b2

	Y5=		0			0			a0			a1			a2			a3			0			0			0			b0			b1		b2
			b1a0		b1a1		b1a2		b1a3		0			0			0			b1b0		b1b1		b1b2
			0			b2a0		b2a1		b2a2		b2a3		0			0			0			b2b0		b2b1
			b2b2a0		b2b2a1		b2b2a2		b2b2a3		0			0			0			b2b2b0		b2b2b1		b2b2b2

	Y6=		0			0			0			a0			a1			a2			a3			0			0			0			b0		b1		b2
			b0a0		b0a1		b0a2		b0a3		0			0			0			b0b0		b0b1		b0b2
			0			b1a0		b1a1		b1a2		b1a3		0			0			0			b1b0		b1b1
			b1b2a0		b1b2a1		b1b2a2		b1b2a3		0			0			0			b1b2b0		b1b2b1		b1b2b2
			0			0			b2a0		b2a1		b2a2		b2a3		0			0			0			b2b0
			b2b1a0		b2b1a1		b2b1a2		b2b1a3		0			0			0			b2b1b0		b2b1b1		b2b1b2
			0			b2b2a0		b2b2a1		b2b2a2		b2b2a3		0			0			0			b2b2b0		b2b2b1
			b2b2b2a0	b2b2b2a1	b2b2b2a2	b2b2b2a3	0			0			0			b2b2b2b0	b2b2b2b1	b2b2b2b2
	*/


/*
void butterworth_coef(double a0, double a1, double a2, double a3, double b0, double b1, double b2, double g)
{
	enum
	{
		NPOLES = 3,
		NZEROS = 3,
		NSIZE = 16,
		NDATA = NPOLES + NSIZE + NZEROS + NSIZE - 1,
	};

	double data[NSIZE][NDATA] = { 0 };
	for (int s = 0; s < NSIZE; ++s)
	{
		//init basic table
		data[s][s + 0] = a0;
		data[s][s + 1] = a1;
		data[s][s + 2] = a2;
		data[s][s + 3] = a3;
		data[s][s + NPOLES + NSIZE + 0] = b0;
		data[s][s + NPOLES + NSIZE + 1] = b1;
		data[s][s + NPOLES + NSIZE + 2] = b2;

		for (int y = 0; y < s; ++y)
		{
			double const coef = data[s][y + NPOLES + NSIZE + NZEROS];
			for (int i = 0; i < NDATA; ++i)
			{
				data[s][i] += data[y][i] * coef;
			}
		}
	}
	for (int s = 0; s < NSIZE; ++s)
		for (int i = 0; i < NDATA; ++i)
			data[s][i] /= g;

	char buffer[1024] = { 0 };
	int vector_sizes[] = { NPOLES + 4, NPOLES + 8, NPOLES + 16 };
	int jump_offsets[3][4] = { { 0,1,2,3 },{ 1, 3, 5, 7 },{ 3, 7, 11, 15 } };

	for (int j = 0; j < 3; ++j)
	{
		for (int i = 0; i < vector_sizes[j]; ++i)
		{
			snprintf(buffer, sizeof(buffer), "_m128 const X%i = _mm_set_ps(%e, %e, %e, %e) \n",
				i,
				data[jump_offsets[j][3]][i],
				data[jump_offsets[j][2]][i],
				data[jump_offsets[j][1]][i],
				data[jump_offsets[j][0]][i]);

			OutputDebugStringA(buffer);
		}

		for (int i = 0; i < 3; ++i)
		{
			snprintf(buffer, sizeof(buffer), "_m128 const Y%i = _mm_set_ps(%e, %e, %e, %e) \n",
				i,
				data[jump_offsets[j][3]][NPOLES + NSIZE + i],
				data[jump_offsets[j][2]][NPOLES + NSIZE + i],
				data[jump_offsets[j][1]][NPOLES + NSIZE + i],
				data[jump_offsets[j][0]][NPOLES + NSIZE + i]);

			OutputDebugStringA(buffer);
		}
	}
}*/

#define BUTTERWORTH3_X0(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)((b0 * a0 +								   \
	b1 * b2 * a0 +									   \
	b2 * b1 * a0 +									   \
	b2 * b2 * b2 * a0) / g),						   \
	(float)((b1 * a0 +								   \
	b2 * b2 * a0) / g),								   \
	(float)((b2 * a0) / g),							   \
	(float)((a0) / g)								   \
)

#define BUTTERWORTH3_X1(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)((b0 * a1 +								   \
	b1 * a0 +										   \
	b1 * b2 * a1 +									   \
	b2 * b1 * a1 +									   \
	b2 * b2 * a0 +									   \
	b2 * b2 * b2 * a1) / g),						   \
	(float)((b1 * a1 +								   \
	b2 * a0 +										   \
	b2 * b2 * a1) / g),								   \
	(float)((a0 +									   \
	b2 * a1) / g),									   \
	(float)((a1) / g)								   \
)
	
#define BUTTERWORTH3_X2(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)((b0 * a2 +								   \
	b1 * a1 +										   \
	b1 * b2 * a2 +									   \
	b2 * a0 +										   \
	b2 * b1 * a2 +									   \
	b2 * b2 * a1 +									   \
	b2 * b2 * b2 * a2) / g),						   \
	(float)((a0 +									   \
	b1 * a2 +										   \
	b2 * a1 +										   \
	b2 * b2 * a2) / g),								   \
	(float)((a1 +									   \
	b2 * a2) / g),									   \
	(float)((a2) / g)								   \
)

#define BUTTERWORTH3_X3(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)((a0 +									   \
	b0 * a3 +										   \
	b1 * a2 +										   \
	b1 * b2 * a3 +									   \
	b2 * a1 +										   \
	b2 * b1 * a3 +									   \
	b2 * b2 * a2 +									   \
	b2 * b2 * b2 * a3) / g),						   \
	(float)((a1 +									   \
	b1 * a3 +										   \
	b2 * a2 +										   \
	b2 * b2 * a3) / g),								   \
	(float)((a2 +									   \
	b2 * a3) / g),									   \
	(float)((a3) / g)								   \
)

#define BUTTERWORTH3_X4(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)((a1 +									   \
	b1 * a3 +										   \
	b2 * a2 +										   \
	b2 * b2 * a3) / g),								   \
	(float)((a2 +									   \
	b2 * a3) / g),									   \
	(float)((a3) / g),								   \
	(float)((0) / g)								   \
)

#define BUTTERWORTH3_X5(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)((a2 +									   \
	b2 * a3) / g),									   \
	(float)((a3) / g),								   \
	(float)((0) / g),								   \
	(float)((0) / g)								   \
)

#define BUTTERWORTH3_X6(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)((a3) / g),								   \
	(float)((0) / g),								   \
	(float)((0) / g),								   \
	(float)((0) / g)								   \
)

#define BUTTERWORTH3_Y0(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)(b0 * b0 +								   \
	b1 * b2 * b0 +									   \
	b2 * b1 * b0 +									   \
	b2 * b2 * b2 * b0),								   \
	(float)(b1 * b0 +								   \
	b2 * b2 * b0),									   \
	(float)(b2 * b0),								   \
	(float)(b0)										   \
);

#define BUTTERWORTH3_Y1(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)(b0 * b1 +								   \
	b1 * b0 +										   \
	b1 * b2 * b1 +									   \
	b2 * b1 * b1 +									   \
	b2 * b2 * b0 +									   \
	b2 * b2 * b2 * b1),								   \
	(float)(b1 * b1 +								   \
	b2 * b0 +										   \
	b2 * b2 * b1),									   \
	(float)(b0 +									   \
	b2 * b1),										   \
	(float)(b1)										   \
)

#define BUTTERWORTH3_Y2(a0, a1, a2, a3, b0, b1, b2, g) \
_mm_set_ps(											   \
	(float)(b0 * b2 +								   \
	b1 * b1 +										   \
	b1 * b2 * b2 +									   \
	b2 * b0 +										   \
	b2 * b1 * b2 +									   \
	b2 * b2 * b1 +									   \
	b2 * b2 * b2 * b2),								   \
	(float)(b0 +									   \
	b1 * b2 +										   \
	b2 * b1 +										   \
	b2 * b2 * b2),									   \
	(float)(b1 +									   \
	b2 * b2),										   \
	(float)(b2)										   \
)

#define BW3_QUARTER_A0 1
#define BW3_QUARTER_A1 3
#define BW3_QUARTER_A2 3
#define BW3_QUARTER_A3 1
#define BW3_QUARTER_B0 0.1978251873
#define BW3_QUARTER_B1 -0.9103690003
#define BW3_QUARTER_B2 1.4590290622
#define BW3_QUARTER_G 3.155634919e+01

#define BW3_HALF_A0 1
#define BW3_HALF_A1 3
#define BW3_HALF_A2 3
#define BW3_HALF_A3 1
#define BW3_HALF_B0 0
#define BW3_HALF_B1 -0.3333333333333333
#define BW3_HALF_B2 0
#define BW3_HALF_G 6.000000000e+00

#define BW3_QUARTER_BAND_X0 BUTTERWORTH3_X0(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_X1 BUTTERWORTH3_X1(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_X2 BUTTERWORTH3_X2(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_X3 BUTTERWORTH3_X3(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_X4 BUTTERWORTH3_X4(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_X5 BUTTERWORTH3_X5(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_X6 BUTTERWORTH3_X6(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_Y0 BUTTERWORTH3_Y0(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_Y1 BUTTERWORTH3_Y1(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)
#define BW3_QUARTER_BAND_Y2 BUTTERWORTH3_Y2(BW3_QUARTER_A0, BW3_QUARTER_A1, BW3_QUARTER_A2, BW3_QUARTER_A3, BW3_QUARTER_B0, BW3_QUARTER_B1, BW3_QUARTER_B2, BW3_QUARTER_G)

#define BW3_HALF_BAND_X0 BUTTERWORTH3_X0(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_X1 BUTTERWORTH3_X1(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_X2 BUTTERWORTH3_X2(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_X3 BUTTERWORTH3_X3(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_X4 BUTTERWORTH3_X4(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_X5 BUTTERWORTH3_X5(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_X6 BUTTERWORTH3_X6(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_Y0 BUTTERWORTH3_Y0(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_Y1 BUTTERWORTH3_Y1(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)
#define BW3_HALF_BAND_Y2 BUTTERWORTH3_Y2(BW3_HALF_A0, BW3_HALF_A1, BW3_HALF_A2, BW3_HALF_A3, BW3_HALF_B0, BW3_HALF_B1, BW3_HALF_B2, BW3_HALF_G)


void ct_audio_decimate4(CtAudioButterworthFilter filter, float* dst, float const* src, size_t size)
{
	__m128 const X0 = BW3_QUARTER_BAND_X0;
	__m128 const X1 = BW3_QUARTER_BAND_X1;
	__m128 const X2 = BW3_QUARTER_BAND_X2;
	__m128 const X3 = BW3_QUARTER_BAND_X3;
	__m128 const X4 = BW3_QUARTER_BAND_X4;
	__m128 const X5 = BW3_QUARTER_BAND_X5;
	__m128 const X6 = BW3_QUARTER_BAND_X6;
	__m128 const Y0 = BW3_QUARTER_BAND_Y0;
	__m128 const Y1 = BW3_QUARTER_BAND_Y1;
	__m128 const Y2 = BW3_QUARTER_BAND_Y2;

	__m128 x0;
	__m128 x1;
	__m128 x2;
	__m128 x3;
	__m128 x4 = _mm_load1_ps(filter + 0);
	__m128 x5 = _mm_load1_ps(filter + 1);
	__m128 x6 = _mm_load1_ps(filter + 2);
	__m128 y0;
	__m128 y1;
	__m128 y2;
	__m128 y3 = _mm_load_ps(filter + 4);

	__m128 o01;
	__m128 o23;

	size <<= 2;
	size -= size & 15;
	for (size_t i = 0; i < size; i += 16)
	{
		x0 = x4;
		x1 = x5;
		x2 = x6;
		x3 = _mm_load_ps(src + i);
		x4 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(1, 1, 1, 1));
		x5 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(2, 2, 2, 2));
		x6 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(3, 3, 3, 3));
		x3 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(0, 0, 0, 0));

		y0 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(1, 1, 1, 1));
		y1 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(2, 2, 2, 2));
		y2 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(3, 3, 3, 3));
		y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_mul_ps(X0, x0),
			_mm_mul_ps(X1, x1)),
			_mm_mul_ps(X2, x2)),
			_mm_mul_ps(X3, x3)),
			_mm_mul_ps(X4, x4)),
			_mm_mul_ps(X5, x5)),
			_mm_mul_ps(X6, x6)),
			_mm_mul_ps(Y0, y0)),
			_mm_mul_ps(Y1, y1)),
			_mm_mul_ps(Y2, y2));

		o01 = y3;

		x0 = x4;
		x1 = x5;
		x2 = x6;
		x3 = _mm_load_ps(src + i + 4);
		x4 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(1, 1, 1, 1));
		x5 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(2, 2, 2, 2));
		x6 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(3, 3, 3, 3));
		x3 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(0, 0, 0, 0));

		y0 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(1, 1, 1, 1));
		y1 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(2, 2, 2, 2));
		y2 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(3, 3, 3, 3));
		y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_mul_ps(X0, x0),
			_mm_mul_ps(X1, x1)),
			_mm_mul_ps(X2, x2)),
			_mm_mul_ps(X3, x3)),
			_mm_mul_ps(X4, x4)),
			_mm_mul_ps(X5, x5)),
			_mm_mul_ps(X6, x6)),
			_mm_mul_ps(Y0, y0)),
			_mm_mul_ps(Y1, y1)),
			_mm_mul_ps(Y2, y2));

		o01 = _mm_shuffle_ps(o01, y3, _MM_SHUFFLE(3, 3, 3, 3));

		x0 = x4;
		x1 = x5;
		x2 = x6;
		x3 = _mm_load_ps(src + i + 8);
		x4 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(1, 1, 1, 1));
		x5 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(2, 2, 2, 2));
		x6 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(3, 3, 3, 3));
		x3 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(0, 0, 0, 0));

		y0 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(1, 1, 1, 1));
		y1 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(2, 2, 2, 2));
		y2 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(3, 3, 3, 3));
		y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_mul_ps(X0, x0),
			_mm_mul_ps(X1, x1)),
			_mm_mul_ps(X2, x2)),
			_mm_mul_ps(X3, x3)),
			_mm_mul_ps(X4, x4)),
			_mm_mul_ps(X5, x5)),
			_mm_mul_ps(X6, x6)),
			_mm_mul_ps(Y0, y0)),
			_mm_mul_ps(Y1, y1)),
			_mm_mul_ps(Y2, y2));

		o23 = y3;

		x0 = x4;
		x1 = x5;
		x2 = x6;
		x3 = _mm_load_ps(src + i + 12);
		x4 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(1, 1, 1, 1));
		x5 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(2, 2, 2, 2));
		x6 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(3, 3, 3, 3));
		x3 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(0, 0, 0, 0));

		y0 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(1, 1, 1, 1));
		y1 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(2, 2, 2, 2));
		y2 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(3, 3, 3, 3));
		y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_mul_ps(X0, x0),
			_mm_mul_ps(X1, x1)),
			_mm_mul_ps(X2, x2)),
			_mm_mul_ps(X3, x3)),
			_mm_mul_ps(X4, x4)),
			_mm_mul_ps(X5, x5)),
			_mm_mul_ps(X6, x6)),
			_mm_mul_ps(Y0, y0)),
			_mm_mul_ps(Y1, y1)),
			_mm_mul_ps(Y2, y2));

		o23 = _mm_shuffle_ps(o23, y3, _MM_SHUFFLE(3, 3, 3, 3));

		_mm_store_ps(dst + (i >> 2), _mm_shuffle_ps(o01, o23, _MM_SHUFFLE(2, 0, 2, 0)));
	}

	_mm_store_ps(filter + 4, y3);
	_mm_store_ss(filter + 0, x4);
	_mm_store_ss(filter + 1, x5);
	_mm_store_ss(filter + 2, x6);
}


void ct_audio_decimate2(CtAudioButterworthFilter filter, float* dst, float const* src, size_t size)
{
	__m128 const X0 = BW3_HALF_BAND_X0;
	__m128 const X1 = BW3_HALF_BAND_X1;
	__m128 const X2 = BW3_HALF_BAND_X2;
	__m128 const X3 = BW3_HALF_BAND_X3;
	__m128 const X4 = BW3_HALF_BAND_X4;
	__m128 const X5 = BW3_HALF_BAND_X5;
	__m128 const X6 = BW3_HALF_BAND_X6;
	//__m128 const Y0 = BW3_HALF_BAND_Y0; all zeros
	__m128 const Y1 = BW3_HALF_BAND_Y1;
	__m128 const Y2 = BW3_HALF_BAND_Y2;

	__m128 x0;
	__m128 x1;
	__m128 x2;
	__m128 x3;
	__m128 x4 = _mm_load1_ps(filter + 0);
	__m128 x5 = _mm_load1_ps(filter + 1);
	__m128 x6 = _mm_load1_ps(filter + 2);
	//__m128 y0;
	__m128 y1;
	__m128 y2;
	__m128 y3 = _mm_load_ps(filter + 4);

	__m128 O0;

	size <<= 1;
	size -= size & 7;
	for (size_t i = 0; i < size; i += 8)
	{
		x0 = x4;
		x1 = x5;
		x2 = x6;
		x3 = _mm_load_ps(src + i);
		x4 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(1, 1, 1, 1));
		x5 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(2, 2, 2, 2));
		x6 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(3, 3, 3, 3));
		x3 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(0, 0, 0, 0));

		//y0 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(1, 1, 1, 1));
		y1 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(2, 2, 2, 2));
		y2 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(3, 3, 3, 3));
		y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(//_mm_add_ps(
			_mm_mul_ps(X0, x0),
			_mm_mul_ps(X1, x1)),
			_mm_mul_ps(X2, x2)),
			_mm_mul_ps(X3, x3)),
			_mm_mul_ps(X4, x4)),
			_mm_mul_ps(X5, x5)),
			_mm_mul_ps(X6, x6)),
			//_mm_mul_ps(Y0, y0)),
			_mm_mul_ps(Y1, y1)),
			_mm_mul_ps(Y2, y2));

		O0 = y3;

		x0 = x4;
		x1 = x5;
		x2 = x6;
		x3 = _mm_load_ps(src + i + 4);
		x4 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(1, 1, 1, 1));
		x5 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(2, 2, 2, 2));
		x6 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(3, 3, 3, 3));
		x3 = _mm_shuffle_ps(x3, x3, _MM_SHUFFLE(0, 0, 0, 0));

		//y0 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(1, 1, 1, 1));
		y1 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(2, 2, 2, 2));
		y2 = _mm_shuffle_ps(y3, y3, _MM_SHUFFLE(3, 3, 3, 3));
		y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(//_mm_add_ps(
			_mm_mul_ps(X0, x0),
			_mm_mul_ps(X1, x1)),
			_mm_mul_ps(X2, x2)),
			_mm_mul_ps(X3, x3)),
			_mm_mul_ps(X4, x4)),
			_mm_mul_ps(X5, x5)),
			_mm_mul_ps(X6, x6)),
			//_mm_mul_ps(Y0, y0)),
			_mm_mul_ps(Y1, y1)),
			_mm_mul_ps(Y2, y2));

		_mm_store_ps(dst + (i >> 1), _mm_shuffle_ps(O0, y3, _MM_SHUFFLE(3, 1, 3, 1)));
	}

	_mm_store_ps(filter + 4, y3);
	_mm_store_ss(filter + 0, x4);
	_mm_store_ss(filter + 1, x5);
	_mm_store_ss(filter + 2, x6);
}

void ct_audio_upsample4(CtAudioButterworthFilter filter, CtAudioCubicSpline interpolator, float* dst, float const* src, size_t size)
{
	size -= size & 15;
	size >>= 2;
	if (!size)
		return;

	__m128 const bf_X0 = BW3_QUARTER_BAND_X0;
	__m128 const bf_X1 = BW3_QUARTER_BAND_X1;
	__m128 const bf_X2 = BW3_QUARTER_BAND_X2;
	__m128 const bf_X3 = BW3_QUARTER_BAND_X3;
	__m128 const bf_X4 = BW3_QUARTER_BAND_X4;
	__m128 const bf_X5 = BW3_QUARTER_BAND_X5;
	__m128 const bf_X6 = BW3_QUARTER_BAND_X6;
	__m128 const bf_Y0 = BW3_QUARTER_BAND_Y0;
	__m128 const bf_Y1 = BW3_QUARTER_BAND_Y1;
	__m128 const bf_Y2 = BW3_QUARTER_BAND_Y2;

	__m128 bf_x0;
	__m128 bf_x1;
	__m128 bf_x2;
	__m128 bf_x3;
	__m128 bf_x4 = _mm_load1_ps(filter + 0);
	__m128 bf_x5 = _mm_load1_ps(filter + 1);
	__m128 bf_x6 = _mm_load1_ps(filter + 2);
	__m128 bf_y0;
	__m128 bf_y1;
	__m128 bf_y2;
	__m128 bf_y3 = _mm_load_ps(filter + 4);

	alignas(CT_AUDIO_ALIGN) float src_stack[8];
	memcpy(src_stack, interpolator, sizeof(CtAudioCubicSpline));
	memcpy(src_stack + 4, src, sizeof(CtAudioCubicSpline));

	//cubic spline initial input
	__m128 cs_x0 = _mm_loadu_ps(src_stack + 1);
	__m128 cs_x1 = _mm_loadu_ps(src_stack + 2);
	__m128 cs_x2 = _mm_loadu_ps(src_stack + 3);
	__m128 cs_x3 = _mm_load_ps(src_stack + 4);
	//cubic spline output/filter input
	__m128 cs_y0;
	__m128 cs_y1;
	__m128 cs_y2;
	__m128 cs_y3;

	size_t i = 0;
	goto _skip_first_load;
	do
	{	
		//cubic spline interpolation
		{
			cs_x0 = _mm_loadu_ps(src + i - 3);
			cs_x1 = _mm_loadu_ps(src + i - 2);
			cs_x2 = _mm_loadu_ps(src + i - 1);
			cs_x3 = _mm_load_ps(src + i - 0);
		_skip_first_load:;

			__m128 const d0 = _mm_mul_ps(_mm_sub_ps(cs_x2, cs_x0), _mm_set1_ps(0.5f));
			__m128 const d1 = _mm_mul_ps(_mm_sub_ps(cs_x3, cs_x1), _mm_set1_ps(0.5f));
			//__m128 const a0 = cs_x1;
			//__m128 const a1 = d0;
			__m128 a2 = _mm_sub_ps(_mm_sub_ps(cs_x2, cs_x1/*a0*/), d0/*a1*/);
			__m128 const a3 = _mm_sub_ps(_mm_sub_ps(d1, d0), _mm_mul_ps(a2, _mm_set1_ps(2)));
			a2 = _mm_sub_ps(a2, a3);

			//__m128 y0 = a0;
			__m128 y1 = _mm_add_ps(_mm_add_ps(_mm_add_ps(
				cs_x1, //a0,
				_mm_mul_ps(d0/*a1*/, _mm_set1_ps(0.25f))),
				_mm_mul_ps(a2, _mm_set1_ps(0.0625f))),
				_mm_mul_ps(a3, _mm_set1_ps(0.015625f)));
			__m128 y2 = _mm_add_ps(_mm_add_ps(_mm_add_ps(
				cs_x1, //a0,
				_mm_mul_ps(d0/*a1*/, _mm_set1_ps(0.5f))),
				_mm_mul_ps(a2, _mm_set1_ps(0.25f))),
				_mm_mul_ps(a3, _mm_set1_ps(0.125f)));
			__m128 y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(
				cs_x1, //a0,
				_mm_mul_ps(d0/*a1*/, _mm_set1_ps(0.75f))),
				_mm_mul_ps(a2, _mm_set1_ps(0.5625f))),
				_mm_mul_ps(a3, _mm_set1_ps(0.421875f)));

			__m128 ylo02 = _mm_unpacklo_ps(cs_x1/*y0*/, y2);
			__m128 ylo13 = _mm_unpacklo_ps(y1, y3);
			cs_y0 = _mm_unpacklo_ps(ylo02, ylo13);
			cs_y1 = _mm_unpackhi_ps(ylo02, ylo13);
			__m128 yhi02 = _mm_unpackhi_ps(cs_x1/*y0*/, y2);
			__m128 yhi13 = _mm_unpackhi_ps(y1, y3);
			cs_y2 = _mm_unpacklo_ps(yhi02, yhi13);
			cs_y3 = _mm_unpackhi_ps(yhi02, yhi13);

			/*_mm_store_ps(dst + i * 4 + 0, cs_y0);
			_mm_store_ps(dst + i * 4 + 4, cs_y1);
			_mm_store_ps(dst + i * 4 + 8, cs_y2);
			_mm_store_ps(dst + i * 4 + 12, cs_y3);*/
		}

		//filter for each cs output
		{
			bf_x0 = bf_x4;
			bf_x1 = bf_x5;
			bf_x2 = bf_x6;
			bf_x3 = cs_y0;
			bf_x4 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_x5 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_x6 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_x3 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(0, 0, 0, 0));

			bf_y0 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_y1 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_y2 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_mul_ps(bf_X0, bf_x0),
				_mm_mul_ps(bf_X1, bf_x1)),
				_mm_mul_ps(bf_X2, bf_x2)),
				_mm_mul_ps(bf_X3, bf_x3)),
				_mm_mul_ps(bf_X4, bf_x4)),
				_mm_mul_ps(bf_X5, bf_x5)),
				_mm_mul_ps(bf_X6, bf_x6)),
				_mm_mul_ps(bf_Y0, bf_y0)),
				_mm_mul_ps(bf_Y1, bf_y1)),
				_mm_mul_ps(bf_Y2, bf_y2));

			_mm_store_ps(dst + i * 4 + 0, bf_y3);

			bf_x0 = bf_x4;
			bf_x1 = bf_x5;
			bf_x2 = bf_x6;
			bf_x3 = cs_y1;
			bf_x4 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_x5 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_x6 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_x3 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(0, 0, 0, 0));

			bf_y0 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_y1 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_y2 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_mul_ps(bf_X0, bf_x0),
				_mm_mul_ps(bf_X1, bf_x1)),
				_mm_mul_ps(bf_X2, bf_x2)),
				_mm_mul_ps(bf_X3, bf_x3)),
				_mm_mul_ps(bf_X4, bf_x4)),
				_mm_mul_ps(bf_X5, bf_x5)),
				_mm_mul_ps(bf_X6, bf_x6)),
				_mm_mul_ps(bf_Y0, bf_y0)),
				_mm_mul_ps(bf_Y1, bf_y1)),
				_mm_mul_ps(bf_Y2, bf_y2));

			_mm_store_ps(dst + i * 4 + 4, bf_y3);

			bf_x0 = bf_x4;
			bf_x1 = bf_x5;
			bf_x2 = bf_x6;
			bf_x3 = cs_y2;
			bf_x4 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_x5 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_x6 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_x3 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(0, 0, 0, 0));

			bf_y0 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_y1 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_y2 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_mul_ps(bf_X0, bf_x0),
				_mm_mul_ps(bf_X1, bf_x1)),
				_mm_mul_ps(bf_X2, bf_x2)),
				_mm_mul_ps(bf_X3, bf_x3)),
				_mm_mul_ps(bf_X4, bf_x4)),
				_mm_mul_ps(bf_X5, bf_x5)),
				_mm_mul_ps(bf_X6, bf_x6)),
				_mm_mul_ps(bf_Y0, bf_y0)),
				_mm_mul_ps(bf_Y1, bf_y1)),
				_mm_mul_ps(bf_Y2, bf_y2));

			_mm_store_ps(dst + i * 4 + 8, bf_y3);

			bf_x0 = bf_x4;
			bf_x1 = bf_x5;
			bf_x2 = bf_x6;
			bf_x3 = cs_y3;
			bf_x4 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_x5 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_x6 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_x3 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(0, 0, 0, 0));

			bf_y0 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_y1 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_y2 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_mul_ps(bf_X0, bf_x0),
				_mm_mul_ps(bf_X1, bf_x1)),
				_mm_mul_ps(bf_X2, bf_x2)),
				_mm_mul_ps(bf_X3, bf_x3)),
				_mm_mul_ps(bf_X4, bf_x4)),
				_mm_mul_ps(bf_X5, bf_x5)),
				_mm_mul_ps(bf_X6, bf_x6)),
				_mm_mul_ps(bf_Y0, bf_y0)),
				_mm_mul_ps(bf_Y1, bf_y1)),
				_mm_mul_ps(bf_Y2, bf_y2));

			_mm_store_ps(dst + i * 4 + 12, bf_y3);
		}

		i += 4;
	} while (i < size);

	_mm_store_ps(filter + 4, bf_y3);
	_mm_store_ss(filter + 0, bf_x4);
	_mm_store_ss(filter + 1, bf_x5);
	_mm_store_ss(filter + 2, bf_x6);

	_mm_store_ps(interpolator, cs_x3);
	interpolator[0] = 0;
}


void ct_audio_upsample2(CtAudioButterworthFilter filter, CtAudioCubicSpline interpolator, float* dst, float const* src, size_t size)
{
	size -= size & 7;
	size >>= 1;
	if (!size)
		return;

	__m128 const bf_X0 = BW3_HALF_BAND_X0;
	__m128 const bf_X1 = BW3_HALF_BAND_X1;
	__m128 const bf_X2 = BW3_HALF_BAND_X2;
	__m128 const bf_X3 = BW3_HALF_BAND_X3;
	__m128 const bf_X4 = BW3_HALF_BAND_X4;
	__m128 const bf_X5 = BW3_HALF_BAND_X5;
	__m128 const bf_X6 = BW3_HALF_BAND_X6;
	//__m128 const bf_Y0 = BW3_HALF_BAND_Y0;
	__m128 const bf_Y1 = BW3_HALF_BAND_Y1;
	__m128 const bf_Y2 = BW3_HALF_BAND_Y2;

	__m128 bf_x0;
	__m128 bf_x1;
	__m128 bf_x2;
	__m128 bf_x3;
	__m128 bf_x4 = _mm_load1_ps(filter + 0);
	__m128 bf_x5 = _mm_load1_ps(filter + 1);
	__m128 bf_x6 = _mm_load1_ps(filter + 2);
	//__m128 bf_y0;
	__m128 bf_y1;
	__m128 bf_y2;
	__m128 bf_y3 = _mm_load_ps(filter + 4);

	alignas(CT_AUDIO_ALIGN) float src_stack[8];
	memcpy(src_stack, interpolator, sizeof(CtAudioCubicSpline));
	memcpy(src_stack + 4, src, sizeof(CtAudioCubicSpline));

	//cubic spline initial input
	__m128 cs_x0 = _mm_loadu_ps(src_stack + 1);
	__m128 cs_x1 = _mm_loadu_ps(src_stack + 2);
	__m128 cs_x2 = _mm_loadu_ps(src_stack + 3);
	__m128 cs_x3 = _mm_load_ps(src_stack + 4);
	//cubic spline output/filter input
	__m128 cs_y0;
	__m128 cs_y1;

	size_t i = 0;
	goto _skip_first_load;
	do
	{
		//cubic spline interpolation
		{
			cs_x0 = _mm_loadu_ps(src + i - 3);
			cs_x1 = _mm_loadu_ps(src + i - 2);
			cs_x2 = _mm_loadu_ps(src + i - 1);
			cs_x3 = _mm_load_ps(src + i - 0);
		_skip_first_load:;

			__m128 const d0 = _mm_mul_ps(_mm_sub_ps(cs_x2, cs_x0), _mm_set1_ps(0.5f));
			__m128 const d1 = _mm_mul_ps(_mm_sub_ps(cs_x3, cs_x1), _mm_set1_ps(0.5f));
			//__m128 const a0 = cs_x1;
			//__m128 const a1 = d0;
			__m128 a2 = _mm_sub_ps(_mm_sub_ps(cs_x2, cs_x1/*a0*/), d0/*a1*/);
			__m128 const a3 = _mm_sub_ps(_mm_sub_ps(d1, d0), _mm_mul_ps(a2, _mm_set1_ps(2)));
			a2 = _mm_sub_ps(a2, a3);

			//__m128 y0 = a0;
			__m128 y1 = _mm_add_ps(_mm_add_ps(_mm_add_ps(
				cs_x1, //a0,
				_mm_mul_ps(d0/*a1*/, _mm_set1_ps(0.5f))),
				_mm_mul_ps(a2, _mm_set1_ps(0.25f))),
				_mm_mul_ps(a3, _mm_set1_ps(0.125f)));
			
			cs_y0 = _mm_unpacklo_ps(cs_x1/*y0*/, y1);
			cs_y1 = _mm_unpackhi_ps(cs_x1/*y0*/, y1);
		}

		//filter for each cs output
		{
			bf_x0 = bf_x4;
			bf_x1 = bf_x5;
			bf_x2 = bf_x6;
			bf_x3 = cs_y0;
			bf_x4 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_x5 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_x6 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_x3 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(0, 0, 0, 0));

			//bf_y0 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_y1 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_y2 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(//_mm_add_ps(
				_mm_mul_ps(bf_X0, bf_x0),
				_mm_mul_ps(bf_X1, bf_x1)),
				_mm_mul_ps(bf_X2, bf_x2)),
				_mm_mul_ps(bf_X3, bf_x3)),
				_mm_mul_ps(bf_X4, bf_x4)),
				_mm_mul_ps(bf_X5, bf_x5)),
				_mm_mul_ps(bf_X6, bf_x6)),
				//_mm_mul_ps(bf_Y0, bf_y0)),
				_mm_mul_ps(bf_Y1, bf_y1)),
				_mm_mul_ps(bf_Y2, bf_y2));

			_mm_store_ps(dst + i * 2 + 0, bf_y3);

			bf_x0 = bf_x4;
			bf_x1 = bf_x5;
			bf_x2 = bf_x6;
			bf_x3 = cs_y1;
			bf_x4 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_x5 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_x6 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_x3 = _mm_shuffle_ps(bf_x3, bf_x3, _MM_SHUFFLE(0, 0, 0, 0));

			//bf_y0 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(1, 1, 1, 1));
			bf_y1 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(2, 2, 2, 2));
			bf_y2 = _mm_shuffle_ps(bf_y3, bf_y3, _MM_SHUFFLE(3, 3, 3, 3));
			bf_y3 = _mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(//_mm_add_ps(
				_mm_mul_ps(bf_X0, bf_x0),
				_mm_mul_ps(bf_X1, bf_x1)),
				_mm_mul_ps(bf_X2, bf_x2)),
				_mm_mul_ps(bf_X3, bf_x3)),
				_mm_mul_ps(bf_X4, bf_x4)),
				_mm_mul_ps(bf_X5, bf_x5)),
				_mm_mul_ps(bf_X6, bf_x6)),
				//_mm_mul_ps(bf_Y0, bf_y0)),
				_mm_mul_ps(bf_Y1, bf_y1)),
				_mm_mul_ps(bf_Y2, bf_y2));

			_mm_store_ps(dst + i * 2 + 4, bf_y3);
		}

		i += 4;
	} while (i < size);

	_mm_store_ps(filter + 4, bf_y3);
	_mm_store_ss(filter + 0, bf_x4);
	_mm_store_ss(filter + 1, bf_x5);
	_mm_store_ss(filter + 2, bf_x6);

	_mm_store_ps(interpolator, cs_x3);
	interpolator[0] = 0;
}

size_t ct_audio_resample(CtAudioCubicSpline interpolator, float* dst, float const* src, size_t size, float drift)
{
	alignas(CT_AUDIO_ALIGN) float src_stack[8];
	memcpy(src_stack, interpolator + 1, 3 * sizeof(float));
	memcpy(src_stack + 3, src, 5 * sizeof(float));

	float const samples = (float)size;
	float const in_per_out = (samples + drift) / samples;
	__m128 const in_per_4out = _mm_set1_ps(4 * in_per_out);
	__m128 position = _mm_set_ps(interpolator[0] + 3 * in_per_out, interpolator[0] + 2 * in_per_out, interpolator[0] + in_per_out, interpolator[0]);

	//min drift is -0.25 * samples!! clamp it?

	//cubic spline initial input
	__m128 cs_x0 = _mm_loadu_ps(src_stack + (int32_t)(interpolator[0] + 0 * in_per_out));
	__m128 cs_x1 = _mm_loadu_ps(src_stack + (int32_t)(interpolator[0] + 1 * in_per_out));
	__m128 cs_x2 = _mm_loadu_ps(src_stack + (int32_t)(interpolator[0] + 2 * in_per_out));
	__m128 cs_x3 = _mm_loadu_ps(src_stack + (int32_t)(interpolator[0] + 3 * in_per_out));

	__m128i ipart = _mm_cvttps_epi32(position);
	size_t i = 0;
	goto _skip_first_load;
	do
	{

		{
			ipart = _mm_cvttps_epi32(position);
			alignas(16) int32_t idx[4];
			_mm_store_si128((__m128i*)idx, ipart);
			//cubic spline interpolation
			cs_x0 = _mm_loadu_ps(src + idx[0] - 3);
			cs_x1 = _mm_loadu_ps(src + idx[1] - 3);
			cs_x2 = _mm_loadu_ps(src + idx[2] - 3);
			cs_x3 = _mm_loadu_ps(src + idx[3] - 3);
		}
	_skip_first_load:;

		//reshuffle/rotate by 90deg
		{
			__m128 cs_lo02 = _mm_unpacklo_ps(cs_x0, cs_x2);
			__m128 cs_lo13 = _mm_unpacklo_ps(cs_x1, cs_x3);
			__m128 cs_hi02 = _mm_unpackhi_ps(cs_x0, cs_x2);
			__m128 cs_hi13 = _mm_unpackhi_ps(cs_x1, cs_x3);
			cs_x0 = _mm_unpacklo_ps(cs_lo02, cs_lo13);
			cs_x1 = _mm_unpackhi_ps(cs_lo02, cs_lo13);
			cs_x2 = _mm_unpacklo_ps(cs_hi02, cs_hi13);
			cs_x3 = _mm_unpackhi_ps(cs_hi02, cs_hi13);
		}

		__m128 const d0 = _mm_mul_ps(_mm_sub_ps(cs_x2, cs_x0), _mm_set1_ps(0.5f));
		__m128 const d1 = _mm_mul_ps(_mm_sub_ps(cs_x3, cs_x1), _mm_set1_ps(0.5f));
		//__m128 const a0 = cs_x1;
		//__m128 const a1 = d0;
		__m128 a2 = _mm_sub_ps(_mm_sub_ps(cs_x2, cs_x1/*a0*/), d0/*a1*/);
		__m128 const a3 = _mm_sub_ps(_mm_sub_ps(d1, d0), _mm_mul_ps(a2, _mm_set1_ps(2)));
		a2 = _mm_sub_ps(a2, a3);

		__m128 x = _mm_sub_ps(position, _mm_cvtepi32_ps(ipart));
		__m128 x2 = _mm_mul_ps(x, x);
		__m128 x3 = _mm_mul_ps(x, x2);

		__m128 y = _mm_add_ps(_mm_add_ps(_mm_add_ps(
			cs_x1, //a0,
			_mm_mul_ps(d0/*a1*/, x)),
			_mm_mul_ps(a2, x2)),
			_mm_mul_ps(a3, x3));

		_mm_store_ps(dst + i, y);
		
	
		position = _mm_add_ps(position, in_per_4out);
		i += 4;
	} while (i < size);

	ipart = _mm_cvttps_epi32(position);
	int32_t idx = _mm_cvtsi128_si32(ipart);
	float off = _mm_cvtss_f32(position) - (float)idx;
	memcpy(interpolator + 1, src + idx - 3, 3 * sizeof(float));
	interpolator[0] = off;
	
	return idx;
}

float ct_audio_resample_offset(CtAudioCubicSpline interpolator)
{
	return interpolator[0];
}
