#pragma once
#include "audio_include.h"

typedef struct CtAudioHrtfTable*			CtAudioHrtfTable;
typedef struct CtAudioHrtfTableUpdate*		CtAudioHrtfTableUpdate;
typedef struct CtAudioHrtfSpatializer*		CtAudioHrtfSpatializer;

CT_AUDIO_API size_t					ct_audio_hrtf_table_size(uint32_t impulse_power, uint32_t elev_size, uint32_t const* elev_points);
CT_AUDIO_API CtAudioHrtfTable		ct_audio_hrtf_table_create(uint32_t impulse_power, uint32_t elev_size, uint32_t const* elev_points, float const* elev_angles, void* memory);
CT_AUDIO_API size_t					ct_audio_hrtf_table_update_size(CtAudioHrtfTable table);
CT_AUDIO_API CtAudioHrtfTableUpdate	ct_audio_hrtf_table_update_create(CtAudioHrtfTable table, void* memory);
CT_AUDIO_API void					ct_audio_hrtf_table_update_resource(CtAudioHrtfTableUpdate update, uint32_t resource_idx, float angle, float const* data_l, float const* data_r);


CT_AUDIO_API void					ct_audio_hrtf_invert(uint32_t power, float* dst, float const* src, float const* filter);
