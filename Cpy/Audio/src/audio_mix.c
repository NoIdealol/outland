#include "pch.h"
#include "audio_mix.h"
#include <immintrin.h>

void ct_audio_mix2(float* dst, float const* src1, float const* src2, size_t size)
{
	size -= size & 3;
	for (uint32_t idx = 0; idx < size; idx += 4)
	{
		_mm_store_ps(dst + idx, _mm_add_ps(
			_mm_load_ps(src1 + idx),
			_mm_load_ps(src2 + idx))
		);
	}
}

void ct_audio_mix3(float* dst, float const* src1, float const* src2, float const* src3, size_t size)
{
	size -= size & 3;
	for (uint32_t idx = 0; idx < size; idx += 4)
	{
		_mm_store_ps(dst + idx, _mm_add_ps(_mm_add_ps(
			_mm_load_ps(src1 + idx),
			_mm_load_ps(src2 + idx)),
			_mm_load_ps(src3 + idx))
		);
	}
}

void ct_audio_mix4(float* dst, float const* src1, float const* src2, float const* src3, float const* src4, size_t size)
{
	size -= size & 3;
	for (uint32_t idx = 0; idx < size; idx += 4)
	{
		_mm_store_ps(dst + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_load_ps(src1 + idx),
			_mm_load_ps(src2 + idx)),
			_mm_load_ps(src3 + idx)),
			_mm_load_ps(src4 + idx))
		);
	}
}

void ct_audio_mix5(float* dst, float const* const* src_array, size_t size)
{
	float const* src1 = src_array[0];
	float const* src2 = src_array[1];
	float const* src3 = src_array[2];
	float const* src4 = src_array[3];
	float const* src5 = src_array[4];

	for (uint32_t idx = 0; idx < size; idx += 4)
	{
		_mm_store_ps(dst + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_add_ps(
				_mm_load_ps(src1 + idx),
				_mm_load_ps(src2 + idx)),
				_mm_load_ps(src3 + idx)),
				_mm_load_ps(src4 + idx)),
				_mm_load_ps(src5 + idx))
		);
	}
}

void ct_audio_mix6(float* dst, float const* const* src_array, size_t size)
{
	float const* src1 = src_array[0];
	float const* src2 = src_array[1];
	float const* src3 = src_array[2];
	float const* src4 = src_array[3];
	float const* src5 = src_array[4];
	float const* src6 = src_array[5];

	for (uint32_t idx = 0; idx < size; idx += 4)
	{
		_mm_store_ps(dst + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_add_ps(_mm_add_ps(
				_mm_load_ps(src1 + idx),
				_mm_load_ps(src2 + idx)),
				_mm_load_ps(src3 + idx)),
				_mm_load_ps(src4 + idx)),
				_mm_load_ps(src5 + idx)),
				_mm_load_ps(src6 + idx))
		);
	}
}

void ct_audio_mix7(float* dst, float const* const* src_array, size_t size)
{
	float const* src1 = src_array[0];
	float const* src2 = src_array[1];
	float const* src3 = src_array[2];
	float const* src4 = src_array[3];
	float const* src5 = src_array[4];
	float const* src6 = src_array[5];
	float const* src7 = src_array[6];

	for (uint32_t idx = 0; idx < size; idx += 4)
	{
		_mm_store_ps(dst + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_load_ps(src1 + idx),
				_mm_load_ps(src2 + idx)),
				_mm_load_ps(src3 + idx)),
				_mm_load_ps(src4 + idx)),
				_mm_load_ps(src5 + idx)),
				_mm_load_ps(src6 + idx)),
				_mm_load_ps(src7 + idx))
		);
	}
}

void ct_audio_mix8(float* dst, float const* const* src_array, size_t size)
{
	float const* src1 = src_array[0];
	float const* src2 = src_array[1];
	float const* src3 = src_array[2];
	float const* src4 = src_array[3];
	float const* src5 = src_array[4];
	float const* src6 = src_array[5];
	float const* src7 = src_array[6];
	float const* src8 = src_array[7];

	for (uint32_t idx = 0; idx < size; idx += 4)
	{
		_mm_store_ps(dst + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
			_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_load_ps(src1 + idx),
				_mm_load_ps(src2 + idx)),
				_mm_load_ps(src3 + idx)),
				_mm_load_ps(src4 + idx)),
				_mm_load_ps(src5 + idx)),
				_mm_load_ps(src6 + idx)),
				_mm_load_ps(src7 + idx)),
				_mm_load_ps(src8 + idx))
		);
	}
}

void ct_audio_mixa(float* dst, float const* const* src_array, size_t src_count, size_t size)
{
	switch (src_count)
	{
	case 0:
		return;
	case 1:
		memcpy(dst, src_array[0], sizeof(float) * size);
		return;
	case 2:
		ct_audio_mix2(dst, src_array[0], src_array[1], size);
		return;
	case 3:
		ct_audio_mix3(dst, src_array[0], src_array[1], src_array[2], size);
		return;
	case 4:
		ct_audio_mix4(dst, src_array[0], src_array[1], src_array[2], src_array[3], size);
		return;
	case 5:
		ct_audio_mix5(dst, src_array, size);
		return;
	case 6:
		ct_audio_mix6(dst, src_array, size);
		return;
	case 7:
		ct_audio_mix7(dst, src_array, size);
		return;
	case 8:
		ct_audio_mix8(dst, src_array, size);
		return;
	default:
		ct_audio_mix8(dst, src_array, size);
		src_array += 8;
		src_count -= 8;
		break;
	}

	while (8 <= src_count)
	{
		float const* src1 = src_array[0];
		float const* src2 = src_array[1];
		float const* src3 = src_array[2];
		float const* src4 = src_array[3];
		float const* src5 = src_array[4];
		float const* src6 = src_array[5];
		float const* src7 = src_array[6];
		float const* src8 = src_array[7];

		for (uint32_t idx = 0; idx < size; idx += 4)
		{
			_mm_store_ps(dst + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(_mm_add_ps(
					_mm_load_ps(dst + idx),
					_mm_load_ps(src1 + idx)),
					_mm_load_ps(src2 + idx)),
					_mm_load_ps(src3 + idx)),
					_mm_load_ps(src4 + idx)),
					_mm_load_ps(src5 + idx)),
					_mm_load_ps(src6 + idx)),
					_mm_load_ps(src7 + idx)),
					_mm_load_ps(src8 + idx))
			);
		}

		src_array += 8;
		src_count -= 8;
	}

	float const* tmp_array[8];
	switch (src_count)
	{
	case 0:
		return;
	case 1:;
		ct_audio_mix2(dst, dst, src_array[0], size);
		return;
	case 2:
		ct_audio_mix3(dst, dst, src_array[0], src_array[1], size);
		return;
	case 3:
		ct_audio_mix4(dst, dst, src_array[0], src_array[1], src_array[2], size);
		return;
	case 4:
		tmp_array[0] = dst;
		memcpy((tmp_array + 1), src_array, sizeof(float*) * 4);
		ct_audio_mix5(dst, src_array, size);
		return;
	case 5:
		tmp_array[0] = dst;
		memcpy(tmp_array + 1, src_array, sizeof(float*) * 5);
		ct_audio_mix6(dst, src_array, size);
		return;
	case 6:
		tmp_array[0] = dst;
		memcpy(tmp_array + 1, src_array, sizeof(float*) * 6);
		ct_audio_mix7(dst, src_array, size);
		return;
	case 7:
	default:
		tmp_array[0] = dst;
		memcpy(tmp_array + 1, src_array, sizeof(float*) * 7);
		ct_audio_mix8(dst, src_array, size);
		return;
	}
}
