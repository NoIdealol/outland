#include "pch.h"
#include "audio_file_wav.h"

#define AUDIO_FORMAT_PCM 1
#define AUDIO_FORMAT_IEEE_FLOAT 3
#define AUDIO_FORMAT_EXTENSIBLE 0xFFFE

typedef struct WaveSubformat {
	uint32_t	data1;
	uint16_t	data2;
	uint16_t	data3;
	uint8_t		data4[8];
} WaveSubformat;

//00000003-0000-0010-8000-00aa00389b71
static WaveSubformat const WAVE_SUBFORMAT_IEEE_FLOAT = { 0x00000003, 0x0000, 0x0010,{ 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71 } };
//00000001-0000-0010-8000-00aa00389b71
static WaveSubformat const WAVE_SUBFORMAT_PCM = { 0x00000001, 0x0000, 0x0010,{ 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71 } };

typedef struct RiffChunk
{
	uint32_t	chunk_id;
	uint32_t	chunk_size;
} RiffChunk;

typedef struct RiffFormat
{
	uint32_t	format;
} RiffFormat;

typedef struct WaveFormat
{
	uint16_t	audio_format;
	uint16_t	num_channels;
	uint32_t	sample_rate;
	uint32_t	byte_rate;
	uint16_t	block_align;
	uint16_t	bits_per_sample;
} WaveFormat;

typedef struct WaveFormatExt
{
	WaveFormat		format;
	uint16_t		extra_param_size;
	uint16_t		valid_bits_per_sample;
	uint32_t		channel_mask;
	WaveSubformat	sub_format;
} WaveFormatExt;

static void const* audio_file_fmt_PCM(CtAudioFileWav* wav, WaveFormat const* fmt, void const* data, uint32_t size)
{
	switch (fmt->bits_per_sample)
	{
	case 8:
		wav->format = CT_AUDIO_FORMAT_UINT8;
		break;
	case 16:
		wav->format = CT_AUDIO_FORMAT_INT16;
		break;
	case 24:
		wav->format = CT_AUDIO_FORMAT_INT24;
		break;
	default:
		return null;
	}

	switch (fmt->num_channels)
	{
	case 1:
		wav->channel_count = 1;
		wav->channel_mask = CT_AUDIO_CHANNEL_FRONT_CENTER;
		break;
	case 2:
		wav->channel_count = 2;
		wav->channel_mask = CT_AUDIO_CHANNEL_FRONT_LEFT | CT_AUDIO_CHANNEL_FRONT_RIGHT;
		break;
	default:
		return null;
	}
	
	if (fmt->block_align != ((fmt->num_channels * fmt->bits_per_sample) >> 3))
		return null;

	wav->frame_count = size / fmt->block_align;
	if (size % fmt->block_align)
		return null;

	wav->sample_rate = fmt->sample_rate;

	return data;
}

static void const* audio_file_fmt_IEEE_FLOAT(CtAudioFileWav* wav, WaveFormat const* fmt, void const* data, uint32_t size)
{
	switch (fmt->bits_per_sample)
	{
	case 32:
		wav->format = CT_AUDIO_FORMAT_FLOAT;
		break;
	default:
		return null;
	}

	switch (fmt->num_channels)
	{
	case 1:
		wav->channel_count = 1;
		wav->channel_mask = CT_AUDIO_CHANNEL_FRONT_CENTER;
		break;
	case 2:
		wav->channel_count = 2;
		wav->channel_mask = CT_AUDIO_CHANNEL_FRONT_LEFT | CT_AUDIO_CHANNEL_FRONT_RIGHT;
		break;
	default:
		return null;
	}

	if (fmt->block_align != ((fmt->num_channels * fmt->bits_per_sample) >> 3))
		return null;

	wav->frame_count = size / fmt->block_align;
	if (size % fmt->block_align)
		return null;

	wav->sample_rate = fmt->sample_rate;

	return data;
}

static void const* audio_file_fmtext_PCM(CtAudioFileWav* wav, WaveFormatExt const* fmtext, void const* data, uint32_t size)
{
	switch (fmtext->format.bits_per_sample)
	{
	case 8:
		wav->format = CT_AUDIO_FORMAT_UINT8;
		break;
	case 16:
		wav->format = CT_AUDIO_FORMAT_INT16;
		break;
	case 24:
		wav->format = CT_AUDIO_FORMAT_INT24;
		break;
	case 32:
		wav->format = CT_AUDIO_FORMAT_INT32;
		break;
	default:
		return null;
	}

	if (fmtext->valid_bits_per_sample > fmtext->format.bits_per_sample)
		return null;

	if (fmtext->format.num_channels != m_bit_count_u32(fmtext->channel_mask))
		return null;

	wav->channel_count = fmtext->format.num_channels;
	wav->channel_mask = fmtext->channel_mask;

	if (fmtext->format.block_align != ((fmtext->format.num_channels * fmtext->format.bits_per_sample) >> 3))
		return null;

	wav->frame_count = size / fmtext->format.block_align;
	if (size % fmtext->format.block_align)
		return null;

	wav->sample_rate = fmtext->format.sample_rate;

	return data;
}

static void const* audio_file_fmtext_IEEE_FLOAT(CtAudioFileWav* wav, WaveFormatExt const* fmtext, void const* data, uint32_t size)
{
	switch (fmtext->format.bits_per_sample)
	{
	case 32:
		wav->format = CT_AUDIO_FORMAT_FLOAT;
		break;
	default:
		return null;
	}

	if (fmtext->valid_bits_per_sample != fmtext->format.bits_per_sample)
		return null;

	if (fmtext->format.num_channels != m_bit_count_u32(fmtext->channel_mask))
		return null;

	wav->channel_count = fmtext->format.num_channels;
	wav->channel_mask = fmtext->channel_mask;

	if (fmtext->format.block_align != ((fmtext->format.num_channels * fmtext->format.bits_per_sample) >> 3))
		return null;

	wav->frame_count = size / fmtext->format.block_align;
	if (size % fmtext->format.block_align)
		return null;

	wav->sample_rate = fmtext->format.sample_rate;

	return data;
}


void const* ct_audio_file_wav(CtAudioFileWav* wav, void const* file, size_t size)
{
	if (!wav)
		return null;

	if (!file)
		return null;

	if (mem_align_offset_const(file, 4))
		return null;

	//RIFF header
	if (sizeof(RiffChunk) + sizeof(RiffFormat) > size)
		return null;

	RiffChunk const* riff_chunk = file;
	if ('FFIR' != riff_chunk->chunk_id)
		return null;

	if (riff_chunk->chunk_size != size - sizeof(RiffChunk))
		return null;

	RiffFormat const* riff_format = mem_offset_const(file, sizeof(RiffChunk));
	if ('EVAW' != riff_format->format)
		return null;

	RiffChunk const* fmt_chunk = null;
	RiffChunk const* data_chunk = null;

	size_t offset = sizeof(RiffChunk) + sizeof(RiffFormat);
	//find the needed chunks
	while (offset < size)
	{
		if (sizeof(RiffChunk) > size - offset)
			return null;

		RiffChunk const* chunk = mem_offset_const(file, offset);
		offset += sizeof(RiffChunk);

		if (chunk->chunk_size > size - offset)
			return null;

		offset += m_align_u32(chunk->chunk_size, 4);

		switch (chunk->chunk_id)
		{
		case ' tmf':
			if (fmt_chunk)
				return null;
			fmt_chunk = chunk;
			break;
		case 'atad':
			if (data_chunk)
				return null;
			data_chunk = chunk;
			break;
		default:
			break;
		}
	}

	if (!(fmt_chunk && data_chunk))
		return null;

	if (sizeof(WaveFormat) > fmt_chunk->chunk_size)
		return null;

	void const* data = mem_offset_const(data_chunk, sizeof(RiffChunk));
	WaveFormat const* fmt = mem_offset_const(fmt_chunk, sizeof(RiffChunk));

	switch (fmt->audio_format)
	{
	case AUDIO_FORMAT_PCM:
	{
		if (sizeof(WaveFormat) != fmt_chunk->chunk_size)
			return null;

		return audio_file_fmt_PCM(wav, fmt, data, data_chunk->chunk_size);
	}
	case AUDIO_FORMAT_IEEE_FLOAT:
	{
		if (sizeof(WaveFormat) != fmt_chunk->chunk_size)
			return null;

		return audio_file_fmt_IEEE_FLOAT(wav, fmt, data, data_chunk->chunk_size);
	}
	case AUDIO_FORMAT_EXTENSIBLE:
	{
		if (sizeof(WaveFormatExt) != fmt_chunk->chunk_size)
			return null;

		WaveFormatExt const* fmtext = (WaveFormatExt const*)fmt;
		if (sizeof(WaveFormatExt) - sizeof(WaveFormat) - sizeof(fmtext->extra_param_size) != fmtext->extra_param_size)
			return null;

		if (0 == memcmp(&WAVE_SUBFORMAT_PCM, &fmtext->sub_format, sizeof(WaveSubformat)))
		{
			return audio_file_fmtext_PCM(wav, fmtext, data, data_chunk->chunk_size);
		}

		if (0 == memcmp(&WAVE_SUBFORMAT_IEEE_FLOAT, &fmtext->sub_format, sizeof(WaveSubformat)))
		{
			return audio_file_fmtext_IEEE_FLOAT(wav, fmtext, data, data_chunk->chunk_size);
		}

		return null;
	}
	default:
		return null;
	}
}

typedef struct WaveFileHeader
{
	RiffChunk		riff_chunk;
	RiffFormat		riff_format;
	RiffChunk		wave_chunk;
	WaveFormatExt	wave_format;
	RiffChunk		data_chunk;
} WaveFileHeader;

STATIC_ASSERT(CT_AUDIO_FILE_WAV_HEADER_SIZE == sizeof(WaveFileHeader));

void* ct_audio_file_wav_header(CtAudioFileWav const* wav, void* memory)
{
	if (!wav)
		return null;
	if (!memory)
		return null;

	if (mem_align_offset(memory, alignof(WaveFileHeader)))
		return null;

	WaveSubformat const* sub_format;
	uint32_t bytes_per_sample;
	switch (wav->format)
	{
	case CT_AUDIO_FORMAT_UINT8:
		bytes_per_sample = 1;
		sub_format = &WAVE_SUBFORMAT_PCM;
		break;
	case CT_AUDIO_FORMAT_INT16:
		bytes_per_sample = 2;
		sub_format = &WAVE_SUBFORMAT_PCM;
		break;
	case CT_AUDIO_FORMAT_INT24:
		bytes_per_sample = 3;
		sub_format = &WAVE_SUBFORMAT_PCM;
		break;
	case CT_AUDIO_FORMAT_INT32:
		bytes_per_sample = 4;
		sub_format = &WAVE_SUBFORMAT_PCM;
		break;
	case CT_AUDIO_FORMAT_FLOAT:
		bytes_per_sample = 4;
		sub_format = &WAVE_SUBFORMAT_IEEE_FLOAT;
		break;
	default:
		return null;
	}

	if (wav->channel_count != m_bit_count_u32(wav->channel_mask))
		return null;
	
	WaveFileHeader* header = memory;
	header->riff_chunk.chunk_id = 'FFIR';
	header->riff_chunk.chunk_size = sizeof(WaveFileHeader) - sizeof(RiffChunk) + bytes_per_sample * wav->channel_count * wav->frame_count;
	header->riff_format.format = 'EVAW';
	header->wave_chunk.chunk_id = ' tmf';
	header->wave_chunk.chunk_size = sizeof(WaveFormatExt);
	header->wave_format.channel_mask = wav->channel_mask;
	header->wave_format.sub_format = *sub_format;
	header->wave_format.valid_bits_per_sample = bytes_per_sample * 8;
	header->wave_format.extra_param_size = sizeof(WaveFormatExt) - sizeof(WaveFormat) - sizeof(header->wave_format.extra_param_size);
	header->wave_format.format.audio_format = AUDIO_FORMAT_EXTENSIBLE;
	header->wave_format.format.bits_per_sample = bytes_per_sample * 8;
	header->wave_format.format.num_channels = wav->channel_count;
	header->wave_format.format.block_align = bytes_per_sample * wav->channel_count;
	header->wave_format.format.sample_rate = wav->sample_rate;
	header->wave_format.format.byte_rate = bytes_per_sample * wav->channel_count * wav->sample_rate;
	header->data_chunk.chunk_id = 'atad';
	header->data_chunk.chunk_size = bytes_per_sample * wav->channel_count * wav->frame_count;
	
	return header;
}