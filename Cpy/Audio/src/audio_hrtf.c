#include "pch.h"
#include "audio_hrtf.h"
#include "audio_hartley.h"
#include "audio_mix.h"
#include <immintrin.h>

//one pair of transfer functions
typedef float* HrtfData[2];

//one circle of elevation
typedef struct HrtfElevation
{
	float*		hrtf_dir_x;
	float*		hrtf_dir_y;
	uint32_t	hrtf_size;
	HrtfData*	hrtf_data;
} HrtfElevation;

typedef struct HrtfSphere
{
	float*			elev_dir_z; //ordered z values
	float*			elev_dir_xy;
	uint32_t		elev_size;
	HrtfElevation*	elev_data;
	float*			hrtf_top;
	float*			hrtf_bot;
} HrtfSphere;

typedef struct HrtfMix
{
	float*	l;
	float*	r;
	float	a;
} HrtfMix;

struct CtAudioHrtfTable
{
	CtAudioHartleyTable	table;
	HrtfSphere			sphere;
};

struct CtAudioHrtfTableUpdate
{
	CtAudioHrtfTable	hrtf;
	CtAudioHartleyTable	table;
	void*				scratch;
};

struct CtAudioHrtfSpatializer
{
	CtAudioHrtfTable	table;
	float*				filter_l;
	float*				filter_r;
};

static size_t hrtf_table_struct(size_t offsets[6], uint32_t impulse_power, uint32_t elev_size, uint32_t const* elev_points)
{
	uint32_t const ht_size = 2 << impulse_power;

	uint32_t measurements_size = 0;
	for (uint32_t i = 0; i < elev_size; ++i)
		measurements_size += elev_points[i];

	size_t const align[6] = 
	{ 
		alignof(struct CtAudioHrtfTable), 
		alignof(float), 
		alignof(HrtfElevation), 
		alignof(HrtfData), 
		CT_AUDIO_ALIGN,
		CT_AUDIO_ALIGN,
	};

	size_t const size[6] = 
	{ 
		sizeof(struct CtAudioHrtfTable), 
		sizeof(float) * (elev_size * 2 + measurements_size * 2),
		sizeof(HrtfElevation) * elev_size,
		sizeof(HrtfData) * measurements_size,
		sizeof(float) * ht_size * (measurements_size * 2 + 2),
		ct_audio_hartley_table_size(impulse_power + 1),
	};

	return mem_struct(offsets, align, size, lengthof(size));
}

size_t ct_audio_hrtf_table_size(uint32_t impulse_power, uint32_t elev_size, uint32_t const* elev_points)
{
	size_t offsets[6];
	return hrtf_table_struct(offsets, impulse_power, elev_size, elev_points);
}

CtAudioHrtfTable ct_audio_hrtf_table_create(uint32_t impulse_power, uint32_t elev_size, uint32_t const* elev_points, float const* elev_angles, void* memory)
{
	uint32_t const ht_size = 2 << impulse_power;

	size_t offsets[6];
	size_t size = hrtf_table_struct(offsets, impulse_power, elev_size, elev_points);
	memset(memory, 0, size);

	CtAudioHrtfTable hrtf = mem_offset(memory, offsets[0]);
	float* dir = mem_offset(memory, offsets[1]);
	HrtfElevation* elev = mem_offset(memory, offsets[2]);
	HrtfData* data = mem_offset(memory, offsets[3]);
	float* samples = mem_offset(memory, offsets[4]);

	hrtf->table = ct_audio_hartley_table_create(impulse_power + 1, mem_offset(memory, offsets[5]));
	hrtf->sphere.elev_size = elev_size;
	hrtf->sphere.elev_data = elev;
	hrtf->sphere.elev_dir_z = dir;
	hrtf->sphere.elev_dir_xy = dir + elev_size;
	hrtf->sphere.hrtf_bot = samples;
	hrtf->sphere.hrtf_top = samples + ht_size;

	dir += elev_size * 2;
	samples += ht_size * 2;

	for (uint32_t i = 0; i < elev_size; ++i)
	{
		hrtf->sphere.elev_dir_z[i] = m_sin_f32(elev_angles[i]);
		hrtf->sphere.elev_dir_xy[i] = m_cos_f32(elev_angles[i]);
		hrtf->sphere.elev_data[i].hrtf_data = data;
		hrtf->sphere.elev_data[i].hrtf_size = elev_points[i];
		hrtf->sphere.elev_data[i].hrtf_dir_x = dir;
		hrtf->sphere.elev_data[i].hrtf_dir_y = dir + elev_points[i];
	
		for (uint32_t j = 0; j < elev_points[i]; ++j)
		{
			hrtf->sphere.elev_data[i].hrtf_data[j][0] = samples;
			hrtf->sphere.elev_data[i].hrtf_data[j][1] = samples + ht_size;
			samples += ht_size * 2;
		}

		data += elev_points[i];
		dir + elev_points[i] * 2;
	}

	return hrtf;
}

static size_t hrtf_table_update_struct(size_t offsets[3], CtAudioHrtfTable table)
{
	size_t const align[3] =
	{
		alignof(struct CtAudioHrtfTableUpdate),
		CT_AUDIO_ALIGN,
		CT_AUDIO_ALIGN,
	};

	size_t const size[3] =
	{
		sizeof(struct CtAudioHrtfTableUpdate),
		ct_audio_hartley_table_size(ct_audio_hartley_table_power(table->table) - 1),
		sizeof(float) * (1 << ct_audio_hartley_table_power(table->table)),
	};

	return mem_struct(offsets, align, size, lengthof(size));
}

size_t ct_audio_hrtf_table_update_size(CtAudioHrtfTable table)
{
	size_t offsets[3];
	return hrtf_table_update_struct(offsets, table);
}

CtAudioHrtfTableUpdate ct_audio_hrtf_table_update_create(CtAudioHrtfTable table, void* memory)
{
	size_t offsets[3];
	hrtf_table_update_struct(offsets, table);

	CtAudioHrtfTableUpdate update = mem_offset(memory, offsets[0]);
	update->hrtf = table;
	update->table = ct_audio_hartley_table_create(ct_audio_hartley_table_power(table->table) - 1, mem_offset(memory, offsets[1]));
	update->scratch = mem_offset(memory, offsets[2]);
	
	return update;
}

static void hrtf_zero_phase_pad(CtAudioHrtfTableUpdate update, float* resource, float const* data)
{
	uint32_t const size = 1 << ct_audio_hartley_table_power(update->table);
	uint32_t const mask = size - 1;

	float* src = update->scratch;
	float* dst = mem_offset(update->scratch, size * sizeof(float));
	float* ht = ct_audio_hartley_transform(update->table, data, src, dst, 1);
	float* zp = ht == src ? dst : src;

	for (uint32_t i = 0; i < size; i += 4)
	{
		/*float real = 0.5f * (ht[i] + ht[mask & (size - i)]);
		float imag = 0.5f * (ht[i] - ht[mask & (size - i)]);

		ret[i] = m_sqrt_f32(real * real + imag * imag);*/

		__m128 x = _mm_load_ps(ht + i);
		//todo: optimize this?
		__m128 y = _mm_set_ps(ht[mask & (size - i - 3)], ht[mask & (size - i - 2)], ht[mask & (size - i - 1)], ht[mask & (size - i)]);

		__m128 real = _mm_mul_ps(_mm_add_ps(x, y), _mm_set1_ps(0.5f));
		__m128 imag = _mm_mul_ps(_mm_sub_ps(x, y), _mm_set1_ps(0.5f));
		__m128 amp = _mm_sqrt_ps(_mm_add_ps(_mm_mul_ps(real, real), _mm_mul_ps(imag, imag)));
		_mm_store_ps(zp + i, amp);
	}

	float* zpp = ct_audio_hartley_transform(update->table, zp, zp, ht, 1.0f / (float)size);
	if (zpp != src)
		memcpy(src, zpp, size * sizeof(float));
	memset(dst, 0, size * sizeof(float));

	ht = ct_audio_hartley_transform(update->hrtf->table, src, src, resource, 1);
	if (ht != resource)
		memcpy(resource, ht, (1 << ct_audio_hartley_table_power(update->hrtf->table)) * sizeof(float));
}

static void hrtf_mix_elevation(HrtfElevation const* elevation, float* resource, uint32_t size)
{
	ct_audio_mix4(
		resource, 
		elevation->hrtf_data[0][0], 
		elevation->hrtf_data[0][1], 
		elevation->hrtf_data[elevation->hrtf_size - 1][0], 
		elevation->hrtf_data[elevation->hrtf_size - 1][1],
		size);
	
	for (uint32_t i = 0; i < size; i += 4)
	{
		_mm_store_ps(
			resource + i, 
			_mm_mul_ps(
				_mm_load_ps(resource + i), 
				_mm_set1_ps(0.25f))
		);
	}
}

void ct_audio_hrtf_table_update_resource(CtAudioHrtfTableUpdate update, uint32_t resource_idx, float angle, float const* data_l, float const* data_r)
{
	void* const scratch = update->scratch;
	uint32_t const size = 1 << ct_audio_hartley_table_power(update->hrtf->table);

	uint32_t const top_idx = update->hrtf->sphere.elev_size + 1;
	if (0 == resource_idx)
	{
		if (data_l)
			hrtf_zero_phase_pad(update, update->hrtf->sphere.hrtf_bot, data_l);
		else
			hrtf_mix_elevation(update->hrtf->sphere.elev_data, update->hrtf->sphere.hrtf_bot, size);
	}
	else if (top_idx == resource_idx)
	{
		if (data_l)
			hrtf_zero_phase_pad(update, update->hrtf->sphere.hrtf_top, data_l);
		else
			hrtf_mix_elevation(update->hrtf->sphere.elev_data, update->hrtf->sphere.hrtf_top, size);
	}
	else if (top_idx > resource_idx)
	{
		--resource_idx;

		uint32_t elev = 0;
		uint32_t point_offset = 0;
		while ((update->hrtf->sphere.elev_data[elev].hrtf_size + point_offset) <= resource_idx)
		{
			point_offset += update->hrtf->sphere.elev_data[elev].hrtf_size;
			++elev;
		}
		uint32_t point_idx = resource_idx - point_offset;

		hrtf_zero_phase_pad(update, update->hrtf->sphere.elev_data[elev].hrtf_data[point_idx][0], data_l);
		hrtf_zero_phase_pad(update, update->hrtf->sphere.elev_data[elev].hrtf_data[point_idx][1], data_r);
	
		update->hrtf->sphere.elev_data[elev].hrtf_dir_x[point_idx] = m_cos_f32(angle);
		update->hrtf->sphere.elev_data[elev].hrtf_dir_y[point_idx] = m_sin_f32(angle);
	}
	else
	{
		assert(false); //invalid index
	}
}



//find nearest greater idx, returns size if all are less or equal,
static uint32_t find_nearest_greater(float const* data, uint32_t size, float value)
{
	uint32_t idx1 = 0;
	uint32_t idx2 = size;

	while (1 != idx2 - idx1)
	{
		uint32_t idx = idx1 + (idx2 - idx1 >> 1);
		if (data[idx] > value)
			idx2 = idx;
		else
			idx1 = idx;
	}

	if (!(data[idx1] > value))
		++idx1;

	return idx1;
}

static void find_amp(float amp[2], float src_x, float src_y, float vec1_x, float vec1_y, float vec2_x, float vec2_y)
{
	float inv_det = 1.0f / (vec1_x * vec2_y - vec2_x * vec1_y);
	float gain1 = inv_det * (+vec2_y * src_x - vec2_x * src_y);
	float gain2 = inv_det * (-vec1_y * src_x + vec1_x * src_y);

	float inv_gain = 1.0f / (gain1 + gain2);
	gain1 *= inv_gain;
	gain2 *= inv_gain;

	amp[0] = gain1;
	amp[1] = gain2;
}

static uint32_t find_elev_mix(HrtfMix mix[4], HrtfElevation const* elev, float const dir_xy[2], float amp_scale)
{
	float const dir_y = m_abs_f32(dir_xy[1]);
	uint32_t const azim_idx = find_nearest_greater(elev->hrtf_dir_x, elev->hrtf_size, dir_xy[0]);
	int const l_idx = 0 > dir_xy[1];
	int const r_idx = 1 - l_idx;

	uint32_t mix_size = 0;
	if (0 == azim_idx)
	{
		//mixing front
		uint32_t const idx = 0;
		if (elev->hrtf_dir_x[idx] == dir_xy[0]) //exact match/front
		{
			mix[0].l = elev->hrtf_data[idx][l_idx];
			mix[0].r = elev->hrtf_data[idx][r_idx];
			mix[0].a = amp_scale;
			mix_size = 1;
		}
		else //mirror
		{
			float amp[2];
			find_amp(amp, dir_xy[0], dir_y, elev->hrtf_dir_x[idx], elev->hrtf_dir_y[idx], elev->hrtf_dir_x[idx], -elev->hrtf_dir_y[idx]);
			mix[0].l = elev->hrtf_data[idx][l_idx];
			mix[0].r = elev->hrtf_data[idx][r_idx];
			mix[0].a = amp[0] * amp_scale;
			mix[1].l = elev->hrtf_data[idx][r_idx];
			mix[1].r = elev->hrtf_data[idx][l_idx];
			mix[1].a = amp[1] * amp_scale;
			mix_size = 2;
		}
	}
	else if (elev->hrtf_size == azim_idx)
	{
		//mixing back
		uint32_t const idx = azim_idx - 1;
		if (elev->hrtf_dir_x[idx] == dir_xy[0]) //exact match/back
		{
			mix[0].l = elev->hrtf_data[idx][l_idx];
			mix[0].r = elev->hrtf_data[idx][r_idx];
			mix[0].a = amp_scale;
			mix_size = 1;
		}
		else //mirror
		{
			float amp[2];
			find_amp(amp, dir_xy[0], dir_y, elev->hrtf_dir_x[idx], elev->hrtf_dir_y[idx], elev->hrtf_dir_x[idx], -elev->hrtf_dir_y[idx]);
			mix[0].l = elev->hrtf_data[idx][l_idx];
			mix[0].r = elev->hrtf_data[idx][r_idx];
			mix[0].a = amp[0] * amp_scale;
			mix[1].l = elev->hrtf_data[idx][r_idx];
			mix[1].r = elev->hrtf_data[idx][l_idx];
			mix[1].a = amp[1] * amp_scale;
			mix_size = 2;
		}
	}
	else
	{
		//mixing side
		uint32_t const idx1 = azim_idx - 1;
		uint32_t const idx2 = azim_idx;
		float amp[2];
		find_amp(amp, dir_xy[0], dir_y, elev->hrtf_dir_x[idx1], elev->hrtf_dir_y[idx1], elev->hrtf_dir_x[idx2], elev->hrtf_dir_y[idx2]);
		mix[0].l = elev->hrtf_data[idx1][l_idx];
		mix[0].r = elev->hrtf_data[idx1][r_idx];
		mix[0].a = amp[0] * amp_scale;
		mix[1].l = elev->hrtf_data[idx2][l_idx];
		mix[1].r = elev->hrtf_data[idx2][r_idx];
		mix[1].a = amp[1] * amp_scale;
		mix_size = 2;
	}

	return mix_size;
}

static uint32_t find_sphere_mix(HrtfMix mix_data[4], HrtfSphere const* sphere, float const dir[3])
{
	uint32_t elev_idx = find_nearest_greater(sphere->elev_dir_z, sphere->elev_size, dir[2]);
	uint32_t mix_size = 0;

	float amp[2];
	float dir_ele[2] = { dir[0], dir[1] };
	float dir_xy = m_sqrt_f32(dir[0] * dir[0] + dir[1] * dir[1]);

	if (0 == elev_idx)
	{
		//mixing bot
		if (dir[2] <= -0.99f)
		{
			mix_data[0].l = sphere->hrtf_bot;
			mix_data[0].r = sphere->hrtf_bot;
			mix_data[0].a = 1;
			mix_size = 1;
		}
		else
		{
			find_amp(amp, dir_xy, dir[2], 0.0f, -1.0f, sphere->elev_dir_xy[elev_idx], sphere->elev_dir_z[elev_idx]);

			mix_data[0].l = sphere->hrtf_bot;
			mix_data[0].r = sphere->hrtf_bot;
			mix_data[0].a = amp[0];
			mix_size = 1;

			dir_ele[0] /= dir_xy;
			dir_ele[1] /= dir_xy;
			mix_size += find_elev_mix(mix_data + mix_size, sphere->elev_data + elev_idx, dir_ele, amp[1]);
		}
	}
	else if (sphere->elev_size == elev_idx)
	{
		//mixing top
		if (dir[2] >= 0.99f)
		{
			mix_data[0].l = sphere->hrtf_top;
			mix_data[0].r = sphere->hrtf_top;
			mix_data[0].a = 1;
			mix_size = 1;
		}
		else
		{
			--elev_idx;
			find_amp(amp, dir_xy, dir[2], sphere->elev_dir_xy[elev_idx], sphere->elev_dir_z[elev_idx], 0.0f, 1.0f);

			mix_data[0].l = sphere->hrtf_top;
			mix_data[0].r = sphere->hrtf_top;
			mix_data[0].a = amp[1];
			mix_size = 1;

			dir_ele[0] /= dir_xy;
			dir_ele[1] /= dir_xy;
			mix_size += find_elev_mix(mix_data + mix_size, sphere->elev_data + elev_idx, dir_ele, amp[0]);
		}
	}
	else
	{
		//mixing mid
		uint32_t elev1_idx = elev_idx;
		uint32_t elev0_idx = elev_idx - 1;
		find_amp(amp, 
			dir_xy, dir[2], 
			sphere->elev_dir_xy[elev0_idx], sphere->elev_dir_z[elev0_idx], 
			sphere->elev_dir_xy[elev1_idx], sphere->elev_dir_z[elev1_idx]);
	
		dir_ele[0] /= dir_xy;
		dir_ele[1] /= dir_xy;
		mix_size += find_elev_mix(mix_data + mix_size, sphere->elev_data + elev0_idx, dir_ele, amp[0]);
		mix_size += find_elev_mix(mix_data + mix_size, sphere->elev_data + elev1_idx, dir_ele, amp[1]);
	}

	return mix_size;
}

static void hrtf_mix(HrtfMix const mix_data[4], uint32_t mix_size, float l[], float r[], uint32_t size)
{
	if (4 == mix_size)
	{
		__m128 a0 = _mm_set1_ps(mix_data[0].a);
		__m128 a1 = _mm_set1_ps(mix_data[1].a);
		__m128 a2 = _mm_set1_ps(mix_data[2].a);
		__m128 a3 = _mm_set1_ps(mix_data[3].a);
		float* l0 = mix_data[0].l;
		float* l1 = mix_data[1].l;
		float* l2 = mix_data[2].l;
		float* l3 = mix_data[3].l;
		float* r0 = mix_data[0].r;
		float* r1 = mix_data[1].r;
		float* r2 = mix_data[2].r;
		float* r3 = mix_data[3].r;

		for (uint32_t idx = 0; idx < size; idx += 4)
		{
			_mm_store_ps(l + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_mul_ps(_mm_load_ps(l0 + idx), a0),
				_mm_mul_ps(_mm_load_ps(l1 + idx), a1)),
				_mm_mul_ps(_mm_load_ps(l2 + idx), a2)),
				_mm_mul_ps(_mm_load_ps(l3 + idx), a3))
			);
		}

		for (uint32_t idx = 0; idx < size; idx += 4)
		{
			_mm_store_ps(r + idx, _mm_add_ps(_mm_add_ps(_mm_add_ps(
				_mm_mul_ps(_mm_load_ps(r0 + idx), a0),
				_mm_mul_ps(_mm_load_ps(r1 + idx), a1)),
				_mm_mul_ps(_mm_load_ps(r2 + idx), a2)),
				_mm_mul_ps(_mm_load_ps(r3 + idx), a3))
			);
		}
	}
	else if(3 == mix_size)
	{
		__m128 a0 = _mm_set1_ps(mix_data[0].a);
		__m128 a1 = _mm_set1_ps(mix_data[1].a);
		__m128 a2 = _mm_set1_ps(mix_data[2].a);
		float* l0 = mix_data[0].l;
		float* l1 = mix_data[1].l;
		float* l2 = mix_data[2].l;
		float* r0 = mix_data[0].r;
		float* r1 = mix_data[1].r;
		float* r2 = mix_data[2].r;

		for (uint32_t idx = 0; idx < size; idx += 4)
		{
			_mm_store_ps(l + idx, _mm_add_ps(_mm_add_ps(
				_mm_mul_ps(_mm_load_ps(l0 + idx), a0),
				_mm_mul_ps(_mm_load_ps(l1 + idx), a1)),
				_mm_mul_ps(_mm_load_ps(l2 + idx), a2))
			);
		}

		for (uint32_t idx = 0; idx < size; idx += 4)
		{
			_mm_store_ps(r + idx, _mm_add_ps(_mm_add_ps(
				_mm_mul_ps(_mm_load_ps(r0 + idx), a0),
				_mm_mul_ps(_mm_load_ps(r1 + idx), a1)),
				_mm_mul_ps(_mm_load_ps(r2 + idx), a2))
			);
		}
	}
	else if (1 == mix_size)
	{
		assert(1 == mix_data[0].a);
		memcpy(l, mix_data[0].l, size * sizeof(float));
		memcpy(r, mix_data[0].r, size * sizeof(float));
	}
	else
	{
		assert(false);
	}
}











void	ct_audio_hrtf_invert(uint32_t power, float* dst, float const* src, float const* filter)
{
	uint32_t const size = 1 << power;
	for (uint32_t i = 0; i < size; i += 4)
	{
		__m128 v = _mm_load_ps(src + i);
		__m128 f = _mm_load_ps(filter + i);
		__m128 r = _mm_div_ps(v, f);
		_mm_store_ps(dst + i, r);
	}
}