#include "pch.h"
#include "audio_hartley.h"
#include <immintrin.h>

struct CtAudioHartleyTable
{
	uint32_t	power;
	uint32_t*	idx_table;
	float*		sin_table;
};

//--------------------------------------------------
static size_t hartley_table_struct(uint32_t power, size_t* offsets)
{
	size_t align[] = { alignof(struct CtAudioHartleyTable), alignof(uint32_t), alignof(float) };
	size_t size[3];
	size[0] = sizeof(struct CtAudioHartleyTable);
	size[1] = sizeof(uint32_t) << (power - 4);
	size[2] = (sizeof(float) << (power - 1)) * 3;
	
	return mem_struct(offsets, align, size, 3);
}

//--------------------------------------------------
size_t ct_audio_hartley_table_size(uint32_t power)
{
	size_t offsets[3];
	return hartley_table_struct(power, offsets);
}

//--------------------------------------------------
CtAudioHartleyTable ct_audio_hartley_table_create(uint32_t power, void* memory)
{
	size_t offsets[3];
	hartley_table_struct(power, offsets);

	CtAudioHartleyTable table = memory;
	table->idx_table = mem_offset(memory, offsets[1]);
	table->sin_table = mem_offset(memory, offsets[2]);
	table->power = power;

	{
		float* sin_table = table->sin_table;
		for (uint32_t p = 2; p < power; ++p)
		{
			uint32_t const size = 1 << p;
			uint32_t const loops = size + (size >> 1);
			for (uint32_t i = 0; i < loops; ++i)
			{
				sin_table[i] = m_sin_f32((i * m_pi_f32) / size);
			}

			sin_table += loops;
		}
	}

	{
		uint32_t const loops = 1 << (power - 4);
		uint32_t const max_bit = 1 << (power - 1);
		for (uint32_t l = 0; l < loops; ++l)
		{
			uint32_t const val = l << 2;
			uint32_t rbits = 0;
			for (uint32_t i = 0; i < power; ++i)
			{
				if ((1 << i) & val)
					rbits |= max_bit >> i;
			}

			table->idx_table[l] = rbits;
		}
	}

	return table;
}

//--------------------------------------------------
uint32_t ct_audio_hartley_table_power(CtAudioHartleyTable table)
{
	return table->power;
}

//--------------------------------------------------
float* ct_audio_hartley_transform(CtAudioHartleyTable table, float const* data, float* src, float* dst, float scale)
{
	uint32_t const power = table->power;
	
	//first 2 trivial passes, reshuffle order -> 4 parallel chunks of data
	{
		uint32_t const* const idx_table = table->idx_table;
		uint32_t const size = 1 << power;
		uint32_t const off_val = size >> 1;
		uint32_t const off_hft = size >> 2;
		uint32_t const off_hft_val = off_hft + off_val;
		//
		uint32_t off0 = 0;
		uint32_t off1 = size >> 2;
		uint32_t off2 = size >> 1;
		uint32_t off3 = off1 + off2;
		//

		__m128 v_scale = _mm_set1_ps(scale);

		uint32_t const loops = size >> 4;
		for (uint32_t i = 0; i < loops; ++i)
		{
			uint32_t index = idx_table[i];

			__m128 i0 = _mm_load_ps(data + index);
			__m128 i1 = _mm_load_ps(data + index + off_val);
			__m128 o0 = _mm_add_ps(i0, i1);
			__m128 o1 = _mm_sub_ps(i0, i1);

			__m128 i2 = _mm_load_ps(data + index + off_hft);
			__m128 i3 = _mm_load_ps(data + index + off_hft_val);
			__m128 o2 = _mm_add_ps(i2, i3);
			__m128 o3 = _mm_sub_ps(i2, i3);

			__m128 h0 = _mm_add_ps(o0, o2);
			__m128 h2 = _mm_sub_ps(o0, o2);
			__m128 h1 = _mm_add_ps(o1, o3);
			__m128 h3 = _mm_sub_ps(o1, o3);

			//shuffle to order
			__m128 s11_10_01_00 = _mm_movelh_ps(h0, h1);
			__m128 s31_30_21_20 = _mm_movelh_ps(h2, h3);
			__m128 s30_20_10_00 = _mm_shuffle_ps(s11_10_01_00, s31_30_21_20, _MM_SHUFFLE(2, 0, 2, 0));
			__m128 s31_21_11_01 = _mm_shuffle_ps(s11_10_01_00, s31_30_21_20, _MM_SHUFFLE(3, 1, 3, 1));

			__m128 s03_02_13_12 = _mm_movehl_ps(h0, h1);
			__m128 s23_22_33_32 = _mm_movehl_ps(h2, h3);
			__m128 s32_22_12_02 = _mm_shuffle_ps(s03_02_13_12, s23_22_33_32, _MM_SHUFFLE(0, 2, 0, 2));
			__m128 s33_23_13_03 = _mm_shuffle_ps(s03_02_13_12, s23_22_33_32, _MM_SHUFFLE(1, 3, 1, 3));

			_mm_store_ps(dst + off0, _mm_mul_ps(s30_20_10_00, v_scale));
			_mm_store_ps(dst + off1, _mm_mul_ps(s32_22_12_02, v_scale));
			_mm_store_ps(dst + off2, _mm_mul_ps(s31_21_11_01, v_scale));
			_mm_store_ps(dst + off3, _mm_mul_ps(s33_23_13_03, v_scale));

			off0 += 4;
			off1 += 4;
			off2 += 4;
			off3 += 4;
		}
	}

	//flip buffers
	{
		float* tmp = dst;
		dst = src;
		src = tmp;
	}

	//do iterative passes
	{
		float const* sin_table = table->sin_table;
		float const* cos_table;
		uint32_t const loops = power - 2; //first 2 loops are hard coded
		for (uint32_t l = 0; l < loops; ++l)
		{
			uint32_t const inv_power = power - l - 3; //minus one power
			uint32_t const blocks = 1 << inv_power;
			uint32_t const w_loops4 = 4 << l;
			uint32_t const cos_off = 2 << l;
			cos_table = sin_table + cos_off;

			float* src0 = src;
			float* src1 = src0 + w_loops4;
			float* src2 = src1 + w_loops4 - 3;
			float* dst0 = dst;
			float* dst1 = dst0 + w_loops4;

			for (uint32_t b = 0; b < blocks; ++b)
			{
				//first 4 freq
				{
					__m128 hd = _mm_load_ps(src0);
					__m128 hc = _mm_load_ps(src1);
					__m128 hs = _mm_load_ps(src2 - 1);
					hs = _mm_shuffle_ps(hs, hs, _MM_SHUFFLE(1, 2, 3, 0));

					hc = _mm_mul_ps(hc, _mm_loadu_ps(cos_table));
					hs = _mm_mul_ps(hs, _mm_loadu_ps(sin_table));

					__m128 h1 = _mm_add_ps(hc, hs);

					_mm_store_ps(dst0, _mm_add_ps(hd, h1));
					_mm_store_ps(dst1, _mm_sub_ps(hd, h1));
				}

				//butterfly loop for rest of freq
				for (uint32_t w4 = 4; w4 < w_loops4; w4 += 4)
				{
					__m128 hd = _mm_load_ps(src0 + w4);
					__m128 hc = _mm_load_ps(src1 + w4);
					__m128 hs = _mm_loadu_ps(src2 - w4);
					hs = _mm_shuffle_ps(hs, hs, _MM_SHUFFLE(0, 1, 2, 3));

					hc = _mm_mul_ps(hc, _mm_loadu_ps(w4 + cos_table));
					hs = _mm_mul_ps(hs, _mm_loadu_ps(w4 + sin_table));

					__m128 h1 = _mm_add_ps(hc, hs);

					_mm_store_ps(dst0 + w4, _mm_add_ps(hd, h1));
					_mm_store_ps(dst1 + w4, _mm_sub_ps(hd, h1));
				}

				//offset pointers
				src0 += (w_loops4 << 1);
				src1 += (w_loops4 << 1);
				src2 += (w_loops4 << 1);
				dst0 += (w_loops4 << 1);
				dst1 += (w_loops4 << 1);
			}

			sin_table += cos_off * 3;

			//flip buffers
			{
				float* tmp = dst;
				dst = src;
				src = tmp;
			}
		}
	}

	return src;
}

//--------------------------------------------------
void ct_audio_hartley_convolve(CtAudioHartleyTable table, float* z, float const* x, float const* y)
{
	// Z[w] = { X[w] * (Y[w] + Y[-w]) + X[-w] * (Y[w] - Y[-w]) } / 2

	uint32_t const size = 1 << table->power;
	uint32_t const loops = (size >> 2) - 1;
	float const* xi = x + size;
	float const* yi = y + size;

	//first misaligned
	z[0] = x[0] * (y[0] + y[0]) + x[0] * (y[0] - y[0]);

	++x;
	++y;
	++z;

	for (uint32_t l = 0; l < loops; ++l)
	{
		xi -= 4;
		yi -= 4;

		__m128 X = _mm_loadu_ps(x);
		__m128 Xi = _mm_loadr_ps(xi);
		__m128 Y = _mm_loadu_ps(y);
		__m128 Yi = _mm_loadr_ps(yi);

		_mm_storeu_ps(z, _mm_add_ps(
			_mm_mul_ps(_mm_add_ps(Y, Yi), X),
			_mm_mul_ps(_mm_sub_ps(Y, Yi), Xi)
		));

		z += 4;
		x += 4;
		y += 4;
	}

	--x;
	--y;
	--z;

	{
		xi -= 4;
		yi -= 4;

		__m128 X = _mm_load_ps(x);
		__m128 Xi = _mm_load_ps(xi);
		Xi = _mm_shuffle_ps(Xi, Xi, _MM_SHUFFLE(1, 2, 3, 0));
		__m128 Y = _mm_load_ps(y);
		__m128 Yi = _mm_load_ps(yi);
		Yi = _mm_shuffle_ps(Yi, Yi, _MM_SHUFFLE(1, 2, 3, 0));
		__m128 Z1 = _mm_load1_ps(z);

		_mm_store_ps(z, _mm_move_ss(_mm_add_ps(
			_mm_mul_ps(_mm_add_ps(Y, Yi), X),
			_mm_mul_ps(_mm_sub_ps(Y, Yi), Xi)
		), Z1));
	}
}

//--------------------------------------------------
void ct_audio_hartley_convolve_add(CtAudioHartleyTable table, float* z, float const* x, float const* y)
{
	// Z[w] = { X[w] * (Y[w] + Y[-w]) + X[-w] * (Y[w] - Y[-w]) } / 2

	uint32_t const size = 1 << table->power;
	uint32_t const loops = (size >> 2) - 1;
	float const* xi = x + size;
	float const* yi = y + size;

	//first misaligned
	z[0] += x[0] * (y[0] + y[0]) + x[0] * (y[0] - y[0]);

	++x;
	++y;
	++z;

	for (uint32_t l = 0; l < loops; ++l)
	{
		xi -= 4;
		yi -= 4;

		__m128 X = _mm_loadu_ps(x);
		__m128 Xi = _mm_loadr_ps(xi);
		__m128 Y = _mm_loadu_ps(y);
		__m128 Yi = _mm_loadr_ps(yi);

		_mm_storeu_ps(z, _mm_add_ps(_mm_loadu_ps(z), _mm_add_ps(
			_mm_mul_ps(_mm_add_ps(Y, Yi), X),
			_mm_mul_ps(_mm_sub_ps(Y, Yi), Xi)
		)));

		z += 4;
		x += 4;
		y += 4;
	}

	--x;
	--y;
	--z;

	{
		xi -= 4;
		yi -= 4;

		__m128 X = _mm_load_ps(x);
		__m128 Xi = _mm_load_ps(xi);
		Xi = _mm_shuffle_ps(Xi, Xi, _MM_SHUFFLE(1, 2, 3, 0));
		__m128 Y = _mm_load_ps(y);
		__m128 Yi = _mm_load_ps(yi);
		Yi = _mm_shuffle_ps(Yi, Yi, _MM_SHUFFLE(1, 2, 3, 0));
		__m128 Z1 = _mm_load1_ps(z);

		_mm_store_ps(z, _mm_move_ss(_mm_add_ps(_mm_load_ps(z), _mm_add_ps(
			_mm_mul_ps(_mm_add_ps(Y, Yi), X),
			_mm_mul_ps(_mm_sub_ps(Y, Yi), Xi)
		)), Z1));
	}
}