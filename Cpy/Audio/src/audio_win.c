#include "core.h"
#if OS_WINDOWS
#include "audio.h"

#define CONST_VTABLE
#include <initguid.h>

#include <Mmdeviceapi.h>
#include <Audioclient.h>

#define _clsid(Class) &CLSID_##Class
#define _iid(Interface) &IID_##Interface

#define def_clsid(Class) CLSID const CLSID_##Class = 
#define def_iid(Interface) IID const IID_##Interface =

//There is no .lib with GUIDs, so we hack them in here.
//GUIDs are ABI, must be constant.
def_clsid(MMDeviceEnumerator) { 0xbcde0395, 0xe52f, 0x467c, { 0x8e, 0x3d, 0xc4, 0x57, 0x92, 0x91, 0x69, 0x2e } };
def_iid(IMMDeviceEnumerator) { 0xa95664d2, 0x9614, 0x4f35, { 0xa7, 0x46, 0xde, 0x8d, 0xb6, 0x36, 0x17, 0xe6 } };
def_iid(IMMEndpoint) { 0x1be09788, 0x6894, 0x4089, { 0x85, 0x86, 0x9a, 0x2a, 0x6c, 0x26, 0x5a, 0xc5 } };
def_iid(IAudioClient) { 0x1cb9ad4c, 0xdbfa, 0x4c32, { 0xb1, 0x78, 0xc2, 0xf5, 0x68, 0xa7, 0x03, 0xb2 } };
def_iid(IAudioClock) { 0xcd63314f, 0x3fba, 0x4a1b, { 0x81, 0x2c, 0xef, 0x96, 0x35, 0x87, 0x28, 0xe7 } };
def_iid(IAudioCaptureClient) { 0xc8adbd64, 0xe71e, 0x48a0, { 0xa4, 0xde, 0x18, 0x5c, 0x39, 0x5c, 0xd3, 0x17 } };
def_iid(IAudioRenderClient) { 0xf294acfc, 0x3146, 0x4483, { 0xa7, 0xbf, 0xad, 0xdc, 0xa7, 0xc2, 0x60, 0xe2 } };

//static PROPERTYKEY const KEY_Device_FriendlyName = { { 0xa45c254e, 0xdf1c, 0x4efd,{ 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0, } }, 14 };
GUID const KSDATAFORMAT_SUBTYPE_IEEE_FLOAT = { 0x00000003, 0x0000, 0x0010,{ 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71 } };	//00000003-0000-0010-8000-00aa00389b71
GUID const KSDATAFORMAT_SUBTYPE_PCM = { 0x00000001, 0x0000, 0x0010,{ 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71 } };		//00000001-0000-0010-8000-00aa00389b71

//////////////////////////////////////////////////
// CHANNEL MAP
//////////////////////////////////////////////////

typedef struct SwChMap
{
	uint8_t sw_ch_idx;
	uint8_t hw_ch_idx;
} SwChMap;

typedef struct ChConvertor
{
	DWORD		hw_ch_mask;
	uint32_t	hw_ch_count;
	uint32_t	hw_bps;
	SwChMap		sw_ch_map[AUDIO_CHANNEL_COUNT];
	uint32_t	sw_ch_count;
	uint32_t	sw_ch_mask;
} ChConvertor;

typedef void FmtEncode(flt32_t const* src, void* dst, uint8_t ch_stride, uint32_t frames);
typedef void FmtDecode(void const* src, flt32_t* dst, uint8_t ch_stride, uint32_t frames);

//--------------------------------------------------
static bool_t chcvt_map_channels(ChConvertor* chcvt, AudioChannel* ch_map, uint32_t ch_count, uint32_t ch_mask)
{
	memset(chcvt->sw_ch_map, 0, sizeof(chcvt->sw_ch_map));
	chcvt->sw_ch_count = 0;
	chcvt->sw_ch_mask = 0;

	if (AUDIO_CHANNEL_COUNT < ch_count)
		return false;

	for (uint8_t sw_ch_idx = 0; sw_ch_idx < ch_count; ++sw_ch_idx)
	{
		uint32_t const sw_ch_bit = 1 << ch_map[sw_ch_idx];
		if (sw_ch_bit & ch_mask)
		{
			if (sw_ch_bit & chcvt->sw_ch_mask)
			{
				chcvt->sw_ch_count = 0;
				return false;
			}

			DWORD hw_ch;
			switch (ch_map[sw_ch_idx])
			{
			case AUDIO_CHANNEL_MONO:
				hw_ch = SPEAKER_FRONT_CENTER;
				break;
			case AUDIO_CHANNEL_FRONT_LEFT:
				hw_ch = SPEAKER_FRONT_LEFT;
				break;
			case AUDIO_CHANNEL_FRONT_RIGHT:
				hw_ch = SPEAKER_FRONT_RIGHT;
				break;
			case AUDIO_CHANNEL_FRONT_CENTER:
				hw_ch = SPEAKER_FRONT_CENTER;
				break;
			case AUDIO_CHANNEL_LOW_FREQUENCY:
				hw_ch = SPEAKER_LOW_FREQUENCY;
				break;
			case AUDIO_CHANNEL_REAR_LEFT:
				hw_ch = SPEAKER_BACK_LEFT;
				break;
			case AUDIO_CHANNEL_REAR_RIGHT:
				hw_ch = SPEAKER_BACK_RIGHT;
				break;
			case AUDIO_CHANNEL_REAR_CENTER:
				hw_ch = SPEAKER_BACK_CENTER;
				break;
			case AUDIO_CHANNEL_SIDE_LEFT:
				hw_ch = SPEAKER_SIDE_LEFT;
				break;
			case AUDIO_CHANNEL_SIDE_RIGHT:
				hw_ch = SPEAKER_SIDE_RIGHT;
				break;
			default:
				chcvt->sw_ch_count = 0;
				return false;
			}

			uint8_t hw_ch_idx = 0;
			for (uint8_t ch_idx = 0; hw_ch != (1 << ch_idx); ++ch_idx)
			{
				if (chcvt->hw_ch_mask & (1 << ch_idx))
					++hw_ch_idx;
			}

			chcvt->sw_ch_mask |= sw_ch_bit;
			chcvt->sw_ch_map[chcvt->sw_ch_count].sw_ch_idx = sw_ch_idx;
			chcvt->sw_ch_map[chcvt->sw_ch_count].hw_ch_idx = hw_ch_idx;
			++chcvt->sw_ch_count;
		}
	}

	return true;
}

//--------------------------------------------------
static bool_t chcvt_init_wfmtex(ChConvertor* chcvt, AudioFormat* afmt, WAVEFORMATEX const* wfmtex)
{
	memset(chcvt, 0, sizeof(ChConvertor));
	afmt->frequency = wfmtex->nSamplesPerSec;
	chcvt->hw_bps = wfmtex->wBitsPerSample >> 3;
	chcvt->hw_ch_count = wfmtex->nChannels;

	switch (wfmtex->nChannels)
	{
	case 1:
		chcvt->hw_ch_mask = SPEAKER_FRONT_CENTER;
		afmt->channel_mask = 1 << AUDIO_CHANNEL_MONO;
		afmt->channel_count = 1;
		return true;
	case 2:
		chcvt->hw_ch_mask = SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT;
		afmt->channel_mask = (1 << AUDIO_CHANNEL_FRONT_LEFT) | (1 << AUDIO_CHANNEL_FRONT_RIGHT);
		afmt->channel_count = 2;
		return true;
	default:
		return false;
	}
}

//--------------------------------------------------
static bool_t chcvt_init_wfmtext(ChConvertor* chcvt, AudioFormat* afmt, WAVEFORMATEXTENSIBLE const* wfmtext)
{
	memset(chcvt, 0, sizeof(ChConvertor));
	afmt->frequency = wfmtext->Format.nSamplesPerSec;
	chcvt->hw_ch_mask = wfmtext->dwChannelMask;
	chcvt->hw_bps = wfmtext->Format.wBitsPerSample >> 3;
	chcvt->hw_ch_count = wfmtext->Format.nChannels;
	afmt->channel_mask = 0;
	afmt->channel_count = 0;

	if (SPEAKER_FRONT_LEFT & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_FRONT_LEFT;
	}
	if (SPEAKER_FRONT_RIGHT & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_FRONT_RIGHT;
	}
	if (SPEAKER_FRONT_CENTER & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_FRONT_CENTER;
	}
	if (SPEAKER_LOW_FREQUENCY & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_LOW_FREQUENCY;
	}
	if (SPEAKER_BACK_LEFT & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_REAR_LEFT;
	}
	if (SPEAKER_BACK_RIGHT & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_REAR_RIGHT;
	}
	if (SPEAKER_BACK_CENTER & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_REAR_CENTER;
	}
	if (SPEAKER_SIDE_LEFT & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_SIDE_LEFT;
	}
	if (SPEAKER_SIDE_RIGHT & chcvt->hw_ch_mask)
	{
		++(afmt->channel_count);
		afmt->channel_mask |= 1 << AUDIO_CHANNEL_SIDE_RIGHT;
	}

	return true;
}

//--------------------------------------------------
static void fmt_enc_i8(flt32_t const* src, void* dst, uint8_t ch_stride, uint32_t frames)
{
	int8_t* idst8 = dst;
	for (uint32_t f = 0; f < frames; ++f)
	{
		idst8[f * ch_stride] = (int8_t)(src[f] * 127.f);
	}
}

//--------------------------------------------------
static void fmt_enc_i16(flt32_t const* src, void* dst, uint8_t ch_stride, uint32_t frames)
{
	int16_t* idst16 = dst;
	for (uint32_t f = 0; f < frames; ++f)
	{
		idst16[f * ch_stride] = (int16_t)(src[f] * 65535.f);
	}
}

//--------------------------------------------------
static void fmt_enc_i32(flt32_t const* src, void* dst, uint8_t ch_stride, uint32_t frames)
{
	int32_t* idst32 = dst;
	for (uint32_t f = 0; f < frames; ++f)
	{
		idst32[f * ch_stride] = (int32_t)(src[f] * 2147400000.f);
	}
}

//--------------------------------------------------
static void fmt_enc_f32(flt32_t const* src, void* dst, uint8_t ch_stride, uint32_t frames)
{
	flt32_t* fdst32 = dst;
	for (uint32_t f = 0; f < frames; ++f)
	{
		fdst32[f * ch_stride] = src[f];
	}
}

//--------------------------------------------------
static void fmt_dec_i8(void const* src, flt32_t* dst, uint8_t ch_stride, uint32_t frames)
{
	int8_t const* isrc8 = src;
	for (uint32_t f = 0; f < frames; ++f)
	{
		dst[f] = ((flt32_t)isrc8[f * ch_stride]) * 781e-5f;
	}
}

//--------------------------------------------------
static void fmt_dec_i16(void const* src, flt32_t* dst, uint8_t ch_stride, uint32_t frames)
{
	int16_t const* isrc16 = src;
	for (uint32_t f = 0; f < frames; ++f)
	{
		dst[f] = ((flt32_t)isrc16[f * ch_stride]) * 1525e-8f;
	}
}

//--------------------------------------------------
static void fmt_dec_i32(void const* src, flt32_t* dst, uint8_t ch_stride, uint32_t frames)
{
	int32_t const* isrc32 = src;
	for (uint32_t f = 0; f < frames; ++f)
	{
		dst[f] = ((flt32_t)isrc32[f * ch_stride]) * 4656e-13f;
	}
}

//--------------------------------------------------
static void fmt_dec_f32(void const* src, flt32_t* dst, uint8_t ch_stride, uint32_t frames)
{
	flt32_t const* fsrc32 = src;
	for (uint32_t f = 0; f < frames; ++f)
	{
		dst[f] = fsrc32[f * ch_stride];
	}
}

//--------------------------------------------------
static bool_t fmtcvt_init_wfmtex(FmtEncode** fmtenc, FmtDecode** fmtdec, WAVEFORMATEX const* wfmtex)
{
	if (WAVE_FORMAT_PCM == wfmtex->wFormatTag)
	{
		if (8 == wfmtex->wBitsPerSample)
		{
			if (fmtenc)
				*fmtenc = &fmt_enc_i8;
			if (fmtdec)
				*fmtdec = &fmt_dec_i8;

			return true;
		}

		if (16 == wfmtex->wBitsPerSample)
		{
			if (fmtenc)
				*fmtenc = &fmt_enc_i16;
			if (fmtdec)
				*fmtdec = &fmt_dec_i16;

			return true;
		}

		return false;
	}

	if (WAVE_FORMAT_IEEE_FLOAT == wfmtex->wFormatTag)
	{
		if (32 != wfmtex->wBitsPerSample)
			return false;

		if (fmtenc)
			*fmtenc = &fmt_enc_f32;
		if (fmtdec)
			*fmtdec = &fmt_dec_f32;

		return true;
	}

	return false;
}

//--------------------------------------------------
static bool_t fmtcvt_init_wfmtext(FmtEncode** fmtenc, FmtDecode** fmtdec, WAVEFORMATEXTENSIBLE const* wfmtext)
{
	if (IsEqualGUID(&KSDATAFORMAT_SUBTYPE_PCM, &wfmtext->SubFormat))
	{
		if (8 == wfmtext->Format.wBitsPerSample)
		{
			if (fmtenc)
				*fmtenc = &fmt_enc_i8;
			if (fmtdec)
				*fmtdec = &fmt_dec_i8;

			return true;
		}

		if (16 == wfmtext->Format.wBitsPerSample)
		{
			if (fmtenc)
				*fmtenc = &fmt_enc_i16;
			if (fmtdec)
				*fmtdec = &fmt_dec_i16;

			return true;
		}

		if (32 == wfmtext->Format.wBitsPerSample)
		{
			if (fmtenc)
				*fmtenc = &fmt_enc_i32;
			if (fmtdec)
				*fmtdec = &fmt_dec_i32;

			return true;
		}

		return false;
	}

	if (IsEqualGUID(&KSDATAFORMAT_SUBTYPE_IEEE_FLOAT, &wfmtext->SubFormat))
	{
		if (32 != wfmtext->Format.wBitsPerSample)
			return false;

		if (fmtenc)
			*fmtenc = &fmt_enc_f32;
		if (fmtdec)
			*fmtdec = &fmt_dec_f32;

		return true;
	}

	return false;
}

//--------------------------------------------------
static bool_t check_format(WAVEFORMATEX const* wfmtex, ChConvertor* chcvt, AudioFormat* afmt, FmtEncode** fmtenc, FmtDecode** fmtdec, WAVEFORMATEXTENSIBLE* mixfmt)
{
	switch (wfmtex->wFormatTag)
	{
	case WAVE_FORMAT_IEEE_FLOAT:
	case WAVE_FORMAT_PCM:
	{
		if (!chcvt_init_wfmtex(chcvt, afmt, wfmtex))
			return false;

		if (!fmtcvt_init_wfmtex(fmtenc, fmtdec, wfmtex))
			return false;

		memset(mixfmt, 0, sizeof(WAVEFORMATEXTENSIBLE));
		memcpy(&mixfmt->Format, wfmtex, sizeof(WAVEFORMATEX));

		return true;
	}
	case WAVE_FORMAT_EXTENSIBLE:
	{
		WAVEFORMATEXTENSIBLE* wfmtext = (WAVEFORMATEXTENSIBLE*)wfmtex;

		if (!chcvt_init_wfmtext(chcvt, afmt, wfmtext))
			return false;

		if (!fmtcvt_init_wfmtext(fmtenc, fmtdec, wfmtext))
			return false;

		memcpy(mixfmt, wfmtext, sizeof(WAVEFORMATEXTENSIBLE));

		return true;
	}
	default:
		return false;
	}
}


//////////////////////////////////////////////////
// AUDIO STREAM
//////////////////////////////////////////////////

typedef enum AudioStreamState
{
	AUDIO_STREAM_STATE_DEVICE_CLEARED,			//initial clean state							-> try to get a device
	AUDIO_STREAM_STATE_DEVICE_FATAL,			//clean state									-> locked here, other events can cause recovery
	AUDIO_STREAM_STATE_DEVICE_INVALID,			//clean state, cannot be used					-> listen for default device changes
	AUDIO_STREAM_STATE_DEVICE_NOT_ACTIVE,		//device is valid, endpoint is not				-> listed for device state changes
	AUDIO_STREAM_STATE_CLIENT_CLEARED,			//device is valid, initial clean state			-> try opening a client
	AUDIO_STREAM_STATE_CLIENT_FATAL,			//device is valid, clean state					-> locked here, other events can cause recovery
	AUDIO_STREAM_STATE_CLIENT_FORMAT,			//device is valid, clean state					-> listen for "OnPropertyValueChanged" for format change
	AUDIO_STREAM_STATE_CLIENT_OPEN,				//device is valid, client cannot be used		-> poll "Initialize"

	AUDIO_STREAM_STATE_USER_START,
	AUDIO_STREAM_STATE_USER_PLAY,
} AudioStreamState;

enum
{
	AUDIO_STREAM_EVENT_DEVICE_STATE_ACTIVE		= 0x1,
	AUDIO_STREAM_EVENT_DEVICE_CHANGED			= 0x2,
	AUDIO_STREAM_EVENT_FORMAT_CHANGED			= 0x4,
	AUDIO_STREAM_EVENT_EXIT						= 0x8,
};

struct AudioStream
{
	IMMNotificationClient		base;
	IMMNotificationClientVtbl	vtbl;
	ChConvertor					chcvt;
	AudioFormat					afmt;
	LONG volatile				events;
};

//////////////////////////////////////////////////
// AUDIO RENDER STREAM
//////////////////////////////////////////////////

typedef struct AudioRenderStream
{
	struct AudioStream			base;
	HANDLE						event;
	IMMDeviceEnumerator*		enumerator;
	IMMDevice*					device;
	LPWSTR						guid;
	IAudioClient*				audio_client;
	IAudioRenderClient*			render_client;
	FmtEncode*					fmtenc;
	WAVEFORMATEXTENSIBLE		mixfmt;
	REFERENCE_TIME				period;
	UINT32						buffer_frames;
	UINT32						period_frames;
	
	int64_t						mix_qpc_offset;
} AudioRenderStream;

//--------------------------------------------------
static HRESULT STDMETHODCALLTYPE render_stream_QueryInterface(IMMNotificationClient* This, REFIID riid, void **ppvObject)
{
	assert(false); //we are not expecting this to be called

	if (!ppvObject || !riid)
		return E_POINTER;

	if (IsEqualIID(_iid(IUnknown), riid))
	{
		*ppvObject = This;
		return S_OK;
	}

	return E_NOINTERFACE;
}

//--------------------------------------------------
static ULONG STDMETHODCALLTYPE render_stream_AddRef(IMMNotificationClient* This)
{
	assert(false); //we are not expecting this to be called

	return 1;
}

//--------------------------------------------------
static ULONG STDMETHODCALLTYPE render_stream_Release(IMMNotificationClient* This)
{
	assert(false); //we are not expecting this to be called

	return 1;
}

//--------------------------------------------------
static HRESULT STDMETHODCALLTYPE render_stream_OnDeviceStateChanged(IMMNotificationClient* This, LPCWSTR pwstrDeviceId, DWORD dwNewState)
{
	AudioStream const stream = (AudioStream)This;

	if (DEVICE_STATE_ACTIVE == dwNewState)
	{
		InterlockedOr(&stream->events, AUDIO_STREAM_EVENT_DEVICE_STATE_ACTIVE);
	}

	return S_OK;
}

//--------------------------------------------------
static HRESULT STDMETHODCALLTYPE render_stream_OnDeviceAdded(IMMNotificationClient* This, LPCWSTR pwstrDeviceId)
{
	return S_OK;
}

//--------------------------------------------------
static HRESULT STDMETHODCALLTYPE render_stream_OnDeviceRemoved(IMMNotificationClient* This, LPCWSTR pwstrDeviceId)
{
	return S_OK;
}

//--------------------------------------------------
static HRESULT STDMETHODCALLTYPE render_stream_OnDefaultDeviceChanged(IMMNotificationClient* This, EDataFlow flow, ERole role, LPCWSTR pwstrDefaultDeviceId)
{
	AudioStream const stream = (AudioStream)This;

	if (eRender == flow && eConsole == role)
	{
		InterlockedOr(&stream->events, AUDIO_STREAM_EVENT_DEVICE_CHANGED);
	}

	return S_OK;
}

//--------------------------------------------------
static HRESULT STDMETHODCALLTYPE render_stream_OnPropertyValueChanged(IMMNotificationClient* This, LPCWSTR pwstrDeviceId, const PROPERTYKEY key)
{
	AudioStream const stream = (AudioStream)This;
	
	if (IsEqualGUID(&PKEY_AudioEngine_DeviceFormat.fmtid, &key.fmtid) && PKEY_AudioEngine_DeviceFormat.pid == key.pid)
	{
		InterlockedOr(&stream->events, AUDIO_STREAM_EVENT_FORMAT_CHANGED);
	}

	return S_OK; 
}

//--------------------------------------------------
static bool_t render_stream_init(AudioRenderStream* stream)
{
	HRESULT hr;

	BOOL b = SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);

	stream->base.base.lpVtbl = &stream->base.vtbl;
	stream->base.vtbl.QueryInterface = render_stream_QueryInterface;
	stream->base.vtbl.AddRef = render_stream_AddRef;
	stream->base.vtbl.Release = render_stream_Release;
	stream->base.vtbl.OnDeviceStateChanged = render_stream_OnDeviceStateChanged;
	stream->base.vtbl.OnDeviceAdded = render_stream_OnDeviceAdded;
	stream->base.vtbl.OnDeviceRemoved = render_stream_OnDeviceRemoved;
	stream->base.vtbl.OnDefaultDeviceChanged = render_stream_OnDefaultDeviceChanged;
	stream->base.vtbl.OnPropertyValueChanged = render_stream_OnPropertyValueChanged;

	hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	if (hr)
	{
		return false;
	}

	HANDLE event = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (NULL == event)
	{
		CoUninitialize();
		return false;
	}

	IMMDeviceEnumerator* enumerator = null;
	hr = CoCreateInstance(_clsid(MMDeviceEnumerator), NULL, CLSCTX_ALL, _iid(IMMDeviceEnumerator), &enumerator);
	if (hr)
	{
		CloseHandle(event);
		CoUninitialize();
		return false;
	}

	hr = enumerator->lpVtbl->RegisterEndpointNotificationCallback(enumerator, &stream->base.base);
	if (hr)
	{
		enumerator->lpVtbl->Release(enumerator);
		CloseHandle(event);
		CoUninitialize();
		return false;
	}

	memset(&stream->base.chcvt, 0, sizeof(stream->base.chcvt));
	memset(&stream->base.afmt, 0, sizeof(stream->base.afmt));
	stream->base.events = 0;
	stream->event = event;
	stream->enumerator = enumerator;
	stream->device = null;
	stream->guid = null;
	stream->audio_client = null;
	stream->render_client = null;
	stream->fmtenc = null;
	memset(&stream->mixfmt, 0, sizeof(stream->mixfmt));
	stream->period = 0;
	stream->buffer_frames = 0;
	stream->period_frames = 0;
	stream->mix_qpc_offset = 0;

	return true;
}

//--------------------------------------------------
static void render_stream_clean(AudioRenderStream* stream)
{
	assert(NULL != stream->event);
	assert(null != stream->enumerator);
	assert(null == stream->device);
	assert(null == stream->guid);
	assert(null == stream->audio_client);
	assert(null == stream->render_client);

	HRESULT hr;
	hr = stream->enumerator->lpVtbl->UnregisterEndpointNotificationCallback(stream->enumerator, &stream->base.base);
	assert(S_OK == hr);

	stream->enumerator->lpVtbl->Release(stream->enumerator);
	stream->enumerator = null;

	CloseHandle(stream->event);
	stream->event = NULL;

	CoUninitialize();
}

//--------------------------------------------------
static void device_clean(AudioRenderStream* stream)
{
	CoTaskMemFree(stream->guid);
	stream->device->lpVtbl->Release(stream->device);

	stream->guid = null;
	stream->device = null;
}

//--------------------------------------------------
static AudioStreamState device_refresh_state(AudioRenderStream* stream)
{
	HRESULT hr;
	DWORD state;
	hr = stream->device->lpVtbl->GetState(stream->device, &state);
	if (S_OK != hr)
	{
		device_clean(stream);
		return AUDIO_STREAM_STATE_DEVICE_FATAL;
	}

	if (DEVICE_STATE_ACTIVE == state)
		return AUDIO_STREAM_STATE_CLIENT_CLEARED;
	else
		return AUDIO_STREAM_STATE_DEVICE_NOT_ACTIVE;
}

//--------------------------------------------------
static AudioStreamState device_init(AudioRenderStream* stream)
{
	HRESULT hr;
	IMMDevice* device = null;
	hr = stream->enumerator->lpVtbl->GetDefaultAudioEndpoint(stream->enumerator, eRender, eConsole, &device);
	if (S_OK != hr)
	{
		if (E_NOTFOUND == hr)
		{
			return AUDIO_STREAM_STATE_DEVICE_INVALID;
		}
		else
		{
			return AUDIO_STREAM_STATE_DEVICE_FATAL;
		}
	}

	LPWSTR guid = null;
	hr = device->lpVtbl->GetId(device, &guid);
	if (S_OK != hr)
	{
		device->lpVtbl->Release(device);
		return AUDIO_STREAM_STATE_DEVICE_FATAL;
	}

	stream->device = device;
	stream->guid = guid;

	return device_refresh_state(stream);
}

//--------------------------------------------------
static void client_clean(AudioRenderStream* stream)
{
	if (stream->render_client)
		stream->render_client->lpVtbl->Release(stream->render_client);
	if (stream->audio_client)
		stream->audio_client->lpVtbl->Release(stream->audio_client);

	memset(&stream->base.chcvt, 0, sizeof(stream->base.chcvt));
	memset(&stream->base.afmt, 0, sizeof(stream->base.afmt));
	stream->audio_client = null;
	stream->render_client = null;
	stream->fmtenc = null;
	memset(&stream->mixfmt, 0, sizeof(stream->mixfmt));
	stream->period = 0;
	stream->buffer_frames = 0;
	stream->period_frames = 0;
	stream->mix_qpc_offset = 0;
}

//--------------------------------------------------
static AudioStreamState client_open(AudioRenderStream* stream)
{
	HRESULT hr;

	hr = stream->audio_client->lpVtbl->Initialize(
		stream->audio_client,
		AUDCLNT_SHAREMODE_SHARED,
		AUDCLNT_STREAMFLAGS_EVENTCALLBACK,
		stream->period *2, //~200% of period
		0,
		&stream->mixfmt.Format,
		NULL);
	if (S_OK != hr)
	{
		if (AUDCLNT_E_CPUUSAGE_EXCEEDED == hr)
		{
			return AUDIO_STREAM_STATE_CLIENT_OPEN;
		}

		if (AUDCLNT_E_DEVICE_IN_USE == hr)
		{
			return AUDIO_STREAM_STATE_CLIENT_OPEN;
		}

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			client_clean(stream);
			return device_refresh_state(stream);
		}

		client_clean(stream);
		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	hr = stream->audio_client->lpVtbl->SetEventHandle(stream->audio_client, stream->event);
	if (S_OK != hr)
	{
		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			client_clean(stream);
			return device_refresh_state(stream);
		}

		client_clean(stream);
		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	hr = stream->audio_client->lpVtbl->GetBufferSize(stream->audio_client, &stream->buffer_frames);
	if (S_OK != hr)
	{
		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			client_clean(stream);
			return device_refresh_state(stream);
		}

		client_clean(stream);
		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	hr = stream->audio_client->lpVtbl->GetService(stream->audio_client, _iid(IAudioRenderClient), &stream->render_client);
	if (S_OK != hr)
	{
		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			client_clean(stream);
			return device_refresh_state(stream);
		}

		client_clean(stream);
		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	stream->period_frames = (UINT32)(((stream->period * stream->mixfmt.Format.nSamplesPerSec) + 9999999) / (10000000));

	LARGE_INTEGER qpf;
	QueryPerformanceFrequency(&qpf);
	//period + ~2ms for jitter and overhead
	stream->mix_qpc_offset = (qpf.QuadPart * stream->period + 9999999) / (10000000) + (qpf.QuadPart * 2) / (1000);

	return AUDIO_STREAM_STATE_USER_START;
}

//--------------------------------------------------
static AudioStreamState client_init(AudioRenderStream* stream)
{
	HRESULT hr;

	hr = stream->device->lpVtbl->Activate(stream->device, _iid(IAudioClient), CLSCTX_ALL, NULL, &stream->audio_client);
	if (S_OK != hr)
	{
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		device_clean(stream);
		return AUDIO_STREAM_STATE_DEVICE_FATAL;
	}
	
	WAVEFORMATEX* wfmtex;
	hr = stream->audio_client->lpVtbl->GetMixFormat(stream->audio_client, &wfmtex);
	if (S_OK != hr)
	{
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	if (false == check_format(wfmtex, &stream->base.chcvt, &stream->base.afmt, &stream->fmtenc, null, &stream->mixfmt))
	{
		CoTaskMemFree(wfmtex);

		client_clean(stream);

		return AUDIO_STREAM_STATE_CLIENT_FORMAT;
	}

	CoTaskMemFree(wfmtex);
	
	hr = stream->audio_client->lpVtbl->GetDevicePeriod(stream->audio_client, &stream->period, NULL);
	if (S_OK != hr)
	{
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	return AUDIO_STREAM_STATE_CLIENT_OPEN;
}

//--------------------------------------------------
static AudioStreamState client_start(AudioRenderStream* stream)
{
	HRESULT hr;

	UINT32 want_frames = stream->period_frames + (stream->period_frames >> 4); // 125% of period
	if (want_frames > stream->buffer_frames)
		want_frames = stream->buffer_frames;

	BYTE* data;
	hr = stream->render_client->lpVtbl->GetBuffer(stream->render_client, want_frames, &data);
	if (S_OK != hr)
	{
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	hr = stream->render_client->lpVtbl->ReleaseBuffer(stream->render_client, want_frames, AUDCLNT_BUFFERFLAGS_SILENT);
	if (S_OK != hr)
	{
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	hr = stream->audio_client->lpVtbl->Start(stream->audio_client);
	if (S_OK != hr)
	{
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	return AUDIO_STREAM_STATE_USER_PLAY;
}

//--------------------------------------------------
static void client_stop(AudioRenderStream* stream)
{
	stream->audio_client->lpVtbl->Stop(stream->audio_client);
}

//--------------------------------------------------
static AudioStreamState client_wait_packet(AudioRenderStream* stream, UINT32* frames, BYTE** buffer, int64_t* mix_time)
{
	HRESULT hr;
	DWORD wait = WaitForSingleObject(stream->event, 1000);

	LARGE_INTEGER qpc;
	QueryPerformanceCounter(&qpc);
	*mix_time = qpc.QuadPart - stream->mix_qpc_offset;

	UINT32 padding = 0;
	hr = stream->audio_client->lpVtbl->GetCurrentPadding(stream->audio_client, &padding);
	if (S_OK != hr)
	{
		client_stop(stream);
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	//do a later timeout check, to see if the client is OK
	if (WAIT_OBJECT_0 != wait)
	{
		client_stop(stream);
		client_clean(stream);
		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	UINT32 want_frames = stream->period_frames + (stream->period_frames >> 4); // 125% of period
	if (want_frames > stream->buffer_frames)
		want_frames = stream->buffer_frames;

		
	if (padding >= want_frames)
	{
		*frames = 0;
		*buffer = null;
		return AUDIO_STREAM_STATE_USER_PLAY;
	}

	UINT32 const mix_frames = want_frames - padding;
	hr = stream->render_client->lpVtbl->GetBuffer(stream->render_client, mix_frames, buffer);
	if (S_OK != hr)
	{
		client_stop(stream);
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	memset(*buffer, 0, mix_frames * stream->base.chcvt.hw_bps * stream->base.chcvt.hw_ch_count);
	*frames = mix_frames;
	return AUDIO_STREAM_STATE_USER_PLAY;
}

//--------------------------------------------------
static AudioStreamState client_free_packet(AudioRenderStream* stream, UINT32 frames)
{
	HRESULT hr;
	hr = stream->render_client->lpVtbl->ReleaseBuffer(stream->render_client, frames, 0);
	if (S_OK != hr)
	{
		client_stop(stream);
		client_clean(stream);

		if (AUDCLNT_E_DEVICE_INVALIDATED == hr)
		{
			return device_refresh_state(stream);
		}

		return AUDIO_STREAM_STATE_CLIENT_FATAL;
	}

	return AUDIO_STREAM_STATE_USER_PLAY;
}

//--------------------------------------------------
static void render_stream_clean_state(AudioRenderStream* stream, AudioStreamState state)
{
	switch (state)
	{
	case AUDIO_STREAM_STATE_USER_PLAY:
	{
		client_stop(stream);
	}
	case AUDIO_STREAM_STATE_CLIENT_OPEN:
	case AUDIO_STREAM_STATE_USER_START:
	{
		client_clean(stream);
	}
	case AUDIO_STREAM_STATE_DEVICE_NOT_ACTIVE:
	case AUDIO_STREAM_STATE_CLIENT_CLEARED:
	case AUDIO_STREAM_STATE_CLIENT_FATAL:
	case AUDIO_STREAM_STATE_CLIENT_FORMAT:
	{
		device_clean(stream);
	}
	default:
		break;
	}
}

//--------------------------------------------------
static AudioStreamState render_stream_on_FORMAT_CHANGED(AudioRenderStream* stream, AudioStreamState state)
{
	switch (state)
	{
	case AUDIO_STREAM_STATE_CLIENT_FORMAT:
		return AUDIO_STREAM_STATE_CLIENT_CLEARED;
	default:
		return state;
	}
}

//--------------------------------------------------
static AudioStreamState render_stream_on_DEVICE_STATE_ACTIVE(AudioRenderStream* stream, AudioStreamState state)
{
	switch (state)
	{
	case AUDIO_STREAM_STATE_DEVICE_NOT_ACTIVE:
	case AUDIO_STREAM_STATE_CLIENT_FATAL:
	case AUDIO_STREAM_STATE_CLIENT_FORMAT:
		return AUDIO_STREAM_STATE_CLIENT_CLEARED;
	default:
		return state;
	}
}

//--------------------------------------------------
static AudioStreamState render_stream_on_DEVICE_CHANGED(AudioRenderStream* stream, AudioStreamState state, AudioCallback* callback_fnc, void* callback_ctx)
{
	switch (state)
	{
	case AUDIO_STREAM_STATE_USER_PLAY:
		callback_fnc(callback_ctx, &stream->base, AUDIO_EVENT_STOP, null);
	default:
		render_stream_clean_state(stream, state);
		return AUDIO_STREAM_STATE_DEVICE_CLEARED;
	}
}

//--------------------------------------------------
static void render_stream_on_EXIT(AudioRenderStream* stream, AudioStreamState state, AudioCallback* callback_fnc, void* callback_ctx)
{
	switch (state)
	{
	case AUDIO_STREAM_STATE_USER_PLAY:
		callback_fnc(callback_ctx, &stream->base, AUDIO_EVENT_STOP, null);
	default:
		callback_fnc(callback_ctx, &stream->base, AUDIO_EVENT_EXIT, null);
		render_stream_clean_state(stream, state);
		render_stream_clean(stream);
		return;
	}
}

//--------------------------------------------------
static AudioStreamState render_stream_idle(AudioRenderStream* stream, AudioStreamState state, AudioCallback* callback_fnc, void* callback_ctx)
{
	Sleep(100);
	callback_fnc(callback_ctx, &stream->base, AUDIO_EVENT_IDLE, null);
	return state;
}

//--------------------------------------------------
static AudioStreamState render_stream_update(AudioRenderStream* stream, AudioStreamState state, AudioCallback* callback_fnc, void* callback_ctx)
{
	switch (state)
	{
	case AUDIO_STREAM_STATE_DEVICE_CLEARED:
	{
		state = device_init(stream);
		if (AUDIO_STREAM_STATE_CLIENT_CLEARED == state)
			return render_stream_update(stream, state, callback_fnc, callback_ctx);

		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_DEVICE_FATAL:
	{
		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_DEVICE_INVALID:
	{
		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_DEVICE_NOT_ACTIVE:
	{
		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_CLIENT_CLEARED:
	{
		state = client_init(stream);
		if (AUDIO_STREAM_STATE_CLIENT_OPEN == state)
			return render_stream_update(stream, state, callback_fnc, callback_ctx);

		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_CLIENT_FATAL:
	{
		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_CLIENT_FORMAT:
	{
		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_CLIENT_OPEN:
	{
		state = client_open(stream);
		if (AUDIO_STREAM_STATE_USER_START == state)
			return render_stream_update(stream, state, callback_fnc, callback_ctx);

		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_USER_START:
	{
		state = client_start(stream);
		if (AUDIO_STREAM_STATE_USER_PLAY == state)
		{
			callback_fnc(callback_ctx, &stream->base, AUDIO_EVENT_START, null);
			return state;
		}

		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	case AUDIO_STREAM_STATE_USER_PLAY:
	{
		UINT32 frames;
		BYTE* buffer;
		int64_t mix_time;
		state = client_wait_packet(stream, &frames, &buffer, &mix_time);
		if (AUDIO_STREAM_STATE_USER_PLAY == state)
		{
			if (0 == frames)
				return state;

			//user data callback
			AudioPacket packet;
			packet.buffer = buffer;
			packet.frames = frames;
			packet.mix_time = mix_time;

			callback_fnc(callback_ctx, &stream->base, AUDIO_EVENT_DATA, &packet);

			state = client_free_packet(stream, frames);
			if (AUDIO_STREAM_STATE_USER_PLAY == state)
				return state;
		}

		//user stop callback
		callback_fnc(callback_ctx, &stream->base, AUDIO_EVENT_STOP, null);
		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
	default:
		assert(false);
		return render_stream_idle(stream, state, callback_fnc, callback_ctx);
	}
}

//--------------------------------------------------
bool_t	audio_render(AudioCallback* callback_fnc, void* callback_ctx)
{
	AudioStreamState state = AUDIO_STREAM_STATE_DEVICE_CLEARED;
	AudioRenderStream stream;
	if (!render_stream_init(&stream))
	{
		callback_fnc(callback_ctx, null, AUDIO_EVENT_EXIT, null);
		return false;
	}

	callback_fnc(callback_ctx, &stream.base, AUDIO_EVENT_INIT, null);

	while (true)
	{
		MSG msg;
		while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessageW(&msg);
		}

		LONG events = InterlockedExchange(&stream.base.events, 0);
		
		if (AUDIO_STREAM_EVENT_EXIT & events)
		{
			render_stream_on_EXIT(&stream, state, callback_fnc, callback_ctx);
			return true;
		}

		if (AUDIO_STREAM_EVENT_DEVICE_CHANGED & events)
		{
			state = render_stream_on_DEVICE_CHANGED(&stream, state, callback_fnc, callback_ctx);
		}

		if (AUDIO_STREAM_EVENT_FORMAT_CHANGED & events)
		{
			state = render_stream_on_FORMAT_CHANGED(&stream, state);
		}

		if (AUDIO_STREAM_EVENT_DEVICE_STATE_ACTIVE & events)
		{
			state = render_stream_on_DEVICE_STATE_ACTIVE(&stream, state);
		}

		state = render_stream_update(&stream, state, callback_fnc, callback_ctx);
	}
}

//--------------------------------------------------
void	audio_stream_exit(AudioStream stream)
{
	InterlockedOr(&stream->events, AUDIO_STREAM_EVENT_EXIT);
}

//--------------------------------------------------
void audio_stream_write(AudioStream stream, void* buffer, flt32_t const** data, uint32_t frames, uint32_t offset)
{
	ChConvertor* const chcvt = &stream->chcvt;
	FmtEncode* const fmtenc = ((AudioRenderStream*)stream)->fmtenc;

	for (uint32_t ch = 0; ch < chcvt->sw_ch_count; ++ch)
	{
		SwChMap const map = chcvt->sw_ch_map[ch];
		fmtenc(
			data[map.sw_ch_idx], 
			(byte_t*)buffer + chcvt->hw_bps * (map.hw_ch_idx + chcvt->hw_ch_count * offset),
			chcvt->hw_ch_count, frames);
	}
}

//--------------------------------------------------
bool_t audio_stream_map_channels(AudioStream stream, AudioChannel* ch_map, uint32_t ch_count)
{
	if (ch_count && null == ch_map)
		return false;

	return chcvt_map_channels(&stream->chcvt, ch_map, ch_count, stream->afmt.channel_mask);
}

//--------------------------------------------------
void audio_stream_format(AudioStream stream, AudioFormat* format)
{
	if (format)
		*format = stream->afmt;
}

#endif