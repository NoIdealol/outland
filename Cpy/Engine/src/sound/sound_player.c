#include "pch.h"
#include "sound_common.h"
#include "sound_player.h"

#define SOUND_MIX_CHUNK_POWER 4
STATIC_ASSERT((1 << SOUND_MIX_CHUNK_POWER) == SOUND_MIX_CHUNK);

#if ARCH_X86 || ARCH_X64
#include <immintrin.h>
static void sound_player_mix_mono_i16_simd4(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
static void sound_player_mix_mono_i16_simd8(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
#endif
static void sound_player_mix_mono_i16_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
static void sound_player_mix_mono_f32_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);

#if ARCH_X86 || ARCH_X64
static struct SoundSubmixVtbl const g_player_i16_simd8_vtbl =
{
	.mix = sound_player_mix_mono_i16_simd8,
};

static struct SoundSubmixVtbl const g_player_i16_simd4_vtbl =
{
	.mix = sound_player_mix_mono_i16_simd4,
};
#endif
static struct SoundSubmixVtbl const g_player_i16_scalar_vtbl =
{
	.mix = sound_player_mix_mono_i16_scalar,
};

static struct SoundSubmixVtbl const g_player_f32_scalar_vtbl =
{
	.mix = sound_player_mix_mono_f32_scalar,
};

static SoundSubmixVtbl g_player_vtbl = &g_player_i16_scalar_vtbl;


struct SoundBuffer
{
	SoundBufferFormat format;
	void*		samples; //align to mix chunk
	uint32_t	frames;
};

struct SoundPlayer
{
	struct SoundSubmix	base;

	SoundBuffer 		buffers[SOUND_PLAYER_QUEUE_SIZE];
	int32_t				offsets[SOUND_PLAYER_QUEUE_SIZE];
	uint32_t			buffer_idx;
	uint32_t			buffer_end;
	uint32_t			buffer_frame;
	uint32_t			underflow;
};

static size_t format_to_size(SoundBufferFormat format)
{
	switch (format)
	{
	case SOUND_BUFFER_FORMAT_INT16:
		return sizeof(int16_t);
	case SOUND_BUFFER_FORMAT_FLT32:
		return sizeof(flt32_t);
	default:
		return 0;
	}
}


static size_t sound_buffer_struct(SoundBufferCreateInfo const* info, size_t (*offsets)[2])
{
	size_t aligns[2];
	size_t sizes[2];
	aligns[0] = alignof(struct SoundBuffer);
	aligns[1] = SOUND_MIN_ALIGN;
	sizes[0] = sizeof(struct SoundBuffer);
	sizes[1] = m_align_u32(info->frames, SOUND_MIX_CHUNK) * format_to_size(info->format);
	return mem_struct(*offsets, aligns, sizes, 2);
}

size_t sound_buffer_size(SoundBufferCreateInfo const* info)
{
	size_t offs[2];
	return sound_buffer_struct(info, &offs);
}

SoundBuffer sound_buffer_create(SoundBufferCreateInfo const* info, void* memory)
{
	size_t offs[2];
	size_t size = sound_buffer_struct(info, &offs);
	memset(memory, 0, size);

	SoundBuffer buffer = mem_offset(memory, offs[0]);
	buffer->format = info->format;
	buffer->frames = m_align_u32(info->frames, SOUND_MIX_CHUNK);
	buffer->samples = mem_offset(memory, offs[1]);

	return buffer;
}

void			sound_buffer_info(SoundBuffer buffer, SoundBufferCreateInfo* info)
{
	info->format = buffer->format;
	info->frames = buffer->frames;
}

void*			sound_buffer_data(SoundBuffer buffer, uint32_t offset)
{
	return mem_offset(buffer->samples, format_to_size(buffer->format) * offset);
}

static void sound_buffer_copy_int16_to_int16(void* dst, void const* src, uint32_t frames)
{
	memcpy(dst, src, frames * sizeof(int16_t));
}

static void sound_buffer_copy_flt32_to_flt32(void* dst, void const* src, uint32_t frames)
{
	memcpy(dst, src, frames * sizeof(flt32_t));
}

static void sound_buffer_copy_int16_to_flt32(void* dst, void const* src, uint32_t frames)
{
	int16_t const* int16 = src;
	flt32_t* flt32 = dst;
	flt32_t const imul = 1.0f / 32768.0f;

	while (frames)
	{
		*flt32 = ((flt32_t)*int16) * imul;
		++flt32;
		++int16;
		--frames;
	}
}

static void sound_buffer_copy_flt32_to_int16(void* dst, void const* src, uint32_t frames)
{
	flt32_t const* flt32 = src;
	int16_t* int16 = dst;
	flt32_t const mul = 32767.0f;

	while (frames)
	{
		*int16 = (int16_t)(*flt32 * mul);
		++flt32;
		++int16;
		--frames;
	}
}

typedef void SoundBufferCopy(void* dst, void const* src, uint32_t frames);

void			sound_buffer_copy(SoundBuffer buffer, SoundBufferFormat format, void const* data, uint32_t frames, uint32_t offset)
{
	void* samples = mem_offset(buffer->samples, offset * format_to_size(buffer->format));

	SoundBufferCopy* const copy[2][2] =
	{
		{ sound_buffer_copy_int16_to_int16, sound_buffer_copy_flt32_to_int16 },
		{ sound_buffer_copy_int16_to_flt32, sound_buffer_copy_flt32_to_flt32 },
	};

	copy[buffer->format][format](samples, data, frames);
}

void sound_player_init()
{
#if ARCH_X86 || ARCH_X64
	uint32_t const os = arch_os();
	uint32_t const hw = arch_hw();

	uint32_t const simd4 = ARCH_HW_SSE | ARCH_HW_SSE2;
	if (ARCH_OS_SSE & os && simd4 == (simd4 & hw))
	{
		g_player_vtbl = &g_player_i16_simd4_vtbl;
	}

	uint32_t const simd8 = ARCH_HW_AVX | ARCH_HW_AVX2;
	if (ARCH_OS_AVX & os && simd8 == (simd8 & hw))
	{
		//g_player_vtbl = &g_player_i16_simd8_vtbl;
	}
#endif
}

size_t sound_player_size()
{
	return m_align_size(sizeof(struct SoundPlayer), SOUND_MIN_ALIGN);
}

SoundPlayer sound_player_create(void* memory)
{
	SoundPlayer player = memory;
	memset(player, 0, sizeof(struct SoundPlayer));
	player->base.vtbl = g_player_vtbl;

	return player;
}

bool_t sound_player_play(SoundPlayer player, SoundBuffer buffer, int32_t offset)
{
	uint32_t queue = sound_player_queue(player);
	if (SOUND_PLAYER_QUEUE_SIZE == queue)
		return false;

	if (0 == queue)
	{
		switch (buffer->format)
		{
		case SOUND_BUFFER_FORMAT_INT16:
			player->base.vtbl = g_player_vtbl;
			break;
		case SOUND_BUFFER_FORMAT_FLT32:
			player->base.vtbl = &g_player_f32_scalar_vtbl;
			break;
		default:
			return false;
		}
	}
	else
	{
		if (buffer->format != player->buffers[player->buffer_idx & (SOUND_PLAYER_QUEUE_SIZE - 1)]->format)
			return false;
	}

	if (0 > offset)
	{
		offset = -(int32_t)m_min_u32(m_align_u32(-offset, SOUND_MIX_CHUNK) >> SOUND_MIX_CHUNK_POWER, buffer->frames);
	}
	else
	{
		offset = (int32_t)m_min_u32(m_align_u32(offset, SOUND_MIX_CHUNK) >> SOUND_MIX_CHUNK_POWER, buffer->frames);
	}

	player->underflow = 0;
	player->buffers[player->buffer_end & (SOUND_PLAYER_QUEUE_SIZE - 1)] = buffer;
	player->offsets[player->buffer_end & (SOUND_PLAYER_QUEUE_SIZE - 1)] = offset;
	++player->buffer_end;
	return true;
}

void sound_player_stop(SoundPlayer player)
{
	player->buffer_idx = player->buffer_end;
	player->buffer_frame = 0;
}

uint32_t sound_player_underflow(SoundPlayer player)
{
	return player->underflow;
}

uint32_t sound_player_queue(SoundPlayer player)
{
	return player->buffer_end - player->buffer_idx;
}

SoundSubmix sound_player_submix(SoundPlayer player)
{
	return &player->base;
}

static void const* sound_player_beg_buffer(SoundPlayer player, uint32_t* frames, flt32_t** dst, uint32_t* nchunks)
{
	if (player->buffer_idx == player->buffer_end)
	{
		player->underflow += *nchunks * SOUND_MIX_CHUNK;
		memset(*dst, 0, *nchunks * SOUND_MIX_CHUNK * sizeof(flt32_t));
		return null;
	}

	if (0 == *nchunks)
		return null;

	SoundBuffer buffer = player->buffers[player->buffer_idx & (SOUND_PLAYER_QUEUE_SIZE - 1)];
	int32_t* offset = &player->offsets[player->buffer_idx & (SOUND_PLAYER_QUEUE_SIZE - 1)];

	if (0 == player->buffer_frame)
	{
		if (0 > *offset)
		{
			uint32_t skip_chunks = m_min_u32(-*offset, *nchunks);
			memset(*dst, 0, skip_chunks * SOUND_MIX_CHUNK * sizeof(flt32_t));
			*dst += skip_chunks * SOUND_MIX_CHUNK;
			*offset += skip_chunks;
			*nchunks -= skip_chunks;
			if (0 == *nchunks)
				return null;
		}
		else
		{
			player->buffer_frame = *offset * SOUND_MIX_CHUNK;
		}
	}

	*frames = buffer->frames - player->buffer_frame;
	return mem_offset(buffer->samples,format_to_size(buffer->format) * player->buffer_frame);
}

static void sound_player_end_buffer(SoundPlayer player, uint32_t frames)
{
	SoundBuffer buffer = player->buffers[player->buffer_idx & (SOUND_PLAYER_QUEUE_SIZE - 1)];
	player->buffer_frame += frames;
	if (buffer->frames == player->buffer_frame)
	{
		++player->buffer_idx;
		player->buffer_frame = 0;
	}
}

#include "clock.h"
#include <stdio.h>
void __stdcall OutputDebugStringA(char const*);

static void do_measure(os_clock_t c1, os_clock_t c2, uint32_t nchunks)
{
	static uint32_t cnc = 0;
	static int cnt = 0;
	static os_clock_t clk = 0;

	clk += c2 - c1;
	cnc += nchunks;
	++cnt;
	if (256 == cnt)
	{
		clk = os_clock_to(clk, 100000000000);
		clk /= cnc;

		char cbuff[256];
		snprintf(cbuff, sizeof(cbuff), "time_100ns: %lli \n", clk);
		//OutputDebugStringA(cbuff);

		cnc = 0;
		cnt = 0;
		clk = 0;
	}
}

static void sound_player_mix_mono_f32_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundPlayer player = (SoundPlayer)submix;

	flt32_t* dst = data[0];
	uint32_t frames = 0;
	flt32_t const* samples = null;

	while (samples = sound_player_beg_buffer(player, &frames, &dst, &nchunks))
	{
		uint32_t const do_nchunks = m_min_u32(frames >> SOUND_MIX_CHUNK_POWER, nchunks);
		uint32_t const do_frames = do_nchunks * SOUND_MIX_CHUNK;
		memcpy(dst, samples, do_frames * sizeof(flt32_t));
		dst += do_frames;
		samples += do_frames;
		nchunks -= do_nchunks;

		sound_player_end_buffer(player, do_frames);
	}
}

static void sound_player_mix_mono_i16_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundPlayer player = (SoundPlayer)submix;

	flt32_t* dst = data[0];
	uint32_t frames = 0;
	int16_t const* samples = null;

	os_clock_t c1 = os_clock();
	uint32_t cnchunks = nchunks;

	while (samples = sound_player_beg_buffer(player, &frames, &dst, &nchunks))
	{
		uint32_t frame = 0;
		while (nchunks && frame < frames)
		{
			flt32_t const imul = 1.0f / 32768.0f;

			dst[0] = samples[0] * imul;
			dst[1] = samples[1] * imul;
			dst[2] = samples[2] * imul;
			dst[3] = samples[3] * imul;
			dst[4] = samples[4] * imul;
			dst[5] = samples[5] * imul;
			dst[6] = samples[6] * imul;
			dst[7] = samples[7] * imul;
			dst[8] = samples[8] * imul;
			dst[9] = samples[9] * imul;
			dst[10] = samples[10] * imul;
			dst[11] = samples[11] * imul;
			dst[12] = samples[12] * imul;
			dst[13] = samples[13] * imul;
			dst[14] = samples[14] * imul;
			dst[15] = samples[15] * imul;

			dst += SOUND_MIX_CHUNK;
			frame += SOUND_MIX_CHUNK;
			samples += SOUND_MIX_CHUNK;

			--nchunks;
		}

		sound_player_end_buffer(player, frame);
	}

	os_clock_t c2 = os_clock();
	do_measure(c1, c2, cnchunks);
}

static void sound_player_mix_mono_i16_simd4(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundPlayer player = (SoundPlayer)submix;

	flt32_t* dst = data[0];
	uint32_t frames = 0;
	int16_t const* samples = null;

	os_clock_t c1 = os_clock();
	uint32_t cnchunks = nchunks;

	while (samples = sound_player_beg_buffer(player, &frames, &dst, &nchunks))
	{
		uint32_t frame = 0;
		while (nchunks && frame < frames)
		{
			__m128i vec_src1 = _mm_load_si128((__m128i const*)(samples));
			__m128i vec_src2 = _mm_load_si128((__m128i const*)(samples + 8));
			//__m128i vec_src1 = _mm_set_epi16(samples[7], samples[6], samples[5], samples[4], samples[3], samples[2], samples[1], samples[0]);
			//__m128i vec_src2 = _mm_set_epi16(samples[15], samples[14], samples[13], samples[12], samples[11], samples[10], samples[9], samples[8]);
			__m128i vec_sign1 = _mm_cmpgt_epi16(_mm_setzero_si128(), vec_src1);
			__m128i vec_sign2 = _mm_cmpgt_epi16(_mm_setzero_si128(), vec_src2);

			__m128 vec_lo1 = _mm_cvtepi32_ps(_mm_unpacklo_epi16(vec_src1, vec_sign1));
			__m128 vec_hi1 = _mm_cvtepi32_ps(_mm_unpackhi_epi16(vec_src1, vec_sign1));
			__m128 vec_lo2 = _mm_cvtepi32_ps(_mm_unpacklo_epi16(vec_src2, vec_sign2));
			__m128 vec_hi2 = _mm_cvtepi32_ps(_mm_unpackhi_epi16(vec_src2, vec_sign2));
			__m128 const vec_imul = _mm_set1_ps(1.0f / 32768.0f);

			_mm_store_ps(dst + 0, _mm_mul_ps(vec_lo1, vec_imul));
			_mm_store_ps(dst + 4, _mm_mul_ps(vec_hi1, vec_imul));
			_mm_store_ps(dst + 8, _mm_mul_ps(vec_lo2, vec_imul));
			_mm_store_ps(dst + 12, _mm_mul_ps(vec_hi2, vec_imul));

			dst += SOUND_MIX_CHUNK;
			frame += SOUND_MIX_CHUNK;
			samples += SOUND_MIX_CHUNK;
			--nchunks;
		}

		sound_player_end_buffer(player, frame);
	}

	os_clock_t c2 = os_clock();
	do_measure(c1, c2, cnchunks);
}

static void sound_player_mix_mono_i16_simd8(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundPlayer player = (SoundPlayer)submix;

	flt32_t* dst = data[0];
	uint32_t frames = 0;
	int16_t const* samples = null;

	while (samples = sound_player_beg_buffer(player, &frames, &dst, &nchunks))
	{
		uint32_t frame = 0;
		while (nchunks && frame < frames)
		{
			__m256i vec_src = _mm256_set_epi16(
				samples[15], samples[14], samples[13], samples[12],
				samples[7], samples[6], samples[5], samples[4],
				samples[11], samples[10], samples[9], samples[8],
				samples[3], samples[2], samples[1], samples[0]);
			__m256i vec_sign = _mm256_cmpgt_epi16(_mm256_setzero_si256(), vec_src);

			__m256 vec_lo = _mm256_cvtepi32_ps(_mm256_unpacklo_epi16(vec_src, vec_sign));
			__m256 vec_hi = _mm256_cvtepi32_ps(_mm256_unpackhi_epi16(vec_src, vec_sign));
			__m256 const vec_imul = _mm256_set1_ps(1.0f / 32768.0f);

			_mm256_store_ps(dst + 0, _mm256_mul_ps(vec_lo, vec_imul));
			_mm256_store_ps(dst + 8, _mm256_mul_ps(vec_hi, vec_imul));

			dst += SOUND_MIX_CHUNK;
			frame += SOUND_MIX_CHUNK;
			samples += SOUND_MIX_CHUNK;
			--nchunks;
		}

		sound_player_end_buffer(player, frame);
	}
}

