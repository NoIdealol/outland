#include "pch.h"
#include "sound_buffer.h"

typedef struct WAVE_SUBFORMAT {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	unsigned char  Data4[8];
} WAVE_SUBFORMAT;

//00000003-0000-0010-8000-00aa00389b71
static WAVE_SUBFORMAT const WAVE_SUBFORMAT_IEEE_FLOAT = { 0x00000003, 0x0000, 0x0010,{ 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71 } };
//00000001-0000-0010-8000-00aa00389b71
static WAVE_SUBFORMAT const WAVE_SUBFORMAT_PCM = { 0x00000001, 0x0000, 0x0010,{ 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71 } };


typedef struct RIFF_CHUNK
{
	uint32_t	ChunkID;
	uint32_t	ChunkSize;
} RIFF_CHUNK;

typedef struct RIFF_FORMAT
{
	uint32_t	Format;
} RIFF_FORMAT;

typedef struct WAVE_FORMAT
{
	uint16_t	AudioFormat;
	uint16_t	NumChannels;
	uint32_t	SampleRate;
	uint32_t	ByteRate;
	uint16_t	BlockAlign;
	uint16_t	BitsPerSample;
} WAVE_FORMAT;

typedef struct WAVE_FORMATEX
{
	WAVE_FORMAT		Format;
	uint16_t		ExtraParamSize;
	uint16_t		ValidBitsPerSample;
	uint32_t		ChannelMask;
	WAVE_SUBFORMAT	SubFormat;
} WAVE_FORMATEX;


static SoundBuffer sound_buffer_fmt_pcm(WAVE_FORMAT const* format, void const* data, uint32_t size, Allocator allocator)
{
	if (16 != format->BitsPerSample)
		return null;
	if (0 == format->NumChannels)
		return null;

	uint32_t frames = size / format->BlockAlign;

	SoundBufferCreateInfo buffer_info;
	buffer_info.format = SOUND_BUFFER_FORMAT_INT16;
	buffer_info.frames = frames;

	size_t buffer_size = sound_buffer_size(&buffer_info);
	void* buffer_memory = mem_alloc_aligned(allocator, SOUND_MIN_ALIGN, buffer_size);
	SoundBuffer buffer = sound_buffer_create(&buffer_info, buffer_memory);
	int16_t* buffer_data = sound_buffer_data(buffer, 0);

	switch (format->NumChannels)
	{
	case 1:
	{
		memcpy(buffer_data, data, frames * sizeof(int16_t));
		break;
	}
	case 2:
	{
		int16_t* const buffer_data_end = buffer_data + frames;
		int16_t const* samples = data;

		while (buffer_data_end != buffer_data)
		{
			*buffer_data = (samples[0] + samples[1]) / 2;
			samples += 2;
			++buffer_data;
		}
		break;
	}
	default:
	{
		int16_t* const buffer_data_end = buffer_data + frames;
		int16_t const* samples = data;
		while (buffer_data_end != buffer_data)
		{
			int32_t sample = 0;
			for (int s = 0; s < format->NumChannels; ++s)
				sample += samples[s];
			samples += format->NumChannels;
			*buffer_data = (int16_t)(sample / format->NumChannels);
			++buffer_data;
		}

		break;
	}
	}

	return buffer;
}

static SoundBuffer sound_buffer_fmt_flt(WAVE_FORMAT const* format, void const* data, uint32_t size, Allocator allocator)
{
	if (32 != format->BitsPerSample)
		return null;
	if (0 == format->NumChannels)
		return null;

	uint32_t frames = size / format->BlockAlign;

	SoundBufferCreateInfo buffer_info;
	buffer_info.format = SOUND_BUFFER_FORMAT_FLT32;
	buffer_info.frames = frames;

	size_t buffer_size = sound_buffer_size(&buffer_info);
	void* buffer_memory = mem_alloc_aligned(allocator, SOUND_MIN_ALIGN, buffer_size);
	SoundBuffer buffer = sound_buffer_create(&buffer_info, buffer_memory);
	flt32_t* buffer_data = sound_buffer_data(buffer, 0);

	switch (format->NumChannels)
	{
	case 1:
	{
		memcpy(buffer_data, data, frames * sizeof(flt32_t));
		break;
	}
	case 2:
	{
		flt32_t const* src = data;
		for (uint32_t f = 0; f < frames; ++f)
		{
			buffer_data[f] = (src[f * 2] + src[f * 2 + 1]) / 2.0f;
		}
		break;
	}
	default:
	{
		float const* src = data;
		for (uint32_t f = 0; f < frames; ++f)
		{
			float s = 0;
			for (uint32_t ch = 0; ch < format->NumChannels; ++ch)
			{
				s += src[f * format->NumChannels + ch];
			}

			buffer_data[f] = s / (format->NumChannels);
		}

		break;
	}
	}

	return buffer;
}

static SoundBuffer sound_buffer_fmtex_pcm(WAVE_FORMATEX const* format, void const* data, uint32_t size, Allocator allocator)
{
	return sound_buffer_fmt_pcm(&format->Format, data, size, allocator);
}

static SoundBuffer sound_buffer_fmtex_flt(WAVE_FORMATEX const* format, void const* data, uint32_t size, Allocator allocator)
{
	if (32 != format->Format.BitsPerSample)
		return null;
	if (32 != format->ValidBitsPerSample)
		return null;
	if (0 == format->Format.NumChannels)
		return null;

	uint32_t frames = size / format->Format.BlockAlign;

	SoundBufferCreateInfo buffer_info;
	buffer_info.format = SOUND_BUFFER_FORMAT_FLT32;
	buffer_info.frames = frames;

	size_t buffer_size = sound_buffer_size(&buffer_info);
	void* buffer_memory = mem_alloc_aligned(allocator, SOUND_MIN_ALIGN, buffer_size);
	SoundBuffer buffer = sound_buffer_create(&buffer_info, buffer_memory);
	flt32_t* buffer_data = sound_buffer_data(buffer, 0);

	switch (format->Format.NumChannels)
	{
	case 1:
	{
		memcpy(buffer_data, data, frames * sizeof(flt32_t));
		break;
	}
	case 2:
	{
		flt32_t* const buffer_data_end = buffer_data + frames;
		flt32_t const* samples = data;

		while (buffer_data_end != buffer_data)
		{
			*buffer_data = (samples[0] + samples[1]) * 0.5f;
			samples += 2;
			++buffer_data;
		}
		break;
	}
	default:
	{
		flt32_t* const buffer_data_end = buffer_data + frames;
		flt32_t const* samples = data;
		flt32_t const inv_nchannels = 1.0f / format->Format.NumChannels;
		while (buffer_data_end != buffer_data)
		{
			flt32_t sample = 0;
			for (int s = 0; s < format->Format.NumChannels; ++s)
				sample += samples[s];
			samples += format->Format.NumChannels;
			*buffer_data = sample * inv_nchannels;
			++buffer_data;
		}
		break;
	}
	}

	return buffer;
}

SoundBuffer sound_buffer_wav(void* file, size_t size, Allocator allocator)
{
	if (mem_align_offset(file, 4))
		return null;

	//whole file must be aligned
	/*if (m_align_size(size, 4) != size)
		return null;*/

	//RIFF header
	if (sizeof(RIFF_CHUNK) + sizeof(RIFF_FORMAT) > size)
		return null;

	RIFF_CHUNK* riff_chunk = file;
	if ('FFIR' != riff_chunk->ChunkID)
		return null;

	if (riff_chunk->ChunkSize != size - sizeof(RIFF_CHUNK))
		return null;

	RIFF_FORMAT const* riff_format = mem_offset(file, sizeof(RIFF_CHUNK));
	if ('EVAW' != riff_format->Format)
		return null;

	RIFF_CHUNK* fmt_chunk = null;
	RIFF_CHUNK* data_chunk = null;

	size_t offset = sizeof(RIFF_CHUNK) + sizeof(RIFF_FORMAT);
	//find the needed chunks
	while (offset < size)
	{
		if (sizeof(RIFF_CHUNK) > size - offset)
			return null;

		RIFF_CHUNK* chunk = mem_offset(file, offset);
		offset += sizeof(RIFF_CHUNK);

		if (chunk->ChunkSize > size - offset)
			return null;

		offset += m_align_u32(chunk->ChunkSize, 4);

		if (' tmf' == chunk->ChunkID)
		{
			//duplicit chunk
			if (fmt_chunk)
				return null;
			fmt_chunk = chunk;
		}

		if ('atad' == chunk->ChunkID)
		{
			//duplicit chunk
			if (data_chunk)
				return null;
			data_chunk = chunk;
		}
	}

	//this should be impossible
	/*if (offset != size)
	{
		assert(false);
		return null;
	}*/

	if (null == fmt_chunk || null == data_chunk)
		return null;

	if (sizeof(WAVE_FORMAT) > fmt_chunk->ChunkSize)
		return null;

	void const* data = mem_offset(data_chunk, sizeof(RIFF_CHUNK));
	WAVE_FORMAT* fmt = mem_offset(fmt_chunk, sizeof(RIFF_CHUNK));

	switch (fmt->AudioFormat)
	{
	case 1: //PCM
	{
		return sound_buffer_fmt_pcm(fmt, data, data_chunk->ChunkSize, allocator);
	}
	case 3: //IEEE FLOAT
	{
		return sound_buffer_fmt_flt(fmt, data, data_chunk->ChunkSize, allocator);
	}
	case 0xFFFE: //EXTENSIBLE
	{
		if (sizeof(WAVE_FORMATEX) > fmt_chunk->ChunkSize)
			return null;

		WAVE_FORMATEX* fmtex = mem_offset(fmt_chunk, sizeof(RIFF_CHUNK));
		if (sizeof(WAVE_FORMATEX) - sizeof(WAVE_FORMAT) - sizeof(fmtex->ExtraParamSize) != fmtex->ExtraParamSize)
			return null;

		if (0 == memcmp(&WAVE_SUBFORMAT_PCM, &fmtex->SubFormat, sizeof(WAVE_SUBFORMAT)))
		{
			return sound_buffer_fmtex_pcm(fmtex, data, data_chunk->ChunkSize, allocator);
		}

		if (0 == memcmp(&WAVE_SUBFORMAT_IEEE_FLOAT, &fmtex->SubFormat, sizeof(WAVE_SUBFORMAT)))
		{
			return sound_buffer_fmtex_flt(fmtex, data, data_chunk->ChunkSize, allocator);
		}

		return null;
	}
	default:
		return null;
	}
}


typedef struct WAVE_FILE
{
	RIFF_CHUNK		riff_chunk;
	RIFF_FORMAT		riff_format;
	RIFF_CHUNK		wave_chunk;
	WAVE_FORMATEX	wave_format;
	RIFF_CHUNK		data_chunk;
} WAVE_FILE;

void* save_sound_buffer_wav(float* data, uint32_t frames, uint32_t sample_rate, size_t* size, Allocator allocator)
{
	*size = sizeof(WAVE_FILE) + sizeof(float) * frames;
	WAVE_FILE* wave_file = mem_alloc(allocator, *size);
	wave_file->riff_chunk.ChunkID = 'FFIR';
	wave_file->riff_chunk.ChunkSize = sizeof(WAVE_FILE) - sizeof(RIFF_CHUNK) + sizeof(float) * frames;
	wave_file->riff_format.Format = 'EVAW';
	wave_file->wave_chunk.ChunkID = ' tmf';
	wave_file->wave_chunk.ChunkSize = sizeof(WAVE_FORMATEX);
	wave_file->wave_format.ChannelMask = 0x4; //front center/mono
	wave_file->wave_format.SubFormat = WAVE_SUBFORMAT_IEEE_FLOAT;
	wave_file->wave_format.ValidBitsPerSample = sizeof(float) * 8;
	wave_file->wave_format.ExtraParamSize = sizeof(WAVE_FORMATEX) - sizeof(WAVE_FORMAT) - sizeof(wave_file->wave_format.ExtraParamSize);
	wave_file->wave_format.Format.AudioFormat = 0xFFFE;
	wave_file->wave_format.Format.BitsPerSample = sizeof(float) * 8;
	wave_file->wave_format.Format.NumChannels = 1;
	wave_file->wave_format.Format.BlockAlign = sizeof(float) * wave_file->wave_format.Format.NumChannels;
	wave_file->wave_format.Format.SampleRate = sample_rate;
	wave_file->wave_format.Format.ByteRate = wave_file->wave_format.Format.BlockAlign * wave_file->wave_format.Format.SampleRate;
	wave_file->data_chunk.ChunkID = 'atad';
	wave_file->data_chunk.ChunkSize = sizeof(float) * frames;
	memcpy(mem_offset(wave_file, sizeof(WAVE_FILE)), data, sizeof(float) * frames);

	return wave_file;
}