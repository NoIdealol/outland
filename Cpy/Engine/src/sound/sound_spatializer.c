#include "pch.h"
#include "sound_common.h"
#include "sound_spatializer.h"

#define SOUND_EAR_BUFFER 2 * SOUND_MIX_CHUNK
#define SOUND_EAR_DELAY 13.5f

static void sound_spatializer_mix(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);

static struct SoundSubmixVtbl const g_spatializer_vtbl = 
{
	.mix = sound_spatializer_mix,
};

typedef struct HeadFilter
{
	flt32_t x;
	flt32_t y;
} HeadFilter;

struct SoundSpatializer
{
	struct SoundSubmix base;

	SoundSubmix	source;
	flt32_t		buffer[SOUND_EAR_BUFFER];
	HeadFilter	filter_l;
	HeadFilter	filter_r;
	flt32_t		src_position[3];
	flt32_t		src_volume;
	flt32_t		dst_position[3];
	flt32_t		dst_volume;
	flt32_t		dst_time;
};

typedef struct StereoState
{
	flt32_t delay_l;
	flt32_t delay_r;
	flt32_t volume_l;
	flt32_t volume_r;
	flt32_t filter_l;
	flt32_t filter_r;
} StereoState;



static void stereo_mix(flt32_t* dst, flt32_t const* src, uint32_t nchunks,
	flt32_t delay_src, 
	flt32_t delay_dst, 
	flt32_t volume_src, 
	flt32_t volume_dst,
	flt32_t filter_src,
	flt32_t filter_dst,
	HeadFilter* filter)
{
	uint32_t const samples = SOUND_MIX_CHUNK * nchunks;
	flt32_t const mix_samples = samples -(delay_dst - delay_src) * SOUND_EAR_DELAY;
	flt32_t const inv_samples = 1.0f / samples;
	flt32_t const in_per_out = mix_samples * inv_samples;
	flt32_t const vol_per_out = (volume_dst - volume_src) * inv_samples;
	flt32_t position = SOUND_EAR_DELAY - delay_src * SOUND_EAR_DELAY;
	flt32_t volume = volume_src;

	flt32_t const c = 343.f; //speed of sound
	flt32_t const a = 0.105f; //avg head radius
	flt32_t const w0 = c / a;
	flt32_t const Fs = SOUND_FREQUENCY;
	flt32_t const wf = w0 / Fs;
	flt32_t const inv_Z = 1.0f / (1 + wf);
	//flt32_t  const A0 = 1;
	flt32_t const A1 = -(1 - wf) * inv_Z;
	flt32_t B0 = (filter_src + wf) * inv_Z;
	flt32_t B1 = (-filter_src + wf) * inv_Z;
	flt32_t const B0_per_out = ((filter_dst + wf) * inv_Z - B0) * inv_samples;
	flt32_t const B1_per_out = ((-filter_dst + wf) * inv_Z - B1) * inv_samples;
	
	for (uint32_t s = 0; s < samples; ++s)
	{
		int32_t const sample = (int32_t)position;
		flt32_t const lerp = position - (flt32_t)sample;
		flt32_t const x = ((src[sample] * (1.0f - lerp) + src[sample + 1] * lerp) * volume);
		flt32_t const y = B0 * x + B1 * filter->x - A1 * filter->y;
		dst[s] = y;
		filter->x = x;
		filter->y = y;

		position += in_per_out;
		volume += vol_per_out;
		B0 += B0_per_out;
		B1 += B1_per_out;
	}


	/*for (int s = 0; s < SOUND_MIX_CHUNK; ++s)
	{
	int32_t sample = (int32_t)position;
	flt32_t lerp = position - (flt32_t)sample;

	flt32_t d0 = (src[sample + 2] - src[sample]) * 0.5f;
	flt32_t d1 = (src[sample + 3] - src[sample + 1]) * 0.5f;
	flt32_t y0 = src[sample + 1];
	flt32_t y1 = src[sample + 2];

	flt32_t a0 = y0;
	flt32_t a1 = d0;
	flt32_t a3 = d1 - d0 - 2 * (y1 - a0 - a1);
	flt32_t a2 = y1 - a0 - a1 - a3;
	flt32_t x = lerp;
	flt32_t x2 = x * x;
	flt32_t x3 = x2 * x;

	dst[s] = (a0 + a1 * x + a2 * x2 + a3 * x3) * volume;

	position += in_per_out;
	volume += vol_per_out;
	}*/
}


/*static void stereo_state_2D(StereoState* state, flt32_t* src, flt32_t* dst, flt32_t* dir)
{
	flt32_t pos[2];
	pos[0] = src[0] - dst[0];
	pos[1] = src[1] - dst[1];

	flt32_t dist = sqrtf(pos[0] * pos[0] + pos[1] * pos[1]);
	flt32_t idist = 1.0f / dist;
	//normalize
	pos[0] *= idist;
	pos[1] *= idist;

	flt32_t ear[2];
	ear[0] = dir[1];
	ear[1] = -dir[0];

	flt32_t dot = ear[0] * pos[0] + ear[1] * pos[1];

	state->delay_r = -dot;
	state->delay_l = dot;
	state->volume_r = 0.5f + 0.5f * dot;
	state->volume_l = 0.5f - 0.5f * dot;
}*/

static void stereo_state_3D(StereoState* state, flt32_t const* pos, flt32_t vol)
{
	flt32_t const dist = m_max_f32(m_sqrt_f32(pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]), 0.1f);
	flt32_t const idist = 1.0f / dist;

	flt32_t const dot = pos[1] * idist;
	assert(1 >= dot && -1 <= dot);
	//1/dist sound attenuation
	vol *= m_min_f32(idist, 1.0f);
	//hack to fake back sounds
	//vol *= 1.0f + 0.4f * m_max_f32(m_min_f32(pos[0] * idist, 0.0f), -1.0f);
	//flt32_t const back = 1.0f + 0.9f * m_max_f32(m_min_f32(pos[0] * idist, 0.0f), -1.0f);

	/*flt32_t const angle_l = m_acos_f32(pos[1] * idist);// * (pos[1] >= 0 ? 1 : -1);
	flt32_t const angle_r = m_acos_f32(-pos[1] * idist);// * (pos[1] >= 0 ? 1 : -1);
	flt32_t const pi = 3.1415926f;*/

	state->filter_l = 1.05f + 0.95f * (pos[0] * 0.2588f + pos[1] * 0.9659f) * idist;
	state->filter_r = 1.05f + 0.95f * (pos[0] * 0.2588f - pos[1] * 0.9659f) * idist;
	//state->factor_l = 1.0f + 0.9f * m_max_f32(m_min_f32((pos[0] * 0.707f + pos[1] * 0.707f) * idist, 0.0f), -1.0f);
	//state->factor_r = 1.0f + 0.9f * m_max_f32(m_min_f32((pos[0] * 0.707f - pos[1] * 0.707f) * idist, 0.0f), -1.0f);

	state->delay_r = dot * m_abs_f32(dot);
	state->delay_l = -dot * m_abs_f32(dot);
	//state->volume_r = m_sqrt_f32((0.5f - 0.3f * dot * m_abs_f32(dot))) * vol;
	//state->volume_l = m_sqrt_f32((0.5f + 0.3f * dot * m_abs_f32(dot))) * vol;

	//state->delay_r = 0;
	//state->delay_l = 0;
	state->volume_r = vol;
	state->volume_l = vol;
}




static void sound_spatializer_mix(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundSpatializer const ss = (SoundSpatializer)submix;
	flt32_t const dt = (flt32_t)SOUND_MIX_CHUNK / (flt32_t)SOUND_FREQUENCY;

	alignas(SOUND_MIN_ALIGN) flt32_t buffer[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX + SOUND_EAR_BUFFER + 1]; //1extra for memory access
	StereoState state_dst;
	StereoState state_src;

	memcpy(buffer, ss->buffer, sizeof(flt32_t) * SOUND_EAR_BUFFER);
	flt32_t* const p_buffer = buffer + SOUND_EAR_BUFFER;
	ss->source->vtbl->mix(ss->source, &p_buffer, nchunks);
	buffer[lengthof(buffer) - 1] = buffer[lengthof(buffer) - 2]; //1extra copy

	//src state
	stereo_state_3D(&state_src, ss->src_position, ss->src_volume);

	//advance state
	if (ss->dst_time)
	{
		flt32_t const dt_mix = m_min_f32(dt * nchunks, ss->dst_time);
		flt32_t const lerp = dt_mix / ss->dst_time;
		ss->src_position[0] += (ss->dst_position[0] - ss->src_position[0]) * lerp;
		ss->src_position[1] += (ss->dst_position[1] - ss->src_position[1]) * lerp;
		ss->src_position[2] += (ss->dst_position[2] - ss->src_position[2]) * lerp;
		//ss->src_volume += (ss->dst_volume - ss->src_volume) * lerp;
		ss->dst_time -= dt_mix;
	}
	else
	{
		memcpy(ss->src_position, ss->dst_position, 3 * sizeof(flt32_t));
	}

	ss->src_volume = ss->dst_volume;

	//dst state
	stereo_state_3D(&state_dst, ss->src_position, ss->src_volume);

	stereo_mix(data[0], buffer, nchunks, 
		state_src.delay_l, 
		state_dst.delay_l, 
		state_src.volume_l, 
		state_dst.volume_l,
		state_src.filter_l,
		state_dst.filter_l,
		&ss->filter_l);
	stereo_mix(data[1], buffer, nchunks, 
		state_src.delay_r, 
		state_dst.delay_r, 
		state_src.volume_r, 
		state_dst.volume_r,
		state_src.filter_r,
		state_dst.filter_r,
		&ss->filter_r);

	memcpy(ss->buffer, buffer + SOUND_MIX_CHUNK * nchunks, sizeof(flt32_t) * SOUND_EAR_BUFFER);
}

uint32_t			sound_spatializer_delay()
{
	return SOUND_EAR_BUFFER;
}

size_t				sound_spatializer_size()
{
	return m_align_size(sizeof(struct SoundSpatializer), SOUND_MIN_ALIGN);
}

SoundSpatializer	sound_spatializer_create(void* memory, SoundSubmix source, flt32_t const* position, flt32_t volume)
{
	SoundSpatializer spat = memory;
	memset(spat, 0, sizeof(struct SoundSpatializer));
	spat->base.vtbl = &g_spatializer_vtbl;
	spat->source = source;
	spat->src_volume = volume;
	spat->dst_volume = volume;
	memcpy(spat->src_position, position, 3 * sizeof(flt32_t));
	memcpy(spat->dst_position, position, 3 * sizeof(flt32_t));
	return spat;
}

void		sound_spatializer_position(SoundSpatializer spatializer, flt32_t const* position, flt32_t dt)
{
	memcpy(spatializer->dst_position, position, 3 * sizeof(flt32_t));
	spatializer->dst_time = dt;
}

void		sound_spatializer_volume(SoundSpatializer spatializer, flt32_t volume)
{
	spatializer->dst_volume = volume;
}

SoundSubmix sound_spatializer_submix(SoundSpatializer spatializer)
{
	return &spatializer->base;
}