#pragma once
#include "sound_submix.h"

typedef struct SoundFader* SoundFader;

size_t				sound_fader_size();
SoundFader			sound_fader_create(void* memory, SoundSubmix source);
bool_t				sound_fader_is_fading(SoundFader fader);
void				sound_fader_fadeout(SoundFader fader);
void				sound_fader_delay(SoundFader fader);
SoundSubmix			sound_fader_submix(SoundFader fader);