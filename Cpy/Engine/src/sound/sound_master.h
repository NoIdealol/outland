#pragma once
#include "sound_submix.h"

typedef struct SoundMaster* SoundMaster;

size_t			sound_master_size();
SoundMaster		sound_master_create(void* memory, SoundSubmix source);
void			sound_master_frequency(SoundMaster master, uint32_t frequency);
void			sound_master_mix(SoundMaster master, flt32_t* const* data, uint32_t samples);