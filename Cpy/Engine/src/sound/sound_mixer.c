#include "pch.h"
#include "sound_common.h"
#include "sound_mixer.h"

#if ARCH_X86 || ARCH_X64
#include <immintrin.h>
static void sound_mixer_mix_simd8(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
static void sound_mixer_mix_simd4(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
#endif
static void sound_mixer_mix_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);

#if ARCH_X86 || ARCH_X64
static struct SoundSubmixVtbl const g_mixer_simd8_vtbl =
{
	.mix = sound_mixer_mix_simd8,
};

static struct SoundSubmixVtbl const g_mixer_simd4_vtbl =
{
	.mix = sound_mixer_mix_simd4,
};
#endif
static struct SoundSubmixVtbl const g_mixer_scalar_vtbl =
{
	.mix = sound_mixer_mix_scalar,
};

static SoundSubmixVtbl g_mixer_vtbl = &g_mixer_scalar_vtbl;

void sound_mixer_init()
{
#if ARCH_X86 || ARCH_X64
	uint32_t const os = arch_os();
	uint32_t const hw = arch_hw();

	uint32_t const simd4 = ARCH_HW_SSE;
	if (ARCH_OS_SSE & os && simd4 == (simd4 & hw))
	{
		g_mixer_vtbl = &g_mixer_simd4_vtbl;
	}

	uint32_t const simd8 = ARCH_HW_AVX;
	if (ARCH_OS_AVX & os && simd8 == (simd8 & hw))
	{
		//g_mixer_vtbl = &g_mixer_simd8_vtbl;
	}
#endif
}

struct SoundMixer
{
	struct SoundSubmix base;

	SoundSubmix*	track_array;
	uint32_t		track_count;
};

static size_t sound_mixer_mem_struct(size_t* offset, SoundMixerCreateInfo* const info)
{
	size_t align[] = { alignof(struct SoundMixer), alignof(SoundSubmix) };
	size_t size[] = { sizeof(struct SoundMixer), sizeof(SoundSubmix) };
	size[1] *= info->track_count;
	return mem_struct(offset, align, size, lengthof(size));
}

size_t		sound_mixer_size(SoundMixerCreateInfo* const info)
{
	size_t offset[2];
	return m_align_size(sound_mixer_mem_struct(offset, info), SOUND_MIN_ALIGN);
}

SoundMixer	sound_mixer_create(SoundMixerCreateInfo* const info, void* memory)
{
	size_t offset[2];
	sound_mixer_mem_struct(offset, info);
	SoundMixer mixer = mem_offset(memory, offset[0]);
	mixer->base.vtbl = g_mixer_vtbl;
	mixer->track_array = mem_offset(memory, offset[1]);
	mixer->track_count = info->track_count;

	memset(mixer->track_array, 0, info->track_count * sizeof(SoundSubmix));

	return mixer;
}

void sound_mixer_track(SoundMixer mixer, SoundSubmix submix, uint32_t track)
{
	mixer->track_array[track] = submix;
}

SoundSubmix sound_mixer_submix(SoundMixer mixer)
{
	return &mixer->base;
}

#include "clock.h"
#include <stdio.h>
void __stdcall OutputDebugStringA(char const*);

static void do_measure(os_clock_t c1, os_clock_t c2, uint32_t nchunks)
{
	static uint32_t cnc = 0;
	static int cnt = 0;
	static os_clock_t clk = 0;

	clk += c2 - c1;
	cnc += nchunks;
	++cnt;
	if (256 == cnt)
	{
		clk = os_clock_to(clk, 100000000000);
		clk /= cnc;

		char cbuff[256];
		snprintf(cbuff, sizeof(cbuff), "time_100ns: %lli \n", clk);
		//OutputDebugStringA(cbuff);

		cnc = 0;
		cnt = 0;
		clk = 0;
	}
}

#if ARCH_X86 || ARCH_X64
static void sound_mixer_mix_simd8(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundMixer const mixer = (SoundMixer)submix;

	/*flt32_t* buffer_mixed_l = data[0];
	flt32_t* buffer_mixed_r = data[1];
	memset(buffer_mixed_l, 0, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));
	memset(buffer_mixed_r, 0, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));*/
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_mixed_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX] = { 0 };
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_mixed_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX] = { 0 };

	alignas(SOUND_MIN_ALIGN) flt32_t buffer_track_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_track_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];

	flt32_t* buffers[2];
	buffers[0] = buffer_track_l;
	buffers[1] = buffer_track_r;

	
	for (uint32_t t_idx = 0; t_idx < mixer->track_count; ++t_idx)
	{
		SoundSubmix track = mixer->track_array[t_idx];
		if (track)
		{
			track->vtbl->mix(track, buffers, nchunks);

			os_clock_t c1 = os_clock();

			//_mm256_zeroall();

			uint32_t const idx_end = SOUND_MIX_CHUNK * nchunks;
			for (uint32_t idx = 0; idx < idx_end; idx += SOUND_MIX_CHUNK)
			{
				__m256 vec_mixed1 = _mm256_load_ps(buffer_mixed_l + idx);
				__m256 vec_track1 = _mm256_load_ps(buffer_track_l + idx);
				__m256 vec_added1 = _mm256_add_ps(vec_mixed1, vec_track1);
				_mm256_store_ps(buffer_mixed_l + idx, vec_added1);

				__m256 vec_mixed2 = _mm256_load_ps(buffer_mixed_r + idx);
				__m256 vec_track2 = _mm256_load_ps(buffer_track_r + idx);
				__m256 vec_added2 = _mm256_add_ps(vec_mixed2, vec_track2);
				_mm256_store_ps(buffer_mixed_r + idx, vec_added2);

				__m256 vec_mixed3 = _mm256_load_ps(buffer_mixed_l + idx + 8);
				__m256 vec_track3 = _mm256_load_ps(buffer_track_l + idx + 8);
				__m256 vec_added3 = _mm256_add_ps(vec_mixed3, vec_track3);
				_mm256_store_ps(buffer_mixed_l + idx + 8, vec_added3);

				__m256 vec_mixed4 = _mm256_load_ps(buffer_mixed_r + idx + 8);
				__m256 vec_track4 = _mm256_load_ps(buffer_track_r + idx + 8);
				__m256 vec_added4 = _mm256_add_ps(vec_mixed4, vec_track4);
				_mm256_store_ps(buffer_mixed_r + idx + 8, vec_added4);
			}

			//_mm256_zeroall();
			_mm256_zeroupper();

			os_clock_t c2 = os_clock();
			do_measure(c1, c2, nchunks - 1);
		}
	}

	memcpy(data[0], buffer_mixed_l, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));
	memcpy(data[1], buffer_mixed_r, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));

	//_mm256_zeroall();
	//_mm256_zeroupper();
}

static void sound_mixer_mix_simd4(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundMixer const mixer = (SoundMixer)submix;

	/*flt32_t* buffer_mixed_l = data[0];
	flt32_t* buffer_mixed_r = data[1];
	memset(buffer_mixed_l, 0, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));
	memset(buffer_mixed_r, 0, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));*/
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_mixed_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX] = { 0 };
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_mixed_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX] = { 0 };

	alignas(SOUND_MIN_ALIGN) flt32_t buffer_track_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_track_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];

	flt32_t* buffers[2];
	buffers[0] = buffer_track_l;
	buffers[1] = buffer_track_r;

	for (uint32_t t_idx = 0; t_idx < mixer->track_count; ++t_idx)
	{
		SoundSubmix track = mixer->track_array[t_idx];
		if (track)
		{
			track->vtbl->mix(track, buffers, nchunks);

			os_clock_t c1 = os_clock();

			uint32_t const idx_end = SOUND_MIX_CHUNK * nchunks;
			for (uint32_t idx = 0; idx < idx_end; idx += SOUND_MIX_CHUNK)
			{
				__m128 vec_mixed1 = _mm_load_ps(buffer_mixed_l + idx);
				__m128 vec_track1 = _mm_load_ps(buffer_track_l + idx);
				__m128 vec_added1 = _mm_add_ps(vec_mixed1, vec_track1);
				_mm_store_ps(buffer_mixed_l + idx, vec_added1);

				__m128 vec_mixed2 = _mm_load_ps(buffer_mixed_r + idx);
				__m128 vec_track2 = _mm_load_ps(buffer_track_r + idx);
				__m128 vec_added2 = _mm_add_ps(vec_mixed2, vec_track2);
				_mm_store_ps(buffer_mixed_r + idx, vec_added2);

				__m128 vec_mixed3 = _mm_load_ps(buffer_mixed_l + idx + 4);
				__m128 vec_track3 = _mm_load_ps(buffer_track_l + idx + 4);
				__m128 vec_added3 = _mm_add_ps(vec_mixed3, vec_track3);
				_mm_store_ps(buffer_mixed_l + idx + 4, vec_added3);

				__m128 vec_mixed4 = _mm_load_ps(buffer_mixed_r + idx + 4);
				__m128 vec_track4 = _mm_load_ps(buffer_track_r + idx + 4);
				__m128 vec_added4 = _mm_add_ps(vec_mixed4, vec_track4);
				_mm_store_ps(buffer_mixed_r + idx + 4, vec_added4);

				__m128 vec_mixed5 = _mm_load_ps(buffer_mixed_l + idx + 8);
				__m128 vec_track5 = _mm_load_ps(buffer_track_l + idx + 8);
				__m128 vec_added5 = _mm_add_ps(vec_mixed5, vec_track5);
				_mm_store_ps(buffer_mixed_l + idx + 8, vec_added5);

				__m128 vec_mixed6 = _mm_load_ps(buffer_mixed_r + idx + 8);
				__m128 vec_track6 = _mm_load_ps(buffer_track_r + idx + 8);
				__m128 vec_added6 = _mm_add_ps(vec_mixed6, vec_track6);
				_mm_store_ps(buffer_mixed_r + idx + 8, vec_added6);

				__m128 vec_mixed7 = _mm_load_ps(buffer_mixed_l + idx + 12);
				__m128 vec_track7 = _mm_load_ps(buffer_track_l + idx + 12);
				__m128 vec_added7 = _mm_add_ps(vec_mixed7, vec_track7);
				_mm_store_ps(buffer_mixed_l + idx + 12, vec_added7);

				__m128 vec_mixed8 = _mm_load_ps(buffer_mixed_r + idx + 12);
				__m128 vec_track8 = _mm_load_ps(buffer_track_r + idx + 12);
				__m128 vec_added8 = _mm_add_ps(vec_mixed8, vec_track8);
				_mm_store_ps(buffer_mixed_r + idx + 12, vec_added8);
			}

			os_clock_t c2 = os_clock();
			do_measure(c1, c2, nchunks);
		}
	}
	
	memcpy(data[0], buffer_mixed_l, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));
	memcpy(data[1], buffer_mixed_r, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));
}
#endif

static void sound_mixer_mix_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundMixer const mixer = (SoundMixer)submix;

	alignas(SOUND_MIN_ALIGN) flt32_t buffer_mixed_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX] = { 0 };
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_mixed_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX] = { 0 };

	alignas(SOUND_MIN_ALIGN) flt32_t buffer_track_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];
	alignas(SOUND_MIN_ALIGN) flt32_t buffer_track_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];

	flt32_t* buffers[2];
	buffers[0] = buffer_track_l;
	buffers[1] = buffer_track_r;

	

	for (uint32_t t_idx = 0; t_idx < mixer->track_count; ++t_idx)
	{
		SoundSubmix track = mixer->track_array[t_idx];
		if (track)
		{
			track->vtbl->mix(track, buffers, nchunks);

			os_clock_t c1 = os_clock();

			uint32_t const idx_end = SOUND_MIX_CHUNK * nchunks;
			for (uint32_t idx = 0; idx < idx_end; ++idx)
			{
				buffer_mixed_l[idx] += buffer_track_l[idx];
				buffer_mixed_r[idx] += buffer_track_r[idx];
			}

			os_clock_t c2 = os_clock();
			do_measure(c1, c2, nchunks);
		}
	}

	

	memcpy(data[0], buffer_mixed_l, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));
	memcpy(data[1], buffer_mixed_r, SOUND_MIX_CHUNK * nchunks * sizeof(flt32_t));

	
}