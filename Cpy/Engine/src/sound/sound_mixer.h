#pragma once
#include "sound_submix.h"

typedef struct SoundMixer* SoundMixer;

typedef struct SoundMixerCreateInfo
{
	uint32_t track_count;
} SoundMixerCreateInfo;

void			sound_mixer_init();
size_t			sound_mixer_size(SoundMixerCreateInfo* const info);
SoundMixer		sound_mixer_create(SoundMixerCreateInfo* const info, void* memory);
void			sound_mixer_track(SoundMixer mixer, SoundSubmix submix, uint32_t track);
SoundSubmix		sound_mixer_submix(SoundMixer mixer);