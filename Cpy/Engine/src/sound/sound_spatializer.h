#pragma once
#include "sound_submix.h"

typedef struct SoundSpatializer* SoundSpatializer;

uint32_t			sound_spatializer_delay();
size_t				sound_spatializer_size();
SoundSpatializer	sound_spatializer_create(void* memory, SoundSubmix source, flt32_t const* position, flt32_t volume);
void				sound_spatializer_position(SoundSpatializer spatializer, flt32_t const* position, flt32_t dt);
void				sound_spatializer_volume(SoundSpatializer spatializer, flt32_t volume);
SoundSubmix			sound_spatializer_submix(SoundSpatializer spatializer);