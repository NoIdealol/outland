#pragma once
#include "sound_submix.h"

typedef struct SoundResampler* SoundResampler;

void				sound_resampler_init();
uint32_t			sound_resampler_delay();
size_t				sound_resampler_size();
SoundResampler		sound_resampler_create(void* memory, SoundSubmix source, flt32_t speed, flt32_t const* position, flt32_t const* velocity);
void				sound_resampler_doppler(SoundResampler resampler, flt32_t const* position, flt32_t const* velocity, flt32_t dt);
void				sound_resampler_speed(SoundResampler resampler, flt32_t speed);
SoundSubmix			sound_resampler_submix(SoundResampler resampler);