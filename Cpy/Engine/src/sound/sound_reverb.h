#pragma once
#include "sound_submix.h"

typedef struct SoundReverb* SoundReverb;

void			sound_reverb_init();
size_t			sound_reverb_size();
SoundReverb		sound_reverb_create(void* memory, SoundSubmix source);
SoundSubmix		sound_reverb_submix(SoundReverb reverb);