#pragma once
#include "sound_common.h"
typedef struct SoundBuffer* SoundBuffer;

typedef enum SoundBufferFormat
{
	SOUND_BUFFER_FORMAT_INT16,
	SOUND_BUFFER_FORMAT_FLT32,
} SoundBufferFormat;

typedef struct SoundBufferCreateInfo
{
	SoundBufferFormat	format;
	uint32_t			frames; //multiple of SOUND_MIX_CHUNK
} SoundBufferCreateInfo;

size_t			sound_buffer_size(SoundBufferCreateInfo const* info);
SoundBuffer		sound_buffer_create(SoundBufferCreateInfo const* info, void* memory);
void			sound_buffer_info(SoundBuffer buffer, SoundBufferCreateInfo* info);
void*			sound_buffer_data(SoundBuffer buffer, uint32_t offset);
void			sound_buffer_copy(SoundBuffer buffer, SoundBufferFormat format, void const* data, uint32_t frames, uint32_t offset);