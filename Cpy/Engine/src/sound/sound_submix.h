#pragma once

typedef struct SoundSubmix* SoundSubmix;

typedef void SoundSubmixMix(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);

typedef struct SoundSubmixVtbl
{
	SoundSubmixMix*	mix;
} const* SoundSubmixVtbl;

struct SoundSubmix
{
	SoundSubmixVtbl vtbl;
};

