#pragma once
#include "sound_buffer.h"

typedef struct SoundEngine* SoundEngine;

enum SoundChannelPlayBit
{
	SOUND_CHANNEL_PLAY_ENQUEUE	= 0x1,
};
typedef uint32_t SoundChannelPlayFlags;

typedef struct SoundChannelPlayInfo
{
	SoundChannelPlayFlags	flags;
	SoundBuffer				buffer;
	int32_t					offset;
	flt32_t					position[3];
	flt32_t					velocity[3];
} SoundChannelPlayInfo;

typedef struct SoundChannelMoveInfo
{
	flt32_t					position[3];
	flt32_t					velocity[3];
} SoundChannelMoveInfo;


enum SoundChannelEventBit
{
	SOUND_CHANNEL_EVENT_PLAY = 0x1,
	SOUND_CHANNEL_EVENT_STOP = 0x2,
};

typedef uint32_t SoundChannelEventFlags;

typedef struct SoundChannelEvent
{
	SoundChannelEventFlags	flags;
	uint32_t				free;
} SoundChannelEvent;

void		sound_engine_epoll(SoundEngine engine, SoundChannelEvent* channel_events);
bool_t		sound_engine_play(SoundEngine engine, uint32_t channel, SoundChannelPlayInfo const* info);
void		sound_engine_move(SoundEngine engine, uint32_t channel, SoundChannelMoveInfo const* info);
void		sound_engine_stop(SoundEngine engine, uint32_t channel);


typedef struct SoundEngineCreateInfo
{
	uint32_t	channels;
} SoundEngineCreateInfo;

SoundEngine	sound_engine_create(SoundEngineCreateInfo const* info, Allocator allocator);
void		sound_engine_destroy(SoundEngine engine);
void		sound_engine_update(SoundEngine engine);