#include "pch.h"
#include "sound_common.h"
#include "sound_master.h"

struct SoundMaster
{
	alignas(SOUND_MIN_ALIGN) flt32_t		buffer_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX + SOUND_MIX_CHUNK];
	alignas(SOUND_MIN_ALIGN) flt32_t		buffer_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX + SOUND_MIX_CHUNK];
	flt32_t					in_per_out;
	flt32_t					in_offset;
	SoundSubmix				track;
};

size_t sound_master_size()
{
	return m_align_size(sizeof(struct SoundMaster), SOUND_MIN_ALIGN);
}

SoundMaster sound_master_create(void* memory, SoundSubmix source)
{
	SoundMaster master = memory;
	memset(master, 0, sizeof(struct SoundMaster));
	master->in_per_out = 1.0f;
	master->track = source;

	return master;
}

void sound_master_frequency(SoundMaster master, uint32_t frequency)
{
	master->in_per_out = (flt32_t)((flt64_t)SOUND_FREQUENCY / (flt64_t)frequency);
}

static flt32_t master_mix_sample(flt32_t const* src, int32_t sample, flt32_t lerp)
{
	flt32_t const d0 = (src[sample + 2] - src[sample]) * 0.5f;
	flt32_t const d1 = (src[sample + 3] - src[sample + 1]) * 0.5f;
	flt32_t const y0 = src[sample + 1];
	flt32_t const y1 = src[sample + 2];
	flt32_t const a0 = y0;
	flt32_t const a1 = d0;
	flt32_t const a3 = d1 - d0 - 2 * (y1 - a0 - a1);
	flt32_t const a2 = y1 - a0 - a1 - a3;
	flt32_t const x = lerp;
	flt32_t const x2 = x * x;
	flt32_t const x3 = x2 * x;

	flt32_t const value = a0 + a1 * x + a2 * x2 + a3 * x3;
	
	static flt32_t const compressor[] = { -1.0f, -0.8f, 0.0f, 0.8f, 1.0f, 1.0f };

	flt32_t const norm_index = m_min_f32(m_max_f32((value /** 0.25f*/ + 2.0f), 0.0f), 4.0f);
	int32_t const index = (int32_t)norm_index;

	return compressor[index] + (norm_index - (flt32_t)index) * (compressor[index + 1] - compressor[index]);
}

void sound_master_mix(SoundMaster master, flt32_t* const* data, uint32_t samples)
{
	flt32_t const in_per_out = master->in_per_out;
	flt32_t position = master->in_offset;
	//flt32_t volume = volume_src;

	uint32_t sample = 0;

	while (samples > sample)
	{
		if ((flt32_t)(SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX) <= position)
		{
			memcpy(master->buffer_l, master->buffer_l + SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX, SOUND_MIX_CHUNK * sizeof(flt32_t));
			memcpy(master->buffer_r, master->buffer_r + SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX, SOUND_MIX_CHUNK * sizeof(flt32_t));

			flt32_t* buffers[2];
			buffers[0] = master->buffer_l + SOUND_MIX_CHUNK;
			buffers[1] = master->buffer_r + SOUND_MIX_CHUNK;
			master->track->vtbl->mix(master->track, buffers, SOUND_NCHUNKS_MAX);
			position -= (flt32_t)(SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX);
		}

		while (samples > sample && (flt32_t)(SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX) > position)
		{
			int32_t offset = (int32_t)position;
			flt32_t lerp = position - (flt32_t)offset;
			data[0][sample] = master_mix_sample(master->buffer_l, offset, lerp);
			data[1][sample] = master_mix_sample(master->buffer_r, offset, lerp);

			position += in_per_out;
			++sample;
		}
	}

	master->in_offset = position;
}