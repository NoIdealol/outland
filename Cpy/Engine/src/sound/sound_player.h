#pragma once
#include "sound_submix.h"
#include "sound_buffer.h"

typedef struct SoundPlayer* SoundPlayer;

#define	SOUND_PLAYER_QUEUE_SIZE 4

void			sound_player_init();
size_t			sound_player_size();
SoundPlayer		sound_player_create(void* memory);
bool_t			sound_player_play(SoundPlayer player, SoundBuffer buffer, int32_t offset);
void			sound_player_stop(SoundPlayer player);
uint32_t		sound_player_underflow(SoundPlayer player);
uint32_t		sound_player_queue(SoundPlayer player);
SoundSubmix		sound_player_submix(SoundPlayer player);