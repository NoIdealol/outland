#include "pch.h"
#include "sound_common.h"
#include "sound_fader.h"

#define SOUND_FADE_NCHUNKS 4

static void sound_fader_mix_pass(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
static void sound_fader_mix_buff(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);

static struct SoundSubmixVtbl const g_fader_pass_vtbl =
{
	.mix = sound_fader_mix_pass,
};

static struct SoundSubmixVtbl const g_fader_buff_vtbl =
{
	.mix = sound_fader_mix_buff,
};

struct SoundFader
{
	struct SoundSubmix					base;
	SoundSubmix							source;
	uint32_t							chunks;
	alignas(SOUND_MIN_ALIGN) flt32_t	buffer_l[SOUND_FADE_NCHUNKS * SOUND_MIX_CHUNK];
	alignas(SOUND_MIN_ALIGN) flt32_t	buffer_r[SOUND_FADE_NCHUNKS * SOUND_MIX_CHUNK];
};

size_t				sound_fader_size()
{
	return m_align_size(sizeof(struct SoundFader), SOUND_MIN_ALIGN);
}

SoundFader			sound_fader_create(void* memory, SoundSubmix source)
{
	SoundFader fader = memory;
	memset(fader, 0, sizeof(struct SoundFader));
	fader->base.vtbl = &g_fader_pass_vtbl;
	fader->source = source;
	return fader;
}

bool_t				sound_fader_is_fading(SoundFader fader)
{
	return &g_fader_buff_vtbl == fader->base.vtbl;
}

void				sound_fader_fadeout(SoundFader fader)
{
	fader->base.vtbl = &g_fader_buff_vtbl;

	flt32_t* data[2];
	data[0] = fader->buffer_l;
	data[1] = fader->buffer_r;
	fader->source->vtbl->mix(fader->source, data, SOUND_FADE_NCHUNKS);
	fader->chunks = SOUND_FADE_NCHUNKS;

	flt32_t const step = 1.0f / (SOUND_FADE_NCHUNKS * SOUND_MIX_CHUNK);
	flt32_t vol = 1.0f;
	for (int i = 0; i < (SOUND_FADE_NCHUNKS * SOUND_MIX_CHUNK); ++i)
	{
		vol -= step;
		fader->buffer_l[i] *= vol;
		fader->buffer_r[i] *= vol;
	}
}

void				sound_fader_delay(SoundFader fader)
{
	fader->base.vtbl = &g_fader_buff_vtbl;

	flt32_t* data[2];
	data[0] = fader->buffer_l;
	data[1] = fader->buffer_r;
	fader->source->vtbl->mix(fader->source, data, SOUND_FADE_NCHUNKS);
	fader->chunks = SOUND_FADE_NCHUNKS;
}

SoundSubmix			sound_fader_submix(SoundFader fader)
{
	return &fader->base;
}

static void sound_fader_mix_pass(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundFader fader = (SoundFader)submix;
	fader->source->vtbl->mix(fader->source, data, nchunks);
}

static void sound_fader_mix_buff(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundFader fader = (SoundFader)submix;

	uint32_t const chunks = m_min_u32(nchunks, fader->chunks);
	uint32_t const samples = chunks * SOUND_MIX_CHUNK;
	memcpy(data[0], fader->buffer_l, samples * sizeof(flt32_t));
	memcpy(data[1], fader->buffer_r, samples * sizeof(flt32_t));
	fader->chunks -= chunks;
	nchunks -= chunks;

	if (0 == fader->chunks)
	{
		fader->base.vtbl = &g_fader_pass_vtbl;

		if (nchunks)
		{
			flt32_t* sub_data[2];
			sub_data[0] = data[0] + samples;
			sub_data[1] = data[1] + samples;
			fader->source->vtbl->mix(fader->source, sub_data, nchunks);
		}
	}
}