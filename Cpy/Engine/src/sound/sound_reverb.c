#include "pch.h"
#include "sound_reverb.h"
#include "sound_common.h"

#define ALLPASS_BUFFER 560
#define COMB_BUFFER 1650
#define REVERB_BUFFER 10 * SOUND_FREQUENCY / 1000

static void sound_reverb_mix(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);

static struct SoundSubmixVtbl const g_reverb_vtbl =
{
	.mix = sound_reverb_mix,
};

typedef struct AllpassFilter
{
	flt32_t		buffer[ALLPASS_BUFFER];
	uint32_t	capacity;
	uint32_t	sample;
	flt32_t		feedback;
} AllpassFilter;

typedef struct CombFilter
{
	flt32_t		buffer[COMB_BUFFER];
	flt32_t		store;
	flt32_t		damp;
	flt32_t		inv_damp;
	flt32_t		feedback;
	uint32_t	capacity;
	uint32_t	sample;
} CombFilter;

struct SoundReverb
{
	struct SoundSubmix	base;
	SoundSubmix			source;
	flt32_t				buffer[REVERB_BUFFER];
	uint32_t			sample;
	AllpassFilter		allpass[4];
	CombFilter			comb[8];
};

static flt32_t allpass_filter(AllpassFilter* filter, flt32_t input)
{
	flt32_t output = -input * filter->feedback + filter->buffer[filter->sample];
	filter->buffer[filter->sample] = input + output * filter->feedback;

	if (++filter->sample >= filter->capacity)
		filter->sample = 0;

	return output;
}

static flt32_t comb_filter(CombFilter* filter, flt32_t input)
{
	flt32_t output = filter->buffer[filter->sample];
	filter->store = output * filter->inv_damp + filter->store * filter->damp;
	filter->buffer[filter->sample] = input + filter->store * filter->feedback;

	if (++filter->sample >= filter->capacity)
		filter->sample = 0;

	return output;
}

static void sound_reverb_mix(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundReverb const reverb = (SoundReverb)submix;
	reverb->source->vtbl->mix(reverb->source, data, nchunks);

	flt32_t* buffer = data[0];

	uint32_t const samples = nchunks * SOUND_MIX_CHUNK;
	for (uint32_t s = 0; s < samples; ++s)
	{

		flt32_t input = reverb->buffer[reverb->sample] * 0.015f * 2;
		flt32_t output = 0;
		reverb->buffer[reverb->sample] = buffer[s];
		if (REVERB_BUFFER <= ++reverb->sample)
			reverb->sample = 0;

		for (uint32_t f = 0; f < lengthof(reverb->comb); ++f)
		{
			output += comb_filter(&reverb->comb[f], input);
		}

		for (uint32_t f = 0; f < lengthof(reverb->allpass); ++f)
		{
			output = allpass_filter(&reverb->allpass[f], output);
		}

		buffer[s] += output;// +buffer[s];
	}
}

void			sound_reverb_init()
{

}

size_t			sound_reverb_size()
{
	return sizeof(struct SoundReverb);
}

SoundReverb		sound_reverb_create(void* memory, SoundSubmix source)
{
	SoundReverb reverb = memory;
	memset(reverb, 0, sizeof(struct SoundReverb));
	reverb->base.vtbl = &g_reverb_vtbl;
	reverb->source = source;

	for (uint32_t f = 0; f < lengthof(reverb->allpass); ++f)
	{
		reverb->allpass[f].feedback = 0.5f;
	}
	
	reverb->allpass[0].capacity = 225 * 1;
	reverb->allpass[1].capacity = 556 * 1;
	reverb->allpass[2].capacity = 441 * 1;
	reverb->allpass[3].capacity = 341 * 1;

	for (uint32_t f = 0; f < lengthof(reverb->comb); ++f)
	{
		reverb->comb[f].feedback = 0.04f;// 0.84f;
		reverb->comb[f].damp = 0.2f;// 0.2f;
		reverb->comb[f].inv_damp = 1.0f - reverb->comb[f].damp;
	}

	reverb->comb[0].capacity = 1116;
	reverb->comb[1].capacity = 1188;
	reverb->comb[2].capacity = 1356;
	reverb->comb[3].capacity = 1277;
	reverb->comb[4].capacity = 1422;
	reverb->comb[5].capacity = 1491;
	reverb->comb[6].capacity = 1617;
	reverb->comb[7].capacity = 1557;

	return reverb;
}

SoundSubmix		sound_reverb_submix(SoundReverb reverb)
{
	return &reverb->base;
}