#include "pch.h"
#include "sound_engine.h"
#include "sound_common.h"

#include "sound_master.h"
#include "sound_mixer.h"
#include "sound_resampler.h"
#include "sound_spatializer.h"
#include "sound_player.h"
#include "sound_fader.h"

#include "audio.h"
#include "thread.h"

typedef struct SoundQueue
{
	int32_t volatile*	sync;
	int32_t				index;
} SoundQueue;

static void sound_queue_produce(SoundQueue* queue)
{
	queue->index = 0x0F & os_atomic_exchange_i32(queue->sync, 0x10 | queue->index);
}

static bool_t sound_queue_consume(SoundQueue* queue)
{
	if (0x10 & os_atomic_load_i32(queue->sync))
	{
		queue->index = 0x0F & os_atomic_exchange_i32(queue->sync, queue->index);
		return true;
	}

	return false;
}
typedef enum SoundAudioState
{
	SOUND_AUDIO_STATE_INIT,
	SOUND_AUDIO_STATE_STOP,
	SOUND_AUDIO_STATE_PLAY,
	SOUND_AUDIO_STATE_EXIT,
} SoundAudioState;

typedef struct SoundChannelCommand
{
	uint32_t				play_ncmd;
	uint32_t				stop_ncmd;
	uint32_t				move_ncmd;
	SoundChannelPlayInfo	play_info;
	SoundChannelMoveInfo	move_info;
} SoundChannelCommand;

typedef struct SoundChannelResponse
{
	uint32_t				play_ncmd;
	uint32_t				stop_ncmd;
	uint32_t				free_ncmd;
} SoundChannelResponse;

typedef struct SoundGraphCommand
{
	uint32_t	ncmd;
	bool_t		exit;
	flt32_t		master_volume;
} SoundGraphCommand;

typedef struct SoundGraphResponse
{
	SoundAudioState	state;
} SoundGraphResponse;


typedef struct SoundEngineChannel
{
	uint32_t				play_ncmd;
	uint32_t				stop_ncmd;
	uint32_t				free_ncmd;
} SoundEngineChannel;

struct SoundEngine
{
	SoundQueue					command_queue;
	SoundChannelCommand*		command_channel[3];
	SoundGraphCommand*			command_graph[3];

	SoundQueue					response_queue;
	SoundChannelResponse*		response_channel[3];
	SoundGraphResponse*			response_graph[3];

	SoundEngineChannel*			channel_array;
	uint32_t					channel_count;

	OsThread					audio_thread;
	Allocator					allocator;
	void*						graph;
};




void sound_engine_epoll(SoundEngine engine, SoundChannelEvent* channel_events)
{
	memset(channel_events, 0, engine->channel_count * sizeof(SoundChannelEvent));

	if (sound_queue_consume(&engine->response_queue))
	{
		SoundChannelResponse* response_channel = engine->response_channel[engine->response_queue.index];

		for (uint32_t ch = 0; ch < engine->channel_count; ++ch)
		{
			SoundChannelEvent* event = channel_events + ch;
			SoundChannelResponse const* data = response_channel + ch;
			SoundEngineChannel* channel = engine->channel_array + ch;
			
			if (data->play_ncmd != channel->play_ncmd)
			{
				event->flags |= SOUND_CHANNEL_EVENT_PLAY;
				channel->play_ncmd = data->play_ncmd;
			}

			if (data->stop_ncmd != channel->stop_ncmd)
			{
				event->flags |= SOUND_CHANNEL_EVENT_STOP;
				channel->stop_ncmd = data->stop_ncmd;
			}

			if (data->free_ncmd != channel->free_ncmd)
			{
				event->free = data->free_ncmd - channel->free_ncmd;
				channel->free_ncmd = data->free_ncmd;
			}			
		}
	}
}

void sound_engine_update(SoundEngine engine)
{
	SoundChannelCommand* old_command_channel = engine->command_channel[engine->command_queue.index];
	SoundGraphCommand* old_command_graph = engine->command_graph[engine->command_queue.index];
	sound_queue_produce(&engine->command_queue);
	SoundChannelCommand* new_command_channel = engine->command_channel[engine->command_queue.index];
	SoundGraphCommand* new_command_graph = engine->command_graph[engine->command_queue.index];
	memcpy(new_command_channel, old_command_channel, engine->channel_count * sizeof(SoundChannelCommand));
	memcpy(new_command_graph, old_command_graph, sizeof(SoundGraphCommand));
}

bool_t sound_engine_play(SoundEngine engine, uint32_t channel, SoundChannelPlayInfo const* info)
{
	SoundChannelCommand* command_channel = engine->command_channel[engine->command_queue.index];
	if (command_channel[channel].stop_ncmd != engine->channel_array[channel].stop_ncmd)
		return false; //stoping, cannot submit!
	if (command_channel[channel].play_ncmd != engine->channel_array[channel].play_ncmd)
		return false; //playing, cannot submit!
	if(SOUND_PLAYER_QUEUE_SIZE == engine->channel_array[channel].play_ncmd - engine->channel_array[channel].free_ncmd)
		return false; //queue full, cannot submit!

	command_channel[channel].play_ncmd ++;
	command_channel[channel].play_info = *info;
	return true;
}

void sound_engine_stop(SoundEngine engine, uint32_t channel)
{
	SoundChannelCommand* command_channel = engine->command_channel[engine->command_queue.index];
	if (command_channel[channel].stop_ncmd != engine->channel_array[channel].stop_ncmd)
		return; //already stoping, cannot submit!

	command_channel[channel].stop_ncmd ++;
}

void sound_engine_move(SoundEngine engine, uint32_t channel, SoundChannelMoveInfo const* info)
{
	SoundChannelCommand* command_channel = engine->command_channel[engine->command_queue.index];
	command_channel[channel].move_ncmd ++;
	command_channel[channel].move_info = *info;
}

typedef struct SoundGraphChannel
{
	SoundPlayer				player;
	SoundResampler			resampler;
	SoundSpatializer		spatializer;
	SoundFader				fader;

	uint32_t				play_ncmd;
	uint32_t				stop_ncmd;
	uint32_t				move_ncmd;
	uint32_t				free_ncmd;
} SoundGraphChannel;

typedef struct SoundGraph
{
	SoundQueue					command_queue;
	SoundChannelCommand*		command_channel[3];
	SoundGraphCommand*			command_graph[3];

	SoundQueue					response_queue;
	SoundChannelResponse*		response_channel[3];
	SoundGraphResponse*			response_graph[3];

	SoundMaster					master;
	SoundMixer					mixer;

	SoundGraphChannel*			channel_array;
	uint32_t					channel_count;

	uint32_t					command_ncmd;
} SoundGraph;

static size_t sound_engine_struct(uint32_t channels, size_t(*offsets)[2])
{
	size_t aligns[2];
	size_t sizes[2];
	aligns[0] = alignof(struct SoundEngine);
	aligns[1] = alignof(SoundEngineChannel);
	sizes[0] = sizeof(struct SoundEngine);
	sizes[1] = channels * sizeof(SoundEngineChannel);
	return mem_struct(*offsets, aligns, sizes, 2);
}

static size_t sound_channel_struct(size_t(*offsets)[4])
{
	size_t aligns[4];
	size_t sizes[4];

	aligns[0] = SOUND_MIN_ALIGN;
	aligns[1] = SOUND_MIN_ALIGN;
	aligns[2] = SOUND_MIN_ALIGN;
	aligns[3] = SOUND_MIN_ALIGN;
	
	sizes[0] = sound_player_size();
	sizes[1] = sound_resampler_size();
	sizes[2] = sound_spatializer_size();
	sizes[3] = sound_fader_size();

	return mem_struct(*offsets, aligns, sizes, 4);
}

static size_t sound_graph_struct(uint32_t channels, size_t(*offsets)[5])
{
	size_t ch_offsets[4];
	size_t ch_size = sound_channel_struct(&ch_offsets);

	SoundMixerCreateInfo mixer_info = { 0 };
	mixer_info.track_count = channels;

	size_t aligns[5];
	size_t sizes[5];

	aligns[0] = alignof(SoundGraph);
	aligns[1] = SOUND_MIN_ALIGN;
	aligns[2] = SOUND_MIN_ALIGN;
	aligns[3] = alignof(SoundGraphChannel);
	aligns[4] = SOUND_MIN_ALIGN;

	sizes[0] = sizeof(SoundGraph);
	sizes[1] = sound_mixer_size(&mixer_info);
	sizes[2] = sound_master_size();
	sizes[3] = sizeof(SoundGraphChannel) * channels;
	sizes[4] = ch_size * channels;
	
	return mem_struct(*offsets, aligns, sizes, 5);
}

static size_t sound_command_struct(uint32_t channels, size_t cache_line, size_t(*offsets)[14])
{
	size_t aligns[14];
	size_t sizes[14];
	aligns[0] = m_max_size(alignof(int32_t volatile), cache_line);
	aligns[1] = m_max_size(alignof(int32_t volatile), cache_line);
	aligns[2] = m_max_size(alignof(SoundGraphCommand), cache_line);
	aligns[3] = m_max_size(alignof(SoundGraphCommand), cache_line);
	aligns[4] = m_max_size(alignof(SoundGraphCommand), cache_line);
	aligns[5] = m_max_size(alignof(SoundGraphResponse), cache_line);
	aligns[6] = m_max_size(alignof(SoundGraphResponse), cache_line);
	aligns[7] = m_max_size(alignof(SoundGraphResponse), cache_line);
	aligns[8] = m_max_size(alignof(SoundChannelCommand), cache_line);
	aligns[9] = m_max_size(alignof(SoundChannelCommand), cache_line);
	aligns[10] = m_max_size(alignof(SoundChannelCommand), cache_line);
	aligns[11] = m_max_size(alignof(SoundChannelResponse), cache_line);
	aligns[12] = m_max_size(alignof(SoundChannelResponse), cache_line);
	aligns[13] = m_max_size(alignof(SoundChannelResponse), cache_line);

	sizes[0] = sizeof(int32_t volatile);
	sizes[1] = sizeof(int32_t volatile);
	sizes[2] = sizeof(SoundGraphCommand);
	sizes[3] = sizeof(SoundGraphCommand);
	sizes[4] = sizeof(SoundGraphCommand);
	sizes[5] = sizeof(SoundGraphResponse);
	sizes[6] = sizeof(SoundGraphResponse);
	sizes[7] = sizeof(SoundGraphResponse);
	sizes[8] = channels * sizeof(SoundChannelCommand);
	sizes[9] = channels * sizeof(SoundChannelCommand);
	sizes[10] = channels * sizeof(SoundChannelCommand);
	sizes[11] = channels * sizeof(SoundChannelResponse);
	sizes[12] = channels * sizeof(SoundChannelResponse);
	sizes[13] = channels * sizeof(SoundChannelResponse);

	return mem_struct(*offsets, aligns, sizes, 14);
}

#include "clock.h"
#include <stdio.h>

void __stdcall OutputDebugStringA(char const*);

static void do_measure(os_clock_t c1, os_clock_t c2, uint32_t nchunks)
{
	static uint32_t cnc = 0;
	static int cnt = 0;
	static os_clock_t clk = 0;

	clk += c2 - c1;
	cnc += nchunks;
	++cnt;
	if (256 == cnt)
	{
		clk = os_clock_to(clk, 100000000000);
		clk /= cnc;

		char cbuff[256];
		snprintf(cbuff, sizeof(cbuff), "time_100ns: %lli \n", clk);
		//OutputDebugStringA(cbuff);

		cnc = 0;
		cnt = 0;
		clk = 0;
	}
}

static void render_callback(void* ctx, AudioStream stream, AudioEvent event, AudioPacket const* packet)
{
	SoundGraph* const graph = ctx;

	if (sound_queue_consume(&graph->command_queue))
	{
		SoundGraphCommand const* command_graph = graph->command_graph[graph->command_queue.index];
		if (command_graph->ncmd != graph->command_ncmd)
		{
			graph->command_ncmd = command_graph->ncmd;
			//todo: apply command
		}

		if (command_graph->exit)
			audio_stream_exit(stream);

		SoundChannelCommand const* command_channel = graph->command_channel[graph->command_queue.index];

		for (uint32_t ch = 0; ch < graph->channel_count; ++ch)
		{
			SoundChannelCommand const* command = command_channel + ch;
			SoundGraphChannel* channel = graph->channel_array + ch;

			if (command->play_ncmd != channel->play_ncmd)
			{
				//signal the command as accepted
				channel->play_ncmd = command->play_ncmd;

				if (SOUND_CHANNEL_PLAY_ENQUEUE & command->play_info.flags)
				{
					//queue the buffer, fast cleanup in case of an error, should not happen
					if (!sound_player_play(channel->player, command->play_info.buffer, command->play_info.offset))
					{
						assert(false);
						//causing an audio glitch
						sound_player_stop(channel->player);
					}
				}
				else
				{
					if (sound_fader_is_fading(channel->fader))
					{
						//no audio was pulled, just flush the queue
						sound_player_stop(channel->player);
					}
					else
					{
						//do the buffered fade processing and flush the queue
						if (sound_player_queue(channel->player))
						{
							sound_fader_fadeout(channel->fader);
							sound_player_stop(channel->player);
						}
						else if (sound_player_underflow(channel->player) < (sound_resampler_delay() + sound_spatializer_delay()))
						{
							sound_fader_delay(channel->fader);
						}
					}

					sound_resampler_create(channel->resampler, sound_player_submix(channel->player), 1.0f, command->play_info.position, command->play_info.velocity);
					sound_spatializer_create(channel->spatializer, sound_resampler_submix(channel->resampler), command->play_info.position, 1.0f);

					//todo: apply other flags and settings

					//no cleanup needed in case of an error, should not happen
					if (!sound_player_play(channel->player, command->play_info.buffer, command->play_info.offset))
					{
						assert(false);
					}
				}
			}

			if (command->move_ncmd != channel->move_ncmd)
			{
				channel->move_ncmd = command->move_ncmd;
				flt32_t const dt = 50e-3f;
				sound_resampler_doppler(channel->resampler, command->move_info.position, command->move_info.velocity, dt);
				sound_spatializer_position(channel->spatializer, command->move_info.position, dt);
			}

			if (command->stop_ncmd != channel->stop_ncmd)
			{
				channel->stop_ncmd = command->stop_ncmd;
				if (sound_player_queue(channel->player))
				{
					if (false == sound_fader_is_fading(channel->fader))
						sound_fader_fadeout(channel->fader);
					sound_player_stop(channel->player);
				}
			}
		}
	}

	SoundGraphResponse* response_graph = graph->response_graph[graph->response_queue.index];

	switch (event)
	{
	case AUDIO_EVENT_INIT:
	{
		response_graph->state = SOUND_AUDIO_STATE_STOP;
		break;
	}
	case AUDIO_EVENT_START:
	{
		response_graph->state = SOUND_AUDIO_STATE_PLAY;

		AudioFormat format;
		audio_stream_format(stream, &format);
		AudioChannel ch_map[2];
		ch_map[0] = AUDIO_CHANNEL_FRONT_LEFT;
		ch_map[1] = AUDIO_CHANNEL_FRONT_RIGHT;

		bool_t res = audio_stream_map_channels(stream, ch_map, (uint32_t)lengthof(ch_map));
		assert(res);
		sound_master_frequency(graph->master, format.frequency);
		break;
	}
	case AUDIO_EVENT_DATA:
	{
		uint32_t rendered = 0;

		flt32_t out_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];
		flt32_t out_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];

		flt32_t* out[2];
		out[0] = out_l;
		out[1] = out_r;

		os_clock_t c1 = os_clock();

		while (packet->frames != rendered)
		{
			uint32_t to_render = m_min_u32(SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX, packet->frames - rendered);
			sound_master_mix(graph->master, out, to_render);
			audio_stream_write(stream, packet->buffer, out, to_render, rendered);
			rendered += to_render;
		}

		os_clock_t c2 = os_clock();
		do_measure(c1, c2, packet->frames);

		break;
	}
	case AUDIO_EVENT_STOP:
	{
		response_graph->state = SOUND_AUDIO_STATE_STOP;
		break;
	}
	case AUDIO_EVENT_IDLE:
	{
		//dummy mixing
		flt32_t out_l[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];
		flt32_t out_r[SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX];

		flt32_t* out[2];
		out[0] = out_l;
		out[1] = out_r;
		sound_master_mix(graph->master, out, SOUND_MIX_CHUNK * SOUND_NCHUNKS_MAX);
		break;
	}
	case AUDIO_EVENT_EXIT:
	default:
	{
		response_graph->state = SOUND_AUDIO_STATE_EXIT;
		break;
	}
	}

	SoundChannelResponse* response_channel = graph->response_channel[graph->response_queue.index];

	for (uint32_t ch = 0; ch < graph->channel_count; ++ch)
	{
		SoundChannelResponse* response = response_channel + ch;
		SoundGraphChannel* channel = graph->channel_array + ch;

		uint32_t buffer_count = channel->play_ncmd - channel->free_ncmd;
		channel->free_ncmd += buffer_count - sound_player_queue(channel->player);

		response->play_ncmd = channel->play_ncmd;
		response->stop_ncmd = channel->stop_ncmd;
		response->free_ncmd = channel->free_ncmd;
	}

	sound_queue_produce(&graph->response_queue);
	memcpy(graph->response_graph[graph->response_queue.index], response_graph, sizeof(SoundGraphResponse));
}

static uint8_t audio_main(void* param)
{
	return !audio_render(render_callback, param);
}

SoundEngine	sound_engine_create(SoundEngineCreateInfo const* info, Allocator allocator)
{
	sound_player_init();
	sound_resampler_init();
	sound_mixer_init();

	size_t engine_offsets[2];
	size_t engine_size = sound_engine_struct(info->channels, &engine_offsets);
	SoundEngine engine = mem_alloc(allocator, engine_size);
	memset(engine, 0, engine_size);

	size_t graph_offsets[5];
	size_t graph_size = sound_graph_struct(info->channels, &graph_offsets);
	SoundGraph* graph = mem_alloc_aligned(allocator, m_max_size(SOUND_MIN_ALIGN, ALLOCATOR_MIN_ALIGNMENT), graph_size);
	memset(graph, 0, graph_size);
	engine->graph = graph;

	size_t channel_offsets[4];
	size_t channel_size = sound_channel_struct(&channel_offsets);

	size_t cache_line = os_cache_line_size();
	size_t command_offsets[14];
	size_t command_size = sound_command_struct(info->channels, cache_line, &command_offsets);
	void* command = mem_alloc_aligned(allocator, m_max_size(cache_line, ALLOCATOR_MIN_ALIGNMENT), command_size);
	memset(command, 0, command_size);

	engine->allocator = allocator;
	engine->channel_count = info->channels;
	engine->channel_array = mem_offset(engine, engine_offsets[1]);
	engine->command_queue.index = 1;
	engine->command_queue.sync = mem_offset(command, command_offsets[0]);
	engine->response_queue.index = 1;
	engine->response_queue.sync = mem_offset(command, command_offsets[1]);
	engine->command_graph[0] = mem_offset(command, command_offsets[2]);
	engine->command_graph[1] = mem_offset(command, command_offsets[3]);
	engine->command_graph[2] = mem_offset(command, command_offsets[4]);
	engine->response_graph[0] = mem_offset(command, command_offsets[5]);
	engine->response_graph[1] = mem_offset(command, command_offsets[6]);
	engine->response_graph[2] = mem_offset(command, command_offsets[7]);
	engine->command_channel[0] = mem_offset(command, command_offsets[8]);
	engine->command_channel[1] = mem_offset(command, command_offsets[9]);
	engine->command_channel[2] = mem_offset(command, command_offsets[10]);
	engine->response_channel[0] = mem_offset(command, command_offsets[11]);
	engine->response_channel[1] = mem_offset(command, command_offsets[12]);
	engine->response_channel[2] = mem_offset(command, command_offsets[13]);

	
	SoundMixerCreateInfo mixer_info = { 0 };
	mixer_info.track_count = info->channels;

	graph->mixer = sound_mixer_create(&mixer_info, mem_offset(graph, graph_offsets[1]));
	graph->master = sound_master_create(mem_offset(graph, graph_offsets[2]), sound_mixer_submix(graph->mixer));
	graph->channel_count = info->channels;
	graph->channel_array = mem_offset(graph, graph_offsets[3]);
	
	flt32_t const zero_vec3[] = { 0, 0, 0 };
	for (uint32_t ch = 0; ch < info->channels; ++ch)
	{
		void* channel = mem_offset(graph, graph_offsets[4] + ch * channel_size);

		graph->channel_array[ch].player = sound_player_create(
			mem_offset(channel, channel_offsets[0]));

		graph->channel_array[ch].resampler = sound_resampler_create(
			mem_offset(channel, channel_offsets[1]),
			sound_player_submix(graph->channel_array[ch].player), 
			1.f,
			zero_vec3,
			zero_vec3);

		graph->channel_array[ch].spatializer = sound_spatializer_create(
			mem_offset(channel, channel_offsets[2]), 
			sound_resampler_submix(graph->channel_array[ch].resampler),
			zero_vec3,
			1.0f);

		graph->channel_array[ch].fader = sound_fader_create(
			mem_offset(channel, channel_offsets[3]), 
			sound_spatializer_submix(graph->channel_array[ch].spatializer));

		sound_mixer_track(graph->mixer, sound_fader_submix(graph->channel_array[ch].fader), ch);
	}

	graph->command_queue.index = 2;
	graph->command_queue.sync = mem_offset(command, command_offsets[0]);
	graph->response_queue.index = 2;
	graph->response_queue.sync = mem_offset(command, command_offsets[1]);
	graph->command_graph[0] = mem_offset(command, command_offsets[2]);
	graph->command_graph[1] = mem_offset(command, command_offsets[3]);
	graph->command_graph[2] = mem_offset(command, command_offsets[4]);
	graph->response_graph[0] = mem_offset(command, command_offsets[5]);
	graph->response_graph[1] = mem_offset(command, command_offsets[6]);
	graph->response_graph[2] = mem_offset(command, command_offsets[7]);
	graph->command_channel[0] = mem_offset(command, command_offsets[8]);
	graph->command_channel[1] = mem_offset(command, command_offsets[9]);
	graph->command_channel[2] = mem_offset(command, command_offsets[10]);
	graph->response_channel[0] = mem_offset(command, command_offsets[11]);
	graph->response_channel[1] = mem_offset(command, command_offsets[12]);
	graph->response_channel[2] = mem_offset(command, command_offsets[13]);


	bool_t res = os_thread_create(&engine->audio_thread, audio_main, graph, "audio_thread", 1024 * 64);

	//wait for init by spinlock :(
	while (false == sound_queue_consume(&engine->response_queue))
	{
		os_thread_sleep(1);
	}

	if (SOUND_AUDIO_STATE_EXIT == engine->response_graph[engine->response_queue.index]->state)
	{
		//cleanup
	}

	return engine;
}

void sound_engine_destroy(SoundEngine engine)
{
	SoundGraphCommand* command_graph = engine->command_graph[engine->command_queue.index];
	command_graph->exit = true;
	sound_queue_produce(&engine->command_queue);

	os_thread_join(engine->audio_thread, null, -1);

	size_t cache_line = os_cache_line_size();
	size_t command_offsets[14];
	size_t command_size = sound_command_struct(engine->channel_count, cache_line, &command_offsets);
	mem_free_aligned(engine->allocator, engine->command_queue.sync, command_size);

	size_t graph_offsets[5];
	size_t graph_size = sound_graph_struct(engine->channel_count, &graph_offsets);
	mem_free_aligned(engine->allocator, engine->graph, graph_size);

	size_t engine_offsets[2];
	size_t engine_size = sound_engine_struct(engine->channel_count, &engine_offsets);
	mem_free(engine->allocator, engine, engine_size);	
}