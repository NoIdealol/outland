#include "pch.h"
#include "sound_common.h"
#include "sound_resampler.h"

#define SOUND_RESAMPLE_BUFFER 2 * SOUND_MIX_CHUNK
#define SOUND_RESAMPLE_MAX_SPEED 2

#if ARCH_X86 || ARCH_X64
#include <immintrin.h>
static void sound_resampler_mix_simd8(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
static void sound_resampler_mix_simd4(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);
#endif
static void sound_resampler_mix_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks);

#if ARCH_X86 || ARCH_X64
static struct SoundSubmixVtbl const g_resampler_simd8_vtbl =
{
	.mix = sound_resampler_mix_simd8,
};

static struct SoundSubmixVtbl const g_resampler_simd4_vtbl =
{
	.mix = sound_resampler_mix_simd4,
};
#endif

static struct SoundSubmixVtbl const g_resampler_scalar_vtbl =
{
	.mix = sound_resampler_mix_scalar,
};

static SoundSubmixVtbl g_resampler_vtbl = &g_resampler_scalar_vtbl;

void sound_resampler_init()
{
#if ARCH_X86 || ARCH_X64
	uint32_t const os = arch_os();
	uint32_t const hw = arch_hw();

	uint32_t const simd4 = ARCH_HW_SSE | ARCH_HW_SSE2;
	if (ARCH_OS_SSE & os && simd4 == (simd4 & hw))
	{
		g_resampler_vtbl = &g_resampler_simd4_vtbl;
	}

	uint32_t const simd8 = ARCH_HW_AVX;
	if (ARCH_OS_AVX & os && simd8 == (simd8 & hw))
	{
		//g_resampler_vtbl = &g_resampler_simd8_vtbl;
	}
#endif
}





struct SoundResampler
{
	struct SoundSubmix	base;
	SoundSubmix			source;
	flt32_t				buffer[SOUND_RESAMPLE_BUFFER];
	flt32_t				offset;
	flt32_t				src_speed;
	flt32_t				src_position[3];
	flt32_t				src_velocity[3];
	flt32_t				dst_speed;
	flt32_t				dst_position[3];
	flt32_t				dst_velocity[3];
	flt32_t				dst_time;

};

uint32_t sound_resampler_delay()
{
	return SOUND_RESAMPLE_BUFFER;
}

size_t sound_resampler_size()
{
	return sizeof(struct SoundResampler);
}

SoundResampler sound_resampler_create(void* memory, SoundSubmix source, flt32_t speed, flt32_t const* position, flt32_t const* velocity)
{
	SoundResampler const resampler = memory;
	memset(resampler, 0, sizeof(struct SoundResampler));
	resampler->source = source;
	resampler->base.vtbl = g_resampler_vtbl;
	resampler->offset = SOUND_MIX_CHUNK - 1;
	resampler->src_speed = speed;
	resampler->dst_speed = speed;
	memcpy(resampler->src_position, position, 3 * sizeof(flt32_t));
	memcpy(resampler->dst_position, position, 3 * sizeof(flt32_t));
	memcpy(resampler->src_velocity, velocity, 3 * sizeof(flt32_t));
	memcpy(resampler->dst_velocity, velocity, 3 * sizeof(flt32_t));

	return resampler;
}

void sound_resampler_doppler(SoundResampler resampler, flt32_t const* position, flt32_t const* velocity, flt32_t dt)
{
	memcpy(resampler->dst_position, position, 3 * sizeof(flt32_t));
	memcpy(resampler->dst_velocity, velocity, 3 * sizeof(flt32_t));
	resampler->dst_time = dt;
}

void sound_resampler_speed(SoundResampler resampler, flt32_t speed)
{
	resampler->dst_speed = speed;
}

SoundSubmix sound_resampler_submix(SoundResampler resampler)
{
	return &resampler->base;
}

static flt32_t doppler_state_3D(flt32_t const* position, flt32_t const* velocity)
{
	flt32_t const c = 343.f; //speed of sound
	flt32_t const inv_c = 1.f / c;

	flt32_t const dist = m_max_f32(m_sqrt_f32(position[0] * position[0] + position[1] * position[1] + position[2] * position[2]), 0.1f);
	flt32_t const v = -(position[0] * velocity[0] + position[1] * velocity[1] + position[2] * velocity[2]) / dist;
	flt32_t const doppler = (c + v) * inv_c;

	return doppler;
}

/*static void sound_resampler_mix_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundResampler const _this = (SoundResampler)submix;
	flt32_t const dt = (flt32_t)SOUND_MIX_CHUNK / (flt32_t)SOUND_FREQUENCY;

	flt32_t speed_src = _this->src_speed * doppler_state_3D(_this->src_position, _this->src_velocity);
	speed_src = m_min_f32(speed_src, SOUND_RESAMPLE_MAX_SPEED);

	if (_this->dst_time)
	{
		flt32_t const dt_mix = m_min_f32(dt * nchunks, _this->dst_time);
		flt32_t const lerp = dt_mix / _this->dst_time;

		_this->src_position[0] += (_this->dst_position[0] - _this->src_position[0]) * lerp;
		_this->src_position[1] += (_this->dst_position[1] - _this->src_position[1]) * lerp;
		_this->src_position[2] += (_this->dst_position[2] - _this->src_position[2]) * lerp;

		_this->src_velocity[0] += (_this->dst_velocity[0] - _this->src_velocity[0]) * lerp;
		_this->src_velocity[1] += (_this->dst_velocity[1] - _this->src_velocity[1]) * lerp;
		_this->src_velocity[2] += (_this->dst_velocity[2] - _this->src_velocity[2]) * lerp;

		_this->dst_time -= dt_mix;
	}
	_this->src_speed = _this->dst_speed;

	flt32_t speed_dst = _this->src_speed * doppler_state_3D(_this->src_position, _this->src_velocity);
	speed_dst = m_min_f32(speed_dst, SOUND_RESAMPLE_MAX_SPEED);

	flt32_t offset = _this->offset;
	
	alignas(SOUND_MIN_ALIGN) flt32_t buffer[SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX * SOUND_MIX_CHUNK + SOUND_RESAMPLE_BUFFER];
	{
		memcpy(buffer, _this->buffer, sizeof(flt32_t) * SOUND_RESAMPLE_BUFFER);
		flt32_t* mix_buffer = buffer + SOUND_RESAMPLE_BUFFER;

		flt32_t mix_fchunks = m_min_f32(nchunks * ((speed_dst - speed_src) * 0.5f + speed_src), SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX);
		uint32_t mix_nchunks = (uint32_t)mix_fchunks;
		_this->offset += (mix_fchunks - (flt32_t)mix_nchunks) * SOUND_MIX_CHUNK;
		if (SOUND_MIX_CHUNK <= _this->offset)
		{
			++mix_nchunks;
			_this->offset -= SOUND_MIX_CHUNK;
		}
		assert(SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX >= mix_nchunks);

		while (SOUND_NCHUNKS_MAX <= mix_nchunks)
		{
			_this->source->vtbl->mix(_this->source, &mix_buffer, SOUND_NCHUNKS_MAX);
			mix_nchunks -= SOUND_NCHUNKS_MAX;
			mix_buffer += SOUND_NCHUNKS_MAX * SOUND_MIX_CHUNK;
		}
		if (mix_nchunks)
		{
			_this->source->vtbl->mix(_this->source, &mix_buffer, mix_nchunks);
			mix_buffer += mix_nchunks * SOUND_MIX_CHUNK;
		}

		memcpy(_this->buffer, mix_buffer - SOUND_RESAMPLE_BUFFER, sizeof(flt32_t) * SOUND_RESAMPLE_BUFFER);
	}

	{
		uint32_t const samples = SOUND_MIX_CHUNK * nchunks;
		flt32_t const speedup_per_out = (speed_dst - speed_src) / samples;
		flt32_t speed_per_out = speed_src;
		flt32_t* dst = data[0];

		for (uint32_t s = 0; s < samples; ++s)
		{
			int32_t const sample = (int32_t)offset;
			flt32_t const lerp = offset - (flt32_t)sample;
			dst[s] = (buffer[sample] * (1.0f - lerp) + buffer[sample + 1] * lerp);
			
			speed_per_out += speedup_per_out;
			offset += speed_per_out;
		}
	}
}*/

static void mix_source(SoundResampler const _this, flt32_t* buffer, flt32_t* speed_src, flt32_t* speed_dst, uint32_t nchunks)
{
	flt32_t const dt = (flt32_t)SOUND_MIX_CHUNK / (flt32_t)SOUND_FREQUENCY;

	*speed_src = m_min_f32(_this->src_speed * doppler_state_3D(_this->src_position, _this->src_velocity), SOUND_RESAMPLE_MAX_SPEED);

	if (_this->dst_time)
	{
		flt32_t const dt_mix = m_min_f32(dt * nchunks, _this->dst_time);
		flt32_t const lerp = dt_mix / _this->dst_time;

		_this->src_position[0] += (_this->dst_position[0] - _this->src_position[0]) * lerp;
		_this->src_position[1] += (_this->dst_position[1] - _this->src_position[1]) * lerp;
		_this->src_position[2] += (_this->dst_position[2] - _this->src_position[2]) * lerp;

		_this->src_velocity[0] += (_this->dst_velocity[0] - _this->src_velocity[0]) * lerp;
		_this->src_velocity[1] += (_this->dst_velocity[1] - _this->src_velocity[1]) * lerp;
		_this->src_velocity[2] += (_this->dst_velocity[2] - _this->src_velocity[2]) * lerp;

		_this->dst_time -= dt_mix;
	}
	_this->src_speed = _this->dst_speed;

	*speed_dst = m_min_f32(_this->src_speed * doppler_state_3D(_this->src_position, _this->src_velocity), SOUND_RESAMPLE_MAX_SPEED);

	memcpy(buffer, _this->buffer, sizeof(flt32_t) * SOUND_RESAMPLE_BUFFER);
	buffer += SOUND_RESAMPLE_BUFFER;

	flt32_t const fchunks = m_min_f32(nchunks * ((*speed_dst - *speed_src) * 0.5f + *speed_src), SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX);
	nchunks = (uint32_t)fchunks;
	_this->offset += (fchunks - (flt32_t)nchunks) * SOUND_MIX_CHUNK;
	if (SOUND_MIX_CHUNK <= _this->offset)
	{
		++nchunks;
		_this->offset -= SOUND_MIX_CHUNK;
	}
	assert(SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX >= nchunks);

	while (SOUND_NCHUNKS_MAX <= nchunks)
	{
		_this->source->vtbl->mix(_this->source, &buffer, SOUND_NCHUNKS_MAX);
		nchunks -= SOUND_NCHUNKS_MAX;
		buffer += SOUND_NCHUNKS_MAX * SOUND_MIX_CHUNK;
	}
	if (nchunks)
	{
		_this->source->vtbl->mix(_this->source, &buffer, nchunks);
		buffer += nchunks * SOUND_MIX_CHUNK;
	}

	memcpy(_this->buffer, buffer - SOUND_RESAMPLE_BUFFER, sizeof(flt32_t) * SOUND_RESAMPLE_BUFFER);
}


#include "clock.h"
#include <stdio.h>
void __stdcall OutputDebugStringA(char const*);

static void do_measure(os_clock_t c1, os_clock_t c2, uint32_t nchunks)
{
	static uint32_t cnc = 0;
	static int cnt = 0;
	static os_clock_t clk = 0;

	clk += c2 - c1;
	cnc += nchunks;
	++cnt;
	if (256 == cnt)
	{
		clk = os_clock_to(clk, 100000000000);
		clk /= cnc;

		char cbuff[256];
		snprintf(cbuff, sizeof(cbuff), "time_100ns: %lli \n", clk);
		OutputDebugStringA(cbuff);

		cnc = 0;
		cnt = 0;
		clk = 0;
	}
}


#if ARCH_X86 || ARCH_X64

/*
expansion chart for coef. derivation, p = position, s = speed, a = acceleration

s[n] = s[n-1] + a;
p[n] = p[n-1] + s[n];

p[0] = p[0] + 0 * s[0] + ( 0 + 0 + 0 + 0) * a;
p[1] = p[0] + 1 * s[0] + ( 0 + 0 + 0 + 1) * a;
p[2] = p[0] + 2 * s[0] + ( 0 + 0 + 1 + 2) * a;
p[3] = p[0] + 3 * s[0] + ( 0 + 1 + 2 + 3) * a;
p[4] = p[0] + 4 * s[0] + ( 1 + 2 + 3 + 4) * a;
p[5] = p[0] + 5 * s[0] + ( 3 + 3 + 4 + 5) * a;
p[6] = p[0] + 6 * s[0] + ( 6 + 4 + 5 + 6) * a;
p[7] = p[0] + 7 * s[0] + (10 + 5 + 6 + 7) * a;
p[8] = p[0] + 8 * s[0] + (15 + 6 + 7 + 8) * a;
*/

static void sound_resampler_mix_simd8(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundResampler const _this = (SoundResampler)submix;

	flt32_t speed_src;
	flt32_t speed_dst;
	flt32_t position = _this->offset;

	alignas(SOUND_MIN_ALIGN) flt32_t buffer[SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX * SOUND_MIX_CHUNK + SOUND_RESAMPLE_BUFFER];

	mix_source(_this, buffer, &speed_src, &speed_dst, nchunks);

	os_clock_t c1 = os_clock();

	flt32_t const speedup_per_out = (speed_dst - speed_src) / (SOUND_MIX_CHUNK * nchunks);

	__m256 const acc = _mm256_set1_ps(speedup_per_out * 64);
	__m256 vel = _mm256_set_ps(
		speed_src * 8 + speedup_per_out * (56 + 36),
		speed_src * 8 + speedup_per_out * (48 + 36),
		speed_src * 8 + speedup_per_out * (40 + 36),
		speed_src * 8 + speedup_per_out * (32 + 36),
		speed_src * 8 + speedup_per_out * (24 + 36),
		speed_src * 8 + speedup_per_out * (16 + 36),
		speed_src * 8 + speedup_per_out * ( 8 + 36),
		speed_src * 8 + speedup_per_out * ( 0 + 36));
	__m256 pos = _mm256_set_ps(
		position + speed_src * 7 + speedup_per_out * 28,
		position + speed_src * 6 + speedup_per_out * 21,
		position + speed_src * 5 + speedup_per_out * 15,
		position + speed_src * 4 + speedup_per_out * 10,
		position + speed_src * 3 + speedup_per_out * 6,
		position + speed_src * 2 + speedup_per_out * 3,
		position + speed_src * 1 + speedup_per_out * 1,
		position + speed_src * 0 + speedup_per_out * 0);

	flt32_t* dst = data[0];

	for (uint32_t c = 0; c < nchunks * 2; ++c)
	{
		alignas(32) int32_t samp[8];
		__m256i ipos = _mm256_cvtps_epi32(pos);
		_mm256_store_si256((__m256i*)samp, ipos);
		__m256 lerp = _mm256_sub_ps(pos, _mm256_cvtepi32_ps(ipos));
		__m256 samp0 = _mm256_set_ps(
			buffer[samp[7]],
			buffer[samp[6]],
			buffer[samp[5]],
			buffer[samp[4]],
			buffer[samp[3]],
			buffer[samp[2]],
			buffer[samp[1]],
			buffer[samp[0]]);
		__m256 samp1 = _mm256_set_ps(
			buffer[samp[7] + 1],
			buffer[samp[6] + 1],
			buffer[samp[5] + 1],
			buffer[samp[4] + 1],
			buffer[samp[3] + 1],
			buffer[samp[2] + 1],
			buffer[samp[1] + 1],
			buffer[samp[0] + 1]);

		_mm256_store_ps(dst, _mm256_add_ps(_mm256_mul_ps(_mm256_sub_ps(samp1, samp0), lerp), samp0));
		dst += 8;
		pos = _mm256_add_ps(pos, vel);
		vel = _mm256_add_ps(vel, acc);
	}

	os_clock_t c2 = os_clock();
	do_measure(c1, c2, nchunks);
}

static void sound_resampler_mix_simd4(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundResampler const _this = (SoundResampler)submix;

	flt32_t speed_src;
	flt32_t speed_dst;
	flt32_t position = _this->offset;

	alignas(SOUND_MIN_ALIGN)flt32_t buffer[SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX * SOUND_MIX_CHUNK + SOUND_RESAMPLE_BUFFER];

	mix_source(_this, buffer, &speed_src, &speed_dst, nchunks);

	os_clock_t c1 = os_clock();

	flt32_t const speedup_per_out = (speed_dst - speed_src) / (SOUND_MIX_CHUNK * nchunks);

	__m128 const acc = _mm_set1_ps(speedup_per_out * 16);
	__m128 vel = _mm_set_ps(
		speed_src * 4 + speedup_per_out * (12 + 10),
		speed_src * 4 + speedup_per_out * ( 8 + 10),
		speed_src * 4 + speedup_per_out * ( 4 + 10),
		speed_src * 4 + speedup_per_out * ( 0 + 10));
	__m128 pos = _mm_set_ps(
		position + speed_src * 3 + speedup_per_out * 6,
		position + speed_src * 2 + speedup_per_out * 3,
		position + speed_src * 1 + speedup_per_out * 1,
		position + speed_src * 0 + speedup_per_out * 0);

	flt32_t* dst = data[0];

	for (uint32_t c = 0; c < nchunks * 4; ++c)
	{
		alignas(16) int32_t samp[4];
		__m128i ipos = _mm_cvtps_epi32(pos);
		_mm_store_si128((__m128i*)samp, ipos);
		__m128 lerp = _mm_sub_ps(pos, _mm_cvtepi32_ps(ipos));
		__m128 samp0 = _mm_set_ps(
			buffer[samp[3]],
			buffer[samp[2]],
			buffer[samp[1]],
			buffer[samp[0]]);
		__m128 samp1 = _mm_set_ps(
			buffer[samp[3] + 1],
			buffer[samp[2] + 1],
			buffer[samp[1] + 1],
			buffer[samp[0] + 1]);

		_mm_store_ps(dst, _mm_add_ps(_mm_mul_ps(_mm_sub_ps(samp1, samp0), lerp), samp0));
		dst += 4;
		pos = _mm_add_ps(pos, vel);
		vel = _mm_add_ps(vel, acc);
	}

	os_clock_t c2 = os_clock();
	do_measure(c1, c2, nchunks);
}
#endif

static void sound_resampler_mix_scalar(SoundSubmix submix, flt32_t* const* data, uint32_t nchunks)
{
	SoundResampler const _this = (SoundResampler)submix;

	flt32_t speed_src;
	flt32_t speed_dst;
	flt32_t position = _this->offset;

	alignas(SOUND_MIN_ALIGN)flt32_t buffer[SOUND_RESAMPLE_MAX_SPEED * SOUND_NCHUNKS_MAX * SOUND_MIX_CHUNK + SOUND_RESAMPLE_BUFFER];

	mix_source(_this, buffer, &speed_src, &speed_dst, nchunks);
	
	os_clock_t c1 = os_clock();

	uint32_t const samples = SOUND_MIX_CHUNK * nchunks;
	flt32_t const speedup_per_out = (speed_dst - speed_src) / samples;
	flt32_t speed_per_out = speed_src;
	flt32_t* dst = data[0];

	for (uint32_t s = 0; s < samples; ++s)
	{
		int32_t const sample = (int32_t)position;
		flt32_t const lerp = position - (flt32_t)sample;
		*dst = (buffer[sample] * (1.0f - lerp) + buffer[sample + 1] * lerp);

		++dst;
		speed_per_out += speedup_per_out;
		position += speed_per_out;
	}

	os_clock_t c2 = os_clock();
	do_measure(c1, c2, nchunks);
}