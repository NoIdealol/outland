#include "pch.h"
#include <Windows.h>

static wchar_t const CWIN_CLS_FORM[] = L"CWIN_FORM";
static wchar_t const CWIN_CLS_MENU[] = L"CWIN_MENU";

static HMODULE GetCurrentModuleHandle()
{
	HMODULE h_module = NULL;
	GetModuleHandleExW(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		(LPCWSTR)GetCurrentModuleHandle, &h_module);
	return h_module;
}

static LRESULT CALLBACK cwin_form_proc(HWND, UINT, WPARAM, LPARAM);
static LRESULT CALLBACK cwin_menu_proc(HWND, UINT, WPARAM, LPARAM);

//--------------------------------------------------
static ATOM cwin_register_form()
{
	SetLastError(0);

	HICON hicon = NULL;
	HICON hicon_sm = NULL;
	HBRUSH hbrush = (COLOR_3DDKSHADOW + 1);

	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof(WNDCLASSEXW);
	wcex.lpfnWndProc = cwin_form_proc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = GetCurrentModuleHandle();
	wcex.hIcon = hicon;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = hbrush;
	wcex.lpszMenuName = NULL;
	wcex.hIconSm = hicon_sm;
	wcex.style = 0;//CS_HREDRAW | CS_VREDRAW;
	wcex.lpszClassName = CWIN_CLS_FORM;

	return RegisterClassExW(&wcex);
}

//--------------------------------------------------
static ATOM cwin_register_menu()
{
	SetLastError(0);

	HICON hicon = NULL;
	HICON hicon_sm = NULL;
	HBRUSH hbrush = (COLOR_3DDKSHADOW + 1);

	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof(WNDCLASSEXW);
	wcex.lpfnWndProc = cwin_menu_proc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = GetCurrentModuleHandle();
	wcex.hIcon = hicon;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = hbrush;
	wcex.lpszMenuName = NULL;
	wcex.hIconSm = hicon_sm;
	wcex.style = CS_DROPSHADOW;//CS_HREDRAW | CS_VREDRAW;
	wcex.lpszClassName = CWIN_CLS_MENU;

	return RegisterClassExW(&wcex);
}



typedef struct CWinWindow*	CWinWindow;
typedef uint32_t			CWinProc(CWinWindow, void*);
typedef uint32_t			CWinWindowFlags;

typedef struct CWinCreateWindow
{
	CWinWindow	owner;
	CWinProc*	proc;
	char const* name;
	int32_t		left;
	int32_t		top;
	int32_t		width;
	int32_t		height;
} CWinCreateWindow;

struct CWinWindow
{
	HWND		wnd;
	CWinProc*	proc;

};

CWinWindow cwin_create_window(CWinCreateWindow* create, void* memory)
{
	CWinWindow win = memory;
	wchar_t wname[256];

	DWORD wstyle_ex = WS_EX_APPWINDOW;
	DWORD wstyle = WS_CAPTION;
	HWND wnd = CreateWindowExW(
		wstyle_ex,
		CWIN_CLS_FORM,
		wname,
		wstyle,
		create->left,
		create->top,
		create->width,
		create->height,
		create->owner->wnd,
		NULL,
		GetCurrentModuleHandle(),
		win);
}