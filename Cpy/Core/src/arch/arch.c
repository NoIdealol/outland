#include "pch.h"

#if ARCH_X86 || ARCH_X64

static void cpuid(uint32_t eax, uint32_t ecx, uint32_t* abcd)
{
#if defined(_MSC_VER)
	__cpuidex((int*)abcd, eax, ecx);
#else
	uint32_t ebx, edx;
# if defined( __i386__ ) && defined ( __PIC__ )
	/* in case of PIC under 32-bit EBX cannot be clobbered */
	__asm__("movl %%ebx, %%edi \n\t cpuid \n\t xchgl %%ebx, %%edi" : "=D" (ebx),
# else
	__asm__("cpuid" : "+b" (ebx),
# endif
		"+a" (eax), "+c" (ecx), "=d" (edx));
	abcd[0] = eax; abcd[1] = ebx; abcd[2] = ecx; abcd[3] = edx;
#endif
}

static uint64_t xgetbv(uint32_t idx)
{
	uint64_t xcr;
	/*
	Bit		Meaning
	0		X87 (x87 FPU/MMX State, note, must be '1')
	1		SSE (XSAVE feature set enable for MXCSR and XMM regs)
	2		AVX (AVX enable, and XSAVE feature set can be used to manage YMM regs)
	3		BNDREG (MPX enable, and XSAVE feature set can be used for BND regs)
	4		BNDCSR (MPX enable, and XSAVE feature set can be used for BNDCFGU and BNDSTATUS regs)
	5		opmask (AVX-512 enable, and XSAVE feature set can be used for AVX opmask, AKA k-mask, regs)
	6		ZMM_hi256 (AVX-512 enable, and XSAVE feature set can be used for upper-halves of the lower ZMM regs)
	7		Hi16_ZMM (AVX-512 enable, and XSAVE feature set can be used for the upper ZMM regs)
	8		Reserved
	9		PKRU (XSAVE feature set can be used for PKRU register, which is part of the protection keys mechanism.)
	10		Reserved (must be '0')
	11		Control-flow Enforcement Technology (CET) User State
	12		Control-flow Enforcement Technology (CET) Supervisor State
	13-63	Reserved (must be '0')
	*/

#if defined(_MSC_VER)
	xcr = (uint64_t)_xgetbv(idx);  /* min VS2010 SP1 compiler is required */
#else
	__asm__("xgetbv" : "=a" (xcr) : "c" (idx) : "%edx");
#endif

	return xcr;
}

uint32_t arch_os()
{
	uint64_t const xcr0 = xgetbv(0);
	uint32_t os = 0;

#define is_xcr0(flag) ((xcr0 & flag) == flag)

	if (is_xcr0(0x02))
		os |= ARCH_OS_SSE;
	if (is_xcr0(0x06))
		os |= ARCH_OS_AVX;
	if (is_xcr0(0xE6))
		os |= ARCH_OS_AVX512;

#undef is_xcr0

	return os;
}

uint32_t arch_hw()
{
	uint32_t hw = 0;

	uint32_t info[4];
	cpuid(0, 0, info);
	uint32_t nIds = info[0];
	cpuid(0x80000000, 0, info);
	uint32_t nExIds = info[0];

#define is_info(idx, bit) ((info[idx] & ((uint32_t)1 << bit)) != 0)

	//  Detect Features
	if (nIds >= 0x00000001)
	{
		cpuid(0x00000001, 0, info);

		if (is_info(3, 23))
			hw |= ARCH_HW_MMX;
		if (is_info(3, 25))
			hw |= ARCH_HW_SSE;	
		if (is_info(3, 26))
			hw |= ARCH_HW_SSE2;
		if (is_info(2, 0))
			hw |= ARCH_HW_SSE3;
		if (is_info(2, 9))
			hw |= ARCH_HW_SSSE3;
		if (is_info(2, 19))
			hw |= ARCH_HW_SSE41;
		if (is_info(2, 20))
			hw |= ARCH_HW_SSE42;
		if (is_info(2, 25))
			hw |= ARCH_HW_AES;
		if (is_info(2, 28))
			hw |= ARCH_HW_AVX;
		if (is_info(2, 12))
			hw |= ARCH_HW_FMA3;
		if (is_info(2, 30))
			hw |= ARCH_HW_RDRAND;
	}

	if (nIds >= 0x00000007)
	{
		cpuid(0x00000007, 0, info);

		if (is_info(1, 5))
			hw |= ARCH_HW_AVX2;
		if (is_info(1, 3))
			hw |= ARCH_HW_BMI1;
		if (is_info(1, 8))
			hw |= ARCH_HW_BMI2;
		if (is_info(1, 19))
			hw |= ARCH_HW_ADX;
		if (is_info(1, 29))
			hw |= ARCH_HW_SHA;
		if (is_info(2, 0))
			hw |= ARCH_HW_PREFETCHWT1;
		if (is_info(1, 16))
			hw |= ARCH_HW_AVX512F;
		if (is_info(1, 28))
			hw |= ARCH_HW_AVX512CD;
		if (is_info(1, 26))
			hw |= ARCH_HW_AVX512PF;
		if (is_info(1, 27))
			hw |= ARCH_HW_AVX512ER;
		if (is_info(1, 31))
			hw |= ARCH_HW_AVX512VL;
		if (is_info(1, 30))
			hw |= ARCH_HW_AVX512BW;
		if (is_info(1, 17))
			hw |= ARCH_HW_AVX512DQ;
		if (is_info(1, 21))
			hw |= ARCH_HW_AVX512IFMA;
		if (is_info(2, 1))
			hw |= ARCH_HW_AVX512VBMI;
	}

	if (nExIds >= 0x80000001)
	{
		cpuid(0x80000001, 0, info);

		if (is_info(3, 29))
			hw |= ARCH_HW_x64;
		if (is_info(2, 5))
			hw |= ARCH_HW_ABM;
		if (is_info(2, 6))
			hw |= ARCH_HW_SSE4a;
		if (is_info(2, 16))
			hw |= ARCH_HW_FMA4;
		if (is_info(2, 11))
			hw |= ARCH_HW_XOP;
	}

#undef is_info

	return hw;
}

#endif
