#include "pch.h"
#include "allocator.h"

typedef struct ProxyAllocator
{
	struct Allocator	base;
	Allocator			parent;
} ProxyAllocator;

//--------------------------------------------------
static void* proxy_allocator_alloc(Allocator allocator, size_t align, size_t size, char const* file, uint32_t line)
{
	ProxyAllocator* const this = (ProxyAllocator*)allocator;

	return this->parent->alloc(this->parent, align, size, file, line);
}

//--------------------------------------------------
static void proxy_allocator_free(Allocator allocator, void* memory, size_t size)
{
	ProxyAllocator* const this = (ProxyAllocator*)allocator;

	this->parent->free(this->parent, memory, size);
}

//--------------------------------------------------
static Allocator proxy_allocator_create(void* memory, AllocatorCreateInfo const* info)
{
	ProxyAllocator* const this = memory;
	this->base.alloc = &proxy_allocator_alloc;
	this->base.free = &proxy_allocator_free;
	this->parent = info->parent;

	return &this->base;
}

//--------------------------------------------------
static void proxy_allocator_destroy(Allocator allocator)
{

}

//--------------------------------------------------
void proxy_allocator_desc(AllocatorDesc* desc)
{
	desc->type = ALLOCATOR_TYPE_PROXY;
	desc->name = "Proxy";
	desc->size = sizeof(ProxyAllocator);
	desc->create = &proxy_allocator_create;
	desc->destroy = &proxy_allocator_destroy;
}