#include "pch.h"
#include "allocator.h"

typedef struct CircularAllocator
{
	struct Allocator	base;
	Allocator			parent;
	byte_t*				buffer;
	size_t				size;
	size_t				head;
	size_t				tail;
} CircularAllocator;

//ALLOCATION LAYOUT
//HEADER
//1 used bit, 7 pading bits (not including this byte)
//padding(optional), used for alignment, last byte has pading copy
//DATA
//user data or size_t(free size) if block is free

enum
{
	ALLOC_MIN_SIZE = sizeof(size_t),
	ALLOC_MAX_ALIGN = 1 << 7,

	HEADER_USED_BIT = 1 << 7,
	HEADER_PADDING_MASK = HEADER_USED_BIT - 1,
};

static size_t get_capacity(CircularAllocator* this)
{
	return this->size - (this->head - this->tail);
}

//--------------------------------------------------
static size_t get_head_offset(CircularAllocator* this)
{
	return this->head & (this->size - 1);
}

//--------------------------------------------------
static size_t get_tail_offset(CircularAllocator* this)
{
	return this->tail & (this->size - 1);
}

//--------------------------------------------------
static void	collect(CircularAllocator* this)
{
	while (this->tail != this->head)
	{
		size_t header = get_tail_offset(this);

		if ((header + ALLOC_MIN_SIZE + 1) > this->size)
		{
			this->tail += this->size - header;
			continue;
		}

		if (HEADER_USED_BIT & this->buffer[header])
			return;

		byte_t padding = HEADER_PADDING_MASK & this->buffer[header];
		byte_t* data = this->buffer + header + (padding + 1);
		size_t size;
		memcpy(&size, data, sizeof(size_t));

		this->tail += padding + 1;
		this->tail += size;
	}
}

//--------------------------------------------------
static void* circular_allocator_alloc(Allocator allocator, size_t align, size_t size, char const* file, uint32_t line)
{
	CircularAllocator* const this = (CircularAllocator*)allocator;

	size = m_max_size(size, ALLOC_MIN_SIZE);
	if (ALLOC_MAX_ALIGN < align)
		return null;

	size_t header = get_head_offset(this);
	size_t data = mem_align_offset(this->buffer + header + 1, align) + header + 1;
	size_t next = data + size;

	//do we have total size?
	if (get_capacity(this) < (next - header))
		return null;

	//do we wrap?
	if (next > this->size)
	{
		data = mem_align_offset(this->buffer + 1, align) + 1;
		next = data + size;

		size_t const wrap_next = next;
		size_t const wrap_size = this->size - header;
		if (get_capacity(this) < (next + (this->size - header)))
			return null;

		//insert dummy wrap allocation
		if ((header + ALLOC_MIN_SIZE + 1) <= this->size)
		{
			this->buffer[header] = 0;
			size_t dummy_size = this->size - (header + 1);
			memcpy(this->buffer + header + 1, &dummy_size, sizeof(size_t));
		}

		this->head += this->size - header;
		header = 0;
		assert(header == get_head_offset(this));
	}

	byte_t padding = (byte_t)(data - header - 1);
	this->buffer[header] = HEADER_USED_BIT | padding;
	memset(this->buffer + header + 1, padding, padding);
	
	this->head += next - header;
	return this->buffer + data;
}

//--------------------------------------------------
static void circular_allocator_free(Allocator allocator, void* memory, size_t size)
{
	CircularAllocator* const this = (CircularAllocator*)allocator;

	size = m_max_size(size, (size_t)ALLOC_MIN_SIZE);

	byte_t* ptr = memory;
	byte_t padding = *(ptr - 1) & HEADER_PADDING_MASK;
	byte_t* header = ptr - (padding + 1);
	memcpy(memory, &size, sizeof(size_t));
	*header = padding;

	collect(this);
}

//--------------------------------------------------
static Allocator circular_allocator_create(void* memory, AllocatorCreateInfo const* info)
{
	CircularAllocator* const this = memory;

	this->base.alloc = &circular_allocator_alloc;
	this->base.free = &circular_allocator_free;
	this->parent = info->parent;
	this->head = 0;
	this->tail = 0;
	this->size = info->size;
	this->buffer = mem_alloc(this->parent, this->size);

	return &this->base;
}

//--------------------------------------------------
static void circular_allocator_destroy(Allocator allocator)
{
	CircularAllocator* const this = (CircularAllocator*)allocator;
	mem_free(this->parent, this->buffer, this->size);
}

//--------------------------------------------------
void circular_allocator_desc(AllocatorDesc* desc)
{
	desc->type = ALLOCATOR_TYPE_CIRCULAR;
	desc->name = "Circular";
	desc->size = sizeof(CircularAllocator);
	desc->create = &circular_allocator_create;
	desc->destroy = &circular_allocator_destroy;
}