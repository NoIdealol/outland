#include "pch.h"
#include "allocator.h"

static struct Allocator			g_allocator = { 0 };
static AllocatorDesc*			g_desc_table = null;
static size_t					g_desc_size = 0;
static AllocatorCreateFlags		g_flags = 0;

typedef struct AllocatorTypeInfo
{
	AllocatorCreateFlags	flags;
	AllocatorType			type;
	char					name[ALLOCATOR_MAX_NAME_SIZE];
} AllocatorTypeInfo;

enum
{	
	TYPE_INFO_SIZE = (sizeof(AllocatorTypeInfo) + (ALLOCATOR_MIN_ALIGNMENT - 1)) & ~(ALLOCATOR_MIN_ALIGNMENT - 1),
};

//--------------------------------------------------
void	allocator_init(Allocator static_allocator, AllocatorCreateFlags flags, size_t custom_type_count)
{
	custom_type_count += ALLOCATOR_TYPE_COUNT;

	g_allocator = *static_allocator;
	g_desc_size = custom_type_count;
	g_desc_table = mem_alloc(&g_allocator, sizeof(AllocatorDesc) * g_desc_size);
	g_flags = flags;

	void malloc_allocator_desc(AllocatorDesc* desc);
	void circular_allocator_desc(AllocatorDesc* desc);
	void proxy_allocator_desc(AllocatorDesc* desc);

	AllocatorDesc desc;

	malloc_allocator_desc(&desc);
	allocator_register(&desc);

	circular_allocator_desc(&desc);
	allocator_register(&desc);

	proxy_allocator_desc(&desc);
	allocator_register(&desc);
}

//--------------------------------------------------
void	allocator_clean()
{
	mem_free(&g_allocator, g_desc_table, sizeof(AllocatorDesc) * g_desc_size);
	g_desc_table = null;
	g_desc_size = 0;
	g_flags = 0;
	memset(&g_allocator, 0, sizeof(struct Allocator));
}

//--------------------------------------------------
void allocator_register(AllocatorDesc const* desc)
{
	assert(g_desc_size > (size_t)desc->type);

	g_desc_table[desc->type] = *desc;
}

//--------------------------------------------------
Allocator allocator_create(AllocatorCreateInfo const* info)
{
	assert(g_desc_size > (size_t)info->type);
	assert(ALLOCATOR_MAX_NAME_SIZE > strlen(info->name));

	size_t size = g_desc_table[info->type].size + TYPE_INFO_SIZE;
	void* memory = mem_alloc(&g_allocator, size);

	AllocatorTypeInfo* type_info = memory;
	type_info->flags = info->flags;
	type_info->type = info->type;
	memset(type_info->name, 0, ALLOCATOR_MAX_NAME_SIZE);
	memcpy(type_info->name, info->name, m_min_size(strlen(info->name), ALLOCATOR_MAX_NAME_SIZE - 1));

	return g_desc_table[info->type].create((byte_t*)memory + TYPE_INFO_SIZE, info);
}

//--------------------------------------------------
void		allocator_destroy(Allocator allocator)
{
	void* memory = (byte_t*)allocator - TYPE_INFO_SIZE;

	AllocatorTypeInfo* type_info = memory;

	size_t size = g_desc_table[type_info->type].size + TYPE_INFO_SIZE;

	g_desc_table[type_info->type].destroy(allocator);
	mem_free(&g_allocator, memory, size);
}

#if !CFG_ALLOCATOR_DEBUG
//--------------------------------------------------
void* mem_alloc(Allocator allocator, size_t size)
{
	return allocator->alloc(allocator, ALLOCATOR_MIN_ALIGNMENT, size, null, 0);
}


//--------------------------------------------------
void* mem_alloc_aligned(Allocator allocator, size_t align, size_t size)
{
	return allocator->alloc(allocator, align, size, null, 0);
}
#endif

//--------------------------------------------------
void mem_free(Allocator allocator, void* memory, size_t size)
{
	allocator->free(allocator, memory, size);
}


//--------------------------------------------------
void mem_free_aligned(Allocator allocator, void* memory, size_t size)
{
	allocator->free(allocator, memory, size);
}

//--------------------------------------------------
void* mem_align(void* memory, size_t align)
{
	return (void*)((uintptr_t)((byte_t*)memory + align - 1) & ~(align - 1));
}

//--------------------------------------------------
void const* mem_align_const(void const* memory, size_t align)
{
	return (void const*)((uintptr_t)((byte_t const*)memory + align - 1) & ~(align - 1));
}

//--------------------------------------------------
size_t	mem_align_offset(void* memory, size_t align)
{
	return (size_t)((align - ((uintptr_t)memory & (align - 1))) & (align - 1));
}

//--------------------------------------------------
size_t		mem_align_offset_const(void const* memory, size_t align)
{
	return (size_t)((align - ((uintptr_t)memory & (align - 1))) & (align - 1));
}

//--------------------------------------------------
size_t	mem_struct(size_t* offset, size_t const* align, size_t const* size, size_t count)
{
	size_t size_sum = 0;
	for (size_t i = 0; i < count; ++i)
	{
		size_t aligner = align[i] - 1; //alignment is power of 2, invert bits to get mask
		size_sum = (size_sum + aligner) & (~aligner); //& mask				 
		offset[i] = size_sum;
		size_sum += size[i];
	}

	return size_sum;
}

//--------------------------------------------------
void*	mem_offset(void* memory, size_t offset)
{
	return (byte_t*)memory + offset;
}

//--------------------------------------------------
void const*	mem_offset_const(void const* memory, size_t offset)
{
	return (byte_t const*)memory + offset;
}