#include "pch.h"
#include "allocator.h"

#include <stdlib.h>


//--------------------------------------------------
static void* malloc_allocator_alloc(Allocator allocator, size_t align, size_t size, char const* file, uint32_t line)
{
	byte_t* mem = malloc(size + align);
	//memset(mem, 0, size + align);
	byte_t* align_mem = mem_align(mem + 1, align);
	align_mem[-1] = (uint8_t)(align_mem - mem);
	return align_mem;
}

//--------------------------------------------------
static void malloc_allocator_free(Allocator allocator, void* memory, size_t size)
{
	byte_t* align_mem = memory;
	byte_t* mem = align_mem - align_mem[-1];
	free(mem);
}

//--------------------------------------------------
Allocator malloc_allocator_create(void* memory, AllocatorCreateInfo const* info)
{
	Allocator allocator = memory;
	allocator->alloc = &malloc_allocator_alloc;
	allocator->free = &malloc_allocator_free;

	return allocator;
}

//--------------------------------------------------
static void malloc_allocator_destroy(Allocator allocator)
{

}

//--------------------------------------------------
void malloc_allocator_desc(AllocatorDesc* desc)
{
	desc->type = ALLOCATOR_TYPE_MALLOC;
	desc->name = "Malloc";
	desc->size = sizeof(struct Allocator);
	desc->create = &malloc_allocator_create;
	desc->destroy = &malloc_allocator_destroy;
}