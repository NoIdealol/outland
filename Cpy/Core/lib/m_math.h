#pragma once

size_t		m_max_size(size_t v1, size_t v2);
size_t		m_min_size(size_t v1, size_t v2);
uint32_t	m_max_u32(uint32_t v1, uint32_t v2);
uint32_t	m_min_u32(uint32_t v1, uint32_t v2);
uint64_t	m_max_u64(uint64_t v1, uint64_t v2);
uint64_t	m_min_u64(uint64_t v1, uint64_t v2);
int32_t		m_max_i32(int32_t v1, int32_t v2);
int32_t		m_min_i32(int32_t v1, int32_t v2);
int64_t		m_max_i64(int64_t v1, int64_t v2);
int64_t		m_min_i64(int64_t v1, int64_t v2);
flt32_t		m_max_f32(flt32_t v1, flt32_t v2);
flt32_t		m_min_f32(flt32_t v1, flt32_t v2);
flt64_t		m_max_f64(flt64_t v1, flt64_t v2);
flt64_t		m_min_f64(flt64_t v1, flt64_t v2);
size_t		m_align_size(size_t v, size_t align);
uint32_t	m_align_u32(uint32_t v, uint32_t align);

extern flt32_t const m_pi_f32;

flt32_t		m_abs_f32(flt32_t v);
flt32_t		m_sqrt_f32(flt32_t v);
flt32_t		m_sin_f32(flt32_t v);
flt32_t		m_asin_f32(flt32_t v);
flt32_t		m_cos_f32(flt32_t v);
flt32_t		m_acos_f32(flt32_t v);

int			m_bit_count_u32(uint32_t v);
int			m_bit_index_u32(uint32_t v);
uint32_t	m_to_pow2_u32(uint32_t v);

int			m_bit_count_u64(uint64_t v);
int			m_bit_index_u64(uint64_t v);
uint64_t	m_to_pow2_u64(uint64_t v);