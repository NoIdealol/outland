#pragma once

#define lengthof(array) sizeof(array) / sizeof(array[0])
#define alignof(type) __alignof(type)
#define alignas(expr) __declspec(align(expr))


#define STATIC_ASSERT(expr) typedef char static_assert_t[1]; typedef char static_assert_t[(expr) ? 1 : 2]

#define null ((void*)0)
#define true 1
#define false 0
