#pragma once

#include <stdint.h>


typedef int				result_t;
typedef unsigned char	byte_t;
typedef int				bool_t;
typedef char			char8_t;
typedef float			flt32_t;
typedef double			flt64_t;
//typedef uint64_t		tick_t;
typedef int				flags_t;
