#pragma once

//////////////////////////////////////////////////
// ARCH DEFINES
//////////////////////////////////////////////////

#if defined(_MSC_VER)

#if defined(_M_IX86)
#define ARCH_X86 1
#elif defined(_M_X64)
#define ARCH_X64 1
#elif defined(_M_ARM)
#define ARCH_ARM 1
#else
#error unknown architecture
#endif

#else

#if defined(__i386__)
#define ARCH_X86 1
#elif defined(__x86_64__)
#define ARCH_X64 1
#elif defined(__arm__)
#define ARCH_ARM 1
#else
#error unknown architecture
#endif

#endif

#ifndef ARCH_X86
#define ARCH_X86	0
#endif
#ifndef ARCH_X64
#define ARCH_X64	0
#endif
#ifndef ARCH_ARM
#define ARCH_ARM	0
#endif

//////////////////////////////////////////////////
// ARCH CODE
//////////////////////////////////////////////////

#if ARCH_X86 || ARCH_X64

#define	ARCH_OS_SSE 0x1
#define	ARCH_OS_AVX 0x2
#define	ARCH_OS_AVX512 0x4

	//  Misc.
#define	ARCH_HW_MMX 0x1
#define	ARCH_HW_x64 0x2
#define	ARCH_HW_ABM 0x4      // Advanced Bit Manipulation
#define	ARCH_HW_RDRAND 0x8
#define	ARCH_HW_BMI1 0x10
#define	ARCH_HW_BMI2 0x20
#define	ARCH_HW_ADX 0x40
#define	ARCH_HW_PREFETCHWT1 0x80

	//  SIMD: 128-bit
#define	ARCH_HW_SSE 0x100
#define	ARCH_HW_SSE2 0x200
#define	ARCH_HW_SSE3 0x400
#define	ARCH_HW_SSSE3 0x800
#define	ARCH_HW_SSE41 0x1000
#define	ARCH_HW_SSE42 0x2000
#define	ARCH_HW_SSE4a 0x4000
#define	ARCH_HW_AES 0x8000
#define	ARCH_HW_SHA 0x10000

	//  SIMD: 256-bit
#define	ARCH_HW_AVX 0x20000
#define	ARCH_HW_XOP 0x40000
#define	ARCH_HW_FMA3 0x80000
#define	ARCH_HW_FMA4 0x100000
#define	ARCH_HW_AVX2 0x200000

	//  SIMD: 512-bit
#define	ARCH_HW_AVX512F 0x400000			//  AVX512 Foundation
#define	ARCH_HW_AVX512CD 0x800000		//  AVX512 Conflict Detection
#define	ARCH_HW_AVX512PF 0x1000000		//  AVX512 Prefetch
#define	ARCH_HW_AVX512ER 0x2000000		//  AVX512 Exponential + Reciprocal
#define	ARCH_HW_AVX512VL 0x4000000		//  AVX512 Vector Length Extensions
#define	ARCH_HW_AVX512BW 0x8000000		//  AVX512 Byte + Word
#define	ARCH_HW_AVX512DQ 0x10000000		//  AVX512 Doubleword + Quadword
#define	ARCH_HW_AVX512IFMA 0x20000000	//  AVX512 Integer 52-bit Fused Multiply-Add
#define	ARCH_HW_AVX512VBMI 0x40000000	//  AVX512 Vector Byte Manipulation Instructions

uint32_t arch_os();
uint32_t arch_hw();

#endif
