#pragma once

#define CFG_ALLOCATOR_DEBUG 0


#define ALLOCATOR_MIN_ALIGNMENT sizeof(void*)
#define ALLOCATOR_MAX_NAME_SIZE 32


typedef struct Allocator* Allocator;

typedef void*	MemAlloc(Allocator allocator, size_t align, size_t size, char8_t const* file, uint32_t line);
typedef void	MemFree(Allocator allocator, void* memory, size_t size);

struct Allocator
{
	MemAlloc*	alloc;
	MemFree*	free;
};

#if CFG_ALLOCATOR_DEBUG
#define mem_alloc(allocator,size) (allocator)->alloc(allocator, ALLOCATOR_MIN_ALIGNMENT, size, __FILE__, __LINE__)
#define mem_alloc_aligned(allocator, align, size) (allocator)->alloc(allocator, align, size, __FILE__, __LINE__)
#else
void*	mem_alloc(Allocator allocator, size_t size);
void*	mem_alloc_aligned(Allocator allocator, size_t align, size_t size);
#endif

void	mem_free(Allocator allocator, void* memory, size_t size);
void	mem_free_aligned(Allocator allocator, void* memory, size_t size);

void*		mem_align(void* memory, size_t align);
void const*	mem_align_const(void const* memory, size_t align);
size_t		mem_align_offset(void* memory, size_t align);
size_t		mem_align_offset_const(void const* memory, size_t align);
size_t		mem_struct(size_t* offset, size_t const* align, size_t const* size, size_t count);
void*		mem_offset(void* memory, size_t offset);
void const*	mem_offset_const(void const* memory, size_t offset);

typedef enum AllocatorType
{
	ALLOCATOR_TYPE_MALLOC,
	ALLOCATOR_TYPE_CIRCULAR,
	ALLOCATOR_TYPE_PROXY,

	ALLOCATOR_TYPE_COUNT,
} AllocatorType;

enum
{
	ALLOCATOR_CREATE_THREAD_SAFE		= 0x01,
	ALLOCATOR_CREATE_TRACK_MEMORY		= 0x02,
	ALLOCATOR_CREATE_TRACK_FILE			= 0x04,
};
typedef uint32_t AllocatorCreateFlags;

typedef struct AllocatorCreateInfo
{
	AllocatorType			type;
	AllocatorCreateFlags	flags;
	char const*				name;
	Allocator				parent;
	size_t					size;
} AllocatorCreateInfo;

typedef Allocator	AllocatorCreate(void* memory, AllocatorCreateInfo const* info);
typedef void		AllocatorDestroy(Allocator allocator);

typedef struct AllocatorDesc
{
	AllocatorType		type;
	char*				name;
	size_t				size;
	AllocatorCreate*	create;
	AllocatorDestroy*	destroy;
} AllocatorDesc;

void		allocator_init(Allocator static_allocator, AllocatorCreateFlags flags, size_t custom_type_count);
void		allocator_clean();
void		allocator_register(AllocatorDesc const* desc);
Allocator	allocator_create(AllocatorCreateInfo const* info);
void		allocator_destroy(Allocator allocator);