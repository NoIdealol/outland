#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include "types.h"
#include "util.h"
#include "m_math.h"
#include "allocator.h"
#include "arch.h"

#include <assert.h>
#include <string.h>

//////////////////////////////////////////////////
// PLATFORM DEFINES
//////////////////////////////////////////////////

#if defined(_WIN32)
#define OS_WINDOWS	1
#endif

#if defined(__linux__) || defined(linux) || defined(__linux)
#define OS_LINUX	1
#endif

#if defined(__APPLE__) || defined(__MACH__)
#define OS_MAC	1
#endif

#ifndef OS_WINDOWS
#define OS_WINDOWS	0
#endif
#ifndef OS_LINUX
#define OS_LINUX	0
#endif
#ifndef OS_MAC
#define OS_MAC	0
#endif

