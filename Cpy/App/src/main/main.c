#include "pch.h"
#include "main.h"

#include "input.h"
#include <Windows.h>
#include "../../Engine/src/sound/sound_engine.h"
#include "..\..\System\lib\file.h"

#include "clock.h"

//--------------------------------------------------
static COLORREF dc_color(uint32_t color)
{
	return color;
	//return ((color >> 16) & 0x000000FF) | (color & 0x0000FF00) | ((color & 0x000000FF) << 16);
}

//--------------------------------------------------
static void dc_set_pixel(HDC dc, uint32_t x, uint32_t y, uint32_t color)
{
	SetPixel(dc, x, y, dc_color(color));
}

//--------------------------------------------------
void dc_draw_line(HDC dc, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color)
{
	BOOL res;
	HGDIOBJ old_pen = SelectObject(dc, GetStockObject(DC_PEN));
	SetDCPenColor(dc, dc_color(color));
	res = MoveToEx(dc, x1, y1, NULL);
	res = LineTo(dc, x2, y2);
	SelectObject(dc, old_pen);
}

//--------------------------------------------------
void dc_draw_ellipse(HDC dc, uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t color)
{
	BOOL res;
	HGDIOBJ old_pen = SelectObject(dc, GetStockObject(DC_PEN));
	SetDCPenColor(dc, dc_color(color));
	HGDIOBJ old_brush = SelectObject(dc, GetStockObject(NULL_BRUSH));

	res = Ellipse(dc,
		x,
		y,
		x + w,
		y + h);

	SelectObject(dc, old_brush);
	SelectObject(dc, old_pen);
}

//--------------------------------------------------
void dc_draw_rect(HDC dc, uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t color)
{
	RECT rect;
	rect.left = x;
	rect.top = y;
	rect.right = x + w;
	rect.bottom = y + h;

	HGDIOBJ old_brush = SelectObject(dc, GetStockObject(NULL_BRUSH));
	HGDIOBJ old_pen = SelectObject(dc, GetStockObject(DC_PEN));
	SetDCPenColor(dc, dc_color(color));

	Rectangle(dc,
		x,
		y,
		x + w,
		y + h);

	SelectObject(dc, old_pen);
	SelectObject(dc, old_brush);
}

//--------------------------------------------------
void dc_fill_rect(HDC dc, uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t color)
{
	RECT rect;
	rect.left = x;
	rect.top = y;
	rect.right = x + w;
	rect.bottom = y + h;

	HGDIOBJ dc_brush = GetStockObject(DC_BRUSH);
	HGDIOBJ old_brush = SelectObject(dc, dc_brush);
	SetDCBrushColor(dc, dc_color(color));

	FillRect(dc, &rect, (HBRUSH)dc_brush);
	SelectObject(dc, old_brush);
}

static struct App
{
	KeyboardState kb_state;
	flt32_t	pos_x;
	flt32_t	pos_y;
} app;

static void app_window_paint(HWND hWnd, PAINTSTRUCT const* ps)
{
	HDC dc = ps->hdc;

	dc_fill_rect(dc,
		0,
		0,
		800,
		600,
		0x000000);

	flt32_t const inv_room_size = 800.0f / 5.0f;
	dc_fill_rect(dc, 
		400 + app.pos_x * inv_room_size - 25, 
		300 + app.pos_y * inv_room_size - 25,
		50, 
		50, 
		0x0000FF);

	dc_draw_ellipse(dc,
		400 - 25,
		300 - 25,
		50,
		50,
		0xFF0000);
}

static LRESULT CALLBACK app_window_callback(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		if (BeginPaint(hWnd, &ps))
		{
			app_window_paint(hWnd, &ps);
			EndPaint(hWnd, &ps);
		}

		return 0;
	}
	case WM_ERASEBKGND:
	{
		return TRUE; //no flickering please
	}
	default:
		break;
	}
	return DefWindowProcW(hWnd, msg, wParam, lParam);
}

HWND app_init_window(HINSTANCE hInstance)
{
	HICON hIcon = NULL;
	HICON hIconSm = NULL;
	HBRUSH hbrBackground = (HBRUSH)(COLOR_3DDKSHADOW + 1);

	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof(WNDCLASSEXW);
	wcex.lpfnWndProc = app_window_callback;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = hIcon;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = hbrBackground;
	wcex.lpszMenuName = NULL;
	wcex.hIconSm = hIconSm;

	wcex.style = 0;//CS_HREDRAW | CS_VREDRAW;
	wcex.lpszClassName = L"APP_WINDOW";
	ATOM atom_window = RegisterClassExW(&wcex);
	if (!atom_window)
	{
	}

	return CreateWindowExW(
		WS_EX_APPWINDOW,
		(LPCWSTR)atom_window, 
		L"App", 
		WS_CAPTION | WS_BORDER | WS_VISIBLE, 
		400, 200, 
		800, 600, 
		NULL, 
		NULL, 
		hInstance, 
		null);
}

void core_init_allocators()
{
	Allocator malloc_allocator_create(void* memory, AllocatorCreateInfo const* info);

	struct Allocator mallocator;
	allocator_init(malloc_allocator_create(&mallocator, null), 0, 0);

}
SoundBuffer sound_buffer_wav(void* file, size_t size, Allocator allocator);
void* save_sound_buffer_wav(float* data, uint32_t frames, uint32_t sample_rate, size_t* size, Allocator allocator);

#define CT_AUDIO_LIB_EXPORT 1
#include "audio_reverb.h"
#include "audio_resample.h"
#include <stdio.h>
#include <math.h>

int CALLBACK WinMain2(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow)
{
	os_clock_init();

	core_init_allocators();

	Allocator allocator;
	{
		AllocatorCreateInfo info = { 0 };
		info.type = ALLOCATOR_TYPE_MALLOC;
		info.name = "App allocator";

		allocator = allocator_create(&info);
	}

	SoundBuffer sound_buffer_impulse;
	SoundBuffer sound_buffer_signal;
	{
		OsFileCreateInfo file_info;
		file_info.epoll = null;
		file_info.create = OS_FILE_CREATE_MUST_EXIST;
		file_info.access = OS_FILE_ACCESS_READ;
		file_info.attributes = 0;


		OsFile file;
		uint64_t file_size = 0;
		void* file_buffer = null;
		OsFileResult file_result;
		bool_t bool_result;

		//char const* imp_file = "D:/MEDIUM METAL ROOM E001 M2S.wav";
		//char const* imp_file = "D:/EchoBridge.wav";
		//char const* imp_file = "D:/inv_L0e090a_ext.wav";
		char const* imp_file = "D:/Senn-HD480-L.wav";

		file_result = os_file_create(&file, imp_file, &file_info);
		if (file_result)
			return 0;

		bool_result = os_file_get_size(file, &file_size);
		file_buffer = mem_alloc(allocator, file_size);
		bool_result = os_file_read(file, file_buffer, file_size, null);
		os_file_close(file);

		sound_buffer_impulse = sound_buffer_wav(file_buffer, file_size, allocator);
		mem_free(allocator, file_buffer, file_size);


		file_result = os_file_create(&file, "D:/palo_lala.wav", &file_info);
		if (file_result)
			return 0;

		bool_result = os_file_get_size(file, &file_size);
		file_buffer = mem_alloc(allocator, file_size);
		bool_result = os_file_read(file, file_buffer, file_size, null);
		os_file_close(file);

		sound_buffer_signal = sound_buffer_wav(file_buffer, file_size, allocator);
		mem_free(allocator, file_buffer, file_size);
	}

	SoundBufferCreateInfo sbi;
	sound_buffer_info(sound_buffer_impulse, &sbi);
	//sbi.frames /= 6;

	CtAudioReverbRate crate = CT_AUDIO_REVERB_RATE_48000;
	CtAudioReverbSize csize = CT_AUDIO_REVERB_SIZE_4_SECONDS;
	CtAudioReverbCompression ccomp = CT_AUDIO_REVERB_COMPRESSION_HIGH;

	CtAudioReverbContext cctx = ct_audio_reverb_context_create(crate, csize, ccomp, mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, ct_audio_reverb_context_size(crate, csize, ccomp)));
	void* cscr = mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, ct_audio_reverb_context_scratch_size(cctx));
	/*CtAudioReverbImpulse cimp = ct_audio_reverb_impulse_create(
		cctx, 
		sound_buffer_data(sound_buffer_impulse, 0), 
		sbi.frames, 
		mem_alloc_aligned(allocator, CT_AUDIO_REVERB_ALIGN, ct_audio_reverb_impulse_size(cctx, sbi.frames)), 
		cscr);*/

	CtAudioReverb crev_a[1];
	for (uint32_t i = 0; i < lengthof(crev_a); ++i)
	{
		CtAudioReverbImpulse cimp = ct_audio_reverb_impulse_create(
			cctx,
			sound_buffer_data(sound_buffer_impulse, 0),
			sbi.frames,
			mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, ct_audio_reverb_impulse_size(cctx, sbi.frames)),
			cscr);

		crev_a[i] = ct_audio_reverb_create(
			cctx,
			cimp,
			i,
			mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, ct_audio_reverb_size(cctx, cimp)));
	}

	uint32_t const L = 128 >> 0;

	sound_buffer_info(sound_buffer_signal, &sbi);
	uint32_t const  tail = 0;// impulse_sound_buffer->frames / L + 1;
	uint32_t blocks = sbi.frames / L;
	flt32_t* output = mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, sizeof(flt32_t) * (blocks + tail) * L);
	flt32_t* perf = mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, sizeof(flt32_t) * (blocks + tail));
	memset(output, 0, sizeof(flt32_t) * (blocks + tail) * L);
	memset(perf, 0, sizeof(flt32_t) * (blocks + tail));

	os_clock_t t_max = 0;
	os_clock_t t_sum = 0;
	

	
	/*for (uint32_t b = 0; b < blocks; b += 4)
	{
		Sleep(1);

		os_clock_t t1 = os_clock();
		for (uint32_t i = b; i < (b + 4); ++i)
			for (uint32_t j = 0; j < lengthof(crev_a); ++j)
				ct_audio_reverb_process(crev_a[j], sound_buffer_data(sound_buffer_signal, i * L), output + i * L, 1.0f, cscr);

		os_clock_t t2 = os_clock();
		os_clock_t dt_nano = os_clock_to(t2 - t1, 1000000000);
		perf[b] = (float)dt_nano;
		if (dt_nano > t_max)
			t_max = dt_nano;

		t_sum += dt_nano;
	}*/
	blocks -= blocks % 6;
	for (uint32_t i = 0; i < blocks; i += 6)
	{
		//Sleep(1);

		os_clock_t t1 = os_clock();
		for (uint32_t j = 0; j < lengthof(crev_a); ++j)
		{
			ct_audio_reverb_process(crev_a[j], sound_buffer_data(sound_buffer_signal, (i + 0) * L), output + (i + 0) * L, 1.0f, cscr);
			ct_audio_reverb_process(crev_a[j], sound_buffer_data(sound_buffer_signal, (i + 1) * L), output + (i + 1) * L, 1.0f, cscr);
			ct_audio_reverb_process(crev_a[j], sound_buffer_data(sound_buffer_signal, (i + 2) * L), output + (i + 2) * L, 1.0f, cscr);
			ct_audio_reverb_process(crev_a[j], sound_buffer_data(sound_buffer_signal, (i + 3) * L), output + (i + 3) * L, 1.0f, cscr);
			ct_audio_reverb_process(crev_a[j], sound_buffer_data(sound_buffer_signal, (i + 4) * L), output + (i + 4) * L, 1.0f, cscr);
			ct_audio_reverb_process(crev_a[j], sound_buffer_data(sound_buffer_signal, (i + 5) * L), output + (i + 5) * L, 1.0f, cscr);
		}
		os_clock_t t2 = os_clock();
		os_clock_t dt_nano = os_clock_to(t2 - t1, 1000000000);
		perf[i] = (float)dt_nano;
		if (dt_nano > t_max)
			t_max = dt_nano;

		t_sum += dt_nano;
	}

	alignas(CT_AUDIO_ALIGN) static float inp[60000] = { 0 };
	for (size_t i = 0; i < lengthof(inp); ++i)
	{
		inp[i] = sinf(i * 3.14 / 100);
		//inp[i * 2] = 1.0f - (i & 1) * 2.0f;
		//inp[i * 2 + 1] = 1.0f - (i & 1) * 2.0f;
	}

	alignas(CT_AUDIO_ALIGN)CtAudioCubicSpline spline = { 0.9999999f };

	int out_off0 = 0;
	int out_fil_size = 600;
	out_fil_size -= out_fil_size & 3;
	int drift = +30;
	flt32_t* output2 = mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, sizeof(flt32_t) * (blocks + tail) * L);
	memcpy(output2, output, sizeof(flt32_t) * (blocks + tail) * L);
	//ct_resample_offset_cubic_spline(spline, output2 + out_off0, output + out_off0 - drift, out_fil_size, 0, drift);
	size_t spline_done = 0;

	os_clock_t t1 = os_clock();
	for (int i = 0; i < 100; ++i)
	{
		/*spline_done += ct_resample_drift_cubic_spline(spline,
			output2 + out_fil_size * i,
			inp + spline_done,
			out_fil_size, 
			drift + 0.05f, 
			null);*/

		spline_done += ct_audio_resample(spline,
			output2 + out_fil_size * i,
			output + spline_done,
			out_fil_size,
			drift + 0.999999f);
	}
	os_clock_t t2 = os_clock();
	os_clock_t tt = os_clock_to(t2 - t1, 1000000000);

	//ct_resample_offset_cubic_spline(spline, output2 + out_fil_size / 2, inp + out_fil_size / 2 + drift / 2, out_fil_size / 2, 0, drift / 2);

	//ct_resample_offset_cubic_spline(spline, output + out_off0 + out_fil_size, output + out_off0 + out_fil_size, out_fil_size, 0, -200);
	//output = output2;

	{
		OsFileCreateInfo file_info;
		file_info.epoll = null;
		file_info.create = OS_FILE_CREATE_OVERWRITE;
		file_info.access = OS_FILE_ACCESS_WRITE;
		file_info.attributes = 0;

		OsFile file;
		uint64_t file_size = 0;
		OsFileResult file_result;
		bool_t bool_result;

		size_t out_size = 0;
		void* out_buff = save_sound_buffer_wav(output, blocks * L, 44100, &out_size, allocator);

		file_result = os_file_create(&file, "D:/convolution2.wav", &file_info);
		bool_result = os_file_write(file, out_buff, out_size, null);
		os_file_close(file);

		out_buff = save_sound_buffer_wav(perf, blocks, 44100, &out_size, allocator);

		file_result = os_file_create(&file, "D:/perf2.wav", &file_info);
		bool_result = os_file_write(file, out_buff, out_size, null);
		os_file_close(file);
	}

	char str[256];
	sprintf(str, "Time avg(nano): %lli \n", t_sum / (blocks + tail));
	OutputDebugStringA(str);
	sprintf(str, "Time max(nano): %lli \n", t_max);
	OutputDebugStringA(str);

	sprintf(str, "Time sumresam(nano): %lli \n", tt);
	OutputDebugStringA(str);

	return 0;
}

//--------------------------------------------------
int CALLBACK WinMain3(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow)
{
	os_clock_init();

	core_init_allocators();
	HWND hWnd = app_init_window(hInstance);

	Allocator allocator;
	{
		AllocatorCreateInfo info = { 0 };
		info.type = ALLOCATOR_TYPE_MALLOC;
		info.name = "App allocator";

		allocator = allocator_create(&info);
	}

	SoundBuffer sound_buffer;
	{
		OsFileCreateInfo file_info;
		file_info.epoll = null;
		file_info.create = OS_FILE_CREATE_MUST_EXIST;
		file_info.access = OS_FILE_ACCESS_READ;
		file_info.attributes = 0;

		OsFile file;
		uint64_t file_size = 0;
		void* file_buffer = null;
		OsFileResult file_result;
		bool_t bool_result;

		file_result = os_file_create(&file, "D:/engine.wav", &file_info);
		if (file_result)
			return 0;
		//file_result = os_file_create(&file, "D:/c304-2.wav", &file_info);
		bool_result = os_file_get_size(file, &file_size);
		file_buffer = mem_alloc(allocator, file_size);
		bool_result = os_file_read(file, file_buffer, file_size, null);
		os_file_close(file);

		sound_buffer = sound_buffer_wav(file_buffer, file_size, allocator);
	}

	SoundEngine engine;
	{
		SoundEngineCreateInfo engine_info;
		engine_info.channels = 64;
		engine = sound_engine_create(&engine_info, allocator);
	}

	Input input;
	{
		bool_t res;
		res = input_init(&input, allocator, 32);
		res = input_module_init(input, INPUT_DEVICE_TYPE_KEYBOARD);
	}

	
	while (true)
	{
		input_update(input);

		MSG msg;
		while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				uint8_t exit_code = (uint8_t)msg.wParam;
				return 0;
			}

			TranslateMessage(&msg);
			DispatchMessageW(&msg);
		}

		InputEvent ev_arr[32] = { 0 };
		size_t ev_size = input_epoll(input, ev_arr, lengthof(ev_arr));
		
		for (size_t ev_id = 0; ev_id < ev_size; ++ev_id)
		{
			if (INPUT_EVENT_TYPE_DEVICE_READING == ev_arr[ev_id].event_type && INPUT_DEVICE_TYPE_KEYBOARD == ev_arr[ev_id].device_type)
			{
				memcpy(&app.kb_state, &ev_arr[ev_id].device_state.keyboard, sizeof(KeyboardState));
			}
		}

		

		SoundChannelEvent channel_events[64];
		sound_engine_epoll(engine, channel_events);

		SoundChannelPlayInfo play_info = { 0 };
		play_info.flags = SOUND_CHANNEL_PLAY_ENQUEUE;
		play_info.buffer = sound_buffer;
		play_info.offset = 0;
		sound_engine_play(engine, 0, &play_info);

#define is_key_pressed(kb_state, KEY) (kb_state.buttons[KEY >> 6] & (((uint64_t)1) << (KEY & 63)))
		if (is_key_pressed(app.kb_state, KEYBOARD_KEY_A))
			app.pos_x -= 1.0f / 30.0f;
		if (is_key_pressed(app.kb_state, KEYBOARD_KEY_D))
			app.pos_x += 1.0f / 30.0f;
		if (is_key_pressed(app.kb_state, KEYBOARD_KEY_W))
			app.pos_y -= 1.0f / 30.0f * 5;
		if (is_key_pressed(app.kb_state, KEYBOARD_KEY_S))
			app.pos_y += 1.0f / 30.0f * 5;

		/*if (is_key_pressed(app.kb_state, KEYBOARD_KEY_A) ||
			is_key_pressed(app.kb_state, KEYBOARD_KEY_D) ||
			is_key_pressed(app.kb_state, KEYBOARD_KEY_W) ||
			is_key_pressed(app.kb_state, KEYBOARD_KEY_S))*/
		{
			SoundChannelMoveInfo move_info = { 0 };
			move_info.position[0] = app.pos_y;
			move_info.position[1] = app.pos_x;
			move_info.position[2] = 0;
			if (is_key_pressed(app.kb_state, KEYBOARD_KEY_W))
				move_info.velocity[0] = -13.5;
			else if (is_key_pressed(app.kb_state, KEYBOARD_KEY_S))
				move_info.velocity[0] = 13.5;
			else
				move_info.velocity[0] = 0;
			move_info.velocity[1] = 0;
			move_info.velocity[2] = 0;
			sound_engine_move(engine, 0, &move_info);
		}

		if (is_key_pressed(app.kb_state, KEYBOARD_KEY_ESCAPE))
		{
			//OutputDebugStringA("ESC\n");
			PostQuitMessage(0);
			return 0;
		}

		sound_engine_update(engine);
		InvalidateRect(hWnd, NULL, FALSE);

		Sleep(33);
	}
}



//#include "../../Engine/src/sound/sound_common.h"




//--------------------------------------------------
//int os_main(size_t cmd_size, char8_t const* const* cmd_line)
#if 0
int main2()
{
	core_init_allocators();

	Allocator allocator;
	{
		AllocatorCreateInfo info = { 0 };
		info.type = ALLOCATOR_TYPE_MALLOC;
		info.name = "App allocator";

		allocator = allocator_create(&info);
	}

	SoundBuffer sound_buffer;
	{
		OsFileCreateInfo file_info;
		file_info.epoll = null;
		file_info.create = OS_FILE_CREATE_MUST_EXIST;
		file_info.access = OS_FILE_ACCESS_READ;
		file_info.attributes = 0;

		OsFile file;
		uint64_t file_size = 0;
		void* file_buffer = null;
		OsFileResult file_result;
		bool_t bool_result;

		file_result = os_file_create(&file, "piano2.wav", &file_info);
		//file_result = os_file_create(&file, "D:/c304-2.wav", &file_info);
		bool_result = os_file_get_size(file, &file_size);
		file_buffer = mem_alloc(allocator, file_size);
		bool_result = os_file_read(file, file_buffer, file_size, null);
		os_file_close(file);

		sound_buffer = sound_buffer_wav(file_buffer, file_size, allocator);
	}

	bool_t res = false;

	SoundEngineCreateInfo engine_info;
	engine_info.channels = 1;
	SoundEngine engine = sound_engine_create(&engine_info, allocator);


	Input input;
	res = input_init(&input, allocator, 32);
	res = input_module_init(input, INPUT_DEVICE_TYPE_KEYBOARD);

	InputEvent ev_arr[32] = { 0 };
	size_t ev_size;

	KeyboardState kb_state = { 0 };

	double time = 0;

	while (true)
	{
		input_update(input);
		ev_size = input_epoll(input, ev_arr, lengthof(ev_arr));
		os_thread_sleep(33);

		//time += 33e-3;

		SoundChannelEvent channel_events[4];
		sound_channel_epoll(engine, channel_events);

		SoundChannelPlayInfo play_info = { 0 };
		play_info.flags = SOUND_CHANNEL_PLAY_ENQUEUE;
		play_info.buffer = sound_buffer;

		sound_channel_play(engine, 0, &play_info);

		SoundChannelMoveInfo move_info;
		move_info.position[0] = 2 * cos(time * 2 * 3.14159265);
		move_info.position[1] = 2 * sin(time * 2 * 3.14159265);
		move_info.position[2] = 0;
		sound_channel_move(engine, 0, &move_info);

#define is_key_pressed(KEY) (kb_state.buttons[KEY >> 6] & (((uint64_t)1) << (KEY & 63)))

		if (is_key_pressed(KEYBOARD_KEY_A))
		{
			//printf("rotation: %f \n", time * 360.0f);
			time += 1.0f / 360.0f;
		}

		if (is_key_pressed(KEYBOARD_KEY_D))
		{
			//printf("rotation: %f \n", time * 360.0f);
			time -= 1.0f / 360.0f;
		}

		for (size_t ev_id = 0; ev_id < ev_size; ++ev_id)
		{
			if (INPUT_EVENT_TYPE_DEVICE_READING == ev_arr[ev_id].event_type && INPUT_DEVICE_TYPE_KEYBOARD == ev_arr[ev_id].device_type)
			{
				uint64_t* buttons = ev_arr[ev_id].device_state.keyboard.buttons;

#define is_key_triggered(KEY) (buttons[KEY >> 6] & (((uint64_t)1) << (KEY & 63))) && 0 == (kb_state.buttons[KEY >> 6] & (((uint64_t)1) << (KEY & 63)))

				/*if (is_key_triggered(KEYBOARD_KEY_Q))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = 2;
					move_info.position[1] = 2;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}

				if (is_key_triggered(KEYBOARD_KEY_W))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = 2.82;
					move_info.position[1] = 0;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}

				if (is_key_triggered(KEYBOARD_KEY_E))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = 2;
					move_info.position[1] = -2;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}
				
				if (is_key_triggered(KEYBOARD_KEY_A))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = 0;
					move_info.position[1] = 2.82;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}

				if (is_key_triggered(KEYBOARD_KEY_D))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = 0;
					move_info.position[1] = -2.82;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}

				if (is_key_triggered(KEYBOARD_KEY_Z))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = -2;
					move_info.position[1] = 2;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}

				if (is_key_triggered(KEYBOARD_KEY_X))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = -2.82;
					move_info.position[1] = 0;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}

				if (is_key_triggered(KEYBOARD_KEY_C))
				{
					SoundChannelMoveInfo move_info;
					move_info.position[0] = -2;
					move_info.position[1] = -2;
					move_info.position[2] = 0;
					sound_channel_move(engine, 0, &move_info);
				}*/

				if (buttons[KEYBOARD_KEY_ESCAPE >> 6] & (((uint64_t)1) << (KEYBOARD_KEY_ESCAPE & 63)))
				{
					input_module_clean(input, INPUT_DEVICE_TYPE_KEYBOARD);
					input_clean(input);

					return 0;
				}

				kb_state = ev_arr[ev_id].device_state.keyboard;
			}
		}

		sound_engine_update(engine);
	}


}

#endif