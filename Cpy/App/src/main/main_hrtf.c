#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

#define CT_AUDIO_LIB_EXPORT 1
#include "../../Engine/src/sound/sound_engine.h"
#include "..\..\System\lib\file.h"

//#include <math.h>
#include "audio_hartley.h"




static bool_t read_file(void** buffer, size_t* size, char const* path,  Allocator allocator)
{
	OsFileCreateInfo file_info;
	file_info.epoll = null;
	file_info.create = OS_FILE_CREATE_MUST_EXIST;
	file_info.access = OS_FILE_ACCESS_READ;
	file_info.attributes = 0;

	OsFile file;
	uint64_t file_size = 0;
	void* file_buffer = null;
	OsFileResult file_result;
	bool_t bool_result;

	file_result = os_file_create(&file, path, &file_info);
	if (OS_FILE_RESULT_S_OK != file_result)
		return false;
	
	bool_result = os_file_get_size(file, &file_size);
	assert(true == bool_result);
	file_buffer = mem_alloc(allocator, file_size);
	bool_result = os_file_read(file, file_buffer, file_size, null);
	assert(true == bool_result);
	os_file_close(file);

	*buffer = file_buffer;
	*size = file_size;

	return true;
}

#include "audio_file_wav.h"
int CALLBACK MainHeader()
{
	core_init_allocators();

	Allocator allocator;
	{
		AllocatorCreateInfo info = { 0 };
		info.type = ALLOCATOR_TYPE_MALLOC;
		info.name = "App allocator";

		allocator = allocator_create(&info);
	}

	enum
	{
		HT_POWER = 8,
		HT_SIZE = 1 << HT_POWER,
	};

	CtAudioHartleyTable ht = ct_audio_hartley_table_create(HT_POWER, mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, ct_audio_hartley_table_size(HT_POWER)));

	//for (int elevation = -40; elevation <= 90; elevation += 10)
	int elevation = 0;
	{
		int pos_elev = 0 > elevation ? 360 + elevation : elevation;
		OsFile out_file;
		{
			OsFileCreateInfo file_info;
			file_info.epoll = null;
			file_info.create = OS_FILE_CREATE_OVERWRITE;
			file_info.access = OS_FILE_ACCESS_WRITE;
			file_info.attributes = 0;

			OsFileResult file_result;

			char path[256];
			sprintf(path, "D:/HrtfAvg/audio_hrtf_e%i.h", pos_elev);
			file_result = os_file_create(&out_file, path, &file_info);
			assert(OS_FILE_RESULT_S_OK <= file_result);
		}
		int angle_array[180];
		int angle_count = 0;
		for (int angle = 0; angle <= 180; ++angle)
		{
			void* file;
			size_t size;
			char path[256];
			sprintf(path, "D:/HrtfAvg/H%ie%.3ia.wav", elevation, angle);
			if (!read_file(&file, &size, path, allocator))
				continue;
			angle_array[angle_count] = angle;
			++angle_count;

			CtAudioFileWav wav;
			/*int16_t*/float const* samples = ct_audio_file_wav(&wav, file, size);
			assert(samples);
			//assert(CT_AUDIO_FORMAT_INT16 == wav.format);
			assert(CT_AUDIO_FORMAT_FLOAT == wav.format);
			assert(128 == wav.frame_count);
			assert(2 == wav.channel_count);

			alignas(CT_AUDIO_ALIGN) float buffer_l[HT_SIZE] = { 0 };
			alignas(CT_AUDIO_ALIGN) float buffer_r[HT_SIZE] = { 0 };
			for (size_t i = 0; i < HT_SIZE / 2; ++i)
			{
				buffer_l[i] = samples[i * 2 + 0];// / 32768.0f;
				buffer_r[i] = samples[i * 2 + 1];// / 32768.0f;
			}
			mem_free(allocator, file, size);

			alignas(CT_AUDIO_ALIGN) float buffer_l1[HT_SIZE] = { 0 };
			alignas(CT_AUDIO_ALIGN) float buffer_l2[HT_SIZE] = { 0 };
			alignas(CT_AUDIO_ALIGN) float buffer_r1[HT_SIZE] = { 0 };
			alignas(CT_AUDIO_ALIGN) float buffer_r2[HT_SIZE] = { 0 };
			float* h_l = ct_audio_hartley_transform(ht, buffer_l, buffer_l1, buffer_l2, 1);
			float* h_r = ct_audio_hartley_transform(ht, buffer_r, buffer_r1, buffer_r2, 1);
			
			char out_buffer_l[HT_SIZE * 20];
			char out_buffer_r[HT_SIZE * 20];
			char* ptr_out_l = out_buffer_l;
			char* ptr_out_r = out_buffer_r;

			char const head[] = "	{ ";
			ptr_out_l += sprintf(ptr_out_l, "static float const HRTF_ELEVATION_%i_ANGLE_%i[2][256] = \n{ \n%s", pos_elev, angle, head);
			ptr_out_r += sprintf(ptr_out_r, "%s", head);

			for (size_t i = 0; i < HT_SIZE; ++i)
			{
				ptr_out_l += sprintf(ptr_out_l, "%.9Ef, ", h_l[i]);
				ptr_out_r += sprintf(ptr_out_r, "%.9Ef, ", h_r[i]);
			}
			char const tail[] = "}, \n";
			ptr_out_l += sprintf(ptr_out_l, "%s", tail);
			ptr_out_r += sprintf(ptr_out_r, "%s}; \n", tail);


			os_file_write(out_file, out_buffer_l, ptr_out_l - out_buffer_l, null);
			os_file_write(out_file, out_buffer_r, ptr_out_r - out_buffer_r, null);
		}

		{
			char out_buffer[4096];
			char* ptr_out = out_buffer;
			ptr_out += sprintf(ptr_out, "typedef float const Hrtf[2][256]; \n");
			ptr_out += sprintf(ptr_out, "static Hrtf* const HRTF_ELEVATION_%i[] = \n{ \n", pos_elev);
			for (int i = 0; i < angle_count; ++i)
			{
				int angle = angle_array[i];
				ptr_out += sprintf(ptr_out, "	&HRTF_ELEVATION_%i_ANGLE_%i, \n", pos_elev, angle);
			}
			ptr_out += sprintf(ptr_out, "}; \n");
			ptr_out += sprintf(ptr_out, "typedef float Vbap[3]; \n");
			ptr_out += sprintf(ptr_out, "static Vbap const VBAP_ELEVATION_%i[] = \n{ \n", pos_elev);
			for (int i = 0; i < angle_count; ++i)
			{
				int angle = angle_array[i];
				float rad_a = m_pi_f32 * angle / 180.0f;
				float rad_e = m_pi_f32 * elevation / 180.0f;
				ptr_out += sprintf(ptr_out, "	{ %.9Ef, %.9Ef, %.9Ef, }, \n", m_cos_f32(rad_a) * m_cos_f32(rad_e), m_sin_f32(rad_a) * m_cos_f32(rad_e), m_sin_f32(rad_e));
			}
			ptr_out += sprintf(ptr_out, "}; \n");

			os_file_write(out_file, out_buffer, ptr_out - out_buffer, null);
		}

		os_file_close(out_file);
	}

}
//*
#include "D:/HrtfAvg/audio_hrtf_e0.h"
/*#include "D:/HrtfCompact/audio_hrtf_e0.h"
#include "D:/HrtfCompact/audio_hrtf_e10.h"
#include "D:/HrtfCompact/audio_hrtf_e20.h"
#include "D:/HrtfCompact/audio_hrtf_e30.h"
#include "D:/HrtfCompact/audio_hrtf_e40.h"
#include "D:/HrtfCompact/audio_hrtf_e50.h"
#include "D:/HrtfCompact/audio_hrtf_e60.h"
#include "D:/HrtfCompact/audio_hrtf_e70.h"
#include "D:/HrtfCompact/audio_hrtf_e80.h"
#include "D:/HrtfCompact/audio_hrtf_e90.h"
#include "D:/HrtfCompact/audio_hrtf_e320.h"
#include "D:/HrtfCompact/audio_hrtf_e330.h"
#include "D:/HrtfCompact/audio_hrtf_e340.h"
#include "D:/HrtfCompact/audio_hrtf_e350.h"*/
//*/

//http://www.freesteel.co.uk/wpblog/2009/06/05/encoding-2d-angles-without-trigonometry/
/*static float diamond_atan2(float y, float x) //max error 4.07 degrees???
{
	if (y >= 0)
		return (x >= 0 ? y / (x + y) : 1 - x / (-x + y));
	else
		return (x < 0 ? 2 - y / (-x - y) : 3 + x / (x - y));
}*/

static float diamond_atan2(float y, float x) //max error 4.07 degrees! y always positive!
{
	return (x >= 0 ? y / (x + y) : 1 - x / (-x + y));
}

void find_vbap(float const* dir, float* gain, float const** hrtf0l, float const** hrtf1l, float const** hrtf0r, float const** hrtf1r)
{
	float x = dir[0];
	float y = dir[1];

	//fix l/r indexing
	int _l = 1;
	int _r = 0;
	if (0 > y)
	{
		y = -y;
		_l = 0;
		_r = 1;
	}

	float angle = diamond_atan2(y, x) * 90.0f;
	int32_t idx1 = (int32_t)(m_max_f32(angle - 5, 0) / 180.0f * lengthof(HRTF_ELEVATION_0)); //-1???
	while (lengthof(HRTF_ELEVATION_0) > idx1 && VBAP_ELEVATION_0[idx1][0] * y > VBAP_ELEVATION_0[idx1][1] * x)
	{
		++idx1;
	}

	if (0 == idx1)
		idx1 = 1;
	int32_t idx2 = idx1 - 1;

	float inv_det = 1.0f / (VBAP_ELEVATION_0[idx1][0] * VBAP_ELEVATION_0[idx2][1] - VBAP_ELEVATION_0[idx2][0] * VBAP_ELEVATION_0[idx1][1]);
	float gain1 = inv_det * (+ VBAP_ELEVATION_0[idx2][1] * x - VBAP_ELEVATION_0[idx2][0] * y);
	float gain2 = inv_det * (- VBAP_ELEVATION_0[idx1][1] * x + VBAP_ELEVATION_0[idx1][0] * y);

	float inv_gain = 1.0f / /*m_sqrt_f32*/(m_sqrt_f32(gain1 * gain1 + gain2 * gain2));
	inv_gain = 1.0f / (gain1 + gain2);
	gain1 *= inv_gain;
	gain2 *= inv_gain;
	/*float comp_factor = -0.5f;
	float comp = gain1 - 0.5f;
	comp = comp * comp * comp_factor;
	comp = 1 + comp - comp_factor;
	gain1 *= comp;
	gain2 *= comp;*/

	gain[0] = /*m_sqrt_f32(m_sqrt_f32*/(gain1);
	gain[1] = /*m_sqrt_f32(m_sqrt_f32*/(gain2);
	*hrtf0l = HRTF_ELEVATION_0[idx1][0][_l];
	*hrtf1l = HRTF_ELEVATION_0[idx2][0][_l];
	*hrtf0r = HRTF_ELEVATION_0[idx1][0][_r];
	*hrtf1r = HRTF_ELEVATION_0[idx2][0][_r];

	//gain[0] = 1;
	//gain[1] = 0;
}

#include <math.h>
int CALLBACK MainAvg()
{
	core_init_allocators();

	Allocator allocator;
	{
		AllocatorCreateInfo info = { 0 };
		info.type = ALLOCATOR_TYPE_MALLOC;
		info.name = "App allocator";

		allocator = allocator_create(&info);
	}

	enum
	{
		HT_POWER = 7,
		HT_SIZE = 1 << HT_POWER,

		TD_OFFSET = 15,
	};

	CtAudioHartleyTable ht = ct_audio_hartley_table_create(HT_POWER, mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, ct_audio_hartley_table_size(HT_POWER)));
	
	
	CtAudioFileWav wav;
	int subjects[] = 
	{ 
		1002,
		/*1003,
		1004,
		1005,
		1006,
		1007,
		1008,
		1009,
		1012,
		1013,
		1014,
		1015,
		1016,
		1017,
		1018,*/
	};

	alignas(CT_AUDIO_ALIGN) float buffer_inv_l[HT_SIZE] = { 0 };
	alignas(CT_AUDIO_ALIGN) float buffer_inv_r[HT_SIZE] = { 0 };

	for (int ang = 0; ang <= 180; ang += 15)
	{
		alignas(CT_AUDIO_ALIGN) float sum_ampl[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float sum_phase[HT_SIZE] = { 0 };
		for (int sub = 0; sub < lengthof(subjects); ++sub)
		{
			void* file;
			size_t size;
			char path[256];
			//snprintf(path, sizeof(path), "D:/Hrtf/IRC_%i/COMPENSATED/WAV/IRC_%i_C/IRC_%i_C_R0195_T%.3i_P000.wav", subjects[sub], subjects[sub], subjects[sub], ang);
			snprintf(path, sizeof(path), "D:/HrtfCompact/elev0/H0e%.3ia.wav", ang);
			bool_t br = read_file(&file, &size, path, allocator);
			assert(br);
			char const* samples = ct_audio_file_wav(&wav, file, size);
			assert(samples);

			for (size_t i = 0; i < HT_SIZE; ++i)
			{
				int16_t const* isamples = (int16_t const*)samples;
				sum_ampl[i] += (float)(isamples[i * 2 + 0] / (0.5f * 65536.0f));
				sum_phase[i] += (float)(isamples[i * 2 + 1] / (0.5f * 65536.0f));
			}
		
			/*for (size_t i = 0; i < HT_SIZE; ++i)
			{
				int32_t val = 0;
				memcpy(((char*)&val) + 1, samples + (i + TD_OFFSET) * 3 * 2 + 3, 3);
				val >>= 8;
				sum_ampl[i] += (float)(val / 16777216.0);;
				val = 0;
				memcpy(((char*)&val) + 1, samples + (i + TD_OFFSET) * 3 * 2 + 0, 3);
				val >>= 8;
				sum_phase[i] += (float)(val / 16777216.0);;
			}*/
			mem_free(allocator, file, size);

			
		}

		alignas(CT_AUDIO_ALIGN) float buffer1[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float buffer2[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float buffer3[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float buffer4[HT_SIZE] = { 0 };
		float* imp1 = ct_audio_hartley_transform(ht, sum_ampl, buffer1, buffer2, 1);
		float real1[HT_SIZE];
		float imag1[HT_SIZE];
		float* imp2 = ct_audio_hartley_transform(ht, sum_phase, buffer3, buffer4, 1);
		float real2[HT_SIZE];
		float imag2[HT_SIZE];

		alignas(CT_AUDIO_ALIGN) float buffer5[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float buffer6[HT_SIZE] = { 0 };
		for (uint32_t i = 0; i < (HT_SIZE); ++i)
		{
			real1[i] = 0.5f * (imp1[i] + imp1[(HT_SIZE - 1) & (HT_SIZE - i)]);
			imag1[i] = 0.5f * (imp1[i] - imp1[(HT_SIZE - 1) & (HT_SIZE - i)]);

			real2[i] = 0.5f * (imp2[i] + imp2[(HT_SIZE - 1) & (HT_SIZE - i)]);
			imag2[i] = 0.5f * (imp2[i] - imp2[(HT_SIZE - 1) & (HT_SIZE - i)]);

			buffer5[i] = m_sqrt_f32(real1[i] * real1[i] + imag1[i] * imag1[i]);
			buffer6[i] = m_sqrt_f32(real2[i] * real2[i] + imag2[i] * imag2[i]);

			//sum_ampl[i] += sqrtf(real[i] * real[i] + imag[i] * imag[i]);
			//sum_phase[i] += atan2f(imag[i], real[i]);
		}

		/*if (0 == ang)
		{
			memcpy(buffer_inv_l, buffer5, sizeof(buffer5));
			memcpy(buffer_inv_r, buffer6, sizeof(buffer6));
		}

		for (uint32_t i = 0; i < (HT_SIZE); ++i)
		{
			buffer5[i] /= buffer_inv_l[i];
			buffer6[i] /= buffer_inv_r[i];
		}*/

	
		alignas(CT_AUDIO_ALIGN) float buffer7[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float buffer8[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float buffer9[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float buffer10[HT_SIZE] = { 0 };
		imp1 = ct_audio_hartley_transform(ht, buffer5, buffer7, buffer8, 0.5f / HT_SIZE);
		imp2 = ct_audio_hartley_transform(ht, buffer6, buffer9, buffer10, 0.5f / HT_SIZE);

		alignas(CT_AUDIO_ALIGN) float outb[HT_SIZE * 2] = { 0 };
		for (size_t i = 0; i < HT_SIZE; ++i)
		{
			outb[i * 2 + 0] = imp1[i];
			outb[i * 2 + 1] = imp2[i];
		}

		alignas(CT_AUDIO_ALIGN)byte_t header[CT_AUDIO_FILE_WAV_HEADER_SIZE];
		wav.format = CT_AUDIO_FORMAT_FLOAT;
		wav.frame_count = HT_SIZE;
		ct_audio_file_wav_header(&wav, header);

		{
			OsFile out_file;
			OsFileCreateInfo file_info;
			file_info.epoll = null;
			file_info.create = OS_FILE_CREATE_OVERWRITE;
			file_info.access = OS_FILE_ACCESS_WRITE;
			file_info.attributes = 0;

			OsFileResult file_result;

			char path[256];
			snprintf(path, sizeof(path), "D:/HrtfAvg/H0e%.3ia.wav", ang);
			file_result = os_file_create(&out_file, path, &file_info);
			assert(OS_FILE_RESULT_S_OK <= file_result);
			os_file_write(out_file, header, sizeof(header), null);
			os_file_write(out_file, outb, sizeof(outb), null);
			os_file_close(out_file);
		}
	}
}

void interp_htrf(float out[256], float gain[2], float const hrtf1[256], float const hrtf2[256])
{
	float real1[256];
	float imag1[256];
	float real2[256];
	float imag2[256];
	for (uint32_t i = 0; i < 256; ++i)
	{
		real1[i] = 0.5f * (hrtf1[i] + hrtf1[255 & (256 - i)]);
		imag1[i] = 0.5f * (hrtf1[i] - hrtf1[255 & (256 - i)]);
		real2[i] = 0.5f * (hrtf2[i] + hrtf2[255 & (256 - i)]);
		imag2[i] = 0.5f * (hrtf2[i] - hrtf2[255 & (256 - i)]);
	}
	for (uint32_t i = 0; i < 256; ++i)
	{
		float amp1 = m_sqrt_f32(real1[i] * real1[i] + imag1[i] * imag1[i]);
		float amp2 = m_sqrt_f32(real2[i] * real2[i] + imag2[i] * imag2[i]);
		out[i] = amp1 * gain[0] + amp2 * gain[1];

		float real = (real1[i] * gain[0] + real2[i] * gain[1]);
		float imag = (imag1[i] * gain[0] + imag2[i] * gain[1]);
		float amp = m_sqrt_f32(real * real + imag * imag);
		//amp = 1;
		/*float imag1[256];
		float real2[256];
		float imag2[256];*/
		//out[i] = ((real + imag) / amp);// *(amp1 * gain[0] + amp2 * gain[1]);
		out[i] = hrtf1[i] * gain[0] + hrtf2[i] * gain[1];

		/*if (amp1 > amp2)
		{
			float scale = (amp1 * gain[0] + amp2 * gain[1]) / amp1;
			out[i] = hrtf1[i] * scale;
		}
		else
		{
			float scale = (amp1 * gain[0] + amp2 * gain[1]) / amp2;
			out[i] = hrtf2[i] * scale;
		}*/
		//out[i] = (real1[i] + imag1[i]) *scale;
	}

	/*for (uint32_t i = 0; i < 256; ++i)
	{
		out[i] = hrtf1[i] * gain[0] + hrtf2[i] * gain[1];
		out[i] = hrtf1[i] * gain[0] + hrtf2[i] * gain[1];
	}*/
}
#include "..\..\System\lib\clock.h"

int CALLBACK WinMainRot()
{
	os_clock_init();
	core_init_allocators();

	Allocator allocator;
	{
		AllocatorCreateInfo info = { 0 };
		info.type = ALLOCATOR_TYPE_MALLOC;
		info.name = "App allocator";

		allocator = allocator_create(&info);
	}

	enum
	{
		HT_POWER = 8,
		HT_SIZE = 1 << HT_POWER,
		HT_BLOCK = HT_SIZE >> 1,
	};

	CtAudioHartleyTable ht = ct_audio_hartley_table_create(HT_POWER, mem_alloc_aligned(allocator, CT_AUDIO_ALIGN, ct_audio_hartley_table_size(HT_POWER)));

	float* input = null;
	CtAudioFileWav wav;
	{
		void* file;
		size_t size;
		//char const* path = "D:/palo_lala.wav";
		char const* path = "D:/engine_filter.wav";
		//char const* path = "D:/AnechoicVoice.wav";
		//char const* path = "D:/tone200.wav";
		//char const* path = "D:/tone1200.wav";
		bool_t br = read_file(&file, &size, path,allocator);
		assert(br);
		input = ct_audio_file_wav(&wav, file, size);
		assert(input);
		assert(CT_AUDIO_FORMAT_FLOAT == wav.format);
		//mem_free(allocator, file, size);
		float* new_input = mem_alloc(allocator, wav.frame_count * 4 * 2);
		memcpy(new_input + wav.frame_count * 0, input, wav.frame_count * 4);
		memcpy(new_input + wav.frame_count * 1, input, wav.frame_count * 4);
		//memcpy(new_input + wav.frame_count * 2, input, wav.frame_count * 4);
		//memcpy(new_input + wav.frame_count * 3, input, wav.frame_count * 4);
		wav.frame_count *= 2;
		input = new_input;
	}

	float* output = mem_alloc(allocator, sizeof(float) * 2 * wav.frame_count);

	alignas(CT_AUDIO_ALIGN) float ov_save[HT_BLOCK] = { 0 };
	os_clock_t ts = 0;
	int tcnt = 0;
	alignas(CT_AUDIO_ALIGN) float ht_impulse_old_l[HT_SIZE] = { 0 };
	alignas(CT_AUDIO_ALIGN) float ht_impulse_old_r[HT_SIZE] = { 0 };
	for (uint32_t i = 0; i + HT_BLOCK <= wav.frame_count; i += HT_BLOCK)
	{
		alignas(CT_AUDIO_ALIGN) float ht_impulse_l[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float ht_impulse_r[HT_SIZE] = { 0 };
		{
			float gain[2];
			float angle = - 0 + 0.2f * m_pi_f32 * i / HT_BLOCK / 180.0f;
			float dir[2];
			//angle = 0;
			//angle = m_pi_f32 * 145.0f / 180.0f;
			dir[0] = m_cos_f32(angle);
			dir[1] = m_sin_f32(angle);

			float const* hrtf0l;
			float const* hrtf1l;
			float const* hrtf0r;
			float const* hrtf1r;
			find_vbap(dir, gain, &hrtf0l, &hrtf1l, &hrtf0r, &hrtf1r);

			interp_htrf(ht_impulse_l, gain, hrtf0l, hrtf1l);
			interp_htrf(ht_impulse_r, gain, hrtf0r, hrtf1r);
			for (uint32_t i = 0; i < HT_SIZE; ++i)
			{
				//ht_impulse_l[i] = hrtf0l[i] * gain[0] + hrtf1l[i] * gain[1];
				//ht_impulse_r[i] = hrtf0r[i] * gain[0] + hrtf1r[i] * gain[1];

				/*ht_impulse_l[i] = hrtf0l[i];
				ht_impulse_r[i] = hrtf0r[i];

				ht_impulse_old_l[i] = hrtf1l[i];
				ht_impulse_old_r[i] = hrtf1r[i];*/
			}
		}

		alignas(CT_AUDIO_ALIGN) float ht_input[HT_SIZE] = { 0 };
		memcpy(ht_input, ov_save, sizeof(float) * HT_BLOCK);
		memcpy(ht_input + HT_BLOCK, input + i, sizeof(float) * HT_BLOCK);
		memcpy(ov_save, input + i, sizeof(float) * HT_BLOCK);

		alignas(CT_AUDIO_ALIGN) float ht_scratch[HT_SIZE] = { 0 };
		float* ht_transform = ct_audio_hartley_transform(ht, ht_input, ht_input, ht_scratch, 1);

		alignas(CT_AUDIO_ALIGN) float ht_response_l[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float ht_response_r[HT_SIZE] = { 0 };
		ct_audio_hartley_convolve(ht, ht_response_l, ht_transform, ht_impulse_l);
		ct_audio_hartley_convolve(ht, ht_response_r, ht_transform, ht_impulse_r);
		alignas(CT_AUDIO_ALIGN) float ht_response_old_l[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float ht_response_old_r[HT_SIZE] = { 0 };
		ct_audio_hartley_convolve(ht, ht_response_old_l, ht_transform, ht_impulse_old_l);
		ct_audio_hartley_convolve(ht, ht_response_old_r, ht_transform, ht_impulse_old_r);
		//memcpy(ht_response_l, ht_transform, sizeof(float) * HT_SIZE);
		//memcpy(ht_response_r, ht_transform, sizeof(float) * HT_SIZE);

		os_clock_t t1 = os_clock();
		alignas(CT_AUDIO_ALIGN) float ht_scratch_l[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float ht_scratch_r[HT_SIZE] = { 0 };
		float* ht_output_l = ct_audio_hartley_transform(ht, ht_response_l, ht_response_l, ht_scratch_l, 0.5f / HT_SIZE);
		float* ht_output_r = ct_audio_hartley_transform(ht, ht_response_r, ht_response_r, ht_scratch_r, 0.5f / HT_SIZE);

		alignas(CT_AUDIO_ALIGN) float ht_scratch_old_l[HT_SIZE] = { 0 };
		alignas(CT_AUDIO_ALIGN) float ht_scratch_old_r[HT_SIZE] = { 0 };
		float* ht_output_old_l = ct_audio_hartley_transform(ht, ht_response_old_l, ht_response_old_l, ht_scratch_old_l, 0.5f / HT_SIZE);
		float* ht_output_old_r = ct_audio_hartley_transform(ht, ht_response_old_r, ht_response_old_r, ht_scratch_old_r, 0.5f / HT_SIZE);
		os_clock_t t2 = os_clock();
		ts += t2 - t1;
		tcnt++;

		float* out_block = output + i * 2;
		for (uint32_t j = 0; j < HT_BLOCK; ++j)
		{
			float factor1 = m_sin_f32(m_pi_f32 * 0.5f * j / HT_BLOCK);
			factor1 = factor1 * factor1;
			//factor1 = 1;
			float factor2 = m_cos_f32(m_pi_f32 * 0.5f * j / HT_BLOCK);
			factor2 = factor2 * factor2;
			//factor2 = 0;

			//factor1 = gain[0];
			//factor2 = gain[1];
			out_block[j * 2 + 0] = ht_output_l[HT_BLOCK + j] * factor1 + ht_output_old_l[HT_BLOCK + j] * factor2;
			out_block[j * 2 + 1] = ht_output_r[HT_BLOCK + j] * factor1 + ht_output_old_r[HT_BLOCK + j] * factor2;
		}
		memcpy(ht_impulse_old_l, ht_impulse_l, sizeof(ht_impulse_old_l));
		memcpy(ht_impulse_old_r, ht_impulse_r, sizeof(ht_impulse_old_r));
	}

	ts = os_clock_to(ts, 1000000);
	char buffprt[256];
	snprintf(buffprt, 256, "%f \n", (double)ts / tcnt);
	OutputDebugStringA(buffprt);

	wav.channel_count = 2;
	wav.channel_mask = CT_AUDIO_CHANNEL_FRONT_LEFT | CT_AUDIO_CHANNEL_FRONT_RIGHT;
	wav.format = CT_AUDIO_FORMAT_FLOAT;
	wav.frame_count = wav.frame_count - (wav.frame_count & (HT_BLOCK - 1));
	
	alignas(4) byte_t wav_buffer[CT_AUDIO_FILE_WAV_HEADER_SIZE] = { 0 };
	void* wav_header = ct_audio_file_wav_header(&wav, wav_buffer);
	assert(wav_header);

	{
		OsFile out_file;
		OsFileCreateInfo file_info;
		file_info.epoll = null;
		file_info.create = OS_FILE_CREATE_OVERWRITE;
		file_info.access = OS_FILE_ACCESS_WRITE;
		file_info.attributes = 0;

		OsFileResult file_result;
		file_result = os_file_create(&out_file, "D:/Hrtf_out.wav", &file_info);
		assert(OS_FILE_RESULT_S_OK <= file_result);

		os_file_write(out_file, wav_header, CT_AUDIO_FILE_WAV_HEADER_SIZE, null);
		os_file_write(out_file, output, sizeof(float) * 2 * wav.frame_count, null);
		os_file_close(out_file);
	}
}

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow)
{
	MainAvg();
	//MainHeader();
	//WinMainRot();
}