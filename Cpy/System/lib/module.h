#pragma once

typedef struct OsModule*			OsModule;
typedef	void						OsModuleFnc(void);
typedef	void						OsModuleVar;

bool_t			os_module_load(OsModule* module, char8_t const* path);
void			os_module_free(OsModule module);
OsModuleFnc*	os_module_find_function(OsModule module, char8_t const* name);
OsModuleVar*	os_module_find_variable(OsModule module, char8_t const* name);
