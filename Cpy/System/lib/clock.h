#pragma once

typedef int64_t os_clock_t;

bool_t		os_clock_init();
os_clock_t	os_clock();
os_clock_t	os_clocks_per_sec();
os_clock_t	os_clock_to(os_clock_t clock, os_clock_t os_clocks_per_sec);
