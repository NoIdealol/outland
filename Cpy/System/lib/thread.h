#pragma once

typedef struct OsThread*			OsThread;
typedef struct OsMutex*				OsMutex;
typedef struct OsSemaphore*			OsSemaphore;
typedef struct OsEvent*				OsEvent;

typedef	uint8_t OsThreadMain(void* thread_ctx);

bool_t		os_thread_create(OsThread* thread, OsThreadMain* main, void* ctx, char8_t const* name, size_t stack_size);
bool_t		os_thread_join(OsThread thread, uint8_t* exit_code, uint32_t timeout_ms);
void		os_thread_detach(OsThread thread);
void		os_thread_terminate(OsThread thread);

void		os_thread_sleep(uint32_t time_ms);
void		os_thread_yield();

bool_t		os_mutex_create(OsMutex* mutex, uint32_t spin_count);
void		os_mutex_destroy(OsMutex mutex);
void		os_mutex_lock(OsMutex mutex);
void		os_mutex_unlock(OsMutex mutex);
bool_t		os_mutex_try_lock(OsMutex mutex);

bool_t		os_event_create(OsEvent* event, bool_t is_signaled);
void		os_event_destroy(OsEvent event);
void		os_event_signal(OsEvent event);
void		os_event_clear(OsEvent event);
void		os_event_wait(OsEvent event);

bool_t		os_semaphore_create(OsSemaphore* semaphore, uint32_t max_signal_count, uint32_t signal_count);
void		os_semaphore_destroy(OsSemaphore semaphore);
void		os_semaphore_signal(OsSemaphore semaphore, uint32_t signal_count);
void		os_semaphore_wait(OsSemaphore semaphore);

uint32_t	os_cache_line_size();

int32_t		os_atomic_load_i32(int32_t volatile* var);
int32_t		os_atomic_exchange_i32(int32_t volatile* var, int32_t val);