#pragma once

typedef struct OsFile*				OsFile;
typedef struct OsFileEpoll*			OsFileEpoll;

typedef enum OsFileResult
{
	OS_FILE_RESULT_E_UNEXPECTED = -8,
	OS_FILE_RESULT_E_OUT_OF_HANDLES,
	OS_FILE_RESULT_E_ACCESS_DENIED,
	OS_FILE_RESULT_E_ALREADY_EXISTS,
	OS_FILE_RESULT_E_PATH_NOT_FOUND,
	OS_FILE_RESULT_E_FILE_NOT_FOUND,
	OS_FILE_RESULT_E_INVALID_PATH,
	OS_FILE_RESULT_E_INVALID_PARAMETER,
	OS_FILE_RESULT_S_OK,
	OS_FILE_RESULT_S_ALREADY_EXISTS,
	OS_FILE_RESULT_S_FILE_NOT_FOUND,
} OsFileResult;

enum OsFileAttributeBits
{
	OS_FILE_ATTRIBUTE_FOLDER		= 0x1,
	OS_FILE_ATTRIBUTE_READ_ONLY		= 0x2,
	OS_FILE_ATTRIBUTE_HIDDEN		= 0x4,
	OS_FILE_ATTRIBUTE_SYSTEM		= 0x8,
};
typedef flags_t OsFileAttributeFlags;

enum OsFileAccessBits
{
	OS_FILE_ACCESS_READ		= 0x1,
	OS_FILE_ACCESS_WRITE	= 0x2,
	OS_FILE_ACCESS_AIO		= 0x4,
};
typedef flags_t OsFileAccessFlags;

typedef enum OsFileCreate
{
	OS_FILE_CREATE_MUST_EXIST,
	OS_FILE_CREATE_NEW_ONLY,
	OS_FILE_CREATE_OVERWRITE,
	OS_FILE_CREATE_PRESERVE,
} OsFileCreate;

typedef struct OsFileCreateInfo
{
	OsFileEpoll				epoll;
	OsFileCreate			create;
	OsFileAccessFlags		access;
	OsFileAttributeFlags	attributes;
} OsFileCreateInfo;

typedef struct OsFileEvent
{
	OsFile	file;
	void*	aio;
	bool_t	result;
} OsFileEvent;

OsFileResult	os_file_create(OsFile* file, char8_t const* path, OsFileCreateInfo const* info);
OsFileResult	os_file_delete(char8_t const* path);
void			os_file_close(OsFile file);
bool_t			os_file_get_size(OsFile file, uint64_t* size);
bool_t			os_file_set_size(OsFile file, uint64_t size);
bool_t			os_file_set_head(OsFile file, uint64_t offset);
bool_t			os_file_get_head(OsFile file, uint64_t* offset);
bool_t			os_file_read(OsFile file, void* buffer, size_t size, size_t* read);
bool_t			os_file_write(OsFile file, void const* buffer, size_t size, size_t* written);
OsFileResult	os_file_get_attributes(char8_t const* path, OsFileAttributeFlags* attributes);
OsFileResult	os_file_set_attributes(char8_t const* path, OsFileAttributeFlags attributes);

bool_t	os_file_aio_read(OsFile file, void* buffer, size_t size, uint64_t offset, void* aio);
bool_t	os_file_aio_write(OsFile file, void const* buffer, size_t size, uint64_t offset, void* aio);
size_t	os_file_aio_size(size_t user_size);
void*	os_file_aio_user(void* aio);

bool_t	os_file_epoll_create(OsFileEpoll* epoll);
void	os_file_epoll_destroy(OsFileEpoll epoll);
size_t	os_file_epoll_wait(OsFileEpoll epoll, OsFileEvent* ev_buffer, size_t ev_capacity, uint32_t wait_ms);
