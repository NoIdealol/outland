#pragma once
#include "core.h"

//#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>

enum
{
	OS_MAX_FILE_PATH = 1024,
};

size_t			os_utf8_to_wchar(char8_t const* utf8, wchar_t* wstr, size_t capacity);
size_t			os_wchar_to_utf8(wchar_t const* wstr, char8_t* utf8, size_t capacity);
void			os_path_to_system(wchar_t* wstr);
void			os_crash(char8_t* msg);