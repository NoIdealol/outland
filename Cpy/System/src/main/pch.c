#include "pch.h"

//--------------------------------------------------
size_t	os_utf8_to_wchar(char8_t const* utf8, wchar_t* wstr, size_t capacity)
{
	//returns num of bytes required
	int charsReq = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, wstr, (int)capacity);
	if (1 > charsReq)
		return 0;

	return (size_t)charsReq;
}

//--------------------------------------------------
size_t	os_wchar_to_utf8(wchar_t const* wstr, char8_t* utf8, size_t capacity)
{
	//returns num of bytes required
	int charsReq = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, utf8, (int)capacity, NULL, NULL);
	if (1 > charsReq)
		return 0;

	return (size_t)charsReq;
}

//--------------------------------------------------
void os_path_to_system(wchar_t* wstr)
{
	while (*wstr)
	{
		if (L'/' == *wstr)
			*wstr = L'\\';
		++wstr;
	}
}

//--------------------------------------------------
void	os_crash(char8_t* msg)
{
	abort();
}