#include "pch.h"
#include "module.h"

#if OS_WINDOWS

//--------------------------------------------------
bool_t		os_module_load(OsModule* module, char8_t const* path)
{
	wchar_t wstr[OS_MAX_FILE_PATH];
	size_t wlen = os_utf8_to_wchar(path, wstr, lengthof(wstr));
	if (!wlen)
	{
		return false;
	}

	os_path_to_system(wstr);

	HMODULE hModule = LoadLibraryW(wstr);
	if (NULL == hModule)
		return false;

	*module = (OsModule)hModule;
	return true;
}

//--------------------------------------------------
void			os_module_free(OsModule module)
{
	FreeLibrary((HMODULE)module);
}

//--------------------------------------------------
OsModuleFnc*	os_module_find_function(OsModule module, char8_t const* name)
{
	FARPROC farproc = GetProcAddress((HMODULE)module, name);
	return (OsModuleFnc*)farproc;
}

//--------------------------------------------------
OsModuleVar*	os_module_find_variable(OsModule module, char8_t const* name)
{
	FARPROC farproc = GetProcAddress((HMODULE)module, name);
	return (OsModuleVar*)farproc;
}

#endif