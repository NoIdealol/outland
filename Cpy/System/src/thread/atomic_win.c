#include "pch.h"
#include "thread.h"

#if OS_WINDOWS

uint32_t os_cache_line_size()
{
	uint32_t line_size = 64; //some default
	DWORD buffer_size = 0;
	DWORD i = 0;
	SYSTEM_LOGICAL_PROCESSOR_INFORMATION* buffer = null;

	if (FALSE != GetLogicalProcessorInformation(null, &buffer_size) || ERROR_INSUFFICIENT_BUFFER != GetLastError())
	{
		os_crash(__FUNCTION__ " : GetLogicalProcessorInformation");
		return line_size;
	}

	buffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, buffer_size);
	if (null == buffer)
	{
		os_crash(__FUNCTION__ " : HeapAlloc");
		return line_size;
	}

	if (GetLogicalProcessorInformation(buffer, &buffer_size))
	{
		for (i = 0; i != buffer_size / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION); ++i)
		{
			if (RelationCache == buffer[i].Relationship && 1 == buffer[i].Cache.Level)
			{
				line_size = m_max_u32(buffer[i].Cache.LineSize, line_size);
			}
		}
	}
	else
	{
		os_crash(__FUNCTION__ " : GetLogicalProcessorInformation");
		return line_size;
	}

	if (FALSE == HeapFree(GetProcessHeap(), 0, buffer))
	{
		os_crash(__FUNCTION__ " : HeapFree");
	}

	return line_size;
}

//--------------------------------------------------
int32_t	os_atomic_load_i32(int32_t volatile* var)
{
	return ReadAcquire(var);
	/*int32_t val = *var;
	MemoryBarrier();
	_ReadWriteBarrier();
	return val*/
}

//--------------------------------------------------
int32_t	os_atomic_exchange_i32(int32_t volatile* var, int32_t val)
{
	return InterlockedExchange(var, val);
}

#endif