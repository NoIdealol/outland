#include "pch.h"
#include "thread.h"

#if OS_WINDOWS

//--------------------------------------------------
bool_t	os_semaphore_create(OsSemaphore* semaphore, uint32_t max_signal_count, uint32_t signal_count)
{
	HANDLE sp = CreateSemaphoreA(
		NULL,
		(LONG)signal_count,
		(LONG)max_signal_count,
		NULL);

	if (NULL == sp)
		return false;

	*semaphore = (OsSemaphore)sp;

	return true;
}

//--------------------------------------------------
void		os_semaphore_destroy(OsSemaphore semaphore)
{
	HANDLE const sp = (HANDLE)semaphore;

	if (FALSE == CloseHandle(sp))
	{
		os_crash(__FUNCTION__ " : CloseHandle");
	}
}

//--------------------------------------------------
void		os_semaphore_signal(OsSemaphore semaphore, uint32_t signal_count)
{
	HANDLE const sp = (HANDLE)semaphore;

	if (FALSE == ReleaseSemaphore(sp, (LONG)signal_count, NULL))
	{
		os_crash(__FUNCTION__ " : ReleaseSemaphore");
	}
}

//--------------------------------------------------
void		os_semaphore_wait(OsSemaphore semaphore)
{
	HANDLE const sp = (HANDLE)semaphore;

	if (WAIT_OBJECT_0 != WaitForSingleObject(sp, INFINITE))
	{
		os_crash(__FUNCTION__" : WaitForSingleObject");
	}

}

#endif