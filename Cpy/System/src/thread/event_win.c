#include "pch.h"
#include "thread.h"

#if OS_WINDOWS

//--------------------------------------------------
bool_t	os_event_create(OsEvent* event, bool_t is_signaled)
{
	HANDLE ev = CreateEventW(NULL, TRUE, is_signaled ? TRUE : FALSE, NULL);
	if (NULL == ev)
		return false;

	*event = (OsEvent)ev;
	return true;
}

//--------------------------------------------------
void		os_event_destroy(OsEvent event)
{
	HANDLE const ev = (HANDLE)event;

	if (FALSE == CloseHandle(ev))
	{
		os_crash(__FUNCTION__ " : CloseHandle");
	}
}

//--------------------------------------------------
void		os_event_signal(OsEvent event)
{
	HANDLE const ev = (HANDLE)event;

	if (FALSE == SetEvent(ev))
	{
		os_crash(__FUNCTION__ " : SetEvent");
	}
}

//--------------------------------------------------
void		os_event_clear(OsEvent event)
{
	HANDLE const ev = (HANDLE)event;

	if (FALSE == ResetEvent(ev))
	{
		os_crash(__FUNCTION__ " : ResetEvent");
	}
}

//--------------------------------------------------
void		os_event_wait(OsEvent event)
{
	HANDLE const ev = (HANDLE)event;

	if (WAIT_OBJECT_0 != WaitForSingleObject(ev, INFINITE))
	{
		os_crash(__FUNCTION__" : WaitForSingleObject");
	}
}

#endif
