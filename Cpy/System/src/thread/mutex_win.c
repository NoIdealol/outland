#include "pch.h"
#include "thread.h"

#if OS_WINDOWS

//--------------------------------------------------
bool_t	os_mutex_create(OsMutex* mutex, uint32_t spin_count)
{
	CRITICAL_SECTION* cs = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(CRITICAL_SECTION));
	if (!cs)
		return false;

	if (FALSE == InitializeCriticalSectionAndSpinCount(cs, (DWORD)spin_count))
	{
		if (FALSE == HeapFree(GetProcessHeap(), 0, cs))
		{
			os_crash(__FUNCTION__ " : HeapFree");
		}

		return false;
	}

	*mutex = (OsMutex)cs;
	return true;
}

//--------------------------------------------------
void		os_mutex_destroy(OsMutex mutex)
{
	CRITICAL_SECTION* const cs = (CRITICAL_SECTION*)mutex;

	DeleteCriticalSection(cs);
	if (FALSE == HeapFree(GetProcessHeap(), 0, cs))
	{
		os_crash(__FUNCTION__ " : HeapFree");
	}
}

//--------------------------------------------------
void		os_mutex_lock(OsMutex mutex)
{
	CRITICAL_SECTION* const cs = (CRITICAL_SECTION*)mutex;

	EnterCriticalSection(cs);
}

//--------------------------------------------------
void		os_mutex_unlock(OsMutex mutex)
{
	CRITICAL_SECTION* const cs = (CRITICAL_SECTION*)mutex;

	LeaveCriticalSection(cs);
}

//--------------------------------------------------
bool_t		os_mutex_try_lock(OsMutex mutex)
{
	CRITICAL_SECTION* const cs = (CRITICAL_SECTION*)mutex;

	return TryEnterCriticalSection(cs);
}

#endif