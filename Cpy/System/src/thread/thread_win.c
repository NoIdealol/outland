#include "pch.h"
#include "thread.h"

#if OS_WINDOWS

struct ThreadParams
{
	OsThreadMain* main;
	void* ctx;
	volatile LONG started;
};

//--------------------------------------------------
static DWORD WINAPI os_thread_main(LPVOID lpThreadParameter)
{
	struct ThreadParams* params = lpThreadParameter;
	OsThreadMain* main = params->main;
	void* ctx = params->ctx;
	InterlockedExchange(&params->started, true);
	params = null;
	return main(ctx);
}

//--------------------------------------------------
bool_t	os_thread_create(OsThread* thread, OsThreadMain* main, void* ctx, char8_t const* name, size_t stack_size)
{
	struct ThreadParams params;
	params.main = main;
	params.ctx = ctx;
	params.started = false;

	DWORD id;
	HANDLE t = CreateThread(
		NULL, //default securit
		stack_size, //default stack size
		&os_thread_main, //starting function
		&params, //starting fnc argument
		0, //no flags
		&id); //thread_t ID

	if (NULL == t)
		return false;

	while (false == params.started)
		os_thread_yield();

	*thread = (OsThread)t;
	return true;
}

//--------------------------------------------------
bool_t		os_thread_join(OsThread thread, uint8_t* exit_code, uint32_t timeout_ms)
{
	HANDLE const th = (HANDLE)thread;

	switch (WaitForSingleObject(th, -1 == timeout_ms ? INFINITE : timeout_ms))
	{
	case WAIT_OBJECT_0:
		return true;
	case WAIT_TIMEOUT:
		return false;
	default:
		os_crash(__FUNCTION__" : WaitForSingleObject");
		return -1;
	}
	
	DWORD code = 0;
	if (FALSE == GetExitCodeThread(th, &code))
	{
		os_crash(__FUNCTION__" : GetExitCodeThread");
		return -1;
	}

	if (0 == CloseHandle((HANDLE)thread))
	{
		os_crash(__FUNCTION__" : CloseHandle");
		return -1;
	}

	if (exit_code)
		*exit_code = (uint8_t)code;

	return true;
}

//--------------------------------------------------
void		os_thread_detach(OsThread thread)
{
	HANDLE const th = (HANDLE)thread;

	if (0 == CloseHandle(th))
	{
		os_crash(__FUNCTION__" : CloseHandle");
		return;
	}
}

//--------------------------------------------------
void		os_thread_terminate(OsThread thread)
{
	HANDLE const th = (HANDLE)thread;

	if (FALSE == TerminateThread(th, -1))
	{
		os_crash(__FUNCTION__" : TerminateThread");
		return;
	}

	if (0 == CloseHandle(th))
	{
		os_crash(__FUNCTION__" : CloseHandle");
		return;
	}
}

//--------------------------------------------------
void		os_thread_sleep(uint32_t time_ms)
{
	Sleep((DWORD)time_ms);
}

//--------------------------------------------------
void		os_thread_yield()
{
	YieldProcessor();
}

#endif