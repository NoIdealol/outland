#include "pch.h"
#include "file.h"

#if OS_WINDOWS

//--------------------------------------------------
static OsFileAttributeFlags os_from_sys_attributes(DWORD dwAttr)
{
	OsFileAttributeFlags attr = 0;
	if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
		attr |= OS_FILE_ATTRIBUTE_FOLDER;
	if (dwAttr & FILE_ATTRIBUTE_READONLY)
		attr |= OS_FILE_ATTRIBUTE_READ_ONLY;
	if (dwAttr & FILE_ATTRIBUTE_HIDDEN)
		attr |= OS_FILE_ATTRIBUTE_HIDDEN;
	if (dwAttr & FILE_ATTRIBUTE_SYSTEM)
		attr |= OS_FILE_ATTRIBUTE_SYSTEM;

	return attr;
}

//--------------------------------------------------
static DWORD os_to_sys_attributes(OsFileAttributeFlags attr)
{
	DWORD dwAttr = 0;
	if (OS_FILE_ATTRIBUTE_FOLDER & attr)
		dwAttr |= FILE_ATTRIBUTE_DIRECTORY;
	if (OS_FILE_ATTRIBUTE_SYSTEM & attr)
		dwAttr |= FILE_ATTRIBUTE_SYSTEM;
	if (OS_FILE_ATTRIBUTE_READ_ONLY & attr)
		dwAttr |= FILE_ATTRIBUTE_READONLY;
	if (OS_FILE_ATTRIBUTE_HIDDEN & attr)
		dwAttr |= FILE_ATTRIBUTE_HIDDEN;

	return dwAttr;
}

//--------------------------------------------------
OsFileResult os_file_create(OsFile* file, char8_t const* path, OsFileCreateInfo const* info)
{
	if (!info)
		return OS_FILE_RESULT_E_INVALID_PARAMETER;

	if (!path)
		return OS_FILE_RESULT_E_INVALID_PARAMETER;

	wchar_t wstr[OS_MAX_FILE_PATH];
	size_t wlen = os_utf8_to_wchar(path, wstr, lengthof(wstr));
	if (!wlen)
		return OS_FILE_RESULT_E_INVALID_PATH;

	os_path_to_system(wstr);
	
	DWORD acces = 0;
	if (OS_FILE_ACCESS_READ & info->access)
		acces |= GENERIC_READ;
	if (OS_FILE_ACCESS_WRITE & info->access)
		acces |= GENERIC_WRITE;

	DWORD flagsAndAttributes = FILE_ATTRIBUTE_NORMAL;
	if (OS_FILE_ACCESS_AIO & info->access)
		flagsAndAttributes |= FILE_FLAG_OVERLAPPED;
	flagsAndAttributes |= os_to_sys_attributes(info->attributes);
	
	DWORD creation = 0;
	switch (info->create)
	{
	case OS_FILE_CREATE_MUST_EXIST:
		creation = OPEN_EXISTING;
		break;
	case OS_FILE_CREATE_NEW_ONLY:
		creation = CREATE_NEW;
		break;
	case OS_FILE_CREATE_OVERWRITE:
		creation = CREATE_ALWAYS;
		break;
	case OS_FILE_CREATE_PRESERVE:
		creation = OPEN_ALWAYS;
		break;
	default:
		return OS_FILE_RESULT_E_INVALID_PARAMETER;
	}

	HANDLE hFile = CreateFileW(wstr, acces, FILE_SHARE_READ, NULL, creation, flagsAndAttributes, NULL);
	DWORD err = GetLastError();
	if (INVALID_HANDLE_VALUE == hFile)
	{
		switch (creation)
		{
		case CREATE_NEW:
			if (ERROR_FILE_EXISTS == err)
				return OS_FILE_RESULT_E_ALREADY_EXISTS;
		case OPEN_EXISTING:
			if (ERROR_FILE_NOT_FOUND == err)
				return OS_FILE_RESULT_E_FILE_NOT_FOUND;		
		default:
			break;
		}

		switch (err)
		{
		case ERROR_PATH_NOT_FOUND:
			return OS_FILE_RESULT_E_PATH_NOT_FOUND;
		case ERROR_BAD_PATHNAME:
			return OS_FILE_RESULT_E_INVALID_PATH;
		case ERROR_ACCESS_DENIED:
			return OS_FILE_RESULT_E_ACCESS_DENIED;
		case ERROR_TOO_MANY_OPEN_FILES:
			return OS_FILE_RESULT_E_OUT_OF_HANDLES;
		default:
			return OS_FILE_RESULT_E_UNEXPECTED;
		}
	}
	

	if (OS_FILE_ACCESS_AIO & info->access)
	{
		if (NULL == CreateIoCompletionPort(hFile, (HANDLE)info->epoll, (ULONG_PTR)hFile, 0))
		{
			if (FALSE == CloseHandle(hFile))
				os_crash(__FUNCTION__ " : CloseHandle");

			return OS_FILE_RESULT_E_UNEXPECTED;
		}
	}

	*file = (OsFile)hFile;

	
	switch (creation)
	{
	case CREATE_ALWAYS:
		if (ERROR_ALREADY_EXISTS == err)
			return OS_FILE_RESULT_S_ALREADY_EXISTS;
		break;
	case OPEN_ALWAYS:
		if (ERROR_ALREADY_EXISTS == err)
			return OS_FILE_RESULT_S_ALREADY_EXISTS;
		break;
	default:
		break;
	}

	return OS_FILE_RESULT_S_OK;
}

//--------------------------------------------------
OsFileResult os_file_delete(char8_t const* path)
{
	if (!path)
		return OS_FILE_RESULT_E_INVALID_PARAMETER;

	wchar_t wstr[OS_MAX_FILE_PATH];
	size_t wlen = os_utf8_to_wchar(path, wstr, lengthof(wstr));
	if (!wlen)
		return OS_FILE_RESULT_E_INVALID_PATH;

	os_path_to_system(wstr);

	if (FALSE == DeleteFileW(wstr))
	{
		DWORD err = GetLastError();
		switch (err)
		{
		case ERROR_FILE_NOT_FOUND:
			return OS_FILE_RESULT_E_FILE_NOT_FOUND;
		case ERROR_PATH_NOT_FOUND:
			return OS_FILE_RESULT_E_PATH_NOT_FOUND;
		case ERROR_BAD_PATHNAME:
			return OS_FILE_RESULT_E_INVALID_PATH;
		case ERROR_ACCESS_DENIED:
			return OS_FILE_RESULT_E_ACCESS_DENIED;
		case ERROR_TOO_MANY_OPEN_FILES:
			return OS_FILE_RESULT_E_OUT_OF_HANDLES;
		default:
			return OS_FILE_RESULT_E_UNEXPECTED;
		}
	}

	return OS_FILE_RESULT_S_OK;
}

//--------------------------------------------------
void	os_file_close(OsFile file)
{
	HANDLE const hFile = (HANDLE)file;

	if (FALSE == CloseHandle(hFile))
		os_crash(__FUNCTION__ " : CloseHandle");
}

//--------------------------------------------------
bool_t	os_file_get_size(OsFile file, uint64_t* size)
{
	HANDLE const hFile = (HANDLE)file;

	LARGE_INTEGER li;
	if (FALSE == GetFileSizeEx(hFile, &li))
		return false;

	*size = li.QuadPart;

	return true;
}

//--------------------------------------------------
bool_t	os_file_set_size(OsFile file, uint64_t size)
{
	HANDLE const hFile = (HANDLE)file;

	LARGE_INTEGER li;
	li.QuadPart = size;
	if (FALSE == SetFilePointerEx(hFile, li, NULL, FILE_BEGIN))
		return false;

	if (FALSE == SetEndOfFile(hFile))
		return false;

	return true;
}

//--------------------------------------------------
bool_t	os_file_set_head(OsFile file, uint64_t offset)
{
	HANDLE const hFile = (HANDLE)file;

	LARGE_INTEGER li;
	li.QuadPart = offset;
	if (FALSE == SetFilePointerEx(hFile, li, NULL, FILE_BEGIN))
		return false;

	return true;
}

//--------------------------------------------------
bool_t	os_file_get_head(OsFile file, uint64_t* offset)
{
	HANDLE const hFile = (HANDLE)file;

	LARGE_INTEGER zero;
	zero.QuadPart = 0;
	LARGE_INTEGER li;
	if (FALSE == SetFilePointerEx(hFile, zero, &li, FILE_CURRENT))
		return false;

	*offset = li.QuadPart;
	return true;
}

//--------------------------------------------------
bool_t	os_file_read(OsFile file, void* buffer, size_t size, size_t* read)
{
	HANDLE const hFile = (HANDLE)file;
	
	DWORD dwRead;
	if (FALSE == ReadFile(hFile, buffer, (DWORD)(size), &dwRead, NULL))
		return false;

	if (read)
		*read = (size_t)dwRead;

	return true;
}

//--------------------------------------------------
bool_t	os_file_write(OsFile file, void const* buffer, size_t size, size_t* written)
{
	HANDLE const hFile = (HANDLE)file;
	
	DWORD dwWritten;
	if (FALSE == WriteFile(hFile, buffer, (DWORD)(size), &dwWritten, NULL))
		return false;

	if (written)
		*written = (size_t)dwWritten;

	return true;
}

//--------------------------------------------------
OsFileResult	os_file_get_attributes(char8_t const* path, OsFileAttributeFlags* attributes)
{
	if (!path)
		return OS_FILE_RESULT_E_INVALID_PARAMETER;

	if (!attributes)
		return OS_FILE_RESULT_E_INVALID_PARAMETER;

	wchar_t wstr[OS_MAX_FILE_PATH];
	size_t wlen = os_utf8_to_wchar(path, wstr, lengthof(wstr));
	if (!wlen)
		return OS_FILE_RESULT_E_INVALID_PATH;

	os_path_to_system(wstr);

	DWORD dwAttr = GetFileAttributesW(wstr);
	if (INVALID_FILE_ATTRIBUTES != dwAttr)
	{
		*attributes = os_from_sys_attributes(dwAttr);
		return OS_FILE_RESULT_S_OK;
	}
	else
	{
		attributes = 0;
		DWORD err = GetLastError();
		switch (err)
		{
		case ERROR_FILE_NOT_FOUND:
			return OS_FILE_RESULT_E_FILE_NOT_FOUND;
		case ERROR_PATH_NOT_FOUND:
			return OS_FILE_RESULT_E_PATH_NOT_FOUND;
		case ERROR_BAD_PATHNAME:
			return OS_FILE_RESULT_E_INVALID_PATH;
		case ERROR_ACCESS_DENIED:
			return OS_FILE_RESULT_E_ACCESS_DENIED;
		case ERROR_TOO_MANY_OPEN_FILES:
			return OS_FILE_RESULT_E_OUT_OF_HANDLES;
		default:
			return OS_FILE_RESULT_E_UNEXPECTED;
		}
	}
}

//--------------------------------------------------
OsFileResult	os_file_set_attributes(char8_t const* path, OsFileAttributeFlags attributes)
{
	if (!path)
		return OS_FILE_RESULT_E_INVALID_PARAMETER;

	if (!attributes)
		return OS_FILE_RESULT_E_INVALID_PARAMETER;

	wchar_t wstr[OS_MAX_FILE_PATH];
	size_t wlen = os_utf8_to_wchar(path, wstr, lengthof(wstr));
	if (!wlen)
		return OS_FILE_RESULT_E_INVALID_PATH;

	os_path_to_system(wstr);

	DWORD dwAttr = os_to_sys_attributes(attributes);
	if (FALSE == SetFileAttributesW(wstr, dwAttr))
	{
		DWORD err = GetLastError();
		switch (err)
		{
		case ERROR_FILE_NOT_FOUND:
			return OS_FILE_RESULT_E_FILE_NOT_FOUND;
		case ERROR_PATH_NOT_FOUND:
			return OS_FILE_RESULT_E_PATH_NOT_FOUND;
		case ERROR_BAD_PATHNAME:
			return OS_FILE_RESULT_E_INVALID_PATH;
		case ERROR_ACCESS_DENIED:
			return OS_FILE_RESULT_E_ACCESS_DENIED;
		case ERROR_TOO_MANY_OPEN_FILES:
			return OS_FILE_RESULT_E_OUT_OF_HANDLES;
		default:
			return OS_FILE_RESULT_E_UNEXPECTED;
		}
	}

	return OS_FILE_RESULT_S_OK;
}

//--------------------------------------------------
bool_t	os_file_aio_read(OsFile file, void* buffer, size_t size, uint64_t offset, void* aio)
{
	HANDLE const hFile = (HANDLE)file;
	OVERLAPPED* const ov = aio;

	memset(ov, 0, sizeof(OVERLAPPED));
	ov->hEvent = NULL;
	ov->Internal = 0;
	ov->InternalHigh = 0;
	ov->Offset = (DWORD)(offset & 0xFFffFFff);
	ov->OffsetHigh = (DWORD)(offset >> 32);

	if (FALSE == ReadFile(hFile, buffer, (DWORD)(size), NULL, ov))
	{
		DWORD err = GetLastError();
		if (ERROR_IO_PENDING == err)
			return true;

		return false;
	}

	return true;
}

//--------------------------------------------------
bool_t	os_file_aio_write(OsFile file, void const* buffer, size_t size, uint64_t offset, void* aio)
{
	HANDLE const hFile = (HANDLE)file;
	OVERLAPPED* const ov = aio;

	memset(ov, 0, sizeof(OVERLAPPED));
	ov->hEvent = NULL;
	ov->Internal = 0;
	ov->InternalHigh = 0;
	ov->Offset = (DWORD)(offset & 0xFFffFFff);
	ov->OffsetHigh = (DWORD)(offset >> 32);

	if (FALSE == WriteFile(hFile, buffer, (DWORD)(size), NULL, ov))
	{
		DWORD err = GetLastError();
		if (ERROR_IO_PENDING == err)
			return true;

		return false;
	}

	return true;
}

//--------------------------------------------------
size_t		os_file_aio_size(size_t user_size)
{
	return 
		m_align_size(sizeof(OVERLAPPED), ALLOCATOR_MIN_ALIGNMENT) + 
		m_align_size(user_size, ALLOCATOR_MIN_ALIGNMENT);
}

//--------------------------------------------------
void*		os_file_aio_user(void* aio)
{
	byte_t* const ov = aio;
	return ov + m_align_size(sizeof(OVERLAPPED), ALLOCATOR_MIN_ALIGNMENT);
}

//--------------------------------------------------
bool_t		os_file_epoll_create(OsFileEpoll* epoll)
{
	HANDLE hPort = CreateIoCompletionPort(
		INVALID_HANDLE_VALUE, //create new port
		NULL, //create new port
		0, //ignored, so use NULL
		0 //as many concurrently running threads as there are processors in the system
	);

	if (NULL == hPort)
		return false;
	
	*epoll = (OsFileEpoll)hPort;
	return true;
}

//--------------------------------------------------
void		os_file_epoll_destroy(OsFileEpoll epoll)
{
	HANDLE const hPort = (HANDLE)epoll;

	if (FALSE == CloseHandle(hPort))
	{
		os_crash(__FUNCTION__ " : CloseHandle");
	}
}

//--------------------------------------------------
size_t		os_file_epoll_wait(OsFileEpoll epoll, OsFileEvent* ev_buffer, size_t ev_capacity, uint32_t wait_ms)
{
	HANDLE const hPort = (HANDLE)epoll;

	OVERLAPPED_ENTRY ov_entry_array[64] = { 0 };
	ULONG ov_entry_size = 0;

	if (FALSE == GetQueuedCompletionStatusEx(
		hPort,
		ov_entry_array,
		(DWORD)m_min_size(lengthof(ov_entry_array), ev_capacity),
		&ov_entry_size,
		(DWORD)wait_ms,
		FALSE))
	{
		return 0;
	}

	for (ULONG i = 0; i < ov_entry_size; ++i)
	{
		HANDLE const hFile = (HANDLE)ov_entry_array[i].lpCompletionKey;
		ev_buffer[i].file = (OsFile)hFile;
		ev_buffer[i].aio = ov_entry_array[i].lpOverlapped;

		DWORD dwSize = 0;
		if (FALSE == GetOverlappedResult(hFile, ov_entry_array[i].lpOverlapped, &dwSize, FALSE))
		{
			ev_buffer[i].result = false;
		}
		else
		{
			ev_buffer[i].result = true;
		}
	}

	return (size_t)ov_entry_size;
}
#endif