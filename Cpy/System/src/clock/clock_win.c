#include "pch.h"
#include "clock.h"

static LARGE_INTEGER g_qpf;

bool_t		os_clock_init()
{
	return QueryPerformanceFrequency(&g_qpf) ? true : false;
}

os_clock_t os_clock()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return li.QuadPart;
}

os_clock_t os_clocks_per_sec()
{
	return g_qpf.QuadPart;
}

os_clock_t	os_clock_to(os_clock_t clock, os_clock_t os_clocks_per_sec)
{
	os_clock_t whole = clock / g_qpf.QuadPart;
	os_clock_t frac = clock % g_qpf.QuadPart;
	return whole * os_clocks_per_sec + (frac * os_clocks_per_sec) / g_qpf.QuadPart;
}