#pragma once
#include "constants.h"

namespace outland { namespace gpu 
{

	//////////////////////////////////////////////////
	// ADAPTER
	//////////////////////////////////////////////////

	enum adapter_id : uintptr_t {};

	struct adapter_caps_t
	{
		//bool				sampler_anisotropy;
	};

	struct adapter_info_t
	{

	};

	struct adapter_limits_t
	{
		uint32_t			max_descriptor_uniform_buffer_size;
		uint32_t			max_descriptor_storage_buffer_size;

		uint32_t			max_index_value;
		uint32_t			max_vertex_slot_size;
		uint32_t			max_vertex_stride;
		uint32_t			max_vertex_attribute_slot_size;
		uint32_t			max_vertex_attribute_offset;


		uint32_t			max_uniform_size;
		uint32_t			max_descriptor_slot_size;
		uint32_t			max_per_stage_descriptor_sampler_size;
		uint32_t			max_per_stage_descriptor_uniform_buffer_size;
		uint32_t			max_per_stage_descriptor_storage_buffer_size;
		uint32_t			max_per_stage_descriptor_sampled_texture_size;
		uint32_t			max_per_stage_descriptor_storage_texture_size;
		uint32_t			max_per_stage_descriptor_read_attachment_size;
		uint32_t			max_per_stage_descriptor_size;
		uint32_t			max_descriptor_set_sampler_size;
		uint32_t			max_descriptor_set_uniform_buffer_size;
		uint32_t			max_descriptor_set_storage_buffer_size;
		uint32_t			max_descriptor_set_sampled_texture_size;
		uint32_t			max_descriptor_set_storage_texture_size;
		uint32_t			max_descriptor_set_read_attachment_size;

		float				max_sampler_mip_bias;
		uint32_t			max_sampler_anisotropy;

		uint32_t			max_attachment_set_width;
		uint32_t			max_attachment_set_height;
		uint32_t			max_subpass_render_target_size;
	};

} }
