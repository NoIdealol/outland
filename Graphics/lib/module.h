#pragma once
//#include "adapter.h"
#include "device.h"

namespace outland { namespace gpu 
{
	using module_id = class module_t*;

	enum module_create_flags_t : uint32_t
	{
		module_create_debug			= 1 << 0,
	};

	using module_create_mask_t = uint32_t;

	struct module_create_info_t
	{
		module_create_mask_t	create_mask;
		allocator_t*			module_allocator;
		allocator_t*			object_allocator;
	};

	namespace module
	{
		error_t adapter_enumerate(module_id module, adapter_id* adapter_array, size_t adapter_capacity, size_t& adapter_size, scratch_allocator_t* scratch);
		void	adapter_get_info(module_id module, adapter_id adapter, adapter_info_t& info);
		void	adapter_get_caps(module_id module, adapter_id adapter, adapter_caps_t& caps);
		void	adapter_get_limits(module_id module, adapter_id adapter, adapter_limits_t& limits);

		error_t device_create(module_id module, device_id& device, cmd_queue_id& cmd_queue, device_create_t const& create, scratch_allocator_t* scratch);
		void	device_destroy(module_id module, device_id device, cmd_queue_id cmd_queue);

		error_t	surface_create(module_id module, surface_id& surface, surface_create_info_t const& info, scratch_allocator_t* scratch);
		void	surface_destroy(module_id module, surface_id surface);
	}
} }

extern "C"
{
	outland::error_t	outland_gpu_module_create(outland::gpu::module_t** module, outland::gpu::module_create_info_t const* info, outland::scratch_allocator_t* scratch);
	void				outland_gpu_module_destroy(outland::gpu::module_t* module);
}
