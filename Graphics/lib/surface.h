#pragma once
#include "format.h"
#include "constants.h"
#include "adapter.h"

namespace outland { namespace gpu 
{

	//////////////////////////////////////////////////
	// SURFACE
	//////////////////////////////////////////////////

	using surface_id = class surface_t*;

	struct surface_caps_t
	{
		present_mode_mask_t		present_mode_mask;

		uint32_t				back_buffer_min_count;
		uint32_t				back_buffer_max_count;

		uint32_t				back_buffer_min_width;
		uint32_t				back_buffer_min_height;

		uint32_t				back_buffer_max_width;
		uint32_t				back_buffer_max_height;
	};

	struct surface_create_info_t
	{
		void* window_ptr;
		void* window_ctx;
	};

	namespace surface
	{
		error_t get_caps(surface_id surface, adapter_id adapter, surface_caps_t& caps);
		error_t format_enumerate(surface_id surface, adapter_id adapter, format_t* format_array, size_t format_capacity, size_t& format_size, scratch_allocator_t* scratch);
	}

} }
