#pragma once
#include "constants.h"

namespace outland { namespace gpu 
{
	using shader_id = struct shader_t*;

	struct shader_create_t
	{
		void const*				shader_data; //must be 4 aligned
		size_t					shader_size; //must be mul of 4 on vk
		//shader_stage_flags_t	stage;
	};

} }
