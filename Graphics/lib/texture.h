#pragma once
#include "format.h"
#include "constants.h"

namespace outland { namespace gpu 
{
	using texture_id = struct texture_t*;
	//enum sampled_texture_id : uintptr_t {};
	//enum storage_texture_id : uintptr_t {};
	//enum depth_stencil_id : uintptr_t {};
	//enum render_target_id : uintptr_t {};

	using shader_view_id = struct shader_view_t*;
	using depth_stencil_view_id = struct depth_stencil_view_t*;
	using render_target_view_id = struct render_target_view_t*;

	enum texture_type_t
	{
		texture_type_sampled,
		texture_type_storage,
		texture_type_render_target,
		texture_type_depth_stencil,
	};

	enum texture_dimension_t
	{
		texture_dimension_1D,
		texture_dimension_2D,
		texture_dimension_3D,
	};

	enum texture_usage_flags_t : uint32_t
	{
		texture_usage_cube				= 1 << 0,
		texture_usage_shader_resource	= 1 << 1,
		//texture_create_alias		= 1 << 1,
		texture_usage_3D_to_2D			= 1 << 2,
	};
	using texture_usage_mask_t = uint32_t;

	struct texture_size_t
	{
		uint32_t				width;
		uint32_t				height;
		uint32_t				depth;
	};

	struct texture_create_t
	{
		texture_usage_mask_t	usage_mask;
		texture_type_t			type;
		texture_dimension_t		dimension;
		format_t				format;
		texture_size_t			size;
		uint32_t				mip_size;
		uint32_t				array_size;
		shader_stage_mask_t		stage_mask;
	};





	enum texture_shader_view_type_t
	{
		texture_shader_view_type_1D,
		texture_shader_view_type_2D,
		texture_shader_view_type_3D,
		texture_shader_view_type_cube_map,
		texture_shader_view_type_1D_array,
		texture_shader_view_type_2D_array,
		texture_shader_view_type_cube_map_array,
	};

	enum texture_aspect_flags_t : uint32_t
	{
		texture_aspect_color	= 1 << 0,
		texture_aspect_depth	= 1 << 1,
		texture_aspect_stencil	= 1 << 2,
	};
	using texture_aspect_mask_t = uint32_t;

	/*struct texture_subresource_range_t
	{
		texture_aspect_mask_t		aspect_mask;
		uint32_t					mip_level;
		uint32_t					mip_size;
		uint32_t					array_level;
		uint32_t					array_size;
	};*/

	/*struct texture_subresource_t
	{
		texture_aspect_mask_t		aspect_mask;
		uint32_t					mip_level;
		uint32_t					array_level;
		uint32_t					array_size;
	};*/

	struct  texture_shader_view_create_t
	{
		texture_id						texture;
		format_t						format;
		texture_shader_view_type_t		type;
		texture_aspect_mask_t			aspect_mask;
		uint32_t						mip_level;
		uint32_t						mip_size;
		uint32_t						array_level;
		uint32_t						array_size;
	};

	struct  render_target_view_create_t
	{
		texture_id					texture;
		format_t					format;
		texture_aspect_mask_t		aspect_mask;
		uint32_t					mip_level;
		uint32_t					array_level;
	};

	struct  depth_stencil_view_create_t
	{
		texture_id					texture;
		texture_aspect_mask_t		aspect_mask;
		uint32_t					mip_level;
		uint32_t					array_level;
	};
} }
