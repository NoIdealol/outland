#pragma once

namespace outland { namespace gpu 
{
	enum : error_t
	{
		err_out_of_vram = 1 << 16,
	};

	//////////////////////////////////////////////////
	// PRESENT MODE
	//////////////////////////////////////////////////

	enum present_mode_t : uint32_t
	{
		present_mode_immediate		= 1 << 0,
		present_mode_mailbox		= 1 << 1,
		present_mode_fifo			= 1 << 2,
	};

	using present_mode_mask_t = uint32_t;

	//////////////////////////////////////////////////
	// COMPARE OPERATION
	//////////////////////////////////////////////////

	enum compare_operation_t
	{
		compare_operation_never,
		compare_operation_less,
		compare_operation_equal,
		compare_operation_less_or_equal,
		compare_operation_greater,
		compare_operation_not_equal,
		compare_operation_greater_or_equal,
		compare_operation_always,
	};

	//////////////////////////////////////////////////
	// SHADER STAGE
	//////////////////////////////////////////////////

	enum shader_stage_flags_t : uint32_t
	{
		shader_stage_vertex = 1 << 0,
		shader_stage_geometry = 1 << 1,
		shader_stage_fragment = 1 << 2,
		shader_stage_compute = 1 << 3,
	};

	using shader_stage_mask_t = uint32_t;

} }