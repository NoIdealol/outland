#pragma once
#include "constants.h"

namespace outland { namespace gpu 
{
	using buffer_id = struct buffer_t*;
	//enum buffer_id : uintptr_t {};
	/*enum uniform_buffer_id : uintptr_t {};
	enum vertex_buffer_id : uintptr_t {};
	enum index_buffer_id : uintptr_t {};*/
	
	enum buffer_type_t
	{
		buffer_type_uniform,
		buffer_type_storage,
		buffer_type_vertex,
		buffer_type_index,
	};

	struct buffer_create_t
	{
		buffer_type_t			type;
		uint32_t				size;
		shader_stage_mask_t		stage_mask;
	};

	enum index_format_t : uint8_t
	{
		index_format_uint16,
		index_format_uint32,
	};

} }
