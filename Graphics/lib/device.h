#pragma once
#include "swap_chain.h"
#include "texture.h"
#include "render_pass.h"
#include "adapter.h"
#include "descriptor_set.h"
#include "pipeline.h"
#include "cmd_ctx.h"

namespace outland { namespace gpu 
{

} }

namespace outland { namespace gpu 
{
	using device_id = class device_t*;

	struct device_create_t
	{
		adapter_id		adapter;
		allocator_t*	device_allocator;
		allocator_t*	object_allocator;
	};

	enum device_event_type_t
	{
		device_event_type_texture_create,
		device_event_type_texture_copy,
		device_event_type_buffer_create,
		device_event_type_buffer_copy,
	};

	struct device_event_texture_t
	{
		error_t			error;
		texture_id		texture;
		void*			user_data;
	};

	struct device_event_buffer_t
	{
		error_t			error;
		buffer_id		buffer;
		void*			user_data;
	};

	struct device_event_t
	{
		device_event_type_t			type;
		union
		{
			device_event_texture_t	texture;
			device_event_buffer_t	buffer;
		};
	};

	struct host_to_buffer_copy_t
	{
		void*		user_data;
		buffer_id	dst_buffer;
		void const*	src_data;
	};

	struct texture_mip_data_t
	{
		size_t		byte_row_pitch;
		size_t		byte_slice_pitch;
		void const*	src_data;
	};

	struct host_to_texture_copy_t
	{
		void*						user_data;
		texture_id					dst_texture;
		texture_mip_data_t const*	mip_data_array;
		uint32_t					mip_size;
		uint32_t					mip_level;
		uint32_t					array_level;
		uint32_t					array_size;
	};

	namespace device
	{
		void	update(device_id device, device_event_t* event_array, size_t event_capacity, size_t& event_size); //resource thread
		
		error_t swap_chain_create(device_id device, swap_chain_id& swap_chain, swap_chain_create_t const& create, scratch_allocator_t* scratch);
		void	swap_chain_destroy(device_id device, swap_chain_id swap_chain);

		error_t cmd_allocator_create(device_id device, cmd_allocator_id& allocator, cmd_allocator_create_t const& create, scratch_allocator_t* scratch);
		void	cmd_allocator_destroy(device_id device, cmd_allocator_id allocator);
		
		error_t render_pass_create(device_id device, render_pass_id& pass, render_pass_create_t const& create, scratch_allocator_t* scratch);
		void	render_pass_destroy(device_id device, render_pass_id pass);
		error_t render_set_create(device_id device, render_set_id& set, render_set_create_t const& create, scratch_allocator_t* scratch);
		void	render_set_destroy(device_id device, render_set_id set);

		error_t	descriptor_layout_create(device_id device, descriptor_layout_id& layout, descriptor_layout_create_t const& create, scratch_allocator_t* scratch);
		void	descriptor_layout_destroy(device_id device, descriptor_layout_id layout);
		error_t	descriptor_set_create(device_id device, descriptor_set_id* set_array, descriptor_set_create_t const& create, scratch_allocator_t* scratch);
		void	descriptor_set_destroy(device_id device, descriptor_set_id const* set_array, size_t set_size);

		error_t	shader_create(device_id device, shader_id& shader, shader_create_t const& create, scratch_allocator_t* scratch);
		void	shader_destroy(device_id device, shader_id shader);

		error_t	input_layout_create(device_id device, input_layout_id& layout, input_layout_create_t const& create, scratch_allocator_t* scratch);
		void	input_layout_destroy(device_id device, input_layout_id layout);
		error_t	pipeline_layout_create(device_id device, pipeline_layout_id& layout, pipeline_layout_create_t const& create, scratch_allocator_t* scratch);
		void	pipeline_layout_destroy(device_id device, pipeline_layout_id layout);
		error_t	pipeline_create(device_id device, pipeline_id* pipeline_array, pipeline_create_t const& create, scratch_allocator_t* scratch);
		void	pipeline_destroy(device_id device, pipeline_id const* pipeline_array, size_t pipeline_size);

		error_t	sampler_create(device_id device, sampler_id& sampler, sampler_create_t const& create, scratch_allocator_t* scratch);
		void	sampler_destroy(device_id device, sampler_id sampler);

		error_t	texture_create(device_id device, void* user_data, texture_create_t const& create, scratch_allocator_t* scratch);
		void	texture_destroy(device_id device, texture_id texture);

		error_t shader_view_create(device_id device, shader_view_id& view, texture_shader_view_create_t const& create, scratch_allocator_t* scratch);
		void	shader_view_destroy(device_id device, shader_view_id view);
		error_t render_target_view_create(device_id device, render_target_view_id& view, render_target_view_create_t const& create, scratch_allocator_t* scratch);
		void	render_target_view_destroy(device_id device, render_target_view_id view);
		error_t depth_stencil_view_create(device_id device, depth_stencil_view_id& view, depth_stencil_view_create_t const& create, scratch_allocator_t* scratch);
		void	depth_stencil_view_destroy(device_id device, depth_stencil_view_id view);

		error_t	buffer_create(device_id device, void* user_data, buffer_create_t const& create, scratch_allocator_t* scratch);
		void	buffer_destroy(device_id device, buffer_id buffer);

		error_t	buffer_copy(device_id device, host_to_buffer_copy_t const& copy);
		error_t	texture_copy(device_id device, host_to_texture_copy_t const& copy);
	}


} }

