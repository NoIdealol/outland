#pragma once
#include "render_pass.h"
#include "pipeline.h"
#include "swap_chain.h"

namespace outland { namespace gpu 
{
	//enum cmd_ctx_id : uintptr_t {};
	using cmd_queue_id = class cmd_queue_t*;
	using cmd_ctx_id = class cmd_ctx_t*;
	using cmd_allocator_id = class cmd_allocator_t*;
	//enum cmd_allocator_id : uintptr_t {};

	//////////////////////////////////////////////////
	// COMMAND CREATION
	//////////////////////////////////////////////////

	struct cmd_allocator_create_t
	{
		allocator_t* allocator;
	};

	/*struct cmd_scope_t
	{
		render_pass_id	pass;
		uint32_t		subpass;
	};*/

	//////////////////////////////////////////////////
	// COMMAND CLEAR
	//////////////////////////////////////////////////

	struct clear_color_t
	{
		uint32_t		index;
		union
		{
			float		fmt_float32[4];
			uint32_t	fmt_uint32[4];
			int32_t		fmt_int32[4];
		} value;
	};

	struct clear_depth_t
	{
		uint32_t	index;
		float		depth;
		uint32_t	stencil;
	};

	//////////////////////////////////////////////////
	// COMMAND RENDER PASS
	//////////////////////////////////////////////////

	enum cmd_type_t
	{
		cmd_type_deferred,
		cmd_type_primary,
	};

	struct render_state_t
	{
		render_set_id			render_set;
		uint32_t				render_offset_x;
		uint32_t				render_offset_y;
		uint32_t				render_width;
		uint32_t				render_height;
		clear_color_t const*	clear_color_array;
		uint32_t				clear_color_size;
		clear_depth_t const*	clear_depth_array;
		uint32_t				clear_depth_size;
	};

	//////////////////////////////////////////////////
	// COMMAND LIST API
	//////////////////////////////////////////////////

	namespace cmd
	{
		error_t update(cmd_queue_id cmd_queue);
		error_t submit(cmd_queue_id cmd_queue, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size, swap_chain_id const* present_array, size_t present_size);
		error_t ctx_alloc(cmd_queue_id cmd_queue, cmd_allocator_id allocator, cmd_ctx_id* cmd_ctx_array, size_t cmd_ctx_size, cmd_type_t cmd_type);
		void	ctx_free(cmd_queue_id cmd_queue, cmd_allocator_id allocator, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size);

		//shared commands
		void bind_pipeline_layout(cmd_ctx_id cmd_ctx, pipeline_layout_id pipeline_layout);
		void bind_pipeline(cmd_ctx_id cmd_ctx, pipeline_id pipeline);
		void draw(cmd_ctx_id cmd_ctx, uint32_t vertex_size, uint32_t vertex_first, uint32_t instance_size, uint32_t instance_first);
		void draw_indexed(cmd_ctx_id cmd_ctx, uint32_t index_size, uint32_t index_first, uint32_t instance_size, uint32_t instance_first, uint32_t vertex_offset);
		void bind_vertex(cmd_ctx_id cmd_ctx, buffer_id const* buffer_array, uint32_t buffer_size, uint32_t slot_first);
		void bind_index(cmd_ctx_id cmd_ctx, buffer_id buffer, index_format_t format);
		void bind_descriptor(cmd_ctx_id cmd_ctx, descriptor_set_id const* set_array, uint32_t set_size, uint32_t slot_first);
		void uniform(cmd_ctx_id cmd_ctx, void const* uniform_array, uint32_t uniform_size, uint32_t uniform_offset, uint32_t slot);
		void transform(cmd_ctx_id cmd_ctx, void*& transform_buffer, uint32_t transform_size, uint32_t slot);
		error_t	record_finish(cmd_ctx_id cmd_ctx);

		error_t	record_deferred(cmd_ctx_id cmd_ctx, render_pass_id pass, uint32_t subpass);
		error_t	record_primary(cmd_ctx_id cmd_ctx);

		
		void back_buffer_acquire(cmd_ctx_id cmd_ctx, swap_chain_id swap_chain, size_t& index);
		void back_buffer_release(cmd_ctx_id cmd_ctx, swap_chain_id swap_chain);

		void execute(cmd_ctx_id cmd_ctx, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size);
		void render_state(cmd_ctx_id cmd_ctx, render_pass_id pass, render_state_t const& state, cmd_type_t cmd_type);
		void render_subpass(cmd_ctx_id cmd_ctx, render_pass_id pass, cmd_type_t cmd_type);
		void render_finish(cmd_ctx_id cmd_ctx, render_pass_id pass);

		
	}

} }
