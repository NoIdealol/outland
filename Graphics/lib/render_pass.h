#pragma once
//#include "format.h"
#include "texture.h"
//#include "shader.h"

namespace outland { namespace gpu 
{
	using render_pass_id = struct render_pass_t*;

	enum attachment_load_t
	{
		attachment_load_discard,
		attachment_load_read,
		attachment_load_clear,
	};

	enum attachment_store_t
	{
		attachment_store_discard,
		attachment_store_write,
	};

	/*struct attachment_info_t
	{
		format_t				format;
		attachment_load_t		load;
		attachment_store_t		store;
		attachment_load_t		load_stencil;
		attachment_store_t		store_stencil;
		attachment_access_t		access_initial;
		attachment_access_t		access_final;
		//texture_layout_t		layout_initial;
		//texture_layout_t		layout_final;
		//texture_access_mask_t	access_mask_initial;
		//texture_access_mask_t	access_mask_final;
	};*/



	/*enum class attachment_access_t
	{
		shader_stage_mask_t	shader_stage_mask;
	};*/

	/*struct attachment_access_t
	{
		attachment_usage_t		usage;
		shader_stage_mask_t		shader_stage_mask;
		//uint32_t				index;
		//texture_layout_t		layout;
		//texture_access_mask_t	access_mask;
	};*/

	enum render_target_access_t : uint32_t
	{
		//inside render pass
		render_target_access_store,			//no shader stage
		render_target_access_blend,			//no shader stage
		render_target_access_input,			//only pixel shader, exclusive
		//outside render pass
		render_target_access_sampled,
		render_target_access_copy_src,
	};

	enum depth_stencil_access_t : uint32_t
	{
		//inside render pass
		depth_stencil_access_store,				//no shader stage
		depth_stencil_access_compare,			//no shader stage
		depth_stencil_access_store_compare,
		depth_stencil_access_input,				//only pixel shader, exclusive
		depth_stencil_access_input_compare,		//only pixel shader, exclusive

		//outside render pass
		depth_stencil_access_sampled,
		depth_stencil_access_copy_src,

	};

	struct render_target_t
	{
		render_target_access_t	access;
		uint32_t				index;
		uint32_t				slot;
	};

	struct depth_stencil_t
	{
		depth_stencil_access_t	access;
		uint32_t				index;
		uint32_t				slot;
	};

	struct render_target_desc_t
	{
		format_t				format;
		attachment_load_t		color_load;
		attachment_store_t		color_store;
		render_target_access_t	access_initial;
		render_target_access_t	access_final;
		shader_stage_mask_t		stage_initial;
		shader_stage_mask_t		stage_final;
	};

	struct depth_stencil_desc_t
	{
		format_t				format;
		attachment_load_t		depth_load;
		attachment_store_t		depth_store;
		attachment_load_t		stencil_load;
		attachment_store_t		stencil_store;
		depth_stencil_access_t	access_initial;
		depth_stencil_access_t	access_final;
		shader_stage_mask_t		stage_initial;
		shader_stage_mask_t		stage_final;
	};


	struct subpass_desc_t
	{
		render_target_t const*	render_target_array;
		uint32_t				render_target_size;
		depth_stencil_t const*	depth_stencil_array;
		uint32_t				depth_stencil_size;
		uint32_t const*			render_target_preserve_array;
		uint32_t				render_target_preserve_size;
		uint32_t const*			depth_stencil_preserve_array;
		uint32_t				depth_stencil_preserve_size;
		uint32_t const*			subpass_dependency_array;
		uint32_t				subpass_dependency_size;
	};

	/*enum : uint32_t
	{
		subpass_index_external = c::max_uint32,
	};

	struct subpass_dependency_t
	{
		uint32_t const*	attachment_array;
		size_t			attachment_size;
		uint32_t		src_subpass_index;
		uint32_t		dst_subpass_index;
	};*/

	struct render_pass_create_t
	{
		render_target_desc_t const*		render_target_array;
		uint32_t						render_target_size;
		depth_stencil_desc_t const*		depth_stencil_array;
		uint32_t						depth_stencil_size;
		subpass_desc_t const*			subpass_array;
		uint32_t						subpass_size;
		
	};

	using render_set_id = struct render_set_t*;

	struct render_set_create_t
	{
		render_pass_id					render_pass;
		render_target_view_id const*	render_target_array;
		uint32_t						render_target_size;
		depth_stencil_view_id const*	depth_stencil_array;
		uint32_t						depth_stencil_size;
		uint32_t						width;
		uint32_t						height;
		//uint32_t						layers;
	};

} }
