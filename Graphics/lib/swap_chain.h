#pragma once
//#include "cmd_list.h"
//#include "event.h"
//#include "texture.h"
#include "constants.h"
#include "surface.h"
#include "render_pass.h"

namespace outland { namespace gpu 
{

	/*struct back_buffer_info_t
	{
		device_event_id	acquire_event;
		device_event_id	release_event;
		uint32_t		back_buffer_index;
	};*/

	struct back_buffer_state_t
	{
		render_target_access_t	access;
		//shader_stage_mask_t		stage;
	};

	struct swap_chain_create_t
	{
		surface_id			surface;
		present_mode_t		present_mode;

		uint32_t			back_buffer_min_count;
		uint32_t			back_buffer_width;
		uint32_t			back_buffer_height;
		format_t			back_buffer_format;
		back_buffer_state_t	back_buffer_acquire_state;
		back_buffer_state_t	back_buffer_release_state;
	};

	using swap_chain_id = class swap_chain_t*;

	namespace swap_chain
	{
		error_t	back_buffer_enumerate(swap_chain_id swap_chain, render_target_view_id* view_array, size_t view_capacity, size_t& view_size);
	}


} }
