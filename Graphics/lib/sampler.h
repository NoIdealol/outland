#pragma once
#include "constants.h"

namespace outland { namespace gpu 
{
	//////////////////////////////////////////////////
	// SAMPLER
	//////////////////////////////////////////////////

	using sampler_id = struct sampler_t*;

	enum filter_t
	{
		filter_nearest,
		filter_linear,
	};

	enum address_mode_t
	{
		address_mode_wrap,
		address_mode_mirror,
		address_mode_clamp,
		address_mode_border,
	};
	
	enum border_color_t
	{
		border_color_R0G0B0A0_FLOAT,
		border_color_R0G0B0A1_FLOAT,
		border_color_R1G1B1A1_FLOAT,
		border_color_R0G0B0A0_INT,
		border_color_R0G0B0A1_INT,
		border_color_R1G1B1A1_INT,
	};
	
	enum sampler_create_flags_t : uint32_t
	{
		sampler_create_anisotropy	= 1 << 0,
		sampler_create_compare		= 1 << 1,
	};

	using sampler_create_mask_t = uint32_t;

	struct sampler_create_t
	{
		sampler_create_mask_t	create_mask;
		address_mode_t			address_u;
		address_mode_t			address_v;
		address_mode_t			address_w;
		filter_t				min_filter;
		filter_t				mag_filter;
		filter_t				mip_filter;
		float					mip_bias;
		float					mip_min;
		float					mip_max;
		uint32_t				max_anisotropy;
		compare_operation_t		compare_operation;
		border_color_t			border_color;
	};

} }
