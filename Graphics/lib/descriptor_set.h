#pragma once
#include "texture.h"
#include "buffer.h"
#include "sampler.h"

namespace outland { namespace gpu 
{
	//////////////////////////////////////////////////
	// DESC LAYOUT
	//////////////////////////////////////////////////

	using descriptor_layout_id = struct descriptor_layout_t*;
	//enum descriptor_allocator_id : uintptr_t {};

	enum descriptor_type_t
	{
		descriptor_type_sampled_texture,
		descriptor_type_uniform_buffer,
		descriptor_type_storage_buffer,
		descriptor_type_render_target,
		descriptor_type_depth_stencil,
		descriptor_type_sampler, //exclusive
	};

	struct descriptor_desc_t
	{
		uint32_t				binding;
		descriptor_type_t		type;
		uint32_t				size;
		shader_stage_mask_t		stage_mask;
	};

	struct descriptor_layout_create_t
	{
		descriptor_desc_t const*		descriptor_array;
		uint32_t						descriptor_size;
	};

	//////////////////////////////////////////////////
	// DESC SET
	//////////////////////////////////////////////////

	using descriptor_set_id = struct descriptor_set_t*;
	
	union descriptor_state_t
	{
		shader_view_id	shader_view;
		buffer_id		buffer;
		sampler_id		sampler;
	};

	struct descriptor_set_state_t
	{
		descriptor_layout_id			layout;
		descriptor_state_t const*		descriptor_array;
		uint32_t						descriptor_size;
	};

	struct descriptor_set_create_t
	{	
		descriptor_set_state_t const*		state_array;
		size_t								state_size;
	};
	
} }
