#pragma once
#include "descriptor_set.h"
#include "render_pass.h"
#include "shader.h"

namespace outland { namespace gpu 
{

	//////////////////////////////////////////////////
	// PIPELINE LAYOUT
	//////////////////////////////////////////////////

	using pipeline_layout_id = struct pipeline_layout_t*;
	using input_layout_id = struct input_layout_t*;

	struct uniform_desc_t
	{
		uint32_t				size;
		shader_stage_mask_t		stage_mask;
	};

	struct transform_desc_t
	{
		uint32_t				binding;
		shader_stage_mask_t		stage_mask;
	};

	struct input_layout_create_t
	{
		uniform_desc_t const*			uniform_desc_array;
		uint32_t						uniform_desc_size;
		transform_desc_t const*			transform_desc_array;
		uint32_t						transform_desc_size;
	};

	struct pipeline_layout_create_t
	{
		descriptor_layout_id const*		descriptor_layout_array;
		uint32_t						descriptor_layout_size;
		input_layout_id					input_layout;
	};


	//////////////////////////////////////////////////
	// INPUT ASSEMBLY
	//////////////////////////////////////////////////

	enum primitive_topology_t
	{
		primitive_topology_point_list,
		primitive_topology_line_list,
		primitive_topology_line_strip,
		primitive_topology_triangle_list,
		primitive_topology_triangle_strip,
		primitive_topology_triangle_fan,
	};

	enum vertex_input_rate_t
	{
		vertex_input_rate_vertex,
		vertex_input_rate_instance,
	};

	struct vertex_desc_t
	{
		//uint32_t			slot;
		uint32_t			stride;
		vertex_input_rate_t rate;
	};

	struct attribute_desc_t
	{
		uint32_t		binding;
		format_t		format;
		uint32_t		offset;
		uint32_t		slot;
	};

	struct input_assembly_state_t
	{
		primitive_topology_t			topology;
		vertex_desc_t const*			vertex_array;
		uint32_t						vertex_size;
		attribute_desc_t const*			attribute_array;
		uint32_t						attribute_size;
	};

	//////////////////////////////////////////////////
	// VIEWPORT
	//////////////////////////////////////////////////

	struct viewport_t
	{
		float	x;
		float	y;
		float	width;
		float	height;
		float	depth_min;
		float	depth_max;
	};

	struct scissor_t
	{
		int32_t		x;
		int32_t		y;
		uint32_t	width;
		uint32_t	height;
	};

	struct viewport_state_t
	{
		uint32_t				size;			//viewports used by the pipeline. Is multiple are not supported, then it should be 1.
		viewport_t const*		viewport_array;	//ignored if dynamic. set to nullptr
		scissor_t const*		scissor_array;	//ignored if dynamic. set to nullptr
	};

	//////////////////////////////////////////////////
	// RASTERIZER
	//////////////////////////////////////////////////

	enum fill_mode_t
	{
		fill_mode_solid,
		fill_mode_wireframe,
	};

	enum cull_mode_t
	{
		cull_mode_none,
		cull_mode_front,
		cull_mode_back,
	};

	enum rasterizer_state_flags_t : uint32_t
	{
		rasterizer_state_front_face_counter_clockwise		= 1 << 0,
		rasterizer_state_depth_bias_enable					= 1 << 1,
		rasterizer_state_disable							= 1 << 2,
	};

	using rasterizer_state_mask_t = uint32_t;

	struct rasterizer_state_t
	{
		rasterizer_state_mask_t		state_mask;
		fill_mode_t					fill_mode;
		cull_mode_t					cull_mode;
		float						depth_bias_offset;
		float						depth_bias_slope;
		float						depth_bias_clamp;
	};

	//////////////////////////////////////////////////
	// DEPTH STENCIL
	//////////////////////////////////////////////////

	enum depth_stencil_state_flags_t : uint32_t
	{
		depth_stencil_state_depth_compare_enable	= 1 << 0,
		depth_stencil_state_depth_store_enable		= 1 << 1,
		//depth_stencil_state_stencil_test_enable, stencil not supported yet
	};

	using depth_stencil_state_mask_t = uint32_t;

	struct depth_stencil_state_t
	{
		depth_stencil_state_mask_t		state_mask;
		compare_operation_t				depth_compare_operation;
	};

	//////////////////////////////////////////////////
	// BLEND
	//////////////////////////////////////////////////

	enum blend_operation_t
	{
		blend_operation_add,
		blend_operation_subtract,
		blend_operation_rev_subtract,
		blend_operation_minimum,
		blend_operation_maximum,
	};

	enum blend_factor_t
	{
		blend_factor_zero,
		blend_factor_one,
		blend_factor_src_color,
		blend_factor_inv_src_color,
		blend_factor_dst_color,
		blend_factor_inv_dst_color,
		blend_factor_src_alpha,
		blend_factor_inv_src_alpha,
		blend_factor_dst_alpha,
		blend_factor_inv_dst_alpha,
		blend_factor_constant,
		blend_factor_inv_constant,
	};

	enum blend_desc_flags_t : uint32_t
	{
		blend_desc_blend_enabled	= 1 << 0,
		blend_desc_write_r			= 1 << 1,
		blend_desc_write_g			= 1 << 2,
		blend_desc_write_b			= 1 << 3,
		blend_desc_write_a			= 1 << 4,
	};

	using blend_desc_mask_t = uint32_t;

	struct blend_desc_t
	{
		blend_desc_mask_t	desc_mask;
		blend_operation_t	color_operation;
		blend_factor_t		color_src_factor;
		blend_factor_t		color_dst_factor;
		blend_operation_t	alpha_operation;
		blend_factor_t		alpha_src_factor;
		blend_factor_t		alpha_dst_factor;
	};

	struct blend_state_t
	{
		blend_desc_t const*		blend_array;
		uint32_t				blend_size; //must equal render targets in used subpasses
		float					constant[4];
	};

	//////////////////////////////////////////////////
	// SHADER
	//////////////////////////////////////////////////

	struct shader_state_t
	{
		shader_stage_flags_t	stage;
		shader_id				shader;
	};

	//////////////////////////////////////////////////
	// PIPELINE
	//////////////////////////////////////////////////

	using pipeline_id = struct pipeline_t*;

	enum pipeline_create_flags_t : uint32_t
	{
		pipeline_create_dynamic_viewport			= 1 << 0,
		pipeline_create_dynamic_scissor				= 1 << 1,
		pipeline_create_dynamic_depth_bias			= 1 << 2,
		pipeline_create_dynamic_blend_constant		= 1 << 3,
	};

	using pipeline_create_mask_t = uint32_t;

	struct pipeline_state_t
	{
		pipeline_create_mask_t	create_mask;
		pipeline_layout_id		layout;
		render_pass_id			pass;
		uint32_t				subpass;

		input_assembly_state_t const*	input_assembly_state;
		shader_state_t const*			shader_state_array;
		size_t							shader_state_size;
		viewport_state_t const*			viewport_state;
		depth_stencil_state_t const*	depth_stencil_state;
		rasterizer_state_t const*		rasterizer_state;
		blend_state_t const*			blend_state;
	};

	struct pipeline_create_t
	{
		pipeline_state_t const*		pipeline_array;
		size_t						pipeline_size;
	};
} }
