#include "pch.h"
#include "..\lib\module.h"

namespace outland { namespace gpu 
{

	error_t module_vulkan_create(module_t*& module, module_create_info_t const& info, scratch_allocator_t* scratch);
	void	module_vulkan_destroy(module_t* module);

} }

extern "C"
{
	//--------------------------------------------------
	outland::error_t	outland_gpu_module_create(outland::gpu::module_t** module, outland::gpu::module_create_info_t const* info, outland::scratch_allocator_t* scratch)
	{
		if (nullptr == module)
			return outland::err_invalid_parameter;

		if (nullptr == info)
			return outland::err_invalid_parameter;

		return module_vulkan_create(*module, *info, scratch);
	}

	//--------------------------------------------------
	void				outland_gpu_module_destroy(outland::gpu::module_t* module)
	{
		module_vulkan_destroy(module);
	}
}