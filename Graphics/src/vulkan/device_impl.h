#pragma once
//#include "device.h"
#include "vk_device.h"
#include "vk_upload.h"
#include "cmd_ctx_impl.h"
#include "resource_impl.h"

namespace outland { namespace gpu 
{

	class device_t
	{
		//core
		vk_device_t*					_device;
		allocator_t*					_allocator;
		vk_frame_queue_t*				_frame_queue;
		vk_cmd_heap_t					_cmd_heap;
		vk_resource_frame_t				_resource_frame;
		vk_render_frame_t				_render_frame;
		vk_upload_t						_upload;

		//functionality
		

		//managers
		vk_render_pass_manager_t		_render_pass_manager;
		vk_render_set_manager_t			_render_set_manager;
		vk_descriptor_layout_manager_t	_descriptor_layout_manager;
		vk_descriptor_manager_t			_descriptor_manager;
		vk_shader_manager_t				_shader_manager;
		vk_input_layout_manager_t		_input_layout_manager;
		vk_pipeline_layout_manager_t	_pipeline_layout_manager;
		vk_pipeline_manager_t			_pipeline_manager;
		vk_sampler_manager_t			_sampler_manager;
		vk_view_manager_t				_view_manager;
		vk_texture_manager_t			_texture_manager;
		vk_buffer_manager_t				_buffer_manager;
		
		//vallocators
		best_fit_vallocator_t			_texture_sampled_vallocator;
		best_fit_vallocator_t			_buffer_uniform_vallocator;
		best_fit_vallocator_t			_buffer_vertex_vallocator;
		best_fit_vallocator_t			_buffer_index_vallocator;

		VkDeviceSize					_max_transform_size;

	public:
		device_t(vk_device_t* device, vk_frame_queue_t* frame_queue, allocator_t* allocator);
		error_t		init(VkPhysicalDevice physical_device, scratch_allocator_t* scratch);
		void		clean();

		vk_descriptor_manager_t*	descriptor_manager() { return &_descriptor_manager; }
		vk_device_t*				device() { return _device; }
		vk_frame_queue_t*			frame_queue() { return _frame_queue; }
		vk_cmd_heap_t*				cmd_heap() { return &_cmd_heap; }

		friend void		device::update(device_id device, device_event_t* event_array, size_t event_capacity, size_t& event_size);

		friend error_t	device::swap_chain_create(device_id device, swap_chain_id& swap_chain, swap_chain_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::swap_chain_destroy(device_id device, swap_chain_id swap_chain);
		friend error_t	device::cmd_allocator_create(device_id device, cmd_allocator_id& allocator, cmd_allocator_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::cmd_allocator_destroy(device_id device, cmd_allocator_id allocator);
		friend error_t	device::render_pass_create(device_id device, render_pass_id& pass, render_pass_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::render_pass_destroy(device_id device, render_pass_id pass);
		friend error_t	device::render_set_create(device_id device, render_set_id& set, render_set_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::render_set_destroy(device_id device, render_set_id set);
		friend error_t	device::descriptor_layout_create(device_id device, descriptor_layout_id& layout, descriptor_layout_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::descriptor_layout_destroy(device_id device, descriptor_layout_id layout);
		friend error_t	device::descriptor_set_create(device_id device, descriptor_set_id* set_array, descriptor_set_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::descriptor_set_destroy(device_id device, descriptor_set_id const* set_array, size_t set_size);
		friend error_t	device::shader_create(device_id device, shader_id& shader, shader_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::shader_destroy(device_id device, shader_id shader);
		friend error_t	device::input_layout_create(device_id device, input_layout_id& layout, input_layout_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::input_layout_destroy(device_id device, input_layout_id layout);
		friend error_t	device::pipeline_layout_create(device_id device, pipeline_layout_id& layout, pipeline_layout_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::pipeline_layout_destroy(device_id device, pipeline_layout_id layout);
		friend error_t	device::pipeline_create(device_id device, pipeline_id* pipeline_array, pipeline_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::pipeline_destroy(device_id device, pipeline_id const* pipeline_array, size_t pipeline_size);
		friend error_t	device::sampler_create(device_id device, sampler_id& sampler, sampler_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::sampler_destroy(device_id device, sampler_id sampler);
		friend error_t	device::texture_create(device_id device, void* user_data, texture_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::texture_destroy(device_id device, texture_id texture);
		friend error_t	device::shader_view_create(device_id device, shader_view_id& view, texture_shader_view_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::shader_view_destroy(device_id device, shader_view_id view);
		friend error_t	device::render_target_view_create(device_id device, render_target_view_id& view, render_target_view_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::render_target_view_destroy(device_id device, render_target_view_id view);
		friend error_t	device::depth_stencil_view_create(device_id device, depth_stencil_view_id& view, depth_stencil_view_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::depth_stencil_view_destroy(device_id device, depth_stencil_view_id view);
		friend error_t	device::buffer_create(device_id device, void* user_data, buffer_create_t const& create, scratch_allocator_t* scratch);
		friend void		device::buffer_destroy(device_id device, buffer_id buffer);

		friend error_t	device::buffer_copy(device_id device, host_to_buffer_copy_t const& copy);
		friend error_t	device::texture_copy(device_id device, host_to_texture_copy_t const& copy);
	};

} }
