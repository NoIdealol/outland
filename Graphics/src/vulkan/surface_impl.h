#pragma once
#include "vk_instance.h"

namespace outland { namespace gpu
{

#if defined(VK_USE_SWAP_CHAIN_EXTENSION)

	extern char8_t const* const VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME;

	struct vk_surface_capabilities_t
	{
		VkSurfaceCapabilitiesKHR	surface_capabilities;
		size_t						present_mode_size;
		VkPresentModeKHR			present_mode_array[4];
	};

	class surface_t
	{
		vk_instance_t*	_instance;
		VkSurfaceKHR	_surface;

	public:
		surface_t(vk_instance_t* instance);
		VkResult	init(void const* window_context_ptr, void const* window_instance_ptr);
		void		clean();
		VkResult	get_capabilities(VkPhysicalDevice physical_device, vk_surface_capabilities_t& capabilities);
		VkResult	get_valid_format(VkPhysicalDevice physical_device,
			VkSurfaceFormatKHR const* prefered_surface_format_array,
			size_t prefered_surface_format_size,
			VkSurfaceFormatKHR& valid_format,
			scratch_allocator_t* scratch);
		VkResult	enumerate_format(VkPhysicalDevice physical_device, array<VkSurfaceFormatKHR>& surface_format_array);

		VkSurfaceKHR	surface() const { return _surface; }
	};


#endif

} }
