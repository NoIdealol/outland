#pragma once
#include "vk_library.h"
//#include "surface_vk.h"

//////////////////////////////////////////////////
// INSTANCE
//////////////////////////////////////////////////

#if defined(VK_USE_SWAP_CHAIN_EXTENSION)

	#define VK_FNC_INSTANCE_SWAP_CHAIN(DO_MACRO) \
		DO_MACRO(vkDestroySurfaceKHR) \
		DO_MACRO(vkGetPhysicalDeviceSurfaceSupportKHR) \
		DO_MACRO(vkGetPhysicalDeviceSurfaceCapabilitiesKHR) \
		DO_MACRO(vkGetPhysicalDeviceSurfaceFormatsKHR) \
		DO_MACRO(vkGetPhysicalDeviceSurfacePresentModesKHR) \

#else

	#define VK_FNC_INSTANCE_SWAP_CHAIN(DO_MACRO)

#endif

#define VK_FNC_INSTANCE_UNIVERSAL(DO_MACRO) \
	DO_MACRO(vkDestroyInstance) \
	DO_MACRO(vkEnumeratePhysicalDevices) \
	DO_MACRO(vkGetPhysicalDeviceProperties) \
	DO_MACRO(vkGetPhysicalDeviceFeatures) \
	DO_MACRO(vkGetPhysicalDeviceQueueFamilyProperties) \
	DO_MACRO(vkGetPhysicalDeviceMemoryProperties) \
	DO_MACRO(vkCreateDevice) \
	DO_MACRO(vkGetDeviceProcAddr) \
	DO_MACRO(vkEnumerateDeviceExtensionProperties) \

#define VK_FNC_INSTANCE(DO_MACRO) \
	VK_FNC_INSTANCE_UNIVERSAL(DO_MACRO) \
	VK_FNC_INSTANCE_SWAP_CHAIN(DO_MACRO) \

#define VK_FNC_INSTANCE_EXT_DEBUG(DO_MACRO) \
	DO_MACRO(vkCreateDebugUtilsMessengerEXT) \
	DO_MACRO(vkDestroyDebugUtilsMessengerEXT) \

namespace outland { namespace gpu
{
	struct vk_instance_vtable_t
	{
		VK_FNC_INSTANCE(VK_FNC_PROTOTYPE);
		VK_FNC_INSTANCE_EXT_DEBUG(VK_FNC_PROTOTYPE);
	};

	class vk_instance_t
	{
		vk_library_t*					_library;
		VkInstance						_instance;
		VkAllocationCallbacks const*	_allocation_cb;
		vk_instance_vtable_t			_vtable;
		VkDebugUtilsMessengerEXT		_debug;

	private:
		VkResult	_load(module_create_mask_t create_mask);
		VkResult	_create(module_create_mask_t create_mask, char8_t const* const* extension_array, size_t extension_size, char8_t const* const* layer_array, size_t layer_size);
		VkResult	_register_debug_callback();

		static VKAPI_ATTR VkBool32 VKAPI_CALL _debug_callback(
			VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
			VkDebugUtilsMessageTypeFlagsEXT messageType,
			const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			void* pUserData);
	public:
		vk_instance_t(vk_library_t* library, VkAllocationCallbacks const*	allocation_cb);
		VkResult						init(module_create_mask_t create_mask, scratch_allocator_t* scratch);
		void							clean();
		vk_instance_vtable_t const*		vtable() const { return &_vtable; }
		VkInstance						instance() const { return _instance; }
		VkAllocationCallbacks const*	allocation_cb() const { return _allocation_cb; }
		vk_library_t*					library() const { return _library; }

		VkResult	get_device_extension_array(array<char8_t const*>& extension_array);
		VkResult	enumerate_physical_devices(array<VkPhysicalDevice>& device_array);
		VkResult	enumerate_device_queue_props(VkPhysicalDevice physical_device, array<VkQueueFamilyProperties>& props_array);
		VkResult	check_device_extensions(VkPhysicalDevice physical_device, char8_t const* const* extension_array, size_t extension_size, scratch_allocator_t* scratch);
		VkResult	check_device_capabilities(VkPhysicalDeviceProperties const& device_properties);
		VkResult	check_device_queues(VkPhysicalDevice physical_device, uint32_t& graphics_queue_family_index, uint32_t& copy_queue_family_index, scratch_allocator_t* scratch);
	};


} }

