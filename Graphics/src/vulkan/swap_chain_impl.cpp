#include "pch.h"
#include "swap_chain_impl.h"

namespace outland { namespace gpu
{
#if defined(VK_USE_SWAP_CHAIN_EXTENSION)

	swap_chain_t::swap_chain_t(vk_device_t* device, surface_t* surface)
		: _device(device)
		, _surface(surface)
		, _swap_chain(VK_NULL_HANDLE)
		, _back_buffer_size(0)
	{

	}

	VkResult	swap_chain_t::_find_queue(scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock = { scratch };

		VkResult vk_result = VK_SUCCESS;

		auto& queue_array = _device->queue_array();
		VkBool32* supoported_array;
		if (u::mem_alloc<VkBool32>(scratch, supoported_array, queue_array.size()))
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		for (size_t i = 0; i < queue_array.size(); ++i)
		{
			vk_result = _device->instance()->vtable()->vkGetPhysicalDeviceSurfaceSupportKHR(
				_device->physical_device(),
				queue_array[i].family_index,
				_surface->surface(),
				supoported_array + i);

			if (vk_result)
				supoported_array[i] = VK_FALSE;
		}

		auto* queue_graphics = _device->queue_graphics();
		_graphics_queue = queue_graphics;

		//prefer the graphics queue
		for (size_t i = 0; i < queue_array.size(); ++i)
		{
			if (queue_graphics->family_index == queue_array[i].family_index)
			{
				if (supoported_array[i])
				{
					_present_queue = queue_graphics;
					return VK_SUCCESS;
				}
				else
				{
					break;
				}
			}
		}

		//find a present queue
		for (size_t i = 0; i < queue_array.size(); ++i)
		{
			if (supoported_array[i])
			{
				_present_queue = &queue_array[i];
				return VK_SUCCESS;
			}
		}

		return VK_ERROR_INCOMPATIBLE_DISPLAY_KHR;
	}

	VkResult swap_chain_t::_create_swap_chain(
		VkPresentModeKHR present_mode,
		VkCompositeAlphaFlagBitsKHR composite_alpha_bit,
		VkImageUsageFlags image_usage_flags,
		VkExtent2D image_extent,
		uint32_t image_min_count,
		VkSurfaceFormatKHR surface_format,
		VkSurfaceTransformFlagBitsKHR surface_transform_bit,
		scratch_allocator_t* scratch)
	{
		VkResult vk_result = VK_SUCCESS;

		vk_result = _find_queue(scratch);
		if (vk_result)
			return vk_result;

		vk_result = _surface->get_capabilities(_device->physical_device(), _surface_capabilities);
		if (vk_result)
			return vk_result;

		vk_result = _surface->get_valid_format(_device->physical_device(), &surface_format, 1, _surface_format, scratch);
		if (vk_result)
			return vk_result;

		auto& surface_capabilities = _surface_capabilities.surface_capabilities;

		if (image_usage_flags != (surface_capabilities.supportedUsageFlags & image_usage_flags))
			return VK_ERROR_FEATURE_NOT_PRESENT;
		_image_usage_flags = image_usage_flags;

		if (surface_transform_bit != (surface_capabilities.supportedTransforms & surface_transform_bit))
			return VK_ERROR_FEATURE_NOT_PRESENT;
		_surface_transform_bit = surface_transform_bit;

		if (composite_alpha_bit != (surface_capabilities.supportedCompositeAlpha & composite_alpha_bit))
			return VK_ERROR_FEATURE_NOT_PRESENT;
		_composite_alpha_bit = composite_alpha_bit;

		if (image_min_count > surface_capabilities.maxImageCount ||
			image_min_count < surface_capabilities.minImageCount)
			return VK_ERROR_FEATURE_NOT_PRESENT;

		_image_min_count = image_min_count;

		if (image_extent.width > surface_capabilities.maxImageExtent.width ||
			image_extent.height > surface_capabilities.maxImageExtent.height ||
			image_extent.width < surface_capabilities.minImageExtent.width ||
			image_extent.height < surface_capabilities.minImageExtent.height)
			return VK_ERROR_FEATURE_NOT_PRESENT;

		_image_extent = image_extent;

		vk_result = VK_ERROR_FEATURE_NOT_PRESENT;
		for (size_t i = 0; i < _surface_capabilities.present_mode_size; ++i)
		{
			if (present_mode == _surface_capabilities.present_mode_array[i])
			{
				_present_mode = present_mode;
				vk_result = VK_SUCCESS;
				break;
			}
		}
		if (vk_result)
			return vk_result;

		VkBool32 clipped = (0 == (image_usage_flags & (VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT))) ? VK_TRUE : VK_FALSE;

		VkSwapchainCreateInfoKHR swap_chain_create_info;
		swap_chain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swap_chain_create_info.pNext = nullptr;
		swap_chain_create_info.flags = 0;
		swap_chain_create_info.surface = _surface->surface();
		swap_chain_create_info.minImageCount = _image_min_count;
		swap_chain_create_info.imageFormat = _surface_format.format;
		swap_chain_create_info.imageColorSpace = _surface_format.colorSpace;
		swap_chain_create_info.imageExtent = _image_extent;
		swap_chain_create_info.imageArrayLayers = 1;
		swap_chain_create_info.imageUsage = _image_usage_flags;
		swap_chain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swap_chain_create_info.queueFamilyIndexCount = 0;
		swap_chain_create_info.pQueueFamilyIndices = nullptr;
		swap_chain_create_info.preTransform = _surface_transform_bit;
		swap_chain_create_info.compositeAlpha = _composite_alpha_bit;
		swap_chain_create_info.presentMode = _present_mode;
		swap_chain_create_info.clipped = clipped;
		swap_chain_create_info.oldSwapchain = _swap_chain;

		vk_result = _device->vtable()->vkCreateSwapchainKHR(_device->device(), &swap_chain_create_info, _device->allocation_cb(), &_swap_chain);
		if (vk_result)
			return vk_result;

		if (swap_chain_create_info.oldSwapchain != VK_NULL_HANDLE)
		{
			_device->vtable()->vkDestroySwapchainKHR(_device->device(), swap_chain_create_info.oldSwapchain, _device->allocation_cb());
		}

		return VK_SUCCESS;
	}

	void swap_chain_t::_destroy_swap_chain()
	{
		_device->vtable()->vkDestroySwapchainKHR(_device->device(), _swap_chain, _device->allocation_cb());
	}


	VkResult swap_chain_t::_create_back_buffers(vk_back_buffer_state_t const& release_buffer_state, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock = { scratch };

		VkResult vk_result;

		uint32_t image_count = 0;
		VkImage* image_array = nullptr;
		
		//get images
		{
			vk_result = _device->vtable()->vkGetSwapchainImagesKHR(_device->device(), _swap_chain, &image_count, image_array);
			if (vk_result)
				return vk_result;

			if (_image_min_count > image_count)
				return VK_ERROR_FEATURE_NOT_PRESENT;

			if (u::mem_alloc<VkImage>(scratch, image_array, image_count))
				return VK_ERROR_OUT_OF_HOST_MEMORY;

			vk_result = _device->vtable()->vkGetSwapchainImagesKHR(_device->device(), _swap_chain, &image_count, image_array);
			if (vk_result)
				return vk_result;

			if (u::array_size(_back_buffer_array) < image_count)
				return VK_ERROR_OUT_OF_HOST_MEMORY;

			_back_buffer_size = image_count;
		}

		//create cmd pool
		{
			VkCommandPoolCreateInfo cmd_pool_create_info;
			cmd_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			cmd_pool_create_info.pNext = nullptr;
			cmd_pool_create_info.flags = 0; //we want a long lived buffer without reseting.
			cmd_pool_create_info.queueFamilyIndex = _present_queue->family_index;

			vk_result = _device->vtable()->vkCreateCommandPool(_device->device(), &cmd_pool_create_info, _device->allocation_cb(), &_cmd_pool);
			if (vk_result)
			{
				_back_buffer_size = 0;
				return vk_result;
			}
		}

		VkCommandBuffer* cmd_buffer_array = nullptr;
		uint32_t image_index = 0;
		{
			if (u::mem_alloc<VkCommandBuffer>(scratch, cmd_buffer_array, image_count))
			{
				vk_result = VK_ERROR_OUT_OF_HOST_MEMORY;
				goto _clean_cmd_pool;
			}

			VkCommandBufferAllocateInfo cmd_buffer_alloc_info;
			cmd_buffer_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			cmd_buffer_alloc_info.pNext = nullptr;
			cmd_buffer_alloc_info.commandPool = _cmd_pool;
			cmd_buffer_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
			cmd_buffer_alloc_info.commandBufferCount = image_count;

			vk_result = _device->vtable()->vkAllocateCommandBuffers(_device->device(), &cmd_buffer_alloc_info, cmd_buffer_array);
			if (vk_result)
				goto _clean_cmd_pool;	
		}

		{
			vk_result = _device->semaphore_create(_acquire_semaphore);
			if (vk_result)
			{
				goto _clean_cmd_buffers;
			}
		}

		while (image_index < image_count)
		{
			auto& back_buffer = _back_buffer_array[image_index];
			back_buffer.cmd_buffer = cmd_buffer_array[image_index];
			back_buffer.image = image_array[image_index];

			//record barrier
			{
				VkImageSubresourceRange subresourceRange;
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.baseArrayLayer = 0;
				subresourceRange.layerCount = 1;

				VkImageMemoryBarrier image_memory_barrier;
				image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				image_memory_barrier.pNext = nullptr;
				image_memory_barrier.srcAccessMask = release_buffer_state.access_mask; // 0;
				image_memory_barrier.dstAccessMask = 0;
				image_memory_barrier.oldLayout = release_buffer_state.layout;// VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
				image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
				image_memory_barrier.srcQueueFamilyIndex = _graphics_queue->family_index;
				image_memory_barrier.dstQueueFamilyIndex = _present_queue->family_index;
				image_memory_barrier.image = back_buffer.image;
				image_memory_barrier.subresourceRange = subresourceRange;

				VkCommandBufferBeginInfo cmd_buffer_begin_info;
				cmd_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				cmd_buffer_begin_info.pNext = nullptr;
				cmd_buffer_begin_info.flags = 0;// VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
				cmd_buffer_begin_info.pInheritanceInfo = nullptr;

				vk_result = _device->cmd_vtable()->vkBeginCommandBuffer(back_buffer.cmd_buffer, &cmd_buffer_begin_info);
				if (vk_result)
				{
					goto _clean_back_buffers;
				}

				_device->cmd_vtable()->vkCmdPipelineBarrier(
					back_buffer.cmd_buffer,
					release_buffer_state.stage_mask,// VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
					VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &image_memory_barrier
				);

				vk_result = _device->cmd_vtable()->vkEndCommandBuffer(back_buffer.cmd_buffer);
				if (vk_result)
				{
					goto _clean_back_buffers;
				}
			}

			//create sync primitives
			{
				vk_result = _device->semaphore_create(back_buffer.present_semaphore);
				if (vk_result)
				{
					goto _clean_back_buffers;
				}

				VkImageViewCreateInfo iw_ci;
				iw_ci.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				iw_ci.pNext = nullptr;
				iw_ci.flags = 0;
				iw_ci.image = back_buffer.image;
				iw_ci.viewType = VK_IMAGE_VIEW_TYPE_2D;
				iw_ci.format = _surface_format.format;
				iw_ci.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
				iw_ci.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
				iw_ci.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
				iw_ci.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
				iw_ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				iw_ci.subresourceRange.baseArrayLayer = 0;
				iw_ci.subresourceRange.layerCount = 1;
				iw_ci.subresourceRange.baseMipLevel = 0;
				iw_ci.subresourceRange.levelCount = 1;

				vk_result = _device->vtable()->vkCreateImageView(_device->device(), &iw_ci, _device->allocation_cb(), &back_buffer.view.view);
				//vk_result = device_fence_create(device, back_buffer.present_fence, true);
				if (vk_result)
				{
					_device->semaphore_destroy(back_buffer.present_semaphore);
					goto _clean_back_buffers;
				}

				vk_result = _device->semaphore_create(back_buffer.acquire_semaphore);
				if (vk_result)
				{
					_device->vtable()->vkDestroyImageView(_device->device(), back_buffer.view.view, _device->allocation_cb());
					//device_fence_destroy(device, back_buffer.present_fence);
					_device->semaphore_destroy(back_buffer.present_semaphore);
					goto _clean_back_buffers;
				}

				vk_result = _device->semaphore_create(back_buffer.release_semaphore);
				if (vk_result)
				{
					_device->semaphore_destroy(back_buffer.release_semaphore);
					_device->vtable()->vkDestroyImageView(_device->device(), back_buffer.view.view, _device->allocation_cb());
					//device_fence_destroy(device, back_buffer.present_fence);
					_device->semaphore_destroy(back_buffer.present_semaphore);
					goto _clean_back_buffers;
				}
				
			}

			++image_index;
		}

		return VK_SUCCESS;

	_clean_back_buffers:
		for (uint32_t i = 0; i < image_index; ++i)
		{
			auto& back_buffer = _back_buffer_array[i];

			_device->semaphore_destroy(back_buffer.release_semaphore);
			_device->semaphore_destroy(back_buffer.acquire_semaphore);
			_device->vtable()->vkDestroyImageView(_device->device(), back_buffer.view.view, _device->allocation_cb());
			//device_fence_destroy(device, back_buffer.present_fence);
			_device->semaphore_destroy(back_buffer.present_semaphore);
		}
		_back_buffer_size = 0;
		_device->semaphore_destroy(_acquire_semaphore);
	_clean_cmd_buffers:
		_device->vtable()->vkFreeCommandBuffers(_device->device(), _cmd_pool, image_count, cmd_buffer_array);
	_clean_cmd_pool:
		_device->vtable()->vkDestroyCommandPool(_device->device(), _cmd_pool, _device->allocation_cb());

		return vk_result;
	}

	void swap_chain_t::_destroy_back_buffers()
	{
		size_t image_count = _back_buffer_size;

		for (size_t i = 0; i < image_count; ++i)
		{
			auto& back_buffer = _back_buffer_array[i];

			_device->semaphore_destroy(back_buffer.release_semaphore);
			_device->semaphore_destroy(back_buffer.acquire_semaphore);
			_device->vtable()->vkDestroyImageView(_device->device(), back_buffer.view.view, _device->allocation_cb());
			//device_fence_destroy(device, back_buffer.present_fence);
			_device->semaphore_destroy(back_buffer.present_semaphore);

			_device->vtable()->vkFreeCommandBuffers(_device->device(), _cmd_pool, 1, &back_buffer.cmd_buffer);
		}

		_device->semaphore_destroy(_acquire_semaphore);
	
		_device->vtable()->vkDestroyCommandPool(_device->device(), _cmd_pool, _device->allocation_cb());

		_back_buffer_size = 0;
	}

	bool swap_chain_t::_is_same_queue()
	{
		return _graphics_queue->family_index == _present_queue->family_index;
	}

	void swap_chain_t::graphics_acquire(VkCommandBuffer graphics_command_buffer)
	{
		auto& back_buffer = _back_buffer_array[_image_current_index];

		/* Queue ownership transfer is only required when we need the content to remain valid across queues.
		Since we are transitioning from UNDEFINED -- and therefore discarding the image contents to begin with --
		we are not required to perform an ownership transfer from the presentation queue to graphics.

		This transition could also be made as an EXTERNAL -> subpass #0 render pass dependency as shown earlier. */

		VkImageSubresourceRange subresourceRange;
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = 1;
		subresourceRange.baseArrayLayer = 0;
		subresourceRange.layerCount = 1;

		VkImageMemoryBarrier imageMemoryBarrier;
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.pNext = nullptr;
		imageMemoryBarrier.srcAccessMask = 0;
		imageMemoryBarrier.dstAccessMask = _acquire_buffer_state.access_mask;//VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageMemoryBarrier.newLayout = _acquire_buffer_state.layout;//VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.image = back_buffer.image;
		imageMemoryBarrier.subresourceRange = subresourceRange;

		//recommended same stages for src and dst
		_device->cmd_vtable()->vkCmdPipelineBarrier(
			graphics_command_buffer,
			_acquire_buffer_state.stage_mask,
			_acquire_buffer_state.stage_mask,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier
		);
	}

	void swap_chain_t::graphics_release(VkCommandBuffer graphics_command_buffer)
	{
		uint32_t srcQueueFamilyIndex;
		uint32_t dstQueueFamilyIndex;

		if (_is_same_queue())
		{
			srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		}
		else
		{
			srcQueueFamilyIndex = _graphics_queue->family_index;
			dstQueueFamilyIndex = _present_queue->family_index;
		}

		// Queue release operation. dstAccessMask should always be 0.
		VkImageSubresourceRange subresourceRange;
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = 1;
		subresourceRange.baseArrayLayer = 0;
		subresourceRange.layerCount = 1;

		VkImageMemoryBarrier imageMemoryBarrier;
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.pNext = nullptr;
		imageMemoryBarrier.srcAccessMask = _release_buffer_state.access_mask;// VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		imageMemoryBarrier.dstAccessMask = 0;
		imageMemoryBarrier.oldLayout = _release_buffer_state.layout;// VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		imageMemoryBarrier.srcQueueFamilyIndex = srcQueueFamilyIndex;
		imageMemoryBarrier.dstQueueFamilyIndex = dstQueueFamilyIndex;
		imageMemoryBarrier.image = _back_buffer_array[_image_current_index].image;
		imageMemoryBarrier.subresourceRange = subresourceRange;
		
		//no need to block at dst stage for present
		_device->cmd_vtable()->vkCmdPipelineBarrier(
			graphics_command_buffer,
			_release_buffer_state.stage_mask, // VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier
		);
	}

	VkResult swap_chain_t::present_acquire()
	{
		VkResult vk_result;

		auto& back_buffer = _back_buffer_array[_image_current_index];

		if (!_is_same_queue())
		{
			VkPipelineStageFlags wait_dst_stage_mask = _release_buffer_state.stage_mask;// VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

			VkSubmitInfo submit_info;
			submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submit_info.pNext = nullptr;
			submit_info.waitSemaphoreCount = 1;
			submit_info.pWaitSemaphores = &back_buffer.release_semaphore;
			submit_info.pWaitDstStageMask = &wait_dst_stage_mask;
			submit_info.commandBufferCount = 1;
			submit_info.pCommandBuffers = &back_buffer.cmd_buffer;
			submit_info.signalSemaphoreCount = 1;
			submit_info.pSignalSemaphores = &back_buffer.present_semaphore;

			//can return OOM. Primitives are signaled
			vk_result = _device->vtable()->vkQueueSubmit(_present_queue->queue, 1, &submit_info, /*back_buffer.present_fence*/VK_NULL_HANDLE);
			if (vk_result)
				return vk_result;

		}

		return VK_SUCCESS;
	}

	VkResult swap_chain_t::present_swap_buffers()
	{
		VkResult vk_result;

		auto& back_buffer = _back_buffer_array[_image_current_index];

		VkSemaphore present_semaphore;
		if (_is_same_queue())
		{
			present_semaphore = back_buffer.release_semaphore;
		}
		else
		{
			present_semaphore = back_buffer.present_semaphore;
		}
		
		VkPresentInfoKHR present_info;
		present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		present_info.pNext = nullptr;
		present_info.waitSemaphoreCount = 1;
		present_info.pWaitSemaphores = &present_semaphore;
		present_info.swapchainCount = 1;
		present_info.pSwapchains = &_swap_chain;
		present_info.pImageIndices = &_image_current_index;
		present_info.pResults = nullptr;

		vk_result = _device->vtable()->vkQueuePresentKHR(_present_queue->queue, &present_info);

		return vk_result;	
	}

	/*VkResult swap_chain_wait_for_image(swap_chain_t* swap_chain, uint32_t image_index)
	{	
		VkResult vk_result;

		auto& engine = swap_chain->swap_engine;
		auto& back_buffer = engine.back_buffer_array[image_index];

		//make sure the prev image is acquired and rendering is done
		vk_result = swap_chain->device->vkWaitForFences(swap_chain->device->device, 1, &back_buffer.present_fence, VK_TRUE, UINT64_MAX);	

		return vk_result;
	}*/

	VkResult swap_chain_t::wait_next_image()
	{
		VkResult vk_result;

		vk_result = _device->vtable()->vkAcquireNextImageKHR(
			_device->device(),
			_swap_chain,
			UINT64_MAX,
			_acquire_semaphore,
			VK_NULL_HANDLE,
			&_image_current_index);

		switch (vk_result)
		{	
		case VK_SUCCESS:
		case VK_SUBOPTIMAL_KHR:
			break;
			/*VK_TIMEOUT
			VK_NOT_READY
			VK_ERROR_OUT_OF_HOST_MEMORY
			VK_ERROR_OUT_OF_DEVICE_MEMORY
			VK_ERROR_DEVICE_LOST
			VK_ERROR_OUT_OF_DATE_KHR
			VK_ERROR_SURFACE_LOST_KHR*/
		default:
			return vk_result;
		}

		auto& back_buffer = _back_buffer_array[_image_current_index];
		
		//swap semaphores
		{
			VkSemaphore temp_semaphore = _acquire_semaphore;
			_acquire_semaphore = back_buffer.acquire_semaphore;
			back_buffer.acquire_semaphore = temp_semaphore;
		}

		return vk_result;
	}

	void swap_chain_t::back_buffer_info(vk_back_buffer_info_t& info)
	{
		auto& back_buffer = _back_buffer_array[_image_current_index];

		info.release_semaphore = back_buffer.release_semaphore;
		info.acquire_semaphore = back_buffer.acquire_semaphore;
		info.image_index = _image_current_index;

		info.acquire_stage_mask = _acquire_buffer_state.stage_mask;
		info.release_stage_mask = _release_buffer_state.stage_mask;
	}

	void swap_chain_t::get_view(render_target_view_t*& view, size_t index)
	{
		auto& back_buffer = _back_buffer_array[index];
		view = &back_buffer.view;
	}
	
	VkResult swap_chain_t::init(swap_chain_create_vk_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result = VK_SUCCESS;
		
		VkCompositeAlphaFlagBitsKHR const composite_alpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		VkImageUsageFlags const image_usage_flags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;// | VK_IMAGE_USAGE_TRANSFER_DST_BIT; // Color attachment flag must always be supported
		VkSurfaceTransformFlagBitsKHR const surface_transform_bit = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		VkPresentModeKHR const present_mode = create.present_mode;// VK_PRESENT_MODE_FIFO_KHR;
		uint32_t const image_min_count = create.image_min_count;// 2;
		VkExtent2D const image_extent = create.image_extent;// { 800, 600 };
		VkSurfaceFormatKHR const surface_format = create.surface_format;// { VK_FORMAT_B8G8R8A8_UNORM , VK_COLOR_SPACE_SRGB_NONLINEAR_KHR }

		vk_result = _create_swap_chain(
			present_mode, 
			composite_alpha, 
			image_usage_flags, 
			image_extent, 
			image_min_count, 
			surface_format, 
			surface_transform_bit, 
			scratch);

		if (vk_result)
			return vk_result;

		vk_result = _create_back_buffers(create.release_buffer_state, scratch);
		if (vk_result)
		{
			_destroy_swap_chain();
			return vk_result;
		}

		_release_buffer_state = create.release_buffer_state;
		_acquire_buffer_state = create.acquire_buffer_state;

		return vk_result;
	}

	void swap_chain_t::clean()
	{
		_destroy_back_buffers();
		_destroy_swap_chain();
	}

	
	error_t	swap_chain::back_buffer_enumerate(swap_chain_id swap_chain, render_target_view_id* view_array, size_t view_capacity, size_t& view_size)
	{
		if (nullptr == view_array)
		{
			view_size = swap_chain->back_buffer_size();
			return err_ok;
		}

		if (view_capacity < swap_chain->back_buffer_size())
			return err_invalid_parameter;

		for (size_t i = 0; i < swap_chain->back_buffer_size(); ++i)
		{
			swap_chain->get_view(view_array[i], i);
		}

		view_size = swap_chain->back_buffer_size();
		return err_ok;
	}
	

#endif

} }

