#pragma once
//#include "swap_chain.h"
//#include "cmd_list_vk.h"
#include "vk_device.h"
#include "surface_impl.h"
#include "resource_impl.h"

namespace outland { namespace gpu
{

#if defined(VK_USE_SWAP_CHAIN_EXTENSION)

	struct back_buffer_vk_t
	{
		VkImage					image;
		render_target_view_t	view;
		VkSemaphore			present_semaphore;	//semaphore signaling presentation can be done
		VkSemaphore			acquire_semaphore;	//semaphore signaling image is ready
		VkSemaphore			release_semaphore;	//semaphore signaling image is ready
		VkCommandBuffer		cmd_buffer;			//cmd_buffer for image transfer and barrier
	};

	struct vk_back_buffer_info_t
	{
		VkSemaphore				acquire_semaphore;
		VkSemaphore				release_semaphore;
		uint32_t				image_index;
		VkPipelineStageFlags	acquire_stage_mask;
		VkPipelineStageFlags	release_stage_mask;
	};

	struct vk_back_buffer_state_t
	{
		VkAccessFlags			access_mask;
		VkImageLayout			layout;
		VkPipelineStageFlags	stage_mask;
	};

	struct swap_chain_create_vk_t
	{
		VkPresentModeKHR		present_mode;
		uint32_t				image_min_count;
		VkExtent2D				image_extent;
		VkSurfaceFormatKHR		surface_format;
		vk_back_buffer_state_t	release_buffer_state;
		vk_back_buffer_state_t	acquire_buffer_state;
	};

	class swap_chain_t
	{
		vk_device_t*							_device;
		surface_t*								_surface;
		VkSwapchainKHR							_swap_chain;
		VkPresentModeKHR						_present_mode;
		vk_queue_t const*						_present_queue;
		vk_queue_t const*						_graphics_queue;
		VkCompositeAlphaFlagBitsKHR				_composite_alpha_bit;
		VkImageUsageFlags						_image_usage_flags;
		VkExtent2D								_image_extent;
		uint32_t								_image_min_count;
		vk_back_buffer_state_t					_release_buffer_state;
		vk_back_buffer_state_t					_acquire_buffer_state;

		VkSurfaceFormatKHR						_surface_format;
		VkSurfaceTransformFlagBitsKHR			_surface_transform_bit;
		vk_surface_capabilities_t				_surface_capabilities;
		
		VkCommandPool							_cmd_pool;
		back_buffer_vk_t						_back_buffer_array[4];
		size_t									_back_buffer_size;
		uint32_t								_image_current_index;
		VkSemaphore								_acquire_semaphore;

	private:
		VkResult	_find_queue(scratch_allocator_t* scratch);
		VkResult	_create_swap_chain(
			VkPresentModeKHR present_mode,
			VkCompositeAlphaFlagBitsKHR composite_alpha_bit,
			VkImageUsageFlags image_usage_flags,
			VkExtent2D image_extent,
			uint32_t image_min_count,
			VkSurfaceFormatKHR surface_format,
			VkSurfaceTransformFlagBitsKHR surface_transform_bit,
			scratch_allocator_t* scratch);
		void		_destroy_swap_chain();

		VkResult	_create_back_buffers(vk_back_buffer_state_t const& release_buffer_state, scratch_allocator_t* scratch);
		void		_destroy_back_buffers();
		bool		_is_same_queue();

	public:
		swap_chain_t(vk_device_t* device, surface_t* surface);

		VkResult	init(swap_chain_create_vk_t const& create_info, scratch_allocator_t* scratch);
		void		clean();

		void		graphics_acquire(VkCommandBuffer graphics_command_buffer);
		void		graphics_release(VkCommandBuffer graphics_command_buffer);
		VkResult	present_acquire();
		VkResult	present_swap_buffers();
		VkResult	wait_next_image();
		void		back_buffer_info(vk_back_buffer_info_t& info);
		void		get_view(render_target_view_t*& view, size_t index);
		size_t		back_buffer_size() { return _back_buffer_size; }
	};

#endif

} }

