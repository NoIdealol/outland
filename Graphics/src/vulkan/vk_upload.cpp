#include "pch.h"
#include "vk_upload.h"
#include "math\integer.h"

namespace outland { namespace gpu 
{

	//--------------------------------------------------
	vk_upload_t::vk_upload_t(vk_device_t* device)
		: _device(device)
		, _cmd_pool_copy(VK_NULL_HANDLE)
		, _cmd_pool_graphics(VK_NULL_HANDLE)
		, _memory(VK_NULL_HANDLE)
		, _ptr(nullptr)
		, _buffer(VK_NULL_HANDLE)
		, _submit_size(0)
		, _frame_free_queue(device->device_allocator())
		, _frame_submited_queue(device->device_allocator())
		, _frame_clean_queue(device->device_allocator())
		, _ctx_free_queue(device->device_allocator())
		, _ctx_submited_queue(device->device_allocator())
		, _ctx(nullptr)
	{
		
		for (auto& it : _frame_array)
		{
			it.buffer_size = 0;
			it.image_size = 0;
			it.cmd_buffer = VK_NULL_HANDLE;
			it.fence = VK_NULL_HANDLE;
		}

		for(auto& it : _ctx_array)
		{
			it.buffer_offset = 0;
			it.cmd_buffer = VK_NULL_HANDLE;
			it.frame = nullptr;
		}
		
		_cache.size = 0;
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::init(VkDeviceSize buffer_size)
	{
		VkResult vk_result;

		{
			if (_frame_free_queue.reserve(_copy_frame_size))
				return VK_ERROR_OUT_OF_HOST_MEMORY;
			if (_frame_submited_queue.reserve(_copy_frame_size))
				return VK_ERROR_OUT_OF_HOST_MEMORY;
			if (_frame_clean_queue.reserve(_copy_frame_size))
				return VK_ERROR_OUT_OF_HOST_MEMORY;
			if (_ctx_free_queue.reserve(_copy_ctx_size))
				return VK_ERROR_OUT_OF_HOST_MEMORY;
			if (_ctx_submited_queue.reserve(_copy_ctx_size))
				return VK_ERROR_OUT_OF_HOST_MEMORY;

			for (auto& it : _frame_array)
				_frame_free_queue.push_back(&it);
			for (auto& it : _ctx_array)
				_ctx_free_queue.push_back(&it);
		}

		{
			//pool
			VkCommandPoolCreateInfo cp_ci;
			cp_ci.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			cp_ci.pNext = nullptr;
			cp_ci.queueFamilyIndex = _device->queue_copy()->family_index;
			cp_ci.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

			vk_result = _device->vtable()->vkCreateCommandPool(_device->device(), &cp_ci, _device->allocation_cb(), &_cmd_pool_copy);
			if (vk_result)
			{
				clean();
				return vk_result;
			}

			//ctx...........
			VkCommandBufferAllocateInfo cb_ai;
			cb_ai.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			cb_ai.pNext = nullptr;
			cb_ai.commandPool = _cmd_pool_copy;
			cb_ai.commandBufferCount = static_cast<uint32_t>(_copy_ctx_size);
			cb_ai.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

			VkCommandBuffer cmd_buffer_array[_copy_ctx_size];
			vk_result = _device->vtable()->vkAllocateCommandBuffers(_device->device(), &cb_ai, cmd_buffer_array);
			if (vk_result)
			{
				clean();
				return vk_result;
			}

			for (size_t i = 0; i < _copy_ctx_size; ++i)
			{
				auto& ctx = _ctx_array[i];
				ctx.cmd_buffer = cmd_buffer_array[i];
			}
		}

		{
			//pool
			VkCommandPoolCreateInfo cp_ci;
			cp_ci.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			cp_ci.pNext = nullptr;
			cp_ci.queueFamilyIndex = _device->queue_graphics()->family_index;
			cp_ci.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

			vk_result = _device->vtable()->vkCreateCommandPool(_device->device(), &cp_ci, _device->allocation_cb(), &_cmd_pool_graphics);
			if (vk_result)
			{
				clean();
				return vk_result;
			}

			//ctx...........
			VkCommandBufferAllocateInfo cb_ai;
			cb_ai.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			cb_ai.pNext = nullptr;
			cb_ai.commandPool = _cmd_pool_graphics;
			cb_ai.commandBufferCount = static_cast<uint32_t>(_copy_frame_size);
			cb_ai.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

			VkCommandBuffer cmd_buffer_array[_copy_frame_size];
			vk_result = _device->vtable()->vkAllocateCommandBuffers(_device->device(), &cb_ai, cmd_buffer_array);
			if (vk_result)
			{
				clean();
				return vk_result;
			}

			for (size_t i = 0; i < _copy_frame_size; ++i)
			{
				auto& frame = _frame_array[i];
				frame.cmd_buffer = cmd_buffer_array[i];
			}
		}

		for (auto& it : _frame_array)
		{
			vk_result = _device->fence_create(it.fence, true);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		{
			_submit_size = m::align(buffer_size, _device->properties().limits.nonCoherentAtomSize);
			for (size_t i = 0; i < _copy_ctx_size; ++i)
				_ctx_array[i].buffer_offset = _submit_size * i;

			VkDeviceSize buffer_size = _submit_size * _copy_ctx_size;

			VkBufferCreateInfo b_ci;
			b_ci.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
			b_ci.pNext = nullptr;
			b_ci.flags = 0;
			b_ci.size = buffer_size;
			b_ci.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
			b_ci.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			b_ci.queueFamilyIndexCount = 0;
			b_ci.pQueueFamilyIndices = nullptr;

			vk_result = _device->vtable()->vkCreateBuffer(_device->device(), &b_ci, _device->allocation_cb(), &_buffer);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		{
			VkMemoryRequirements mem_req;
			_device->vtable()->vkGetBufferMemoryRequirements(_device->device(), _buffer, &mem_req);

			uint32_t memory_type;
			if (!util::find_memory_index(_device->memory_properties(),
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				mem_req.memoryTypeBits, memory_type))
			{
				clean();
				return VK_ERROR_OUT_OF_DEVICE_MEMORY;
			}

			VkMemoryAllocateInfo mem_ai;
			mem_ai.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			mem_ai.pNext = nullptr;
			mem_ai.memoryTypeIndex = memory_type;
			mem_ai.allocationSize = mem_req.size;

			vk_result = _device->vtable()->vkAllocateMemory(_device->device(), &mem_ai, _device->allocation_cb(), &_memory);
			if (vk_result)
			{
				clean();
				return vk_result;
			}

		}

		{
			vk_result = _device->vtable()->vkBindBufferMemory(_device->device(), _buffer, _memory, 0);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		{
			vk_result = _device->vtable()->vkMapMemory(_device->device(), _memory, 0, buffer_size, 0, &_ptr);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_upload_t::clean()
	{
		if (_ptr)
		{
			_device->vtable()->vkUnmapMemory(_device->device(), _memory);
			_ptr = nullptr;
		}

		if (_buffer)
		{
			_device->vtable()->vkDestroyBuffer(_device->device(), _buffer, _device->allocation_cb());
			_buffer = VK_NULL_HANDLE;
		}	

		if (_memory)
		{
			_device->vtable()->vkFreeMemory(_device->device(), _memory, _device->allocation_cb());
			_memory = VK_NULL_HANDLE;
		}

		for (auto& it : _frame_array)
		{
			if (it.cmd_buffer)
			{
				_device->vtable()->vkFreeCommandBuffers(_device->device(), _cmd_pool_graphics, 1, &it.cmd_buffer);
				it.cmd_buffer = VK_NULL_HANDLE;
			}
		}

		for (auto& it : _ctx_array)
		{
			if (it.cmd_buffer)
			{
				_device->vtable()->vkFreeCommandBuffers(_device->device(), _cmd_pool_copy, 1, &it.cmd_buffer);
				it.cmd_buffer = VK_NULL_HANDLE;
			}
		}

		for (auto& it : _frame_array)
		{
			if (it.fence)
			{
				_device->fence_destroy(it.fence);
				it.fence = VK_NULL_HANDLE;
			}
		}

		if (_cmd_pool_copy)
		{
			_device->vtable()->vkDestroyCommandPool(_device->device(), _cmd_pool_copy, _device->allocation_cb());
			_cmd_pool_copy = VK_NULL_HANDLE;
		}

		if (_cmd_pool_graphics)
		{
			_device->vtable()->vkDestroyCommandPool(_device->device(), _cmd_pool_graphics, _device->allocation_cb());
			_cmd_pool_graphics = VK_NULL_HANDLE;
		}

		O_ASSERT(_copy_frame_size == _frame_free_queue.size());
		O_ASSERT(_copy_ctx_size == _ctx_free_queue.size());
		_frame_free_queue.clear();
		_ctx_free_queue.clear();
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::update(device_event_t* event_array, size_t event_capacity, size_t& event_size)
	{
		VkResult result;

		event_size = 0;

		//check done frames
		while (_frame_submited_queue.size())
		{
			auto* frame = _frame_submited_queue.front();
			result = _is_done(frame);
			switch (result)
			{
			case VK_SUCCESS:
			{
				_frame_submited_queue.pop_front();
				_frame_clean_queue.push_back(frame);
				break;
			}
			case VK_TIMEOUT:
				goto _frame_submited_timeout;
			default:
				return result;
			}
		}
	_frame_submited_timeout:

		//check clean frames
		while (_frame_clean_queue.size() && event_capacity > event_size)
		{
			auto* frame = _frame_clean_queue.front();
			
			while (frame->buffer_size && event_capacity > event_size)
			{
				--frame->buffer_size;

				auto& ev = event_array[event_size];
				ev.type = device_event_type_buffer_copy;
				ev.buffer.error = err_ok;
				ev.buffer.user_data = frame->buffer_array[frame->buffer_size].user_data;
				ev.buffer.buffer = frame->buffer_array[frame->buffer_size].buffer;

				++event_size;
			}

			while (frame->image_size && event_capacity > event_size)
			{
				--frame->image_size;

				auto& ev = event_array[event_size];
				ev.type = device_event_type_texture_copy;
				ev.texture.error = err_ok;
				ev.texture.user_data = frame->image_array[frame->image_size].user_data;
				ev.texture.texture = frame->image_array[frame->image_size].texture;

				++event_size;
			}

			if (0 == (frame->buffer_size + frame->image_size))
			{
				_frame_clean_queue.pop_front();
				_frame_free_queue.push_back(frame);
			}
		}

		//claim done ctx
		while (_ctx_submited_queue.size())
		{
			auto* ctx = _ctx_submited_queue.front();
			result = _is_done(ctx);
			switch (result)
			{
			case VK_SUCCESS:
			{
				result = _flush(ctx->frame);
				if (result)
					return result;
				_frame_submited_queue.push_back(ctx->frame);
				_ctx_submited_queue.pop_front();
				_ctx_free_queue.push_back(ctx);
				break;
			}
			case VK_TIMEOUT:
				goto _ctx_submited_timeout;
			default:
				return result;
			}
		}
	_ctx_submited_timeout:

		//submit ctx
		if (_ctx && _ctx->frame->buffer_size + _ctx->frame->image_size)
		{
			result = _flush(_ctx);
			if (result)
				return result;
			_ctx_submited_queue.push_back(_ctx);
			_ctx = nullptr;
		}

		//try alloc ctx
		if (nullptr == _ctx && _ctx_free_queue.size() && _frame_free_queue.size())
		{
			_ctx = _ctx_free_queue.front();
			_ctx_free_queue.pop_front();
			_ctx->frame = _frame_free_queue.front();
			_frame_free_queue.pop_front();
			result = _reset(_ctx);
			if (result)
				return result;
		}

		return VK_SUCCESS;
	}	

	//--------------------------------------------------
	VkResult	vk_upload_t::_is_done(frame_t* frame)
	{
		return _device->vtable()->vkWaitForFences(_device->device(), 1, &frame->fence, VK_TRUE, 0);
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::_is_done(ctx_t* ctx)
	{
		return _is_done(ctx->frame);
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::_flush(frame_t* frame)
	{
		VkResult result;

		result = _device->vtable()->vkResetFences(_device->device(), 1, &frame->fence);
		if (result)
			return result;

		VkSubmitInfo submit;
		submit.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submit.pNext = nullptr;
		submit.waitSemaphoreCount = 0;
		submit.pWaitSemaphores = nullptr;
		submit.pWaitDstStageMask = nullptr;
		submit.commandBufferCount = 1;
		submit.pCommandBuffers = &frame->cmd_buffer;
		submit.signalSemaphoreCount = 0;
		submit.pSignalSemaphores = nullptr;

		os::mutex_lock_t queue_lock = { _device->queue_lock() };

		return _device->vtable()->vkQueueSubmit(_device->queue_graphics()->queue, 1, &submit, frame->fence);
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::_flush(ctx_t* ctx)
	{
		VkResult result;

		VkCommandBufferBeginInfo cb_bi;
		cb_bi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		cb_bi.pNext = nullptr;
		cb_bi.pInheritanceInfo = nullptr;
		cb_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		result = _device->cmd_vtable()->vkBeginCommandBuffer(ctx->cmd_buffer, &cb_bi);
		if (result)
			return result;

		result = _device->cmd_vtable()->vkBeginCommandBuffer(ctx->frame->cmd_buffer, &cb_bi);
		if (result)
			return result;

		_cmd(ctx, ctx->frame);

		result = _device->cmd_vtable()->vkEndCommandBuffer(ctx->frame->cmd_buffer);
		if (result)
			return result;

		result = _device->cmd_vtable()->vkEndCommandBuffer(ctx->cmd_buffer);
		if (result)
			return result;

		VkSubmitInfo submit;
		submit.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submit.pNext = nullptr;
		submit.waitSemaphoreCount = 0;
		submit.pWaitSemaphores = nullptr;
		submit.pWaitDstStageMask = nullptr;
		submit.commandBufferCount = 1;
		submit.pCommandBuffers = &ctx->cmd_buffer;
		submit.signalSemaphoreCount = 0;
		submit.pSignalSemaphores = nullptr;

		result = _device->vtable()->vkQueueSubmit(_device->queue_copy()->queue, 1, &submit, ctx->frame->fence);
		if (result)
			return result;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::_reset(ctx_t* ctx)
	{
		VkResult result;

		ctx->frame->buffer_size = 0;
		ctx->frame->image_size = 0;
		_cache.size = 0;
		
		result = _device->vtable()->vkResetFences(_device->device(), 1, &ctx->frame->fence);
		if (result)
			return result;
		result = _device->cmd_vtable()->vkResetCommandBuffer(ctx->frame->cmd_buffer, VkCommandBufferResetFlags(0));
		if (result)
			return result;
		result = _device->cmd_vtable()->vkResetCommandBuffer(ctx->cmd_buffer, VkCommandBufferResetFlags(0));
		if (result)
			return result;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void	vk_upload_t::_cmd(ctx_t* ctx, frame_t* frame)
	{
		VkBufferMemoryBarrier buffer_barrier_array[_frame_batch_size];
		VkImageMemoryBarrier image_barrier_array[_frame_batch_size];

		//image barriers for copy
		for (size_t i = 0; i < frame->image_size; ++i)
		{
			auto& barrier = image_barrier_array[i];
			barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barrier.pNext = nullptr;
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.image = frame->image_array[i].texture->image;
			barrier.subresourceRange = frame->image_array[i].range;
		}

		if (frame->image_size)
		{
			_device->cmd_vtable()->vkCmdPipelineBarrier(
				ctx->cmd_buffer,
				VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				static_cast<uint32_t>(frame->image_size), image_barrier_array
			);
		}

		//do the copy
		for (size_t i = 0; i < frame->image_size; ++i)
		{
			_device->cmd_vtable()->vkCmdCopyBufferToImage(
				ctx->cmd_buffer,
				_buffer,
				frame->image_array[i].texture->image,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				frame->image_array[i].range.levelCount,
				&_cache.image_array[i * _max_mip_size]);
		}

		for (size_t i = 0; i < frame->buffer_size; ++i)
		{
			_device->cmd_vtable()->vkCmdCopyBuffer(
				ctx->cmd_buffer,
				_buffer,
				frame->buffer_array[i].buffer->buffer,
				1,
				&_cache.buffer_array[i]);
		}

		//transfer barriers
		for (size_t i = 0; i < frame->image_size; ++i)
		{
			auto& barrier = image_barrier_array[i];
			barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barrier.pNext = nullptr;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = 0;
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			barrier.srcQueueFamilyIndex = _device->queue_copy()->family_index;
			barrier.dstQueueFamilyIndex = _device->queue_graphics()->family_index;
			barrier.image = frame->image_array[i].texture->image;
			barrier.subresourceRange = frame->image_array[i].range;
		}

		for (size_t i = 0; i < frame->buffer_size; ++i)
		{
			auto& barrier = buffer_barrier_array[i];
			barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
			barrier.pNext = nullptr;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = 0;
			barrier.srcQueueFamilyIndex = _device->queue_copy()->family_index;
			barrier.dstQueueFamilyIndex = _device->queue_graphics()->family_index;
			barrier.buffer = frame->buffer_array[i].buffer->buffer;
			barrier.offset = frame->buffer_array[i].buffer->offset;
			barrier.size = frame->buffer_array[i].buffer->size;
		}

		_device->cmd_vtable()->vkCmdPipelineBarrier(
			ctx->cmd_buffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
			0,
			0, nullptr,
			static_cast<uint32_t>(frame->buffer_size), buffer_barrier_array,
			static_cast<uint32_t>(frame->image_size), image_barrier_array
		);

		//accept barriers
		VkPipelineStageFlags dst_stage = 0;
		for (size_t i = 0; i < frame->image_size; ++i)
		{
			auto& barrier = image_barrier_array[i];
			barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barrier.pNext = nullptr;
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			barrier.srcQueueFamilyIndex = _device->queue_copy()->family_index;
			barrier.dstQueueFamilyIndex = _device->queue_graphics()->family_index;
			barrier.image = frame->image_array[i].texture->image;
			barrier.subresourceRange = frame->image_array[i].range;

			dst_stage |= frame->image_array[i].texture->stage;
		}

		for (size_t i = 0; i < frame->buffer_size; ++i)
		{
			auto& barrier = buffer_barrier_array[i];
			barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
			barrier.pNext = nullptr;
			barrier.srcAccessMask = 0;
			//barrier.dstAccessMask = 0;
			barrier.srcQueueFamilyIndex = _device->queue_copy()->family_index;
			barrier.dstQueueFamilyIndex = _device->queue_graphics()->family_index;
			barrier.buffer = frame->buffer_array[i].buffer->buffer;
			barrier.offset = frame->buffer_array[i].buffer->offset;
			barrier.size = frame->buffer_array[i].buffer->size;

			switch ((VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT) & frame->buffer_array[i].buffer->usage)
			{
			case VK_BUFFER_USAGE_VERTEX_BUFFER_BIT:
				barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
				break;
			case VK_BUFFER_USAGE_INDEX_BUFFER_BIT:
				barrier.dstAccessMask = VK_ACCESS_INDEX_READ_BIT;
				break;
			case VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT:
				barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
				break;
			default:
				O_ASSERT(false);
				barrier.dstAccessMask = 0;
				break;
			}

			dst_stage |= frame->buffer_array[i].buffer->stage;
		}

		_device->cmd_vtable()->vkCmdPipelineBarrier(
			frame->cmd_buffer,
			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
			dst_stage,
			0,
			0, nullptr,
			static_cast<uint32_t>(frame->buffer_size), buffer_barrier_array,
			static_cast<uint32_t>(frame->image_size), image_barrier_array
		);
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::buffer_copy(host_to_buffer_copy_t const& copy)
	{
		if (nullptr == _ctx || _frame_batch_size == _ctx->frame->buffer_size)
			return VK_ERROR_OUT_OF_DEVICE_MEMORY;

		VkDeviceSize const buffer_start = _ctx->buffer_offset + _cache.size;
		VkResult vk_result = _buffer_copy(
			copy, 
			_cache.buffer_array + _ctx->frame->buffer_size, 
			_ctx->frame->buffer_array[_ctx->frame->buffer_size],
			_cache.size, 
			buffer_start);

		if (vk_result)
			return vk_result;

		if (nullptr != copy.src_data)
			++_ctx->frame->buffer_size;

		return VK_SUCCESS;

	}

	//--------------------------------------------------
	VkResult	vk_upload_t::texture_copy(host_to_texture_copy_t const& copy)
	{
		if (nullptr == _ctx || _frame_batch_size == _ctx->frame->image_size)
			return VK_ERROR_OUT_OF_DEVICE_MEMORY;

		VkDeviceSize const buffer_start = _ctx->buffer_offset + _cache.size;
		VkResult vk_result = _texture_copy(
			copy, 
			_cache.image_array + _ctx->frame->image_size * _max_mip_size, 
			_ctx->frame->image_array[_ctx->frame->image_size],
			_cache.size, 
			buffer_start);

		if (vk_result)
			return vk_result;

		if (nullptr != copy.mip_data_array)
			++_ctx->frame->image_size;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::_texture_copy(
		host_to_texture_copy_t const& host_copy, 
		VkBufferImageCopy* copy_array, 
		image_info_t& info, 
		VkDeviceSize& current_size, 
		VkDeviceSize buffer_start) const
	{
		auto const& limits = _device->properties().limits;

		uint32_t texel_block_byte_size;
		uint32_t texel_block_pixel_size;
		if (!util::get_format_texel_info(host_copy.dst_texture->format, texel_block_byte_size, texel_block_pixel_size))
			return VK_ERROR_FORMAT_NOT_SUPPORTED;

		uint32_t pixel_align = 1;
		if (limits.optimalBufferCopyRowPitchAlignment > texel_block_byte_size)
		{
			if (0 == (limits.optimalBufferCopyRowPitchAlignment % texel_block_byte_size))
			{
				pixel_align = uint32_t(limits.optimalBufferCopyRowPitchAlignment / texel_block_byte_size) * texel_block_pixel_size;
			}
		}

		VkDeviceSize buffer_offset = buffer_start;
		struct data_info_t
		{
			uint32_t mip_width_aligned;
			uint32_t mip_row_pitch_byte_size;
			uint32_t mip_row_byte_size;
			uint32_t slice_size;
		};
		data_info_t data_info_array[_max_mip_size];

		for (uint32_t mip = 0; mip < host_copy.mip_size; ++mip)
		{
			auto& copy = copy_array[mip];
			auto& data_info = data_info_array[mip];

			VkExtent3D mip_extent = host_copy.dst_texture->extent;
			mip_extent.width >>= mip + host_copy.mip_level;
			if (VK_IMAGE_TYPE_2D == host_copy.dst_texture->image_type || VK_IMAGE_TYPE_3D == host_copy.dst_texture->image_type)
				mip_extent.height >>= mip + host_copy.mip_level;
			if (VK_IMAGE_TYPE_3D == host_copy.dst_texture->image_type)
				mip_extent.depth >>= mip + host_copy.mip_level;

			uint32_t mip_width_aligned = m::align(mip_extent.width, pixel_align);
			uint32_t mip_row_pitch_byte_size = (mip_width_aligned / texel_block_pixel_size) * texel_block_byte_size;
			uint32_t mip_row_byte_size = (mip_extent.width / texel_block_pixel_size) * texel_block_byte_size;
			uint32_t slice_size = mip_extent.depth * host_copy.array_size; //one of them must be 1

			data_info.mip_width_aligned = mip_width_aligned;
			data_info.mip_row_pitch_byte_size = mip_row_pitch_byte_size;
			data_info.mip_row_byte_size = mip_row_byte_size;
			data_info.slice_size = slice_size;

			VkDeviceSize mip_total_size = slice_size * (mip_extent.height / texel_block_pixel_size) * mip_row_pitch_byte_size;
			buffer_offset = m::align(buffer_offset, limits.optimalBufferCopyOffsetAlignment);
			buffer_offset = m::align(buffer_offset, VkDeviceSize(texel_block_byte_size)); //ensure alignmnent, probably not needed
			buffer_offset = m::align(buffer_offset, VkDeviceSize(4)); //must be 4 aligned

			copy.bufferOffset = buffer_offset;
			copy.bufferRowLength = mip_width_aligned;
			copy.bufferImageHeight = mip_extent.height;
			copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			copy.imageSubresource.mipLevel = mip + host_copy.mip_level;
			copy.imageSubresource.baseArrayLayer = host_copy.array_level;
			copy.imageSubresource.layerCount = host_copy.array_size;
			copy.imageOffset.x = 0;
			copy.imageOffset.y = 0;
			copy.imageOffset.z = 0;
			copy.imageExtent = mip_extent;

			buffer_offset += mip_total_size;
		}

		VkDeviceSize total_size = buffer_offset - buffer_start;
		
		if (_submit_size < (current_size + total_size))
			return VK_ERROR_OUT_OF_DEVICE_MEMORY;

		if (nullptr == host_copy.mip_data_array)
			return VK_SUCCESS;

		//copy data
		for (uint32_t mip = 0; mip < host_copy.mip_size; ++mip)
		{
			auto const& mip_data = host_copy.mip_data_array[mip];
			auto& copy = copy_array[mip];
			auto& data_info = data_info_array[mip];

			byte_t const* ptr_src = reinterpret_cast<byte_t const*>(mip_data.src_data);
			byte_t* ptr_dst = reinterpret_cast<byte_t*>(_ptr) + copy.bufferOffset;

			for (uint32_t s = 0; s < data_info.slice_size; ++s)
			{
				byte_t const* ptr_src_row = ptr_src;
				for (uint32_t h = 0; h < copy.imageExtent.height; h += texel_block_pixel_size)
				{
					u::mem_copy(
						ptr_dst,
						ptr_src_row,
						data_info.mip_row_byte_size);

					ptr_src_row += mip_data.byte_row_pitch;
					ptr_dst += data_info.mip_row_pitch_byte_size;
				}

				ptr_src += mip_data.byte_slice_pitch;
			}
		}

		current_size += total_size;

		info.texture = host_copy.dst_texture;
		info.range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		info.range.baseArrayLayer = host_copy.array_level;
		info.range.layerCount = host_copy.array_size;
		info.range.baseMipLevel = host_copy.mip_level;
		info.range.levelCount = host_copy.mip_size;
		info.user_data = host_copy.user_data;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_upload_t::_buffer_copy(
		host_to_buffer_copy_t const& host_copy, 
		VkBufferCopy* copy_array, 
		buffer_info_t& info, 
		VkDeviceSize& current_size, 
		VkDeviceSize buffer_start) const
	{
		auto const& limits = _device->properties().limits;

		VkDeviceSize buffer_offset = buffer_start;
		buffer_offset = m::align(buffer_offset, VkDeviceSize(_buffer_copy_align));

		auto& copy = *copy_array;
		copy.dstOffset = host_copy.dst_buffer->offset;
		copy.size = host_copy.dst_buffer->size;
		copy.srcOffset = buffer_offset;

		buffer_offset += host_copy.dst_buffer->size;
		VkDeviceSize total_size = buffer_offset - buffer_start;

		if (_submit_size < (current_size + total_size))
			return VK_ERROR_OUT_OF_DEVICE_MEMORY;

		if (nullptr == host_copy.src_data)
			return VK_SUCCESS;

		byte_t* ptr_dst = reinterpret_cast<byte_t*>(_ptr) + copy.srcOffset;

		u::mem_copy(
			ptr_dst,
			host_copy.src_data,
			static_cast<size_t>(host_copy.dst_buffer->size));

		current_size += total_size;

		info.buffer = host_copy.dst_buffer;
		info.user_data = host_copy.user_data;

		return VK_SUCCESS;
	}

} }