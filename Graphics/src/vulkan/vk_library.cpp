#include "pch.h"
#include "vk_library.h"

#define VK_LIBRARY_LOAD(fnc) \
	if (nullptr == (_vtable.fnc = reinterpret_cast<PFN_##fnc>(_vtable.vkGetInstanceProcAddr(NULL, #fnc)))) \
	{ \
		os::library::free(_library); \
		return VK_ERROR_INITIALIZATION_FAILED; \
	}

namespace outland { namespace gpu
{

	VkResult	vk_library_t::load()
	{
		error_t err;
		err = os::library::load(_library, "vulkan-1.dll");
		if (err)
			return VK_ERROR_INITIALIZATION_FAILED;

		os::library::function_t vulkan_vkGetInstanceProcAddr;
		vulkan_vkGetInstanceProcAddr = os::library::find(_library, "vkGetInstanceProcAddr");
		if (nullptr == vulkan_vkGetInstanceProcAddr)
		{
			err = os::library::free(_library);
			O_ASSERT(err_ok == err);
			return VK_ERROR_INITIALIZATION_FAILED;
		}

		_vtable.vkGetInstanceProcAddr = reinterpret_cast<PFN_vkGetInstanceProcAddr>(vulkan_vkGetInstanceProcAddr);

		VK_FNC_LIBRARY(VK_LIBRARY_LOAD);

		return VK_SUCCESS;
	}

	void		vk_library_t::free()
	{
		error_t err;
		err = os::library::free(_library);
		O_ASSERT(err_ok == err);
	}

	VkResult vk_library_t::check_instance_extensions(char8_t const* const* extension_array, size_t extension_size, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock = { scratch };

		VkResult vk_result;
		VkExtensionProperties* extension_properties_array = nullptr;
		uint32_t extension_properties_size = 0;

		vk_result = _vtable.vkEnumerateInstanceExtensionProperties(nullptr, &extension_properties_size, extension_properties_array);
		if (vk_result)
			return vk_result;

		error_t err = u::mem_alloc<VkExtensionProperties>(scratch, extension_properties_array, extension_properties_size);
		if (err)
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		vk_result = _vtable.vkEnumerateInstanceExtensionProperties(nullptr, &extension_properties_size, extension_properties_array);
		if (vk_result != VK_SUCCESS && vk_result != VK_INCOMPLETE)
			return vk_result;

		for (size_t i = 0; i < extension_size; ++i)
		{
			bool is_found = false;
			for (uint32_t j = 0; j < extension_properties_size; ++j)
			{
				if (u::str_cmp(extension_properties_array[j].extensionName, extension_array[i]))
				{
					is_found = true;
					break;
				}
			}

			if (!is_found)
				return VK_ERROR_EXTENSION_NOT_PRESENT;
		}

		return VK_SUCCESS;
	}

	VkResult	vk_library_t::check_instance_layers(char8_t const* const* layer_array, size_t layer_size, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock = { scratch };

		VkResult vk_result;
		VkLayerProperties* layer_properties_array = nullptr;
		uint32_t layer_properties_size = 0;

		vk_result = _vtable.vkEnumerateInstanceLayerProperties(&layer_properties_size, layer_properties_array);
		if (vk_result)
			return vk_result;

		error_t err = u::mem_alloc(scratch, layer_properties_array, layer_properties_size);
		if (err)
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		vk_result = _vtable.vkEnumerateInstanceLayerProperties(&layer_properties_size, layer_properties_array);
		if (vk_result != VK_SUCCESS && vk_result != VK_INCOMPLETE)
			return vk_result;

		for (size_t i = 0; i < layer_size; ++i)
		{
			bool is_found = false;
			for (uint32_t j = 0; j < layer_properties_size; ++j)
			{
				if (u::str_cmp(layer_properties_array[j].layerName, layer_array[i]))
				{
					is_found = true;
					break;
				}
			}

			if (!is_found)
				return VK_ERROR_EXTENSION_NOT_PRESENT;
		}

		return VK_SUCCESS;
	}

} }
