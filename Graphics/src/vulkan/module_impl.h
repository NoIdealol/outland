#pragma once
#include "module.h"
//#include "library_vk.h"
#include "vk_instance.h"

namespace outland { namespace gpu 
{

	class module_t
	{
		vk_library_t			_library;
		vk_instance_t			_instance;
		allocator_t*			_module_allocator;
		allocator_t*			_object_allocator;
		//VkAllocationCallbacks	_allocation_cb;

	private:
		
		
	public:
		module_t(allocator_t* module_allocator, allocator_t* object_allocator);

		friend error_t	module_vulkan_create(module_t*& module, module_create_info_t const& info, scratch_allocator_t* scratch);
		friend void		module_vulkan_destroy(module_t* module);

		friend error_t	module::adapter_enumerate(module_id module, adapter_id* adapter_array, size_t adapter_capacity, size_t& adapter_size, scratch_allocator_t* scratch);
		friend void		module::adapter_get_info(module_id module, adapter_id adapter, adapter_info_t& info);
		friend void		module::adapter_get_caps(module_id module, adapter_id adapter, adapter_caps_t& caps);
		friend void		module::adapter_get_limits(module_id module, adapter_id adapter, adapter_limits_t& limits);
		friend error_t	module::device_create(module_id module, device_id& device, cmd_queue_id& cmd_queue, device_create_t const& create, scratch_allocator_t* scratch);
		friend void		module::device_destroy(module_id module, device_id device, cmd_queue_id cmd_queue);
		friend error_t	module::surface_create(module_id module, surface_id& surface, surface_create_info_t const& info, scratch_allocator_t* scratch);
		friend void		module::surface_destroy(module_id module, surface_id surface);
	};



} }