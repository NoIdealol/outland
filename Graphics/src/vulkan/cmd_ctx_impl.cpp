#include "pch.h"
#include "cmd_ctx_impl.h"
#include "math\integer.h"

namespace outland { namespace gpu 
{

	//--------------------------------------------------
	error_t vk_frame_queue_t::init()
	{
		memory_info_t queue_mem;
		triple_buffer_queue::memory_info(queue_mem);

		size_t align_arr[] = { queue_mem.align , queue_mem.align };
		size_t size_arr[] = { queue_mem.size, queue_mem.size };
		size_t offset_arr[u::array_size(size_arr)];

		size_t max_align;
		size_t total_size;
		u::mem_offsets(align_arr, size_arr, offset_arr, max_align, total_size, u::array_size(offset_arr));

		void* memory = _allocator->alloc(total_size, max_align);
		if (nullptr == memory)
			return err_out_of_memory;

		triple_buffer_queue::create_info_t info;
		info.initial_producer_index = 0;
		info.initial_consumer_index = 1;
		info.initial_unused_index = 2;

		triple_buffer_queue::create(_render_queue, info, reinterpret_cast<byte_t*>(memory) + offset_arr[0]);
		triple_buffer_queue::create(_resource_queue, info, reinterpret_cast<byte_t*>(memory) + offset_arr[1]);

		return err_ok;
	}

	//--------------------------------------------------
	void	vk_frame_queue_t::clean()
	{
		triple_buffer_queue::destroy(_render_queue);
		triple_buffer_queue::destroy(_resource_queue);

		memory_info_t queue_mem;
		triple_buffer_queue::memory_info(queue_mem);

		size_t align_arr[] = { queue_mem.align , queue_mem.align };
		size_t size_arr[] = { queue_mem.size, queue_mem.size };
		size_t offset_arr[u::array_size(size_arr)];

		size_t max_align;
		size_t total_size;
		u::mem_offsets(align_arr, size_arr, offset_arr, max_align, total_size, u::array_size(offset_arr));

		_allocator->free(_render_queue, total_size);
	}

	//--------------------------------------------------
	vk_frame_queue_t::vk_frame_queue_t(allocator_t* allocator)
		: _resource_msg(_resource_msg_array)
		, _render_msg(_render_msg_array)
		, _allocator(allocator)
	{
		u::mem_clr(_resource_msg_array, sizeof(_resource_msg_array));
		u::mem_clr(_render_msg_array, sizeof(_render_msg_array));
	}

	//--------------------------------------------------
	bool vk_frame_queue_t::render_recv_msg(vk_resource_frame_t& frame)
	{
		bool result;
		size_t index;
		result = triple_buffer_queue::consume(_resource_queue, index);
		frame = _resource_msg_array[index];

		return result;
	}

	//--------------------------------------------------
	void vk_frame_queue_t::render_send_msg(vk_render_frame_t const& frame)
	{
		*_render_msg = frame;

		size_t index;
		triple_buffer_queue::produce(_render_queue, index);
		_render_msg = _render_msg_array + index;
	}

	//--------------------------------------------------
	bool vk_frame_queue_t::resource_recv_msg(vk_render_frame_t& frame)
	{
		bool result;
		size_t index;
		result = triple_buffer_queue::consume(_render_queue, index);
		frame = _render_msg_array[index];

		return result;
	}

	//--------------------------------------------------
	void vk_frame_queue_t::resource_send_msg(vk_resource_frame_t const& frame)
	{
		*_resource_msg = frame;

		size_t index;
		triple_buffer_queue::produce(_resource_queue, index);
		_resource_msg = _resource_msg_array + index;
	}

	//--------------------------------------------------
	vk_cmd_heap_t::vk_cmd_heap_t(vk_device_t* device)
		: _device(device)
		, _memory(VK_NULL_HANDLE)
		, _buffer(VK_NULL_HANDLE)
		, _size(0)
		, _page(0)
		, _ptr(nullptr)
		//, _pool(VK_NULL_HANDLE)
		, _head(nullptr)
		, _atomic_storage(nullptr)
		, _index(1)
	{
		//for (auto& it : _set_array)
			//it = VK_NULL_HANDLE;
		memory_info_t mem_info;
		os::atomic::memory_info_uint32(mem_info);
		O_ASSERT(mem_info.align <= alignof(decltype(_atomic_storage)));
		O_ASSERT(mem_info.size <= sizeof(vk_cmd_heap_t::_atomic_storage));

		os::atomic::create(_head, 0, &_atomic_storage);
	}

	//--------------------------------------------------
	vk_cmd_heap_t::~vk_cmd_heap_t()
	{
		os::atomic::destroy(_head);
	}

	//--------------------------------------------------
	VkResult		vk_cmd_heap_t::init(size_t heap_size, VkDeviceSize max_transform_size)
	{
		VkResult vk_result;

		/*{
			VkDescriptorSetLayoutBinding vk_binding_array[max_transform_size];

			VkDescriptorSetLayoutCreateInfo vk_info;
			vk_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
			vk_info.pNext = nullptr;
			vk_info.flags = 0;
			vk_info.bindingCount = static_cast<uint32_t>(info.member_size);
			vk_info.pBindings = vk_binding_array;

			VkDescriptorSetLayout vk_layout;
			vk_result = _device->vtable()->vkCreateDescriptorSetLayout(_device->device(), &vk_info, _device->allocation_cb(), &vk_layout);
			if (vk_result)
				return vk_result;
		}

		{
			VkDescriptorPoolSize pool_size_array[1];
			pool_size_array[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
			pool_size_array[0].descriptorCount = (max_transform_size * (max_transform_size + 1)) >> 1;

			VkDescriptorPoolCreateInfo vk_info;
			vk_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			vk_info.pNext = nullptr;
			vk_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
			vk_info.maxSets = max_transform_size;
			vk_info.poolSizeCount = static_cast<uint32_t>(u::array_size(pool_size_array));
			vk_info.pPoolSizes = pool_size_array;

			vk_result = _device->vtable()->vkCreateDescriptorPool(_device->device(), &vk_info, _device->allocation_cb(), &_pool);
			if (vk_result)
				return vk_result;
		}*/

		{
			VkDeviceSize vk_heap_size = static_cast<VkDeviceSize>(heap_size);
			VkDeviceSize vk_page_size = 1 << c::cmd_heap_page_power;
			vk_page_size = m::align(vk_page_size, _device->properties().limits.minUniformBufferOffsetAlignment);
			VkDeviceSize vk_page_count = m::to_pow2(m::max(vk_heap_size >> c::cmd_heap_page_power, VkDeviceSize(c::cmd_heap_page_min_count)));
			vk_heap_size = vk_page_size * vk_page_count + max_transform_size;
			
			_size = vk_heap_size;
			_page = vk_page_size;
			_page_mask = static_cast<uint32_t>(vk_page_count) - 1;

			VkBufferCreateInfo vk_info;
			vk_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
			vk_info.pNext = nullptr;
			vk_info.flags = 0;
			vk_info.size = _size;
			vk_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
			vk_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			vk_info.queueFamilyIndexCount = 0;
			vk_info.pQueueFamilyIndices = nullptr;

			vk_result = _device->vtable()->vkCreateBuffer(_device->device(), &vk_info, _device->allocation_cb(), &_buffer);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		{
			VkMemoryRequirements mem_req;
			_device->vtable()->vkGetBufferMemoryRequirements(_device->device(), _buffer, &mem_req);

			uint32_t memory_type;
			if (!util::find_memory_index(_device->memory_properties(),
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				mem_req.memoryTypeBits, memory_type))
			{
				clean();
				return VK_ERROR_OUT_OF_DEVICE_MEMORY;
			}

			VkMemoryAllocateInfo vk_info;
			vk_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			vk_info.pNext = nullptr;
			vk_info.memoryTypeIndex = memory_type;
			vk_info.allocationSize = mem_req.size;

			vk_result = _device->vtable()->vkAllocateMemory(_device->device(), &vk_info, _device->allocation_cb(), &_memory);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		{
			vk_result = _device->vtable()->vkBindBufferMemory(_device->device(), _buffer, _memory, 0);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		{
			vk_result = _device->vtable()->vkMapMemory(_device->device(), _memory, 0, _size, 0, &_ptr);
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		os::atomic::write(_head, 0, os::memory::order_relaxed);

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void			vk_cmd_heap_t::clean()
	{
		if (nullptr != _ptr)
		{
			_device->vtable()->vkUnmapMemory(_device->device(), _memory);
			_ptr = nullptr;
		}

		if (VK_NULL_HANDLE != _buffer)
		{
			_device->vtable()->vkDestroyBuffer(_device->device(), _buffer, _device->allocation_cb());
			_buffer = VK_NULL_HANDLE;
		}

		if (VK_NULL_HANDLE != _memory)
		{
			_device->vtable()->vkFreeMemory(_device->device(), _memory, _device->allocation_cb());
			_memory = VK_NULL_HANDLE;
		}		
	}

	//--------------------------------------------------
	void			vk_cmd_heap_t::alloc(VkDeviceSize& begin, VkDeviceSize& end)
	{
		uint32_t page_index = os::atomic::increment(_head, 1, os::memory::order_relaxed);
		begin = _page * (page_index & _page_mask);
		end = begin + _page;
	}

	//--------------------------------------------------
	cmd_allocator_t::cmd_allocator_t(vk_cmd_heap_t* heap, allocator_t* allocator)
		: _heap(heap)	
		, _begin(0)
		, _end(0)
		, _head(0)
		, _ptr(heap->ptr())
		, _align(heap->device()->properties().limits.minUniformBufferOffsetAlignment)
		, _allocator(allocator)
	{
		for (auto& it : _pool_array)
			it = VK_NULL_HANDLE;
	}

	//--------------------------------------------------
	VkResult		cmd_allocator_t::init()
	{
		VkResult vk_result;
		auto* device = _heap->device();

		for (auto& pool : _pool_array)
		{
			VkCommandPoolCreateInfo vk_info;
			vk_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			vk_info.pNext = nullptr;
			vk_info.queueFamilyIndex = device->queue_graphics()->family_index;
			vk_info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;// | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

			vk_result = device->vtable()->vkCreateCommandPool(device->device(), &vk_info, device->allocation_cb(), &pool);
			if (vk_result)
			{
				pool = VK_NULL_HANDLE;
				clean();
				return vk_result;
			}
		}
		
		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void			cmd_allocator_t::clean()
	{
		auto* device = _heap->device();
		for (auto& pool : _pool_array)
		{
			if (VK_NULL_HANDLE != pool)
			{
				device->vtable()->vkDestroyCommandPool(device->device(), pool, device->allocation_cb());
				pool = VK_NULL_HANDLE;
			}
		}
	}

	//--------------------------------------------------
	void			cmd_allocator_t::alloc(uint32_t size, void*& ptr, uint32_t& dyn_offset)
	{
		VkDeviceSize vk_size = static_cast<VkDeviceSize>(size);
		_head = m::align(_head, _align);
		if ((_head + vk_size) > _end)
		{
			_heap->alloc(_begin, _end);
			_head = _begin;
			O_ASSERT(m::align(_head, _align) == _head);
		}

		dyn_offset = static_cast<uint32_t>(_head);
		ptr = reinterpret_cast<byte_t*>(_ptr) + dyn_offset;
		_head += vk_size;
	}

	//--------------------------------------------------
	VkResult		cmd_allocator_t::reset(size_t index)
	{
		auto* device = _heap->device();
		return device->cmd_vtable()->vkResetCommandPool(device->device(), _pool_array[index], 0);
	}

	//--------------------------------------------------
	cmd_ctx_t::cmd_ctx_t(cmd_allocator_t* allocator)
		: _vtable(allocator->heap()->device()->cmd_vtable())
		, _allocator(allocator)
		, _cmd_buffer(VK_NULL_HANDLE)
		, _pipeline(nullptr)
		, _layout(nullptr)
	{
		for (auto& cmd_buffer : _cmd_buffer_array)
		{
			cmd_buffer = VK_NULL_HANDLE;
		}
		//allocator->heap()->set_array(_set_array);
		u::mem_clr(_offset_array, sizeof(_offset_array));
	}

	//--------------------------------------------------
	VkResult	cmd_ctx_t::init(VkCommandBufferLevel level)
	{
		VkResult vk_result;
		auto* device = _allocator->heap()->device();

		for (size_t i = 0; i < u::array_size(_cmd_buffer_array); ++i)
		{
			VkCommandBufferAllocateInfo vk_info;
			vk_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			vk_info.pNext = nullptr;
			vk_info.commandPool = _allocator->pool(i);
			vk_info.level = level;// VK_COMMAND_BUFFER_LEVEL_SECONDARY;
			vk_info.commandBufferCount = 1;

			vk_result = device->vtable()->vkAllocateCommandBuffers(device->device(), &vk_info, &_cmd_buffer_array[i]);
			if (vk_result)
			{
				_cmd_buffer_array[i] = VK_NULL_HANDLE;
				clean();
				return vk_result;
			}
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		cmd_ctx_t::clean()
	{
		auto* device = _allocator->heap()->device();

		for (size_t i = 0; i < u::array_size(_cmd_buffer_array); ++i)
		{
			if (VK_NULL_HANDLE != _cmd_buffer_array[i])
			{
				device->vtable()->vkFreeCommandBuffers(device->device(), _allocator->pool(i), 1, &_cmd_buffer_array[i]);
				_cmd_buffer_array[i] = VK_NULL_HANDLE;
			}
		}
	}

	//--------------------------------------------------
	void	cmd::bind_pipeline_layout(cmd_ctx_id cmd_ctx, pipeline_layout_id pipeline_layout)
	{
		cmd_ctx->_layout = pipeline_layout;
	}

	//--------------------------------------------------
	void	cmd::bind_pipeline(cmd_ctx_id cmd_ctx, pipeline_id pipeline)
	{
		cmd_ctx->_pipeline = pipeline;
		cmd_ctx->_vtable->vkCmdBindPipeline(cmd_ctx->_cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->pipeline);
	}
	
	//--------------------------------------------------
	void	cmd::draw(cmd_ctx_id cmd_ctx, uint32_t vertex_size, uint32_t vertex_first, uint32_t instance_size, uint32_t instance_first)
	{
		if (cmd_ctx->_layout->transform_size)
		{
			cmd_ctx->_vtable->vkCmdBindDescriptorSets(
				cmd_ctx->_cmd_buffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				cmd_ctx->_layout->layout,
				cmd_ctx->_layout->descriptor_set_size,
				1,
				&cmd_ctx->_layout->transform_set,
				cmd_ctx->_layout->transform_size,
				cmd_ctx->_offset_array);
		}

		cmd_ctx->_vtable->vkCmdDraw(cmd_ctx->_cmd_buffer, vertex_size, instance_size, vertex_first, instance_first);
	}

	//--------------------------------------------------
	void	cmd::draw_indexed(cmd_ctx_id cmd_ctx, uint32_t index_size, uint32_t index_first, uint32_t instance_size, uint32_t instance_first, uint32_t vertex_offset)
	{
		if (cmd_ctx->_layout->transform_size)
		{
			cmd_ctx->_vtable->vkCmdBindDescriptorSets(
				cmd_ctx->_cmd_buffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				cmd_ctx->_layout->layout,
				cmd_ctx->_layout->descriptor_set_size,
				1,
				&cmd_ctx->_layout->transform_set,
				cmd_ctx->_layout->transform_size,
				cmd_ctx->_offset_array);
		}

		cmd_ctx->_vtable->vkCmdDrawIndexed(cmd_ctx->_cmd_buffer, index_size, instance_size, index_first, vertex_offset, instance_first);
	}

	//--------------------------------------------------
	void	cmd::bind_vertex(cmd_ctx_id cmd_ctx, buffer_id const* buffer_array, uint32_t buffer_size, uint32_t slot_first)
	{
		O_ASSERT(cmd_ctx->_pipeline->vertex_size >= (buffer_size + slot_first));
		
		VkBuffer vk_buffer_array[c::max_vertex_buffer_size];
		VkDeviceSize vk_offset_array[c::max_vertex_buffer_size];

		for (uint32_t i = 0; i < buffer_size; ++i)
		{
			vk_buffer_array[i] = buffer_array[i]->buffer;
			vk_offset_array[i] = buffer_array[i]->offset;
		}

		cmd_ctx->_vtable->vkCmdBindVertexBuffers(cmd_ctx->_cmd_buffer, slot_first, buffer_size, vk_buffer_array, vk_offset_array);
	}

	//--------------------------------------------------
	void	cmd::bind_index(cmd_ctx_id cmd_ctx, buffer_id buffer, index_format_t format)
	{
		VkIndexType vk_index_type;
		switch (format)
		{
		case outland::gpu::index_format_uint16:
			vk_index_type = VK_INDEX_TYPE_UINT16;
			break;
		case outland::gpu::index_format_uint32:
			vk_index_type = VK_INDEX_TYPE_UINT32;
			break;
		default:
			O_ASSERT(false);
			break;
		}

		cmd_ctx->_vtable->vkCmdBindIndexBuffer(cmd_ctx->_cmd_buffer, buffer->buffer, buffer->offset, vk_index_type);
	}

	//--------------------------------------------------
	void	cmd::bind_descriptor(cmd_ctx_id cmd_ctx, descriptor_set_id const* set_array, uint32_t set_size, uint32_t slot_first)
	{
		O_ASSERT(cmd_ctx->_layout->descriptor_set_size >= (set_size + slot_first));
		
		VkDescriptorSet	vk_descriptor_set_array[c::max_descriptor_set_size];
		for (uint32_t i = 0; i < set_size; ++i)
		{
			vk_descriptor_set_array[i] = set_array[i]->set;
		}

		cmd_ctx->_vtable->vkCmdBindDescriptorSets(
			cmd_ctx->_cmd_buffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			cmd_ctx->_layout->layout,
			slot_first,
			set_size,
			vk_descriptor_set_array,
			0,
			nullptr);
	}

	//--------------------------------------------------
	void	cmd::uniform(cmd_ctx_id cmd_ctx, void const* uniform_array, uint32_t uniform_size, uint32_t uniform_offset, uint32_t slot)
	{
		O_ASSERT(cmd_ctx->_layout->uniform_size > slot);
		O_ASSERT(cmd_ctx->_layout->uniform_array[slot].size >= (uniform_offset + uniform_size));

		cmd_ctx->_vtable->vkCmdPushConstants(
			cmd_ctx->_cmd_buffer,
			cmd_ctx->_layout->layout,
			cmd_ctx->_layout->uniform_array[slot].stageFlags,
			cmd_ctx->_layout->uniform_array[slot].offset + uniform_offset, uniform_size, uniform_array);
	}

	//--------------------------------------------------
	void	cmd::transform(cmd_ctx_id cmd_ctx, void*& transform_buffer, uint32_t transform_size, uint32_t slot)
	{
		O_ASSERT(cmd_ctx->_layout->transform_size > slot);

		cmd_ctx->_allocator->alloc(transform_size, transform_buffer, cmd_ctx->_offset_array[slot]);
	}

	//--------------------------------------------------
	error_t	cmd::record_finish(cmd_ctx_id cmd_ctx)
	{
		VkResult vk_result;
		vk_result = cmd_ctx->_vtable->vkEndCommandBuffer(cmd_ctx->_cmd_buffer);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	error_t	cmd::record_deferred(cmd_ctx_id cmd_ctx, render_pass_id pass, uint32_t subpass)
	{
		VkResult vk_result;

		cmd_ctx->_cmd_buffer = cmd_ctx->_cmd_buffer_array[cmd_ctx->_allocator->index()];

		VkCommandBufferInheritanceInfo cb_ii;
		cb_ii.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
		cb_ii.pNext = nullptr;
		cb_ii.renderPass = pass->pass;
		cb_ii.subpass = subpass;
		cb_ii.framebuffer = VK_NULL_HANDLE;
		cb_ii.occlusionQueryEnable = VK_FALSE;
		cb_ii.queryFlags = 0;
		cb_ii.pipelineStatistics = 0;

		VkCommandBufferBeginInfo cb_bi;
		cb_bi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		cb_bi.pNext = nullptr;
		cb_bi.pInheritanceInfo = &cb_ii;
		cb_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT | VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
		
		vk_result = cmd_ctx->_vtable->vkBeginCommandBuffer(cmd_ctx->_cmd_buffer, &cb_bi);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	error_t	cmd::record_primary(cmd_ctx_id cmd_ctx)
	{
		VkResult vk_result;

		cmd_ctx->_cmd_buffer = cmd_ctx->_cmd_buffer_array[cmd_ctx->_allocator->index()];

		VkCommandBufferBeginInfo cb_bi;
		cb_bi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		cb_bi.pNext = nullptr;
		cb_bi.pInheritanceInfo = nullptr;
		cb_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vk_result = cmd_ctx->_vtable->vkBeginCommandBuffer(cmd_ctx->_cmd_buffer, &cb_bi);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void cmd::execute(cmd_ctx_id cmd_ctx, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size)
	{
		VkCommandBuffer cmd_buffer_array[32];
		while (cmd_ctx_size)
		{
			size_t batch_size = m::min(u::array_size(cmd_buffer_array), cmd_ctx_size);
			for (size_t i = 0; i < batch_size; ++i)
			{
				cmd_buffer_array[i] = cmd_ctx_array[i]->_cmd_buffer;
			}

			cmd_ctx->_vtable->vkCmdExecuteCommands(cmd_ctx->_cmd_buffer, static_cast<uint32_t>(batch_size), cmd_buffer_array);

			cmd_ctx_array += batch_size;
			cmd_ctx_size -= batch_size;
		}
	}

	//--------------------------------------------------
	void cmd::render_state(cmd_ctx_id cmd_ctx, render_pass_id pass, render_state_t const& state, cmd_type_t cmd_type)
	{
		VkSubpassContents vk_contents = cmd_type_primary == cmd_type ? VK_SUBPASS_CONTENTS_INLINE : VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS;
		
		VkClearValue vk_clear[c::max_attachment_set_size] = { 0 };
		uint32_t vk_clear_count = 0;

		for (size_t i = 0; i < state.clear_color_size; ++i)
		{
			auto& clear = state.clear_color_array[i];
			//copy
			static_assert(sizeof(VkClearColorValue) == sizeof(clear_color_t::value), "bad data copy");
			u::mem_copy(&vk_clear[clear.index].color, &clear.value, sizeof(VkClearColorValue));

			//count
			vk_clear_count = m::max(vk_clear_count, clear.index + 1);
			O_ASSERT(u::array_size(vk_clear) >= vk_clear_count);
		}

		for (size_t i = 0; i < state.clear_depth_size; ++i)
		{
			auto& clear = state.clear_depth_array[i];
			//copy
			vk_clear[pass->render_target_size + clear.index].depthStencil.depth = clear.depth;
			vk_clear[pass->render_target_size + clear.index].depthStencil.stencil = clear.stencil;

			//count
			vk_clear_count = m::max(vk_clear_count, clear.index + 1);
			O_ASSERT(u::array_size(vk_clear) >= vk_clear_count);
		}

		VkRenderPassBeginInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		vk_info.pNext = nullptr;
		vk_info.renderPass = pass->pass;
		vk_info.framebuffer = state.render_set->framebuffer;
		vk_info.renderArea.offset.x = state.render_offset_x;
		vk_info.renderArea.offset.y = state.render_offset_y;
		vk_info.renderArea.extent.width = state.render_width;
		vk_info.renderArea.extent.height = state.render_height;
		vk_info.clearValueCount = vk_clear_count;
		vk_info.pClearValues = vk_clear;

		cmd_ctx->_vtable->vkCmdBeginRenderPass(cmd_ctx->_cmd_buffer, &vk_info, vk_contents);
	}

	//--------------------------------------------------
	void cmd::render_subpass(cmd_ctx_id cmd_ctx, render_pass_id pass, cmd_type_t cmd_type)
	{
		VkSubpassContents vk_contents = cmd_type_primary == cmd_type ? VK_SUBPASS_CONTENTS_INLINE : VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS;
		cmd_ctx->_vtable->vkCmdNextSubpass(cmd_ctx->_cmd_buffer, vk_contents);
	}

	//--------------------------------------------------
	void cmd::render_finish(cmd_ctx_id cmd_ctx, render_pass_id pass)
	{
		cmd_ctx->_vtable->vkCmdEndRenderPass(cmd_ctx->_cmd_buffer);
	}

	//--------------------------------------------------
	cmd_queue_t::cmd_queue_t(vk_device_t* device, vk_frame_queue_t* frame_queue, vk_cmd_heap_t* cmd_heap, vk_descriptor_manager_t* descriptor_manager)
		: _device(device)
		, _frame_queue(frame_queue)
		, _cmd_heap(cmd_heap)
		, _descriptor_manager(descriptor_manager)
	{
		//_resource_frame.defrag_descriptor_submited = 0;

		_render_frame.defrag_descriptor_finished = 0;
		_render_frame.render_frame_submited = 0;
		_render_frame.render_frame_finished = 0;

		for (auto& it : _frame_fence_array)
			it = VK_NULL_HANDLE;

		for (auto& it_frame : _cmd_allocator_array)
		{
			for (auto& it : it_frame)
				it = nullptr;
		}

		for (auto& it : _cmd_allocator_size)
			it = 0;
	}

	//--------------------------------------------------
	error_t	cmd_queue_t::init()
	{
		VkResult vk_result;

		for (auto& it : _frame_fence_array)
		{
			vk_result = _device->fence_create(it, true);
			if (vk_result)
			{
				it = VK_NULL_HANDLE;
				clean();
				return util::to_error(vk_result);
			}
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	cmd_queue_t::clean()
	{
		for (auto& it : _frame_fence_array)
		{
			if (VK_NULL_HANDLE != it)
			{
				_device->fence_destroy(it);
				it = VK_NULL_HANDLE;
			}
		}
	}

	//--------------------------------------------------
	error_t cmd::update(cmd_queue_id cmd_queue)
	{
		//resource update
		{
			vk_resource_frame_t resource_frame;
			if (cmd_queue->_frame_queue->render_recv_msg(resource_frame))
			{
				if (cmd_queue->_render_frame.defrag_descriptor_finished != resource_frame.defrag_descriptor_submited)
				{
					cmd_queue->_descriptor_manager->hotswap(resource_frame.defrag_descriptor_submited);
					cmd_queue->_render_frame.defrag_descriptor_finished = resource_frame.defrag_descriptor_submited;
				}

				//cmd_queue->_resource_frame = resource_frame;
			}
		}

		//frame update
		{
			VkResult vk_result;

			//wait for next frame
			size_t frame_next = static_cast<size_t>((cmd_queue->_render_frame.render_frame_submited + 1) & c::max_frame_modulo);
			vk_result = cmd_queue->_device->vtable()->vkWaitForFences(cmd_queue->_device->device(), 1, &cmd_queue->_frame_fence_array[frame_next], VK_TRUE, UINT64_MAX);
			if (vk_result)
			{
				return util::to_error(vk_result); //some fatal error?
			}

			//signal finished
			while (cmd_queue->_render_frame.render_frame_finished != cmd_queue->_render_frame.render_frame_submited)
			{
				size_t frame_index = static_cast<size_t>((cmd_queue->_render_frame.render_frame_finished + 1) & c::max_frame_modulo);
				vk_result = cmd_queue->_device->vtable()->vkWaitForFences(cmd_queue->_device->device(), 1, &cmd_queue->_frame_fence_array[frame_index], VK_TRUE, 0);
				if (VK_TIMEOUT == vk_result)
					break;
				if (VK_SUCCESS == vk_result)
				{
					++cmd_queue->_render_frame.render_frame_finished;
				}
				else
				{
					return util::to_error(vk_result); //some fatal error?
				}
			}

			//prepare frame
			vk_result = cmd_queue->_device->vtable()->vkResetFences(cmd_queue->_device->device(), 1, &cmd_queue->_frame_fence_array[frame_next]);
			if (vk_result)
				return util::to_error(vk_result); //some fatal error?

			//reset cmd allocators
			auto& cmd_allocator_array = cmd_queue->_cmd_allocator_array[frame_next];
			auto& cmd_allocator_size = cmd_queue->_cmd_allocator_size[frame_next];
			for (size_t i = 0; i < cmd_allocator_size; ++i)
			{
				auto* cmd_allocator = cmd_allocator_array[i];
				vk_result = cmd_allocator->reset(frame_next);
				if (vk_result)
					return util::to_error(vk_result); //some fatal error?
			}
			cmd_allocator_size = 0;
			cmd_queue->_cmd_heap->frame(frame_next);
		}

	
		//inform about finished work
		cmd_queue->_frame_queue->render_send_msg(cmd_queue->_render_frame);

		return err_ok;
	}

	//--------------------------------------------------
	error_t cmd::submit(cmd_queue_id cmd_queue, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size, swap_chain_id const* present_array, size_t present_size)
	{
		VkResult vk_result;
		size_t frame_next = static_cast<size_t>((cmd_queue->_render_frame.render_frame_submited + 1) & c::max_frame_modulo);

		VkSemaphore wait_semaphore_array[c::max_swap_chain_size];
		VkPipelineStageFlags wait_stage_array[c::max_swap_chain_size];
		VkSemaphore signal_semaphore_array[c::max_swap_chain_size];
		VkCommandBuffer cmd_buffer_array[c::max_cmd_ctx_primary_size];

		//swap chains
		for (size_t i = 0; i < present_size; ++i)
		{
			auto* swap_chain = present_array[i];

			vk_back_buffer_info_t info;
			swap_chain->back_buffer_info(info);

			wait_semaphore_array[i] = info.acquire_semaphore;
			wait_stage_array[i] = info.acquire_stage_mask;
			signal_semaphore_array[i] = info.release_semaphore;
		}

		for (size_t i = 0; i < cmd_ctx_size; ++i)
		{
			cmd_buffer_array[i] = cmd_ctx_array[i]->cmd_buffer();
		}
		

		VkSubmitInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		vk_info.pNext = nullptr;
		vk_info.waitSemaphoreCount = static_cast<uint32_t>(present_size);
		vk_info.pWaitSemaphores = wait_semaphore_array;
		vk_info.pWaitDstStageMask = wait_stage_array;
		vk_info.commandBufferCount = static_cast<uint32_t>(cmd_ctx_size);
		vk_info.pCommandBuffers = cmd_buffer_array;
		vk_info.signalSemaphoreCount = static_cast<uint32_t>(present_size);;
		vk_info.pSignalSemaphores = signal_semaphore_array;

		{
			os::mutex_lock_t queue_lock = { cmd_queue->_device->queue_lock() };
			vk_result = cmd_queue->_device->vtable()->vkQueueSubmit(cmd_queue->_device->queue_graphics()->queue, 1, &vk_info, cmd_queue->_frame_fence_array[frame_next]);
		}

		if (vk_result)
			return util::to_error(vk_result); //some fatal error?

		//build unique used cmd allocator array
		auto& cmd_allocator_array = cmd_queue->_cmd_allocator_array[frame_next];
		auto& cmd_allocator_size = cmd_queue->_cmd_allocator_size[frame_next];
		for (size_t i = 0; i < cmd_ctx_size; ++i)
		{
			auto* cmd_allocator = cmd_ctx_array[i]->cmd_allocator();
			for (size_t j = 0; j < cmd_allocator_size; ++j)
			{
				if (cmd_allocator == cmd_allocator_array[j])
				{
					cmd_allocator = nullptr;
					break;
				}
			}

			if (cmd_allocator)
			{
				cmd_allocator_array[cmd_allocator_size] = cmd_allocator;
				++cmd_allocator_size;
			}
		}



		//inform about finished work
		++cmd_queue->_render_frame.render_frame_submited;
		cmd_queue->_frame_queue->render_send_msg(cmd_queue->_render_frame);

		//present results
		for (size_t i = 0; i < present_size; ++i)
		{
			auto* swap_chain = present_array[i];

			vk_result = swap_chain->present_acquire();
			if (vk_result)
				return util::to_error(vk_result); //some fatal error?

			vk_result = swap_chain->present_swap_buffers();
			if (vk_result)
				return util::to_error(vk_result); //some fatal error?

			vk_result = swap_chain->wait_next_image();
			if (vk_result)
				return util::to_error(vk_result); //some fatal error?
		}

		return err_ok;
	}

	//--------------------------------------------------
	void cmd::back_buffer_acquire(cmd_ctx_id cmd_ctx, swap_chain_id swap_chain, size_t& index)
	{
		vk_back_buffer_info_t info;
		swap_chain->back_buffer_info(info);
		index = info.image_index;

		swap_chain->graphics_acquire(cmd_ctx->cmd_buffer());
	}

	//--------------------------------------------------
	void cmd::back_buffer_release(cmd_ctx_id cmd_ctx, swap_chain_id swap_chain)
	{
		swap_chain->graphics_release(cmd_ctx->cmd_buffer());
	}

	//--------------------------------------------------
	error_t cmd::ctx_alloc(cmd_queue_id cmd_queue, cmd_allocator_id allocator, cmd_ctx_id* cmd_ctx_array, size_t cmd_ctx_size, cmd_type_t cmd_type)
	{
		error_t err;
		VkCommandBufferLevel vk_level = cmd_type_primary == cmd_type ? VK_COMMAND_BUFFER_LEVEL_PRIMARY : VK_COMMAND_BUFFER_LEVEL_SECONDARY;
		for (size_t i = 0; i < cmd_ctx_size; ++i)
		{
			auto& cmd_ctx = cmd_ctx_array[i];
			err = u::mem_alloc(allocator->allocator(), cmd_ctx);
			if (err)
				return err;

			O_CREATE(cmd_ctx_t, cmd_ctx)(allocator);
			VkResult vk_result = cmd_ctx->init(vk_level);
			if (vk_result)
			{
				O_DESTROY(cmd_ctx_t, cmd_ctx);
				u::mem_free(allocator->allocator(), cmd_ctx);
				ctx_free(cmd_queue, allocator, cmd_ctx_array, i);
				return util::to_error(vk_result);
			}
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	cmd::ctx_free(cmd_queue_id cmd_queue, cmd_allocator_id allocator, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size)
	{
		VkResult vk_result;

		//wait for frame
		size_t frame_submited = static_cast<size_t>((cmd_queue->_render_frame.render_frame_submited) & c::max_frame_modulo);
		vk_result = cmd_queue->_device->vtable()->vkWaitForFences(cmd_queue->_device->device(), 1, &cmd_queue->_frame_fence_array[frame_submited], VK_TRUE, UINT64_MAX);
		O_ASSERT(VK_SUCCESS == vk_result);
		
		for (size_t i = 0; i < cmd_ctx_size; ++i)
		{
			auto cmd_ctx = cmd_ctx_array[i];
			cmd_ctx->clean();
			O_DESTROY(cmd_ctx_t, cmd_ctx);
			u::mem_free(allocator->allocator(), cmd_ctx);
		}
	}

} }
