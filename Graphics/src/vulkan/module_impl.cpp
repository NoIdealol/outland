#include "pch.h"
#include "module_impl.h"
#include "surface_impl.h"
#include "device_impl.h"

namespace outland { namespace gpu 
{
	//--------------------------------------------------
	error_t module_vulkan_create(module_t*& module, module_create_info_t const& info, scratch_allocator_t* scratch)
	{
		error_t err;
		err = u::mem_alloc(info.module_allocator, module);
		if (err)
			return err;

		O_CREATE(module_t, module)(info.module_allocator, info.object_allocator);

		VkResult vk_result = module->_library.load();
		if (vk_result)
		{
			O_DESTROY(module_t, module);
			u::mem_free(info.module_allocator, module);
			return util::to_error(vk_result);
		}

		vk_result = module->_instance.init(info.create_mask, scratch);
		if (vk_result)
		{
			module->_library.free();
			O_DESTROY(module_t, module);
			u::mem_free(info.module_allocator, module);
			return util::to_error(vk_result);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	module_vulkan_destroy(module_t* module)
	{
		module->_instance.clean();
		module->_library.free();
		O_DESTROY(module_t, module);
		u::mem_free(module->_module_allocator, module);
	}

	//--------------------------------------------------
	module_t::module_t(allocator_t* module_allocator, allocator_t* object_allocator)
		: _instance(&_library, /*&_allocation_cb*/nullptr)
		, _module_allocator(module_allocator)
		, _object_allocator(object_allocator)
	{
		//optional
		/*_allocation_cb.pfnInternalAllocation = NULL;
		_allocation_cb.pfnInternalFree = NULL;

		_allocation_cb.pUserData = this;
		_allocation_cb.*/
	}

	//--------------------------------------------------
	error_t module::adapter_enumerate(module_id module, adapter_id* adapter_array, size_t adapter_capacity, size_t& adapter_size, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock = { scratch };

		error_t err;
		VkResult vk_result = VK_SUCCESS;

		array<VkPhysicalDevice> physical_device_array = { scratch };
		vk_result = module->_instance.enumerate_physical_devices(physical_device_array);
		if (vk_result)
			return util::to_error(vk_result);

		array<char8_t const*> device_extension_array = { scratch };
		vk_result = module->_instance.get_device_extension_array(device_extension_array);
		if (vk_result)
			return util::to_error(vk_result);

		array<VkPhysicalDevice> valid_physical_device_array = { scratch };

		for (size_t i = 0; i < physical_device_array.size(); ++i)
		{
			VkPhysicalDeviceProperties	device_properties;
			module->_instance.vtable()->vkGetPhysicalDeviceProperties(physical_device_array[i], &device_properties);

			vk_result = module->_instance.check_device_extensions(physical_device_array[i], device_extension_array.data(), device_extension_array.size(), scratch);
			if (vk_result)
				continue;

			vk_result = module->_instance.check_device_capabilities(device_properties);
			if (vk_result)
				continue;

			uint32_t queue_graphics_index;
			uint32_t queue_copy_index;
			vk_result = module->_instance.check_device_queues(physical_device_array[i], queue_graphics_index, queue_copy_index, scratch);
			if (vk_result)
				continue;

			err = valid_physical_device_array.push_back(physical_device_array[i]);
			if (err)
				return err;
		}

		if (nullptr == adapter_array)
		{
			adapter_size = valid_physical_device_array.size();
			return err_ok;
		}

		adapter_size = 0;
		while (adapter_capacity && adapter_size < valid_physical_device_array.size())
		{
			*adapter_array = util::to_adapter(valid_physical_device_array[adapter_size]);
			++adapter_size;
			--adapter_capacity;
			++adapter_array;
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	module::adapter_get_info(module_id module, adapter_id adapter, adapter_info_t& info)
	{
		VkPhysicalDevice vk_physical_device = util::to_physical_device(adapter);

		VkPhysicalDeviceProperties device_properties;
		module->_instance.vtable()->vkGetPhysicalDeviceProperties(vk_physical_device, &device_properties);
	}

	//--------------------------------------------------
	void	module::adapter_get_caps(module_id module, adapter_id adapter, adapter_caps_t& caps)
	{
		VkPhysicalDevice vk_physical_device = util::to_physical_device(adapter);

		VkPhysicalDeviceFeatures device_features;
		module->_instance.vtable()->vkGetPhysicalDeviceFeatures(vk_physical_device, &device_features);
	}

	//--------------------------------------------------
	void	module::adapter_get_limits(module_id module, adapter_id adapter, adapter_limits_t& limits)
	{
		VkPhysicalDevice vk_physical_device = util::to_physical_device(adapter);

		VkPhysicalDeviceProperties device_properties;
		module->_instance.vtable()->vkGetPhysicalDeviceProperties(vk_physical_device, &device_properties);
		auto& vk_limits = device_properties.limits;

		limits.max_descriptor_uniform_buffer_size = vk_limits.maxUniformBufferRange;
		limits.max_descriptor_storage_buffer_size = vk_limits.maxStorageBufferRange;

		limits.max_index_value = vk_limits.maxDrawIndexedIndexValue;
		limits.max_vertex_slot_size = vk_limits.maxVertexInputBindings;
		limits.max_vertex_stride = vk_limits.maxVertexInputBindingStride;
		limits.max_vertex_attribute_slot_size = vk_limits.maxVertexInputAttributes;
		limits.max_vertex_attribute_offset = vk_limits.maxVertexInputAttributeOffset;


		limits.max_uniform_size = vk_limits.maxPushConstantsSize;
		limits.max_descriptor_slot_size = vk_limits.maxBoundDescriptorSets;
		limits.max_per_stage_descriptor_sampler_size = vk_limits.maxPerStageDescriptorSamplers;
		limits.max_per_stage_descriptor_uniform_buffer_size = vk_limits.maxPerStageDescriptorUniformBuffers;
		limits.max_per_stage_descriptor_storage_buffer_size = vk_limits.maxPerStageDescriptorStorageBuffers;
		limits.max_per_stage_descriptor_sampled_texture_size = vk_limits.maxPerStageDescriptorSampledImages;
		limits.max_per_stage_descriptor_storage_texture_size = vk_limits.maxPerStageDescriptorStorageImages;
		limits.max_per_stage_descriptor_read_attachment_size = vk_limits.maxPerStageDescriptorInputAttachments;
		limits.max_per_stage_descriptor_size = vk_limits.maxPerStageResources;
		limits.max_descriptor_set_sampler_size = vk_limits.maxDescriptorSetSamplers;
		limits.max_descriptor_set_uniform_buffer_size = vk_limits.maxDescriptorSetUniformBuffers;
		limits.max_descriptor_set_storage_buffer_size = vk_limits.maxDescriptorSetStorageBuffers;
		limits.max_descriptor_set_sampled_texture_size = vk_limits.maxDescriptorSetSampledImages;
		limits.max_descriptor_set_storage_texture_size = vk_limits.maxDescriptorSetStorageImages;
		limits.max_descriptor_set_read_attachment_size = vk_limits.maxDescriptorSetInputAttachments;

		limits.max_sampler_mip_bias = vk_limits.maxSamplerLodBias;
		limits.max_sampler_anisotropy = static_cast<uint32_t>(vk_limits.maxSamplerAnisotropy);

		limits.max_attachment_set_width = vk_limits.maxFramebufferWidth;
		limits.max_attachment_set_height = vk_limits.maxFramebufferHeight;
		limits.max_subpass_render_target_size = vk_limits.maxColorAttachments;
	}
		
	//--------------------------------------------------
	error_t module::device_create(module_id module, device_id& device, cmd_queue_id& cmd_queue, device_create_t const& create, scratch_allocator_t* scratch)
	{
		error_t err;

		vk_device_t* vk_device;
		vk_frame_queue_t* vk_frame_queue;
		//vk_cmd_heap_t* vk_cmd_heap;

		err = u::mem_alloc(create.device_allocator, vk_device);
		if (err)
			goto _error_vk_device_memory;

		err = u::mem_alloc(create.device_allocator, vk_frame_queue);
		if (err)
			goto _error_vk_frame_queue_memory;

		/*err = u::mem_alloc(create.device_allocator, vk_cmd_heap);
		if (err)
			goto _error_vk_cmd_heap_memory;*/

		err = u::mem_alloc(create.device_allocator, device);
		if (err)
			goto _error_device_memory;

		err = u::mem_alloc(create.device_allocator, cmd_queue);
		if (err)
			goto _error_cmd_queue_memory;

		os::mutex_id queue_lock;
		err = os::mutex::create(queue_lock, 256);
		if (err)
			goto _error_queue_lock;

		O_CREATE(vk_device_t, vk_device)(&module->_instance, nullptr, queue_lock, create.device_allocator);
		O_CREATE(vk_frame_queue_t, vk_frame_queue)(create.device_allocator);
		//O_CREATE(vk_cmd_heap_t, vk_cmd_heap)(vk_device);
		O_CREATE(device_t, device)(vk_device, vk_frame_queue, create.object_allocator);
		O_CREATE(cmd_queue_t, cmd_queue)(vk_device, vk_frame_queue, device->cmd_heap(), device->descriptor_manager());

		err = device->init(util::to_physical_device(create.adapter), scratch);
		if (err)
			goto _error_device_init;

		err = vk_frame_queue->init();
		if (err)
			goto _error_vk_frame_queue_init;

		err = cmd_queue->init();
		if (err)
			goto _error_cmd_queue_init;

		return err_ok;

		//cmd_queue->clean();
	_error_cmd_queue_init:
		vk_frame_queue->clean();
	_error_vk_frame_queue_init:
		device->clean();
	_error_device_init:
		O_DESTROY(cmd_queue_t, cmd_queue);
		O_DESTROY(device_t, device);
		//O_DESTROY(vk_cmd_heap_t, vk_cmd_heap);
		O_DESTROY(vk_frame_queue_t, vk_frame_queue);
		O_DESTROY(vk_device_t, vk_device);

		os::mutex::destroy(queue_lock);
	_error_queue_lock:
		u::mem_free(create.device_allocator, cmd_queue);
	_error_cmd_queue_memory:
		u::mem_free(create.device_allocator, device);
	_error_device_memory:
		/*u::mem_free(create.device_allocator, vk_cmd_heap);
	_error_vk_cmd_heap_memory:*/
		u::mem_free(create.device_allocator, vk_frame_queue);
	_error_vk_frame_queue_memory:
		u::mem_free(create.device_allocator, vk_device);
	_error_vk_device_memory:

		device = nullptr;
		cmd_queue = nullptr;
		return err;
	}

	//--------------------------------------------------
	void	module::device_destroy(module_id module, device_id device, cmd_queue_id cmd_queue)
	{
		vk_device_t* vk_device = device->device();
		vk_frame_queue_t* vk_frame_queue = device->frame_queue();
		//vk_cmd_heap_t* vk_cmd_heap = device->cmd_heap();
		allocator_t* device_allocator = vk_device->device_allocator();

		cmd_queue->clean();
		vk_frame_queue->clean();
		device->clean();

		os::mutex::destroy(vk_device->queue_lock());

		O_DESTROY(cmd_queue_t, cmd_queue);
		O_DESTROY(device_t, device);
		//O_DESTROY(vk_cmd_heap_t, vk_cmd_heap);
		O_DESTROY(vk_frame_queue_t, vk_frame_queue);
		O_DESTROY(vk_device_t, vk_device);


		u::mem_free(device_allocator, cmd_queue);
		u::mem_free(device_allocator, device);
		//u::mem_free(device_allocator, vk_cmd_heap);
		u::mem_free(device_allocator, vk_frame_queue);
		u::mem_free(device_allocator, vk_device);
	}
	
	//--------------------------------------------------
	error_t	module::surface_create(module_id module, surface_id& surface, surface_create_info_t const& info, scratch_allocator_t* scratch)
	{
		error_t err;
		err = u::mem_alloc(module->_object_allocator, surface);
		if (err)
			return err;

		O_CREATE(surface_t, surface)(&module->_instance);
		VkResult vk_result = surface->init(info.window_ctx, info.window_ptr);
		if (vk_result)
		{
			O_DESTROY(surface_t, surface);
			u::mem_free(module->_object_allocator, surface);
			return util::to_error(vk_result);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	module::surface_destroy(module_id module, surface_id surface)
	{
		surface->clean();
		O_DESTROY(surface_t, surface);
		u::mem_free(module->_object_allocator, surface);
	}

} }
