#pragma once
#include "cmd_ctx.h"
#include "vk_device.h"
#include "system\thread\atomic.h"
#include "resource_impl.h"
#include "swap_chain_impl.h"
#include "thread\triple_buffer_queue.h"

namespace outland { namespace gpu 
{
	struct vk_resource_frame_t
	{
		uint64_t	defrag_descriptor_submited;
	};

	struct vk_render_frame_t
	{
		uint64_t	render_frame_submited;
		uint64_t	render_frame_finished;
		uint64_t	defrag_descriptor_finished;
	};

	class vk_frame_queue_t
	{
		triple_buffer_queue_id	_render_queue;
		triple_buffer_queue_id	_resource_queue;
		vk_resource_frame_t		_resource_msg_array[3];
		vk_render_frame_t		_render_msg_array[3];
		vk_resource_frame_t*	_resource_msg;
		vk_render_frame_t*		_render_msg;
		allocator_t*			_allocator;

	public:
		vk_frame_queue_t(allocator_t* allocator);
		~vk_frame_queue_t() = default;

		error_t init();
		void	clean();
		
		bool render_recv_msg(vk_resource_frame_t& frame);
		void render_send_msg(vk_render_frame_t const& frame);
		bool resource_recv_msg(vk_render_frame_t& frame);
		void resource_send_msg(vk_resource_frame_t const& frame);
	};

	//circular page heap
	class vk_cmd_heap_t 
	{
		vk_device_t*		_device;
		VkDeviceMemory		_memory;
		VkBuffer			_buffer;
		VkDeviceSize		_size;
		VkDeviceSize		_page;
		void*				_ptr;
		uint32_t			_page_mask;
		//VkDescriptorPool	_pool;
		//VkDescriptorSet		_set_array[max_transform_size];
		os::atomic_uint32_id	_head;
		void*					_atomic_storage;
		size_t				_index;
	public:
		vk_cmd_heap_t(vk_device_t* device);
		~vk_cmd_heap_t();

		VkResult		init(size_t heap_size, VkDeviceSize max_transform_size);
		void			clean();
		void			alloc(VkDeviceSize& begin, VkDeviceSize& end);
		void*			ptr() const { return _ptr; }
		//void			set_array(VkDescriptorSet(&set_array)[max_transform_size]) { u::mem_copy(set_array, _set_array, sizeof(_set_array)); }
		vk_device_t*	device() const { return _device; }
		VkBuffer		buffer() const { return _buffer; }
		size_t			index() const { return _index; }
		void			frame(size_t index) { _index = index; }
	};

	//linear allocator
	class cmd_allocator_t
	{
		vk_cmd_heap_t*	_heap;
		VkCommandPool	_pool_array[c::max_frame_size];
		VkDeviceSize	_begin;
		VkDeviceSize	_end;
		VkDeviceSize	_head;
		VkDeviceSize	_align;
		void*			_ptr;
		allocator_t*	_allocator;

	public:
		cmd_allocator_t(vk_cmd_heap_t* heap, allocator_t* allocator);
		VkResult		init();
		void			clean();
		void			alloc(uint32_t size, void*& ptr, uint32_t& dyn_offset);
		VkResult		reset(size_t index);
		vk_cmd_heap_t*	heap() const { return _heap; }
		VkCommandPool	pool(size_t index) const { return _pool_array[index]; }
		size_t			index() const { return _heap->index(); }
		allocator_t*	allocator() const { return _allocator; }
	};

	class cmd_ctx_t
	{
		vk_cmd_vtable_t const*		_vtable;
		cmd_allocator_t*			_allocator;
		VkCommandBuffer				_cmd_buffer;
		VkCommandBuffer				_cmd_buffer_array[c::max_frame_size];
		uint32_t					_offset_array[c::max_transform_size];
		pipeline_t const*			_pipeline;
		pipeline_layout_t const*	_layout;

	public:
		cmd_ctx_t(cmd_allocator_t* allocator);
		VkResult			init(VkCommandBufferLevel level);
		void				clean();
		VkCommandBuffer		cmd_buffer() const { return _cmd_buffer; }
		cmd_allocator_t*	cmd_allocator() const { return _allocator; }

		friend void cmd::bind_pipeline_layout(cmd_ctx_id cmd_ctx, pipeline_layout_id pipeline_layout);
		friend void cmd::bind_pipeline(cmd_ctx_id cmd_ctx, pipeline_id pipeline);
		friend void cmd::draw(cmd_ctx_id cmd_ctx, uint32_t vertex_size, uint32_t vertex_first, uint32_t instance_size, uint32_t instance_first);
		friend void cmd::draw_indexed(cmd_ctx_id cmd_ctx, uint32_t index_size, uint32_t index_first, uint32_t instance_size, uint32_t instance_first, uint32_t vertex_offset);
		friend void cmd::bind_vertex(cmd_ctx_id cmd_ctx, buffer_id const* buffer_array, uint32_t buffer_size, uint32_t slot_first);
		friend void cmd::bind_index(cmd_ctx_id cmd_ctx, buffer_id buffer, index_format_t format);
		friend void cmd::bind_descriptor(cmd_ctx_id cmd_ctx, descriptor_set_id const* set_array, uint32_t set_size, uint32_t slot_first);
		friend void cmd::uniform(cmd_ctx_id cmd_ctx, void const* uniform_array, uint32_t uniform_size, uint32_t uniform_offset, uint32_t slot);
		friend void cmd::transform(cmd_ctx_id cmd_ctx, void*& transform_buffer, uint32_t transform_size, uint32_t slot);
		friend error_t	cmd::record_finish(cmd_ctx_id cmd_ctx);
		friend error_t	cmd::record_deferred(cmd_ctx_id cmd_ctx, render_pass_id pass, uint32_t subpass);
		friend error_t	cmd::record_primary(cmd_ctx_id cmd_ctx);
		friend void cmd::execute(cmd_ctx_id cmd_ctx, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size);
		friend void cmd::render_state(cmd_ctx_id cmd_ctx, render_pass_id pass, render_state_t const& state, cmd_type_t cmd_type);
		friend void cmd::render_subpass(cmd_ctx_id cmd_ctx, render_pass_id pass, cmd_type_t cmd_type);
		friend void cmd::render_finish(cmd_ctx_id cmd_ctx, render_pass_id pass);
	};

	class cmd_queue_t
	{
		vk_device_t*			_device;
		vk_frame_queue_t*		_frame_queue;
		vk_cmd_heap_t*			_cmd_heap;
		vk_render_frame_t		_render_frame;
		VkFence					_frame_fence_array[c::max_frame_size];
		cmd_allocator_t*		_cmd_allocator_array[c::max_frame_size][c::max_cmd_allocator_size];
		size_t					_cmd_allocator_size[c::max_frame_size];
		
		vk_descriptor_manager_t*	_descriptor_manager;

	public:
		cmd_queue_t(vk_device_t* device, vk_frame_queue_t* frame_queue, vk_cmd_heap_t* cmd_heap, vk_descriptor_manager_t* descriptor_manager);
		error_t	init();
		void	clean();

		friend error_t cmd::update(cmd_queue_id cmd_queue);
		friend error_t cmd::submit(cmd_queue_id cmd_queue, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size, swap_chain_id const* present_array, size_t present_size);

		friend void cmd::back_buffer_acquire(cmd_ctx_id cmd_ctx, swap_chain_id swap_chain, size_t& index);
		friend void cmd::back_buffer_release(cmd_ctx_id cmd_ctx, swap_chain_id swap_chain);

		friend error_t	cmd::ctx_alloc(cmd_queue_id cmd_queue, cmd_allocator_id allocator, cmd_ctx_id* cmd_ctx_array, size_t cmd_ctx_size, cmd_type_t cmd_type);
		friend void		cmd::ctx_free(cmd_queue_id cmd_queue, cmd_allocator_id allocator, cmd_ctx_id const* cmd_ctx_array, size_t cmd_ctx_size);
	};
	


	
} }
