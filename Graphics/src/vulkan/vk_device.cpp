#include "pch.h"
#include "vk_device.h"

#define VK_DEVICE_LOAD(fnc) \
	if (nullptr == (_vtable.fnc = reinterpret_cast<PFN_##fnc>(_instance->vtable()->vkGetDeviceProcAddr(_device, #fnc)))) \
	{ \
		return VK_ERROR_INITIALIZATION_FAILED; \
	}

#define VK_COMMAND_LOAD(fnc) \
	if (nullptr == (_cmd_vtable.fnc = reinterpret_cast<PFN_##fnc>(_instance->vtable()->vkGetDeviceProcAddr(_device, #fnc)))) \
	{ \
		return VK_ERROR_INITIALIZATION_FAILED; \
	}

namespace outland { namespace gpu
{


	vk_device_t::vk_device_t(vk_instance_t* instance, VkAllocationCallbacks const* allocation_cb, os::mutex_id queue_lock, allocator_t* allocator)
		: _instance(instance)
		, _queue_array(allocator)
		, _allocation_cb(allocation_cb)
		, _queue_lock(queue_lock)
	{

	}

	VkResult vk_device_t::_load()
	{
		_vtable.vkDestroyDevice = nullptr;

		VK_FNC_DEVICE(VK_DEVICE_LOAD);
		VK_FNC_COMMAND(VK_COMMAND_LOAD);

		return VK_SUCCESS;
	}


	VkResult vk_device_t::_create(char8_t const* const* extension_array, size_t extension_size,
		uint32_t queue_graphics_index, uint32_t queue_copy_index, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock = { scratch };

		VkResult vk_result = VK_SUCCESS;

		array<VkQueueFamilyProperties> family_props_array = { scratch };
		vk_result = _instance->enumerate_device_queue_props(_physical_device, family_props_array);
		if (vk_result)
			return vk_result;

		float queue_priorities[] = { 1.0f };
		array<VkDeviceQueueCreateInfo> queue_create_info_array = { scratch };

		VkDeviceQueueCreateInfo queue_create_info;
		queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;			// VkStructureType              sType
		queue_create_info.pNext = nullptr;												// const void                  *pNext
		queue_create_info.flags = 0;													// VkDeviceQueueCreateFlags     flags
		queue_create_info.pQueuePriorities = queue_priorities;							// const float                 *pQueuePriorities
		queue_create_info.queueCount = 1;												// uint32_t                     queueCount
		//queue_create_info.queueFamilyIndex = ...;										// uint32_t                     queueFamilyIndex

		for (size_t i = 0; i < family_props_array.size(); ++i)
		{
			if (family_props_array[i].queueCount > 0)
			{
				queue_create_info.queueFamilyIndex = static_cast<uint32_t>(i);
				if (queue_create_info_array.push_back(queue_create_info))
					return VK_ERROR_OUT_OF_HOST_MEMORY;
			}
		}

		if (_queue_array.resize(queue_create_info_array.size()))
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		VkDeviceCreateInfo device_create_info;
		device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		device_create_info.pNext = nullptr;
		device_create_info.flags = 0;
		device_create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_info_array.size());
		device_create_info.pQueueCreateInfos = queue_create_info_array.data();
		device_create_info.enabledLayerCount = 0;
		device_create_info.ppEnabledLayerNames = nullptr;
		device_create_info.enabledExtensionCount = static_cast<uint32_t>(extension_size);
		device_create_info.ppEnabledExtensionNames = extension_array;
		device_create_info.pEnabledFeatures = nullptr;

		vk_result = _instance->vtable()->vkCreateDevice(_physical_device, &device_create_info, _allocation_cb, &_device);
		if (vk_result)
			return vk_result;

		vk_result = _load();
		if (vk_result)
		{
			//avoid crashing, we may have a memory leak if this ptr is not set, nothing we can do about it
			if (nullptr != _vtable.vkDestroyDevice)
				_vtable.vkDestroyDevice(_device, _allocation_cb);
			return vk_result;
		}

		for (size_t i = 0; i < queue_create_info_array.size(); ++i)
		{
			auto const& info = queue_create_info_array[i];
			auto& queue = _queue_array[i];
			queue.family_index = info.queueFamilyIndex;
			queue.familty_properties = family_props_array[info.queueFamilyIndex];
			_vtable.vkGetDeviceQueue(_device, info.queueFamilyIndex, 0, &queue.queue);

			if (info.queueFamilyIndex == queue_graphics_index)
			{
				_queue_graphics = &queue;
			}

			if (info.queueFamilyIndex == queue_copy_index)
			{
				_queue_copy = &queue;
			}
		}

		return VK_SUCCESS;
	}


	VkResult vk_device_t::init(VkPhysicalDevice physical_device, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock(scratch);
		
		VkResult vk_result = VK_SUCCESS;

		_physical_device = physical_device;

		array<char8_t const*> device_extension_array = { scratch };
		vk_result = _instance->get_device_extension_array(device_extension_array);
		if (vk_result)
			return vk_result;
	
		vk_result = _instance->check_device_extensions(physical_device, device_extension_array.data(), device_extension_array.size(), scratch);
		if (vk_result)
			return vk_result;

		_instance->vtable()->vkGetPhysicalDeviceProperties(physical_device, &_device_properties);
		_instance->vtable()->vkGetPhysicalDeviceFeatures(physical_device, &_device_features);
		_instance->vtable()->vkGetPhysicalDeviceMemoryProperties(physical_device, &_memory_properties);
		

		vk_result = _instance->check_device_capabilities(_device_properties);
		if (vk_result)
			return vk_result;

		uint32_t queue_graphics_index;
		uint32_t queue_copy_index;
		vk_result = _instance->check_device_queues(physical_device, queue_graphics_index, queue_copy_index, scratch);
		if (vk_result)
			return vk_result;

		vk_result = _create(device_extension_array.data(), device_extension_array.size(), queue_graphics_index, queue_copy_index, scratch);
		if (vk_result)
			return vk_result;

		return VK_SUCCESS;
	}

	void		vk_device_t::clean()
	{
		_vtable.vkDestroyDevice(_device, _allocation_cb);
	}

	VkResult vk_device_t::semaphore_create(VkSemaphore& semaphore)
	{
		VkResult vk_result;

		VkSemaphoreCreateInfo semaphore_create_info;
		semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		semaphore_create_info.pNext = nullptr;
		semaphore_create_info.flags = 0;

		vk_result = _vtable.vkCreateSemaphore(_device, &semaphore_create_info, _allocation_cb, &semaphore);
		if (vk_result)
			return vk_result;

		return VK_SUCCESS;
	}

	void vk_device_t::semaphore_destroy(VkSemaphore semaphore)
	{
		_vtable.vkDestroySemaphore(_device, semaphore, _allocation_cb);
	}

	VkResult	vk_device_t::fence_create(VkFence& fence, bool is_signaled)
	{
		VkResult vk_result;

		VkFenceCreateInfo fence_create_info;
		fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fence_create_info.pNext = nullptr;
		fence_create_info.flags = is_signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;

		vk_result = _vtable.vkCreateFence(_device, &fence_create_info, _allocation_cb, &fence);
		if (vk_result)
			return vk_result;

		return VK_SUCCESS;
	}

	void		vk_device_t::fence_destroy(VkFence fence)
	{
		_vtable.vkDestroyFence(_device, fence, _allocation_cb);
	}

} }
