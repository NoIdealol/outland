#include "pch.h"
#include "resource_impl.h"
#include "math\integer.h"

namespace outland { namespace gpu 
{
	//////////////////////////////////////////////////
	// RESOURCE LIST
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_resource_list_t::vk_resource_list_t()
		: _begin(nullptr)
		, _end(nullptr)
	{

	}

	//--------------------------------------------------
	void	vk_resource_list_t::move_back(vk_resource_list_t& resource_list)
	{
		if (nullptr == resource_list._begin)
			return;

		if (nullptr == _begin)
		{
			_begin = resource_list._begin;
			_end = resource_list._end;
			resource_list._begin = nullptr;
			resource_list._end = nullptr;
			return;
		}

		_end->next = resource_list._begin;
		_end = resource_list._end;
		resource_list._begin = nullptr;
		resource_list._end = nullptr;
	}

	//--------------------------------------------------
	void	vk_resource_list_t::push_back(vk_resource_t* resource)
	{
		if (_begin)
		{
			_end->next = resource;
			_end = resource;
			resource->next = nullptr;
		}
		else
		{
			_begin = resource;
			_end = resource;
			resource->next = nullptr;	
		}
	}

	//--------------------------------------------------
	vk_resource_t*	vk_resource_list_t::pop_front()
	{
		if (_begin)
		{
			vk_resource_t* ret = _begin;
			_begin = _begin->next;
			if (ret == _end)
				_end = nullptr;

			return ret;
		}
		else
		{
			return nullptr;
		}
	}

	//--------------------------------------------------
	vk_resource_t*	vk_resource_list_t::begin()
	{
		return _begin;
	}

	//--------------------------------------------------
	vk_resource_t*	vk_resource_list_t::end()
	{
		return _end;
	}

	//--------------------------------------------------
	void			vk_resource_list_t::clear()
	{
		_begin = nullptr;
		_end = nullptr;
	}

	//////////////////////////////////////////////////
	// RESOURCE ALLOCATOR
	//////////////////////////////////////////////////

	//--------------------------------------------------
	template<typename resource_t>
	vk_resource_allocator_t<resource_t>::vk_resource_allocator_t(allocator_t* allocator)
		: _allocator(allocator)
	{

	}

	//--------------------------------------------------
	template<typename resource_t>
	VkResult	vk_resource_allocator_t<resource_t>::alloc(resource_t*& resource)
	{
		if (u::mem_alloc<resource_t>(_allocator, resource))
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		resource->next = nullptr;
		return VK_SUCCESS;
	}

	//--------------------------------------------------
	template<typename resource_t>
	void	vk_resource_allocator_t<resource_t>::free(resource_t* resource)
	{
		u::mem_free<resource_t>(_allocator, resource);
	}


#define O_INSTANTIATE(resource_t) template class vk_resource_allocator_t<resource_t>
													
	O_INSTANTIATE(render_pass_t);
	O_INSTANTIATE(render_set_t);
	O_INSTANTIATE(descriptor_layout_t);
	O_INSTANTIATE(descriptor_set_t);
	O_INSTANTIATE(shader_t);
	O_INSTANTIATE(input_layout_t);
	O_INSTANTIATE(pipeline_layout_t);
	O_INSTANTIATE(pipeline_t);
	O_INSTANTIATE(sampler_t);
	O_INSTANTIATE(texture_t);
	O_INSTANTIATE(shader_view_t);
	O_INSTANTIATE(render_target_view_t);
	O_INSTANTIATE(depth_stencil_view_t);
	O_INSTANTIATE(buffer_t);
	
	//////////////////////////////////////////////////
	// RESOURCE QUEUE
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void vk_resource_queue_t::push_back(vk_resource_t* resource)
	{
		_queue_current.push_back(resource);
	}

	//--------------------------------------------------
	void vk_resource_queue_t::update(uint64_t frame_submited_id)
	{
		auto& queue = _queue_array[static_cast<size_t>(frame_submited_id & c::max_frame_modulo)];
		queue.move_back(_queue_current);
	}

	//--------------------------------------------------
	void vk_resource_queue_t::collect(uint64_t frame_finished_id, vk_resource_t*& begin, vk_resource_t*& end)
	{
		auto& queue = _queue_array[static_cast<size_t>(frame_finished_id & c::max_frame_modulo)];
		begin = queue.begin();
		end = queue.end();
		queue.clear();
	}

	//--------------------------------------------------
	void vk_resource_queue_t::collect(uint64_t frame_finished_id, vk_resource_list_t& list)
	{
		auto& queue = _queue_array[static_cast<size_t>(frame_finished_id & c::max_frame_modulo)];
		list.move_back(queue);
	}
	
	//////////////////////////////////////////////////
	// RENDER PASS MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void vk_render_pass_manager_t::_free(render_pass_t* resource)
	{
		_device->vtable()->vkDestroyRenderPass(_device->device(), resource->pass, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	vk_render_pass_manager_t::vk_render_pass_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
	{

	}

	void vk_render_pass_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<render_pass_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void vk_render_pass_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult vk_render_pass_manager_t::alloc(render_pass_t*& resource, VkRenderPassCreateInfo const& vk_info, uint32_t render_target_size)
	{
		VkResult vk_result;
		vk_result = _resource_allocator.alloc(resource);
		if (vk_result)
			return vk_result;

		vk_result = _device->vtable()->vkCreateRenderPass(_device->device(), &vk_info, _device->allocation_cb(), &resource->pass);
		if (vk_result)
		{
			_resource_allocator.free(resource);
			return vk_result;
		}

		resource->render_target_size = render_target_size;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void vk_render_pass_manager_t::free(render_pass_t* resource)
	{
		_garbage.push_back(resource);
	}

	//////////////////////////////////////////////////
	// ATTACHMENT SET MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void		vk_render_set_manager_t::_free(render_set_t* resource)
	{
		_device->vtable()->vkDestroyFramebuffer(_device->device(), resource->framebuffer, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	vk_render_set_manager_t::vk_render_set_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
	{
	}

	//--------------------------------------------------
	void		vk_render_set_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<render_set_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void		vk_render_set_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult	vk_render_set_manager_t::alloc(render_set_t*& resource, VkFramebufferCreateInfo const& vk_info)
	{
		VkResult vk_result;
		vk_result = _resource_allocator.alloc(resource);
		if (vk_result)
			return vk_result;

		vk_result = _device->vtable()->vkCreateFramebuffer(_device->device(), &vk_info, _device->allocation_cb(), &resource->framebuffer);
		if (vk_result)
		{
			_resource_allocator.free(resource);
			return vk_result;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_render_set_manager_t::free(render_set_t* resource)
	{
		_garbage.push_back(resource);
	}

	//////////////////////////////////////////////////
	// DESCRIPTOR LAYOUT MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	void		vk_descriptor_layout_manager_t::_free(descriptor_layout_t* resource)
	{
		_device->vtable()->vkDestroyDescriptorSetLayout(_device->device(), resource->layout, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	vk_descriptor_layout_manager_t::vk_descriptor_layout_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
	{
	}

	//--------------------------------------------------
	void		vk_descriptor_layout_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<descriptor_layout_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void		vk_descriptor_layout_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_layout_manager_t::alloc(descriptor_layout_t*& resource, VkDescriptorSetLayoutCreateInfo const& vk_info, VkImageLayout const* vk_layout_array)
	{
		VkResult vk_result;
		vk_result = _resource_allocator.alloc(resource);
		if (vk_result)
			return vk_result;

		vk_result = _device->vtable()->vkCreateDescriptorSetLayout(_device->device(), &vk_info, _device->allocation_cb(), &resource->layout);
		if (vk_result)
		{
			_resource_allocator.free(resource);
			return vk_result;
		}

		resource->desc_size = vk_info.bindingCount;
		for (uint32_t i = 0; i < vk_info.bindingCount; ++i)
		{
			resource->desc_array[i].binding = vk_info.pBindings[i].binding;
			resource->desc_array[i].size = vk_info.pBindings[i].descriptorCount;
			resource->desc_array[i].type = vk_info.pBindings[i].descriptorType;
			resource->desc_array[i].shader = vk_info.pBindings[i].stageFlags;
			resource->desc_array[i].layout = vk_layout_array[i];
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_descriptor_layout_manager_t::free(descriptor_layout_t* resource)
	{
		_garbage.push_back(resource);
	}
	

	//////////////////////////////////////////////////
	// DESCRIPTOR MANAGER
	//////////////////////////////////////////////////
	
	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::_pool_reset(size_t pool_index)
	{
		VkResult vk_result;
		auto& pool = _descriptor_pool_array[pool_index];
		O_ASSERT(0 == pool.set_size);

		vk_result = _device->vtable()->vkResetDescriptorPool(_device->device(), pool.pool, 0);
		if (vk_result)
			return vk_result;

		pool.set_allocated = 0;
		for (auto& it : pool.type_allocated)
			it = 0;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	size_t		vk_descriptor_manager_t::_pool_type_index(VkDescriptorType type)
	{
		switch (type)
		{
		case VK_DESCRIPTOR_TYPE_SAMPLER:
			return _pool_index_sampler;
		case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
			return _pool_index_sampled_image;
		case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
			return _pool_index_uniform_buffer;
		case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
			return _pool_index_storage_buffer;
		case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
			return _pool_index_input_attachment;
		default:
			return _pool_types_size;
		}
	}

	//--------------------------------------------------
	bool		vk_descriptor_manager_t::_pool_capacity(size_t pool_index, descriptor_layout_t* const* layout_array, size_t layout_size)
	{
		uint32_t	type_size[_pool_types_size] = { 0 };
		auto& pool = _descriptor_pool_array[pool_index];

		if ((pool.set_capacity - pool.set_allocated) < layout_size)
			return false;

		for (size_t i = 0; i < layout_size; ++i)
		{
			auto* layout = layout_array[i];
			for (size_t j = 0; j < layout->desc_size; ++j)
			{
				auto& slot = layout->desc_array[j];
				type_size[_pool_type_index(slot.type)] += slot.size;
			}
		}

		for (size_t i = 0; i < _pool_types_size; ++i)
		{
			if ((pool.type_capacity[i] - pool.type_allocated[i]) < type_size[i])
				return false;
		}

		return true;
	}

	//--------------------------------------------------
	void		vk_descriptor_manager_t::_delist_set(descriptor_set_t* set)
	{
		size_t pool_index = set->udata >> _pool_shift;
		size_t set_index = set->udata & _set_mask;

		auto& pool = _descriptor_pool_array[pool_index];
		--pool.set_size;
		if (set_index != pool.set_size)
		{
			pool.set_array[set_index] = pool.set_array[pool.set_size];
			pool.set_array[set_index]->udata = set->udata;
			pool.set_array[pool.set_size] = nullptr;
		}

	}

	//--------------------------------------------------
	void		vk_descriptor_manager_t::_enlist_set(descriptor_set_t* set, size_t pool_index)
	{
		auto& pool = _descriptor_pool_array[pool_index];
		size_t set_index = pool.set_size;
		set->udata = (pool_index << _pool_shift) | set_index;

		++pool.set_size;
		pool.set_array[set_index] = set;
	}

	//--------------------------------------------------
	void		vk_descriptor_manager_t::_hotswap_sets(uint64_t defrag_id)
	{
		O_ASSERT(_defrag_state_swapping == _defrag_state);
		O_ASSERT(_defrag_id == defrag_id);

		for (size_t i = 0; i < _defrag_set_size; ++i)
		{
			_defrag_set_array[i]->set = _hotswap_set_array[i];
		}	
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::_defrag_swap(uint64_t defrag_finished_id)
	{
		O_ASSERT(_defrag_state_swapping == _defrag_state);

		if (_defrag_id == defrag_finished_id)
		{
			_defrag_state = _defrag_state_running;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::_defrag_sets()
	{
		VkResult vk_result;
		auto& pool = _descriptor_pool_array[_defrag_dst];
		VkDescriptorSetLayout vk_layout_array[_max_defrag_size];

		for (size_t i = 0; i < _defrag_set_size; ++i)
		{
			vk_layout_array[i] = _defrag_set_array[i]->layout->layout;
		}

		VkDescriptorSetAllocateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.descriptorPool = pool.pool;
		vk_info.descriptorSetCount = static_cast<uint32_t>(_defrag_set_size);
		vk_info.pSetLayouts = vk_layout_array;

		vk_result = _device->vtable()->vkAllocateDescriptorSets(_device->device(), &vk_info, _hotswap_set_array);
		if (vk_result)
			return vk_result;

		VkCopyDescriptorSet vk_copy_array[c::max_descriptor_layout_desc_size * _max_defrag_size];
		uint32_t vk_copy_size = 0;

		for (size_t i = 0; i < _defrag_set_size; ++i)
		{
			descriptor_set_t* set = _defrag_set_array[i];
			auto* layout = set->layout;

			++pool.set_allocated;
			for (size_t j = 0; j < layout->desc_size; ++j)
			{
				auto& desc = layout->desc_array[j];
				auto& vk_copy = vk_copy_array[vk_copy_size];

				pool.type_allocated[_pool_type_index(desc.type)] += desc.size;

				vk_copy.sType = VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET;
				vk_copy.pNext = nullptr;
				vk_copy.srcSet = set->set;
				vk_copy.srcBinding = desc.binding;
				vk_copy.srcArrayElement = 0;
				vk_copy.dstSet = _hotswap_set_array[i];
				vk_copy.dstBinding = desc.binding;
				vk_copy.dstArrayElement = 0;
				vk_copy.descriptorCount = desc.size;

				++vk_copy_size;
			}
		}

		_device->vtable()->vkUpdateDescriptorSets(_device->device(), 0, nullptr, vk_copy_size, vk_copy_array);

		//hotswap
		for (size_t i = 0; i < _defrag_set_size; ++i)
		{
			_delist_set(_defrag_set_array[i]);
			_enlist_set(_defrag_set_array[i], _defrag_dst);
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::_defrag_begin()
	{
		O_ASSERT(_defrag_state_none == _defrag_state);

		uint32_t max_fragment = 0;
		size_t pool_index = c::descriptor_pool_size;
		for (size_t i = 0; i < c::descriptor_pool_size; ++i)
		{
			uint32_t fragment = _descriptor_pool_array[i].set_allocated - static_cast<uint32_t>(_descriptor_pool_array[i].set_size);
			if (fragment > max_fragment)
			{
				max_fragment = fragment;
				pool_index = i;
			}
		}

		if (max_fragment && c::descriptor_pool_size != pool_index)
		{
			_defrag_src = pool_index;
			_defrag_state = _defrag_state_running;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::_defrag_run(uint64_t frame_submited_id)
	{
		O_ASSERT(_defrag_state_running == _defrag_state);
		O_ASSERT(_defrag_src != _defrag_dst);

		auto& pool_src = _descriptor_pool_array[_defrag_src];
		auto& pool_dst = _descriptor_pool_array[_defrag_dst];

		//vk_descriptor_set_t* set_array[_max_defrag_size];
		_defrag_set_size = 0;
		while (u::array_size(_defrag_set_array) > _defrag_set_size && pool_src.set_size > _defrag_set_size)
		{
			_defrag_set_array[_defrag_set_size] = pool_src.set_array[_defrag_set_size];
			++_defrag_set_size;
		}

		if (_defrag_set_size)
		{
			VkResult vk_result = _defrag_sets();
			if (vk_result)
				return vk_result;

			++_defrag_id;
			_defrag_state = _defrag_state_swapping;
		}
		else
		{
			_frame_id = frame_submited_id;
			_defrag_dst = _defrag_src;
			_defrag_state = _defrag_state_waiting;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::_defrag_reset(uint64_t frame_finished_id)
	{
		O_ASSERT(_defrag_state_waiting == _defrag_state);

		VkResult vk_result;
		if (frame_finished_id >= _frame_id)
		{
			vk_result = _pool_reset(_defrag_dst);
			if (vk_result)
			{
				//delay the next reset
				_frame_id = frame_finished_id + 30;
				return vk_result;
			}

			_defrag_state = _defrag_state_none;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::_alloc(
		descriptor_set_t** set_array,
		size_t set_size,
		descriptor_layout_t* const* layout_array,
		size_t pool_index
	)
	{
		VkResult vk_result;

		auto& pool = _descriptor_pool_array[pool_index];
		VkDescriptorSet	vk_descriptor_array[_max_alloc_size];
		VkDescriptorSetLayout vk_layout_array[_max_alloc_size];

		for (size_t i = 0; i < set_size; ++i)
		{
			vk_layout_array[i] = layout_array[i]->layout;
			vk_result = _resource_allocator.alloc(set_array[i]);
			if (vk_result)
			{
				//revert
				for (size_t j = 0; j < i; ++j)
				{
					_resource_allocator.free(set_array[j]);
				}
				return vk_result;
			}
		}

		VkDescriptorSetAllocateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.descriptorPool = pool.pool;
		vk_info.descriptorSetCount = static_cast<uint32_t>(set_size);
		vk_info.pSetLayouts = vk_layout_array;

		vk_result = _device->vtable()->vkAllocateDescriptorSets(_device->device(), &vk_info, vk_descriptor_array);
		if (vk_result)
		{
			for (size_t i = 0; i < set_size; ++i)
			{
				_resource_allocator.free(set_array[i]);
			}
			return vk_result;
		}

		pool.set_allocated += static_cast<uint32_t>(set_size);
		for (size_t i = 0; i < set_size; ++i)
		{
			auto* layout = layout_array[i];
			auto* set = set_array[i];
			set->layout = layout;
			set->set = vk_descriptor_array[i];
			_enlist_set(set, pool_index);

			for (size_t j = 0; j < layout->desc_size; ++j)
			{
				auto& desc = layout->desc_array[j];
				pool.type_allocated[_pool_type_index(desc.type)] += desc.size;
			}	
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	vk_descriptor_manager_t::vk_descriptor_manager_t(vk_device_t* device, allocator_t* allocator)
		: _allocator(allocator)
		, _device(device)
		, _resource_allocator(allocator)
		, _defrag_src(0)
		, _defrag_dst(0)
		, _defrag_state(_defrag_state_none)
		, _frame_id(0)
		, _defrag_id(0)
	{
		for (auto& it : _descriptor_pool_array)
		{
			it.pool = VK_NULL_HANDLE;
			it.set_allocated = 0;
			it.set_array = nullptr;
			it.set_capacity = 0;
			it.set_size = 0;
			for (auto& it_allocated : it.type_allocated)
				it_allocated = 0;
			for (auto& it_capacity : it.type_capacity)
				it_capacity = 0;
		}

		
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::init(VkDescriptorPoolSize const* pool_size_array, size_t pool_size_size, uint32_t max_sets)
	{
		VkResult vk_result;

		//linear, reset only pool
		VkDescriptorPoolCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;// VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
		vk_info.maxSets = max_sets;
		vk_info.poolSizeCount = static_cast<uint32_t>(pool_size_size);
		vk_info.pPoolSizes = pool_size_array;

		for (size_t i = 0; i < c::descriptor_pool_size; ++i)
		{
			auto& pool = _descriptor_pool_array[i];
			if (u::mem_alloc(_allocator, pool.set_array, static_cast<size_t>(max_sets)))
				vk_result = VK_ERROR_OUT_OF_HOST_MEMORY;
			else
				vk_result = _device->vtable()->vkCreateDescriptorPool(_device->device(), &vk_info, _device->allocation_cb(), &pool.pool);

			if (vk_result)
			{
				if (pool.set_array)
				{
					u::mem_free(_allocator, pool.set_array, static_cast<size_t>(max_sets));
					pool.set_array = nullptr;
				}

				for (size_t j = 0; j < i; ++j)
				{
					auto& pool = _descriptor_pool_array[j];

					_device->vtable()->vkDestroyDescriptorPool(_device->device(), pool.pool, _device->allocation_cb());
					pool.pool = VK_NULL_HANDLE;

					u::mem_free(_allocator, pool.set_array, static_cast<size_t>(max_sets));
					pool.set_array = nullptr;
				}

				return vk_result;
			}

			pool.set_capacity = max_sets;
			for (auto& it : pool.type_capacity)
				it = 0;
			for (size_t j = 0; j < pool_size_size; ++j)
				pool.type_capacity[_pool_type_index(pool_size_array[j].type)] = pool_size_array[j].descriptorCount;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_descriptor_manager_t::clean()
	{
		for (size_t i = 0; i < c::descriptor_pool_size; ++i)
		{
			auto& pool = _descriptor_pool_array[i];

			_device->vtable()->vkDestroyDescriptorPool(_device->device(), pool.pool, _device->allocation_cb());
			pool.pool = VK_NULL_HANDLE;

			u::mem_free(_allocator, pool.set_array, static_cast<size_t>(pool.set_capacity));
			pool.set_array = nullptr;
		}
	}

	//--------------------------------------------------
	void		vk_descriptor_manager_t::hotswap(uint64_t defrag_id)
	{
		_hotswap_sets(defrag_id);
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id, uint64_t defrag_finished_id, uint64_t& defrag_submission_id)
	{
		defrag_submission_id = _defrag_id;

		switch (_defrag_state)
		{
		case outland::gpu::vk_descriptor_manager_t::_defrag_state_none:
			return _defrag_begin();
		case outland::gpu::vk_descriptor_manager_t::_defrag_state_running:
			return _defrag_run(frame_submited_id);
		case outland::gpu::vk_descriptor_manager_t::_defrag_state_swapping:
			return _defrag_swap(defrag_finished_id);
		case outland::gpu::vk_descriptor_manager_t::_defrag_state_waiting:
			return _defrag_reset(frame_finished_id);
		default:
			O_ASSERT(false);
			return VK_ERROR_DEVICE_LOST;
		}
	}

	//--------------------------------------------------
	VkResult	vk_descriptor_manager_t::alloc(descriptor_set_t** set_array, size_t set_size, descriptor_layout_t* const* layout_array)
	{
		VkResult vk_result;
		//descriptor_set_t* vk_set_array[_max_alloc_size];
		size_t set_allocated = 0;

		while (set_allocated != set_size)
		{
			//calc batch size
			size_t set_batch = 0;
			while (set_batch < _max_alloc_size && (set_allocated + set_batch) < set_size)
			{
				++set_batch; //whatever
			}

			//find pool
			size_t pool_index = 0;
			while (pool_index < c::descriptor_pool_size)
			{
				if (pool_index != _defrag_dst && _pool_capacity(pool_index, layout_array + set_allocated, set_batch))
					break;

				++pool_index;
			}

			if (c::descriptor_pool_size == pool_index)
			{
				free(set_array, set_allocated);
				return VK_ERROR_OUT_OF_DEVICE_MEMORY;
			}

			vk_result = _alloc(set_array + set_allocated, set_batch, layout_array + set_allocated, pool_index);
			if (vk_result)
			{
				free(set_array, set_allocated);
				return vk_result;
			}

			set_allocated += set_batch;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_descriptor_manager_t::free(descriptor_set_t* const* set_array, size_t set_size)
	{
		for (size_t i = 0; i < set_size; ++i)
		{
			auto* set = static_cast<descriptor_set_t*>(set_array[i]);			
			_delist_set(set);
			_resource_allocator.free(set);
		}
	}

	//////////////////////////////////////////////////
	// SHADER MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_shader_manager_t::vk_shader_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
	{

	}

	//--------------------------------------------------
	void		vk_shader_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{

	}

	//--------------------------------------------------
	VkResult	vk_shader_manager_t::alloc(shader_t*& resource, VkShaderModuleCreateInfo const& vk_info)
	{
		VkResult vk_result;
		vk_result = _resource_allocator.alloc(resource);
		if (vk_result)
			return vk_result;

		vk_result = _device->vtable()->vkCreateShaderModule(_device->device(), &vk_info, _device->allocation_cb(), &resource->shader);
		if (vk_result)
		{
			_resource_allocator.free(resource);
			return vk_result;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_shader_manager_t::free(shader_t* resource)
	{
		_device->vtable()->vkDestroyShaderModule(_device->device(), resource->shader, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//////////////////////////////////////////////////
	// INPUT LAYOUT MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_input_layout_manager_t::vk_input_layout_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
		, _pool(VK_NULL_HANDLE)
	{

	}

	//--------------------------------------------------
	VkResult	vk_input_layout_manager_t::init(VkDescriptorPoolSize const& pool_size, uint32_t max_sets)
	{
		VkResult vk_result;

		VkDescriptorPoolCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
		vk_info.maxSets = max_sets;
		vk_info.poolSizeCount = 1;
		vk_info.pPoolSizes = &pool_size;

		vk_result = _device->vtable()->vkCreateDescriptorPool(_device->device(), &vk_info, _device->allocation_cb(), &_pool);
		if (vk_result)
			return vk_result;
		
		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_input_layout_manager_t::clean()
	{
		_device->vtable()->vkDestroyDescriptorPool(_device->device(), _pool, _device->allocation_cb());
	}

	//--------------------------------------------------
	void		vk_input_layout_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<input_layout_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void		vk_input_layout_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult	vk_input_layout_manager_t::alloc(input_layout_t*& resource, VkDescriptorSetLayoutCreateInfo const& vk_info, VkPushConstantRange const* vk_push_array, uint32_t vk_push_size)
	{
		VkResult vk_result;
		vk_result = _resource_allocator.alloc(resource);
		if (vk_result)
			return vk_result;

		vk_result = _device->vtable()->vkCreateDescriptorSetLayout(_device->device(), &vk_info, _device->allocation_cb(), &resource->transform_layout);
		if (vk_result)
		{
			_resource_allocator.free(resource);
			return vk_result;
		}

		VkDescriptorSetAllocateInfo vk_alloc;
		vk_alloc.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		vk_alloc.pNext = nullptr;
		vk_alloc.descriptorPool = _pool;
		vk_alloc.descriptorSetCount = 1;
		vk_alloc.pSetLayouts = &resource->transform_layout;

		vk_result = _device->vtable()->vkAllocateDescriptorSets(_device->device(), &vk_alloc, &resource->transform_set);
		if (vk_result)
		{
			_device->vtable()->vkDestroyDescriptorSetLayout(_device->device(), resource->transform_layout, _device->allocation_cb());
			_resource_allocator.free(resource);
			return vk_result;
		}

		resource->transform_size = vk_info.bindingCount;
		resource->uniform_size = vk_push_size;
		u::mem_copy(resource->uniform_array, vk_push_array, sizeof(VkPushConstantRange) * vk_push_size);

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_input_layout_manager_t::_free(input_layout_t* resource)
	{
		_device->vtable()->vkFreeDescriptorSets(_device->device(), _pool, 1, &resource->transform_set);
		_device->vtable()->vkDestroyDescriptorSetLayout(_device->device(), resource->transform_layout, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	void		vk_input_layout_manager_t::free(input_layout_t* resource)
	{
		_garbage.push_back(resource);
	}

	//////////////////////////////////////////////////
	// PIPELINE LAYOUT MANAGER
	//////////////////////////////////////////////////

	
	//--------------------------------------------------
	vk_pipeline_layout_manager_t::vk_pipeline_layout_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
	{

	}

	//--------------------------------------------------
	void		vk_pipeline_layout_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<pipeline_layout_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void		vk_pipeline_layout_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult	vk_pipeline_layout_manager_t::alloc(pipeline_layout_t*& resource, descriptor_layout_t* const* descriptor_layout_array, size_t descriptor_layout_size, input_layout_t const* input_layout)
	{	
		VkResult vk_result;
		vk_result = _resource_allocator.alloc(resource);
		if (vk_result)
			return vk_result;

		VkDescriptorSetLayout vk_layout_array[c::max_descriptor_set_size + 1];
		for (size_t i = 0; i < descriptor_layout_size; ++i)
		{
			vk_layout_array[i] = descriptor_layout_array[i]->layout;
		}
		vk_layout_array[descriptor_layout_size] = input_layout->transform_layout;

		VkPipelineLayoutCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;
		vk_info.setLayoutCount = static_cast<uint32_t>(descriptor_layout_size + 1);
		vk_info.pSetLayouts = vk_layout_array;
		vk_info.pushConstantRangeCount = input_layout->uniform_size;
		vk_info.pPushConstantRanges = input_layout->uniform_array;

		vk_result = _device->vtable()->vkCreatePipelineLayout(_device->device(), &vk_info, _device->allocation_cb(), &resource->layout);
		if (vk_result)
		{
			_resource_allocator.free(resource);
			return vk_result;
		}

		resource->descriptor_set_size = static_cast<uint32_t>(descriptor_layout_size);
		u::mem_copy(static_cast<input_layout_t*>(resource), input_layout, sizeof(input_layout_t));

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_pipeline_layout_manager_t::_free(pipeline_layout_t* resource)
	{
		_device->vtable()->vkDestroyPipelineLayout(_device->device(), resource->layout, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	void		vk_pipeline_layout_manager_t::free(pipeline_layout_t* resource)
	{
		_garbage.push_back(resource);
	}

	//////////////////////////////////////////////////
	// PIPELINE LAYOUT MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_pipeline_manager_t::vk_pipeline_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
		, _cache(VK_NULL_HANDLE)
	{
		//todo: pipeline cache
	}

	//--------------------------------------------------
	void		vk_pipeline_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<pipeline_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void		vk_pipeline_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult	vk_pipeline_manager_t::_alloc(
		pipeline_t** pipeline_array,
		size_t pipeline_size,
		VkGraphicsPipelineCreateInfo const* vk_info_array)
	{
		VkResult vk_result;

		VkPipeline	vk_pipeline_array[_max_alloc_size];
	
		for (size_t i = 0; i < pipeline_size; ++i)
		{
			vk_result = _resource_allocator.alloc(pipeline_array[i]);
			if (vk_result)
			{
				//revert
				for (size_t j = 0; j < i; ++j)
				{
					_resource_allocator.free(pipeline_array[j]);
				}
				return vk_result;
			}

			pipeline_array[i]->vertex_size = vk_info_array[i].pVertexInputState->vertexBindingDescriptionCount;
		}

		vk_result = _device->vtable()->vkCreateGraphicsPipelines(
			_device->device(), 
			_cache, 
			static_cast<uint32_t>(pipeline_size), 
			vk_info_array, 
			_device->allocation_cb(),
			vk_pipeline_array);

		if (vk_result)
		{
			for (size_t i = 0; i < pipeline_size; ++i)
			{
				_resource_allocator.free(pipeline_array[i]);
			}
			return vk_result;
		}

		for (size_t i = 0; i < pipeline_size; ++i)
		{
			pipeline_array[i]->pipeline = vk_pipeline_array[i];
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_pipeline_manager_t::alloc(pipeline_t** pipeline_array, size_t pipeline_size, VkGraphicsPipelineCreateInfo const* vk_info_array)
	{
		VkResult vk_result;
		size_t pipeline_allocated = 0;

		while (pipeline_allocated != pipeline_size)
		{
			//calc batch size
			size_t pipeline_batch = 0;
			while (pipeline_batch < _max_alloc_size && (pipeline_allocated + pipeline_batch) < pipeline_size)
			{
				++pipeline_batch; //whatever
			}

			vk_result = _alloc(pipeline_array + pipeline_allocated, pipeline_batch, vk_info_array + pipeline_allocated);
			if (vk_result)
			{
				free(pipeline_array, pipeline_allocated);
				return vk_result;
			}

			pipeline_allocated += pipeline_batch;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_pipeline_manager_t::_free(pipeline_t* resource)
	{
		_device->vtable()->vkDestroyPipeline(_device->device(), resource->pipeline, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	void		vk_pipeline_manager_t::free(pipeline_t* const* pipeline_array, size_t pipeline_size)
	{
		auto* end = pipeline_array + pipeline_size;
		while (pipeline_array != end)
		{
			_garbage.push_back(*pipeline_array);
			++pipeline_array;
		}
	}

	//////////////////////////////////////////////////
	// SAMPLER MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_sampler_manager_t::vk_sampler_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
	{

	}

	//--------------------------------------------------
	void		vk_sampler_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<sampler_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void		vk_sampler_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult	vk_sampler_manager_t::alloc(sampler_t*& resource, VkSamplerCreateInfo const& vk_info)
	{
		VkResult vk_result;
		vk_result = _resource_allocator.alloc(resource);
		if (vk_result)
			return vk_result;

		vk_result = _device->vtable()->vkCreateSampler(_device->device(), &vk_info, _device->allocation_cb(), &resource->sampler);
		if (vk_result)
		{
			_resource_allocator.free(resource);
			return vk_result;
		}

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_sampler_manager_t::_free(sampler_t* resource)
	{
		_device->vtable()->vkDestroySampler(_device->device(), resource->sampler, _device->allocation_cb());
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	void		vk_sampler_manager_t::free(sampler_t* resource)
	{
		_garbage.push_back(resource);
	}

	//////////////////////////////////////////////////
	// VIEW MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_view_manager_t::vk_view_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _resource_allocator(allocator)
		, _frame_finished_id(0)
	{

	}

	//--------------------------------------------------
	void		vk_view_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;
		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<vk_texture_view_t*>(begin);
			begin = begin->next;
			_free(resource);
		}
	}

	//--------------------------------------------------
	void		vk_view_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);
	}

	//--------------------------------------------------
	VkResult	vk_view_manager_t::_alloc(vk_texture_view_t* resource, VkImageViewCreateInfo const& vk_info)
	{
		return _device->vtable()->vkCreateImageView(_device->device(), &vk_info, _device->allocation_cb(), &resource->view);
	}

	//--------------------------------------------------
	VkResult	vk_view_manager_t::alloc(shader_view_t*& resource, VkImageViewCreateInfo const& vk_info, texture_t* texture)
	{
		static_assert(sizeof(shader_view_t) == sizeof(vk_texture_view_t), "bad mem_free");

		VkResult vk_result;
		if (u::mem_alloc(_resource_allocator, resource))
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		vk_result = _alloc(resource, vk_info);
		if (vk_result)
		{
			u::mem_free(_resource_allocator, resource);
			return vk_result;
		}

		resource->texture = texture;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_view_manager_t::alloc(render_target_view_t*& resource, VkImageViewCreateInfo const& vk_info, texture_t* texture)
	{
		static_assert(sizeof(render_target_view_t) == sizeof(vk_texture_view_t), "bad mem_free");

		VkResult vk_result;
		if (u::mem_alloc(_resource_allocator, resource))
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		vk_result = _alloc(resource, vk_info);
		if (vk_result)
		{
			u::mem_free(_resource_allocator, resource);
			return vk_result;
		}

		resource->texture = texture;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	VkResult	vk_view_manager_t::alloc(depth_stencil_view_t*& resource, VkImageViewCreateInfo const& vk_info, texture_t* texture)
	{
		static_assert(sizeof(depth_stencil_view_t) == sizeof(vk_texture_view_t), "bad mem_free");

		VkResult vk_result;
		if (u::mem_alloc(_resource_allocator, resource))
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		vk_result = _alloc(resource, vk_info);
		if (vk_result)
		{
			u::mem_free(_resource_allocator, resource);
			return vk_result;
		}

		resource->texture = texture;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_view_manager_t::_free(vk_texture_view_t* resource)
	{
		_device->vtable()->vkDestroyImageView(_device->device(), resource->view, _device->allocation_cb());
		u::mem_free(_resource_allocator, resource);
	}

	//--------------------------------------------------
	void		vk_view_manager_t::free(vk_texture_view_t* resource)
	{
		_garbage.push_back(resource);
	}

	//////////////////////////////////////////////////
	// TEXTURE MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_texture_manager_t::vk_texture_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _frame_finished_id(0)
		, _resource_allocator(allocator)
	{
		_sampled_heap.vallocator = nullptr;
		_sampled_heap.memory = VK_NULL_HANDLE;
		_sampled_heap.size = 0;
		_sampled_heap.type = 0;
	}

	//--------------------------------------------------
	VkResult	vk_texture_manager_t::init(vk_heap_create_t const& sampled_create)
	{
		VkResult result;

		VkImageCreateInfo image_create;
		image_create.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		image_create.pNext = nullptr;
		image_create.flags = 0;
		image_create.imageType = VK_IMAGE_TYPE_2D;
		image_create.format = VK_FORMAT_R8G8B8A8_UNORM;
		image_create.extent.width = 32;
		image_create.extent.height = 32;
		image_create.extent.depth = 1;
		image_create.mipLevels = 4;
		image_create.arrayLayers = 1;
		image_create.samples = VK_SAMPLE_COUNT_1_BIT;
		image_create.tiling = VK_IMAGE_TILING_OPTIMAL;
		image_create.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		image_create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		image_create.queueFamilyIndexCount = 0;
		image_create.pQueueFamilyIndices = nullptr;
		image_create.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		VkImage image;
		result = _device->vtable()->vkCreateImage(_device->device(), &image_create, _device->allocation_cb(), &image);
		if (result)
			return result;

		VkMemoryRequirements	memory_req;
		_device->vtable()->vkGetImageMemoryRequirements(_device->device(), image, &memory_req);
		_device->vtable()->vkDestroyImage(_device->device(), image, _device->allocation_cb());

		uint32_t memory_type;
		VkMemoryPropertyFlags const memory_flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		if (!util::find_memory_index(_device->memory_properties(), memory_flags, memory_req.memoryTypeBits, memory_type))
		{	
			return VK_ERROR_OUT_OF_DEVICE_MEMORY;
		}

		VkMemoryAllocateInfo memory_alloc;
		memory_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memory_alloc.pNext = nullptr;
		memory_alloc.allocationSize = sampled_create.size;
		memory_alloc.memoryTypeIndex = memory_type;

		VkDeviceMemory memory;
		result = _device->vtable()->vkAllocateMemory(_device->device(), &memory_alloc, _device->allocation_cb(), &memory);
		if (result)
		{
			return result;
		}

		_sampled_heap.vallocator = sampled_create.vallocator;
		_sampled_heap.memory = memory;
		_sampled_heap.size = memory_alloc.allocationSize;
		_sampled_heap.type = memory_type;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_texture_manager_t::clean()
	{
		_device->vtable()->vkFreeMemory(_device->device(), _sampled_heap.memory, _device->allocation_cb());
		_sampled_heap.vallocator = nullptr;
		_sampled_heap.memory = VK_NULL_HANDLE;
		_sampled_heap.size = 0;
		_sampled_heap.type = 0;
	}

	//--------------------------------------------------
	bool		vk_texture_manager_t::_is_individually_allocated(texture_t* resource)
	{
		VkImageUsageFlags const usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
		return 0 != (usage & resource->usage);
	}

	//--------------------------------------------------
	void		vk_texture_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;

		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<texture_t*>(begin);
			begin = begin->next;
			_free(resource);
		}

		_unbound.collect(frame_finished_id, _allocating);
	}

	//--------------------------------------------------
	void		vk_texture_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id, device_event_t* result_array, size_t result_capacity, size_t& result_size)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		_unbound.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);


		texture_t* resource;
		result_size = 0;
		while ((result_capacity > result_size) && (nullptr != (resource = static_cast<texture_t*>(_allocating.pop_front()))))
		{
			auto& result = result_array[result_size];
			result.type = device_event_type_texture_create;
			result.texture.user_data = resource->user_data;
			result.texture.error = _alloc(resource);

			if (result.texture.error)
			{
				_device->vtable()->vkDestroyImage(_device->device(), resource->image, _device->allocation_cb());
				_resource_allocator.free(resource);
				result.texture.texture = nullptr;
			}
			else
			{
				result.texture.texture = resource;
			}
			
			++result_size;
		}
	}

	//--------------------------------------------------
	VkResult	vk_texture_manager_t::alloc(void* user_data, VkImageCreateInfo const& vk_info, VkPipelineStageFlags stage, VkShaderStageFlags shader)
	{
		VkResult result;

		texture_t* resource;
		result = _resource_allocator.alloc(resource);
		if (result)
			return result;

		VkImage image;
		result = _device->vtable()->vkCreateImage(_device->device(), &vk_info, _device->allocation_cb(), &image);
		if (result)
		{
			_resource_allocator.free(resource);
			return result;
		}

		//init resource
		resource->image = image;
		resource->image_type = vk_info.imageType;
		resource->format = vk_info.format;
		resource->extent =vk_info.extent;
		resource->mip_levels = vk_info.mipLevels;
		resource->array_layers = vk_info.arrayLayers;
		resource->usage = vk_info.usage;
		resource->stage = stage;
		resource->shader = shader;

		resource->user_data = user_data;
		resource->offset = 0;
		resource->size = 0;
		
		_unbound.push_back(resource);

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	error_t		vk_texture_manager_t::_alloc(texture_t* resource)
	{
		error_t err;
		VkResult result;
		VkMemoryRequirements memory_req;
		_device->vtable()->vkGetImageMemoryRequirements(_device->device(), resource->image, &memory_req);

		if (_is_individually_allocated(resource))
		{
			uint32_t memory_type;
			VkMemoryPropertyFlags const memory_flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
			if (!util::find_memory_index(_device->memory_properties(), memory_flags, memory_req.memoryTypeBits, memory_type))
			{
				return err_out_of_vram;
			}

			VkMemoryAllocateInfo memory_alloc;
			memory_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			memory_alloc.pNext = nullptr;
			memory_alloc.allocationSize = memory_req.size;
			memory_alloc.memoryTypeIndex = memory_type;

			VkDeviceMemory memory;
			result = _device->vtable()->vkAllocateMemory(_device->device(), &memory_alloc, _device->allocation_cb(), &memory);
			if (result)
			{
				return util::to_error(result);
			}

			result = _device->vtable()->vkBindImageMemory(_device->device(), resource->image, memory, 0);
			if (result)
			{
				_device->vtable()->vkFreeMemory(_device->device(), memory, _device->allocation_cb());
				return util::to_error(result);
			}

			resource->memory = memory;
			resource->size = memory_req.size;
			resource->offset = 0;

			return err_ok;
		}
		else
		{
			auto& heap = _sampled_heap; //find heap

			VkDeviceSize offset;
			err = heap.vallocator->alloc(offset, memory_req.size, memory_req.alignment);
			if (err)
				return err;

			result = _device->vtable()->vkBindImageMemory(_device->device(), resource->image, heap.memory, offset);
			if (result)
			{
				heap.vallocator->free(offset, memory_req.size);
				return util::to_error(result);
			}

			resource->vallocator = heap.vallocator;
			resource->size = memory_req.size;
			resource->offset = offset;

			return err_ok;
		}
	}

	//--------------------------------------------------
	void		vk_texture_manager_t::free(texture_t* resource)
	{
		_garbage.push_back(resource);
	}

	//--------------------------------------------------
	void		vk_texture_manager_t::_free(texture_t* resource)
	{
		_device->vtable()->vkDestroyImage(_device->device(), resource->image, _device->allocation_cb());

		if (_is_individually_allocated(resource))
		{
			_device->vtable()->vkFreeMemory(_device->device(), resource->memory, _device->allocation_cb());
		}
		else
		{
			resource->vallocator->free(resource->offset, resource->size);
		}

		_resource_allocator.free(resource);
	}

	//////////////////////////////////////////////////
	// BUFFER MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	vk_buffer_manager_t::vk_buffer_manager_t(vk_device_t* device, allocator_t* allocator)
		: _device(device)
		, _frame_finished_id(0)
		, _resource_allocator(allocator)
	{
		for (auto& heap : _heap_array)
		{
			heap.vallocator = nullptr;
			heap.memory = VK_NULL_HANDLE;
			heap.size = 0;
			heap.type = 0;
			heap.buffer = VK_NULL_HANDLE;
		}		
	}

	//--------------------------------------------------
	VkResult	vk_buffer_manager_t::_init_heap(vk_memory_heap_t& heap, vk_heap_create_t const& create, VkBufferUsageFlags usage, VkDeviceSize align)
	{
		VkResult result;

		VkBufferCreateInfo buffer_create;
		buffer_create.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buffer_create.pNext = nullptr;
		buffer_create.flags = 0;
		buffer_create.size = create.size;
		buffer_create.usage = usage;
		buffer_create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		buffer_create.queueFamilyIndexCount = 0;
		buffer_create.pQueueFamilyIndices = nullptr;

		VkBuffer buffer;
		result = _device->vtable()->vkCreateBuffer(_device->device(), &buffer_create, _device->allocation_cb(), &buffer);
		if (result)
			return result;

		VkMemoryRequirements memory_req;
		_device->vtable()->vkGetBufferMemoryRequirements(_device->device(), buffer, &memory_req);

		uint32_t memory_type;
		VkMemoryPropertyFlags const memory_flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		if (!util::find_memory_index(_device->memory_properties(), memory_flags, memory_req.memoryTypeBits, memory_type))
		{
			_device->vtable()->vkDestroyBuffer(_device->device(), buffer, _device->allocation_cb());
			return VK_ERROR_OUT_OF_DEVICE_MEMORY;
		}

		VkMemoryAllocateInfo memory_alloc;
		memory_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memory_alloc.pNext = nullptr;
		memory_alloc.allocationSize = create.size;
		memory_alloc.memoryTypeIndex = memory_type;

		VkDeviceMemory memory;
		result = _device->vtable()->vkAllocateMemory(_device->device(), &memory_alloc, _device->allocation_cb(), &memory);
		if (result)
		{
			_device->vtable()->vkDestroyBuffer(_device->device(), buffer, _device->allocation_cb());
			return result;
		}

		result = _device->vtable()->vkBindBufferMemory(_device->device(), buffer, memory, 0);
		if (result)
		{
			_device->vtable()->vkFreeMemory(_device->device(), memory, _device->allocation_cb());
			_device->vtable()->vkDestroyBuffer(_device->device(), buffer, _device->allocation_cb());
			return result;
		}

		heap.memory = memory;
		heap.size = create.size;
		heap.type = memory_type;
		heap.vallocator = create.vallocator;
		heap.buffer = buffer;
		heap.align = align;

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_buffer_manager_t::_clean_heap(vk_memory_heap_t& heap)
	{
		_device->vtable()->vkDestroyBuffer(_device->device(), heap.buffer, _device->allocation_cb());
		_device->vtable()->vkFreeMemory(_device->device(), heap.memory, _device->allocation_cb());

		heap.vallocator = nullptr;
		heap.memory = VK_NULL_HANDLE;
		heap.size = 0;
		heap.type = 0;
		heap.buffer = VK_NULL_HANDLE;
	}

	//--------------------------------------------------
	VkResult	vk_buffer_manager_t::init(vk_heap_create_t const& uniform_create, vk_heap_create_t const& vertex_create, vk_heap_create_t const& index_create)
	{
		VkResult result;

		result = _init_heap(_heap_array[_buffer_uniform_heap], uniform_create, _usage_uniform, _device->properties().limits.minUniformBufferOffsetAlignment);
		if (result)
			return result;
		result = _init_heap(_heap_array[_buffer_vertex_heap], vertex_create, _usage_vertex, 8);
		if (result)
		{
			_clean_heap(_heap_array[_buffer_uniform_heap]);
			return result;
		}
		result = _init_heap(_heap_array[_buffer_index_heap], index_create, _usage_index, 8);
		if (result)
		{
			_clean_heap(_heap_array[_buffer_vertex_heap]);
			_clean_heap(_heap_array[_buffer_uniform_heap]);
			return result;
		}
			
		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_buffer_manager_t::clean()
	{
		_clean_heap(_heap_array[_buffer_index_heap]);
		_clean_heap(_heap_array[_buffer_vertex_heap]);
		_clean_heap(_heap_array[_buffer_uniform_heap]);
	}

	//--------------------------------------------------
	vk_buffer_manager_t::vk_memory_heap_t*	vk_buffer_manager_t::_find_heap(VkBufferUsageFlags usage)
	{
		switch (usage)
		{
		case _usage_uniform:
			return &_heap_array[_buffer_uniform_heap];
		case _usage_vertex:
			return &_heap_array[_buffer_vertex_heap];
		case _usage_index:
			return &_heap_array[_buffer_index_heap];
		default:
			return nullptr;
		}
	}

	//--------------------------------------------------
	void		vk_buffer_manager_t::_update(uint64_t frame_finished_id)
	{
		vk_resource_t* begin;
		vk_resource_t* end;

		_garbage.collect(frame_finished_id, begin, end);
		while (begin != end)
		{
			auto* resource = static_cast<buffer_t*>(begin);
			begin = begin->next;
			_free(resource);
		}

		_unbound.collect(frame_finished_id, _allocating);
	}

	//--------------------------------------------------
	void		vk_buffer_manager_t::update(uint64_t frame_submited_id, uint64_t frame_finished_id, device_event_t* result_array, size_t result_capacity, size_t& result_size)
	{
		while (_frame_finished_id != frame_finished_id)
		{
			++_frame_finished_id;
			_update(_frame_finished_id);
		}

		_garbage.update(frame_submited_id);
		_unbound.update(frame_submited_id);
		if (frame_finished_id == frame_submited_id)
			_update(_frame_finished_id);


		buffer_t* resource;
		result_size = 0;
		while ((result_capacity > result_size) && (nullptr != (resource = static_cast<buffer_t*>(_allocating.pop_front()))))
		{
			auto& result = result_array[result_size];
			result.type = device_event_type_buffer_create;
			result.buffer.user_data = resource->user_data;
			result.buffer.error = _alloc(resource);

			if (result.buffer.error)
			{
				_resource_allocator.free(resource);
				result.buffer.buffer = nullptr;
			}
			else
			{
				result.buffer.buffer = resource;
			}

			++result_size;
		}
	}


	//--------------------------------------------------
	error_t		vk_buffer_manager_t::_alloc(buffer_t* resource)
	{
		error_t err;
	
		auto* heap = _find_heap(resource->usage);
		
		VkDeviceSize offset;
		err = heap->vallocator->alloc(offset, resource->size, heap->align);
		if (err)
			return err;

		resource->buffer = heap->buffer;
		resource->vallocator = heap->vallocator;
		resource->offset = offset;

		return err_ok;		
	}

	//--------------------------------------------------
	VkResult	vk_buffer_manager_t::alloc(void* user_data, VkBufferCreateInfo const& vk_info, VkPipelineStageFlags stage, VkShaderStageFlags shader)
	{
		VkResult result;

		if (nullptr == _find_heap(vk_info.usage))
			return VK_ERROR_FEATURE_NOT_PRESENT;

		buffer_t* resource;
		result = _resource_allocator.alloc(resource);
		if (result)
			return result;

		//init resource
		resource->buffer = VK_NULL_HANDLE;
		resource->usage = vk_info.usage;
		resource->stage = stage;
		resource->shader = shader;

		resource->user_data = user_data;
		resource->offset = 0;
		resource->size = vk_info.size;

		_unbound.push_back(resource);

		return VK_SUCCESS;
	}

	//--------------------------------------------------
	void		vk_buffer_manager_t::_free(buffer_t* resource)
	{
		resource->vallocator->free(resource->offset, resource->size);
		_resource_allocator.free(resource);
	}

	//--------------------------------------------------
	void		vk_buffer_manager_t::free(buffer_t* resource)
	{
		_garbage.push_back(resource);
	}

	/*vk_pool_allocator_t::vk_pool_allocator_t(allocator_t* allocator, memory_info_t const& struct_info, size_t page_size)
		: _allocator(allocator)
		, _free_list(nullptr)
		, _page_array(allocator)
		, _page_size(0)
		, _alloc_size(0)
	{
		_page_size = page_size;
		_struct_info = struct_info;
		_struct_info.size = m::max(_struct_info.size, sizeof(list_t));
		_struct_info.align = m::max(_struct_info.align, alignof(list_t));
		_struct_info.size = u::size_align(_struct_info.size, _struct_info.align);
	}

	vk_pool_allocator_t::~vk_pool_allocator_t()
	{
		O_ASSERT(0 == _alloc_size);

		for (auto it : _page_array)
		{
			_allocator->free(it, _page_size * _struct_info.size);
		}
	}

	error_t		vk_pool_allocator_t::alloc(void*& mem)
	{
		error_t err;

		if (_free_list)
		{
			mem = _free_list;
			_free_list = _free_list->next;
			++_alloc_size;
			return err_ok;
		}

		mem = _allocator->alloc(_page_size * _struct_info.size, _struct_info.align);
		if (nullptr == mem)
			return err_out_of_memory;
		
		err = _page_array.push_back(mem);
		if (err)
		{
			_allocator->free(mem, _page_size * _struct_info.size);
			mem = nullptr;
			return err;
		}

		byte_t* list_mem = reinterpret_cast<byte_t*>(mem);
		list_mem += _struct_info.size;

		for (size_t i = 1; i < _page_size; ++i)
		{
			list_t* list = reinterpret_cast<list_t*>(list_mem);
			list->next = _free_list;
			_free_list = list;
			list_mem += _struct_info.size;
		}

		++_alloc_size;
		return err_ok;
	}

	void		vk_pool_allocator_t::free(void* mem)
	{
		list_t* list = reinterpret_cast<list_t*>(mem);
		list->next = _free_list;
		_free_list = list;

		--_alloc_size;
	}*/

} }
