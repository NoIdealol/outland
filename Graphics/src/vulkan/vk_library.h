#pragma once

#include "vk_utility.h"

#include "system\application\library.h"

//////////////////////////////////////////////////
// LIBRARY
//////////////////////////////////////////////////

#define VK_FNC_LIBRARY(DO_MACRO) \
	DO_MACRO(vkEnumerateInstanceVersion) \
	DO_MACRO(vkEnumerateInstanceExtensionProperties) \
	DO_MACRO(vkEnumerateInstanceLayerProperties) \
	DO_MACRO(vkCreateInstance)

namespace outland { namespace gpu
{
	struct vk_library_vtable_t
	{
		PFN_vkGetInstanceProcAddr	vkGetInstanceProcAddr;
		VK_FNC_LIBRARY(VK_FNC_PROTOTYPE)
	};

	class vk_library_t
	{
		os::library_id		_library;
		vk_library_vtable_t	_vtable;

	public:
		VkResult					load();
		void						free();
		vk_library_vtable_t const*	vtable() const { return &_vtable; }
		VkResult					check_instance_extensions(char8_t const* const* extension_array, size_t extension_size, scratch_allocator_t* scratch);
		VkResult					check_instance_layers(char8_t const* const* layer_array, size_t layer_size, scratch_allocator_t* scratch);
	};
} }
