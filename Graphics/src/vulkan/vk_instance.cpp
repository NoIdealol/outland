#include "pch.h"
#include "vk_instance.h"
#include "surface_impl.h"
#include "system\debug\output.h"

#define VK_INSTANCE_LOAD(fnc) \
	if (nullptr == (_vtable.fnc = reinterpret_cast<PFN_##fnc>(_library->vtable()->vkGetInstanceProcAddr(_instance, #fnc)))) \
	{ \
		return VK_ERROR_INITIALIZATION_FAILED; \
	}

namespace outland { namespace gpu
{
	vk_instance_t::vk_instance_t(vk_library_t* library, VkAllocationCallbacks const* allocation_cb)
		: _library(library)
		, _allocation_cb(allocation_cb)
		, _instance(VK_NULL_HANDLE)
		, _debug(VK_NULL_HANDLE)
	{
		u::mem_clr(&_vtable, sizeof(_vtable));
	}

	VkResult vk_instance_t::_load(module_create_mask_t create_mask)
	{
		VK_FNC_INSTANCE(VK_INSTANCE_LOAD);

		if (create_mask & module_create_debug)
		{
			VK_FNC_INSTANCE_EXT_DEBUG(VK_INSTANCE_LOAD);
		}

		return VK_SUCCESS;
	}

	VkResult vk_instance_t::_create(module_create_mask_t create_mask, char8_t const* const* extension_array, size_t extension_size, char8_t const* const* layer_array, size_t layer_size)
	{
		VkResult vk_result;

		VkApplicationInfo application_info =
		{
			VK_STRUCTURE_TYPE_APPLICATION_INFO,				// VkStructureType            sType
			nullptr,										// const void                *pNext
			"Outland game",									// const char                *pApplicationName
			VK_MAKE_VERSION(1, 0, 0),						// uint32_t                   applicationVersion
			"Outland engine",								// const char                *pEngineName
			VK_MAKE_VERSION(1, 0, 0),						// uint32_t                   engineVersion
			VK_API_VERSION_1_0								// uint32_t                   apiVersion
		};


		VkInstanceCreateInfo create_info;
		create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		create_info.pNext = NULL; //extensions, not using
		create_info.flags = 0; //must be 0
		create_info.pApplicationInfo = &application_info;
		create_info.enabledLayerCount = static_cast<uint32_t>(layer_size);
		create_info.ppEnabledLayerNames = layer_array;
		create_info.enabledExtensionCount = static_cast<uint32_t>(extension_size);
		create_info.ppEnabledExtensionNames = extension_array;

		vk_result = _library->vtable()->vkCreateInstance(
			&create_info,
			_allocation_cb,
			&_instance);

		if (vk_result)
			return vk_result;

		_vtable.vkDestroyInstance = nullptr; //clear ptr
		vk_result = _load(create_mask);
		if (vk_result)
		{
			//avoid crashing, we may have a memory leak if this ptr is not set, nothing we can do about it
			if (nullptr != _vtable.vkDestroyInstance)
			{
				_vtable.vkDestroyInstance(_instance, _allocation_cb);
				_instance = VK_NULL_HANDLE;
			}
			return vk_result;
		}

		return VK_SUCCESS;
	}

	VkResult vk_instance_t::_register_debug_callback()
	{
		VkResult result;
		VkDebugUtilsMessengerCreateInfoEXT create_info;
		create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		create_info.pNext = nullptr;
		create_info.flags = 0; //reserved for future use
		create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		create_info.pfnUserCallback = &_debug_callback;
		create_info.pUserData = this;

		result = _vtable.vkCreateDebugUtilsMessengerEXT(_instance, &create_info, _allocation_cb, &_debug);
		if (result)
			return result;
	
		return VK_SUCCESS;
	}

	VkResult vk_instance_t::init(module_create_mask_t create_mask, scratch_allocator_t* scratch)
	{
		VkResult vk_result = VK_SUCCESS;
		scratch_lock_t scratch_lock(scratch);

		array<char8_t const*> extension_array = { scratch };
		array<char8_t const*> layer_array = { scratch };

	#if defined(VK_USE_SWAP_CHAIN_EXTENSION)
		if (extension_array.push_back(VK_KHR_SURFACE_EXTENSION_NAME))
			return VK_ERROR_OUT_OF_HOST_MEMORY;
		if (extension_array.push_back(VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME))
			return VK_ERROR_OUT_OF_HOST_MEMORY;
	#endif
		
		if (create_mask & module_create_debug)
		{
			if (layer_array.push_back("VK_LAYER_LUNARG_standard_validation"))
				return VK_ERROR_OUT_OF_HOST_MEMORY;
			if (extension_array.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME))
				return VK_ERROR_OUT_OF_HOST_MEMORY;
		}

		vk_result = _library->check_instance_extensions(extension_array.data(), extension_array.size(), scratch);
		if (vk_result)
			return vk_result;

		vk_result = _library->check_instance_layers(layer_array.data(), layer_array.size(), scratch);
		if (vk_result)
			return vk_result;

		vk_result = _create(create_mask, extension_array.data(), extension_array.size(), layer_array.data(), layer_array.size());
		if (vk_result)
			return vk_result;

		if (create_mask & module_create_debug)
		{
			vk_result = _register_debug_callback();
			if (vk_result)
			{
				clean();
				return vk_result;
			}
		}

		return VK_SUCCESS;
	}

	void vk_instance_t::clean()
	{
		if (VK_NULL_HANDLE != _instance)
		{
			_vtable.vkDestroyInstance(_instance, _allocation_cb);
			_instance = VK_NULL_HANDLE;
		}
		
		if (VK_NULL_HANDLE != _debug)
		{
			_vtable.vkDestroyDebugUtilsMessengerEXT(_instance, _debug, _allocation_cb);
			_debug = VK_NULL_HANDLE;
		}
	}

	VkResult vk_instance_t::get_device_extension_array(array<char8_t const*>& extension_array)
	{
		extension_array.clear();

#if defined(VK_USE_SWAP_CHAIN_EXTENSION)
		if (extension_array.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME))
			return VK_ERROR_OUT_OF_HOST_MEMORY;
#endif

		return VK_SUCCESS;
	}

	VkResult	vk_instance_t::enumerate_physical_devices(array<VkPhysicalDevice>& device_array)
	{
		VkResult vk_result;
		uint32_t physical_device_size = 0;
		
		vk_result = _vtable.vkEnumeratePhysicalDevices(_instance, &physical_device_size, nullptr);
		if (vk_result)
			return vk_result;

		if (device_array.resize(physical_device_size))
			return VK_ERROR_OUT_OF_HOST_MEMORY;
			
		vk_result = _vtable.vkEnumeratePhysicalDevices(_instance, &physical_device_size, device_array.data());
		if (vk_result)
			return vk_result;

		device_array.resize(physical_device_size);

		return VK_SUCCESS;
	}

	VkResult	vk_instance_t::enumerate_device_queue_props(VkPhysicalDevice physical_device, array<VkQueueFamilyProperties>& props_array)
	{
		uint32_t queue_family_properties_size = 0;
		
		_vtable.vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_properties_size, nullptr);
		if (props_array.resize(queue_family_properties_size))
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		_vtable.vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_properties_size, props_array.data());
		props_array.resize(queue_family_properties_size);

		return VK_SUCCESS;
	}

	VkResult	vk_instance_t::check_device_extensions(VkPhysicalDevice physical_device, char8_t const* const* extension_array, size_t extension_size, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock(scratch);

		VkResult vk_result;

		VkExtensionProperties* extension_properties_array = nullptr;
		uint32_t extension_properties_size = 0;
		{
			vk_result = _vtable.vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &extension_properties_size, extension_properties_array);
			if (vk_result)
				return vk_result;

			error_t err = u::mem_alloc<VkExtensionProperties>(scratch, extension_properties_array, extension_properties_size);
			if (err)
				return VK_ERROR_OUT_OF_HOST_MEMORY;

			vk_result = _vtable.vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &extension_properties_size, extension_properties_array);
			if (vk_result != VK_SUCCESS && vk_result != VK_INCOMPLETE)
				return vk_result;
		}

		for (size_t i = 0; i < extension_size; ++i)
		{
			bool is_found = false;
			for (uint32_t j = 0; j < extension_properties_size; ++j)
			{
				if (u::str_cmp(extension_properties_array[j].extensionName, extension_array[i]))
				{
					is_found = true;
					break;
				}
			}

			if (!is_found)
				return VK_ERROR_EXTENSION_NOT_PRESENT;
		}

		return VK_SUCCESS;
	}

	VkResult	vk_instance_t::check_device_capabilities(VkPhysicalDeviceProperties const& device_properties)
	{
		uint32_t major_version = VK_VERSION_MAJOR(device_properties.apiVersion);
		uint32_t minor_version = VK_VERSION_MINOR(device_properties.apiVersion);
		uint32_t patch_version = VK_VERSION_PATCH(device_properties.apiVersion);

		return VK_SUCCESS;
	}

	VkResult	vk_instance_t::check_device_queues(
		VkPhysicalDevice physical_device,
		uint32_t& graphics_queue_family_index,
		uint32_t& copy_queue_family_index,
		scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock(scratch);

		VkResult vk_result;

		array<VkQueueFamilyProperties> family_props_array = { scratch };
		vk_result = enumerate_device_queue_props(physical_device, family_props_array);
		if (vk_result)
			return vk_result;

		graphics_queue_family_index = UINT32_MAX;
		copy_queue_family_index = UINT32_MAX;

		for (uint32_t i = 0; i < family_props_array.size(); ++i)
		{
			if ((family_props_array[i].queueCount > 0) &&
				(family_props_array[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))
			{
				// Select first queue that supports graphics
				if (UINT32_MAX == graphics_queue_family_index)
				{
					graphics_queue_family_index = i;
				}
			}

			if ((family_props_array[i].queueCount > 0) &&
				(VK_QUEUE_TRANSFER_BIT == family_props_array[i].queueFlags))
			{
				// Select first queue thats a dedicated copy queue
				if (UINT32_MAX == copy_queue_family_index)
				{
					copy_queue_family_index = i;
				}
			}
		}

		//we must have a grpahics queue
		if (UINT32_MAX == graphics_queue_family_index)
			return VK_ERROR_FEATURE_NOT_PRESENT;

		//we must have a dedicated copy queue
		if (UINT32_MAX == copy_queue_family_index)
			return VK_ERROR_FEATURE_NOT_PRESENT;

		return VK_SUCCESS;
	}

	VKAPI_ATTR VkBool32 VKAPI_CALL vk_instance_t::_debug_callback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData)
	{
		os::debug_string("[VULKAN] : ");
		os::debug_string(pCallbackData->pMessage);
		os::debug_string("\n");
		//O_ASSERT(false);
		return VK_FALSE;
	}
} }

