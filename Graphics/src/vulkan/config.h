#pragma once

//////////////////////////////////////////////////
// CONFIG
//////////////////////////////////////////////////

//enable swap chain and surface
#define VK_USE_SWAP_CHAIN_EXTENSION

//////////////////////////////////////////////////
// MACROS
//////////////////////////////////////////////////

#define VK_FNC_PROTOTYPE(fnc) PFN_##fnc		fnc;

namespace outland { namespace gpu 
{

	namespace c
	{
		enum : uint32_t
		{
			//slot count
			max_uniform_size = 4,
			max_transform_size = 4,
			max_descriptor_set_size = 8,
			max_vertex_buffer_size = 8,
			max_descriptor_layout_desc_size = 8,

			//----
			//frame allocator
			cmd_heap_page_power = 10 + 6,
			cmd_heap_page_min_count = 64,	//pow2
			max_cmd_allocator_size = 16,
			max_cmd_ctx_primary_size = 32,
			//frame buffering
			max_frame_size = 2, //pow2
			max_frame_modulo = max_frame_size - 1,
			max_swap_chain_size = 8,
			//garbage collection
			descriptor_pool_size = 4, //garbage granularity

			//--
			max_attachment_set_size = 16,
			max_subpass_size = 8,
		};
	}

} }
