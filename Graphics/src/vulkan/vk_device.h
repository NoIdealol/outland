#pragma once
#include "vk_instance.h"
#include "system\thread\mutex.h"
//#include "config.h"
//#include "vulkan.h"

//#include "device.h"
//#include "cmd_list_vk.h"
//#include "swap_chain_vk.h"

//////////////////////////////////////////////////
// DEVICE
//////////////////////////////////////////////////

#if defined(VK_USE_SWAP_CHAIN_EXTENSION)

	#define VK_FNC_DEVICE_SWAP_CHAIN(DO_MACRO) \
		DO_MACRO(vkCreateSwapchainKHR) \
		DO_MACRO(vkDestroySwapchainKHR) \
		DO_MACRO(vkGetSwapchainImagesKHR) \
		DO_MACRO(vkAcquireNextImageKHR) \
		DO_MACRO(vkQueuePresentKHR)

#else

	#define VK_FNC_DEVICE_SWAP_CHAIN(DO_MACRO)

#endif

//keep destroy first
#define VK_FNC_DEVICE_UNIVERSAL(DO_MACRO) \
	DO_MACRO(vkDestroyDevice) \
	DO_MACRO(vkGetDeviceQueue) \
	DO_MACRO(vkDeviceWaitIdle) \
	DO_MACRO(vkCreateSemaphore) \
	DO_MACRO(vkDestroySemaphore) \
	DO_MACRO(vkCreateFence) \
	DO_MACRO(vkDestroyFence) \
	DO_MACRO(vkWaitForFences) \
	DO_MACRO(vkResetFences) \
	DO_MACRO(vkCreateCommandPool) \
	DO_MACRO(vkDestroyCommandPool) \
	DO_MACRO(vkAllocateCommandBuffers) \
	DO_MACRO(vkFreeCommandBuffers) \
	DO_MACRO(vkQueueSubmit) \
	DO_MACRO(vkCreateRenderPass) \
	DO_MACRO(vkDestroyRenderPass) \
	DO_MACRO(vkCreateFramebuffer) \
	DO_MACRO(vkDestroyFramebuffer) \
	DO_MACRO(vkCreateImageView) \
	DO_MACRO(vkDestroyImageView) \
	DO_MACRO(vkCreateDescriptorSetLayout) \
	DO_MACRO(vkDestroyDescriptorSetLayout) \
	DO_MACRO(vkCreateDescriptorPool) \
	DO_MACRO(vkDestroyDescriptorPool) \
	DO_MACRO(vkResetDescriptorPool) \
	DO_MACRO(vkAllocateDescriptorSets) \
	DO_MACRO(vkFreeDescriptorSets) \
	DO_MACRO(vkUpdateDescriptorSets) \
	DO_MACRO(vkCreateShaderModule) \
	DO_MACRO(vkDestroyShaderModule) \
	DO_MACRO(vkCreateGraphicsPipelines) \
	DO_MACRO(vkDestroyPipeline) \
	DO_MACRO(vkCreatePipelineLayout) \
	DO_MACRO(vkDestroyPipelineLayout) \
	DO_MACRO(vkAllocateMemory) \
	DO_MACRO(vkFreeMemory) \
	DO_MACRO(vkMapMemory) \
	DO_MACRO(vkUnmapMemory) \
	DO_MACRO(vkFlushMappedMemoryRanges) \
	DO_MACRO(vkInvalidateMappedMemoryRanges) \
	DO_MACRO(vkCreateBuffer) \
	DO_MACRO(vkDestroyBuffer) \
	DO_MACRO(vkBindBufferMemory) \
	DO_MACRO(vkGetBufferMemoryRequirements) \
	DO_MACRO(vkCreateImage) \
	DO_MACRO(vkDestroyImage) \
	DO_MACRO(vkBindImageMemory) \
	DO_MACRO(vkGetImageMemoryRequirements) \
	DO_MACRO(vkCreateSampler) \
	DO_MACRO(vkDestroySampler);

#define VK_FNC_COMMAND_UNIVERSAL(DO_MACRO) \
	DO_MACRO(vkResetCommandPool) \
	DO_MACRO(vkBeginCommandBuffer) \
	DO_MACRO(vkEndCommandBuffer) \
	DO_MACRO(vkResetCommandBuffer) \
	DO_MACRO(vkCmdBeginRenderPass) \
	DO_MACRO(vkCmdNextSubpass) \
	DO_MACRO(vkCmdEndRenderPass) \
	DO_MACRO(vkCmdClearColorImage) \
	DO_MACRO(vkCmdBindPipeline) \
	DO_MACRO(vkCmdDraw) \
	DO_MACRO(vkCmdDrawIndexed) \
	DO_MACRO(vkCmdExecuteCommands) \
	DO_MACRO(vkCmdBindVertexBuffers) \
	DO_MACRO(vkCmdBindIndexBuffer) \
	DO_MACRO(vkCmdBindDescriptorSets) \
	DO_MACRO(vkCmdPushConstants) \
	DO_MACRO(vkCmdPipelineBarrier) \
	DO_MACRO(vkCmdCopyImage) \
	DO_MACRO(vkCmdCopyImageToBuffer) \
	DO_MACRO(vkCmdCopyBufferToImage) \
	DO_MACRO(vkCmdCopyBuffer) \
	;

#define VK_FNC_DEVICE(DO_MACRO) \
	VK_FNC_DEVICE_UNIVERSAL(DO_MACRO) \
	VK_FNC_DEVICE_SWAP_CHAIN(DO_MACRO)

#define VK_FNC_COMMAND(DO_MACRO) \
	VK_FNC_COMMAND_UNIVERSAL(DO_MACRO) \

namespace outland { namespace gpu
{
	struct vk_queue_t
	{
		VkQueueFamilyProperties	familty_properties;
		uint32_t				family_index;
		VkQueue					queue;
	};

	struct vk_device_vtable_t
	{
		VK_FNC_DEVICE(VK_FNC_PROTOTYPE)
	};

	struct vk_cmd_vtable_t
	{
		VK_FNC_COMMAND(VK_FNC_PROTOTYPE)
	};

	class vk_device_t
	{
		vk_device_vtable_t					_vtable;
		vk_cmd_vtable_t						_cmd_vtable;
		vk_instance_t*						_instance;
		VkDevice							_device;
		VkPhysicalDevice					_physical_device;
		VkPhysicalDeviceProperties			_device_properties;
		VkPhysicalDeviceFeatures			_device_features;
		VkPhysicalDeviceMemoryProperties	_memory_properties;
		VkAllocationCallbacks const*		_allocation_cb;
		array<vk_queue_t>					_queue_array;
		vk_queue_t*							_queue_graphics;
		vk_queue_t*							_queue_copy;
		os::mutex_id						_queue_lock;

	private:
		VkResult	_load();
		VkResult	_create(char8_t const* const* extension_array, size_t extension_size,
			uint32_t queue_graphics_index, uint32_t queue_copy_index, scratch_allocator_t* scratch);

	public:
		vk_device_t(vk_instance_t* instance, VkAllocationCallbacks const* allocation_cb, os::mutex_id queue_lock, allocator_t* allocator);
		allocator_t*					device_allocator() { return _queue_array.allocator(); }

		vk_instance_t*					instance() const { return _instance; }
		VkDevice						device() const { return _device; }
		vk_device_vtable_t const*		vtable() const { return &_vtable; }
		vk_cmd_vtable_t const*			cmd_vtable() const { return &_cmd_vtable; }
		array<vk_queue_t> const&		queue_array() const { return _queue_array; }
		vk_queue_t const*				queue_graphics() const { return _queue_graphics; }
		vk_queue_t const*				queue_copy() const { return _queue_copy; }
		VkPhysicalDevice				physical_device() const { return _physical_device; }
		VkAllocationCallbacks const*	allocation_cb() const { return _allocation_cb; }
		VkPhysicalDeviceProperties const&		properties() const { return _device_properties; }
		VkPhysicalDeviceFeatures const&			features() const { return _device_features; }
		VkPhysicalDeviceMemoryProperties const&	memory_properties() const { return _memory_properties; }
		os::mutex_id					queue_lock() const { return _queue_lock; }

		VkResult	init(VkPhysicalDevice physical_device, scratch_allocator_t* scratch);
		void		clean();

		VkResult	semaphore_create(VkSemaphore& semaphore); //created in unsignaled state
		void		semaphore_destroy(VkSemaphore semaphore);
		VkResult	fence_create(VkFence& fence, bool is_signaled);
		void		fence_destroy(VkFence fence);
	};

} }
