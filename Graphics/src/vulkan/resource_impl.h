#pragma once
//#include "vk_instance.h"
#include "vk_device.h"
#include "valloc\best_fit_vallocator.h"

namespace outland { namespace gpu 
{
	struct vk_resource_t
	{
		union
		{
			vk_resource_t*	next;
			uintptr_t		udata;
		};
	};

	struct render_pass_t : vk_resource_t
	{
		VkRenderPass	pass;
		uint32_t		render_target_size;
	};

	struct render_set_t : vk_resource_t
	{
		VkFramebuffer	framebuffer;
	};

	struct vk_descriptor_desc_t
	{
		uint32_t				binding;
		uint32_t				size;
		VkDescriptorType		type;
		VkShaderStageFlags		shader;
		VkImageLayout			layout;
	};

	struct descriptor_layout_t : vk_resource_t
	{
		VkDescriptorSetLayout	layout;
		uint32_t				desc_size;
		vk_descriptor_desc_t	desc_array[c::max_descriptor_layout_desc_size];
	};

	struct descriptor_set_t : vk_resource_t
	{
		VkDescriptorSet			set;
		descriptor_layout_t*	layout;
	};

	struct shader_t : vk_resource_t
	{
		VkShaderModule	shader;
	};

	struct input_layout_t : vk_resource_t
	{
		VkPushConstantRange		uniform_array[c::max_uniform_size];
		VkDescriptorSetLayout	transform_layout;
		VkDescriptorSet			transform_set;
		uint32_t				transform_size;
		uint32_t				uniform_size;		
	};

	struct pipeline_layout_t : input_layout_t
	{
		VkPipelineLayout	layout;
		uint32_t			descriptor_set_size;
	};

	struct pipeline_t : vk_resource_t
	{
		VkPipeline			pipeline;
		uint32_t			vertex_size;
	};

	struct sampler_t : vk_resource_t
	{
		VkSampler	sampler;
	};

	struct vk_texture_view_t : vk_resource_t
	{
		VkImageView		view;
		texture_t*		texture;
	};

	struct shader_view_t : vk_texture_view_t {};

	struct render_target_view_t : vk_texture_view_t {};

	struct depth_stencil_view_t : vk_texture_view_t {};

	struct texture_t : vk_resource_t
	{
		VkImage					image;
		VkImageType				image_type;
		VkFormat				format;
		VkExtent3D				extent;
		uint32_t				mip_levels;
		uint32_t				array_layers;
		VkImageUsageFlags		usage;
		VkPipelineStageFlags	stage;
		VkShaderStageFlags		shader;

		union
		{
			vallocator_t*			vallocator;
			void*					user_data;
			VkDeviceMemory			memory;
		};
		VkDeviceSize			offset;
		VkDeviceSize			size;
	};

	struct buffer_t : vk_resource_t
	{
		VkBuffer				buffer;
		VkBufferUsageFlags		usage;
		VkPipelineStageFlags	stage;
		VkShaderStageFlags		shader;

		union
		{
			vallocator_t*			vallocator;
			void*					user_data;
		};
		VkDeviceSize			offset;
		VkDeviceSize			size;
	};


	//////////////////////////////////////////////////
	// RESOURCE LIST
	//////////////////////////////////////////////////

	class vk_resource_list_t
	{
		vk_resource_t*	_begin;
		vk_resource_t*	_end;
	public:
		vk_resource_list_t();

		void			move_back(vk_resource_list_t& resource_list);
		void			push_back(vk_resource_t* resource);
		vk_resource_t*	pop_front();
		vk_resource_t*	begin();
		vk_resource_t*	end();
		void			clear();
	};

	//////////////////////////////////////////////////
	// RESOURCE ALLOCATOR
	//////////////////////////////////////////////////

	template<typename resource_t>
	class vk_resource_allocator_t
	{
		allocator_t*		_allocator;
	public:
		vk_resource_allocator_t(allocator_t* allocator);
		VkResult	alloc(resource_t*& resource);
		void		free(resource_t* resource);
	};

	//////////////////////////////////////////////////
	// RESOURCE QUEUE
	//////////////////////////////////////////////////

	class vk_resource_queue_t
	{
		vk_resource_list_t	_queue_array[c::max_frame_size];
		vk_resource_list_t	_queue_current;

	public:
		void push_back(vk_resource_t* resource);
		void update(uint64_t frame_submited_id);
		void collect(uint64_t frame_finished_id, vk_resource_t*& begin, vk_resource_t*& end);
		void collect(uint64_t frame_finished_id, vk_resource_list_t& list);
	};

	//////////////////////////////////////////////////
	// RENDER PASS MANAGER
	//////////////////////////////////////////////////

	class vk_render_pass_manager_t
	{
		vk_device_t*							_device;
		vk_resource_allocator_t<render_pass_t>	_resource_allocator;
		vk_resource_queue_t						_garbage;
		uint64_t								_frame_finished_id;

	private:
		void		_free(render_pass_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_render_pass_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(render_pass_t*& resource, VkRenderPassCreateInfo const& vk_info, uint32_t render_target_size);
		void		free(render_pass_t* resource);
	};

	//////////////////////////////////////////////////
	// RENDER SET MANAGER
	//////////////////////////////////////////////////

	class vk_render_set_manager_t
	{
		vk_device_t*								_device;
		vk_resource_allocator_t<render_set_t>	_resource_allocator;
		vk_resource_queue_t							_garbage;
		uint64_t									_frame_finished_id;

	private:
		void		_free(render_set_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_render_set_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(render_set_t*& resource, VkFramebufferCreateInfo const& vk_info);
		void		free(render_set_t* resource);
	};

	//////////////////////////////////////////////////
	// DESCRIPTOR LAYOUT MANAGER
	//////////////////////////////////////////////////

	class vk_descriptor_layout_manager_t
	{
		vk_device_t*									_device;
		vk_resource_allocator_t<descriptor_layout_t>	_resource_allocator;
		vk_resource_queue_t								_garbage;
		uint64_t										_frame_finished_id;

	private:
		void		_free(descriptor_layout_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_descriptor_layout_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(descriptor_layout_t*& resource, VkDescriptorSetLayoutCreateInfo const& vk_info, VkImageLayout const* vk_layout_array);
		void		free(descriptor_layout_t* resource);
	};

	//////////////////////////////////////////////////
	// DESCRIPTOR MANAGER
	//////////////////////////////////////////////////

	class vk_descriptor_manager_t
	{	
		enum : size_t
		{
			_pool_shift = 16,
			_set_mask = (1 << _pool_shift) - 1,
			_max_defrag_size = 32,
			_max_alloc_size = 32,

			_pool_index_sampler = 0,
			_pool_index_sampled_image,
			_pool_index_uniform_buffer,
			_pool_index_storage_buffer,
			_pool_index_input_attachment,
			_pool_types_size,

		};

		enum defrag_state_t
		{
			_defrag_state_none,
			_defrag_state_running,
			_defrag_state_swapping,
			_defrag_state_waiting,
		};

		struct pool_t
		{
			VkDescriptorPool	pool;
			uint32_t			type_capacity[_pool_types_size];
			uint32_t			type_allocated[_pool_types_size];
			uint32_t			set_capacity;
			uint32_t			set_allocated;
			descriptor_set_t**	set_array;
			size_t				set_size;
		};

		allocator_t*								_allocator;
		vk_device_t*								_device;
		vk_resource_allocator_t<descriptor_set_t>	_resource_allocator;
		pool_t										_descriptor_pool_array[c::descriptor_pool_size];
		size_t										_defrag_src;
		size_t										_defrag_dst;
		defrag_state_t				_defrag_state;
		uint64_t					_frame_id;
		uint64_t					_defrag_id;
		descriptor_set_t*			_defrag_set_array[_max_defrag_size];
		size_t						_defrag_set_size;
		VkDescriptorSet				_hotswap_set_array[_max_defrag_size];


	private:
		VkResult	_pool_reset(size_t pool_index);
		size_t		_pool_type_index(VkDescriptorType type);
		bool		_pool_capacity(size_t pool_index, descriptor_layout_t* const* layout_array, size_t layout_size);
		void		_delist_set(descriptor_set_t* set);
		void		_enlist_set(descriptor_set_t* set, size_t pool_index);
		void		_hotswap_sets(uint64_t defrag_id);
		VkResult	_defrag_swap(uint64_t defrag_finished_id);
		VkResult	_defrag_sets();
		VkResult	_defrag_begin();
		VkResult	_defrag_run(uint64_t frame_submited_id);
		VkResult	_defrag_reset(uint64_t frame_finished_id);
		VkResult	_alloc(
			descriptor_set_t** set_array,
			size_t set_size,
			descriptor_layout_t* const* layout_array,
			size_t pool_index
		);

	public:
		vk_descriptor_manager_t(vk_device_t* device, allocator_t* allocator);
		VkResult	init(VkDescriptorPoolSize const* pool_size_array, size_t pool_size_size, uint32_t max_sets);
		void		clean();

		void		hotswap(uint64_t defrag_id);
		VkResult	update(uint64_t frame_submited_id, uint64_t frame_finished_id, uint64_t defrag_finished_id, uint64_t& defrag_submission_id);
		VkResult	alloc(descriptor_set_t** set_array, size_t set_size, descriptor_layout_t* const* layout_array);
		void		free(descriptor_set_t* const* set_array, size_t set_size);
	};

	//////////////////////////////////////////////////
	// SHADER MANAGER
	//////////////////////////////////////////////////

	class vk_shader_manager_t
	{
		vk_device_t*							_device;
		vk_resource_allocator_t<shader_t>		_resource_allocator;

	public:
		vk_shader_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(shader_t*& resource, VkShaderModuleCreateInfo const& vk_info);
		void		free(shader_t* resource);
	};

	//////////////////////////////////////////////////
	// INPUT LAYOUT MANAGER
	//////////////////////////////////////////////////

	class vk_input_layout_manager_t
	{
		vk_device_t*									_device;
		vk_resource_allocator_t<input_layout_t>			_resource_allocator;
		vk_resource_queue_t								_garbage;
		uint64_t										_frame_finished_id;
		VkDescriptorPool								_pool;

	private:
		void		_free(input_layout_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_input_layout_manager_t(vk_device_t* device, allocator_t* allocator);
		VkResult	init(VkDescriptorPoolSize const& pool_size, uint32_t max_sets);
		void		clean();
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(input_layout_t*& resource, VkDescriptorSetLayoutCreateInfo const& vk_info, VkPushConstantRange const* vk_push_array, uint32_t vk_push_size);
		void		free(input_layout_t* resource);
	};

	//////////////////////////////////////////////////
	// PIPELINE LAYOUT MANAGER
	//////////////////////////////////////////////////

	class vk_pipeline_layout_manager_t
	{
		vk_device_t*									_device;
		vk_resource_allocator_t<pipeline_layout_t>		_resource_allocator;
		vk_resource_queue_t								_garbage;
		uint64_t										_frame_finished_id;

	private:
		void		_free(pipeline_layout_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_pipeline_layout_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(pipeline_layout_t*& resource, descriptor_layout_t* const* descriptor_layout_array, size_t descriptor_layout_size, input_layout_t const* input_layout);
		void		free(pipeline_layout_t* resource);
	};

	//////////////////////////////////////////////////
	// PIPELINE MANAGER
	//////////////////////////////////////////////////

	class vk_pipeline_manager_t
	{
		enum : size_t
		{
			_max_alloc_size = 32,
		};

		vk_device_t*							_device;
		vk_resource_allocator_t<pipeline_t>		_resource_allocator;
		vk_resource_queue_t						_garbage;
		uint64_t								_frame_finished_id;
		VkPipelineCache							_cache;

	private:
		VkResult	_alloc(
			pipeline_t** pipeline_array,
			size_t pipeline_size,
			VkGraphicsPipelineCreateInfo const* vk_info_array);
		void		_free(pipeline_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_pipeline_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(pipeline_t** pipeline_array, size_t pipeline_size, VkGraphicsPipelineCreateInfo const* vk_info_array);
		void		free(pipeline_t* const* pipeline_array, size_t pipeline_size);
	};

	//////////////////////////////////////////////////
	// SAMPLER MANAGER
	//////////////////////////////////////////////////

	class vk_sampler_manager_t
	{
		vk_device_t*						_device;
		vk_resource_allocator_t<sampler_t>	_resource_allocator;
		vk_resource_queue_t					_garbage;
		uint64_t							_frame_finished_id;

	private:
		void		_free(sampler_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_sampler_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(sampler_t*& resource, VkSamplerCreateInfo const& vk_info);
		void		free(sampler_t* resource);
	};

	//////////////////////////////////////////////////
	// VIEW MANAGER
	//////////////////////////////////////////////////

	class vk_view_manager_t
	{
		vk_device_t*					_device;
		allocator_t*					_resource_allocator;
		vk_resource_queue_t				_garbage;
		uint64_t						_frame_finished_id;

	private:
		VkResult	_alloc(vk_texture_view_t* resource, VkImageViewCreateInfo const& vk_info);
		void		_free(vk_texture_view_t* resource);
		void		_update(uint64_t frame_finished_id);

	public:
		vk_view_manager_t(vk_device_t* device, allocator_t* allocator);
		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id);
		VkResult	alloc(shader_view_t*& resource, VkImageViewCreateInfo const& vk_info, texture_t* texture);
		VkResult	alloc(render_target_view_t*& resource, VkImageViewCreateInfo const& vk_info, texture_t* texture);
		VkResult	alloc(depth_stencil_view_t*& resource, VkImageViewCreateInfo const& vk_info, texture_t* texture);
		void		free(vk_texture_view_t* resource);
	};

	//////////////////////////////////////////////////
	// TEXTURE MANAGER
	//////////////////////////////////////////////////

	struct vk_heap_create_t
	{
		VkDeviceSize	size;
		vallocator_t*	vallocator;
	};

	class vk_texture_manager_t
	{
		struct vk_memory_heap_t
		{
			VkDeviceMemory			memory;
			VkDeviceSize			size;
			uint32_t				type;
			vallocator_t*			vallocator;
		};

	private:
		vk_device_t*				_device;
		//depth stencil and render targets have individual allocations
		//color textures
		//best_fit_vallocator_t		_texture_sampled_vallocator;
		vk_memory_heap_t			_sampled_heap;

		vk_resource_queue_t			_garbage;
		vk_resource_queue_t			_unbound;
		vk_resource_list_t			_allocating;

		uint64_t					_frame_finished_id;
		vk_resource_allocator_t<texture_t>	_resource_allocator;
	private:
		error_t		_alloc(texture_t* resource);
		void		_free(texture_t* resource);
		void		_update(uint64_t frame_finished_id);
		bool		_is_individually_allocated(texture_t* resource);

	public:
		vk_texture_manager_t(vk_device_t* device, allocator_t* allocator);
		VkResult	init(vk_heap_create_t const& sampled_create);
		void		clean();

		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id, device_event_t* result_array, size_t result_capacity, size_t& result_size);
		VkResult	alloc(void* user_data, VkImageCreateInfo const& vk_info, VkPipelineStageFlags stage, VkShaderStageFlags shader);
		void		free(texture_t* resource);
	};

	//////////////////////////////////////////////////
	// BUFFER MANAGER
	//////////////////////////////////////////////////

	class vk_buffer_manager_t
	{
		enum : size_t
		{
			_buffer_uniform_heap,
			_buffer_vertex_heap,
			_buffer_index_heap,

			_buffer_heap_size,
		};

		enum : VkBufferUsageFlags
		{
			_usage_uniform = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			_usage_vertex = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			_usage_index = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		};

		//one giant buffer per memory
		struct vk_memory_heap_t
		{
			VkDeviceMemory		memory;
			VkDeviceSize		size;
			uint32_t			type;
			vallocator_t*		vallocator;
			VkBuffer			buffer;
			VkDeviceSize		align;
		};

	private:

		vk_device_t*				_device;
		vk_memory_heap_t			_heap_array[_buffer_heap_size];

		vk_resource_queue_t			_garbage;
		vk_resource_queue_t			_unbound;
		vk_resource_list_t			_allocating;

		uint64_t					_frame_finished_id;
		vk_resource_allocator_t<buffer_t>	_resource_allocator;

	private:
		error_t				_alloc(buffer_t* resource);
		void				_free(buffer_t* resource);
		void				_update(uint64_t frame_finished_id);
		vk_memory_heap_t*	_find_heap(VkBufferUsageFlags usage);
		VkResult			_init_heap(vk_memory_heap_t& heap, vk_heap_create_t const& create, VkBufferUsageFlags usage, VkDeviceSize align);
		void				_clean_heap(vk_memory_heap_t& heap);

	public:
		vk_buffer_manager_t(vk_device_t* device, allocator_t* allocator);
		VkResult	init(vk_heap_create_t const& uniform_create, vk_heap_create_t const& vertex_create, vk_heap_create_t const& index_create);
		void		clean();

		void		update(uint64_t frame_submited_id, uint64_t frame_finished_id, device_event_t* result_array, size_t result_capacity, size_t& result_size);
		VkResult	alloc(void* user_data, VkBufferCreateInfo const& vk_info, VkPipelineStageFlags stage, VkShaderStageFlags shader);
		void		free(buffer_t* resource);
	};



	/*class vk_pool_allocator_t
	{
		struct list_t
		{
			list_t* next;
		};

		allocator_t*	_allocator;
		list_t*			_free_list;
		array<void*>	_page_array;
		memory_info_t	_struct_info;
		size_t			_page_size;
		size_t			_alloc_size;

	public:
		vk_pool_allocator_t(allocator_t* allocator, memory_info_t const& struct_info, size_t page_size);
		~vk_pool_allocator_t();

		error_t		alloc(void*& mem);
		void		free(void* mem);
	};*/


} }
