#include "pch.h"
#include "device_impl.h"
#include "swap_chain_impl.h"


namespace outland { namespace gpu 
{

	//--------------------------------------------------
	device_t::device_t(vk_device_t* device, vk_frame_queue_t* frame_queue, allocator_t* allocator)
		: _device(device)
		, _allocator(allocator)
		, _frame_queue(frame_queue)
		, _cmd_heap(device)
		, _upload(_device)
		, _render_pass_manager(_device, allocator)
		, _render_set_manager(_device, allocator)
		, _descriptor_layout_manager(_device, allocator)
		, _descriptor_manager(_device, allocator)
		, _shader_manager(_device, allocator)
		, _input_layout_manager(_device, allocator)
		, _pipeline_layout_manager(_device, allocator)
		, _pipeline_manager(_device, allocator)
		, _sampler_manager(_device, allocator)
		, _view_manager(_device, allocator)
		, _texture_manager(_device, allocator)
		, _buffer_manager(_device, allocator)
		, _texture_sampled_vallocator(allocator)
		, _buffer_uniform_vallocator(allocator)
		, _buffer_vertex_vallocator(allocator)
		, _buffer_index_vallocator(allocator)
		, _max_transform_size(0)
	{
		_resource_frame.defrag_descriptor_submited = 0;

		_render_frame.defrag_descriptor_finished = 0;
		_render_frame.render_frame_submited = 0;
		_render_frame.render_frame_finished = 0;
	}

	//--------------------------------------------------
	error_t		device_t::init(VkPhysicalDevice physical_device, scratch_allocator_t* scratch)
	{
		error_t err;
		VkResult result;

		VkDeviceSize const minimal_maxUniformBufferRange = 16384; //same as cmd heap page size
		_max_transform_size = minimal_maxUniformBufferRange;
		size_t cmd_heap_size = 4 * 1024 * 1024; //total in flight
		VkDeviceSize const upload_buffer_size = 4 * 1024 * 1024; //per upload frame buffer

		//per pool
		uint32_t const			descriptor_heap_max_sets = 64; 
		VkDescriptorPoolSize	descriptor_heap_size_array[4];
		descriptor_heap_size_array[0].type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		descriptor_heap_size_array[0].descriptorCount = 4 * descriptor_heap_max_sets;
		descriptor_heap_size_array[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		descriptor_heap_size_array[1].descriptorCount = 1 * descriptor_heap_max_sets;
		descriptor_heap_size_array[2].type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		descriptor_heap_size_array[2].descriptorCount = 1 * descriptor_heap_max_sets;
		descriptor_heap_size_array[3].type = VK_DESCRIPTOR_TYPE_SAMPLER;
		descriptor_heap_size_array[3].descriptorCount = 64;
		
		//combined
		uint32_t const			input_heap_max_sets = 16;
		VkDescriptorPoolSize	input_heap_size;
		input_heap_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		input_heap_size.descriptorCount = 3 * input_heap_max_sets;

		//memory heaps
		VkDeviceSize const			texture_sampled_page_size = 16 * 1024;
		vk_heap_create_t			texture_sampled_heap;
		texture_sampled_heap.size = 16 * 1024 * 1024;
		texture_sampled_heap.vallocator = &_texture_sampled_vallocator;

		VkDeviceSize const			buffer_uniform_page_size = 1 * 256;
		vk_heap_create_t			buffer_uniform_heap;
		buffer_uniform_heap.size = 64 * 1024;
		buffer_uniform_heap.vallocator = &_buffer_uniform_vallocator;

		VkDeviceSize const			buffer_vertex_page_size = 4 * 1024;
		vk_heap_create_t			buffer_vertex_heap;
		buffer_vertex_heap.size = 1 * 1024 * 1024;
		buffer_vertex_heap.vallocator = &_buffer_vertex_vallocator;

		VkDeviceSize const			buffer_index_page_size = 1 * 1024;
		vk_heap_create_t			buffer_index_heap;
		buffer_index_heap.size = 64 * 1024;
		buffer_index_heap.vallocator = &_buffer_index_vallocator;

		//no cleaning required.
		err = _texture_sampled_vallocator.init(texture_sampled_heap.size, texture_sampled_page_size);
		if (err)
			return err;

		err = _buffer_uniform_vallocator.init(buffer_uniform_heap.size, buffer_uniform_page_size);
		if (err)
			return err;

		err = _buffer_vertex_vallocator.init(buffer_vertex_heap.size, buffer_vertex_page_size);
		if (err)
			return err;

		err = _buffer_index_vallocator.init(buffer_index_heap.size, buffer_index_page_size);
		if (err)
			return err;

		//goto cleaning
		result = _device->init(physical_device, scratch);
		if (result)
			goto _error_device;

		result = _cmd_heap.init(cmd_heap_size, _max_transform_size);
		if (result)
			goto _error_cmd_heap;

		result = _upload.init(upload_buffer_size);
		if (result)
			goto _error_upload;

		result = _descriptor_manager.init(descriptor_heap_size_array, u::array_size(descriptor_heap_size_array), descriptor_heap_max_sets);
		if (result)
			goto _error_descriptor_manager;

		result = _input_layout_manager.init(input_heap_size, input_heap_max_sets);
		if (result)
			goto _error_input_layout_manager;

		result = _texture_manager.init(texture_sampled_heap);
		if (result)
			goto _error_texture_manager;

		result = _buffer_manager.init(buffer_uniform_heap, buffer_vertex_heap, buffer_index_heap);
		if (result)
			goto _error_buffer_manager;

		return VK_SUCCESS;

		//_buffer_manager.clean();

	_error_buffer_manager:
		_texture_manager.clean();

	_error_texture_manager:
		_input_layout_manager.clean();

	_error_input_layout_manager:
		_descriptor_manager.clean();

	_error_descriptor_manager:
		_upload.clean();

	_error_upload:
		_cmd_heap.clean();

	_error_cmd_heap:
		_device->clean();

	_error_device:
		return util::to_error(result);
	}

	//--------------------------------------------------
	void		device_t::clean()
	{
		_buffer_manager.clean();
		_texture_manager.clean();
		_input_layout_manager.clean();
		_descriptor_manager.clean();
		_upload.clean();
		_cmd_heap.clean();
		_device->clean();
	}

	void	device::update(device_id device, device_event_t* event_array, size_t event_capacity, size_t& event_size)
	{
		VkResult vk_result;
		vk_render_frame_t render_frame;
		vk_resource_frame_t resource_frame;

		device->_frame_queue->resource_recv_msg(render_frame);
		
		device->_render_pass_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		device->_render_set_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		device->_descriptor_layout_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		vk_result = 
			device->_descriptor_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished,
			render_frame.defrag_descriptor_finished, resource_frame.defrag_descriptor_submited);
		O_ASSERT(VK_SUCCESS == vk_result);
		device->_shader_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		device->_input_layout_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		device->_pipeline_layout_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		device->_pipeline_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		device->_sampler_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);
		device->_view_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished);

		size_t texutre_event_size;
		device->_texture_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished, event_array, event_capacity, texutre_event_size);

		event_array += texutre_event_size;
		event_capacity -= texutre_event_size;

		size_t buffer_event_size;
		device->_buffer_manager.update(render_frame.render_frame_submited, render_frame.render_frame_finished, event_array, event_capacity, buffer_event_size);

		event_array += buffer_event_size;
		event_capacity -= buffer_event_size;

		size_t copy_event_size;
		vk_result =
			device->_upload.update(event_array, event_capacity, copy_event_size);
		O_ASSERT(VK_SUCCESS == vk_result);

		event_size = texutre_event_size + buffer_event_size + copy_event_size;

		device->_frame_queue->resource_send_msg(resource_frame);
	}

	//--------------------------------------------------
	error_t	device::swap_chain_create(device_id device, swap_chain_id& swap_chain, swap_chain_create_t const& create, scratch_allocator_t* scratch)
	{
		error_t err;
		err = u::mem_alloc(device->_allocator, swap_chain);
		if (err)
			return err;

		O_CREATE(swap_chain_t, swap_chain)(device->_device, create.surface);

		swap_chain_create_vk_t create_vk;
		create_vk.image_extent.width = create.back_buffer_width;
		create_vk.image_extent.height = create.back_buffer_height;
		create_vk.image_min_count = create.back_buffer_min_count;

		if (!util::to_present_mode(create.present_mode, create_vk.present_mode))
			return err_invalid_parameter;
		if (!util::to_format(create.back_buffer_format, create_vk.surface_format.format))
			return err_invalid_parameter;
		create_vk.surface_format.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;

		if (!util::to_access(create.back_buffer_release_state.access, create_vk.release_buffer_state.access_mask))
			return err_invalid_parameter;
		if (!util::to_stage(create.back_buffer_release_state.access, create_vk.release_buffer_state.stage_mask))
			return err_invalid_parameter;
		if (!util::to_layout(create.back_buffer_release_state.access, create_vk.release_buffer_state.layout))
			return err_invalid_parameter;

		if (!util::to_access(create.back_buffer_acquire_state.access, create_vk.acquire_buffer_state.access_mask))
			return err_invalid_parameter;
		if (!util::to_stage(create.back_buffer_acquire_state.access, create_vk.acquire_buffer_state.stage_mask))
			return err_invalid_parameter;
		if (!util::to_layout(create.back_buffer_acquire_state.access, create_vk.acquire_buffer_state.layout))
			return err_invalid_parameter;

		VkResult vk_result = swap_chain->init(create_vk, scratch);
		if (vk_result)
		{
			O_DESTROY(swap_chain_t, swap_chain);
			u::mem_free(device->_allocator, swap_chain);
			return util::to_error(vk_result);
		}

		//get first image!
		vk_result = swap_chain->wait_next_image();
		if (vk_result)
		{
			swap_chain->clean();
			O_DESTROY(swap_chain_t, swap_chain);
			u::mem_free(device->_allocator, swap_chain);
			return util::to_error(vk_result);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	device::swap_chain_destroy(device_id device, swap_chain_id swap_chain)
	{
		swap_chain->clean();
		O_DESTROY(swap_chain_t, swap_chain);
		u::mem_free(device->_allocator, swap_chain);
	}

	//--------------------------------------------------
	error_t	device::cmd_allocator_create(device_id device, cmd_allocator_id& allocator, cmd_allocator_create_t const& create, scratch_allocator_t* scratch)
	{
		error_t err;
		err = u::mem_alloc(device->_allocator, allocator);
		if (err)
			return err;

		O_CREATE(cmd_allocator_t, allocator)(&device->_cmd_heap, create.allocator);
		VkResult vk_result = allocator->init();
		if (vk_result)
		{
			O_DESTROY(cmd_allocator_t, allocator);
			u::mem_free(device->_allocator, allocator);
			return util::to_error(vk_result);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	device::cmd_allocator_destroy(device_id device, cmd_allocator_id allocator)
	{
		allocator->clean();
		O_DESTROY(cmd_allocator_t, allocator);
		u::mem_free(device->_allocator, allocator);
	}

	//--------------------------------------------------
	error_t	device::render_pass_create(device_id device, render_pass_id& pass, render_pass_create_t const& create, scratch_allocator_t* scratch)
	{
		scratch_lock_t lock = { scratch };

		error_t err;
		VkResult vk_result;

		//attachment
		array<VkAttachmentDescription> vk_att_desc_array = { scratch };
		err = vk_att_desc_array.resize(create.render_target_size + create.depth_stencil_size);
		if (err)
			return err;

		//render target att
		for (uint32_t i = 0; i < create.render_target_size; ++i)
		{
			auto& vad = vk_att_desc_array[i];
			auto& rta = create.render_target_array[i];

			vad.flags = 0; //may alias?
			vad.samples = VK_SAMPLE_COUNT_1_BIT;
			vad.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			vad.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

			if (!util::to_format(rta.format, vad.format))
				return err_invalid_parameter;

			if (!util::to_attachment_load(rta.color_load, vad.loadOp))
				return err_invalid_parameter;
			if (!util::to_attachment_store(rta.color_store, vad.storeOp))
				return err_invalid_parameter;

			if (!util::to_layout(rta.access_initial, vad.initialLayout))
				return err_invalid_parameter;
			if (!util::to_layout(rta.access_final, vad.finalLayout))
				return err_invalid_parameter;		
		}

		//depth stencil att
		for (uint32_t i = 0; i < create.depth_stencil_size; ++i)
		{
			auto& vad = vk_att_desc_array[create.render_target_size + i];
			auto& dsa = create.depth_stencil_array[i];

			vad.flags = 0; //may alias?
			vad.samples = VK_SAMPLE_COUNT_1_BIT;
			vad.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			vad.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

			if (!util::to_format(dsa.format, vad.format))
				return err_invalid_parameter;

			if (!util::to_attachment_load(dsa.depth_load, vad.loadOp))
				return err_invalid_parameter;
			if (!util::to_attachment_store(dsa.depth_store, vad.storeOp))
				return err_invalid_parameter;

			if (!util::to_attachment_load(dsa.stencil_load, vad.stencilLoadOp))
				return err_invalid_parameter;
			if (!util::to_attachment_store(dsa.stencil_store, vad.stencilStoreOp))
				return err_invalid_parameter;

			if (!util::to_layout(dsa.access_initial, vad.initialLayout))
				return err_invalid_parameter;
			if (!util::to_layout(dsa.access_final, vad.finalLayout))
				return err_invalid_parameter;
		}

		//subpass
		VkSubpassDescription* vk_sub_desc_array;
		err = u::mem_alloc(scratch, vk_sub_desc_array, create.subpass_size);
		if (err)
			return err;

		VkAttachmentReference vk_att_ref_array[c::max_attachment_set_size];
		uint32_t vk_att_ref_size;

		for (uint32_t i = 0; i < create.subpass_size; ++i)
		{
			auto& sub = create.subpass_array[i];
			auto& vk_sub = vk_sub_desc_array[i];
			vk_sub.flags = 0;
			vk_sub.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

			vk_att_ref_size = 0;

			//rt input att
			for (uint32_t j = 0; j < sub.render_target_size; ++j)
			{
				auto& rt = sub.render_target_array[j];
				VkPipelineStageFlags vk_stage;
				if (!util::to_stage(rt.access, vk_stage))
					return err_invalid_parameter;
				
				if (VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT & vk_stage)
				{
					VkAttachmentReference vk_att_ref;
					vk_att_ref.attachment = rt.index;
					if (!util::to_layout(rt.access, vk_att_ref.layout))
						return err_invalid_parameter;

					vk_att_ref_array[rt.slot] = vk_att_ref;
					++vk_att_ref_size;
				}
			}
			//ds input att
			for (uint32_t j = 0; j < sub.depth_stencil_size; ++j)
			{
				auto& ds = sub.depth_stencil_array[j];
				VkPipelineStageFlags vk_stage;
				if (!util::to_stage(ds.access, vk_stage))
					return err_invalid_parameter;

				if (VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT & vk_stage)
				{
					VkAttachmentReference vk_att_ref;
					vk_att_ref.attachment = create.render_target_size + ds.index;
					if (!util::to_layout(ds.access, vk_att_ref.layout))
						return err_invalid_parameter;

					vk_att_ref_array[ds.slot] = vk_att_ref;
					++vk_att_ref_size;
				}
			}

			//input copy
			{
				VkAttachmentReference* vk_input_att_array;
				err = u::mem_alloc(scratch, vk_input_att_array, vk_att_ref_size);
				if (err)
					return err;

				u::mem_copy(vk_input_att_array, vk_att_ref_array, vk_att_ref_size * sizeof(VkAttachmentReference));
				vk_sub.pInputAttachments = vk_input_att_array;
				vk_sub.inputAttachmentCount = vk_att_ref_size;
			}

			//color att
			vk_att_ref_size = 0;

			for (size_t j = 0; j < sub.render_target_size; ++j)
			{
				auto& rt = sub.render_target_array[j];
				VkPipelineStageFlags vk_stage;
				if (!util::to_stage(rt.access, vk_stage))
					return err_invalid_parameter;

				if (VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT == vk_stage)
				{
					VkAttachmentReference vk_att_ref;
					vk_att_ref.attachment = rt.index;
					if (!util::to_layout(rt.access, vk_att_ref.layout))
						return err_invalid_parameter;

					vk_att_ref_array[rt.slot] = vk_att_ref;
					++vk_att_ref_size;
				}
			}

			//color copy
			{
				VkAttachmentReference* vk_input_att_array;
				err = u::mem_alloc(scratch, vk_input_att_array, vk_att_ref_size);
				if (err)
					return err;

				u::mem_copy(vk_input_att_array, vk_att_ref_array, vk_att_ref_size * sizeof(VkAttachmentReference));
				vk_sub.pColorAttachments = vk_input_att_array;
				vk_sub.colorAttachmentCount = vk_att_ref_size;
				vk_sub.pResolveAttachments = nullptr;
			}
		
			//depth
			vk_att_ref_size = 0;

			for (uint32_t j = 0; j < sub.depth_stencil_size; ++j)
			{
				auto& ds = sub.depth_stencil_array[j];
				VkPipelineStageFlags vk_stage;
				if (!util::to_stage(ds.access, vk_stage))
					return err_invalid_parameter;

				if ((VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT) & vk_stage)
				{
					VkAttachmentReference vk_att_ref;
					vk_att_ref.attachment = create.render_target_size + ds.index;
					if (!util::to_layout(ds.access, vk_att_ref.layout))
						return err_invalid_parameter;

					//indexing so that we can see all in debug, slot ignored here
					vk_att_ref_array[vk_att_ref_size] = vk_att_ref;
					++vk_att_ref_size;
				}
			}

			//depth copy
			if(vk_att_ref_size)
			{
				VkAttachmentReference* vk_input_att_array;
				err = u::mem_alloc(scratch, vk_input_att_array, vk_att_ref_size);
				if (err)
					return err;

				u::mem_copy(vk_input_att_array, vk_att_ref_array, vk_att_ref_size * sizeof(VkAttachmentReference));
				vk_sub.pDepthStencilAttachment = vk_input_att_array;
				O_ASSERT(1 == vk_att_ref_size);			
			}
			else
			{
				vk_sub.pDepthStencilAttachment = nullptr;
			}


			//preserve
			{
				uint32_t* vk_preserve_att_array;
				err = u::mem_alloc(scratch, vk_preserve_att_array, sub.render_target_preserve_size + sub.depth_stencil_preserve_size);
				if (err)
					return err;

				for (uint32_t j = 0; j < sub.render_target_preserve_size; ++j)
				{
					vk_preserve_att_array[j] = sub.render_target_preserve_array[j];
				}

				for (uint32_t j = 0; j < sub.depth_stencil_preserve_size; ++j)
				{
					vk_preserve_att_array[sub.render_target_preserve_size + j] = create.render_target_size + sub.depth_stencil_preserve_array[j];
				}

				vk_sub.preserveAttachmentCount = sub.render_target_preserve_size + sub.depth_stencil_preserve_size;
				vk_sub.pPreserveAttachments = vk_preserve_att_array;
			}
		}

		//dependency
		struct vk_attachment_info_t
		{
			VkAccessFlags			access;
			VkPipelineStageFlags	stage;
			bool					is_used;
		};

		array<VkSubpassDependency> vk_sub_dep_array = { scratch };
		vk_attachment_info_t att_info[c::max_subpass_size][c::max_attachment_set_size] = { 0 };

		for (uint32_t i = 0; i < create.subpass_size; ++i)
		{
			auto& sub = create.subpass_array[i];

			//render targets
			for (uint32_t j = 0; j < sub.render_target_size; ++j)
			{
				auto& rt = sub.render_target_array[j];
				auto& atti = att_info[i][rt.index];
				
				if (!util::to_access(rt.access, atti.access))
					return err_invalid_parameter;
				if (!util::to_stage(rt.access, atti.stage))
					return err_invalid_parameter;
				atti.is_used = true;
			}
			//depth stencils
			for (uint32_t j = 0; j < sub.depth_stencil_size; ++j)
			{
				auto& ds = sub.depth_stencil_array[j];
				auto& atti = att_info[i][create.render_target_size + ds.index];
				//already checked for err
				if (!util::to_access(ds.access, atti.access))
					return err_invalid_parameter;
				if (!util::to_stage(ds.access, atti.stage))
					return err_invalid_parameter;
				atti.is_used = true;
			}

			//what sync to put in todo: place barriers???
			O_ASSERT(0 == sub.render_target_preserve_size);
			O_ASSERT(0 == sub.depth_stencil_preserve_size);
		}

		//internal dependencies
		for (uint32_t i = 0; i < create.subpass_size; ++i)
		{
			auto& sub_dst = create.subpass_array[i];
			for (uint32_t j = 0; j < sub_dst.subpass_dependency_size; ++j)
			{
				VkSubpassDependency vk_sub_dep;
				vk_sub_dep.srcSubpass = sub_dst.subpass_dependency_array[j];
				vk_sub_dep.dstSubpass = i;
				vk_sub_dep.srcStageMask = 0;
				vk_sub_dep.dstStageMask = 0;
				vk_sub_dep.srcAccessMask = 0;
				vk_sub_dep.dstAccessMask = 0;
				vk_sub_dep.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;


				auto& sub_src = create.subpass_array[vk_sub_dep.srcSubpass];
				//src
				for (uint32_t k = 0; k < sub_src.render_target_size; ++k)
				{
					auto& rt = sub_src.render_target_array[k];
					vk_sub_dep.srcStageMask |= att_info[vk_sub_dep.srcSubpass][rt.index].stage;
					vk_sub_dep.srcAccessMask |= att_info[vk_sub_dep.srcSubpass][rt.index].access;
				}

				for (uint32_t k = 0; k < sub_src.depth_stencil_size; ++k)
				{
					auto& ds = sub_src.depth_stencil_array[k];
					vk_sub_dep.srcStageMask |= att_info[vk_sub_dep.srcSubpass][create.render_target_size + ds.index].stage;
					vk_sub_dep.srcAccessMask |= att_info[vk_sub_dep.srcSubpass][create.render_target_size + ds.index].access;
				}
				//dst
				for (uint32_t k = 0; k < sub_src.render_target_size; ++k)
				{
					auto& rt = sub_src.render_target_array[k];
					vk_sub_dep.dstStageMask |= att_info[vk_sub_dep.dstSubpass][rt.index].stage;
					vk_sub_dep.dstAccessMask |= att_info[vk_sub_dep.dstSubpass][rt.index].access;
				}

				for (uint32_t k = 0; k < sub_src.depth_stencil_size; ++k)
				{
					auto& ds = sub_src.depth_stencil_array[k];
					vk_sub_dep.dstStageMask |= att_info[vk_sub_dep.dstSubpass][create.render_target_size + ds.index].stage;
					vk_sub_dep.dstAccessMask |= att_info[vk_sub_dep.dstSubpass][create.render_target_size + ds.index].access;
				}
			
				err = vk_sub_dep_array.push_back(vk_sub_dep);
				if (err)
					return err;
			}
		}

		//external dependencies
		{
			//src
			{
				bool is_used_array[c::max_attachment_set_size] = { 0 };
				for (uint32_t i = 0; i < create.subpass_size; ++i)
				{
					auto& sub_dst = create.subpass_array[i];
					bool is_external = false;

					VkSubpassDependency vk_sub_dep;
					vk_sub_dep.srcSubpass = VK_SUBPASS_EXTERNAL;
					vk_sub_dep.dstSubpass = i;
					vk_sub_dep.srcStageMask = 0;
					vk_sub_dep.dstStageMask = 0;
					vk_sub_dep.srcAccessMask = 0;
					vk_sub_dep.dstAccessMask = 0;
					vk_sub_dep.dependencyFlags = 0;

					for (uint32_t j = 0; j < sub_dst.render_target_size; ++j)
					{
						auto& rt = sub_dst.render_target_array[j];
						uint32_t rt_index = rt.index;

						if (VK_ATTACHMENT_LOAD_OP_LOAD == vk_att_desc_array[rt_index].loadOp &&
							att_info[i][rt_index].is_used &&
							!is_used_array[rt_index])
						{
							is_used_array[rt_index] = true;
							is_external = true;

							VkPipelineStageFlags stage_initial;
							VkAccessFlags access_initial;

							if (!util::to_stage(create.render_target_array[rt.index].access_initial, stage_initial))
								return err_invalid_parameter;
							if (!util::to_access(create.render_target_array[rt.index].access_initial, access_initial))
								return err_invalid_parameter;

							vk_sub_dep.srcStageMask |= stage_initial;
							vk_sub_dep.srcAccessMask |= access_initial;

							vk_sub_dep.dstStageMask |= att_info[i][rt_index].stage;
							vk_sub_dep.dstAccessMask |= att_info[i][rt_index].access;
						}
					}

					for (uint32_t j = 0; j < sub_dst.depth_stencil_size; ++j)
					{
						auto& ds = sub_dst.depth_stencil_array[j];
						uint32_t ds_index = create.render_target_size + ds.index;

						if ((VK_ATTACHMENT_LOAD_OP_LOAD == vk_att_desc_array[ds_index].loadOp ||
							VK_ATTACHMENT_LOAD_OP_LOAD == vk_att_desc_array[ds_index].stencilLoadOp) &&
							att_info[i][ds_index].is_used &&
							!is_used_array[ds_index])
						{
							is_used_array[ds_index] = true;
							is_external = true;

							VkPipelineStageFlags stage_initial;
							VkAccessFlags access_initial;

							if (!util::to_stage(create.depth_stencil_array[ds.index].access_initial, stage_initial))
								return err_invalid_parameter;
							if (!util::to_access(create.depth_stencil_array[ds.index].access_initial, access_initial))
								return err_invalid_parameter;

							vk_sub_dep.srcStageMask |= stage_initial;
							vk_sub_dep.srcAccessMask |= access_initial;

							vk_sub_dep.dstStageMask |= att_info[i][ds_index].stage;
							vk_sub_dep.dstAccessMask |= att_info[i][ds_index].access;
						}
					}

					if (is_external)
					{
						err = vk_sub_dep_array.push_back(vk_sub_dep);
						if (err)
							return err;
					}
				}
			}
			//dst
			{
				bool is_used_array[c::max_attachment_set_size] = { 0 };
				for (uint32_t i = 0; i < create.subpass_size; ++i)
				{
					auto& sub_src = create.subpass_array[i];
					bool is_external = false;

					VkSubpassDependency vk_sub_dep;
					vk_sub_dep.srcSubpass = i;
					vk_sub_dep.dstSubpass = VK_SUBPASS_EXTERNAL;
					vk_sub_dep.srcStageMask = 0;
					vk_sub_dep.dstStageMask = 0;
					vk_sub_dep.srcAccessMask = 0;
					vk_sub_dep.dstAccessMask = 0;
					vk_sub_dep.dependencyFlags = 0;

					for (uint32_t j = 0; j < sub_src.render_target_size; ++j)
					{
						auto& rt = sub_src.render_target_array[j];
						uint32_t rt_index = rt.index;

						if (VK_ATTACHMENT_STORE_OP_STORE == vk_att_desc_array[rt_index].storeOp &&
							att_info[i][rt_index].is_used &&
							!is_used_array[rt_index])
						{
							is_used_array[rt_index] = true;
							is_external = true;

							VkPipelineStageFlags stage_final;
							VkAccessFlags access_final;

							if (!util::to_stage(create.render_target_array[rt.index].access_final, stage_final))
								return err_invalid_parameter;
							if (!util::to_access(create.render_target_array[rt.index].access_final, access_final))
								return err_invalid_parameter;

							vk_sub_dep.dstStageMask |= stage_final;
							vk_sub_dep.dstAccessMask |= access_final;

							vk_sub_dep.srcStageMask |= att_info[i][rt_index].stage;
							vk_sub_dep.srcAccessMask |= att_info[i][rt_index].access;
						}
					}

					for (uint32_t j = 0; j < sub_src.depth_stencil_size; ++j)
					{
						auto& ds = sub_src.depth_stencil_array[j];
						uint32_t ds_index = create.render_target_size + ds.index;

						if ((VK_ATTACHMENT_STORE_OP_STORE == vk_att_desc_array[ds_index].storeOp ||
							VK_ATTACHMENT_STORE_OP_STORE == vk_att_desc_array[ds_index].stencilStoreOp) &&
							att_info[i][ds_index].is_used &&
							!is_used_array[ds_index])
						{
							is_used_array[ds_index] = true;
							is_external = true;

							VkPipelineStageFlags stage_final;
							VkAccessFlags access_final;

							if (!util::to_stage(create.depth_stencil_array[ds.index].access_final, stage_final))
								return err_invalid_parameter;
							if (!util::to_access(create.depth_stencil_array[ds.index].access_final, access_final))
								return err_invalid_parameter;

							vk_sub_dep.dstStageMask |= stage_final;
							vk_sub_dep.dstAccessMask |= access_final;

							vk_sub_dep.srcStageMask |= att_info[i][ds_index].stage;
							vk_sub_dep.srcAccessMask |= att_info[i][ds_index].access;
						}
					}

					if (is_external)
					{
						err = vk_sub_dep_array.push_back(vk_sub_dep);
						if (err)
							return err;
					}
				}
			}
		}

		//vk render pass
		VkRenderPassCreateInfo vk_ci;
		vk_ci.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		vk_ci.pNext = nullptr;
		vk_ci.flags = 0;
		vk_ci.attachmentCount = static_cast<uint32_t>(vk_att_desc_array.size());
		vk_ci.pAttachments = vk_att_desc_array.data();
		vk_ci.subpassCount = create.subpass_size;
		vk_ci.pSubpasses = vk_sub_desc_array;
		vk_ci.dependencyCount = static_cast<uint32_t>(vk_sub_dep_array.size());
		vk_ci.pDependencies = vk_sub_dep_array.data();

		vk_result = device->_render_pass_manager.alloc(pass, vk_ci, create.render_target_size);
		if (vk_result)
			return util::to_error(vk_result);
		
		return err_ok;
	}

	//--------------------------------------------------
	void	device::render_pass_destroy(device_id device, render_pass_id pass)
	{
		device->_render_pass_manager.free(pass);
	}

	//--------------------------------------------------
	error_t	device::render_set_create(device_id device, render_set_id& set, render_set_create_t const& create, scratch_allocator_t* scratch)
	{
		error_t err;
		VkResult vk_result;

		VkImageView	vk_view_array[c::max_attachment_set_size];
		O_ASSERT(u::array_size(vk_view_array) >= static_cast<size_t>(create.render_target_size + create.depth_stencil_size));

		for (uint32_t i = 0; i < create.render_target_size; ++i)
		{
			vk_view_array[i] = create.render_target_array[i]->view;
		}

		for (uint32_t i = 0; i < create.depth_stencil_size; ++i)
		{
			vk_view_array[create.render_target_size + i] = create.depth_stencil_array[i]->view;
		}

		err = u::mem_alloc(device->_allocator, set);
		if (err)
			return err;

		VkFramebufferCreateInfo vk_ci;
		vk_ci.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		vk_ci.pNext = nullptr;
		vk_ci.flags = 0;
		vk_ci.renderPass = create.render_pass->pass;
		vk_ci.attachmentCount = create.render_target_size + create.depth_stencil_size;
		vk_ci.pAttachments = vk_view_array;
		vk_ci.width = create.width;
		vk_ci.height = create.height;
		vk_ci.layers = 1;// create.layers;

		vk_result = device->_render_set_manager.alloc(set, vk_ci);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::render_set_destroy(device_id device, render_set_id set)
	{
		device->_render_set_manager.free(set);
	}

	//--------------------------------------------------
	error_t	device::descriptor_layout_create(device_id device, descriptor_layout_id& layout, descriptor_layout_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;
		VkDescriptorSetLayoutBinding vk_binding_array[c::max_descriptor_layout_desc_size];
		VkImageLayout vk_layout_array[c::max_descriptor_layout_desc_size];
		O_ASSERT(u::array_size(vk_binding_array) >= static_cast<size_t>(create.descriptor_size));

		for (uint32_t i = 0; i < create.descriptor_size; ++i)
		{
			auto& vk_layout = vk_layout_array[i];
			auto& vk_binding = vk_binding_array[i];
			auto& desc = create.descriptor_array[i];

			vk_binding.binding = desc.binding;
			vk_binding.descriptorCount = desc.size;
			vk_binding.pImmutableSamplers = nullptr;
			//vk_binding.stageFlags = 0;

			if (!util::to_descriptor_type(desc.type, vk_binding.descriptorType))
				return err_invalid_parameter;

			util::to_shader_stage(desc.stage_mask, vk_binding.stageFlags);

			if (!util::to_layout(desc.type, vk_layout))
				return err_invalid_parameter;
		}

		VkDescriptorSetLayoutCreateInfo vk_ci;
		vk_ci.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		vk_ci.pNext = nullptr;
		vk_ci.flags = 0;
		vk_ci.bindingCount = create.descriptor_size;
		vk_ci.pBindings = vk_binding_array;

		vk_result = device->_descriptor_layout_manager.alloc(layout, vk_ci, vk_layout_array);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::descriptor_layout_destroy(device_id device, descriptor_layout_id layout)
	{
		device->_descriptor_layout_manager.free(layout);
	}

	//--------------------------------------------------
	error_t	device::descriptor_set_create(device_id device, descriptor_set_id* set_array, descriptor_set_create_t const& create, scratch_allocator_t* scratch)
	{
		scratch_lock_t lock = { scratch };

		error_t err;
		VkResult vk_result;

		descriptor_layout_id* layout_array;
		err = u::mem_alloc(scratch, layout_array, create.state_size);
		if (err)
			return err;

		uint32_t write_size = 0;
		for (size_t i = 0; i < create.state_size; ++i)
		{
			layout_array[i] = create.state_array[i].layout;
			write_size += layout_array[i]->desc_size;
		}

		VkWriteDescriptorSet* vk_write_array;
		err = u::mem_alloc(scratch, vk_write_array, write_size);
		if (err)
			return err;

		VkWriteDescriptorSet* vk_write = vk_write_array;
		for (size_t i = 0; i < create.state_size; ++i)
		{
			auto* layout = layout_array[i];
			auto& state = create.state_array[i];

			uint32_t descriptor_size = 0;
			for (uint32_t j = 0; j < layout->desc_size; ++j)
			{
				auto& desc = layout->desc_array[j];
				auto& write = *vk_write;
				++vk_write;

				write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				write.pNext = nullptr;
				write.dstSet = VK_NULL_HANDLE;
				write.dstBinding = desc.binding;
				write.dstArrayElement = 0;
				write.descriptorCount = desc.size;
				write.descriptorType = desc.type;
				write.pImageInfo = nullptr;
				write.pBufferInfo = nullptr;
				write.pTexelBufferView = nullptr;

				switch (desc.type)
				{
				case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
				case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
				{
					VkDescriptorBufferInfo* vk_buffer_array;
					err = u::mem_alloc(scratch, vk_buffer_array, desc.size);
					if (err)
						return err;

					for (uint32_t k = 0; k < desc.size; ++k)
					{
						auto* buffer = state.descriptor_array[descriptor_size + k].buffer;
						vk_buffer_array[k].buffer = buffer->buffer;
						vk_buffer_array[k].offset = buffer->offset;
						vk_buffer_array[k].range = buffer->size;
						O_ASSERT(desc.shader == (desc.shader & buffer->shader));
					}

					descriptor_size += desc.size;
					write.pBufferInfo = vk_buffer_array;

					break;
				}
				case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
				case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
				{
					VkDescriptorImageInfo* vk_image_array;
					err = u::mem_alloc(scratch, vk_image_array, desc.size);
					if (err)
						return err;

					for (uint32_t k = 0; k < desc.size; ++k)
					{
						auto* shader_view = state.descriptor_array[descriptor_size + k].shader_view;
						vk_image_array[k].imageView = shader_view->view;
						vk_image_array[k].imageLayout = desc.layout;
						vk_image_array[k].sampler = VK_NULL_HANDLE;
						O_ASSERT(desc.shader == (desc.shader & shader_view->texture->shader));
					}

					descriptor_size += desc.size;
					write.pImageInfo = vk_image_array;

					break;
				}
				case VK_DESCRIPTOR_TYPE_SAMPLER:
				{
					VkDescriptorImageInfo* vk_image_array;
					err = u::mem_alloc(scratch, vk_image_array, desc.size);
					if (err)
						return err;

					for (uint32_t k = 0; k < desc.size; ++k)
					{
						auto* sampler = state.descriptor_array[descriptor_size + k].sampler;
						vk_image_array[k].imageView = VK_NULL_HANDLE;
						vk_image_array[k].imageLayout = desc.layout;
						vk_image_array[k].sampler = sampler->sampler;
					}

					descriptor_size += desc.size;
					write.pImageInfo = vk_image_array;

					break;
				}
				default:
					return err_invalid_parameter;
				}
			}
		}

		vk_result = device->_descriptor_manager.alloc(set_array, create.state_size, layout_array);
		if (vk_result)
			return util::to_error(vk_result);

		vk_write = vk_write_array;
		for (size_t i = 0; i < create.state_size; ++i)
		{
			auto* layout = layout_array[i];
			for (uint32_t j = 0; j < layout->desc_size; ++j)
			{
				auto& write = *vk_write;
				write.dstSet = set_array[i]->set;
				++vk_write;
			}
		}

		device->_device->vtable()->vkUpdateDescriptorSets(
			device->_device->device(), 
			write_size, 
			vk_write_array, 
			0, 
			nullptr);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::descriptor_set_destroy(device_id device, descriptor_set_id const* set_array, size_t set_size)
	{
		device->_descriptor_manager.free(set_array, set_size);
	}

	//--------------------------------------------------
	error_t	device::shader_create(device_id device, shader_id& shader, shader_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;

		VkShaderModuleCreateInfo vk_ci;
		vk_ci.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		vk_ci.pNext = nullptr;
		vk_ci.flags = 0;
		vk_ci.codeSize = create.shader_size;
		vk_ci.pCode = reinterpret_cast<uint32_t const*>(create.shader_data);

		vk_result = device->_shader_manager.alloc(shader, vk_ci);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::shader_destroy(device_id device, shader_id shader)
	{
		device->_shader_manager.free(shader);
	}

	//--------------------------------------------------
	error_t	device::input_layout_create(device_id device, input_layout_id& layout, input_layout_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;
		VkPushConstantRange	vk_push_array[c::max_uniform_size];
		VkDescriptorSetLayoutBinding vk_binding_array[c::max_transform_size];

		O_ASSERT(u::array_size(vk_push_array) >= static_cast<size_t>(create.uniform_desc_size));
		uint32_t vk_push_offset = 0;
		for (uint32_t i = 0; i < create.uniform_desc_size; ++i)
		{
			auto& uniform = create.uniform_desc_array[i];
			auto& vk_push = vk_push_array[i];
			vk_push.offset = vk_push_offset;
			vk_push.size = uniform.size;
			util::to_shader_stage(uniform.stage_mask, vk_push.stageFlags);

			vk_push_offset += uniform.size;
		}

		O_ASSERT(u::array_size(vk_binding_array) >= static_cast<size_t>(create.transform_desc_size));
		for (uint32_t i = 0; i < create.transform_desc_size; ++i)
		{
			auto& transform = create.transform_desc_array[i];
			auto& vk_binding = vk_binding_array[i];

			vk_binding.binding = transform.binding;
			vk_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
			vk_binding.descriptorCount = 1;
			util::to_shader_stage(transform.stage_mask, vk_binding.stageFlags);
			vk_binding.pImmutableSamplers = nullptr;
		}

		VkDescriptorSetLayoutCreateInfo vk_ci;
		vk_ci.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		vk_ci.pNext = nullptr;
		vk_ci.flags = 0;
		vk_ci.bindingCount = create.transform_desc_size;
		vk_ci.pBindings = vk_binding_array;

		vk_result = device->_input_layout_manager.alloc(layout, vk_ci, vk_push_array, create.uniform_desc_size);
		if (vk_result)
			return util::to_error(vk_result);

		VkWriteDescriptorSet vk_write_array[c::max_transform_size];
		VkDescriptorBufferInfo vk_buffer_array[c::max_transform_size];

		for (uint32_t i = 0; i < create.transform_desc_size; ++i)
		{
			auto& vk_write = vk_write_array[i];
			auto& vk_binding = vk_binding_array[i];
			auto& vk_buffer = vk_buffer_array[i];

			vk_buffer.buffer = device->_cmd_heap.buffer();
			vk_buffer.offset = 0;
			vk_buffer.range = device->_max_transform_size;

			vk_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			vk_write.pNext = nullptr;
			vk_write.dstSet = layout->transform_set;
			vk_write.dstBinding = vk_binding.binding;
			vk_write.dstArrayElement = 0;
			vk_write.descriptorCount = vk_binding.descriptorCount;
			vk_write.descriptorType = vk_binding.descriptorType;
			vk_write.pImageInfo = nullptr;
			vk_write.pBufferInfo = &vk_buffer;
			vk_write.pTexelBufferView = nullptr;
		}

		device->_device->vtable()->vkUpdateDescriptorSets(
			device->_device->device(),
			create.transform_desc_size,
			vk_write_array,
			0,
			nullptr);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::input_layout_destroy(device_id device, input_layout_id layout)
	{
		device->_input_layout_manager.free(layout);
	}

	//--------------------------------------------------
	error_t	device::pipeline_layout_create(device_id device, pipeline_layout_id& layout, pipeline_layout_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;
		vk_result = device->_pipeline_layout_manager.alloc(layout, create.descriptor_layout_array, create.descriptor_layout_size, create.input_layout);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::pipeline_layout_destroy(device_id device, pipeline_layout_id layout)
	{
		device->_pipeline_layout_manager.free(layout);
	}

	//--------------------------------------------------
	struct pipeline_create_data_t
	{
		VkPipelineShaderStageCreateInfo				Stages[4];
		VkPipelineVertexInputStateCreateInfo		VertexInputState;
		VkPipelineInputAssemblyStateCreateInfo		InputAssemblyState;
		VkPipelineTessellationStateCreateInfo		TessellationState;
		VkPipelineViewportStateCreateInfo			ViewportState;
		VkPipelineRasterizationStateCreateInfo		RasterizationState;
		VkPipelineMultisampleStateCreateInfo		MultisampleState;
		VkPipelineDepthStencilStateCreateInfo		DepthStencilState;
		VkPipelineColorBlendStateCreateInfo			ColorBlendState;
		VkPipelineDynamicStateCreateInfo			DynamicState;
	};

	//--------------------------------------------------
	static error_t pipeline_get_info(pipeline_create_data_t& data, VkGraphicsPipelineCreateInfo& vk_info, pipeline_state_t const& state, scratch_allocator_t* scratch)
	{
		error_t err;

		vk_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;

		//stages
		{
			vk_info.stageCount = static_cast<uint32_t>(state.shader_state_size);
			vk_info.pStages = data.Stages;

			O_ASSERT(u::array_size(data.Stages) >= state.shader_state_size);
			for (size_t i = 0; i < state.shader_state_size; ++i)
			{
				auto& vk_shader = data.Stages[i];
				auto& shader_state = state.shader_state_array[i];

				vk_shader.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
				vk_shader.pNext = nullptr;
				vk_shader.flags = 0;

				VkShaderStageFlags vk_stage_flags;
				util::to_shader_stage(shader_state.stage, vk_stage_flags);
				vk_shader.stage = static_cast<VkShaderStageFlagBits>(vk_stage_flags);
				
				vk_shader.module = shader_state.shader->shader;
				vk_shader.pName = "main";
				vk_shader.pSpecializationInfo = nullptr;
			}
		}

		//input assembly
		{
			vk_info.pVertexInputState = &data.VertexInputState;
			vk_info.pInputAssemblyState = &data.InputAssemblyState;
		
			data.VertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
			data.VertexInputState.pNext = nullptr;
			data.VertexInputState.flags = 0;
			{
				VkVertexInputBindingDescription* vk_binding_array;
				err	= u::mem_alloc(scratch, vk_binding_array, state.input_assembly_state->vertex_size);
				if (err)
					return err;

				for (uint32_t i = 0; i < state.input_assembly_state->vertex_size; ++i)
				{
					auto& vertex = state.input_assembly_state->vertex_array[i];
					auto& vk_binding = vk_binding_array[i];

					vk_binding.binding = i;
					vk_binding.stride = vertex.stride;
					vk_binding.inputRate = (vertex_input_rate_vertex == vertex.rate) ? VK_VERTEX_INPUT_RATE_VERTEX : VK_VERTEX_INPUT_RATE_INSTANCE;
				}

				
				data.VertexInputState.pVertexBindingDescriptions = vk_binding_array;
				data.VertexInputState.vertexBindingDescriptionCount = state.input_assembly_state->vertex_size;
			}

			{
				VkVertexInputAttributeDescription* vk_attribute_array;
				err = u::mem_alloc(scratch, vk_attribute_array, state.input_assembly_state->attribute_size);
				if (err)
					return err;
				
				for (uint32_t i = 0; i < state.input_assembly_state->attribute_size; ++i)
				{
					auto& attribute = state.input_assembly_state->attribute_array[i];
					auto& vk_attribute = vk_attribute_array[i];

					vk_attribute.binding = attribute.slot;
					vk_attribute.location = attribute.binding;
					vk_attribute.offset = attribute.offset;
					if (!util::to_format(attribute.format, vk_attribute.format))
						return err_invalid_parameter;
				}

			
				data.VertexInputState.pVertexAttributeDescriptions = vk_attribute_array;
				data.VertexInputState.vertexAttributeDescriptionCount = state.input_assembly_state->attribute_size;
			}

			data.InputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
			data.InputAssemblyState.pNext = nullptr;
			data.InputAssemblyState.flags = 0;
			data.InputAssemblyState.primitiveRestartEnable = VK_FALSE;
			if (!util::to_primitive_topology(state.input_assembly_state->topology, data.InputAssemblyState.topology))
				return err_invalid_parameter;
		}

		//tesselation
		vk_info.pTessellationState = nullptr;

		//viewport
		{
			vk_info.pViewportState = &data.ViewportState;

			data.ViewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
			data.ViewportState.pNext = nullptr;
			data.ViewportState.flags = 0;

			if (pipeline_create_dynamic_viewport & state.create_mask)
			{
				data.ViewportState.pViewports = nullptr;
			}
			else
			{
				VkViewport* vk_viewport_array;
				err = u::mem_alloc(scratch, vk_viewport_array, state.viewport_state->size);
				if (err)
					return err;

				for (uint32_t i = 0; i < state.viewport_state->size; ++i)
				{
					auto& vk_viewport = vk_viewport_array[i];
					auto& viewport = state.viewport_state->viewport_array[i];

					vk_viewport.x = viewport.x;
					vk_viewport.y = viewport.y;

					vk_viewport.width = viewport.width;
					vk_viewport.height = viewport.height;

					vk_viewport.minDepth = viewport.depth_min;
					vk_viewport.maxDepth = viewport.depth_max;
				}

				data.ViewportState.pViewports = vk_viewport_array;
			}
			data.ViewportState.viewportCount = state.viewport_state->size;

			if (pipeline_create_dynamic_scissor & state.create_mask)
			{
				data.ViewportState.pScissors = nullptr;
			}
			else
			{
				VkRect2D* vk_scissor_array;
				err = u::mem_alloc(scratch, vk_scissor_array, state.viewport_state->size);
				if (err)
					return err;

				for (uint32_t i = 0; i < state.viewport_state->size; ++i)
				{
					auto& vk_scissor = vk_scissor_array[i];
					auto& scissor = state.viewport_state->scissor_array[i];

					vk_scissor.offset.x = scissor.x;
					vk_scissor.offset.y = scissor.y;
					vk_scissor.extent.width = scissor.width;
					vk_scissor.extent.height = scissor.height;
				}

				data.ViewportState.pScissors = vk_scissor_array;
			}
			data.ViewportState.scissorCount = state.viewport_state->size;
		}

		//rasterizaiton
		{
			vk_info.pRasterizationState = &data.RasterizationState;

			data.RasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			data.RasterizationState.pNext = nullptr;
			data.RasterizationState.flags = 0;

			data.RasterizationState.depthClampEnable = VK_FALSE;
			if (rasterizer_state_disable & state.rasterizer_state->state_mask)
			{
				data.RasterizationState.rasterizerDiscardEnable = VK_TRUE;
				data.RasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
				data.RasterizationState.cullMode = VK_CULL_MODE_FRONT_AND_BACK;
				data.RasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
			}
			else
			{
				data.RasterizationState.rasterizerDiscardEnable = VK_FALSE;

				if (!util::to_polygon_mode(state.rasterizer_state->fill_mode, data.RasterizationState.polygonMode))
					return err_invalid_parameter;

				if (!util::to_cull_mode(state.rasterizer_state->cull_mode, data.RasterizationState.cullMode))
					return err_invalid_parameter;

				data.RasterizationState.frontFace = (rasterizer_state_front_face_counter_clockwise & state.rasterizer_state->state_mask) ?
					VK_FRONT_FACE_COUNTER_CLOCKWISE : VK_FRONT_FACE_CLOCKWISE;
			}
			
			if (rasterizer_state_depth_bias_enable & state.rasterizer_state->state_mask)
			{
				data.RasterizationState.depthBiasEnable = VK_TRUE;
				data.RasterizationState.depthBiasConstantFactor = state.rasterizer_state->depth_bias_offset;
				data.RasterizationState.depthBiasClamp = state.rasterizer_state->depth_bias_clamp;
				data.RasterizationState.depthBiasSlopeFactor = state.rasterizer_state->depth_bias_slope;
			}
			else
			{
				data.RasterizationState.depthBiasEnable = VK_FALSE;
				data.RasterizationState.depthBiasConstantFactor = 0.0f;
				data.RasterizationState.depthBiasClamp = 0.0f;
				data.RasterizationState.depthBiasSlopeFactor = 0.0f;
			}
			
			data.RasterizationState.lineWidth = 1.0f;
		}

		//multisampling
		{
			vk_info.pMultisampleState = &data.MultisampleState;

			data.MultisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
			data.MultisampleState.pNext = nullptr;
			data.MultisampleState.flags = 0;

			data.MultisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
			data.MultisampleState.sampleShadingEnable = VK_FALSE;
			data.MultisampleState.minSampleShading = 0.0f;
			data.MultisampleState.pSampleMask = nullptr;
			data.MultisampleState.alphaToCoverageEnable = VK_FALSE;
			data.MultisampleState.alphaToOneEnable = VK_FALSE;
		}

		//depthstencil
		{
			vk_info.pDepthStencilState = &data.DepthStencilState;

			data.DepthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
			data.DepthStencilState.pNext = nullptr;
			data.DepthStencilState.flags = 0;

			data.DepthStencilState.depthWriteEnable = (depth_stencil_state_depth_store_enable & state.depth_stencil_state->state_mask) ?
				VK_TRUE : VK_FALSE;

			if (depth_stencil_state_depth_compare_enable & state.depth_stencil_state->state_mask)
			{
				data.DepthStencilState.depthTestEnable = VK_TRUE;
				if (!util::to_compare_operation(state.depth_stencil_state->depth_compare_operation, data.DepthStencilState.depthCompareOp))
					return err_invalid_parameter;
			}
			else
			{
				data.DepthStencilState.depthTestEnable = VK_FALSE;
				data.DepthStencilState.depthCompareOp = VK_COMPARE_OP_NEVER;
			}

			data.DepthStencilState.depthBoundsTestEnable = VK_FALSE;
			data.DepthStencilState.stencilTestEnable = VK_FALSE;

			data.DepthStencilState.front.failOp = VK_STENCIL_OP_KEEP;
			data.DepthStencilState.front.passOp = VK_STENCIL_OP_KEEP;
			data.DepthStencilState.front.depthFailOp = VK_STENCIL_OP_KEEP;
			data.DepthStencilState.front.compareOp = VK_COMPARE_OP_NEVER;
			data.DepthStencilState.front.compareMask = 0;
			data.DepthStencilState.front.writeMask = 0;
			data.DepthStencilState.front.reference = 0;

			data.DepthStencilState.back.failOp = VK_STENCIL_OP_KEEP;
			data.DepthStencilState.back.passOp = VK_STENCIL_OP_KEEP;
			data.DepthStencilState.back.depthFailOp = VK_STENCIL_OP_KEEP;
			data.DepthStencilState.back.compareOp = VK_COMPARE_OP_NEVER;
			data.DepthStencilState.back.compareMask = 0;
			data.DepthStencilState.back.writeMask = 0;
			data.DepthStencilState.back.reference = 0;

			data.DepthStencilState.minDepthBounds = 0.0f;
			data.DepthStencilState.maxDepthBounds = 1.0f;
		}

		//blending
		{
			vk_info.pColorBlendState = &data.ColorBlendState;

			data.ColorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			data.ColorBlendState.pNext = nullptr;
			data.ColorBlendState.flags = 0;

			data.ColorBlendState.logicOpEnable = VK_FALSE;
			data.ColorBlendState.logicOp = VK_LOGIC_OP_CLEAR;
			data.ColorBlendState.attachmentCount = state.blend_state->blend_size;

			VkPipelineColorBlendAttachmentState* vk_blend_array;
			err = u::mem_alloc(scratch, vk_blend_array, state.blend_state->blend_size);
			if (err)
				return err;
			
			for (uint32_t i = 0; i < state.blend_state->blend_size; ++i)
			{
				auto& vk_blend = vk_blend_array[i];
				auto& blend = state.blend_state->blend_array[i];

				if (blend_desc_blend_enabled & blend.desc_mask)
				{
					vk_blend.blendEnable = VK_TRUE;

					if (!util::to_blend_factor(blend.color_src_factor, vk_blend.srcColorBlendFactor, false))
						return err_invalid_parameter;
					if (!util::to_blend_factor(blend.color_dst_factor, vk_blend.dstColorBlendFactor, false))
						return err_invalid_parameter;
					if (!util::to_blend_operation(blend.color_operation, vk_blend.colorBlendOp))
						return err_invalid_parameter;

					if (!util::to_blend_factor(blend.alpha_src_factor, vk_blend.srcAlphaBlendFactor, true))
						return err_invalid_parameter;
					if (!util::to_blend_factor(blend.alpha_dst_factor, vk_blend.dstAlphaBlendFactor, true))
						return err_invalid_parameter;
					if (!util::to_blend_operation(blend.alpha_operation, vk_blend.alphaBlendOp))
						return err_invalid_parameter;

					vk_blend.colorWriteMask = 0;
					if (blend_desc_write_r & blend.desc_mask)
						vk_blend.colorWriteMask |= VK_COLOR_COMPONENT_R_BIT;
					if (blend_desc_write_g & blend.desc_mask)
						vk_blend.colorWriteMask |= VK_COLOR_COMPONENT_G_BIT;
					if (blend_desc_write_b & blend.desc_mask)
						vk_blend.colorWriteMask |= VK_COLOR_COMPONENT_B_BIT;
					if (blend_desc_write_a & blend.desc_mask)
						vk_blend.colorWriteMask |= VK_COLOR_COMPONENT_A_BIT;
				}
				else
				{
					vk_blend.blendEnable = VK_FALSE;
					//do some dummy init
					vk_blend.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
					vk_blend.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
					vk_blend.colorBlendOp = VK_BLEND_OP_ADD;
					vk_blend.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
					vk_blend.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
					vk_blend.alphaBlendOp = VK_BLEND_OP_ADD;
					vk_blend.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
				}
			}

			data.ColorBlendState.pAttachments = vk_blend_array;

			data.ColorBlendState.blendConstants[0] = state.blend_state->constant[0];
			data.ColorBlendState.blendConstants[1] = state.blend_state->constant[1];
			data.ColorBlendState.blendConstants[2] = state.blend_state->constant[2];
			data.ColorBlendState.blendConstants[3] = state.blend_state->constant[3];
		}

		//dynamic state
		{
			data.DynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
			data.DynamicState.pNext = nullptr;
			data.DynamicState.flags = 0;

			VkDynamicState vk_dynamic_array[4];
			
			
			data.DynamicState.dynamicStateCount = 0;

			if (pipeline_create_dynamic_viewport & state.create_mask)
			{
				auto& vk_dynamic = vk_dynamic_array[data.DynamicState.dynamicStateCount];
				vk_dynamic = VK_DYNAMIC_STATE_VIEWPORT;
				++data.DynamicState.dynamicStateCount;
			}

			if (pipeline_create_dynamic_scissor & state.create_mask)
			{
				auto& vk_dynamic = vk_dynamic_array[data.DynamicState.dynamicStateCount];
				vk_dynamic = VK_DYNAMIC_STATE_SCISSOR;
				++data.DynamicState.dynamicStateCount;
			}

			if (pipeline_create_dynamic_depth_bias & state.create_mask)
			{
				auto& vk_dynamic = vk_dynamic_array[data.DynamicState.dynamicStateCount];
				vk_dynamic = VK_DYNAMIC_STATE_DEPTH_BIAS;
				++data.DynamicState.dynamicStateCount;
			}

			if (pipeline_create_dynamic_blend_constant & state.create_mask)
			{
				auto& vk_dynamic = vk_dynamic_array[data.DynamicState.dynamicStateCount];
				vk_dynamic = VK_DYNAMIC_STATE_BLEND_CONSTANTS;
				++data.DynamicState.dynamicStateCount;
			}

			if (data.DynamicState.dynamicStateCount)
			{
				VkDynamicState* copy;
				err = u::mem_alloc(scratch, copy, data.DynamicState.dynamicStateCount);
				if (err)
					return err;

				u::mem_copy(copy, vk_dynamic_array, sizeof(VkDynamicState) * data.DynamicState.dynamicStateCount);

				data.DynamicState.pDynamicStates = copy;
				vk_info.pDynamicState = &data.DynamicState;
			}
			else
			{
				vk_info.pDynamicState = nullptr;
			}
		}

		vk_info.layout = state.layout->layout;
		vk_info.renderPass = state.pass->pass;
		vk_info.subpass = state.subpass;
		vk_info.basePipelineHandle = VK_NULL_HANDLE;
		vk_info.basePipelineIndex = -1;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	device::pipeline_create(device_id device, pipeline_id* pipeline_array, pipeline_create_t const& create, scratch_allocator_t* scratch)
	{
		scratch_lock_t lock = { scratch };

		error_t err;
		VkResult vk_result;
		
		VkGraphicsPipelineCreateInfo* vk_create_array;
		err = u::mem_alloc(scratch, vk_create_array, create.pipeline_size);
		if (err)
			return err;

		pipeline_create_data_t* data_array;
		err = u::mem_alloc(scratch, data_array, create.pipeline_size);
		if (err)
			return err;

		for (size_t i = 0; i < create.pipeline_size; ++i)
		{
			err = pipeline_get_info(data_array[i], vk_create_array[i], create.pipeline_array[i], scratch);
			if (err)
				return err;
		}

		vk_result = device->_pipeline_manager.alloc(pipeline_array, create.pipeline_size, vk_create_array);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::pipeline_destroy(device_id device, pipeline_id const* pipeline_array, size_t pipeline_size)
	{
		device->_pipeline_manager.free(pipeline_array, pipeline_size);
	}

	//--------------------------------------------------
	error_t	device::sampler_create(device_id device, sampler_id& sampler, sampler_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;

		VkSamplerCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;

		if (!util::to_filter(create.mag_filter, vk_info.magFilter))
			return err_invalid_parameter;
		if (!util::to_filter(create.min_filter, vk_info.minFilter))
			return err_invalid_parameter;
		if (!util::to_sampler_mip_mode(create.mip_filter, vk_info.mipmapMode))
			return err_invalid_parameter;
		if (!util::to_sampler_address_mode(create.address_u, vk_info.addressModeU))
			return err_invalid_parameter;
		if (!util::to_sampler_address_mode(create.address_v, vk_info.addressModeV))
			return err_invalid_parameter;
		if (!util::to_sampler_address_mode(create.address_w, vk_info.addressModeW))
			return err_invalid_parameter;

		vk_info.mipLodBias = create.mip_bias;
		vk_info.anisotropyEnable = (sampler_create_anisotropy & create.create_mask) ? VK_TRUE : VK_FALSE;
		vk_info.maxAnisotropy = static_cast<float>(create.max_anisotropy);
		vk_info.compareEnable = (sampler_create_compare & create.create_mask) ? VK_TRUE : VK_FALSE;
		if (!util::to_compare_operation(create.compare_operation, vk_info.compareOp))
			return err_invalid_parameter;
		vk_info.minLod = create.mip_min;
		vk_info.maxLod = create.mip_max;
		if (!util::to_border_color(create.border_color, vk_info.borderColor))
			return err_invalid_parameter;

		vk_info.unnormalizedCoordinates = VK_FALSE;

		vk_result = device->_sampler_manager.alloc(sampler, vk_info);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::sampler_destroy(device_id device, sampler_id sampler)
	{
		device->_sampler_manager.free(sampler);
	}

	//--------------------------------------------------
	error_t	device::texture_create(device_id device, void* user_data, texture_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;

		VkImageCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;

		if (texture_usage_cube & create.usage_mask)
			vk_info.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

		if (texture_usage_3D_to_2D & create.usage_mask)
			vk_info.flags |= VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;

		if (!util::to_image_type(create.dimension, vk_info.imageType))
			return err_invalid_parameter;

		if (!util::to_format(create.format, vk_info.format))
			return err_invalid_parameter;

		vk_info.extent.width = create.size.width;
		vk_info.extent.height = create.size.height;
		vk_info.extent.depth = create.size.depth;
		vk_info.mipLevels = create.mip_size;
		vk_info.arrayLayers = create.array_size;

		vk_info.samples = VK_SAMPLE_COUNT_1_BIT;
		vk_info.tiling = VK_IMAGE_TILING_OPTIMAL;

		switch (create.type)
		{
		case texture_type_sampled:
		{
			vk_info.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
			break;
		}
		case texture_type_storage:
		{
			vk_info.usage = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
			if (texture_usage_shader_resource & create.usage_mask)
				vk_info.usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
			break;
		}
		case texture_type_render_target:
		{
			vk_info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			if (texture_usage_shader_resource & create.usage_mask)
				vk_info.usage |= VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
			break;
		}
		case texture_type_depth_stencil:
		{
			vk_info.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
			if (texture_usage_shader_resource & create.usage_mask)
				vk_info.usage |= VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
			break;
		}
		default:
			return err_invalid_parameter;
		}

		vk_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		vk_info.queueFamilyIndexCount = 0;
		vk_info.pQueueFamilyIndices = nullptr;
		vk_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		VkPipelineStageFlags stage;
		VkShaderStageFlags shader;
		util::to_stage(create.stage_mask, stage);
		util::to_shader_stage(create.stage_mask, shader);

		vk_result = device->_texture_manager.alloc(user_data, vk_info, stage, shader);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::texture_destroy(device_id device, texture_id texture)
	{
		device->_texture_manager.free(texture);
	}

	//--------------------------------------------------
	error_t	device::shader_view_create(device_id device, shader_view_id& view, texture_shader_view_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;

		VkImageViewCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;
		vk_info.image = create.texture->image;

		if (!util::to_image_view_type(create.type, vk_info.viewType))
			return err_invalid_parameter;

		if (!util::to_format(create.format, vk_info.format))
			return err_invalid_parameter;

		vk_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;

		vk_info.subresourceRange.baseMipLevel = create.mip_level;
		vk_info.subresourceRange.levelCount = create.mip_size;
		vk_info.subresourceRange.baseArrayLayer = create.array_level;
		vk_info.subresourceRange.layerCount = create.array_size;
		util::to_image_aspect(create.aspect_mask, vk_info.subresourceRange.aspectMask);

		vk_result = device->_view_manager.alloc(view, vk_info, create.texture);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::shader_view_destroy(device_id device, shader_view_id view)
	{
		device->_view_manager.free(view);
	}

	//--------------------------------------------------
	error_t	device::render_target_view_create(device_id device, render_target_view_id& view, render_target_view_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;

		VkImageViewCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;
		vk_info.image = create.texture->image;

		vk_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
		
		if (!util::to_format(create.format, vk_info.format))
			return err_invalid_parameter;

		vk_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;

		vk_info.subresourceRange.baseMipLevel = create.mip_level;
		vk_info.subresourceRange.levelCount = 1;
		vk_info.subresourceRange.baseArrayLayer = create.array_level;
		vk_info.subresourceRange.layerCount = 1;
		util::to_image_aspect(create.aspect_mask, vk_info.subresourceRange.aspectMask);

		vk_result = device->_view_manager.alloc(view, vk_info, create.texture);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::render_target_view_destroy(device_id device, render_target_view_id view)
	{
		device->_view_manager.free(view);
	}

	//--------------------------------------------------
	error_t	device::depth_stencil_view_create(device_id device, depth_stencil_view_id& view, depth_stencil_view_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;

		VkImageViewCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;
		vk_info.image = create.texture->image;

		vk_info.viewType = VK_IMAGE_VIEW_TYPE_2D;

		vk_info.format = create.texture->format;
		
		vk_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;

		vk_info.subresourceRange.baseMipLevel = create.mip_level;
		vk_info.subresourceRange.levelCount = 1;
		vk_info.subresourceRange.baseArrayLayer = create.array_level;
		vk_info.subresourceRange.layerCount = 1;
		util::to_image_aspect(create.aspect_mask, vk_info.subresourceRange.aspectMask);

		vk_result = device->_view_manager.alloc(view, vk_info, create.texture);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::depth_stencil_view_destroy(device_id device, depth_stencil_view_id view)
	{
		device->_view_manager.free(view);
	}

	//--------------------------------------------------
	error_t	device::buffer_create(device_id device, void* user_data, buffer_create_t const& create, scratch_allocator_t* scratch)
	{
		VkResult vk_result;

		VkPipelineStageFlags stage;
		VkShaderStageFlags shader;
		util::to_stage(create.stage_mask, stage);
		util::to_shader_stage(create.stage_mask, shader);

		VkBufferCreateInfo vk_info;
		vk_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		vk_info.pNext = nullptr;
		vk_info.flags = 0;
		vk_info.size = create.size;

		switch (create.type)
		{
		case outland::gpu::buffer_type_uniform:
			vk_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			break;
		case outland::gpu::buffer_type_storage:
			vk_info.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			break;
		case outland::gpu::buffer_type_vertex:
			vk_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			stage = VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
			break;
		case outland::gpu::buffer_type_index:
			vk_info.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			stage = VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
			break;
		default:
			return err_invalid_parameter;
		}

		vk_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		vk_info.queueFamilyIndexCount = 0;
		vk_info.pQueueFamilyIndices = nullptr;

		

		vk_result = device->_buffer_manager.alloc(user_data, vk_info, stage, shader);
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	void	device::buffer_destroy(device_id device, buffer_id buffer)
	{
		device->_buffer_manager.free(buffer);
	}

	//--------------------------------------------------
	error_t	device::buffer_copy(device_id device, host_to_buffer_copy_t const& copy)
	{
		VkResult vk_result;

		vk_result = device->_upload.buffer_copy(copy);
		if (VK_ERROR_OUT_OF_DEVICE_MEMORY == vk_result)
			return err_too_many_requests;
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

	//--------------------------------------------------
	error_t	device::texture_copy(device_id device, host_to_texture_copy_t const& copy)
	{
		VkResult vk_result;
	
		vk_result = device->_upload.texture_copy(copy);
		if (VK_ERROR_OUT_OF_DEVICE_MEMORY == vk_result)
			return err_too_many_requests;
		if (vk_result)
			return util::to_error(vk_result);

		return err_ok;
	}

} }
