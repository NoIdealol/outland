#include "pch.h"
#include "vk_utility.h"

namespace outland { namespace gpu { namespace util
{


#define O_FORMAT_MAP_COLOR(MAPPING, PREFIX, COLOR1, COLOR2, COLOR3, COLOR4) \
MAPPING(VK_FORMAT_R4G4_UNORM_PACK8, COLOR2(PREFIX, R4, G4, UNORM)) \
 \
MAPPING(VK_FORMAT_R4G4B4A4_UNORM_PACK16, COLOR4(PREFIX, R4, G4, B4, A4, UNORM)) \
MAPPING(VK_FORMAT_B4G4R4A4_UNORM_PACK16, COLOR4(PREFIX, B4, G4, R4, A4, UNORM)) \
 \
MAPPING(VK_FORMAT_R5G6B5_UNORM_PACK16, COLOR3(PREFIX, R5, G6, B5, UNORM)) \
MAPPING(VK_FORMAT_B5G6R5_UNORM_PACK16, COLOR3(PREFIX, B5, G6, R5, UNORM)) \
 \
MAPPING(VK_FORMAT_R5G5B5A1_UNORM_PACK16, COLOR4(PREFIX, R5, G5, B5, A1, UNORM)) \
MAPPING(VK_FORMAT_B5G5R5A1_UNORM_PACK16, COLOR4(PREFIX, B5, G5, R5, A1, UNORM)) \
 \
MAPPING(VK_FORMAT_A1R5G5B5_UNORM_PACK16, COLOR4(PREFIX, A1, R5, G5, B5, UNORM)) \
 \
MAPPING(VK_FORMAT_R8_UNORM,		COLOR1(PREFIX, R8, UNORM)) \
MAPPING(VK_FORMAT_R8_SNORM,		COLOR1(PREFIX, R8, SNORM)) \
MAPPING(VK_FORMAT_R8_UINT,		COLOR1(PREFIX, R8, UINT)) \
MAPPING(VK_FORMAT_R8_SINT,		COLOR1(PREFIX, R8, SINT)) \
MAPPING(VK_FORMAT_R8_SRGB,		COLOR1(PREFIX, R8, SRGB)) \
 \
MAPPING(VK_FORMAT_R8G8_UNORM,		COLOR2(PREFIX, R8, G8, UNORM)) \
MAPPING(VK_FORMAT_R8G8_SNORM,		COLOR2(PREFIX, R8, G8, SNORM)) \
MAPPING(VK_FORMAT_R8G8_UINT,		COLOR2(PREFIX, R8, G8, UINT)) \
MAPPING(VK_FORMAT_R8G8_SINT,		COLOR2(PREFIX, R8, G8, SINT)) \
MAPPING(VK_FORMAT_R8G8_SRGB,		COLOR2(PREFIX, R8, G8, SRGB)) \
 \
MAPPING(VK_FORMAT_R8G8B8_UNORM,		COLOR3(PREFIX, R8, G8, B8, UNORM)) \
MAPPING(VK_FORMAT_R8G8B8_SNORM,		COLOR3(PREFIX, R8, G8, B8, SNORM)) \
MAPPING(VK_FORMAT_R8G8B8_UINT,		COLOR3(PREFIX, R8, G8, B8, UINT)) \
MAPPING(VK_FORMAT_R8G8B8_SINT,		COLOR3(PREFIX, R8, G8, B8, SINT)) \
MAPPING(VK_FORMAT_R8G8B8_SRGB,		COLOR3(PREFIX, R8, G8, B8, SRGB)) \
 \
MAPPING(VK_FORMAT_B8G8R8_UNORM,		COLOR3(PREFIX, B8, G8, R8, UNORM)) \
MAPPING(VK_FORMAT_B8G8R8_SNORM,		COLOR3(PREFIX, B8, G8, R8, SNORM)) \
MAPPING(VK_FORMAT_B8G8R8_UINT,		COLOR3(PREFIX, B8, G8, R8, UINT)) \
MAPPING(VK_FORMAT_B8G8R8_SINT,		COLOR3(PREFIX, B8, G8, R8, SINT)) \
MAPPING(VK_FORMAT_B8G8R8_SRGB,		COLOR3(PREFIX, B8, G8, R8, SRGB)) \
 \
MAPPING(VK_FORMAT_R8G8B8A8_UNORM,		COLOR4(PREFIX, R8, G8, B8, A8, UNORM)) \
MAPPING(VK_FORMAT_R8G8B8A8_SNORM,		COLOR4(PREFIX, R8, G8, B8, A8, SNORM)) \
MAPPING(VK_FORMAT_R8G8B8A8_UINT,		COLOR4(PREFIX, R8, G8, B8, A8, UINT)) \
MAPPING(VK_FORMAT_R8G8B8A8_SINT,		COLOR4(PREFIX, R8, G8, B8, A8, SINT)) \
MAPPING(VK_FORMAT_R8G8B8A8_SRGB,		COLOR4(PREFIX, R8, G8, B8, A8, SRGB)) \
 \
MAPPING(VK_FORMAT_B8G8R8A8_UNORM,		COLOR4(PREFIX, B8, G8, R8, A8, UNORM)) \
MAPPING(VK_FORMAT_B8G8R8A8_SNORM,		COLOR4(PREFIX, B8, G8, R8, A8, SNORM)) \
MAPPING(VK_FORMAT_B8G8R8A8_UINT,		COLOR4(PREFIX, B8, G8, R8, A8, UINT)) \
MAPPING(VK_FORMAT_B8G8R8A8_SINT,		COLOR4(PREFIX, B8, G8, R8, A8, SINT)) \
MAPPING(VK_FORMAT_B8G8R8A8_SRGB,		COLOR4(PREFIX, B8, G8, R8, A8, SRGB)) \
 \
MAPPING(VK_FORMAT_A8B8G8R8_UNORM_PACK32,		COLOR4(PREFIX, A8, B8, G8, R8, UNORM)) \
MAPPING(VK_FORMAT_A8B8G8R8_SNORM_PACK32,		COLOR4(PREFIX, A8, B8, G8, R8, SNORM)) \
MAPPING(VK_FORMAT_A8B8G8R8_UINT_PACK32,			COLOR4(PREFIX, A8, B8, G8, R8, UINT)) \
MAPPING(VK_FORMAT_A8B8G8R8_SINT_PACK32,			COLOR4(PREFIX, A8, B8, G8, R8, SINT)) \
MAPPING(VK_FORMAT_A8B8G8R8_SRGB_PACK32,			COLOR4(PREFIX, A8, B8, G8, R8, SRGB)) \
 \
MAPPING(VK_FORMAT_A2R10G10B10_UNORM_PACK32,		COLOR4(PREFIX, A2, R10, G10, B10, UNORM)) \
MAPPING(VK_FORMAT_A2R10G10B10_SNORM_PACK32,		COLOR4(PREFIX, A2, R10, G10, B10, SNORM)) \
MAPPING(VK_FORMAT_A2R10G10B10_UINT_PACK32,		COLOR4(PREFIX, A2, R10, G10, B10, UINT)) \
MAPPING(VK_FORMAT_A2R10G10B10_SINT_PACK32,		COLOR4(PREFIX, A2, R10, G10, B10, SINT)) \
 \
MAPPING(VK_FORMAT_A2B10G10R10_UNORM_PACK32,		COLOR4(PREFIX, A2, B10, G10, R10, UNORM)) \
MAPPING(VK_FORMAT_A2B10G10R10_SNORM_PACK32,		COLOR4(PREFIX, A2, B10, G10, R10, SNORM)) \
MAPPING(VK_FORMAT_A2B10G10R10_UINT_PACK32,		COLOR4(PREFIX, A2, B10, G10, R10, UINT)) \
MAPPING(VK_FORMAT_A2B10G10R10_SINT_PACK32,		COLOR4(PREFIX, A2, B10, G10, R10, SINT)) \
 \
MAPPING(VK_FORMAT_R16_UNORM,		COLOR1(PREFIX, R16, UNORM)) \
MAPPING(VK_FORMAT_R16_SNORM,		COLOR1(PREFIX, R16, SNORM)) \
MAPPING(VK_FORMAT_R16_UINT,			COLOR1(PREFIX, R16, UINT)) \
MAPPING(VK_FORMAT_R16_SINT,			COLOR1(PREFIX, R16, SINT)) \
MAPPING(VK_FORMAT_R16_SFLOAT,		COLOR1(PREFIX, R16, FLOAT)) \
 \
MAPPING(VK_FORMAT_R16G16_UNORM,			COLOR2(PREFIX, R16, G16, UNORM)) \
MAPPING(VK_FORMAT_R16G16_SNORM,			COLOR2(PREFIX, R16, G16, SNORM)) \
MAPPING(VK_FORMAT_R16G16_UINT,			COLOR2(PREFIX, R16, G16, UINT)) \
MAPPING(VK_FORMAT_R16G16_SINT,			COLOR2(PREFIX, R16, G16, SINT)) \
MAPPING(VK_FORMAT_R16G16_SFLOAT,		COLOR2(PREFIX, R16, G16, FLOAT)) \
 \
MAPPING(VK_FORMAT_R16G16B16_UNORM,			COLOR3(PREFIX, R16, G16, B16, UNORM)) \
MAPPING(VK_FORMAT_R16G16B16_SNORM,			COLOR3(PREFIX, R16, G16, B16, SNORM)) \
MAPPING(VK_FORMAT_R16G16B16_UINT,			COLOR3(PREFIX, R16, G16, B16, UINT)) \
MAPPING(VK_FORMAT_R16G16B16_SINT,			COLOR3(PREFIX, R16, G16, B16, SINT)) \
MAPPING(VK_FORMAT_R16G16B16_SFLOAT,			COLOR3(PREFIX, R16, G16, B16, FLOAT)) \
 \
MAPPING(VK_FORMAT_R16G16B16A16_UNORM,			COLOR4(PREFIX, R16, G16, B16, A16, UNORM)) \
MAPPING(VK_FORMAT_R16G16B16A16_SNORM,			COLOR4(PREFIX, R16, G16, B16, A16, SNORM)) \
MAPPING(VK_FORMAT_R16G16B16A16_UINT,			COLOR4(PREFIX, R16, G16, B16, A16, UINT)) \
MAPPING(VK_FORMAT_R16G16B16A16_SINT,			COLOR4(PREFIX, R16, G16, B16, A16, SINT)) \
MAPPING(VK_FORMAT_R16G16B16A16_SFLOAT,			COLOR4(PREFIX, R16, G16, B16, A16, FLOAT)) \
 \
MAPPING(VK_FORMAT_R32_UINT,			COLOR1(PREFIX, R32, UINT)) \
MAPPING(VK_FORMAT_R32_SINT,			COLOR1(PREFIX, R32, SINT)) \
MAPPING(VK_FORMAT_R32_SFLOAT,		COLOR1(PREFIX, R32, FLOAT)) \
 \
MAPPING(VK_FORMAT_R32G32_UINT,			COLOR2(PREFIX, R32, G32, UINT)) \
MAPPING(VK_FORMAT_R32G32_SINT,			COLOR2(PREFIX, R32, G32, SINT)) \
MAPPING(VK_FORMAT_R32G32_SFLOAT,		COLOR2(PREFIX, R32, G32, FLOAT)) \
 \
MAPPING(VK_FORMAT_R32G32B32_UINT,			COLOR3(PREFIX, R32, G32, B32, UINT)) \
MAPPING(VK_FORMAT_R32G32B32_SINT,			COLOR3(PREFIX, R32, G32, B32, SINT)) \
MAPPING(VK_FORMAT_R32G32B32_SFLOAT,			COLOR3(PREFIX, R32, G32, B32, FLOAT)) \
 \
MAPPING(VK_FORMAT_R32G32B32A32_UINT,			COLOR4(PREFIX, R32, G32, B32, A32, UINT)) \
MAPPING(VK_FORMAT_R32G32B32A32_SINT,			COLOR4(PREFIX, R32, G32, B32, A32, SINT)) \
MAPPING(VK_FORMAT_R32G32B32A32_SFLOAT,			COLOR4(PREFIX, R32, G32, B32, A32, FLOAT))


#define O_FORMAT_MAP_DEPTH(MAPPING, PREFIX, DEPTH1, DEPTH2, DEPTH11) \
MAPPING(VK_FORMAT_D16_UNORM,				DEPTH1(PREFIX, D16, UNORM)) \
MAPPING(VK_FORMAT_X8_D24_UNORM_PACK32,		DEPTH2(PREFIX, X8, D24, UNORM)) \
MAPPING(VK_FORMAT_D32_SFLOAT,				DEPTH1(PREFIX, D32, FLOAT)) \
MAPPING(VK_FORMAT_S8_UINT,					DEPTH1(PREFIX, S8, UINT)) \
MAPPING(VK_FORMAT_D16_UNORM_S8_UINT,		DEPTH11(PREFIX, D16, UNORM, S8, UINT)) \
MAPPING(VK_FORMAT_D24_UNORM_S8_UINT,		DEPTH11(PREFIX, D24, UNORM, S8, UINT)) \
MAPPING(VK_FORMAT_D32_SFLOAT_S8_UINT,		DEPTH11(PREFIX, D32, FLOAT, S8, UINT))

#define O_FORMAT_MAP_BLOCK(MAPPING, PREFIX, BLOCK) \
MAPPING(VK_FORMAT_BC1_RGBA_UNORM_BLOCK,		BLOCK(PREFIX, BC1, UNORM)) \
MAPPING(VK_FORMAT_BC1_RGBA_SRGB_BLOCK,		BLOCK(PREFIX, BC1, SRGB)) \
MAPPING(VK_FORMAT_BC2_UNORM_BLOCK,			BLOCK(PREFIX, BC2, UNORM)) \
MAPPING(VK_FORMAT_BC2_SRGB_BLOCK,			BLOCK(PREFIX, BC2, SRGB)) \
MAPPING(VK_FORMAT_BC3_UNORM_BLOCK,			BLOCK(PREFIX, BC3, UNORM)) \
MAPPING(VK_FORMAT_BC3_SRGB_BLOCK,			BLOCK(PREFIX, BC3, SRGB)) \
MAPPING(VK_FORMAT_BC4_UNORM_BLOCK,			BLOCK(PREFIX, BC4, UNORM)) \
MAPPING(VK_FORMAT_BC4_SNORM_BLOCK,			BLOCK(PREFIX, BC4, SNORM)) \
MAPPING(VK_FORMAT_BC5_UNORM_BLOCK,			BLOCK(PREFIX, BC5, UNORM)) \
MAPPING(VK_FORMAT_BC5_SNORM_BLOCK,			BLOCK(PREFIX, BC5, SNORM)) \
MAPPING(VK_FORMAT_BC6H_UFLOAT_BLOCK,		BLOCK(PREFIX, BC6H, UFLOAT)) \
MAPPING(VK_FORMAT_BC6H_SFLOAT_BLOCK,		BLOCK(PREFIX, BC6H, FLOAT)) \
MAPPING(VK_FORMAT_BC7_UNORM_BLOCK,			BLOCK(PREFIX, BC7, UNORM)) \
MAPPING(VK_FORMAT_BC7_SRGB_BLOCK,			BLOCK(PREFIX, BC7, SRGB))

#define O_PREFIX format_t::
#define O_COLOR_C1(PREFIX, C1, F) PREFIX##C1##_##F
#define O_COLOR_C2(PREFIX, C1, C2, F) PREFIX##C1##C2##_##F
#define O_COLOR_C3(PREFIX, C1, C2, C3, F) PREFIX##C1##C2##C3##_##F
#define O_COLOR_C4(PREFIX, C1, C2, C3, C4, F) PREFIX##C1##C2##C3##C4##_##F

#define O_DEPTH_C1(PREFIX, C1, F) PREFIX##C1##_##F
#define O_DEPTH_C2(PREFIX, C1, C2, F) PREFIX##C1##C2##_##F
#define O_DEPTH_C1_C1(PREFIX, C11, F1, C21, F2) PREFIX##C11##_##F1##_##C21##_##F2

#define O_BLOCK_BC(PREFIX, BC, F) PREFIX##BC##_##F

	//--------------------------------------------------
	bool		to_format(VkFormat vk_fmt, format_t& fmt)
	{
#define O_FORMAT_MAPPING(NATIVE, INTERFACE) case NATIVE: fmt = INTERFACE; return true;

		switch (vk_fmt)
		{
		case VK_FORMAT_UNDEFINED:
			fmt = format_t::undefined; return true;
			O_FORMAT_MAP_COLOR(O_FORMAT_MAPPING, O_PREFIX, O_COLOR_C1, O_COLOR_C2, O_COLOR_C3, O_COLOR_C4)
				O_FORMAT_MAP_DEPTH(O_FORMAT_MAPPING, O_PREFIX, O_DEPTH_C1, O_DEPTH_C2, O_DEPTH_C1_C1)
				O_FORMAT_MAP_BLOCK(O_FORMAT_MAPPING, O_PREFIX, O_BLOCK_BC)
		default:
			break;
		}

		return false;
#undef O_FORMAT_MAPPING
	}

	//--------------------------------------------------
	bool		to_format(format_t fmt, VkFormat& vk_fmt)
	{
#define O_FORMAT_MAPPING(NATIVE, INTERFACE) case INTERFACE: vk_fmt = NATIVE; return true;

		switch (fmt)
		{
		case format_t::undefined:
			vk_fmt = VK_FORMAT_UNDEFINED; return true;
			O_FORMAT_MAP_COLOR(O_FORMAT_MAPPING, O_PREFIX, O_COLOR_C1, O_COLOR_C2, O_COLOR_C3, O_COLOR_C4)
				O_FORMAT_MAP_DEPTH(O_FORMAT_MAPPING, O_PREFIX, O_DEPTH_C1, O_DEPTH_C2, O_DEPTH_C1_C1)
				O_FORMAT_MAP_BLOCK(O_FORMAT_MAPPING, O_PREFIX, O_BLOCK_BC)
		default:
			break;
		}

		return false;
#undef O_FORMAT_MAPPING
	}

	//--------------------------------------------------
	error_t to_error(VkResult result)
	{
		switch (result)
		{
		case VK_SUCCESS:
			return err_ok;
		case VK_NOT_READY:
			break;
		case VK_TIMEOUT:
			break;
		case VK_EVENT_SET:
			break;
		case VK_EVENT_RESET:
			break;
		case VK_INCOMPLETE:
			break;
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			return err_out_of_memory;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			break;
		case VK_ERROR_INITIALIZATION_FAILED:
			break;
		case VK_ERROR_DEVICE_LOST:
			break;
		case VK_ERROR_MEMORY_MAP_FAILED:
			break;
		case VK_ERROR_LAYER_NOT_PRESENT:
			break;
		case VK_ERROR_EXTENSION_NOT_PRESENT:
			break;
		case VK_ERROR_FEATURE_NOT_PRESENT:
			break;
		case VK_ERROR_INCOMPATIBLE_DRIVER:
			break;
		case VK_ERROR_TOO_MANY_OBJECTS:
			return err_out_of_handles;
		case VK_ERROR_FORMAT_NOT_SUPPORTED:
			break;
		case VK_ERROR_FRAGMENTED_POOL:
			return err_out_of_handles;
		case VK_ERROR_OUT_OF_POOL_MEMORY:
			return err_out_of_memory;
		case VK_ERROR_INVALID_EXTERNAL_HANDLE:
			break;
		case VK_ERROR_SURFACE_LOST_KHR:
			break;
		case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
			break;
		case VK_SUBOPTIMAL_KHR:
			break;
		case VK_ERROR_OUT_OF_DATE_KHR:
			break;
		case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
			break;
		case VK_ERROR_VALIDATION_FAILED_EXT:
			break;
		case VK_ERROR_INVALID_SHADER_NV:
			break;
		case VK_ERROR_FRAGMENTATION_EXT:
			break;
		case VK_ERROR_NOT_PERMITTED_EXT:
			break;
		default:
			break;
		}

		return err_unknown;
	}
	
	//--------------------------------------------------
	bool	 to_present_mode(VkPresentModeKHR vk_mode, present_mode_t& mode)
	{
		switch (vk_mode)
		{
		case VK_PRESENT_MODE_IMMEDIATE_KHR:
			mode = present_mode_immediate;
			return true;
		case VK_PRESENT_MODE_MAILBOX_KHR:
			mode = present_mode_mailbox;
			return true;
		case VK_PRESENT_MODE_FIFO_KHR:
			mode = present_mode_fifo;
			return true;
		case VK_PRESENT_MODE_FIFO_RELAXED_KHR:
			break;
		case VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR:
			break;
		case VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR:
			break;
		default:
			break;
		}

		return false;
	}

	//--------------------------------------------------
	bool		to_present_mode(present_mode_t mode, VkPresentModeKHR& vk_mode)
	{
		switch (mode)
		{
		case present_mode_immediate:
			vk_mode = VK_PRESENT_MODE_IMMEDIATE_KHR;
			return true;
		case present_mode_mailbox:
			vk_mode = VK_PRESENT_MODE_MAILBOX_KHR;
			return true;
		case present_mode_fifo:
			vk_mode = VK_PRESENT_MODE_FIFO_KHR;
			return true;
		default:
			break;
		}

		return false;
	}

	//--------------------------------------------------
	bool find_memory_index(VkPhysicalDeviceMemoryProperties const& props, VkMemoryPropertyFlags flags, uint32_t type_bits, uint32_t& memory_index)
	{
		for (uint32_t i = 0; i < props.memoryTypeCount; ++i)
		{
			if ((type_bits & (uint32_t(1) << i)) &&
				(flags == (props.memoryTypes[i].propertyFlags & flags)))
			{
				memory_index = i;
				return true;
			}
		}

		return false;
	}

	//--------------------------------------------------
	bool get_format_texel_info(VkFormat format, uint32_t& texel_block_byte_size, uint32_t& texel_block_pixel_size)
	{
		texel_block_byte_size = 4;
		texel_block_pixel_size = 1;

		return true;
	}

	//--------------------------------------------------
	bool to_primitive_topology(primitive_topology_t topology, VkPrimitiveTopology& vk_topology)
	{
		switch (topology)
		{
		case outland::gpu::primitive_topology_point_list:
			vk_topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
			return true;
		case outland::gpu::primitive_topology_line_list:
			vk_topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
			return true;
		case outland::gpu::primitive_topology_line_strip:
			vk_topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
			return true;
		case outland::gpu::primitive_topology_triangle_list:
			vk_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
			return true;
		case outland::gpu::primitive_topology_triangle_strip:
			vk_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
			return true;
		case outland::gpu::primitive_topology_triangle_fan:
			vk_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_polygon_mode(fill_mode_t mode, VkPolygonMode& vk_mode)
	{
		switch (mode)
		{
		case outland::gpu::fill_mode_solid:
			vk_mode = VK_POLYGON_MODE_FILL;
			return true;
		case outland::gpu::fill_mode_wireframe:
			vk_mode = VK_POLYGON_MODE_LINE;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_cull_mode(cull_mode_t mode, VkCullModeFlags& vk_mode)
	{
		switch (mode)
		{
		case outland::gpu::cull_mode_none:
			vk_mode = VK_CULL_MODE_NONE;
			return true;
		case outland::gpu::cull_mode_front:
			vk_mode = VK_CULL_MODE_FRONT_BIT;
			return true;
		case outland::gpu::cull_mode_back:
			vk_mode = VK_CULL_MODE_BACK_BIT;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_compare_operation(compare_operation_t operation, VkCompareOp& vk_operation)
	{
		switch (operation)
		{
		case outland::gpu::compare_operation_never:
			vk_operation = VK_COMPARE_OP_NEVER;
			return true;
		case outland::gpu::compare_operation_less:
			vk_operation = VK_COMPARE_OP_LESS;
			return true;
		case outland::gpu::compare_operation_equal:
			vk_operation = VK_COMPARE_OP_EQUAL;
			return true;
		case outland::gpu::compare_operation_less_or_equal:
			vk_operation = VK_COMPARE_OP_LESS_OR_EQUAL;
			return true;
		case outland::gpu::compare_operation_greater:
			vk_operation = VK_COMPARE_OP_GREATER;
			return true;
		case outland::gpu::compare_operation_not_equal:
			vk_operation = VK_COMPARE_OP_NOT_EQUAL;
			return true;
		case outland::gpu::compare_operation_greater_or_equal:
			vk_operation = VK_COMPARE_OP_GREATER_OR_EQUAL;
			return true;
		case outland::gpu::compare_operation_always:
			vk_operation = VK_COMPARE_OP_ALWAYS;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_blend_operation(blend_operation_t operation, VkBlendOp& vk_operation)
	{
		switch (operation)
		{
		case outland::gpu::blend_operation_add:
			vk_operation = VK_BLEND_OP_ADD;
			return true;
		case outland::gpu::blend_operation_subtract:
			vk_operation = VK_BLEND_OP_SUBTRACT;
			return true;
		case outland::gpu::blend_operation_rev_subtract:
			vk_operation = VK_BLEND_OP_REVERSE_SUBTRACT;
			return true;
		case outland::gpu::blend_operation_minimum:
			vk_operation = VK_BLEND_OP_MIN;
			return true;
		case outland::gpu::blend_operation_maximum:
			vk_operation = VK_BLEND_OP_MAX;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_blend_factor(blend_factor_t factor, VkBlendFactor& vk_factor, bool is_alpha)
	{
		switch (factor)
		{
		case outland::gpu::blend_factor_zero:
			vk_factor = VK_BLEND_FACTOR_ZERO;
			return true;
		case outland::gpu::blend_factor_one:
			vk_factor = VK_BLEND_FACTOR_ONE;
			return true;
		case outland::gpu::blend_factor_src_color:
			vk_factor = VK_BLEND_FACTOR_SRC_COLOR;
			return true;
		case outland::gpu::blend_factor_inv_src_color:
			vk_factor = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
			return true;
		case outland::gpu::blend_factor_dst_color:
			vk_factor = VK_BLEND_FACTOR_DST_COLOR;
			return true;
		case outland::gpu::blend_factor_inv_dst_color:
			vk_factor = VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
			return true;
		case outland::gpu::blend_factor_src_alpha:
			vk_factor = VK_BLEND_FACTOR_SRC_ALPHA;
			return true;
		case outland::gpu::blend_factor_inv_src_alpha:
			vk_factor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			return true;
		case outland::gpu::blend_factor_dst_alpha:
			vk_factor = VK_BLEND_FACTOR_DST_ALPHA;
			return true;
		case outland::gpu::blend_factor_inv_dst_alpha:
			vk_factor = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
			return true;
		case outland::gpu::blend_factor_constant:
			vk_factor = is_alpha ? VK_BLEND_FACTOR_CONSTANT_ALPHA : VK_BLEND_FACTOR_CONSTANT_COLOR;
			return true;
		case outland::gpu::blend_factor_inv_constant:
			vk_factor = is_alpha ? VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA : VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_sampler_address_mode(address_mode_t mode, VkSamplerAddressMode& vk_mode)
	{
		switch (mode)
		{
		case outland::gpu::address_mode_wrap:
			vk_mode = VK_SAMPLER_ADDRESS_MODE_REPEAT;
			return true;
		case outland::gpu::address_mode_mirror:
			vk_mode = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
			return true;
		case outland::gpu::address_mode_clamp:
			vk_mode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
			return true;
		case outland::gpu::address_mode_border:
			vk_mode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_filter(filter_t filter, VkFilter& vk_filter)
	{
		switch (filter)
		{
		case outland::gpu::filter_nearest:
			vk_filter = VK_FILTER_NEAREST;
			return true;
		case outland::gpu::filter_linear:
			vk_filter = VK_FILTER_LINEAR;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_sampler_mip_mode(filter_t filter, VkSamplerMipmapMode& vk_filter)
	{
		switch (filter)
		{
		case outland::gpu::filter_nearest:
			vk_filter = VK_SAMPLER_MIPMAP_MODE_NEAREST;
			return true;
		case outland::gpu::filter_linear:
			vk_filter = VK_SAMPLER_MIPMAP_MODE_LINEAR;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_border_color(border_color_t color, VkBorderColor& vk_color)
	{
		switch (color)
		{
		case outland::gpu::border_color_R0G0B0A0_FLOAT:
			vk_color = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
			return true;
		case outland::gpu::border_color_R0G0B0A1_FLOAT:
			vk_color = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
			return true;
		case outland::gpu::border_color_R1G1B1A1_FLOAT:
			vk_color = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
			return true;
		case outland::gpu::border_color_R0G0B0A0_INT:
			vk_color = VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
			return true;
		case outland::gpu::border_color_R0G0B0A1_INT:
			vk_color = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
			return true;
		case outland::gpu::border_color_R1G1B1A1_INT:
			vk_color = VK_BORDER_COLOR_INT_OPAQUE_WHITE;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_image_type(texture_dimension_t dim, VkImageType& vk_type)
	{
		switch (dim)
		{
		case outland::gpu::texture_dimension_1D:
			vk_type = VK_IMAGE_TYPE_1D;
			return true;
		case outland::gpu::texture_dimension_2D:
			vk_type = VK_IMAGE_TYPE_2D;
			return true;
		case outland::gpu::texture_dimension_3D:
			vk_type = VK_IMAGE_TYPE_3D;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_image_view_type(texture_shader_view_type_t type, VkImageViewType& vk_type)
	{
		switch (type)
		{
		case outland::gpu::texture_shader_view_type_1D:
			vk_type = VK_IMAGE_VIEW_TYPE_1D;
			return true;
		case outland::gpu::texture_shader_view_type_2D:
			vk_type = VK_IMAGE_VIEW_TYPE_2D;
			return true;
		case outland::gpu::texture_shader_view_type_3D:
			vk_type = VK_IMAGE_VIEW_TYPE_3D;
			return true;
		case outland::gpu::texture_shader_view_type_cube_map:
			vk_type = VK_IMAGE_VIEW_TYPE_CUBE;
			return true;
		case outland::gpu::texture_shader_view_type_1D_array:
			vk_type = VK_IMAGE_VIEW_TYPE_1D_ARRAY;
			return true;
		case outland::gpu::texture_shader_view_type_2D_array:
			vk_type = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			return true;
		case outland::gpu::texture_shader_view_type_cube_map_array:
			vk_type = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	void to_image_aspect(texture_aspect_mask_t aspect, VkImageAspectFlags& vk_aspect)
	{
		vk_aspect = 0;

		if (texture_aspect_color & aspect)
			vk_aspect |= VK_IMAGE_ASPECT_COLOR_BIT;
		if (texture_aspect_depth & aspect)
			vk_aspect |= VK_IMAGE_ASPECT_DEPTH_BIT;
		if (texture_aspect_stencil & aspect)
			vk_aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
	}

	//--------------------------------------------------
	bool to_access(render_target_access_t access, VkAccessFlags& vk_access)
	{
		switch (access)
		{
		case outland::gpu::render_target_access_store:
			vk_access = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			return true;
		case outland::gpu::render_target_access_blend:
			vk_access = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
			return true;
		case outland::gpu::render_target_access_input:
			vk_access = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
			return true;
		case outland::gpu::render_target_access_sampled:
			vk_access = VK_ACCESS_SHADER_READ_BIT;
			return true;
		case outland::gpu::render_target_access_copy_src:
			vk_access = VK_ACCESS_TRANSFER_READ_BIT;
			return true;
		default:
			return false;
		}	
	}

	//--------------------------------------------------
	bool to_access(depth_stencil_access_t access, VkAccessFlags& vk_access)
	{
		switch (access)
		{
		case outland::gpu::depth_stencil_access_store:
			vk_access = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
			return true;
		case outland::gpu::depth_stencil_access_compare:
			vk_access = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
			return true;
		case outland::gpu::depth_stencil_access_store_compare:
			vk_access = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
			return true;
		case outland::gpu::depth_stencil_access_input:
			vk_access = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
			return true;
		case outland::gpu::depth_stencil_access_input_compare:
			vk_access = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
			return true;
		case outland::gpu::depth_stencil_access_sampled:
			vk_access = VK_ACCESS_SHADER_READ_BIT;
			return true;
		case outland::gpu::depth_stencil_access_copy_src:
			vk_access = VK_ACCESS_TRANSFER_READ_BIT;
			return true;
		default:
			return false;
		}	
	}

	//--------------------------------------------------
	void to_stage(shader_stage_mask_t stage, VkPipelineStageFlags& vk_stage)
	{
		vk_stage = 0;
		if (shader_stage_vertex & stage)
			vk_stage |= VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;
		if (shader_stage_geometry & stage)
			vk_stage |= VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT;
		if (shader_stage_fragment & stage)
			vk_stage |= VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		if (shader_stage_compute & stage)
			vk_stage |= VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
	}

	//--------------------------------------------------
	bool to_stage(render_target_access_t access, VkPipelineStageFlags& vk_stage)
	{
		switch (access)
		{
		case outland::gpu::render_target_access_store:
			vk_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			return true;
		case outland::gpu::render_target_access_blend:
			vk_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			return true;
		case outland::gpu::render_target_access_input:
			vk_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			return true;
		case outland::gpu::render_target_access_sampled:
			vk_stage = 0;
			return true;
		case outland::gpu::render_target_access_copy_src:
			vk_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			return true;
		default:
			return false;
		}	
	}

	//--------------------------------------------------
	bool to_stage(depth_stencil_access_t access, VkPipelineStageFlags& vk_stage)
	{
		switch (access)
		{
		case outland::gpu::depth_stencil_access_store:
			vk_stage = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
			return true;
		case outland::gpu::depth_stencil_access_compare:
			vk_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			return true;
		case outland::gpu::depth_stencil_access_store_compare:
			vk_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
			return true;
		case outland::gpu::depth_stencil_access_input:
			vk_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			return true;
		case outland::gpu::depth_stencil_access_input_compare:
			vk_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			return true;
		case outland::gpu::depth_stencil_access_sampled:
			vk_stage = 0;
			return true;
		case outland::gpu::depth_stencil_access_copy_src:
			vk_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			return true;
		default:
			return false;
		}	
	}

	//--------------------------------------------------
	bool to_layout(render_target_access_t access, VkImageLayout& vk_layout)
	{
		switch (access)
		{
		case outland::gpu::render_target_access_store:
			vk_layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			return true;
		case outland::gpu::render_target_access_blend:
			vk_layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			return true;
		case outland::gpu::render_target_access_input:
			vk_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::render_target_access_sampled:
			vk_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::render_target_access_copy_src:
			vk_layout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_layout(depth_stencil_access_t access, VkImageLayout& vk_layout)
	{
		switch (access)
		{
		case outland::gpu::depth_stencil_access_store:
			vk_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			return true;
		case outland::gpu::depth_stencil_access_compare:
			vk_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::depth_stencil_access_store_compare:
			vk_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			return true;
		case outland::gpu::depth_stencil_access_input:
			vk_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::depth_stencil_access_input_compare:
			vk_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::depth_stencil_access_sampled:
			vk_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::depth_stencil_access_copy_src:
			vk_layout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_attachment_load(attachment_load_t load, VkAttachmentLoadOp& op)
	{
		switch (load)
		{
		case outland::gpu::attachment_load_discard:
			op = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			return true;
		case outland::gpu::attachment_load_read:
			op = VK_ATTACHMENT_LOAD_OP_LOAD;
			return true;
		case outland::gpu::attachment_load_clear:
			op = VK_ATTACHMENT_LOAD_OP_CLEAR;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_attachment_store(attachment_store_t store, VkAttachmentStoreOp& op)
	{
		switch (store)
		{
		case outland::gpu::attachment_store_discard:
			op = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			return true;
		case outland::gpu::attachment_store_write:
			op = VK_ATTACHMENT_STORE_OP_STORE;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	bool to_descriptor_type(descriptor_type_t type, VkDescriptorType& vk_type)
	{
		switch (type)
		{
		case outland::gpu::descriptor_type_sampled_texture:
			vk_type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
			return true;
		case outland::gpu::descriptor_type_uniform_buffer:
			vk_type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			return true;
		case outland::gpu::descriptor_type_storage_buffer:
			vk_type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
			return true;
		case outland::gpu::descriptor_type_render_target:
			vk_type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
			return true;
		case outland::gpu::descriptor_type_depth_stencil:
			vk_type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
			return true;
		case outland::gpu::descriptor_type_sampler:
			vk_type = VK_DESCRIPTOR_TYPE_SAMPLER;
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------
	void	to_shader_stage(shader_stage_mask_t stage, VkShaderStageFlags& vk_stage)
	{
		vk_stage = 0;
		if (shader_stage_vertex & stage)
			vk_stage |= VK_SHADER_STAGE_VERTEX_BIT;
		if (shader_stage_geometry & stage)
			vk_stage |= VK_SHADER_STAGE_GEOMETRY_BIT;
		if (shader_stage_fragment & stage)
			vk_stage |= VK_SHADER_STAGE_FRAGMENT_BIT;
		if (shader_stage_compute & stage)
			vk_stage |= VK_SHADER_STAGE_COMPUTE_BIT;
	}
	
	//--------------------------------------------------
	bool	to_layout(descriptor_type_t type, VkImageLayout& vk_layout)
	{
		switch (type)
		{
		case outland::gpu::descriptor_type_sampled_texture:
			vk_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::descriptor_type_uniform_buffer:
			break;
		case outland::gpu::descriptor_type_storage_buffer:
			break;
		case outland::gpu::descriptor_type_render_target:
			vk_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::descriptor_type_depth_stencil:
			vk_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
			return true;
		case outland::gpu::descriptor_type_sampler:
			break;
		default:
			return false;
		}

		vk_layout = VK_IMAGE_LAYOUT_UNDEFINED;
		return true;
	}

	//--------------------------------------------------
	adapter_id			to_adapter(VkPhysicalDevice device)
	{
		return static_cast<adapter_id>(reinterpret_cast<uintptr_t>(device));
	}

	//--------------------------------------------------
	VkPhysicalDevice 	to_physical_device(adapter_id adapter)
	{
		return reinterpret_cast<VkPhysicalDevice>(static_cast<uintptr_t>(adapter));
	}

} } }
