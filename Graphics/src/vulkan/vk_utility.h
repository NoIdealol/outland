#pragma once
#include "config.h"
#include "vulkan.h"
#include "module.h"

namespace outland { namespace gpu { namespace util
{
	bool		to_format(VkFormat vk_fmt, format_t& fmt);
	bool		to_format(format_t fmt, VkFormat& vk_fmt);
	error_t		to_error(VkResult result);
	bool		to_present_mode(VkPresentModeKHR vk_mode, present_mode_t& mode);
	bool		to_present_mode(present_mode_t mode, VkPresentModeKHR& vk_mode);
	bool		find_memory_index(VkPhysicalDeviceMemoryProperties const& props, VkMemoryPropertyFlags flags, uint32_t type_bits, uint32_t& memory_index);
	bool		get_format_texel_info(VkFormat format, uint32_t& texel_block_byte_size, uint32_t& texel_block_pixel_size);
	bool		to_primitive_topology(primitive_topology_t topology, VkPrimitiveTopology& vk_topology);
	bool		to_polygon_mode(fill_mode_t mode, VkPolygonMode& vk_mode);
	bool		to_cull_mode(cull_mode_t mode, VkCullModeFlags& vk_mode);
	bool		to_compare_operation(compare_operation_t operation, VkCompareOp& vk_operation);
	bool		to_blend_operation(blend_operation_t operation, VkBlendOp& vk_operation);
	bool		to_blend_factor(blend_factor_t factor, VkBlendFactor& vk_factor, bool is_alpha);
	bool		to_sampler_address_mode(address_mode_t mode, VkSamplerAddressMode& vk_mode);
	bool		to_filter(filter_t filter, VkFilter& vk_filter);
	bool		to_sampler_mip_mode(filter_t filter, VkSamplerMipmapMode& vk_filter);
	bool		to_border_color(border_color_t color, VkBorderColor& vk_color);
	bool		to_image_type(texture_dimension_t dim, VkImageType& vk_type);
	bool		to_image_view_type(texture_shader_view_type_t type, VkImageViewType& vk_type);
	void		to_image_aspect(texture_aspect_mask_t aspect, VkImageAspectFlags& vk_aspect);

	bool	to_access(render_target_access_t access, VkAccessFlags& vk_access);
	bool	to_access(depth_stencil_access_t access, VkAccessFlags& vk_access);
	void	to_stage(shader_stage_mask_t stage, VkPipelineStageFlags& vk_stage);
	bool	to_stage(render_target_access_t access, VkPipelineStageFlags& vk_stage);
	bool	to_stage(depth_stencil_access_t access, VkPipelineStageFlags& vk_stage);
	bool	to_layout(render_target_access_t access, VkImageLayout& vk_layout);
	bool	to_layout(depth_stencil_access_t access, VkImageLayout& vk_layout);
	bool	to_attachment_load(attachment_load_t load, VkAttachmentLoadOp& op);
	bool	to_attachment_store(attachment_store_t store, VkAttachmentStoreOp& op);
	bool	to_descriptor_type(descriptor_type_t type, VkDescriptorType& vk_type);
	void	to_shader_stage(shader_stage_mask_t stage, VkShaderStageFlags& vk_stage);
	bool	to_layout(descriptor_type_t type, VkImageLayout& vk_layout);
	
	adapter_id			to_adapter(VkPhysicalDevice device);
	VkPhysicalDevice 	to_physical_device(adapter_id adapter);
	
} } }
