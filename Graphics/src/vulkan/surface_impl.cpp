#include "pch.h"
#include "config.h"

#ifdef VK_USE_SWAP_CHAIN_EXTENSION

	#if O_OSYSTEM_WINDOWS

		#define VK_USE_PLATFORM_WIN32_KHR

	#elif O_OSYSTEM_LINUX

		#define VK_USE_PLATFORM_XCB_KHR
		//#define VK_USE_PLATFORM_XLIB_KHR

	#else

		static_assert(false, "platform not implemented");

	#endif

#endif

#include "vulkan.h"
#include "surface_impl.h"

namespace outland { namespace gpu
{

#define VK_SURFACE_LOAD(fnc) \
	if (nullptr == (fnc = reinterpret_cast<PFN_##fnc>(_instance->library()->vtable()->vkGetInstanceProcAddr(_instance->instance(), #fnc)))) \
	{ \
		return VK_ERROR_INITIALIZATION_FAILED; \
	}

#if defined(VK_USE_SWAP_CHAIN_EXTENSION)

	#if defined(VK_USE_PLATFORM_WIN32_KHR)

		char8_t const* const VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME = VK_KHR_WIN32_SURFACE_EXTENSION_NAME;
		#define VK_FNC_INSTANCE_SURFACE(DO_MACRO) \
			DO_MACRO(vkCreateWin32SurfaceKHR)

		VkResult	surface_t::init(void const* window_context_ptr, void const* window_instance_ptr)
		{
			VK_FNC_INSTANCE_SURFACE(VK_FNC_PROTOTYPE);
			VK_FNC_INSTANCE_SURFACE(VK_SURFACE_LOAD);

			HINSTANCE h_instance = *reinterpret_cast<HINSTANCE const*>(window_context_ptr);
			HWND h_wnd = *reinterpret_cast<HWND const*>(window_instance_ptr);

			VkWin32SurfaceCreateInfoKHR surface_create_info;
			surface_create_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;	// VkStructureType                  sType
			surface_create_info.pNext = nullptr;											// const void                      *pNext
			surface_create_info.flags = 0;													// VkWin32SurfaceCreateFlagsKHR     flags
			surface_create_info.hinstance = h_instance;										// HINSTANCE                        hinstance
			surface_create_info.hwnd = h_wnd;												// HWND                             hwnd

			return vkCreateWin32SurfaceKHR(_instance->instance(), &surface_create_info, _instance->allocation_cb(), &_surface);
		}

	#elif defined(VK_USE_PLATFORM_XCB_KHR)

		char8_t const* const VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME = VK_KHR_XCB_SURFACE_EXTENSION_NAME;
		#define VK_FNC_INSTANCE_SURFACE(DO_MACRO) \
			DO_MACRO(vkCreateXcbSurfaceKHR)

		VkResult	surface_t::init(void const* window_context_ptr, void const* window_instance_ptr)
		{
			VK_FNC_INSTANCE_SURFACE(VK_FNC_PROTOTYPE);
			VK_FNC_INSTANCE_SURFACE(VK_SURFACE_LOAD);

			xcb_connection_t p_connection = *reinterpret_cast<xcb_connection_t const*>(window_context_ptr);;
			xcb_window_t h_window = *reinterpret_cast<xcb_window_t const*>(window_instance_ptr);

			VkXcbSurfaceCreateInfoKHR surface_create_info;
			surface_create_info.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;		// VkStructureType                sType
			surface_create_info.pNext = nullptr;											// const void                    *pNext
			surface_create_info.flags = 0;													// VkXlibSurfaceCreateFlagsKHR    flags
			surface_create_info.connection = p_connection;									// xcb_connection_t*              connection
			surface_create_info.window = h_window;											// xcb_window_t                   window

			return vkCreateXcbSurfaceKHR(_instance->instance(), &surface_create_info, _instance->allocation_cb(), &_surface);
		}

	#elif defined(VK_USE_PLATFORM_XLIB_KHR)

		char8_t const* const VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME = VK_KHR_XLIB_SURFACE_EXTENSION_NAME;
		#define VK_FNC_INSTANCE_SURFACE(DO_MACRO) \
			DO_MACRO(vkCreateXlibSurfaceKHR)

		VkResult	surface_t::init(void const* window_context_ptr, void const* window_instance_ptr)
		{
			VK_FNC_INSTANCE_SURFACE(VK_FNC_PROTOTYPE);
			VK_FNC_INSTANCE_SURFACE(VK_SURFACE_LOAD);

			Display p_display = *reinterpret_cast<Display*>(window_context_ptr);;
			Window h_window = *reinterpret_cast<Window*>(window_instance_ptr);

			VkXlibSurfaceCreateInfoKHR surface_create_info;
			surface_create_info.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;		// VkStructureType                sType
			surface_create_info.pNext = nullptr;											// const void                    *pNext
			surface_create_info.flags = 0;													// VkXlibSurfaceCreateFlagsKHR    flags
			surface_create_info.dpy = p_display;											// Display                       *dpy
			surface_create_info.window = h_window;											// Window                         window

			return vkCreateXlibSurfaceKHR(_instance->instance(), &surface_create_info, _instance->allocation_cb(), &_surface);
		}

	#else

		char8_t const* const VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME = nullptr;
		#define VK_FNC_INSTANCE_SURFACE(DO_MACRO)
		static_assert(false, "not implemented");

	#endif

		void		surface_t::clean()
		{
			_instance->vtable()->vkDestroySurfaceKHR(_instance->instance(), _surface, _instance->allocation_cb());
		}

#endif

	surface_t::surface_t(vk_instance_t* instance)
		: _instance(instance)
	{

	}

	VkResult	surface_t::get_capabilities(VkPhysicalDevice physical_device, vk_surface_capabilities_t& capabilities)
	{
		VkResult vk_result = VK_SUCCESS;

		vk_result =_instance->vtable()->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, _surface, &capabilities.surface_capabilities);
		if (vk_result)
			return vk_result;

		VkPresentModeKHR	present_mode_array[16];
		uint32_t			present_mode_size = 0;

		vk_result = _instance->vtable()->vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, _surface, &present_mode_size, nullptr);
		if (vk_result)
			return vk_result;

		if (u::array_size(present_mode_array) < present_mode_size)
			return VK_ERROR_OUT_OF_HOST_MEMORY;
		
		vk_result = _instance->vtable()->vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, _surface, &present_mode_size, present_mode_array);
		if (vk_result)
			return vk_result;

		capabilities.present_mode_size = 0;
		for (uint32_t i = 0; i < present_mode_size; ++i)
		{
			VkPresentModeKHR mode = present_mode_array[i];
			switch (mode)
			{
			case VK_PRESENT_MODE_IMMEDIATE_KHR:
			case VK_PRESENT_MODE_MAILBOX_KHR:
			case VK_PRESENT_MODE_FIFO_KHR:
				capabilities.present_mode_array[capabilities.present_mode_size] = mode;
				++capabilities.present_mode_size;
				break;
			default:
				break;
			}
		}

		return VK_SUCCESS;
	}

	VkResult surface_t::get_valid_format(
		VkPhysicalDevice physical_device,
		VkSurfaceFormatKHR const* prefered_surface_format_array,
		size_t prefered_surface_format_size,
		VkSurfaceFormatKHR& valid_format,
		scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock(scratch);

		VkResult vk_result = VK_SUCCESS;

		VkSurfaceFormatKHR* valid_surface_format_array = nullptr;
		uint32_t valid_surface_format_size = 0;

		vk_result = _instance->vtable()->vkGetPhysicalDeviceSurfaceFormatsKHR(
			physical_device,
			_surface,
			&valid_surface_format_size,
			valid_surface_format_array);

		if (vk_result)
			return vk_result;

		error_t err = u::mem_alloc<VkSurfaceFormatKHR>(scratch, valid_surface_format_array, valid_surface_format_size);
		if (err)
			return VK_ERROR_OUT_OF_HOST_MEMORY;

		vk_result = _instance->vtable()->vkGetPhysicalDeviceSurfaceFormatsKHR(
			physical_device,
			_surface,
			&valid_surface_format_size,
			valid_surface_format_array);

		if (vk_result)
			return vk_result;

		if ((valid_surface_format_size == 1) &&
			(valid_surface_format_array[0].format == VK_FORMAT_UNDEFINED))
		{
			// If the list contains only one entry with undefined format
			// it means that there are no preferred surface formats and any can be chosen
			valid_format = prefered_surface_format_array[0];
			return VK_SUCCESS;
		}
		else
		{
			for (size_t i = 0; i < prefered_surface_format_size; ++i)
			{
				for (uint32_t j = 0; j < valid_surface_format_size; ++j)
				{
					if ((prefered_surface_format_array[i].format == valid_surface_format_array[j].format) &&
						(prefered_surface_format_array[i].colorSpace == valid_surface_format_array[j].colorSpace))
					{
						valid_format = valid_surface_format_array[j];
						return VK_SUCCESS;
					}
				}
			}

			for (size_t i = 0; i < prefered_surface_format_size; ++i)
			{
				for (uint32_t j = 0; j < valid_surface_format_size; ++j)
				{
					if (prefered_surface_format_array[i].format == valid_surface_format_array[j].format)
					{
						valid_format = valid_surface_format_array[j];
						return VK_SUCCESS;
					}
				}
			}
		}

		return VK_ERROR_FORMAT_NOT_SUPPORTED;
	}

	VkResult	surface_t::enumerate_format(VkPhysicalDevice physical_device, array<VkSurfaceFormatKHR>& surface_format_array)
	{
		VkResult vk_result = VK_SUCCESS;

		//VkSurfaceFormatKHR* valid_surface_format_array = nullptr;
		uint32_t surface_format_size = 0;

		vk_result = _instance->vtable()->vkGetPhysicalDeviceSurfaceFormatsKHR(
			physical_device,
			_surface,
			&surface_format_size,
			nullptr);

		if (vk_result)
			return vk_result;

		if (surface_format_array.resize(surface_format_size))
			return VK_ERROR_OUT_OF_HOST_MEMORY;
		
		vk_result = _instance->vtable()->vkGetPhysicalDeviceSurfaceFormatsKHR(
			physical_device,
			_surface,
			&surface_format_size,
			surface_format_array.data());

		surface_format_array.resize(surface_format_size);

		return vk_result;
	}

	//--------------------------------------------------
	error_t surface::get_caps(surface_id surface, adapter_id adapter, surface_caps_t& caps)
	{
		VkPhysicalDevice vk_physical_device = util::to_physical_device(adapter);

		vk_surface_capabilities_t vk_caps;
		VkResult vk_result = surface->get_capabilities(vk_physical_device, vk_caps);
		if (vk_result)
			return util::to_error(vk_result);

		caps.present_mode_mask = 0;
		for (size_t i = 0; i < vk_caps.present_mode_size; ++i)
		{
			present_mode_t mode;
			if (util::to_present_mode(vk_caps.present_mode_array[i], mode))
			{
				caps.present_mode_mask |= mode;
			}
		}

		VkImageUsageFlags const usageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		if (usageFlags != (usageFlags & vk_caps.surface_capabilities.supportedUsageFlags))
			return err_not_implemented;

		caps.back_buffer_min_count = vk_caps.surface_capabilities.minImageCount;
		caps.back_buffer_max_count = vk_caps.surface_capabilities.maxImageCount;

		caps.back_buffer_min_width = vk_caps.surface_capabilities.minImageExtent.width;
		caps.back_buffer_min_height = vk_caps.surface_capabilities.minImageExtent.height;

		caps.back_buffer_max_width = vk_caps.surface_capabilities.maxImageExtent.width;
		caps.back_buffer_max_height = vk_caps.surface_capabilities.maxImageExtent.height;

		return err_ok;
	}

	//--------------------------------------------------
	error_t surface::format_enumerate(surface_id surface, adapter_id adapter, format_t* format_array, size_t format_capacity, size_t& format_size, scratch_allocator_t* scratch)
	{
		scratch_lock_t scratch_lock = { scratch };

		VkPhysicalDevice vk_physical_device = util::to_physical_device(adapter);

		VkResult vk_result;

		array<VkSurfaceFormatKHR> valid_surface_format_array = { scratch };
		vk_result = surface->enumerate_format(vk_physical_device, valid_surface_format_array);
		if (vk_result)
			return util::to_error(vk_result);

		format_size = 0;
		if (nullptr == format_array)
		{
			for (size_t i = 0; i < valid_surface_format_array.size(); ++i)
			{
				format_t fmt;
				if (VK_COLOR_SPACE_SRGB_NONLINEAR_KHR == valid_surface_format_array[i].colorSpace &&
					util::to_format(valid_surface_format_array[i].format, fmt))
				{
					++format_size;
				}
			}
			return err_ok;
		}

		for (size_t i = 0; i < valid_surface_format_array.size() && 0 < format_capacity; ++i, --format_capacity, ++format_array)
		{
			format_t fmt;
			if (VK_COLOR_SPACE_SRGB_NONLINEAR_KHR == valid_surface_format_array[i].colorSpace &&
				util::to_format(valid_surface_format_array[i].format, fmt))
			{
				*format_array = fmt;
				++format_size;
			}
		}

		return err_ok;
	}
} }
