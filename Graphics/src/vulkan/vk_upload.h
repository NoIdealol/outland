#pragma once
#include "vk_device.h"
#include "math\integer.h"
#include "resource_impl.h"

namespace outland { namespace gpu 
{

	class vk_upload_t
	{
		enum : uint32_t
		{
			_copy_ctx_size = 2,
			_copy_frame_size = _copy_ctx_size * 2, //double buffer

			_frame_batch_size = 32,
			_max_mip_size = 16,
		};

		enum : VkDeviceSize
		{
			_buffer_copy_align = 16,
		};

		struct buffer_info_t
		{
			buffer_t*	buffer;
			void*		user_data;
		};

		struct image_info_t
		{
			texture_t*				texture;
			VkImageSubresourceRange	range;
			void*					user_data;
		};

		struct frame_t
		{
			buffer_info_t		buffer_array[_frame_batch_size];
			image_info_t		image_array[_frame_batch_size];
			size_t				buffer_size;
			size_t				image_size;
			VkCommandBuffer		cmd_buffer;
			VkFence				fence;
		};

		struct ctx_t
		{
			VkCommandBuffer		cmd_buffer;
			VkDeviceSize		buffer_offset;
			frame_t*			frame;
		};

		struct copy_cache_t
		{
			VkBufferCopy		buffer_array[_frame_batch_size];
			VkBufferImageCopy	image_array[_frame_batch_size * _max_mip_size];
			VkDeviceSize		size;
		};

		

		vk_device_t*		_device;
		VkCommandPool		_cmd_pool_copy;
		VkCommandPool		_cmd_pool_graphics;
		VkDeviceMemory		_memory;
		void*				_ptr;
		VkBuffer			_buffer;
		VkDeviceSize		_submit_size;
	
		frame_t				_frame_array[_copy_frame_size];
		ctx_t				_ctx_array[_copy_ctx_size];
		copy_cache_t		_cache;

		deque<frame_t*>		_frame_free_queue;
		deque<frame_t*>		_frame_submited_queue;
		deque<frame_t*>		_frame_clean_queue;
		deque<ctx_t*>		_ctx_free_queue;
		deque<ctx_t*>		_ctx_submited_queue;
		ctx_t*				_ctx;

	private:	
		VkResult	_is_done(frame_t* frame);
		VkResult	_is_done(ctx_t* ctx);
		VkResult	_flush(frame_t* frame);
		VkResult	_flush(ctx_t* ctx);
		VkResult	_reset(ctx_t* ctx);
		void		_cmd(ctx_t* ctx, frame_t* frame);

		VkResult	_texture_copy(host_to_texture_copy_t const& host_copy, VkBufferImageCopy* copy_array, image_info_t& info, VkDeviceSize& current_size, VkDeviceSize buffer_start) const;
		VkResult	_buffer_copy(host_to_buffer_copy_t const& host_copy, VkBufferCopy* copy_array, buffer_info_t& info, VkDeviceSize& current_size, VkDeviceSize buffer_start) const;

	public:
		vk_upload_t(vk_device_t* device);
		VkResult	init(VkDeviceSize buffer_size);
		void		clean();

		VkResult	update(device_event_t* event_array, size_t event_capacity, size_t& event_size);
		VkResult	buffer_copy(host_to_buffer_copy_t const& copy);
		VkResult	texture_copy(host_to_texture_copy_t const& copy);
	};



} }