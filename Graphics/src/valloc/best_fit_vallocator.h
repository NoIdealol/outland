#pragma once
#include "constants.h"

namespace outland { namespace gpu 
{
	//////////////////////////////////////////////////
	// VALLOCATOR
	//////////////////////////////////////////////////

	//--------------------------------------------------
	class vallocator_t
	{
	public:
		virtual error_t		alloc(uint64_t& offset, uint64_t size, uint64_t align) = 0;
		virtual void		free(uint64_t offset, uint64_t size) = 0;
	};

	//////////////////////////////////////////////////
	// BEST FIT VALLOCATOR
	//////////////////////////////////////////////////

	class best_fit_vallocator_t : public vallocator_t
	{
	public:
		struct space_t
		{
			uint64_t	offset;
			uint64_t	size;
		};

	private:
		uint64_t			_memory_size;
		uint64_t			_page_size;
		array<space_t>		_space_array;
		size_t				_alloc_size;

	public:
		best_fit_vallocator_t(allocator_t* allocator);
		~best_fit_vallocator_t();

		error_t		init(uint64_t memory_size, uint64_t page_size);

		error_t		alloc(uint64_t& offset, uint64_t size, uint64_t align) final;
		void		free(uint64_t offset, uint64_t size) final;
	};

} }
