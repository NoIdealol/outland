#include "pch.h"
#include "best_fit_vallocator.h"
#include "math\integer.h"

namespace outland { namespace gpu 
{
#if 0
	bool heap_vallocator_test(allocator_t* allocator)
	{
		video::heap_vallocator::create_info_t info;
		info.allocator = allocator;
		info.page_size = 16;

		array<video::heap_vallocator_t::space_t> space_array = { allocator };
		video::heap_vallocator_t vallocator = { info, 1 << 16, space_array };

		uint64_t rng = 0xAF5931D7C_u64;

		struct alloc_t
		{
			uint64_t	offset;
			uint64_t  size;
		};

		array<alloc_t> mem_arr = { allocator };
		error_t err;

		for (size_t i = 0; i < 16; ++i)
		{
			alloc_t alloc;
			alloc.size = (rand(rng) & 0xFF) | 4;
			uint32_t alloc_align = 8 << (rand(rng) & 0x3);

			err = vallocator.alloc(alloc.offset, alloc.size, alloc_align);
			O_ASSERT(err_ok == err);
			err = mem_arr.push_back(alloc);
			O_ASSERT(err_ok == err);
		}

		for (size_t i = 0; i < 64; ++i)
		{
			for (size_t j = 0; j < 3; ++j)
			{
				uint32_t idx = rand(rng) % mem_arr.size();
				vallocator.free(mem_arr[idx].offset, mem_arr[idx].size);
				mem_arr.remove_swap(idx);
			}

			for (size_t j = 0; j < 3; ++j)
			{
				alloc_t alloc;
				alloc.size = (rand(rng) & 0xFF) | 4;
				uint32_t alloc_align = 8 << (rand(rng) & 0x3);

				err = vallocator.alloc(alloc.offset, alloc.size, alloc_align);
				O_ASSERT(err_ok == err);
				err = mem_arr.push_back(alloc);
				O_ASSERT(err_ok == err);
			}


		}

		for (auto& it : mem_arr)
		{
			for (size_t i = 1; i < mem_arr.size(); ++i)
			{
				auto& it1 = mem_arr[i - 1];
				auto& it2 = mem_arr[i];

				if (it1.offset > it2.offset)
				{
					mem_arr.swap(&it1, &it2);
				}
			}

		}

		for (size_t i = 1; i < mem_arr.size(); ++i)
		{
			auto& it1 = mem_arr[i - 1];
			auto& it2 = mem_arr[i];

			if ((it1.offset + it1.size) > it2.offset)
			{
				O_ASSERT(false);
				return false;
			}
		}

		/*uint64_t sum = 0;
		string8_t out = { main_args.allocator };
		for (auto& it : mem_arr)
		{
		sum += it.size;

		out.clear();
		f::print(out, "allocation: offset: %d size: %d end: %d\n", it.mem.offset, it.size, it.mem.offset + it.size);
		os::debug_string(out.data());
		}

		out.clear();
		f::print(out, "allocation: total size: %d \n", sum);
		os::debug_string(out.data());*/

		for (auto& it : mem_arr)
		{
			vallocator.free(it.offset, it.size);
		}
		mem_arr.clear();

		return true;
	}
#endif

	//--------------------------------------------------
	best_fit_vallocator_t::best_fit_vallocator_t(allocator_t* allocator)
		: _memory_size(0)
		, _alloc_size(0)
		, _page_size(0)
		, _space_array(allocator)
	{
		
	}

	//--------------------------------------------------
	best_fit_vallocator_t::~best_fit_vallocator_t()
	{

	}

	//--------------------------------------------------
	error_t		best_fit_vallocator_t::init(uint64_t memory_size, uint64_t page_size)
	{
		error_t err;
		uint64_t page_count = memory_size / page_size;
		err = _space_array.reserve(static_cast<size_t>(page_count >> 1) + 1);
		if (err)
			return err;

		_memory_size = page_count * page_size;
		_page_size = page_size;
		
		space_t space;
		space.offset = 0;
		space.size = _memory_size;

		err = _space_array.push_back(space);
		O_ASSERT(err_ok == err);

		return err_ok;
	}

	//--------------------------------------------------
	error_t		best_fit_vallocator_t::alloc(uint64_t& offset, uint64_t size, uint64_t align)
	{
		error_t err;

		//align size to page
		size = m::align(size, _page_size);

		uint64_t min_offset = _memory_size + 1;
		uint64_t min_remainder = _memory_size + 1;
		space_t* best_fit = nullptr;

		for (auto& it : _space_array)
		{
			uint64_t align_offset = m::align(it.offset, align) - it.offset;
			if (it.size >= (align_offset + size))	//has sufficient size
			{
				uint64_t remainder = it.size - (align_offset + size);

				if (min_offset > align_offset) //minimize align fragmentation
				{
					best_fit = &it;
					min_offset = align_offset;
					min_remainder = remainder;
				}
				else if ((min_offset == align_offset) && (min_remainder > remainder)) //minimize remainder
				{
					best_fit = &it;
					min_remainder = remainder;
				}
			}
		}

		if (nullptr == best_fit)
			return err_out_of_vram;

		O_ASSERT(0 == (min_offset & (_page_size - 1))); //is mul of page

		++_alloc_size;

		uint64_t mem_begin = best_fit->offset + min_offset;
		uint64_t mem_end = mem_begin + size;
		uint64_t best_fit_end = best_fit->offset + best_fit->size;

		if (mem_end == best_fit_end)
		{
			if (min_offset)
			{
				best_fit->size = min_offset;
			}
			else
			{
				_space_array.remove_swap(best_fit);
			}
		}
		else
		{
			if (min_offset)
			{
				best_fit->size = min_offset;

				space_t remainder;
				remainder.offset = mem_end;
				remainder.size = best_fit_end - mem_end;

				err = _space_array.push_back(remainder);
				O_ASSERT(err_ok == err);
			}
			else
			{
				best_fit->size = best_fit_end - mem_end;
				best_fit->offset = mem_end;
			}
		}

		offset = mem_begin;

		return err_ok;
	}

	//--------------------------------------------------
	void		best_fit_vallocator_t::free(uint64_t offset, uint64_t size)
	{
		O_ASSERT(m::align(offset, _page_size) == offset);

		//align size to page
		size = m::align(size, _page_size);
		uint64_t mem_begin = offset;
		uint64_t mem_end = offset + size;
		space_t* merge = nullptr;

		if (0 != mem_begin)
		{
			for (auto& it : _space_array)
			{
				if (mem_begin == (it.offset + it.size))
				{
					it.size += mem_end - mem_begin;
					merge = &it;
					break;
				}
			}
		}

		if (_memory_size != mem_end)
		{
			for (auto& it : _space_array)
			{
				if (mem_end == it.offset)
				{
					if (merge)
					{
						merge->size += it.size;
						_space_array.remove_swap(&it);
					}
					else
					{
						it.offset = mem_begin;
						it.size += mem_end - mem_begin;
						merge = &it;
					}

					break;
				}
			}
		}

		--_alloc_size;

		if (merge)
			return;

		space_t space;
		space.offset = mem_begin;
		space.size = mem_end - mem_begin;

		error_t err = _space_array.push_back(space);
		O_ASSERT(err_ok == err);
	}

} }
