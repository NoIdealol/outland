#include "pch.h"
#include "main\main.h"
#include "application.h"

namespace outland
{
	uint8_t main(main_args_t const& main_args)
	{
		fluid::demobit_application_t app(main_args.allocator);
		return os::app::run(&app, main_args.cmd_count, main_args.cmd_line);
	}
}