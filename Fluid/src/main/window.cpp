#include "pch.h"
#include "window.h"

namespace fluid
{
	//--------------------------------------------------
	window_t::window_t(application_view_t* game_view)
		: _back_buffer(os::texture::null)
		, _is_tracking(false)
		, _application_view(game_view)
		, _is_active(false)
		, _is_minimized(false)
	{

	}

	//--------------------------------------------------
	void window_t::present(os::window_id window, os::texture_id back_buffer)
	{
		error_t err;
		os::surface_id surface;
		os::rect_t dst_rect;
		os::point_t src_base;
		os::bitmap_t bitmap;

		_back_buffer = back_buffer;
		os::texture::get_bitmap(_back_buffer, bitmap);

		src_base.x = 0;
		src_base.y = 0;
		dst_rect.base.x = 0;
		dst_rect.base.y = 0;
		dst_rect.size.x = static_cast<os::coord_t>(bitmap.width);
		dst_rect.size.y = static_cast<os::coord_t>(bitmap.height);

		err = os::surface::draw_begin(surface, window);
		if (err)
			return;

		os::surface::draw_texture(surface, dst_rect, src_base, _back_buffer);

		os::surface::draw_end(surface);
	}

	//--------------------------------------------------
	void window_t::on_create(os::window_id window)
	{
		
	}

	//--------------------------------------------------
	void window_t::on_close(os::window_id window)
	{
		_application_view->on_view_close_request();
	}

	//--------------------------------------------------
	void window_t::on_destroy(os::window_id window)
	{
		_application_view->on_view_destroy();
	}

	//--------------------------------------------------
	void window_t::on_activate(os::window_id window, os::window_id other_window)
	{
		_is_active = true;
		if (_is_minimized)
			return;

		_application_view->on_view_active();
	}

	//--------------------------------------------------
	void window_t::on_deactivate(os::window_id window, os::window_id other_window)
	{
		_is_active = false;
		if (_is_minimized)
			return;

		_application_view->on_view_background();
	}

	//--------------------------------------------------
	void window_t::on_minimize(os::window_id window)
	{
		_is_minimized = true;
		_application_view->on_view_hidden();
	}

	//--------------------------------------------------
	void window_t::on_maximize(os::window_id window, const os::point_t& client_size)
	{
		_back_buffer = os::texture::null;
		_application_view->on_view_resize(static_cast<size_t>(client_size.x), static_cast<size_t>(client_size.y));
	}

	//--------------------------------------------------
	void window_t::on_resize(os::window_id window, const os::point_t& client_size)
	{
		if (_is_minimized)
		{
			_is_minimized = false;
			if (_is_active)
				_application_view->on_view_active();
			else
				_application_view->on_view_background();
		}

		if (_is_tracking)
			return;

		_back_buffer = os::texture::null;
		_application_view->on_view_resize(static_cast<size_t>(client_size.x), static_cast<size_t>(client_size.y));
	}

	//--------------------------------------------------
	void window_t::on_tracking_begin(os::window_id window)
	{
		_is_tracking = true;
	}

	//--------------------------------------------------
	void window_t::on_tracking_end(os::window_id window)
	{
		error_t err;

		_is_tracking = false;
		_back_buffer = os::texture::null;

		os::point_t client_size;
		err = os::window::get_client_size(window, client_size);
		if (err)
			return;

		_application_view->on_view_resize(static_cast<size_t>(client_size.x), static_cast<size_t>(client_size.y));
	}

	//--------------------------------------------------
	void window_t::on_key_down(os::window_id window, os::hid::usage_t key)
	{
		switch (key)
		{
		case os::hid::keyboard::key_escape:
			_application_view->on_view_close_request();
			break;
		/*case os::hid::keyboard::key_W:
			_controller.on_forward_down();
			break;
		case os::hid::keyboard::key_S:
			_controller.on_back_down();
			break;
		case os::hid::keyboard::key_A:
			_controller.on_left_down();
			break;
		case os::hid::keyboard::key_D:
			_controller.on_right_down();
			break;*/
		default:
			_application_view->on_input(key);
			break;
		}
	}

	//--------------------------------------------------
	void window_t::on_key_up(os::window_id window, os::hid::usage_t key)
	{
		switch (key)
		{
		/*case os::hid::keyboard::key_W:
			_controller.on_forward_up();
			break;
		case os::hid::keyboard::key_S:
			_controller.on_back_up();
			break;
		case os::hid::keyboard::key_A:
			_controller.on_left_up();
			break;
		case os::hid::keyboard::key_D:
			_controller.on_right_up();
			break;*/
		default:
			break;
		}
	}

	//--------------------------------------------------
	void window_t::on_mouse_down(os::window_id window, os::hid::usage_t button, const  os::point_t& position)
	{
	}

	//--------------------------------------------------
	void window_t::on_frame_paint(os::window_id window, os::surface_id surface) const
	{
		//os::surface::clear(surface, 0xFF00AA00);
	}

	//--------------------------------------------------
	void window_t::on_paint(os::window_id window, os::surface_id surface) const
	{
		if ((os::texture::null == _back_buffer) || _is_tracking)
		{
			os::surface::clear(surface, 0x303030);
			return;
		}

		os::bitmap_t bitmap;
		os::texture::get_bitmap(_back_buffer, bitmap);

		os::point_t src;
		src.x = 0;
		src.y = 0;

		os::rect_t dst;
		dst.base.x = 0;
		dst.base.y = 0;
		dst.size.x = static_cast<os::coord_t>(bitmap.width);
		dst.size.y = static_cast<os::coord_t>(bitmap.height);

		os::surface::draw_texture(surface, dst, src, _back_buffer);
	}



}