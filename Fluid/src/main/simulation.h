#pragma once
#include "system\gui\surface.h"
#include "system\application\time.h"
#include "system\input\keyboard.h"

namespace fluid
{

	class simulation_t
	{
	public:
		static void		get_memory_info(memory_info_t& memory_info);
		static error_t	create(simulation_t*& simulation, void* memory);
		static void		destroy(simulation_t* simulation);

		virtual error_t	init(size_t width, size_t height, allocator_t* allocator) = 0;
		virtual void	simulate(os::time_t dt_ms) = 0;
		virtual void	render(os::texture_id& back_buffer) = 0;
		virtual void	on_input(os::hid::usage_t key) = 0;
	};

}
