#pragma once
#include "simulation.h"
#include "math\fixed.h"
#include "math\vector2.h"
#include "math\floating.h"
#include "math\integer.h"

namespace fluid
{
	using coord_t = float32_t;
	using vec2_t = vector2<coord_t>;

	class simple_fluid_t : public simulation_t
	{
	private:
		os::texture_id	_back_buffer;
		allocator_t*	_allocator;

	public:
		simple_fluid_t();
		~simple_fluid_t();

		error_t	init(size_t width, size_t height, allocator_t* allocator) final;
		void	simulate(os::time_t dt_ms) final;
		void	render(os::texture_id& back_buffer) final;
		void	on_input(os::hid::usage_t key) final;

		void	sim_step(os::time_t dt_ms);
	
	};

}
