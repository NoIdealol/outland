#include "pch.h"
#include "simple_fluid.h"

namespace fluid
{
	enum : size_t
	{
		grid_size_x = 64,
		grid_size_y = 64,
		grid_mod_x = grid_size_x - 1,
		grid_mod_y = grid_size_y - 1,
	};

#define FOR_EACH_CELL() for(size_t x = 0; x < grid_size_x; ++x) for(size_t y = 0; y < grid_size_y; ++y)
#define FETCH_CELL(data, x, y) data[(x) & grid_mod_x][(y) & grid_mod_y]
#define FETCH_CELL(data, x, y) data[m::max(m::min(int32_t(grid_mod_x), int32_t(x)), int32_t(0))][m::max(m::min(int32_t(grid_mod_y), int32_t(y)), int32_t(0))]
//#define FETCH_CELL(data, x, y) data[(x) & grid_mod_x][m::max(m::min(int32_t(grid_mod_y), int32_t(y)), int32_t(0))]
	
	using data_t = coord_t[grid_size_x][grid_size_y];

	vec2_t to_vy(vec2_t pos) { return pos + vec2_t(coord_t(0.0), coord_t(0.5)); }
	vec2_t to_vx(vec2_t pos) { return pos + vec2_t(coord_t(0.5), coord_t(0.0)); }
	vec2_t from_vy(vec2_t pos) { return pos - vec2_t(coord_t(0.0), coord_t(0.5)); }
	vec2_t from_vx(vec2_t pos) { return pos - vec2_t(coord_t(0.5), coord_t(0.0)); }
	vec2_t v_to_wy(vec2_t pos) { return pos + vec2_t(coord_t(0.5), coord_t(0.0)); }
	vec2_t v_to_wx(vec2_t pos) { return pos + vec2_t(coord_t(0.0), coord_t(0.5)); }
	//vec2_t p_to_w(vec2_t pos) { return pos + vec2_t(coord_t(0.5), coord_t(0.5)); }

	coord_t do_sample(data_t const& data, vec2_t pos)
	{
		//vec2_t pos = { x, y };
		vec2_t ipos = { m::floor(pos[0]), m::floor(pos[1]) };
		vec2_t fpos = pos - ipos;
		vec2_t inv_fpos = { coord_t(1) - fpos[0], coord_t(1) - fpos[1] };

		size_t sx = (int16_t)ipos[0];
		size_t sy = (int16_t)ipos[1];

		return
			FETCH_CELL(data, sx + 0, sy + 0) * inv_fpos[0] * inv_fpos[1] +
			FETCH_CELL(data, sx + 1, sy + 0) * fpos[0] * inv_fpos[1] +
			FETCH_CELL(data, sx + 0, sy + 1) * inv_fpos[0] * fpos[1] +
			FETCH_CELL(data, sx + 1, sy + 1) * fpos[0] * fpos[1];
	}

	void do_external(data_t& heat, data_t& in_x, data_t& in_y)
	{
		coord_t impulse = coord_t(1.7);

		FOR_EACH_CELL()
		{
			vec2_t pos = { coord_t(x), coord_t(y) };
			FETCH_CELL(in_y, x, y) += impulse * do_sample(heat, from_vy(pos));
		}
	}

	void do_vort(coord_t dt_ms, data_t& vort, data_t& in_x, data_t& in_y)
	{
		coord_t mag = coord_t(20.0);
		coord_t radius = coord_t(1.5);

		FOR_EACH_CELL()
		{
			FETCH_CELL(vort, x, y) =
				+ FETCH_CELL(in_y, x, y)
				- FETCH_CELL(in_y, x - 1, y)
				- FETCH_CELL(in_x, x, y)
				+ FETCH_CELL(in_x, x - 1, y);
		}

		FOR_EACH_CELL()
		{
			vec2_t pos = { coord_t(x), coord_t(y) };
			vec2_t right = pos + vec2_t(radius, coord_t(0.0));
			vec2_t left = pos + vec2_t(-radius, coord_t(0.0));
			vec2_t top = pos + vec2_t(coord_t(0.0), radius);
			vec2_t bot = pos + vec2_t(coord_t(0.0), -radius);

			//cross product
			vec2_t wd_x = { m::abs(do_sample(vort, v_to_wx(top))) - m::abs(do_sample(vort, v_to_wx(bot))), m::abs(do_sample(vort, v_to_wx(left))) - m::abs(do_sample(vort, v_to_wx(right))) };
			vec2_t wd_y = { m::abs(do_sample(vort, v_to_wy(top))) - m::abs(do_sample(vort, v_to_wy(bot))), m::abs(do_sample(vort, v_to_wy(left))) - m::abs(do_sample(vort, v_to_wy(right))) };
			
			coord_t mag_x = mag / (m::len(wd_x) + coord_t(0.01)) * wd_x[0];
			coord_t mag_y = mag / (m::len(wd_y) + coord_t(0.01)) * wd_y[1];
			coord_t vort_x = do_sample(vort, v_to_wx(pos));
			coord_t vort_y = do_sample(vort, v_to_wy(pos));
			vort_x = m::max(m::abs(vort_x) - coord_t(3.5), coord_t(0)) * m::sign(vort_x);
			vort_y = m::max(m::abs(vort_y) - coord_t(3.5), coord_t(0)) * m::sign(vort_y);

			FETCH_CELL(in_x, x, y) += mag_x * vort_x * dt_ms * coord_t(0.001);
			FETCH_CELL(in_y, x, y) += mag_y * vort_y * dt_ms * coord_t(0.001);
		}
	}

	void do_advect(data_t& out, data_t& in, data_t const& off_x, data_t const& off_y)
	{
		coord_t dis = coord_t(0.015);

		FOR_EACH_CELL()
		{
			vec2_t pos = { FETCH_CELL(off_x, x, y) + coord_t(x), FETCH_CELL(off_y, x, y) + coord_t(y) };
			FETCH_CELL(out, x, y) = m::max(do_sample(in, pos) - dis, coord_t(0));
		}
	}

	void	do_velocity(data_t& out_x, data_t& out_y, data_t const& in_x, data_t const& in_y)
	{
		FOR_EACH_CELL()
		{
			vec2_t pos = { coord_t(x), coord_t(y) };
			vec2_t pos_x = to_vx(pos);
			vec2_t pos_y = to_vy(pos);

			FETCH_CELL(out_x, x, y) = do_sample(in_x, pos_x);
			FETCH_CELL(out_y, x, y) = do_sample(in_y, pos_y);
		}
	}

	void	do_project(data_t& out_x, data_t& out_y, data_t const& in_x, data_t const& in_y, data_t const& press)
	{
		FOR_EACH_CELL()
		{
			coord_t grad_x = FETCH_CELL(press, x, y) - FETCH_CELL(press, x - 1, y);
			FETCH_CELL(out_x, x, y) = FETCH_CELL(in_x, x, y) - grad_x;

			coord_t grad_y = FETCH_CELL(press, x, y) - FETCH_CELL(press, x, y - 1);
			FETCH_CELL(out_y, x, y) = FETCH_CELL(in_y, x, y) - grad_y;
		}
	}

	void	do_relax(data_t& out, data_t const& init, data_t const& div)
	{
		FOR_EACH_CELL()
		{
			coord_t top = FETCH_CELL(init, x, y + 1);
			coord_t bot = FETCH_CELL(init, x, y - 1);
			coord_t left = FETCH_CELL(init, x - 1, y);
			coord_t right = FETCH_CELL(init, x + 1, y);

			FETCH_CELL(out, x, y) = (right + left + top + bot - FETCH_CELL(div, x, y)) * coord_t(0.25);
		}	
	}

	void do_divergence(data_t& out, data_t const& in_x, data_t const& in_y)
	{
		FOR_EACH_CELL()
		{
			coord_t top = FETCH_CELL(in_y, x, y + 1);
			coord_t bot = FETCH_CELL(in_y, x, y);
			coord_t left = FETCH_CELL(in_x, x, y);
			coord_t right = FETCH_CELL(in_x, x + 1, y);

			FETCH_CELL(out, x, y) = ((right - left) + (top - bot)) * coord_t(0.5);
		}	
	}

	void do_advect(coord_t dt_ms, data_t& off_x, data_t& off_y, data_t& out_x, data_t& out_y, data_t const& in_x, data_t const& in_y)
	{
		size_t advect_count = 4;
		coord_t scale = coord_t(1.0) / coord_t(advect_count);
		coord_t dis = coord_t(0.99);

		FOR_EACH_CELL()
		{
			vec2_t pos = { coord_t(x), coord_t(y) };
			vec2_t start = pos;
			vec2_t pos_x;
			vec2_t pos_y;
			vec2_t vel;

			for (size_t i = 0; i < advect_count; ++i)
			{
				pos_x = to_vx(pos);
				pos_y = to_vy(pos);
				vel = { do_sample(in_x, pos_x), do_sample(in_y, pos_y) };

				pos = pos - vel * dt_ms * coord_t(0.001) * scale;
			}
			//FETCH_CELL(out_x, x, y) = do_sample(in_x, pos);
			//FETCH_CELL(out_y, x, y) = do_sample(in_y, pos);
			FETCH_CELL(off_x, x, y) = pos[0] - start[0];
			FETCH_CELL(off_y, x, y) = pos[1] - start[1];
		}

		FOR_EACH_CELL()
		{
			vec2_t pos = { coord_t(x), coord_t(y) };
			vec2_t pos_x = from_vx(pos);
			vec2_t pos_y = from_vy(pos);

			vec2_t sam_x = { do_sample(off_x, pos_x) + pos[0], do_sample(off_y, pos_x) + pos[1] };
			vec2_t sam_y = { do_sample(off_x, pos_y) + pos[0], do_sample(off_y, pos_y) + pos[1] };
			//sam_x = to_vx(sam_x);
			//sam_y = to_vy(sam_y);

			FETCH_CELL(out_x, x, y) = do_sample(in_x, sam_x) * dis;
			FETCH_CELL(out_y, x, y) = do_sample(in_y, sam_y) * dis;
		}
	
	}

	data_t _do_adv_x = { 0 };
	data_t _do_adv_y = { 0 };
	data_t _do_vel_x = { 0 };
	data_t _do_vel_y = { 0 };
	data_t _do_press = { 0 };
	data_t _do_div = { 0 };
	data_t _do_data = { 0 };
	data_t _do_vort = { 0 };
	uint32_t _do_input = 0;

	void do_input(data_t& vel_x, data_t& vel_y)
	{
		
		coord_t impulse = 100;

		/*for (size_t y = 2; y < 16; ++y)
		{
			for (size_t x = 25; x < 38; ++x)*/

		for (size_t y = 2; y < 16; ++y)
		{
			for (size_t x = 25; x < 38; ++x)
			{
				if (_do_input & (1 << 0))
					_do_data[x][y] = coord_t(1);
				if (_do_input & (1 << 1))
					vel_x[x][y] = -impulse;
				if (_do_input & (1 << 2))
					vel_x[x][y] = impulse;
			}
		}

		
		int32_t radius = 7;
		int32_t center_x = 32;
		int32_t center_y = radius + 3;
		vec2_t center = { coord_t(center_x), coord_t(center_y) };

		for (int32_t x = -radius; x <= radius; ++x)
		{
			for (int32_t y = -radius; y <= radius; ++y)
			{
				vec2_t pos = { coord_t(x), coord_t(y) };

				auto len = m::len(pos);
				if (len < coord_t(radius))
				{
					auto intensity = ((coord_t(radius) - len) / coord_t(radius)) * coord_t(0.6) + coord_t(0.4);
					_do_data[x + center_x][y + center_y] = coord_t(1.0);// intensity;
				}
			}
		}




		_do_input = 0;
	}

	void do_simulate(coord_t dt_ms)
	{

		data_t vel0_x;
		data_t vel0_y;
		do_advect(dt_ms, _do_adv_x, _do_adv_y, vel0_x, vel0_y, _do_vel_x, _do_vel_y);
		do_vort(dt_ms, _do_vort, vel0_x, vel0_y);

		data_t data;
		do_advect(data, _do_data, _do_adv_x, _do_adv_y);
		u::mem_copy(_do_data, data, sizeof(data));
		
		do_external(_do_data, vel0_x, vel0_y);
		do_input(vel0_x, vel0_y);

		do_divergence(_do_div, vel0_x, vel0_y);

		data_t press;

		size_t poisson_round = 4;

		for (size_t i = 0; i < poisson_round; ++i)
		{
			do_relax(press, _do_press, _do_div);
			do_relax(_do_press, press, _do_div);
		}

		do_project(_do_vel_x, _do_vel_y, vel0_x, vel0_y, _do_press);

		//for visual only
		do_divergence(_do_div, _do_vel_x, _do_vel_y);
	}

	


	
	



	void	simulation_t::get_memory_info(memory_info_t& memory_info)
	{
		memory_info.align = alignof(simple_fluid_t);
		memory_info.size = sizeof(simple_fluid_t);
	}

	error_t	simulation_t::create(simulation_t*& simulation, void* memory)
	{
		simulation = O_CREATE(simple_fluid_t, memory);
		return err_ok;
	}

	void	simulation_t::destroy(simulation_t* simulation)
	{
		O_DESTROY(simple_fluid_t, simulation);
	}


	simple_fluid_t::simple_fluid_t()
		: _allocator(nullptr)
		, _back_buffer(os::texture::null)
	{

	}

	simple_fluid_t::~simple_fluid_t()
	{
		if (os::texture::null != _back_buffer)
			os::texture::destroy(_back_buffer);
	}

	error_t	simple_fluid_t::init(size_t width, size_t height, allocator_t* allocator)
	{
		_allocator = allocator;


		os::bitmap_t bitmap;
		bitmap.format = os::bitmap_t::b8_g8_r8_x8;
		bitmap.height = height;
		bitmap.width = width;
		bitmap.pixels = nullptr;
		bitmap.scanline = 4 * width;

		return os::texture::create_dib(_back_buffer, bitmap);
	}

	void	simple_fluid_t::simulate(os::time_t dt_micro)
	{
		sim_step(20000);
	}

	void	simple_fluid_t::on_input(os::hid::usage_t key)
	{
		if (os::hid::keyboard::key_space == key)
			sim_step(16000);

		if (os::hid::keyboard::key_A == key)
		{
			_do_input |= 1 << 0;
		}

		if (os::hid::keyboard::key_B == key)
		{
			_do_input |= 1 << 1;
		}

		if (os::hid::keyboard::key_C == key)
		{
			_do_input |= 1 << 2;
		}
	}

	void	simple_fluid_t::sim_step(os::time_t dt_micro)
	{
		coord_t dt = coord_t((int32_t)dt_micro) * coord_t(0.001);
		coord_t diffusion = 0.7;

		do_simulate(dt);
	}


	void	simple_fluid_t::render(os::texture_id& back_buffer)
	{
		back_buffer = _back_buffer;

		error_t err;
		os::surface_id surface;
		err = os::surface::draw_begin(surface, _back_buffer);
		if (err)
			return;

		os::surface::clear(surface, 0x000000);

		//os::surface::draw_end(surface);
		//return;

		os::coord_t const cell_dim = 0;
		os::coord_t const cell_len = 1 + (cell_dim << 1);
		os::coord_t const grid_spacing = 1 * cell_len;

		os::coord_t const velocity_spacing = grid_spacing;
		os::coord_t const pressure_spacing = velocity_spacing + cell_len * grid_size_x + grid_spacing;
		os::coord_t const divergence_spacing = pressure_spacing + cell_len * grid_size_x + grid_spacing;

		os::color_t fire_col[7] = 
		{
			0x000000,
			0x505050,
			0xFF6000,
			0xFF6000,
			0xFFD000,
			0xFFD000,
			0xFFffFF,
		};

		FOR_EACH_CELL()
		{
			os::point_t p1;

			p1.x = velocity_spacing + x * cell_len;
			p1.y = grid_spacing + y * cell_len;

			os::rect_t rc;
			rc.base = p1;
			rc.size = os::point_t(cell_len, cell_len);


			int16_t index = (int16_t)(_do_data[x][y] * coord_t(6));
			index = m::max(m::min(index, 6), 0);
			auto col = fire_col[index];
			//static_cast<os::color_t>(((int16_t)(_do_data[x][y] * coord_t(0xFF)))) << 16;
			os::surface::fill_rect(surface, rc, col);
		}

		FOR_EACH_CELL()
		{
			//velocity
			{
				os::point_t p1;
				p1.x = velocity_spacing + cell_dim + x * cell_len;
				p1.y = grid_spacing + cell_dim + y * cell_len;

				os::point_t p2 = p1;
				p2.x += static_cast<os::coord_t>(_do_vel_x[x][y] * coord_t(0.3) * coord_t(cell_dim));
				p2.y += static_cast<os::coord_t>(_do_vel_y[x][y] * coord_t(0.3) * coord_t(cell_dim));
					

				os::rect_t rc;
				rc.base = p2 - 1;
				rc.size = os::point_t(3, 3);

				//os::surface::draw_ellipse(surface, rc, 0xf000f0);
				//os::surface::draw_line(surface, p1, p2, 0xf00000);
			}

			//pressure
			{
				os::point_t p1;
				p1.x = pressure_spacing + x * cell_len;
				p1.y = grid_spacing + y * cell_len;

				os::rect_t rc;
				rc.base = p1;
				rc.size = os::point_t(cell_len, cell_len);

				os::surface::fill_rect(surface, rc, static_cast<os::color_t>(((int16_t)(_do_vort[x][y] * coord_t(10))) + 0x80) << 16);
			}

			//divergence
			{
				os::point_t p1;
				p1.x = divergence_spacing + x * cell_len;
				p1.y = grid_spacing + y * cell_len;

				os::rect_t rc;
				rc.base = p1;
				rc.size = os::point_t(cell_len, cell_len);

				os::surface::fill_rect(surface, rc, static_cast<os::color_t>(((int16_t)(_do_div[x][y] * coord_t(10))) + 0x80) << 16);
			}
		}

		os::rect_t rc;
		rc.size = os::point_t(cell_len * grid_size_x, cell_len * grid_size_y) + 2;

		rc.base = os::point_t(velocity_spacing, grid_spacing) -1;
		os::surface::draw_rect(surface, rc, 0x00f000);

		rc.base = os::point_t(pressure_spacing, grid_spacing) - 1;
		os::surface::draw_rect(surface, rc, 0x00f000);

		rc.base = os::point_t(divergence_spacing, grid_spacing) - 1;
		os::surface::draw_rect(surface, rc, 0x00f000);


		os::surface::draw_end(surface);
	}
}