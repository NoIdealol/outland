#pragma once
#include "symbol.h"

namespace outland { namespace parser
{
	enum sid_t : uint32_t
	{
		sid_type_void,
		sid_type_name,
		sid_type_ptr,
		sid_type_func,
		sid_type_array,
		sid_define_struct,
		sid_define_func,
		sid_define_var,
		sid_define_namespace,
		sid_func_scope,

		sid_operator_array,
		sid_operator_prefix,
		sid_operator_postfix,
		sid_operator_binary,
		sid_operator_access,
		sid_operator_subscript,
		sid_operator_call,

		sid_value_number,
		sid_value_null,
		sid_value_name,
		sid_value_lambda,
	};

	using vflags_t = uint32_t;
	enum vflag_t : vflags_t
	{
		vflag_rvalue	= 1 << 16,
		vflag_lvalue	= 1 << 17,
		vflag_mask		= c::max_uint32 - ((1 << 16) - 1),
	};

	
	struct syntax_t 
	{
		sid_t				_sid;	
	};

	struct type_t : syntax_t
	{
	};

	struct value_t : syntax_t
	{
		vflags_t			_vflag;
		type_t*				type;
	};

	struct operator_array_t : value_t
	{
		value_t*	rhs;
		value_t*	index;
	};

	struct operator_prefix_t : value_t
	{
		value_t*	rhs;
	};

	struct operator_postfix_t : value_t
	{
		value_t*	lhs;
	};

	struct operator_binary_t : value_t
	{
		value_t*	lhs;
		value_t*	rhs;
	};

	struct operator_access_t : value_t
	{
		value_t*	lhs;
		symbol_id	name;
	};

	struct operator_subscript_t : value_t
	{
		value_t*	lhs;
		value_t*	index;
	};

	struct operator_call_t : value_t
	{
		value_t*	lhs;
		value_t**	arg_data;
		size_t		arg_size;
	};

	using msize_t = uint64_t;
	union number_t
	{
		int8_t		sint8;
		int16_t		sint16;
		int32_t		sint32;
		int64_t		sint64;
		uint8_t		uint8;
		uint16_t	uint16;
		uint32_t	uint32;
		uint64_t	uint64;
		msize_t		msize;
		float32_t	flt32;
		float64_t	flt64;
		//fixed32_t	x32;
	};

	struct func_scope_t : syntax_t
	{
		syntax_t**	statement_data;
		size_t		statement_size;
	};
	
	struct value_number_t : value_t
	{
		number_t number;
	};

	struct value_null_t : value_t
	{

	};

	struct value_name_t : value_t
	{
		symbol_id			name;
	};

	struct value_lambda_t : value_t
	{
		symbol_id			name;
		symbol_id*			arg_data;
		size_t				arg_size;
		func_scope_t*		body;
	};

	struct type_void_t : type_t
	{
	};

	struct type_name_t : type_t
	{
		symbol_id			name;
	};

	struct type_ptr_t : type_t
	{
		type_t*				type;
	};

	struct func_arg_t// : syntax_t
	{
		uint32_t			name;
		type_t*				type;
	};

	struct type_func_t : type_t
	{
		bool				is_va_arg;
		type_t*				ret_type;
		func_arg_t*			arg_data;
		size_t				arg_size;
	};

	struct type_array_t : type_t
	{
		uint32_t			ar_size;
		type_t*				el_type;
	};

	struct define_struct_t : syntax_t
	{
		symbol_id			name;
		symbol_id*			member_data;
		size_t				member_size;
	};

	

	struct define_func_t : syntax_t
	{
		symbol_id			name;
		symbol_id*			arg_data;
		size_t				arg_size;
		func_scope_t*		body;
	};

	struct define_var_t : syntax_t
	{
		symbol_id			name;
		value_t*			init;
	};

	struct define_namespace_t : syntax_t
	{
		symbol_id	name;
		syntax_t**	statement_data;
		size_t		statement_size;
	};

} }
