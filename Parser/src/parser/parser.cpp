#include "pch.h"
#include "parser.h"
#include "math\integer.h"


namespace outland { namespace parser
{
	//////////////////////////////////////////////////
	// PARSER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	parser_t::parser_t(allocator_t* allocator, scratch_allocator_t* syntax_scratch)
		: _stack(nullptr)
		, _data(nullptr)
		, _top(nullptr)
		, _capacity(0) //init capacity so that realloc allocates something
		, _parse(nullptr)
		, _syntax(nullptr)
		, _allocator(allocator)
		, _scratch(syntax_scratch)
		, _symbol_table(nullptr)
		, _err_msg(nullptr)
		, _params(nullptr)
	{

	}

	//--------------------------------------------------
	error_t	parser_t::_realloc(size_t capacity)
	{
		size_t new_capacity = m::align(capacity, alignof(void*));
		void* new_stack = _allocator->alloc(new_capacity, alignof(void*), &new_capacity);
		if (!new_stack)
			return err_out_of_memory;
		new_capacity &= ~(alignof(void*)-1);

		//copy call
		call_t* new_top;
		{
			call_t* const bot = (call_t*)((byte_t*)_stack + _capacity);
			size_t const old_size = ((byte_t*)bot - (byte_t*)_top);
			new_top = (call_t*)((byte_t*)new_stack + new_capacity - old_size);
			u::mem_copy(new_top, _top, old_size);
		}
		//copy data
		void* new_data;
		{
			size_t const old_size = ((byte_t*)_data - (byte_t*)_stack);
			new_data = ((byte_t*)new_stack + old_size);
			u::mem_copy(new_stack, _stack, old_size);
		}

		_allocator->free(_stack, _capacity);

		_stack = new_stack;
		_data = new_data;
		_top = new_top;
		_capacity = new_capacity;

		return err_ok;
	}

	//--------------------------------------------------
	void*		parser_t::parent()
	{
		return ((call_t*)(((byte_t*)_top) + _top->size)) + 1;
	}

	//--------------------------------------------------
	error_t	parser_t::_push_top(size_t size, parse_t parse)
	{
		size += sizeof(call_t);

		if (size_t((byte_t*)_top - (byte_t*)_data) < size)
		{
			error_t err = _realloc(m::max(_capacity, size_t(64)) + (_capacity >> 1)); //+50%
			if (err)
				return err;
		}

		_top = (call_t*)(((byte_t*)_top) - size);
		_top->linear = ((byte_t*)_data - (byte_t*)_stack);
		_top->parse = parse;
		_top->size = size;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	parser_t::_push_arg(void const* arg, size_t size)
	{
		if (size_t((byte_t*)_top - (byte_t*)_data) < size)
		{
			error_t err = _realloc(m::max(_capacity, size_t(64)) + (_capacity >> 1)); //+50%
			if (err)
				return err;
		}

		u::mem_copy(_data, arg, size);
		_data = (byte_t*)_data + size;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	parser_t::_alloc_args(void*& args, size_t& size, size_t align)
	{
		memory_info_t info;
		info.size = ((byte_t*)_data - (byte_t*)_stack) - _top->linear;
		info.align = align;

		error_t err = u::mem_alloc(_scratch, args, info);
		if (err)
			return err;

		size = info.size;
		_data = ((byte_t*)_stack + _top->linear);

		u::mem_copy(args, _data, size);

		return err_ok;
	}

	//--------------------------------------------------
	void	parser_t::_pop_arg(void* arg, size_t size)
	{
		_data = (byte_t*)_data - size;
		u::mem_copy(arg, _data, size);
	}

	//--------------------------------------------------
	result_t parser_t::pop_top(syntax_t* object)
	{
		_syntax = object;
		_data = (syntax_t**)((byte_t*)_stack + _top->linear);
		auto ret = _top->parse;
		_top = (call_t*)(((byte_t*)_top) + _top->size);
		return (result_t)ret;
	}

	//--------------------------------------------------
	result_t parser_t::error(char8_t const* err)
	{
		_err_msg = err;
		return nullptr;
	}

	//--------------------------------------------------
	result_t parser_t::error_out_of_memory()
	{
		return error("out of memory");
	}

	//--------------------------------------------------
	error_t		parser_t::symbol_resolve(uint32_t name, symbol_id& symbol)
	{
		return _symbol_table->resolve(name, symbol);
	}

	//--------------------------------------------------
	error_t		parser_t::symbol_resolve(uint32_t name, symbol_id space, symbol_id& symbol)
	{
		return _symbol_table->resolve(name, space, symbol);
	}

	//--------------------------------------------------
	error_t		parser_t::symbol_define(uint32_t name, symbol_flag_t flag, symbol_id& symbol)
	{
		return _symbol_table->define(name, flag, symbol);
	}

	//--------------------------------------------------
	void		parser_t::symbol_push_scope(symbol_id symbol)
	{
		_symbol_table->push_scope(symbol);
	}

	//--------------------------------------------------
	void		parser_t::symbol_pop_scope()
	{
		_symbol_table->pop_scope();
	}

	//--------------------------------------------------
	syntax_t*	parser_t::symbol_metadata(symbol_id symbol)
	{
		return (syntax_t*)_symbol_table->metadata(symbol);
	}

	//--------------------------------------------------
	void		parser_t::symbol_metadata(symbol_id symbol, syntax_t* meta)
	{
		_symbol_table->metadata(symbol, meta);
	}

	//--------------------------------------------------
	uint32_t	parser_t::symbol_name(symbol_id symbol)
	{
		return _symbol_table->name(symbol);
	}

	//--------------------------------------------------
	error_t		tables_init(lexer::string_table_t* str_tbl, parser::symbol_table_t* sym_tbl)
	{
		error_t err;
		uint64_t str_hdl;

		struct entry_t
		{
			char8_t const*	data;
			size_t			size;
		};

#define STR_TBL_ENTRY(NAME) { #NAME, u::array_size(#NAME) - 1 },
		entry_t const keword_arr[] = { LANG_KEYWORD(STR_TBL_ENTRY) };
		entry_t const specord_arr[] = { LANG_SPECWORD(STR_TBL_ENTRY) };
		entry_t const typesymbol_arr[] = { LANG_TYPES(STR_TBL_ENTRY) };
#undef STR_TBL_ENTRY

		err = lexer::string_table::insert(str_tbl, str_hdl, "_INVALID", u::array_size("_INVALID") - 1);
		if (err)
			return err;

		if (keyword__INVALID != str_hdl)
			return err_invalid_parameter;

		for (auto& it : keword_arr)
		{
			err = lexer::string_table::insert(str_tbl, str_hdl, it.data, it.size);
			if (err)
				return err;
		}

		if (specword__INVALID != str_hdl)
		{
			O_ASSERT(false);
			return err_invalid_parameter;
		}

		for (auto& it : specord_arr)
		{
			err = lexer::string_table::insert(str_tbl, str_hdl, it.data, it.size);
			if (err)
				return err;
		}

		uint64_t numeric_name_arr[u::array_size(typesymbol_arr)];
		for (size_t i = 0; i < u::array_size(typesymbol_arr); ++i)
		{
			err = lexer::string_table::insert(str_tbl, 
				numeric_name_arr[i], 
				typesymbol_arr[i].data,
				typesymbol_arr[i].size);
			if (err)
				return err;
		}

		symbol_id symbol;
		for (size_t i = 0; i < u::array_size(typesymbol_arr); ++i)
		{
			err = sym_tbl->define((uint32_t)numeric_name_arr[i], symbol_flag_type_number, symbol);
			if (err)
				return err;
		}

		if ((typesymbol__INVALID + u::array_size(typesymbol_arr)) != symbol)
		{
			O_ASSERT(false);
			return err_invalid_parameter;
		}
		
		return err_ok;
	}

	char8_t const* parser_t::parse_tokens(symbol_table_t*	symbol_table, parse_t p, param_t const* token_array, size_t token_size)
	{
		_symbol_table = symbol_table;
		param_t const* const token_end = token_array + token_size;

		_params = token_array;
		auto parse = p;// _parse;

		while (parse && token_end != _params)
		{
			parse = (parse_t)parse(_top + 1, this, _params, _syntax);
		}
		_parse = parse;
		if (parse)
			return nullptr;
		return _err_msg;
	}

} }