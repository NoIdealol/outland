#include "pch.h"
#include "symbol.h"


namespace outland { namespace parser
{

	//--------------------------------------------------	
	symbol_table_t::symbol_table_t(allocator_t* allocator)
		: _map(allocator, decltype(_map)::fnc_hash_t().bind<&symbol_hash>(), decltype(_map)::fnc_cmp_t().bind<&symbol_cmp>())
		, _table(allocator)
		, _ctx(0)
	{

	}

	//--------------------------------------------------
	error_t	symbol_table_t::resolve(uint32_t name, symbol_id& symbol)
	{
		auto it = _ctx;

		while (true)
		{
			key_t key = make_key(it, name);

			if (!_map.find(key, symbol))
				return err_ok;

			if (0 == it)
				return err_not_found;

			it = _table[it & symbol_mask_id].space;
		}
	}

	//--------------------------------------------------
	error_t	symbol_table_t::resolve(uint32_t name, symbol_id space, symbol_id& symbol)
	{
		key_t key = make_key(space, name);
		return _map.find(key, symbol);
	}

	//--------------------------------------------------
	error_t	symbol_table_t::define(uint32_t name, symbol_flag_t flag, symbol_id& symbol)
	{
		error_t err;

		symbol_id tmp_symbol = symbol_id(_table.size()) | flag;

		definition_t def;
		def.name = name;
		def.space = _ctx;
		def.meta = nullptr;

		err = _table.push_back(def);
		if (err)
			return err;

		if (name)
		{
			key_t key = make_key(_ctx, name);

			err = _map.insert(key, tmp_symbol);
			if (err)
			{
				_table.pop_back();
				return err;
			}
		}

		if ((symbol_range_type_beg <= flag && symbol_range_type_end >= flag) || symbol_flag_fnc == flag || symbol_flag_namespace == flag)
			_ctx = tmp_symbol;

		symbol = tmp_symbol;
		return err_ok;
	}

	//--------------------------------------------------
	void	symbol_table_t::push_scope(symbol_id symbol)
	{
		_ctx = symbol;
	}

	//--------------------------------------------------
	void	symbol_table_t::pop_scope()
	{
		O_ASSERT(_ctx);
		_ctx = _table[_ctx & symbol_mask_id].space;
	}

	//--------------------------------------------------
	void*	symbol_table_t::metadata(symbol_id symbol)
	{
		return _table[symbol & symbol_mask_id].meta;
	}

	//--------------------------------------------------
	void	symbol_table_t::metadata(symbol_id symbol, void* meta)
	{
		_table[symbol & symbol_mask_id].meta = meta;
	}

	//--------------------------------------------------
	uint32_t	symbol_table_t::name(symbol_id symbol)
	{
		return _table[symbol & symbol_mask_id].name;
	}

} }
