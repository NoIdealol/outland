#pragma once
#include "..\lexer\lexer.h"
#include "symbol.h"
#include "syntax.h"

#define LANG_TYPES(NAME) \
NAME(int8) \
NAME(int16) \
NAME(int32) \
NAME(int64) \
NAME(uint8) \
NAME(uint16) \
NAME(uint32) \
NAME(uint64) \
NAME(flt32) \
NAME(flt64) \
NAME(msize)
//NAME(fixed32) \

/*#define LANG_TYPES_OTHER(NAME) \
NAME(void)*/

#define LANG_KEYWORD(NAME) \
NAME(void) \
NAME(null) \
NAME(true) \
NAME(false) \
NAME(if) \
NAME(else) \
NAME(return) \
NAME(struct) \
NAME(namespace) \
NAME(cast) \
NAME(sizeof) \
NAME(alignof)

#define LANG_SPECWORD(NAME) \
NAME(i8) \
NAME(i16) \
NAME(i32) \
NAME(i64) \
NAME(u8) \
NAME(u16) \
NAME(u32) \
NAME(u64) \
NAME(f32) \
NAME(f64)

namespace outland { namespace parser
{
#define KEYWORD(NAME) keyword_##NAME,
	enum keyword_t : uint32_t
	{
		KEYWORD(_INVALID)
		LANG_KEYWORD(KEYWORD)
		KEYWORD(_END)
	};
#undef KEYWORD

#define SPECWORD(NAME) specword_##NAME,
	enum specword_t : uint32_t
	{
		specword__INVALID = keyword__END - 1,
		LANG_SPECWORD(SPECWORD)
		SPECWORD(_END)
	};
#undef SPECWORD

#define TYPE(NAME) typesymbol_##NAME,
	enum typesymbol_t : uint32_t
	{
		typesymbol__INVALID = symbol_flag_type_number - 1,
		LANG_TYPES(TYPE)
		//TYPE(_COUNT)
	};
#undef TYPE

	using param_t = lexer::token_t;
	class parser_t;
	using result_t = void(*)();
	using parse_t = result_t(*)(void*, parser_t*, param_t const*, syntax_t*);

	//////////////////////////////////////////////////
	// PARSER
	//////////////////////////////////////////////////

	class parser_t
	{
		struct call_t
		{
			parse_t		parse;
			size_t		linear;
			size_t		size;
		};

	private:

		void*					_stack;
		void*					_data;
		call_t*					_top;
		size_t					_capacity;
		parse_t					_parse;
		syntax_t*				_syntax;
		allocator_t*			_allocator;
		scratch_allocator_t*	_scratch;
		symbol_table_t*			_symbol_table;
		char8_t const*			_err_msg;
		param_t const*			_params;

	private:
		error_t	_realloc(size_t capacity);
		error_t	_push_top(size_t size, parse_t parse);
		error_t	_push_arg(void const* arg, size_t size);
		error_t	_alloc_args(void*& args, size_t& size, size_t align);
		void	_pop_arg(void* arg, size_t size);

	public:
		parser_t(allocator_t* allocator, scratch_allocator_t* syntax_scratch);

		void*		parent();
		template<typename next_t, result_t(*fnc)(next_t*, parser_t*, param_t const*, syntax_t*)>
		parse_t		bind_next();
		template<typename next_t, result_t(*fnc)(next_t*, parser_t*, param_t const*, syntax_t*)>
		result_t	accept(syntax_t* object = nullptr);
		template<typename type_t>
		error_t		push_top(type_t*& object, parse_t next);
		template<typename next_t, result_t(*fnc)(next_t*, parser_t*, param_t const*, syntax_t*), typename type_t>
		error_t		push_top(type_t*& object);		
		template<typename type_t>
		error_t		push_arg(type_t const& object);
		template<typename type_t>
		void		pop_arg(type_t& object);
		result_t	pop_top(syntax_t* object);

		template<typename type_t>
		error_t		alloc_syntax(type_t*& object);
		template<typename type_t>
		error_t		alloc_args(type_t*& object, size_t& size);

		result_t	error(char8_t const* err);
		result_t	error_out_of_memory();

		error_t		symbol_resolve(uint32_t name, symbol_id& symbol);
		error_t		symbol_resolve(uint32_t name, symbol_id space, symbol_id& symbol);
		error_t		symbol_define(uint32_t name, symbol_flag_t flag, symbol_id& symbol);
		void		symbol_push_scope(symbol_id symbol);
		void		symbol_pop_scope();
		syntax_t*	symbol_metadata(symbol_id symbol);
		void		symbol_metadata(symbol_id symbol, syntax_t* meta);
		uint32_t	symbol_name(symbol_id symbol);

		char8_t const* parse_tokens(symbol_table_t*	symbol_table, parse_t p, param_t const* token_array, size_t token_size);
		syntax_t*	get_syntax() { return _syntax; }
	};

	error_t		tables_init(lexer::string_table_t* str_tbl, parser::symbol_table_t* sym_tbl);

	struct output_t
	{
		symbol_table_t*			sym_tbl;
		lexer::string_table_t*	str_tbl;
		void(*print)(const char8_t*);
		size_t					level;
	};
	void print(output_t* output, syntax_t* syntax);

	//--------------------------------------------------
	template<typename next_t, result_t(*fnc)(next_t*, parser_t*, param_t const*, syntax_t*)>
	parse_t	parser_t::bind_next()
	{
		return &function<result_t(parser_t*, param_t const*, syntax_t*)>::object_call<next_t, fnc>;
	}

	//--------------------------------------------------
	template<typename next_t, result_t(*fnc)(next_t*, parser_t*, param_t const*, syntax_t*)>
	result_t parser_t::accept(syntax_t* object)
	{
		++_params;
		_syntax = object;
		auto* ret = bind_next<next_t, fnc>();
		return (result_t)ret;
	}

	//--------------------------------------------------
	template<typename type_t>
	error_t parser_t::push_top(type_t*& object, parse_t next)
	{
		static_assert(alignof(type_t) == alignof(void*), "bad type alignment");
		error_t err = _push_top(sizeof(type_t), next);
		object = (type_t*)(_top + 1);
		return err;
	}

	//--------------------------------------------------
	template<typename next_t, result_t(*fnc)(next_t*, parser_t*, param_t const*, syntax_t*), typename type_t>
	error_t parser_t::push_top(type_t*& object)
	{
		return push_top(object, bind_next<next_t, fnc>());
	}

	//--------------------------------------------------
	template<typename type_t>
	error_t	parser_t::push_arg(type_t const& object)
	{
		return _push_arg(&object, sizeof(type_t));
	}

	//--------------------------------------------------
	template<typename type_t>
	void	parser_t::pop_arg(type_t& object)
	{
		_pop_arg(&object, sizeof(type_t));
	}

	//--------------------------------------------------
	template<typename type_t>
	error_t	parser_t::alloc_syntax(type_t*& object)
	{
		return u::mem_alloc(_scratch, object);
	}

	//--------------------------------------------------
	template<typename type_t>
	error_t	parser_t::alloc_args(type_t*& object, size_t& size)
	{
		void* args;
		error_t err = _alloc_args(args, size, alignof(type_t));
		if (err)
			return err;

		O_ASSERT(0 == (size % sizeof(type_t)));
		size = size / sizeof(type_t);
		object = (type_t*)args;

		return err_ok;
	}

	

} }