#include "pch.h"
#include "rule.h"

//#define TODO(reason) static_assert(false, reason)
#define TODO(reason)

namespace outland { namespace parser
{

	//--------------------------------------------------
	static bool expect_no_ws_nl(param_t const* param)
	{
		return 0 == ((lexer::flag_newline | lexer::flag_whitespace) & param->flags);
	}
	//--------------------------------------------------
	static bool expect_specword(param_t const* param, specword_t specword)
	{
		return lexer::token_identifier == param->type && specword == param->data;
	}

	//--------------------------------------------------
	static bool expect_keyword(param_t const* param, keyword_t keyword)
	{
		return lexer::token_identifier == param->type && keyword == param->data;
	}

	//--------------------------------------------------
	static bool expect_symbol(param_t const* param, char8_t symbol)
	{
		return lexer::token_symbol == param->type && symbol == param->data;
	}

	//--------------------------------------------------
	static bool expect_symbol(param_t const* param)
	{
		return lexer::token_symbol == param->type;
	}

	//--------------------------------------------------
	static bool expect_identifier(param_t const* param)
	{
		return lexer::token_identifier == param->type && specword__END <= param->data;
	}

	//--------------------------------------------------
	static bool expect_number(param_t const* param)
	{
		return lexer::token_number == param->type;
	}

	//--------------------------------------------------
	static bool expect_symbol_flag(symbol_id symbol, symbol_flag_t flag)
	{
		return (symbol_mask_flags & symbol) == flag;
	}

	//--------------------------------------------------
	/*static bool expect_type_name(parser_t* parser, param_t const* param, symbol_id& type_name)
	{
		return expect_identifier(param) && 
			err_ok == parser->symbol_resolve(param->data, type_name) && 
			symbol_range_type_beg <= (symbol_mask_flags & type_name) && 
			symbol_range_type_end >= (symbol_mask_flags & type_name);
	}*/

	//--------------------------------------------------
	/*static bool expect_type_name(type_t* type, symbol_id& symbol)
	{
		auto* type_name = (type_name_t*)type;
		if ((sid_type_name == type->_sid) && (symbol_mask_type & type_name->name))
		{
			symbol = type_name->name;
			return true;
		}
		return false;
	}*/

	//--------------------------------------------------
	static bool expect_type_void(type_t* type)
	{
		return sid_type_void == type->_sid;
	}

	//--------------------------------------------------
	static bool expect_type_func(type_t* type)
	{
		return sid_type_func == type->_sid;
	}

	//--------------------------------------------------
	static bool expect_type_name_struct(type_t* type, symbol_id& symbol)
	{
		auto* type_name = (type_name_t*)type;
		if ((sid_type_name == type->_sid) && expect_symbol_flag(type_name->name, symbol_flag_type_struct))
		{
			symbol = type_name->name;
			return true;
		}
		return false;
	}

	//--------------------------------------------------
	static bool expect_type_number(type_t* type, symbol_id& symbol)
	{
		auto* type_name = (type_name_t*)type;

		if((sid_type_name == type->_sid) && expect_symbol_flag(type_name->name, symbol_flag_type_number))
		{
			symbol = type_name->name;
			return true;
		}
		return false;
	}

	//--------------------------------------------------
	static bool expect_type_array(type_t* type, type_t*& element)
	{
		auto* type_array = (type_array_t*)type;
		if (sid_type_array == type->_sid)
		{
			element = type_array->el_type;
			return true;
		}
		return false;
	}

	//--------------------------------------------------
	static bool expect_type_ptr(type_t* type, type_t*& ptr_to)
	{
		auto* type_ptr = (type_ptr_t*)type;
		if (sid_type_ptr == type->_sid)
		{
			ptr_to = type_ptr->type;
			return true;
		}
		return false;
	}

	//--------------------------------------------------
	static error_t compare_type(parser_t* parser, type_t* t1, type_t* t2)
	{
		size_t type_size = 1;

	_loop:

		if (t1->_sid != t2->_sid)
			return err_bad_data;

		switch (t1->_sid)
		{
		case sid_type_void:
		{
			break;
		}
		case sid_type_name:
		{
			type_name_t* tn1 = (type_name_t*)t1;
			type_name_t* tn2 = (type_name_t*)t2;

			if (tn1->name != tn2->name)
				return err_bad_data;

			break;
		}
		case sid_type_ptr:
		{
			type_ptr_t* tp1 = (type_ptr_t*)t1;
			type_ptr_t* tp2 = (type_ptr_t*)t2;

			t1 = tp1->type;
			t2 = tp2->type;

			goto _loop;
		}
		case sid_type_func:
		{
			type_func_t* tf1 = (type_func_t*)t1;
			type_func_t* tf2 = (type_func_t*)t2;

			t1 = tf1->ret_type;
			t2 = tf2->ret_type;

			if (tf1->arg_size != tf2->arg_size)
				return err_bad_data;

			if (tf1->is_va_arg != tf2->is_va_arg)
				return err_bad_data;

			error_t err;
			for (size_t i = 0; i < tf1->arg_size; ++i)
			{
				err = parser->push_arg(tf1->arg_data[i].type);
				if (err)
					return err;

				err = parser->push_arg(tf2->arg_data[i].type);
				if (err)
					return err;
			}
			type_size += tf1->arg_size;

			goto _loop;
		}
		case sid_type_array:
		{
			type_array_t* ta1 = (type_array_t*)t1;
			type_array_t* ta2 = (type_array_t*)t2;

			t1 = ta1->el_type;
			t2 = ta2->el_type;

			if (ta1->ar_size != ta2->ar_size)
				return err_bad_data;

			goto _loop;
		}
		default:
			O_ASSERT(false);
			return err_not_implemented;
		}

		if (--type_size)
		{
			parser->pop_arg(t2);
			parser->pop_arg(t1);
			goto _loop;
		}

		return err_ok;
	}

	//--------------------------------------------------
	static error_t expect_type_implicit(parser_t* parser, type_t* dst, value_t* src)
	{
		//allow adding qualifiers
		//allow ptr to struct to ptr to first member
		//allow null to ptr
		TODO("other conversions");

		//implicit array conversion
		if (sid_type_array == dst->_sid && sid_type_array == src->type->_sid)
		{
			auto* a_dst = (type_array_t*)dst;
			auto* a_src = (type_array_t*)src->type;
			if (0 == a_dst->ar_size && 0 != a_src->ar_size)
			{
				if (0 == (vflag_lvalue & src->_vflag))
					return err_bad_data; //taking pointer from rvalue
				return compare_type(parser, a_dst->el_type, a_src->el_type);
			}
		}

		//implicit pointer conversion
		if (sid_type_ptr == dst->_sid && sid_type_array == src->type->_sid)
		{
			auto* p_dst = (type_ptr_t*)dst;
			auto* a_src = (type_array_t*)src->type;
			
			if (0 == (vflag_lvalue & src->_vflag))
				return err_bad_data; //taking pointer from rvalue
			return compare_type(parser, p_dst->type, a_src->el_type);	
		}

		//allow exact match
		return compare_type(parser, dst, src->type);
		//static_assert(false, "todo");
	}

	//////////////////////////////////////////////////
	// PARSE IDENTIFIER
	//////////////////////////////////////////////////

	struct parse_identifier_t
	{
		alignas(alignof(uintptr_t))
		symbol_id symbol;
	};

	namespace parse_identifier
	{
		using self_t = parse_identifier_t;

		static result_t	resolve_or_end(self_t* self, parser_t* parser, param_t const* param, syntax_t* result);
		static result_t	parse_identifier(self_t* self, parser_t* parser, param_t const* param);

		//--------------------------------------------------
		static result_t	alloc_value_name(self_t* self, parser_t* parser, vflags_t vflags)
		{
			value_name_t* value_name;
			if (parser->alloc_syntax(value_name))
				return parser->error_out_of_memory();

			value_name->_sid = sid_value_name;
			value_name->type = (type_t*)parser->symbol_metadata(self->symbol);
			value_name->_vflag = vflags;
			value_name->name = self->symbol;

			return parser->pop_top(value_name);
		}

		//--------------------------------------------------
		static result_t	alloc_type_name(self_t* self, parser_t* parser)
		{
			type_name_t* type_name;
			if (parser->alloc_syntax(type_name))
				return parser->error_out_of_memory();

			type_name->_sid = sid_type_name;
			type_name->name = self->symbol;

			return parser->pop_top(type_name);
		}

		//--------------------------------------------------
		static result_t	identifier(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_identifier(param))
				return parser->error("expected 'identifier'");

			if (parser->symbol_resolve(param->data, self->symbol, self->symbol))
				return parser->error("undefined 'identifier'");

			return parser->accept<self_t, &resolve_or_end>();	
		}

		//--------------------------------------------------
		static result_t	scope_end(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!(expect_symbol(param, ':') && expect_no_ws_nl(param)))
				return parser->error("expected '::'");

			return parser->accept<self_t, &identifier>();
		}

		//--------------------------------------------------
		static result_t	scope(self_t* self, parser_t* parser, param_t const* param)
		{
			if (!expect_symbol(param, ':'))
				return parser->error("'namespace' not allowed");

			return parser->accept<self_t, &scope_end>();
		}

		//--------------------------------------------------
		static result_t	resolve_or_end(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			switch (symbol_mask_flags & self->symbol)
			{
			case symbol_flag_var_local:
			case symbol_flag_var_global:
			case symbol_flag_var_member:
				return alloc_value_name(self, parser, vflag_lvalue);
			case symbol_flag_fnc:
				return alloc_value_name(self, parser, vflag_rvalue);
			case symbol_flag_namespace:
				return scope(self, parser, param);
			case symbol_flag_type_number:
			case symbol_flag_type_struct:
				return alloc_type_name(self, parser);
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}

		//--------------------------------------------------
		static result_t	parse_identifier(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_identifier(param));

			if (parser->symbol_resolve(param->data, self->symbol))
				return parser->error("undefined 'identifier'");
		
			return parser->accept<self_t, &resolve_or_end>();
		}
	}

	//////////////////////////////////////////////////
	// PARSE TYPE
	//////////////////////////////////////////////////

	struct parse_type_t
	{
		type_t*		type;
		uint32_t	arr_size;
	};

	namespace parse_type
	{
		using self_t = parse_type_t;

		static result_t modifier(self_t* self, parser_t* parser, param_t const* param, syntax_t* result);
		static result_t	parse_type(self_t* self, parser_t* parser, param_t const* param);
		static result_t	parse_type_type_name(self_t* self, parser_t* parser, param_t const* param, type_name_t* type_name);

		//--------------------------------------------------
		static bool		expect_type(param_t const* param)
		{
			return expect_keyword(param, keyword_void);
		}

		struct parse_fnc_t
		{
			uintptr_t		is_va_arg;
		};

		namespace parse_fnc
		{
			using self_t = parse_fnc_t;

			static result_t arg_type(self_t* self, parser_t* parser, param_t const* param);
			static result_t arg_identifier(self_t* self, parser_t* parser, param_t const* param);
		
			//--------------------------------------------------
			static result_t alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
			{
				type_func_t* type_func;
				if (parser->alloc_syntax(type_func))
					return parser->error_out_of_memory();

				type_func->_sid = sid_type_func;
				type_func->is_va_arg = self->is_va_arg;
				type_func->ret_type = nullptr;
				if (parser->alloc_args(type_func->arg_data, type_func->arg_size))
					return parser->error_out_of_memory();

				return parser->pop_top(type_func);
			}

			//--------------------------------------------------
			static result_t end(self_t*, parser_t* parser, param_t const* param, syntax_t* result)
			{
				if (!expect_symbol(param, ')'))
					return parser->error("expected ')'");

				return parser->accept<self_t, &alloc>();
			}

			//--------------------------------------------------
			static result_t va(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
			{
				if (!expect_symbol(param, '.') || !expect_no_ws_nl(param))
					return parser->error("expected '..'");

				self->is_va_arg = true;

				return parser->accept<self_t, &end>();
			}

			//--------------------------------------------------
			static result_t arg_or_va(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
			{
				if (expect_type(param))
					return arg_type(self, parser, param);
				if (expect_identifier(param))
					return arg_identifier(self, parser, param);
			
				if (expect_symbol(param, '.'))
					return parser->accept<self_t, &va>();

				return parser->error("expected '..' or 'arg'");
			}

			//--------------------------------------------------
			static result_t next_or_end(self_t*, parser_t* parser, param_t const* param, syntax_t* result)
			{
				if (expect_symbol(param, ')'))
					return parser->accept<self_t, &alloc>();

				if (expect_symbol(param, ','))
					return parser->accept<self_t, &arg_or_va>();

				return parser->error("expected ',' or ')'");
			}

			//--------------------------------------------------
			static result_t arg_name(self_t*, parser_t* parser, param_t const* param, syntax_t* result)
			{
				func_arg_t func_arg;
				/*if (parser->alloc_syntax(func_arg))
				return parser->error_out_of_memory();*/

				//func_arg._sid = sid_func_arg;
				func_arg.type = (type_t*)result;

				if (expect_type_void(func_arg.type))
					return parser->error("invalid storage type 'void'");

				if (expect_identifier(param))
					func_arg.name = param->data;
				else if (expect_symbol(param, ',') || expect_symbol(param, ')'))
					func_arg.name = 0;
				else
					return parser->error("expected 'identifier' or ',' or ')'");

				if (parser->push_arg(func_arg))
					return parser->error_out_of_memory();

				if (func_arg.name)
					return parser->accept<self_t, &next_or_end>();
				else
					return next_or_end(/*self*/nullptr, parser, param, nullptr); //invalid self
			}


			//--------------------------------------------------
			static result_t arg_type(self_t* self, parser_t* parser, param_t const* param)
			{
				parse_type_t* type;
				if (parser->push_top<self_t, &arg_name>(type))
					return parser->error_out_of_memory();

				return parse_type(type, parser, param);
			}

			//--------------------------------------------------
			static result_t arg_type_name(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
			{
				switch (result->_sid)
				{
				case sid_type_name:
					break;
				case sid_value_name:
					return parser->error("expected 'type'");
				default:
					O_ASSERT(false);
				}
				
				parse_type_t* type;
				if (parser->push_top<self_t, &arg_name>(type))
					return parser->error_out_of_memory();

				return parse_type_type_name(type, parser, param, (type_name_t*)result);
			}

			//--------------------------------------------------
			static result_t arg_identifier(self_t* self, parser_t* parser, param_t const* param)
			{
				parse_identifier_t* identifier;
				if (parser->push_top<self_t, &arg_type_name>(identifier))
					return parser->error_out_of_memory();

				return parse_identifier::parse_identifier(identifier, parser, param);
			}

			//--------------------------------------------------
			static result_t arg_first(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
			{
				if (expect_symbol(param, ')'))
					return parser->accept<self_t, &alloc>();

				if (expect_type(param))
					return arg_type(self, parser, param);
				if (expect_identifier(param))
					return arg_identifier(self, parser, param);

				return parser->error("expected ')' or 'arg'");
			}

			//--------------------------------------------------
			static result_t parse_fnc(self_t* self, parser_t* parser, param_t const* param)
			{
				O_ASSERT(expect_symbol(param, '(')); //return parser->error("expected '('");

				self->is_va_arg = false;

				return parser->accept<self_t, &arg_first>();
			}
		}

		//--------------------------------------------------
		static result_t fnc_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* type_func = (type_func_t*)result;
			type_func->ret_type = self->type;

			self->type = type_func;
			return modifier(self, parser, param, nullptr);
		}

		//--------------------------------------------------
		static result_t fnc(self_t* self, parser_t* parser, param_t const* param)
		{
			parse_fnc_t* parse_fnc;
			if (parser->push_top<self_t, &fnc_alloc>(parse_fnc))
				return parser->error_out_of_memory();

			return parse_fnc::parse_fnc(parse_fnc, parser, param);
		}

		//--------------------------------------------------
		static result_t ptr(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_symbol(param, '#'));

			type_ptr_t* type_ptr;
			if (parser->alloc_syntax(type_ptr))
				return parser->error_out_of_memory();

			type_ptr->_sid = sid_type_ptr;
			type_ptr->type = self->type;
			self->type = type_ptr;

			return parser->accept<self_t, &modifier>();
		}

		//--------------------------------------------------
		static result_t arr_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t*)
		{
			if (!expect_symbol(param, ']'))
				return parser->error("expected ']'");

			type_array_t* type_array;
			if (parser->alloc_syntax(type_array))
				return parser->error_out_of_memory();

			type_array->_sid = sid_type_array;
			type_array->el_type = self->type;
			type_array->ar_size = self->arr_size;

			self->type = type_array;

			return parser->accept<self_t, &modifier>();
		}

		//--------------------------------------------------
		static result_t arr_size_or_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (expect_symbol(param, ']'))
			{
				self->arr_size = 0;
				return arr_alloc(self, parser, param, nullptr);
			}

			if (expect_number(param))
			{
				if (c::max_uint32 < param->data)
					return parser->error("static array too long");

				if (0 == param->data)
					return parser->error("zero sized array not allowed");

				self->arr_size = (uint32_t)param->data;
				return parser->accept<self_t, &arr_alloc>();
			}

			return parser->error("expected '[' or 'number'");
		}

		//--------------------------------------------------
		static result_t arr(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_symbol(param, '[')); //return parser->error("expected '['");

			return parser->accept<self_t, &arr_size_or_alloc>();
		}

		//--------------------------------------------------
		static result_t modifier(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (expect_symbol(param, '#'))
				return ptr(self, parser, param);
			if (expect_symbol(param, '('))
				return fnc(self, parser, param);
			if (expect_symbol(param, '['))
				return arr(self, parser, param);

			if (sid_type_void == self->type->_sid)
				return parser->error("storage type cannot be 'void'");

			return parser->pop_top(self->type);
		}

		//--------------------------------------------------
		static result_t parse_type(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_keyword(param, keyword_void));
		
			type_void_t* type_void;
			if (parser->alloc_syntax(type_void))
				return parser->error_out_of_memory();

			type_void->_sid = sid_type_void;

			self->type = type_void;
			return parser->accept<self_t, &modifier>();
		}

		//--------------------------------------------------
		static result_t	parse_type_type_name(self_t* self, parser_t* parser, param_t const* param, type_name_t* type_name)
		{
			self->type = type_name;
			return modifier(self, parser, param, nullptr);
		}

	}

	//////////////////////////////////////////////////
	// PARSE FUNC STATEMENT
	//////////////////////////////////////////////////

	namespace parse_func_statement
	{
		static bool		expect_func_statement(param_t const* param);
		static result_t parse_func_statement(parser_t* parser, param_t const* param, parse_t next);
	}

	//////////////////////////////////////////////////
	// PARSE FUNC SCOPE
	//////////////////////////////////////////////////

	struct parse_func_scope_t
	{
		uintptr_t dummy;
	};

	namespace parse_func_scope
	{
		using self_t = parse_func_scope_t;

		static result_t on_statement(self_t*, parser_t* parser, param_t const* param, syntax_t* result);

		//--------------------------------------------------
		static result_t alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			parser->symbol_pop_scope();

			func_scope_t* func_scope;
			if (parser->alloc_syntax(func_scope))
				return parser->error_out_of_memory();

			func_scope->_sid = sid_func_scope;
			if (parser->alloc_args(func_scope->statement_data, func_scope->statement_size))
				return parser->error_out_of_memory();

			return parser->pop_top(func_scope);
		}

		//--------------------------------------------------
		static result_t statement(self_t*, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (expect_symbol(param, '}'))
				return parser->accept<self_t, &alloc>();

			if (parse_func_statement::expect_func_statement(param))
				return parse_func_statement::parse_func_statement(parser, param, parser->bind_next<self_t, &on_statement>());

			return parser->error("expected 'statement' or '}'");
		}

		//--------------------------------------------------
		static result_t on_statement(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (parser->push_arg(result))
				return parser->error_out_of_memory();

			return statement(self, parser, param, result);
		}

		//--------------------------------------------------
		static bool expect_func_scope(param_t const* param)
		{
			return expect_symbol(param, '{');
		}

		//--------------------------------------------------
		static result_t parse_func_scope(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_func_scope(param));

			symbol_id symbol;
			if (parser->symbol_define(0, symbol_flag_namespace, symbol))
				return parser->error_out_of_memory();

			return parser->accept<self_t, &statement>();
		}

		//--------------------------------------------------
		static result_t parse_func_body(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_func_scope(param));

			return parser->accept<self_t, &statement>();
		}
	}

	//////////////////////////////////////////////////
	// PARSE FUNC
	//////////////////////////////////////////////////

	struct parse_func_t
	{
		alignas(alignof(uintptr_t))
		symbol_id		symbol;
	};

	namespace parse_func
	{
		using self_t = parse_func_t;

		//--------------------------------------------------
		static result_t func_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			O_ASSERT(sid_func_scope == result->_sid);

			if (parser->symbol_name(self->symbol))
			{
				define_func_t* define_func;
				if (parser->alloc_syntax(define_func))
					return parser->error_out_of_memory();

				define_func->_sid = sid_define_func;
				define_func->name = self->symbol;
				if (parser->alloc_args(define_func->arg_data, define_func->arg_size))
					return parser->error_out_of_memory();

				define_func->body = (func_scope_t*)result;

				return parser->pop_top(define_func);
			}
			else
			{
				value_lambda_t* value_lambda;
				if (parser->alloc_syntax(value_lambda))
					return parser->error_out_of_memory();

				value_lambda->_sid = sid_value_lambda;
				value_lambda->type = (type_t*)parser->symbol_metadata(self->symbol);
				value_lambda->_vflag = vflag_rvalue;
				value_lambda->name = self->symbol;
				if (parser->alloc_args(value_lambda->arg_data, value_lambda->arg_size))
					return parser->error_out_of_memory();

				value_lambda->body = (func_scope_t*)result;

				return parser->pop_top(value_lambda);
			}
		}

		//--------------------------------------------------
		static bool expect_func(param_t const* param)
		{
			return parse_func_scope::expect_func_scope(param);
		}

		//--------------------------------------------------
		static result_t parse_func(self_t* self, parser_t* parser, param_t const* param, uint32_t name, type_func_t* type)
		{
			if (name)
			{
				switch (parser->symbol_define(name, symbol_flag_fnc, self->symbol))
				{
				case err_ok:
					break;
				case err_already_exists:
					return parser->error("symbol redefinition");
				default:
					return parser->error_out_of_memory();
				}			
			}
			else
			{
				switch (parser->symbol_define(0, symbol_flag_fnc, self->symbol))
				{
				case err_ok:
					break;
				default:
					return parser->error_out_of_memory();
				}
			}
			//note: scope poping in body
			parser->symbol_metadata(self->symbol, type);

			for (size_t i = 0; i < type->arg_size; ++i)
			{		
				symbol_id symbol;
				switch (parser->symbol_define(type->arg_data[i].name, symbol_flag_var_local, symbol))
				{
				case err_ok:
					break;
				case err_already_exists:
					return parser->error("symbol redefinition");
				default:
					return parser->error_out_of_memory();
				}

				parser->symbol_metadata(symbol, type->arg_data[i].type);

				if (parser->push_arg(symbol))
					return parser->error_out_of_memory();			
			}

			parse_func_scope_t* parse_func_scope;
			if (parser->push_top<self_t, &func_alloc>(parse_func_scope))
				return parser->error_out_of_memory();

			return parse_func_scope::parse_func_body(parse_func_scope, parser, param);
		}
	}

	//////////////////////////////////////////////////
	// PARSE VALUE
	//////////////////////////////////////////////////
	
	struct parse_value_t
	{
		union
		{
			number_t	number;
			uint64_t	data;
		};
		symbol_id	name;
		uint16_t	codepoint;
		uint64_t	frac;
	};

	namespace parse_value
	{
		using self_t = parse_value_t;

		static result_t parse_value(self_t* self, parser_t* parser, param_t const* param, syntax_t*);

		//--------------------------------------------------
		static result_t number_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t*)
		{
			struct number_helper_t : value_number_t
			{
				type_name_t	_type_name;
			};

			number_helper_t* number_helper;
			if (parser->alloc_syntax(number_helper))
				return parser->error_out_of_memory();

			number_helper->_sid = sid_value_number;
			number_helper->type = &number_helper->_type_name;
			number_helper->_vflag = vflag_rvalue;
			number_helper->_type_name._sid = sid_type_name;
			number_helper->_type_name.name = self->name;
			number_helper->number = self->number;

			return parser->pop_top(number_helper);
		}

		//--------------------------------------------------
		static result_t number_integer(self_t* self, parser_t* parser, param_t const* param, syntax_t* syntax)
		{
			if (expect_specword(param, specword_i8) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_int8;
				self->number.sint8 = (int8_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_i16) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_int16;
				self->number.sint16 = (int16_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_i32) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_int32;
				self->number.sint32 = (int32_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_i64) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_int64;
				self->number.sint64 = (int64_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_u8) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_uint8;
				self->number.uint8 = (uint8_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_u16) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_uint16;
				self->number.uint16 = (uint16_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_u32) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_uint32;
				self->number.uint32 = (uint32_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_u64) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_uint64;
				self->number.uint64 = (uint64_t)self->data;
				return parser->accept<self_t, &number_alloc>();
			}

			self->name = typesymbol_msize;
			self->number.msize = (msize_t)self->data;
			return number_alloc(self, parser, param, syntax);
		}

		//--------------------------------------------------
		static result_t number_floating(self_t* self, parser_t* parser, param_t const* param, syntax_t* syntax)
		{
			float64_t frac = (float64_t)self->frac;
			uint16_t frac_size = param->codepoint - self->codepoint;
			switch (frac_size)
			{
			case 0: frac *= 1e-0; break;
			case 1: frac *= 1e-1; break;
			case 2: frac *= 1e-2; break;
			case 3: frac *= 1e-3; break;
			case 4: frac *= 1e-4; break;
			case 5: frac *= 1e-5; break;
			case 6: frac *= 1e-6; break;
			case 7: frac *= 1e-7; break;
			case 8: frac *= 1e-8; break;
			case 9: frac *= 1e-9; break;
			case 10: frac *= 1e-10; break;
			case 11: frac *= 1e-11; break;
			case 12: frac *= 1e-12; break;
			case 13: frac *= 1e-13; break;
			case 14: frac *= 1e-14; break;
			case 15: frac *= 1e-15; break;
			case 16: frac *= 1e-16; break;
			case 17: frac *= 1e-17; break;
			case 18: frac *= 1e-18; break;
			default:
				return parser->error("frac part too long");
			}

			if (expect_specword(param, specword_f32) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_flt32;
				self->number.flt32 = (float32_t)self->data + (float32_t)frac;
				return parser->accept<self_t, &number_alloc>();
			}

			if (expect_specword(param, specword_f64) && expect_no_ws_nl(param))
			{
				self->name = typesymbol_flt64;
				self->number.flt64 = (float64_t)self->data + (float64_t)frac;
				return parser->accept<self_t, &number_alloc>();
			}

			return parser->error("expected 'flt specifier'");
		}
	
		//--------------------------------------------------
		static result_t number_frac(self_t* self, parser_t* parser, param_t const* param, syntax_t* syntax)
		{
			if ((expect_specword(param, specword_f32) || expect_specword(param, specword_f64)) && expect_no_ws_nl(param))
			{
				self->frac = 0;
				self->codepoint = param->codepoint;
				return number_floating(self, parser, param, syntax);
			}

			if (expect_number(param) && expect_no_ws_nl(param))
			{
				if (lexer::flag_number_dec != (lexer::flag_number_MASK & param->flags))
					return parser->error("expected 'frac part' in decimal");

				self->frac = param->data;
				self->codepoint = param->codepoint;
				return parser->accept<self_t, &number_floating>();
			}

			return parser->error("expected 'frac part' or 'flt specifier'");
		}

		//--------------------------------------------------
		static result_t number(self_t* self, parser_t* parser, param_t const* param, syntax_t* syntax)
		{
			if (expect_symbol(param, '.') && expect_no_ws_nl(param))
			{
				return parser->accept<self_t, &number_frac>();	
			}

			if ((expect_specword(param, specword_f32) || expect_specword(param, specword_f64)) && expect_no_ws_nl(param))
			{
				self->frac = 0;
				self->codepoint = param->codepoint;
				return number_floating(self, parser, param, syntax);
			}
			else
			{
				return number_integer(self, parser, param, syntax);
			}
		}

		//--------------------------------------------------
		static result_t null_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t*)
		{
			struct null_helper_t : value_null_t
			{
				type_ptr_t	_type_ptr;
				type_void_t	_type_void;
			};

			null_helper_t* null_helper;
			if (parser->alloc_syntax(null_helper))
				return parser->error_out_of_memory();

			null_helper->_sid = sid_value_null;
			null_helper->type = &null_helper->_type_ptr;
			null_helper->_vflag = vflag_rvalue;

			null_helper->_type_ptr._sid = sid_type_ptr;
			null_helper->_type_ptr.type = &null_helper->_type_void;

			null_helper->_type_void._sid = sid_type_void;

			return parser->pop_top(null_helper);
		}

		//--------------------------------------------------
		static result_t lambda_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			O_ASSERT(sid_value_lambda == result->_sid);
			return parser->pop_top(result);
		}

		//--------------------------------------------------
		static bool expect_value_lambda(param_t const* param, type_t* type)
		{
			return parse_func::expect_func(param) && expect_type_func(type);
		}

		//--------------------------------------------------
		static result_t parse_value_lambda(self_t* self, parser_t* parser, param_t const* param, type_t* type)
		{
			O_ASSERT(expect_value_lambda(param, type));

			parse_func_t* parse_func;
			if (parser->push_top<self_t, &lambda_alloc>(parse_func))
				return parser->error_out_of_memory();

			return parse_func::parse_func(parse_func, parser, param, 0, (type_func_t*)type);
		}

		//--------------------------------------------------
		static bool expect_value_struct(param_t const* param, type_t* type)
		{
			symbol_id symbol;
			return expect_symbol(param, '{') && expect_type_name_struct(type, symbol);
		}

		//--------------------------------------------------
		static result_t parse_value_struct(self_t* self, parser_t* parser, param_t const* param, type_t* type)
		{
			O_ASSERT(expect_value_struct(param, type));
			return parser->error("'value_struct' not implemented");//todo
		}

		//--------------------------------------------------
		static result_t lambda_or_struct(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* type = (type_t*)result;
			if (expect_value_lambda(param, type))
			{
				return parse_value_lambda(self, parser, param, type);
			}

			if (expect_value_struct(param, type))
			{
				return parse_value_struct(self, parser, param, type);
			}

			return parser->error("'type' not allowed");
		}

		//--------------------------------------------------
		static result_t type_or_value(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			switch (result->_sid)
			{
			case sid_type_name:
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &lambda_or_struct>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type_type_name(parse_type, parser, param, (type_name_t*)result);
			}
			case sid_value_name:
			{
				return parser->pop_top(result);
			}
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}

		//--------------------------------------------------
		static bool expect_value(param_t const* param)
		{
			return expect_number(param) || expect_keyword(param, keyword_null);
		}

		//--------------------------------------------------
		static result_t parse_value(self_t* self, parser_t* parser, param_t const* param, syntax_t*)
		{
			if (expect_number(param))
			{
				self->data = param->data;
				if (lexer::flag_number_dec == (lexer::flag_number_MASK & param->flags))
					return parser->accept<self_t, &number>();
				else
					return parser->accept<self_t, &number_integer>();		
			}

			if (parse_type::expect_type(param))
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &lambda_or_struct>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type(parse_type, parser, param);
			}

			if (expect_identifier(param))
			{
				parse_identifier_t* parse_identifier;
				if (parser->push_top<self_t, &type_or_value>(parse_identifier))
					return parser->error_out_of_memory();

				return parse_identifier::parse_identifier(parse_identifier, parser, param);
			}

			if (expect_keyword(param, keyword_null))
			{
				return parser->accept<self_t, &null_alloc>();
			}

			return parser->error("expected 'value'");
		}

		
	}

	//////////////////////////////////////////////////
	// PARSE EXPRESSION
	//////////////////////////////////////////////////

	struct parse_expression_t
	{
		uint32_t			precedence;
		uint16_t			op;
		uint16_t			op2;
		value_t*			lhs;
	};

	namespace parse_expression
	{
		using self_t = parse_expression_t;

		enum : uint32_t
		{
			prec_bin_plus_minus,
			prec_bin_mul_div_mod,
			prec_prefix,
			prec_postfix,
		};

		enum : uint8_t //keep this order!!!
		{
			assoc_right_to_left,
			assoc_left_to_right,	
		};

		static result_t end_or_binary_or_postfix(self_t* self, parser_t* parser, param_t const* param, syntax_t* result);
		static result_t subexpression(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence);
		static result_t parse_expression(self_t* self, parser_t* parser, param_t const* param, syntax_t*);
		
		//--------------------------------------------------
		static result_t postfix_lnum_alloc(self_t* self, parser_t* parser, param_t const* param)
		{
			auto* lhs = self->lhs;
			symbol_id number;
			if (!expect_type_number(lhs->type, number))
				return parser->error("expected 'number'");

			if (0 == (vflag_lvalue & lhs->_vflag))
				return parser->error("expected 'l-value'");

			operator_postfix_t* operator_postfix;
			if (parser->alloc_syntax(operator_postfix))
				return parser->error_out_of_memory();

			operator_postfix->_sid = sid_operator_postfix;
			operator_postfix->_vflag = vflag_rvalue | self->op;
			operator_postfix->type = lhs->type;
			operator_postfix->lhs = lhs;

			return end_or_binary_or_postfix(self, parser, param, operator_postfix);
		}

		//--------------------------------------------------
		static result_t binary_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* lhs = self->lhs;
			symbol_id lhs_number;
			if (!expect_type_number(lhs->type, lhs_number))
				return parser->error("expected 'number'");

			auto* rhs = (value_t*)result;
			symbol_id rhs_number;
			if (!expect_type_number(rhs->type, rhs_number))
				return parser->error("expected 'number'");

			if (lhs_number != rhs_number)
				return parser->error("operand 'type' mismatch");

			operator_binary_t* operator_binary;
			if (parser->alloc_syntax(operator_binary))
				return parser->error_out_of_memory();

			operator_binary->_sid = sid_operator_binary;
			operator_binary->_vflag = vflag_rvalue | self->op;
			operator_binary->type = lhs->type;
			operator_binary->lhs = lhs;
			operator_binary->rhs = rhs;

			return end_or_binary_or_postfix(self, parser, param, operator_binary);
		}

		//--------------------------------------------------
		static result_t access_alloc(self_t* self, parser_t* parser, param_t const* param)
		{
			if (!expect_identifier(param))
				return parser->error("expected 'member'");

			auto* lhs = self->lhs;

			symbol_id type_name;
			if (!expect_type_name_struct(lhs->type, type_name))
				return parser->error("cannot access a non-struct value");

			symbol_id member_name;
			if (parser->symbol_resolve(param->data, type_name, member_name) || !expect_symbol_flag(member_name, symbol_flag_var_member))
				return parser->error("expected 'member'");

			operator_access_t* operator_access;
			if (parser->alloc_syntax(operator_access))
				return parser->error_out_of_memory();

			operator_access->_sid = sid_operator_access;
			operator_access->_vflag = (vflag_lvalue | vflag_rvalue) & lhs->_vflag; //preserve l/r-value
			operator_access->type = (type_t*)parser->symbol_metadata(member_name);
			operator_access->lhs = lhs;
			operator_access->name = member_name;

			return parser->accept<self_t, &end_or_binary_or_postfix>(operator_access);
		}

		//--------------------------------------------------
		static result_t subscript_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_symbol(param, ']'))
				return parser->error("expected ']'");

			auto* lhs = self->lhs;

			type_t* element_type;
			if (!(expect_type_array(lhs->type, element_type) || expect_type_ptr(lhs->type, element_type)))
				return parser->error("subscript allowed only on 'array' and 'ptr'");

			auto* index = (value_t*)result;
			symbol_id index_number;
			if (!expect_type_number(index->type, index_number))
				return parser->error("expected 'number'");

			if (typesymbol_msize != index_number)
				return parser->error("expected number type 'msize'");

			operator_subscript_t* operator_subscript;
			if (parser->alloc_syntax(operator_subscript))
				return parser->error_out_of_memory();

			operator_subscript->_sid = sid_operator_subscript;
			operator_subscript->_vflag = vflag_lvalue;
			operator_subscript->type = element_type;
			operator_subscript->lhs = lhs;
			operator_subscript->index = index;

			return parser->accept<self_t, &end_or_binary_or_postfix>(operator_subscript);
		}

		//--------------------------------------------------
		static result_t call_alloc(value_t* lhs, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_symbol(param, ')')); //return parser->error("expected ')'");

			value_t**	arg_data;
			size_t		arg_size;
			if (parser->alloc_args(arg_data, arg_size))
				return parser->error_out_of_memory();

			if (sid_type_func != lhs->type->_sid)
				return parser->error("expected 'function'");

			type_func_t const* type_func = (type_func_t*)lhs->type;

			if (type_func->arg_size > arg_size)
				return parser->error("too few args");

			if (!type_func->is_va_arg && type_func->arg_size < arg_size)
				return parser->error("too many args");

			for (size_t i = 0; i < type_func->arg_size; ++i)
			{
				switch (expect_type_implicit(parser, type_func->arg_data[i].type, arg_data[i]))
				{
				case err_ok:
					break;
				case err_out_of_memory:
					return parser->error_out_of_memory();
				case err_bad_data:
					return parser->error("'type' mismatch");
				default:
					return parser->error("internal error");
				}
			}

			operator_call_t* operator_call;
			if (parser->alloc_syntax(operator_call))
				return parser->error_out_of_memory();

			operator_call->_sid = sid_operator_call;
			operator_call->_vflag = vflag_rvalue;
			operator_call->lhs = lhs;
			operator_call->type = type_func->ret_type;
			operator_call->arg_data = arg_data;
			operator_call->arg_size = arg_size;
			
			return parser->accept<self_t, &end_or_binary_or_postfix>(operator_call);
		}

		//--------------------------------------------------
		static result_t call_arg_or_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* arg)
		{
			auto* lhs = self->lhs;
			if (parser->push_arg(arg))
				return parser->error_out_of_memory();

			if (expect_symbol(param, ')'))
				return call_alloc(lhs, parser, param);

			if (expect_symbol(param, ','))
			{
				self_t* next_arg;
				if (parser->push_top<self_t, &call_arg_or_alloc>(next_arg))
					return parser->error_out_of_memory();

				return parser->accept<self_t, &parse_expression>();
			}
			
			return parser->error("expected ',' or ')'");
		}

		//--------------------------------------------------
		static result_t deref_alloc(self_t* self, parser_t* parser, param_t const* param)
		{
			value_t* lhs = self->lhs;

			type_t* ptr_to;
			if (!expect_type_ptr(lhs->type, ptr_to))
				return parser->error("expected 'ptr'");

			operator_postfix_t* operator_postfix;
			if (parser->alloc_syntax(operator_postfix))
				return parser->error_out_of_memory();

			operator_postfix->_sid = sid_operator_postfix;
			operator_postfix->_vflag = vflag_lvalue | '#';
			operator_postfix->lhs = lhs;
			operator_postfix->type = ptr_to;

			return end_or_binary_or_postfix(self, parser, param, operator_postfix);
		}

		namespace helper
		{
			//--------------------------------------------------
			static result_t reject(self_t* self, parser_t* parser)
			{
				auto* parent = (self_t*)parser->parent();
				parent->op2 = self->op;
				return parser->pop_top(self->lhs);
			}

			//--------------------------------------------------
			static result_t reject_or_postfix_lnum(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence)
			{
				if (precedence < self->precedence)
					return reject(self, parser);
				
				return postfix_lnum_alloc(self, parser, param);
			}

			//--------------------------------------------------
			static result_t reject_or_binary_rhs(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence, uint8_t associativity)
			{
				if (precedence < self->precedence)
					return reject(self, parser);
				
				self_t* rhs;
				if (parser->push_top<self_t, &binary_alloc>(rhs))
					return parser->error_out_of_memory();

				return subexpression(rhs, parser, param, precedence + associativity);
			}

			//--------------------------------------------------
			static result_t reject_or_call(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence)
			{
				if (precedence < self->precedence)
					return reject(self, parser);

				if (expect_symbol(param, ')'))
					return call_alloc(self->lhs, parser, param);

				self_t* arg;
				if (parser->push_top<self_t, &call_arg_or_alloc>(arg))
					return parser->error_out_of_memory();

				return parse_expression(arg, parser, param, nullptr);
			}

			//--------------------------------------------------
			static result_t reject_or_subscript(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence)
			{
				if (precedence < self->precedence)
					return reject(self, parser);
		
				self_t* index;
				if (parser->push_top<self_t, &subscript_alloc>(index))
					return parser->error_out_of_memory();

				return parse_expression(index, parser, param, nullptr);
			}

			//--------------------------------------------------
			static result_t reject_or_access(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence)
			{
				if (precedence < self->precedence)
					return reject(self, parser);
				
				return access_alloc(self, parser, param);
			}

			//--------------------------------------------------
			static result_t reject_or_deref(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence)
			{
				if (precedence < self->precedence)
					return reject(self, parser);

				return deref_alloc(self, parser, param);
			}
		}

		//--------------------------------------------------
		static result_t reject_or_binary_or_postfix(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			self->op = self->op2;
			self->op2 = 0;

			switch (self->op)
			{
				//postfix, numeric
			case '++': return helper::reject_or_postfix_lnum(self, parser, param, prec_postfix);
			case '--': return helper::reject_or_postfix_lnum(self, parser, param, prec_postfix);
				//postfix special
			case '#': return helper::reject_or_deref(self, parser, param, prec_postfix);
			case '(': return helper::reject_or_call(self, parser, param, prec_postfix);
			case '[': return helper::reject_or_subscript(self, parser, param, prec_postfix);
			case '.': return helper::reject_or_access(self, parser, param, prec_postfix);

				//binary, numeric
			case '+': return helper::reject_or_binary_rhs(self, parser, param, prec_bin_plus_minus, assoc_left_to_right);
			case '-': return helper::reject_or_binary_rhs(self, parser, param, prec_bin_plus_minus, assoc_left_to_right);
			case '*': return helper::reject_or_binary_rhs(self, parser, param, prec_bin_mul_div_mod, assoc_left_to_right);
			case '/': return helper::reject_or_binary_rhs(self, parser, param, prec_bin_mul_div_mod, assoc_left_to_right);
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}

		namespace helper
		{
			//--------------------------------------------------
			static result_t binary_or_postfix_symbol(self_t* self, parser_t* parser, param_t const* param, char8_t symbol)
			{
				if (expect_symbol(param, symbol) && expect_no_ws_nl(param))
				{
					self->op2 = (self->op2 << 8) | symbol;
					return parser->accept<self_t, &reject_or_binary_or_postfix>();
				}

				return reject_or_binary_or_postfix(self, parser, param, nullptr);
			}
		}

		//--------------------------------------------------
		static result_t binary_or_postfix_symbol(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{		
			switch (self->op2)
			{
			case '+': return helper::binary_or_postfix_symbol(self, parser, param, '+');
			case '-': return helper::binary_or_postfix_symbol(self, parser, param, '-');
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}	

		//--------------------------------------------------
		static result_t end_or_binary_or_postfix(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			self->lhs = (value_t*)result;

			if (self->op2)
			{
				return reject_or_binary_or_postfix(self, parser, param, nullptr);
			}
	
			if (expect_symbol(param))
			{
				switch (param->data)
				{
				//multi symbol
				case '+':
				case '-':
					self->op2 = (uint16_t)param->data;
					return parser->accept<self_t, &binary_or_postfix_symbol>();
				//single symbol
				case '(':
				case '[':
				case '.':
				case '*':
				case '/':
				case '#':
					self->op2 = (uint16_t)param->data;
					return parser->accept<self_t, &reject_or_binary_or_postfix>();
				default:
					break;
				}
			}

			return parser->pop_top(result);
		}

		//--------------------------------------------------
		static result_t prefix_lnum_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* value = (value_t*)result;
			symbol_id number;
			if (!expect_type_number(value->type, number))
				return parser->error("expected 'number'");

			if(0 == (vflag_lvalue & value->_vflag))
				return parser->error("expected 'l-value'");

			operator_prefix_t* operator_prefix;
			if (parser->alloc_syntax(operator_prefix))
				return parser->error_out_of_memory();

			operator_prefix->_sid = sid_operator_prefix;
			operator_prefix->_vflag = vflag_rvalue | self->op;
			operator_prefix->rhs = value;
			operator_prefix->type = value->type;

			return end_or_binary_or_postfix(self, parser, param, operator_prefix);
		}

		//--------------------------------------------------
		static result_t prefix_num_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* value = (value_t*)result;
			symbol_id number;
			if (!expect_type_number(value->type, number))
				return parser->error("expected 'number'");

			operator_prefix_t* operator_prefix;
			if (parser->alloc_syntax(operator_prefix))
				return parser->error_out_of_memory();

			operator_prefix->_sid = sid_operator_prefix;
			operator_prefix->_vflag = vflag_rvalue | self->op;
			operator_prefix->rhs = value;
			operator_prefix->type = value->type;

			return end_or_binary_or_postfix(self, parser, param, operator_prefix);
		}

		//--------------------------------------------------
		static result_t prefix_addr_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* value = (value_t*)result;

			if (0 == (vflag_lvalue & value->_vflag))
				return parser->error("expected 'l-value'");

			struct addr_helper_t : operator_prefix_t
			{
				type_ptr_t		_type_ptr;
			};

			addr_helper_t* operator_prefix;
			if (parser->alloc_syntax(operator_prefix))
				return parser->error_out_of_memory();

			operator_prefix->_sid = sid_operator_prefix;
			operator_prefix->_vflag = vflag_rvalue | self->op;
			operator_prefix->rhs = value;
			operator_prefix->type = &operator_prefix->_type_ptr;

			operator_prefix->_type_ptr._sid = sid_type_ptr;
			operator_prefix->_type_ptr.type = value->type;

			return end_or_binary_or_postfix(self, parser, param, operator_prefix);
		}

		//--------------------------------------------------
		static result_t prefix_array_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* value = (value_t*)result;

			type_t* el_type;
			if (!(expect_type_ptr(value->type, el_type) || expect_type_array(value->type, el_type)))
				return parser->error("expected 'ptr' or 'array'");

			if (sid_type_array == value->type->_sid &&
				((type_array_t*)value->type)->ar_size &&
				0 == (vflag_lvalue & value->_vflag))
			{
				return parser->error("expected 'l-value'");
			}

			struct operator_array_helper_t : operator_array_t
			{
				type_array_t		_type_array;
			};

			operator_array_helper_t* operator_array;
			if (parser->alloc_syntax(operator_array))
				return parser->error_out_of_memory();

			operator_array->_sid = sid_operator_array;
			operator_array->_vflag = vflag_rvalue;
			operator_array->rhs = value;
			operator_array->index = self->lhs;
			operator_array->type = &operator_array->_type_array;

			operator_array->_type_array._sid = sid_type_array;
			operator_array->_type_array.el_type = el_type;
			operator_array->_type_array.ar_size = 0;

			return end_or_binary_or_postfix(self, parser, param, operator_array);
		}

		//--------------------------------------------------
		static result_t prefix_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			switch (self->op)
			{
			case '++':
			case '--':
				return prefix_lnum_alloc(self, parser, param, result);
			case '+':
			case '-':
			case '!':
			case '~':
				return prefix_num_alloc(self, parser, param, result);
			case '#':
				return prefix_addr_alloc(self, parser, param, result);
			case ']':
				return prefix_array_alloc(self, parser, param, result);
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}

		//--------------------------------------------------
		static result_t prefix_expr(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			self_t* rhs;
			if (parser->push_top<self_t, &prefix_alloc>(rhs))
				return parser->error_out_of_memory();

			return subexpression(rhs, parser, param, prec_prefix);
		}

		//--------------------------------------------------
		static result_t prefix_symbol(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (expect_symbol(param, (char8_t)self->op) && expect_no_ws_nl(param))
			{
				self->op = (self->op << 8) | self->op;
				return parser->accept<self_t, &prefix_expr>();
			}
			else
			{
				return prefix_expr(self, parser, param, result);
			}
		}

		//--------------------------------------------------
		static result_t prefix_array(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_symbol(param, ']'))
				return parser->error("expected ']'");

			auto* value = (value_t*)result;
			symbol_id index_number;
			if (!expect_type_number(value->type, index_number))
				return parser->error("expected 'number'");

			if (typesymbol_msize != index_number)
				return parser->error("expected number type 'msize'");
			
			self->lhs = value;

			return parser->accept<self_t, &prefix_expr>();
		}

		//--------------------------------------------------
		static result_t prefix_array_index(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			self_t* index;
			if (parser->push_top<self_t, &prefix_array>(index))
				return parser->error_out_of_memory();

			return parse_expression(index, parser, param, result);
		}

		//--------------------------------------------------
		static result_t prefix(self_t* self, parser_t* parser, param_t const* param)
		{
			if (!expect_symbol(param))
				return parser->error("expected 'prefix'");

			switch (param->data)
			{
			case '+':
			case '-':
				self->op = (uint16_t)param->data;
				return parser->accept<self_t, &prefix_symbol>();
			case '!':
			case '~':
			case '#':
				self->op = (uint16_t)param->data;		
				return parser->accept<self_t, &prefix_expr>();
			case '[':
				self->op = (uint16_t)']';
				return parser->accept<self_t, &prefix_array_index>();
			default:
				return parser->error("unexpected 'symbol'");
			}
		}

		//--------------------------------------------------
		static result_t value(self_t* self, parser_t* parser, param_t const* param)
		{		
			parse_value_t* parse_value;
			if (parser->push_top<self_t, &end_or_binary_or_postfix>(parse_value))
				return parser->error_out_of_memory();

			return parse_value::parse_value(parse_value, parser, param, nullptr);
		}

		//--------------------------------------------------
		static result_t subexpression(self_t* self, parser_t* parser, param_t const* param, uint32_t precedence)
		{
			self->precedence = precedence;
			self->op = 0;
			self->op2 = 0;
		
			if (expect_symbol(param))
			{
				return prefix(self, parser, param);
			}
			else
			{
				return value(self, parser, param);
			}
		}

		//--------------------------------------------------
		static bool expect_expression(param_t const* param)
		{
			return
				parse_value::expect_value(param) ||
				expect_symbol(param, '+') ||
				expect_symbol(param, '-') ||
				expect_symbol(param, '!') ||
				expect_symbol(param, '~') ||
				expect_symbol(param, '#') ||
				expect_symbol(param, '[');
		}

		//--------------------------------------------------
		static result_t parse_expression(self_t* self, parser_t* parser, param_t const* param, syntax_t*)
		{
			return subexpression(self, parser, param, 0);
		}

		//--------------------------------------------------
		static result_t parse_expression_value(self_t* self, parser_t* parser, param_t const* param, value_t* value)
		{
			self->precedence = 0;
			self->op = 0;
			self->op2 = 0;

			return end_or_binary_or_postfix(self, parser, param, value);
		}
		
	}

	//////////////////////////////////////////////////
	// PARSE STRUCT
	//////////////////////////////////////////////////

	struct parse_struct_t
	{
		alignas(alignof(uintptr_t))
		symbol_id	name;
	};

	namespace parse_struct
	{
		using self_t = parse_struct_t;

		static result_t member_or_brace_r(self_t* self, parser_t* parser, param_t const* param, syntax_t* result);

		//--------------------------------------------------
		static result_t alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			define_struct_t* define_struct;
			if (parser->alloc_syntax(define_struct))
				return parser->error_out_of_memory();

			define_struct->_sid = sid_define_struct;
			define_struct->name = self->name;
			if (parser->alloc_args(define_struct->member_data, define_struct->member_size))
				return parser->error_out_of_memory();

			parser->symbol_metadata(define_struct->name, define_struct);

			return parser->pop_top(define_struct);
		}

		//--------------------------------------------------
		static result_t member_execute(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_symbol(param, ';'))
				return parser->error("expected ';'");

			return parser->accept<self_t, &member_or_brace_r>();
		}

		//--------------------------------------------------
		static result_t member_name(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			auto* member_type = (type_t*)result;
			symbol_id member_name;

			if (expect_type_void(member_type))
				return parser->error("invalid storage type 'void'");

			if (!expect_identifier(param))
				return parser->error("expected 'identifier'");

			switch (parser->symbol_define(param->data, symbol_flag_var_member, member_name))
			{
			case err_ok:
				break;
			case err_already_exists:
				return parser->error("symbol redefinition");
			default:
				return parser->error_out_of_memory();
			}
			parser->symbol_metadata(member_name, member_type);

			if (parser->push_arg(member_name))
				return parser->error_out_of_memory();

			return parser->accept<self_t, &member_execute>();
		}

		//--------------------------------------------------
		static result_t type_or_value(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			switch (result->_sid)
			{
			case sid_type_name:
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &member_name>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type_type_name(parse_type, parser, param, (type_name_t*)result);
			}
			case sid_value_name:
			{
				return parser->error("'value' not allowed");
			}
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}

		//--------------------------------------------------
		static result_t member_or_brace_r(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (expect_symbol(param, '}'))
			{
				parser->symbol_pop_scope();
				return parser->accept<self_t, &alloc>();
			}

			if (parse_type::expect_type(param))
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &member_name>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type(parse_type, parser, param);
			}

			if (expect_identifier(param))
			{
				parse_identifier_t* parse_identifier;
				if (parser->push_top<self_t, &type_or_value>(parse_identifier))
					return parser->error_out_of_memory();

				return parse_identifier::parse_identifier(parse_identifier, parser, param);
			}
			
			return parser->error("expected '}' or 'member'");
		}

		//--------------------------------------------------
		static result_t brace_l(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_symbol(param, '{'))
				return parser->error("expected '{'");

			return parser->accept<self_t, &member_or_brace_r>();
		}

		//--------------------------------------------------
		static result_t struct_name(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_identifier(param))
				return parser->error("expected 'identifier'");

			switch (parser->symbol_define(param->data, symbol_flag_type_struct, self->name))
			{
			case err_ok:
				break;
			case err_already_exists:
				return parser->error("symbol redefinition");
			default:
				return parser->error_out_of_memory();
			}

			return parser->accept<self_t, &brace_l>();
		}

		//--------------------------------------------------
		static bool expect_struct(param_t const* param)
		{
			return expect_keyword(param, keyword_struct);
		}

		//--------------------------------------------------
		static result_t parse_struct(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_keyword(param, keyword_struct)); //return parser->error("expected 'struct'");

			return parser->accept<self_t, &struct_name>();
		}
		
	}

	

	//////////////////////////////////////////////////
	// PARSE VAR OR FUNC
	//////////////////////////////////////////////////

	struct parse_var_or_func_t
	{
		type_t*		type;
		uint32_t	name;
		bool		is_global;
	};

	namespace parse_var_or_func
	{
		using self_t = parse_var_or_func_t;

		//--------------------------------------------------
		static result_t func_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			O_ASSERT(sid_define_func == result->_sid);
			return parser->pop_top(result);
		}

		//--------------------------------------------------
		static result_t func_body_or_forward(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			O_ASSERT(sid_type_func == result->_sid);
			auto* type_func = (type_func_t*)result;
			type_func->ret_type = self->type;

			if (parse_func::expect_func(param))
			{
				parse_func_t* parse_func;
				if (parser->push_top<self_t, &func_alloc>(parse_func))
					return parser->error_out_of_memory();

				return parse_func::parse_func(parse_func, parser, param, self->name, type_func);
			}

			if (expect_symbol(param, ';'))
			{
				return parser->error("func forward not implemented");
			}

			return parser->error("expected '{' or ';' while parsing function");
		}

		//--------------------------------------------------
		static result_t var_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			define_var_t* define_var;
			if (parser->alloc_syntax(define_var))
				return parser->error_out_of_memory();

			define_var->_sid = sid_define_var;
			define_var->init = (value_t*)result;

			switch (parser->symbol_define(self->name, self->is_global ? symbol_flag_var_global : symbol_flag_var_local, define_var->name))
			{
			case err_ok:
				break;
			case err_already_exists:
				return parser->error("symbol redefinition");
			default:
				return parser->error_out_of_memory();
			}

			parser->symbol_metadata(define_var->name, self->type);

			if (define_var->init)
			{
				switch (expect_type_implicit(parser, self->type, define_var->init))
				{
				case err_ok:
					break;
				case err_out_of_memory:
					return parser->error_out_of_memory();
				case err_bad_data:
					return parser->error("'type' mismatch");
				default:
					O_ASSERT(false);
				}
			}

			return parser->pop_top(define_var);
		}

		//--------------------------------------------------
		static result_t var_execute(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_symbol(param, ';'))
				return parser->error("expected ';'");

			return parser->accept<self_t, &var_alloc>(result);
		}

		//--------------------------------------------------
		static result_t var_init(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			parse_expression_t* parse_expression;
			if (parser->push_top<self_t, &var_execute>(parse_expression))
				return parser->error_out_of_memory();

			return parse_expression::parse_expression(parse_expression, parser, param, nullptr);
		}

		//--------------------------------------------------
		static result_t parse_var_or_func(self_t* self, parser_t* parser, param_t const* param, uint32_t name, type_t* type, bool is_global)
		{
			self->name = name;
			self->type = type;
			self->is_global = is_global;

			if (expect_symbol(param, ';')) //default init var
			{
				if (expect_type_void(self->type))
					return parser->error("invalid storage type 'void'");

				return parser->accept<self_t, &var_alloc>(nullptr);
			}

			if (expect_symbol(param, '=')) //value init var
			{
				if (expect_type_void(self->type))
					return parser->error("invalid storage type 'void'");

				return parser->accept<self_t, &var_init>();
			}

			if (expect_symbol(param, '(')) //function
			{
				parse_type::parse_fnc_t* parse_fnc;
				if (parser->push_top<self_t, &func_body_or_forward>(parse_fnc))
					return parser->error_out_of_memory();

				return parse_type::parse_fnc::parse_fnc(parse_fnc, parser, param);
			}

			if (expect_type_void(self->type))
				return parser->error("expected '(' while parsing function");
			else
				return parser->error("expected ';' or '=' or '(' while parsing variable or function");
		}
	}

	//////////////////////////////////////////////////
	// PARSE FUNC VALUE
	//////////////////////////////////////////////////

	using parse_func_value_t = parse_var_or_func_t;
	
	namespace parse_func_value
	{
		using self_t = parse_func_value_t;

		static result_t parse_func_value(self_t* self, parser_t* parser, param_t const* param);

		//--------------------------------------------------
		static result_t var_or_func(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			//hand-over
			return parse_var_or_func::parse_var_or_func(self, parser, param, self->name, self->type, false);
		}

		//--------------------------------------------------
		static result_t expr_alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			return parser->pop_top(result);
		}

		//--------------------------------------------------
		static result_t expr_execute(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_symbol(param, ';'))
				return parser->error("expected ';'");
				
			return parser->accept<self_t, &expr_alloc>(result);
		}

		//--------------------------------------------------
		static result_t expr_value(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			parse_expression_t* parse_expression;
			if (parser->push_top<self_t, &expr_execute>(parse_expression))
				return parser->error_out_of_memory();

			return parse_expression::parse_expression_value(parse_expression, parser, param, (value_t*)result);
		}

		//--------------------------------------------------
		static result_t name_or_temp(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			self->type = (type_t*)result;

			if (expect_identifier(param))
			{
				self->name = param->data;
				return parser->accept<self_t, &var_or_func>();
			}

			if (parse_value::expect_value_lambda(param, self->type))
			{
				parse_value_t* parse_value;
				if (parser->push_top<self_t, &expr_value>(parse_value))
					return parser->error_out_of_memory();

				return parse_value::parse_value_lambda(parse_value, parser, param, self->type);
			}

			if (parse_value::expect_value_struct(param, self->type))
			{
				parse_value_t* parse_value;
				if (parser->push_top<self_t, &expr_value>(parse_value))
					return parser->error_out_of_memory();

				return parse_value::parse_value_struct(parse_value, parser, param, self->type);
			}
			
			return parser->error("expected 'identifier'"); //this is a correct err msg
		}

		//--------------------------------------------------
		static result_t type_or_value(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			switch (result->_sid)
			{
			case sid_type_name:
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &name_or_temp>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type_type_name(parse_type, parser, param, (type_name_t*)result);
			}
			case sid_value_name:
			{
				return expr_value(self, parser, param, result);
			}
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}

		//--------------------------------------------------
		static result_t parse_func_value(self_t* self, parser_t* parser, param_t const* param)
		{
			if (expect_identifier(param))
			{
				parse_identifier_t* parse_identifier;
				if (parser->push_top<self_t, &type_or_value>(parse_identifier))
					return parser->error_out_of_memory();

				return parse_identifier::parse_identifier(parse_identifier, parser, param);
			}

			if (parse_type::expect_type(param))
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &name_or_temp>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type(parse_type, parser, param);
			}

			if (parse_expression::expect_expression(param))
			{
				parse_expression_t* parse_expression;
				if (parser->push_top<self_t, &expr_execute>(parse_expression))
					return parser->error_out_of_memory();

				return parse_expression::parse_expression(parse_expression, parser, param, nullptr);
			}

			O_ASSERT(false);
			return parser->error("internal");
		}

		//--------------------------------------------------
		static bool expect_func_value(param_t const* param)
		{
			return
				expect_identifier(param) ||
				parse_type::expect_type(param) ||
				parse_expression::expect_expression(param);
		}
	}

	

	//////////////////////////////////////////////////
	// PARSE FUNC STATEMENT
	//////////////////////////////////////////////////

	namespace parse_func_statement
	{
		//--------------------------------------------------
		static bool		expect_func_statement(param_t const* param)
		{
			return
				parse_struct::expect_struct(param) ||
				parse_func_scope::expect_func_scope(param) ||
				parse_func_value::expect_func_value(param);
		}

		//--------------------------------------------------
		static result_t parse_func_statement(parser_t* parser, param_t const* param, parse_t next)
		{
			if (parse_struct::expect_struct(param))
			{
				parse_struct_t* parse_struct;
				if (parser->push_top(parse_struct, next))
					return parser->error_out_of_memory();

				return parse_struct::parse_struct(parse_struct, parser, param);
			}

			if (parse_func_scope::expect_func_scope(param))
			{
				parse_func_scope_t* parse_func_scope;
				if (parser->push_top(parse_func_scope, next))
					return parser->error_out_of_memory();

				return parse_func_scope::parse_func_scope(parse_func_scope, parser, param);
			}

			if (parse_func_value::expect_func_value(param))
			{
				parse_func_value_t* parse_func_value;
				if (parser->push_top(parse_func_value, next))
					return parser->error_out_of_memory();

				return parse_func_value::parse_func_value(parse_func_value, parser, param);
			}

			O_ASSERT(false);
			return parser->error("internal");
		}
	}


	//////////////////////////////////////////////////
	// PARSE PROGRAM STATEMENT
	//////////////////////////////////////////////////

	namespace parse_program_statement
	{
		static bool		expect_program_statement(param_t const* param);
		static result_t parse_program_statement(parser_t* parser, param_t const* param, parse_t next);
	}

	//////////////////////////////////////////////////
	// PARSE PROGRAM VALUE
	//////////////////////////////////////////////////

	using parse_program_value_t = parse_var_or_func_t;
	
	namespace parse_program_value
	{
		using self_t = parse_program_value_t;

		//--------------------------------------------------
		static result_t value_var_or_func(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			TODO("check constexpr? if var, maybe in parse_var_or_func, it knows if it is global");
			return parse_var_or_func::parse_var_or_func(self, parser, param, self->name, self->type, true); //handover
		}

		//--------------------------------------------------
		static result_t value_name(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			self->type = (type_t*)result;

			if (expect_identifier(param))
			{
				self->name = param->data;
				return parser->accept<self_t, &value_var_or_func>();
			}

			if (parse_value::expect_value_lambda(param, self->type) || parse_value::expect_value_struct(param, self->type))
			{
				return parser->error("'expression' not allowed");
			}

			return parser->error("expected 'identifier'"); //this is a correct err msg
		}

		//--------------------------------------------------
		static result_t value_type(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			switch (result->_sid)
			{
			case sid_type_name:
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &value_name>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type_type_name(parse_type, parser, param, (type_name_t*)result);
			}
			case sid_value_name:
			{
				return parser->error("'expression' not allowed");
			}
			default:
				O_ASSERT(false);
				return parser->error("internal");
			}
		}
	
		//--------------------------------------------------
		static result_t parse_program_value(self_t* self, parser_t* parser, param_t const* param)
		{
			if (expect_identifier(param))
			{
				parse_identifier_t* parse_identifier;
				if (parser->push_top<self_t, &value_type>(parse_identifier))
					return parser->error_out_of_memory();

				return parse_identifier::parse_identifier(parse_identifier, parser, param);
			}

			if (parse_type::expect_type(param))
			{
				parse_type_t* parse_type;
				if (parser->push_top<self_t, &value_name>(parse_type))
					return parser->error_out_of_memory();

				return parse_type::parse_type(parse_type, parser, param);
			}

			if (parse_expression::expect_expression(param))
				return parser->error("'expression' not allowed");

			O_ASSERT(false);
			return parser->error("internal");
		}

		//--------------------------------------------------
		static bool expect_program_value(param_t const* param)
		{
			return
				expect_identifier(param) ||
				parse_type::expect_type(param) ||
				parse_expression::expect_expression(param);
		}
	}

	//////////////////////////////////////////////////
	// PARSE PROGRAM NAMESPACE
	//////////////////////////////////////////////////

	struct parse_program_namespace_t
	{
		alignas(alignof(uintptr_t))
		uint32_t	namespace_depth;
		symbol_id	name;
	};

	namespace parse_program_namespace
	{
		using self_t = parse_program_namespace_t;
	
		static result_t on_statement(self_t* self, parser_t* parser, param_t const* param, syntax_t* result);
		static result_t name(self_t* self, parser_t* parser, param_t const* param, syntax_t* result);

		//--------------------------------------------------
		static result_t alloc(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			while (self->namespace_depth)
			{
				--self->namespace_depth;
				parser->symbol_pop_scope();
			}

			define_namespace_t* define_namespace;
			if (parser->alloc_syntax(define_namespace))
				return parser->error_out_of_memory();

			define_namespace->_sid = sid_define_namespace;
			define_namespace->name = self->name;
			if (parser->alloc_args(define_namespace->statement_data, define_namespace->statement_size))
				return parser->error_out_of_memory();

			return parser->pop_top(define_namespace);
		}

		//--------------------------------------------------
		static result_t statement(self_t*, parser_t* parser, param_t const* param, syntax_t*)
		{
			if (expect_symbol(param, '}'))
				return parser->accept<self_t, &alloc>();

			if (parse_program_statement::expect_program_statement(param))
				return parse_program_statement::parse_program_statement(parser, param, parser->bind_next<self_t, &on_statement>());

			return parser->error("expected 'statement' or '}'");
		}

		//--------------------------------------------------
		static result_t on_statement(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (parser->push_arg(result))
				return parser->error_out_of_memory();
			
			return statement(self, parser, param, result);
		}

		//--------------------------------------------------
		static result_t namespace_scope(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!(expect_symbol(param, ':') && expect_no_ws_nl(param)))
				return parser->error("expected '::'");

			return parser->accept<self_t, &name>();
		}

		//--------------------------------------------------
		static result_t namespace_beg_or_scope(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (expect_symbol(param))
			{
				switch (param->data)
				{
				case '{':
					return parser->accept<self_t, &statement>(nullptr);
				case ':':
					return parser->accept<self_t, &namespace_scope>();
				default:
					break;
				}
			}
			return parser->error("expected '{' or '::'");
		}

		//--------------------------------------------------
		static result_t name(self_t* self, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (!expect_identifier(param))
				return parser->error("expected 'identifier'");

			switch (parser->symbol_define(param->data, symbol_flag_namespace, self->name))
			{
			case err_ok:
				break;
			case err_already_exists:
				parser->symbol_resolve(param->data, self->name);
				parser->symbol_push_scope(self->name);
				break;
			default:
				return parser->error_out_of_memory();
			}
			++self->namespace_depth;
			return parser->accept<self_t, &namespace_beg_or_scope>();
		}

		//--------------------------------------------------
		static result_t parse_program_namespace(self_t* self, parser_t* parser, param_t const* param)
		{
			O_ASSERT(expect_keyword(param, keyword_namespace));
			self->namespace_depth = 0;
			return parser->accept<self_t, &name>();
		}

		//--------------------------------------------------
		static bool expect_program_namespace(param_t const* param)
		{
			return expect_keyword(param, keyword_namespace);
		}

	}

	//////////////////////////////////////////////////
	// PARSE PROGRAM STATEMENT
	//////////////////////////////////////////////////

	namespace parse_program_statement
	{
		//--------------------------------------------------
		static bool		expect_program_statement(param_t const* param)
		{
			return
				parse_struct::expect_struct(param) ||
				parse_program_namespace::expect_program_namespace(param) ||
				parse_program_value::expect_program_value(param);
		}

		//--------------------------------------------------
		static result_t parse_program_statement(parser_t* parser, param_t const* param, parse_t next)
		{
			if (parse_struct::expect_struct(param))
			{
				parse_struct_t* parse_struct;
				if (parser->push_top(parse_struct, next))
					return parser->error_out_of_memory();

				return parse_struct::parse_struct(parse_struct, parser, param);
			}

			if (parse_program_namespace::expect_program_namespace(param))
			{
				parse_program_namespace_t* parse_program_namespace;
				if (parser->push_top(parse_program_namespace, next))
					return parser->error_out_of_memory();

				return parse_program_namespace::parse_program_namespace(parse_program_namespace, parser, param);
			}

			if (parse_program_value::expect_program_value(param))
			{
				parse_program_value_t* parse_program_value;
				if (parser->push_top(parse_program_value, next))
					return parser->error_out_of_memory();

				return parse_program_value::parse_program_value(parse_program_value, parser, param);
			}

			O_ASSERT(false);
			return parser->error("internal");
		}
	}

	//////////////////////////////////////////////////
	// PARSE PROGRAM
	//////////////////////////////////////////////////

	namespace parse_program
	{
		static result_t on_statement(void* self, parser_t* parser, param_t const* param, syntax_t* result);

		//--------------------------------------------------
		static result_t statement(parser_t* parser, param_t const* param)
		{
			if (lexer::token_end_of_file == param->type)
			{
				define_namespace_t* define_namespace;
				if (parser->alloc_syntax(define_namespace))
					return parser->error_out_of_memory();

				define_namespace->_sid = sid_define_namespace;
				define_namespace->name = 0;
				if (parser->alloc_args(define_namespace->statement_data, define_namespace->statement_size))
					return parser->error_out_of_memory();

				return parser->pop_top(define_namespace);
			}

			if (parse_program_statement::expect_program_statement(param))
				return parse_program_statement::parse_program_statement(parser, param, &on_statement);

			return parser->error("expected 'statement'");
		}

		//--------------------------------------------------
		static result_t on_statement(void*, parser_t* parser, param_t const* param, syntax_t* result)
		{
			if (parser->push_arg(result))
				return parser->error_out_of_memory();

			return statement(parser, param);
		}

		//--------------------------------------------------
		static result_t parse_program(void* self, parser_t* parser, param_t const* param, syntax_t*)
		{
			uintptr_t* dummy;
			if (parser->push_top(dummy, nullptr))
				return parser->error_out_of_memory();
			return statement(parser, param);
		}

	}

	syntax_t* do_test(lexer::string_table_t* string_table, parser::symbol_table_t* symbol_table, param_t const* token_array, size_t token_size, allocator_t* allocator, scratch_allocator_t* syntax_scratch)
	{

		parser::parser_t prs = { allocator, syntax_scratch };
		

		auto e = prs.parse_tokens(symbol_table, &parse_program::parse_program, token_array, token_size);
		O_ASSERT(!e);
		return prs.get_syntax();
		
	}

} }