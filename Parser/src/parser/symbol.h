#pragma once

namespace outland { namespace parser
{

	class symbol_table_t;
	using symbol_id = uint32_t;

	enum symbol_flag_t : symbol_id
	{
		symbol_flag_var_local = 1 << 28,
		symbol_flag_var_global = 2 << 28,
		symbol_flag_var_member = 3 << 28,
		symbol_flag_fnc = 4 << 28,
		symbol_flag_namespace = 5 << 28,
		symbol_flag_type_number = 6 << 28,
		symbol_flag_type_struct = 7 << 28,


		symbol_range_type_beg = symbol_flag_type_number,
		symbol_range_type_end = symbol_flag_type_struct,
		symbol_mask_id = (1 << 28) - 1,
		symbol_mask_flags = ~symbol_mask_id,
	};

	class symbol_table_t
	{
		using key_t = uint64_t;
		struct definition_t
		{
			symbol_id		space;
			uint32_t		name;
			void*			meta;
		};

	private:
		hash_map<symbol_id, key_t>		_map;
		array<definition_t>				_table;
		symbol_id						_ctx;

	private:
		static uint32_t symbol_hash(key_t key)						{ return static_cast<uint32_t>(key) + static_cast<uint32_t>(key >> 32); }
		static bool		symbol_cmp(key_t key1, key_t key2)			{ return key1 == key2; }
		static key_t	make_key(symbol_id space, uint32_t name)	{ return (key_t(space) << 32) | name; }

	public:
		symbol_table_t(allocator_t* allocator);

		error_t		resolve(uint32_t name, symbol_id& symbol);
		error_t		resolve(uint32_t name, symbol_id space, symbol_id& symbol);
		error_t		define(uint32_t name, symbol_flag_t flag, symbol_id& symbol);
		void		push_scope(symbol_id symbol);
		void		pop_scope();

		void*		metadata(symbol_id symbol);
		void		metadata(symbol_id symbol, void* meta);
		uint32_t	name(symbol_id symbol);
	};

} }
