#include "pch.h"
#include "syntax.h"
#include "math\integer.h"
#include "parser.h"


namespace outland { namespace parser
{
	namespace g
	{
		static char8_t const table_x[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		static char8_t const table_X[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		
		static uint64_t const table_d_cmp[] = 
		{ 
			9,
			99,
			999,
			9999,
			99999,
			999999,
			9999999,
			99999999,
			999999999,
			9999999999,
			99999999999,
			999999999999,
			9999999999999,
			99999999999999,
			999999999999999,
			9999999999999999,
			99999999999999999,
			999999999999999999,
			9999999999999999999,
		};

		static uint8_t const table_d_digits[] =
		{
			0, 0, 0, 0,
			1, 1, 1,
			2, 2, 2,
			3, 3, 3, 3,
			4, 4, 4,
			5, 5, 5,
			6, 6, 6, 6,
			7, 7, 7,
			8, 8, 8,
			9, 9, 9, 9,
			10, 10, 10,
			11, 11, 11,
			12, 12, 12, 12,
			13, 13, 13,
			14, 14, 14,
			15, 15, 15, 15,
			16, 16, 16,
			17, 17, 17,
			18, 18, 18, 18,
		};
	}

	size_t print_x(char8_t* buffer, uint64_t value)
	{
		size_t const index = m::bit_index(m::bit_top(value));
		size_t const char_size = (index >> 2) + 1;
		if (buffer)
		{
			buffer += char_size;
			do
			{
				--buffer;
				*buffer = g::table_x[value & 0xF];
				value >>= 4;
			} while (value);
		}
		return char_size;
	}

	size_t print_X(char8_t* buffer, uint64_t value)
	{
		size_t const index = m::bit_index(m::bit_top(value));
		size_t const char_size = (index >> 2) + 1;
		if (buffer)
		{
			buffer += char_size;
			do
			{
				--buffer;
				*buffer = g::table_X[value & 0xF];
				value >>= 4;
			} while (value);
		}
		return char_size;
	}

	size_t print_o(char8_t* buffer, uint64_t value)
	{
		size_t const index = m::bit_index(m::bit_top(value));
		size_t const char_size = ((index * 3) >> 3) + 1;
		if (buffer)
		{
			buffer += char_size;
			do
			{
				--buffer;
				*buffer = g::table_X[value & 0x7];
				value >>= 3;
			} while (value);
		}
		return char_size;
	}

	size_t print_b(char8_t* buffer, uint64_t value)
	{
		size_t const index = m::bit_index(m::bit_top(value));
		size_t const char_size = index + 1;
		if (buffer)
		{
			buffer += char_size;
			do
			{
				--buffer;
				*buffer = g::table_X[value & 0x1];
				value >>= 1;
			} while (value);
		}
		return char_size;
	}

	size_t print_d(char8_t* buffer, uint64_t value)
	{
		size_t const index = m::bit_index(m::bit_top(value));
		size_t const digits = g::table_d_digits[index];
		size_t const char_size = digits + 1 + size_t(g::table_d_cmp[digits] < value);

		if (buffer)
		{
			buffer += char_size;
			do
			{
				--buffer;
				*buffer = g::table_X[value % 10];
				value /= 10;
			} while (value);
		}
		return char_size;
	}

	size_t print_f(char8_t* buffer, float64_t value, uint8_t precision, char8_t sign)
	{
		size_t size = 0;
		if (0 > value)
		{
			++size;
			value = -value;
			if (buffer)
			{
				*buffer = '-';
				++buffer;
			}
		}
		else
		{
			if (' ' == sign || '+' == sign)
			{
				++size;
				if (buffer)
				{
					*buffer = sign;
					++buffer;
				}
			}
		}

		if (1e18 < value)
			return 0;

		uint64_t dec = uint64_t(value);
		size_t len = print_d(buffer, dec);
		if (buffer)
			buffer += len;
		size += len;
		if (precision)
		{			
			float64_t mul = float64_t(g::table_d_cmp[(precision & 0xF) - 1] + 1);
			uint64_t frac = uint64_t((value - float64_t(dec)) * mul + mul + 0.4);
			size += print_d(buffer, frac);
			if (buffer)
			{
				*buffer = '.';
			}
		}

		return size;
	}

	size_t print_s(char8_t* buffer, char8_t const* str, size_t size)
	{
		if (buffer)
			u::mem_copy(buffer, str, size);
		return size;
	}

	template<size_t size>
	size_t print_s(char8_t* buffer, char8_t const (&str)[size])
	{
		if (buffer)
			u::mem_copy(buffer, str, size - 1);
		return size;
	}

	union va_t
	{
		//int32_t			_i32;
		int64_t			_i64;
		//uint32_t		_u32;
		uint64_t		_u64;
		//float32_t		_f32;
		float64_t		_f64;
		void const*		_ptr;
		char8_t const*	_str;
	};

	using va_list_t = va_t const[];
	//typedef va_t const va_list_t [];

	va_t va(int32_t			va) { va_t r; r._i64 = va; return r; }
	va_t va(int64_t			va)	{ va_t r; r._i64 = va; return r; }
	va_t va(uint32_t		va)	{ va_t r; r._u64 = va; return r; }
	va_t va(uint64_t		va)	{ va_t r; r._u64 = va; return r; }
	//va_t va(float32_t		va)	{ va_t r; r._f32 = va; return r; }
	va_t va(float64_t		va)	{ va_t r; r._f64 = va; return r; }
	va_t va(void const*		va)	{ va_t r; r._ptr = va; return r; }
	va_t va(char8_t const*	va)	{ va_t r; r._str = va; return r; }

	size_t parse_d(char8_t const* str, uint64_t& out)
	{
		size_t size = 0;
		uint64_t val = 0;
		while (u8'0' <= *str && u8'9' >= *str)
		{
			val = val * 10 + (*str - u8'0');
			++size;
			++str;
		}
		out = val;
		return size;
	}

	size_t printf_u(char8_t fmt, char8_t* buffer, uint64_t value)
	{
		switch (fmt)
		{
		case 'd':
			return print_d(buffer, value);
		case 'b':
			return print_b(buffer, value);
		case 'o':
			return print_o(buffer, value);
		case 'x':
			return print_x(buffer, value);
			break;
		case 'X':
			return print_X(buffer, value);
		default:
			O_ASSERT(false);
			return 0;
		}
	}

	size_t printf(char8_t* buffer, char8_t const* fmt, va_list_t va_list)
	{
		size_t size = 0;
		size_t idx = 0;

		while (true)
		{
			{
				char8_t const* beg = fmt;
				while (*fmt && '%' != *fmt) //go to end or print
					++fmt;

				size_t len = fmt - beg;
				if (buffer) //print fmt
				{
					u::mem_copy(buffer, beg, len);
					buffer += len;
				}
				size += len;
			}

			if ('%' == *fmt)
			{
				++fmt;
				switch (*fmt)
				{
				case '%':
				{
					if (buffer)
					{
						*buffer = '%';
						++buffer;
					}
					++fmt;
					++size;
					break;
				}
				case 'b': case 'o': case 'd':case 'x':case 'X':
				{
					uint64_t val;
					if (0 > va_list[idx]._i64)
					{
						val = -va_list[idx]._i64;
						++size;
						if (buffer)
						{
							*buffer = '-';
							buffer += 1;
						}
					}
					else
					{
						val = +va_list[idx]._i64;
					}

					size_t len = printf_u(*fmt, buffer, val);
					++fmt;
					size += len;
					if (buffer)
						buffer += len;

					++idx;
					break;
				}
				case 'u':
				{
					++fmt;
					size_t len = printf_u(*fmt, buffer, va_list[idx]._u64);
					if (!len)
						return 0;
					++fmt;
					size += len;
					if (buffer)
						buffer += len;

					++idx;
					break;
				}
				case 'f':
				{
					size_t len = print_f(buffer, va_list[idx]._f64, 3, '-');
					if (!len)
						return 0;
					++fmt;
					size += len;
					if (buffer)
						buffer += len;

					++idx;
					break;
				}
				case 's':
				{
					size_t len = print_s(buffer, va_list[idx]._str, u::str_len(va_list[idx]._str));
					++fmt;
					size += len;
					if (buffer)
						buffer += len;
					++idx;
					break;
				}
				default:
					return 0;
				}
			}
			else
			{
				if (buffer) //print null
					*buffer = 0;
				return size;
			}
		}	
	}
	
	

	void print(output_t* output, char8_t const* fmt, va_list_t va_list = nullptr)
	{
		char8_t buffer[256];
		size_t out = printf(nullptr, fmt, va_list);
		if (u::array_size(buffer) > (out + output->level))
			printf(buffer + output->level * 2, fmt, va_list);

		for (size_t i = 0; i < output->level; ++i)
		{
			buffer[i * 2 + 0] = '|';
			buffer[i * 2 + 1] = '\t';
		}

		output->print(buffer);
	}

	void print_push(output_t* output)
	{
		++output->level;
	}

	void print_pop(output_t* output)
	{
		--output->level;
	}

	void print(output_t* output, syntax_t* syntax);


	char8_t const* get_string(output_t* output, uint32_t str_id)
	{
		char8_t const* name_str;
		size_t name_len;
		error_t err = lexer::string_table::find(output->str_tbl, str_id, name_str, name_len);
		O_ASSERT(err_ok == err);
		return name_str;
	}

	char8_t const* get_symbol_name(output_t* output, symbol_id symbol)
	{
		uint32_t name = output->sym_tbl->name(symbol);
		return get_string(output, name);
	}

	type_t* get_symbol_type(output_t* output, symbol_id symbol)
	{
		return (type_t*)output->sym_tbl->metadata(symbol);
	}


	void print_type_void(output_t* output, type_void_t* syntax)
	{
		print(output, "type<void> \n");
	}

	void print_type_name(output_t* output, type_name_t* syntax)
	{
		va_list_t va_list = { va(get_symbol_name(output, syntax->name)) };
		print(output, "type<name> '%s' \n", va_list);
	}

	void print_type_ptr(output_t* output, type_ptr_t* syntax)
	{
		print(output, "type<ptr> \n");
		print_push(output);
		print(output, syntax->type);
		print_pop(output);
	}
	
	void print_type_func(output_t* output, type_func_t* syntax)
	{
		va_list_t va_list = { va(syntax->arg_size), va(syntax->is_va_arg ? ", ..." : "") };

		print(output, "type<func (%ud%s)> \n", va_list);
		print(output, "{ret} \n");
		print_push(output);
		print(output, syntax->ret_type);
		print_pop(output);
		for (size_t i = 0; i < syntax->arg_size; ++i)
		{
			va_list_t va_list = { va(i), va(get_string(output, syntax->arg_data[i].name)) };

			print(output, "{arg: %ud\t'%s'} \n", va_list);
			print_push(output);
			print(output, syntax->arg_data[i].type);
			print_pop(output);
		}	
	}

	void print_type_array(output_t* output, type_array_t* syntax)
	{
		va_list_t va_list = { va(syntax->ar_size) };

		print(output, syntax->ar_size ? "type<array [%ud]> \n" : "type<array []> \n", va_list);
		print_push(output);
		print(output, syntax->el_type);
		print_pop(output);
	}

	void print_define_struct(output_t* output, define_struct_t* syntax)
	{
		va_list_t va_list = { va(syntax->member_size), va(get_symbol_name(output, syntax->name)) };

		print(output, "define<struct {%ud}> '%s' \n", va_list);
		for (size_t i = 0; i < syntax->member_size; ++i)
		{
			va_list_t va_list = { va(i), va(get_symbol_name(output, syntax->member_data[i])) };

			print(output, "{member: %ud\t'%s'} \n", va_list);
			print_push(output);
			print(output, get_symbol_type(output, syntax->member_data[i]));
			print_pop(output);
		}
	}

	void print_define_func(output_t* output, define_func_t* syntax)
	{
		va_list_t va_list = { va(syntax->arg_size), va(get_symbol_name(output, syntax->name)) };
		print(output, "define<func (%ud)> '%s' \n", va_list);
		print(output, "{signature} \n");
		auto* type = (type_func_t*)get_symbol_type(output, syntax->name);
		print_push(output);
		print(output, type);
		print_pop(output);
		print(output, "{body} \n");
		print_push(output);
		print(output, syntax->body);
		print_pop(output);
	}

	void print_define_var(output_t* output, define_var_t* syntax)
	{
		va_list_t va_list = { va(get_symbol_name(output, syntax->name)) };
		print(output, "define<var> '%s' \n", va_list);
		print(output, "{type} \n");
		print_push(output);
		print(output, get_symbol_type(output, syntax->name));
		print_pop(output);
		print(output, "{init} \n");
		print_push(output);
		if (syntax->init)
			print(output, syntax->init);
		else
		{
			va_list_t va_list = { va(get_string(output, 0)) };
			print(output, "%s \n", va_list);
		}
		print_pop(output);
	}

	void print_define_namespace(output_t* output, define_namespace_t* syntax)
	{
		va_list_t va_list = { va(syntax->statement_size), va(syntax->name ? get_symbol_name(output, syntax->name) : "_GLOBAL") };
		print(output, "define<namespace {%ud}> '%s' \n", va_list);
		for (size_t i = 0; i < syntax->statement_size; ++i)
		{
			va_list_t va_list = { va(i) };
			print(output, "{statement: %ud} \n", va_list);
			print_push(output);
			print(output, syntax->statement_data[i]);
			print_pop(output);
		}
	}

	void print_func_scope(output_t* output, func_scope_t* syntax)
	{
		va_list_t va_list = { va(syntax->statement_size) };
		print(output, "flow<scope {%ud}> \n", va_list);

		for (size_t i = 0; i < syntax->statement_size; ++i)
		{
			va_list_t va_list = { va(i) };
			print(output, "{statement: %ud} \n", va_list);
			print_push(output);
			print(output, syntax->statement_data[i]);
			print_pop(output);
		}
	}

	char8_t const* get_vflags(vflags_t vflags)
	{
		switch (vflag_mask & vflags)
		{
		case vflag_rvalue:
			return "r-value";
		case vflag_lvalue:
			return "l-value";
		default:
			return "VFLAG_ERROR";
		}
	}

	void print_operator_array(output_t* output, operator_array_t* syntax)
	{
		print(output, "operator<prefix []> \n");
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}

		{
			print(output, "{rhs:} \n");
			print_push(output);
			print(output, syntax->rhs);
			print_pop(output);
		}

		{
			print(output, "{index:} \n");
			print_push(output);
			print(output, syntax->index);
			print_pop(output);
		}
	}

	void print_operator_prefix(output_t* output, operator_prefix_t* syntax)
	{

		char8_t op[3] = { 0, 0, 0 };
		u::mem_copy(op, &syntax->_vflag, 2);
		va_list_t va_list = { va(op) };

		print(output,"operator<prefix %s> \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}

		{
			print(output, "{rhs:} \n");
			print_push(output);
			print(output, syntax->rhs);
			print_pop(output);
		}
	}

	void print_operator_postfix(output_t* output, operator_postfix_t* syntax)
	{

		char8_t op[3] = { 0, 0, 0 };
		u::mem_copy(op, &syntax->_vflag, 2);
		va_list_t va_list = { va(op) };

		print(output, "operator<postfix %s> \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}

		{
			print(output, "{lhs:} \n");
			print_push(output);
			print(output, syntax->lhs);
			print_pop(output);
		}
	}

	void print_operator_binary(output_t* output, operator_binary_t* syntax)
	{

		char8_t op[3] = { 0, 0, 0 };
		u::mem_copy(op, &syntax->_vflag, 2);
		va_list_t va_list = { va(op) };

		print(output, "operator<binary %s> \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}

		{
			print(output, "{lhs:} \n");
			print_push(output);
			print(output, syntax->lhs);
			print_pop(output);
		}

		{
			print(output, "{rhs:} \n");
			print_push(output);
			print(output, syntax->rhs);
			print_pop(output);
		}
	}

	void print_operator_access(output_t* output, operator_access_t* syntax)
	{
		va_list_t va_list = { va(get_symbol_name(output, syntax->name)) };
		print(output, "operator<access .> '%s' \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}

		{
			print(output, "{lhs:} \n");
			print_push(output);
			print(output, syntax->lhs);
			print_pop(output);
		}
	}

	void print_operator_subscript(output_t* output, operator_subscript_t* syntax)
	{
		print(output, "operator<subscript []> \n");
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}

		{
			print(output, "{lhs:} \n");
			print_push(output);
			print(output, syntax->lhs);
			print_pop(output);
		}

		{
			print(output, "{index:} \n");
			print_push(output);
			print(output, syntax->index);
			print_pop(output);
		}
	}

	void print_operator_call(output_t* output, operator_call_t* syntax)
	{
		va_list_t va_list = { va(syntax->arg_size) };
		print(output, "operator<call (%ud)> \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}

		{
			print(output, "{lhs:} \n");
			print_push(output);
			print(output, syntax->lhs);
			print_pop(output);
		}

		for (size_t i = 0; i < syntax->arg_size; ++i)
		{
			va_list_t va_list = { va(i) };
			print(output, "{arg: %ud} \n", va_list);
			print_push(output);
			print(output, syntax->arg_data[i]);
			print_pop(output);
		}
	}

	void print_value_number(output_t* output, value_number_t* syntax)
	{
		va_t number[1];
		char8_t buffer[32];
		u::mem_clr(buffer, sizeof(buffer));

		O_ASSERT(sid_type_name == syntax->type->_sid);
		switch (((type_name_t*)syntax->type)->name)
		{
		case typesymbol_msize:
			number[0] = va(syntax->number.msize);
			printf(buffer, "%ud", number);
			break;
		case typesymbol_int8:
			number[0] = va(syntax->number.sint8);
			printf(buffer, "%d", number);
			break;
		case typesymbol_int16:
			number[0] = va(syntax->number.sint16);
			printf(buffer, "%d", number);
			break;
		case typesymbol_int32:
			number[0] = va(syntax->number.sint32);
			printf(buffer, "%d", number);
			break;
		case typesymbol_int64:
			number[0] = va(syntax->number.sint64);
			printf(buffer, "%d", number);
			break;
		case typesymbol_uint8:
			number[0] = va(syntax->number.uint8);
			printf(buffer, "%ud", number);
			break;
		case typesymbol_uint16:
			number[0] = va(syntax->number.uint16);
			printf(buffer, "u%d", number);
			break;
		case typesymbol_uint32:
			number[0] = va(syntax->number.uint32);
			printf(buffer, "%ud", number);
			break;
		case typesymbol_uint64:
			number[0] = va(syntax->number.uint64);
			printf(buffer, "%ud", number);
			break;
		case typesymbol_flt32:
			number[0] = va(syntax->number.flt32);
			printf(buffer, "%f", number);
			break;
		case typesymbol_flt64:
			number[0] = va(syntax->number.flt64);
			printf(buffer, "%f", number);
			break;
		default:
			O_ASSERT(false);
			break;
		}

		va_list_t va_list = { va(buffer) };
		print(output, "value<number> '%s' \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}
	}

	void print_value_null(output_t* output, value_null_t* syntax)
	{
		print(output, "value<null> \n");
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}
	}

	void print_value_name(output_t* output, value_name_t* syntax)
	{
		va_list_t va_list = { va(get_symbol_name(output, syntax->name)) };
		print(output, "value<name> '%s' \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
		}
	}

	void print_value_lambda(output_t* output, value_lambda_t* syntax)
	{
		va_list_t va_list = { va(syntax->arg_size) };
		print(output, "value<lambda(%ud)> \n", va_list);
		{
			va_list_t va_list = { va(get_vflags(syntax->_vflag)) };
			print(output, "{result: %s} \n", va_list);
			print_push(output);
			print(output, syntax->type);
			print_pop(output);
			print(output, "{body} \n");
			print_push(output);
			print(output, syntax->body);
			print_pop(output);
		}
	}

	void print(output_t* output, syntax_t* syntax)
	{
		switch (syntax->_sid)
		{
		case sid_type_void:
			print_type_void(output, (type_void_t*)syntax);
			break;
		case sid_type_name:
			print_type_name(output, (type_name_t*)syntax);
			break;
		case sid_type_ptr:
			print_type_ptr(output, (type_ptr_t*)syntax);
			break;
		case sid_type_func:
			print_type_func(output, (type_func_t*)syntax);
			break;
		case sid_type_array:
			print_type_array(output, (type_array_t*)syntax);
			break;
		case sid_define_struct:
			print_define_struct(output, (define_struct_t*)syntax);
			break;
		case sid_define_func:
			print_define_func(output, (define_func_t*)syntax);
			break;
		case sid_define_var:
			print_define_var(output, (define_var_t*)syntax);
			break;
		case sid_define_namespace:
			print_define_namespace(output, (define_namespace_t*)syntax);
			break;
		case sid_func_scope:
			print_func_scope(output, (func_scope_t*)syntax);
			break;
		case sid_operator_array:
			print_operator_array(output, (operator_array_t*)syntax);
			break;
		case sid_operator_prefix:
			print_operator_prefix(output, (operator_prefix_t*)syntax);
			break;
		case sid_operator_postfix:
			print_operator_postfix(output, (operator_postfix_t*)syntax);
			break;
		case sid_operator_binary:
			print_operator_binary(output, (operator_binary_t*)syntax);
			break;
		case sid_operator_access:
			print_operator_access(output, (operator_access_t*)syntax);
			break;
		case sid_operator_subscript:
			print_operator_subscript(output, (operator_subscript_t*)syntax);
			break;
		case sid_operator_call:
			print_operator_call(output, (operator_call_t*)syntax);
			break;
		case sid_value_number:
			print_value_number(output, (value_number_t*)syntax);
			break;
		case sid_value_null:
			print_value_null(output, (value_null_t*)syntax);
			break;
		case sid_value_name:
			print_value_name(output, (value_name_t*)syntax);
			break;
		case sid_value_lambda:
			print_value_lambda(output, (value_lambda_t*)syntax);
			break;
		default:
			O_ASSERT(false);
			break;
		}
	}

} }

