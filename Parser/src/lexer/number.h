#pragma once
#include "lexer.h"

namespace outland { namespace lexer { namespace number
{
	struct ctx_t
	{
		uint64_t	_value;
		uint8_t		_digits;
	};

	state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end);

} } }