#include "pch.h"
#include "token.h"

namespace outland { namespace lexer
{

	class string_table_t
	{
		struct string_t
		{
			char8_t const*	str;
			size_t			len;
		};

		struct entry_t
		{
			string_t	string;
			size_t		reuse;
		};

		scratch_allocator_t*			_allocator;
		array<entry_t>					_table;
		hash_map<uint64_t, string_t>	_map;

	public:
		string_table_t(allocator_t* allocator, scratch_allocator_t* string_allocator);
		friend error_t	string_table::insert(string_table_t* table, uint64_t& handle, char8_t const* str, size_t size);
		friend error_t	string_table::find(string_table_t* table, uint64_t handle, char8_t const*& str, size_t& len);
		friend void		string_table::dump(string_table_t* table, void* ctx, void(*fnc)(void* ctx, char8_t const* str, size_t len, size_t reuse));

		static void		memory_info(memory_info_t& info, size_t (&offset_arr)[2]);
		static bool		compare(string_t k1, string_t k2);
		static uint32_t hash(string_t k1);
	};

	string_table_t::string_table_t(allocator_t* allocator, scratch_allocator_t* string_allocator)
		: _allocator(string_allocator)
		, _table(allocator)
		, _map(allocator, decltype(_map)::fnc_hash_t().bind<&string_table_t::hash>(), decltype(_map)::fnc_cmp_t().bind<&string_table_t::compare>())
	{

	}

	bool		string_table_t::compare(string_t k1, string_t k2)
	{
		if (k1.len != k2.len)
			return false;
		
		return u::str_cmp(k1.str, k2.str, k2.len);
	}

	uint32_t	string_table_t::hash(string_t e)
	{
		char8_t const* it = e.str;
		char8_t const* end = e.str + e.len;

		uint32_t hash = 0;

		while (it != end)
		{
			hash += *it;
			hash += (hash << 10);
			hash ^= (hash >> 6);
			++it;
		}

		hash += (hash << 3);
		hash ^= (hash >> 11);
		hash += (hash << 15);

		return hash;
	}

	void	string_table_t::memory_info(memory_info_t& info, size_t(&offset_arr)[2])
	{
		memory_info_t scratch_info;
		scratch_allocator::memory_info(scratch_info);

		size_t align_arr[] = { alignof(string_table_t), scratch_info.align };
		size_t size_arr[] = { sizeof(string_table_t), scratch_info.size };

		u::mem_offsets(align_arr, size_arr, offset_arr, info.align, info.size, u::array_size(size_arr));
	}

	void	string_table::memory_info(memory_info_t& info)
	{
		size_t offset_arr[2];
		string_table_t::memory_info(info, offset_arr);
	}

	void	string_table::create(string_table_t*& table, create_t const& create, void* memory)
	{
		memory_info_t info;
		size_t offset_arr[2];
		string_table_t::memory_info(info, offset_arr);

		scratch_allocator_t* scratch;
		{
			scratch_allocator::create_t scratch_create;
			scratch_create.init_buffer = nullptr;
			scratch_create.init_size = 0;
			scratch_create.page_source = create.allocator;
			scratch_create.page_size = 1024 * 16;
			scratch_allocator::create(scratch, scratch_create, (byte_t*)memory + offset_arr[1]);
		}

		table = O_CREATE(string_table_t, memory)(create.allocator, scratch);
	}

	error_t string_table::insert(string_table_t* table, uint64_t& handle, char8_t const* str, size_t size)
	{
		error_t err;

		string_table_t::string_t string;
		string.str = str;
		string.len = size;
		err = table->_map.find(string, handle);
		if (err_ok == err)
		{
			++(table->_table[(size_t)handle].reuse);
			return err_ok;
		}

		char8_t* data = nullptr;
		handle = (uint64_t)table->_table.size();
		err = u::mem_alloc(table->_allocator, data, size + 1);
		if (err)
			return err;

		u::mem_copy(data, str, size);
		data[size] = u8'\0';

		string.str = data;
		err = table->_map.insert(string, handle);
		if (err)
			return err;

		string_table_t::entry_t entry;
		entry.string = string;
		entry.reuse = 1;
		err = table->_table.push_back(entry);
		if (err)
			return err;

		return err_ok;
	}

	error_t string_table::find(string_table_t* table, uint64_t handle, char8_t const*& str, size_t& len)
	{
		if (handle >= table->_table.size())
			return err_invalid_handle;

		str = table->_table[handle].string.str;
		len = table->_table[handle].string.len;

		return err_ok;
	}

	void	string_table::dump(string_table_t* table, void* ctx, void(*fnc)(void* ctx, char8_t const* str, size_t len, size_t reuse))
	{
		for (auto& it : table->_table)
		{
			fnc(ctx, it.string.str, it.string.len, it.reuse);
		}
	}

} }
