#pragma once
#include "token.h"

namespace outland { namespace lexer
{
	
	enum state_t;
	struct lexer_t;
	using state_fnc_t = state_t(*)(void*, lexer_t* lexer, char8_t const* str, char8_t const* end);
	using state_call_t = function<state_t(lexer_t* lexer, char8_t const* str, char8_t const* end)>;

	struct create_t
	{
		string_table_t*		table;
	};

	void	memory_info(memory_info_t& info);
	void	create(lexer_t*& lexer, create_t const& create, void* memory);
	error_t	parse_packet(lexer_t* lexer, char8_t const* str, size_t len, size_t& parsed, token_t* tokens, size_t capacity, size_t& lexed);
	error_t parse_finish(lexer_t* lexer, token_t* tokens, size_t capacity, size_t& lexed);
	state_t store_error(lexer_t* lexer, char8_t const* str, char8_t const* message);
	state_t store_state(lexer_t* lexer, char8_t const* str, state_fnc_t fnc);

	state_t store_whitespace(lexer_t* lexer, char8_t const* str, uint32_t codepoints);
	state_t store_number(lexer_t* lexer, char8_t const* str, uint64_t value, token_flags_t flags, uint32_t codepoints);
	state_t store_string(lexer_t* lexer, char8_t const* str, char8_t const* value, size_t size, uint32_t codepoints);
	state_t store_identifier(lexer_t* lexer, char8_t const* str, char8_t const* value, size_t size, uint32_t codepoints);
	state_t store_newline(lexer_t* lexer, char8_t const* str);
	state_t store_comment(lexer_t* lexer, char8_t const* str, uint32_t codepoints, uint32_t lines);
	state_t store_symbol(lexer_t* lexer, char8_t const* str, char8_t value);
} }