#pragma once

namespace outland { namespace lexer
{

	enum token_type_t : uint8_t
	{
		token_number,
		token_string,
		token_identifier,
		token_symbol,
		token_character,
		token_end_of_file,
	};

	using token_flags_t = uint8_t;
	enum token_flag_t : token_flags_t
	{
		flag_whitespace = 1 << 0,
		flag_newline = 1 << 1,
		flag_number_dec = 0 << 2,
		flag_number_hex = 1 << 2,
		flag_number_oct = 2 << 2,
		flag_number_bin = 3 << 2,
		flag_number_MASK = 3 << 2,
	};

	struct token_t
	{
		token_type_t	type;
		token_flags_t	flags;
		uint16_t		codepoint;
		uint32_t		line;
		uint64_t		data;
	};

	class string_table_t;

	namespace string_table
	{
		struct create_t
		{
			allocator_t*	allocator;
		};

		void	memory_info(memory_info_t& info);
		void	create(string_table_t*& table, create_t const& create, void* memory);
		error_t insert(string_table_t* table, uint64_t& handle, char8_t const* str, size_t size);
		error_t find(string_table_t* table, uint64_t handle, char8_t const*& str, size_t& len);
		void	dump(string_table_t* table, void* ctx, void(*fnc)(void* ctx, char8_t const* str, size_t len, size_t reuse));
	}

} }
