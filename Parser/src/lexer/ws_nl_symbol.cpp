#include "pch.h"
#include "ws_nl_symbol.h"
#include "utf8.h"

namespace outland { namespace lexer
{
	namespace newline
	{
		state_t parse_lf_cr(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_lf_cr>);

			if (u8'\r' == *str)
				return store_newline(lexer, str + 1);
			else
				return store_newline(lexer, str);
		}

		state_t parse_cr_lf(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_cr_lf>);

			if(u8'\n' == *str)
				return store_newline(lexer, str + 1); //win
			else
				return store_newline(lexer, str); //old mac
		}

		state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
		{
			O_ASSERT(utf8::is_newline(*str));

			if (u8'\r' == *str)
				return parse_cr_lf(ctx, lexer, str + 1, end); //win or old mac
			else
			{	
				O_ASSERT(u8'\n' == *str);
				return parse_lf_cr(ctx, lexer, str + 1, end); // *nix system
			}
		}
	}

	namespace whitespace
	{
		state_t parse_whitespaces(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
		{
			char8_t const* const beg = str;
			while (str != end && utf8::is_whitespace(*str))
			{
				++str;
			}

			ctx->spaces += (uint32_t)(str - beg);

			if (str != end)
				return store_whitespace(lexer, str, ctx->spaces);
			else
				return store_state(lexer, str, state_call_t::object_call<ctx_t, &parse_whitespaces>);
		}

		state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
		{
			O_ASSERT(utf8::is_whitespace(*str));

			ctx->spaces = 1;
			return parse_whitespaces(ctx, lexer, str + 1, end);
		}
	}

	namespace symbol
	{
		state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
		{
			switch (*str)
			{
			case u8'{':
			case u8'}':
			case u8'[':
			case u8']':
			case u8'(':
			case u8')':
			case u8'+':
			case u8'-':
			case u8'*':
			case u8'.':
			case u8',':
			case u8':':
			case u8';':
			case u8'!':
			case u8'#':
			case u8'?':
			case u8'%':
			case u8'&':
			case u8'$':
			case u8'@':
			case u8'=':
			case u8'<':
			case u8'>':
			case u8'~':
			case u8'|':
			case u8'^':
			case u8'\\':
				return store_symbol(lexer, str + 1, *str);
			default:
				return store_error(lexer, str, "unrecognized symbol");
			}
		}
	}

} }
