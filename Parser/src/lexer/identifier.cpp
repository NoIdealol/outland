#include "pch.h"
#include "identifier.h"
#include "utf8.h"

namespace outland { namespace lexer { namespace identifier
{

	state_t parse_characters(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		char8_t const* beg = str;

		while (true)
		{
			if (str == end)
			{
				size_t parsed_size = str - beg;
				if ((max_size - ctx->size) < parsed_size)
					return store_error(lexer, str, "identifier exceeded max size");

				u::mem_copy(ctx->buffer + ctx->size, beg, parsed_size);
				ctx->size += parsed_size;

				return store_state(lexer, str, state_call_t::object_call<ctx_t, &parse_characters>);
			}

			if (utf8::is_character(*str) || utf8::is_digit(*str))
			{
				++(ctx->codepoints);
				str += utf8::codepoint_size(*str);
			}
			else
			{
				size_t parsed_size = str - beg;
				if ((max_size - ctx->size) < parsed_size)
					return store_error(lexer, str, "identifier exceeded max size");

				if (0 == ctx->size)
				{
					return store_identifier(lexer, str, beg, parsed_size, ctx->codepoints);
				}
				else
				{
					u::mem_copy(ctx->buffer + ctx->size, beg, parsed_size);
					ctx->size += parsed_size;
					return store_identifier(lexer, str, ctx->buffer, ctx->size, ctx->codepoints);
				}
			}			
		}
	}

	state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		O_ASSERT(utf8::is_character(*str));
		ctx->codepoints = 0;
		ctx->size = 0;
		return parse_characters(ctx, lexer, str, end);
	}

} } }
