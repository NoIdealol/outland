#include "pch.h"
#include "lexer.h"
#include "number.h"
#include "string.h"
#include "ws_nl_symbol.h"
#include "identifier.h"
#include "character.h"
#include "comment.h"
#include "utf8.h"

#include "system\debug\output.h"

#include <stdio.h>

namespace outland { namespace lexer
{
	

	enum state_t
	{
		state_resume,
		state_parse,
		state_error,
	};

	union lex_ctx_t
	{
		number::ctx_t				number;
		newline::ctx_t				newline;
		identifier::ctx_t			identifier;
		whitespace::ctx_t			whitespace;
		comment::ctx_t				comment;
		string::ctx_t				string;
		character::ctx_t			character;
		symbol::ctx_t				symbol;
	};

	struct lexer_t
	{
		state_t		_state;
		lex_ctx_t	_ctx;
		state_fnc_t	_fnc;

		char8_t const*	_str;
		char8_t const*	_end;
		uint32_t		_line;
		uint32_t		_codepoint;

		token_flags_t	_flags;
		token_t*		_tokens;
		string_table_t*	_table;
	};

	void	memory_info(memory_info_t& info)
	{
		info.align = alignof(lexer_t);
		info.size = sizeof(lexer_t);
	}

	void create(lexer_t*& lexer, create_t const& create, void* memory)
	{
		u::mem_clr(memory, sizeof(lexer_t));
		lexer = O_CREATE(lexer_t, memory);
		lexer->_state = state_parse;
		lexer->_table = create.table;
		lexer->_flags = 0;
	}

	state_t parse_lexeme(lexer_t* lexer)
	{
		if (utf8::is_whitespace(*lexer->_str))
		{
			return whitespace::parse(&lexer->_ctx.whitespace, lexer, lexer->_str, lexer->_end);
		}
		else if (utf8::is_control(*lexer->_str))
		{
			if (utf8::is_newline(*lexer->_str))
			{
				return newline::parse(&lexer->_ctx.newline, lexer, lexer->_str, lexer->_end);
			}
			else
			{
				return store_error(lexer, lexer->_str, "unsupported control codepoint");
			}
		}
		else if (utf8::is_digit(*lexer->_str))
		{
			return number::parse(&lexer->_ctx.number, lexer, lexer->_str, lexer->_end);
		}
		else if (utf8::is_character(*lexer->_str))
		{
			return identifier::parse(&lexer->_ctx.identifier, lexer, lexer->_str, lexer->_end);
		}
		else if (u8'"' == *lexer->_str)
		{
			return string::parse(&lexer->_ctx.string, lexer, lexer->_str, lexer->_end);
		}		
		else if (u8'\'' == *lexer->_str)
		{
			return character::parse(&lexer->_ctx.character, lexer, lexer->_str, lexer->_end);
		}
		else if (u8'/' == *lexer->_str) //or slash symbol
		{
			return comment::parse(&lexer->_ctx.comment, lexer, lexer->_str, lexer->_end);
		}
		else
		{
			return symbol::parse(&lexer->_ctx.symbol, lexer, lexer->_str, lexer->_end);
		}
	}

	state_t parse_resume(lexer_t* lexer)
	{
		return lexer->_fnc(&lexer->_ctx, lexer, lexer->_str, lexer->_end);
	}

	error_t	parse_packet(lexer_t* lexer, char8_t const* str, size_t len, size_t& parsed, token_t* tokens, size_t capacity, size_t& lexed)
	{
		lexer->_str = str;
		lexer->_end = str + len;
		lexer->_tokens = tokens;
		token_t* const tokens_end = tokens + capacity;

		parsed = 0;
		lexed = 0;

		while (lexer->_str != lexer->_end && lexer->_tokens != tokens_end)
		{
			switch (lexer->_state)
			{
			case state_resume:
				lexer->_state = parse_resume(lexer);
				break;
			case state_parse:
				lexer->_state = parse_lexeme(lexer);
				break;
			case state_error:
				return err_bad_data;
			default:
				return err_unknown;
			}
		}

		parsed = lexer->_str - str;
		lexed = lexer->_tokens - tokens;

		return err_ok;
	}

	error_t parse_finish(lexer_t* lexer, token_t* tokens, size_t capacity, size_t& lexed)
	{
		if (2 > capacity)
			return err_invalid_parameter;

		char8_t const null = u8'\0';

		lexer->_str = &null;
		lexer->_end = &null + 1;
		lexer->_tokens = tokens;

		if (state_resume == lexer->_state)
			lexer->_state = parse_resume(lexer);

		O_ASSERT(&null == lexer->_str);

		switch (lexer->_state)
		{
		case state_parse:
			break;
		case state_resume:
		case state_error:
			return err_bad_data;
		default:
			return err_unknown;
		}

		lexer->_tokens->line = lexer->_line;
		lexer->_tokens->codepoint = lexer->_codepoint;
		lexer->_tokens->flags = lexer->_flags;

		lexer->_tokens->type = token_end_of_file;
		lexer->_tokens->data = null;

		lexer->_flags = 0;
		++(lexer->_tokens);

		lexed = lexer->_tokens - tokens;
		return err_ok;
	}

	state_t store_error(lexer_t* lexer, char8_t const* str, char8_t const* message)
	{
		lexer->_str = str;
		os::debug_string(message);
		os::debug_string("\n");
		return state_error;
	}

	state_t store_state(lexer_t* lexer, char8_t const* str, state_fnc_t fnc)
	{
		lexer->_str = str;
		lexer->_fnc = fnc;
		return state_resume;
	}

	state_t store_whitespace(lexer_t* lexer, char8_t const* str, uint32_t codepoints)
	{
		lexer->_str = str;
		lexer->_flags |= flag_whitespace;

		lexer->_codepoint += codepoints;

		char8_t out[64] = {};
		snprintf(out, sizeof(out), "whitespace: %u\n", codepoints);
		os::debug_string(out);


		return state_parse;
	}

	state_t store_number(lexer_t* lexer, char8_t const* str, uint64_t value, token_flags_t flags, uint32_t codepoints)
	{
		lexer->_str = str;
		lexer->_tokens->line = lexer->_line;
		lexer->_tokens->codepoint = lexer->_codepoint;
		lexer->_tokens->flags = lexer->_flags | flags;

		lexer->_tokens->type = token_number;
		lexer->_tokens->data = value;

		lexer->_codepoint += codepoints;

		lexer->_flags = 0;
		++(lexer->_tokens);

		char8_t out[64] = {};
		snprintf(out, sizeof(out), "number: %llu\n", value);
		os::debug_string(out);

		return state_parse;
	}

	state_t store_string(lexer_t* lexer, char8_t const* str, char8_t const* value, size_t size, uint32_t codepoints)
	{
		error_t err;
		uint64_t handle;
		err = string_table::insert(lexer->_table, handle, value, size);
		if (err)
		{
			//todo: store err?
			return store_error(lexer, lexer->_str, "error storing string into table");
		}

		lexer->_str = str;
		lexer->_tokens->line = lexer->_line;
		lexer->_tokens->codepoint = lexer->_codepoint;
		lexer->_tokens->flags = lexer->_flags;

		lexer->_tokens->type = token_string;
		lexer->_tokens->data = handle;

		lexer->_codepoint += codepoints;

		lexer->_flags = 0;
		++(lexer->_tokens);



		char8_t val[string::max_size + 1] = {};
		u::mem_copy(val, value, size);
		val[size] = u8'\0';

		char8_t out[64 + string::max_size] = {};
		snprintf(out, sizeof(out), "string: %s\n", val);
		os::debug_string(out);

		return state_parse;
	}

	state_t store_identifier(lexer_t* lexer, char8_t const* str, char8_t const* value, size_t size, uint32_t codepoints)
	{
		error_t err;
		uint64_t handle;
		err = string_table::insert(lexer->_table, handle, value, size);
		if (err)
		{
			//todo: store err?
			return store_error(lexer, lexer->_str, "error storing identifier into table");
		}

		lexer->_str = str;
		lexer->_tokens->line = lexer->_line;
		lexer->_tokens->codepoint = lexer->_codepoint;
		lexer->_tokens->flags = lexer->_flags;

		lexer->_tokens->type = token_identifier;
		lexer->_tokens->data = handle;

		lexer->_codepoint += codepoints;

		lexer->_flags = 0;
		++(lexer->_tokens);


		char8_t val[identifier::max_size + 1] = {};
		u::mem_copy(val, value, size);
		val[size] = u8'\0';

		char8_t out[64 + identifier::max_size] = {};
		snprintf(out, sizeof(out), "identifier: %s\n", val);
		os::debug_string(out);

		return state_parse;
	}

	state_t store_newline(lexer_t* lexer, char8_t const* str)
	{
		lexer->_str = str;
		lexer->_flags |= flag_newline;

		lexer->_codepoint = 0;
		++lexer->_line;

		os::debug_string("newline\n");
		return state_parse;
	}

	state_t store_comment(lexer_t* lexer, char8_t const* str, uint32_t codepoints, uint32_t lines)
	{
		lexer->_str = str;
		lexer->_flags |= flag_whitespace;
		if (lines)
		{
			lexer->_flags |= flag_newline;
			lexer->_codepoint = 0;
			lexer->_line += lines;
		}
		else
		{
			lexer->_codepoint += codepoints;
		}


		char8_t out[64] = {};
		snprintf(out, sizeof(out), "comment: c:%u, l:%u\n", codepoints, lines);
		os::debug_string(out);

		return state_parse;
	}

	state_t store_symbol(lexer_t* lexer, char8_t const* str, char8_t value)
	{
		lexer->_str = str;
		lexer->_tokens->line = lexer->_line;
		lexer->_tokens->codepoint = lexer->_codepoint;
		lexer->_tokens->flags = lexer->_flags;

		lexer->_tokens->type = token_symbol;
		lexer->_tokens->data = value;

		++(lexer->_codepoint);

		lexer->_flags = 0;
		++(lexer->_tokens);

		char8_t val[] = { value, '\0' };
		char8_t out[64] = {};
		snprintf(out, sizeof(out), "symbol: %s\n", val);
		os::debug_string(out);

		return state_parse;
	}

} }