#include "pch.h"
#include "string.h"
#include "utf8.h"

namespace outland { namespace lexer { namespace string
{
	state_t parse_characters(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		char8_t const* beg = str;

		while (true)
		{
			if (str == end)
			{
				size_t parsed_size = str - beg;
				if((max_size - ctx->size) < parsed_size)
					return store_error(lexer, str, "string literal exceeded max size");

				u::mem_copy(ctx->buffer + ctx->size, beg, parsed_size);
				ctx->size += parsed_size;

				return store_state(lexer, str, state_call_t::object_call<ctx_t, &parse_characters>);
			}

			if (utf8::is_control(*str))
			{
				return store_error(lexer, str, "unsupported control character");
			}
			else if (ctx->is_escaped)
			{
				ctx->is_escaped = false;
			}
			else
			{
				if (u8'"' == *str)
				{
					size_t parsed_size = str - beg;
					if ((max_size - ctx->size) < parsed_size)
						return store_error(lexer, str, "string literal exceeded max size");

					if (0 == ctx->size)
					{
						return store_string(lexer, str + 1, beg, parsed_size, ctx->codepoints + 1);
					}
					else
					{
						u::mem_copy(ctx->buffer + ctx->size, beg, parsed_size);
						ctx->size += parsed_size;
						return store_string(lexer, str + 1, ctx->buffer, ctx->size, ctx->codepoints + 1);
					}
				}

				if (u8'\\' == *str)
				{
					ctx->is_escaped = true;
					//++(ctx->escape_size);
				}
			}

			++(ctx->codepoints);
			str += utf8::codepoint_size(*str);
		}
	}

	state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		O_ASSERT(u8'"' == *str);
		ctx->codepoints = 1;
		ctx->size = 0;
		ctx->is_escaped = false;
		//ctx->escape_size = 0;
		return parse_characters(ctx, lexer, str + 1, end);
	}

} } }
