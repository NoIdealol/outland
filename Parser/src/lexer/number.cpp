#include "pch.h"
#include "number.h"
#include "utf8.h"

namespace outland { namespace lexer { namespace number
{

	state_t parse_decimal(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		enum : uint64_t
		{
			_limit = c::max_uint64 / 10,
		};

		uint64_t val;

		while (true)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_decimal>);

			if ((u8'0' <= *str && u8'9' >= *str))
				val = *str - u8'0';
			else
			{
				if (ctx->_digits)
					return store_number(lexer, str, ctx->_value, flag_number_dec, ctx->_digits);
				else
					return store_error(lexer, str, "no digits in number");
			}

			if ((20 == ctx->_digits) || 
				(19 == ctx->_digits && ((_limit == ctx->_value && 5 < val) || (_limit < ctx->_value))))
				return store_error(lexer, str, "too many digits in number");

			ctx->_value = (ctx->_value * 10) + val;
			++ctx->_digits;

			++str;
		}
	}

	state_t parse_hexa(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		uint64_t val;

		while (true)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_hexa>);

			if (utf8::is_digit(*str))
				val = *str - u8'0';
			else if ((u8'a' <= *str && u8'f' >= *str))
				val = *str - u8'a' + 10;
			else if ((u8'A' <= *str && u8'F' >= *str))
				val = *str - u8'A' + 10;
			else
			{
				if (ctx->_digits)
					return store_number(lexer, str, ctx->_value, flag_number_hex, ctx->_digits);
				else
					return store_error(lexer, str, "no digits in number");
			}

			if (16 == ctx->_digits)
				return store_error(lexer, str, "too many digits in number");

			ctx->_value = (ctx->_value << 4) + val;
			++ctx->_digits;
			
			++str;
		}
	}

	state_t parse_binary(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		uint64_t val;

		while (true)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_binary>);

			if ((u8'0' <= *str && u8'1' >= *str))
				val = *str - u8'0';
			else
			{
				if (ctx->_digits)
					return store_number(lexer, str, ctx->_value, flag_number_bin, ctx->_digits);
				else
					return store_error(lexer, str, "no digits in number");
			}

			if (64 == ctx->_digits)
				return store_error(lexer, str, "too many digits in number");

			ctx->_value = (ctx->_value << 1) + val;
			++ctx->_digits;

			++str;
		}
	}

	state_t parse_octal(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{	
		uint64_t val;

		while (true)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_octal>);

			if ((u8'0' <= *str && u8'7' >= *str))
				val = *str - u8'0';
			else
			{
				if (ctx->_digits)
					return store_number(lexer, str, ctx->_value, flag_number_oct, ctx->_digits);
				else
					return store_error(lexer, str, "no digits in number");
			}

			if (21 == ctx->_digits)
				return store_error(lexer, str, "too many digits in number");

			ctx->_value = (ctx->_value << 3) + val;
			++ctx->_digits;

			++str;
		}
	}

	state_t parse_base(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		if (str == end)
			return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_base>);

		if (utf8::is_digit(*str))
		{
			ctx->_digits = 1;
			return parse_decimal(ctx, lexer, str + 1, end);
		}
		else
		{
			switch (*str)
			{
			case u8'x':
				return parse_hexa(ctx, lexer, str + 1, end);
			case u8'b':
				return parse_binary(ctx, lexer, str + 1, end);
			case u8'o':
				return parse_octal(ctx, lexer, str + 1, end);
			default:
				ctx->_digits = 1;
				return store_number(lexer, str, 0, flag_number_dec, 1);
			}
		}
	}

	state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		O_ASSERT(utf8::is_digit(*str));

		ctx->_value = 0;
		ctx->_digits = 0;

		if (u8'0' == *str)
			return parse_base(ctx, lexer, str + 1, end);
		else
			return parse_decimal(ctx, lexer, str, end);
	}

} } }
