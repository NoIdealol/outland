#pragma once
#include "lexer.h"

namespace outland { namespace lexer
{
	namespace newline
	{
		struct ctx_t
		{
			
		};

		state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end);
	}

	namespace whitespace
	{
		struct ctx_t
		{
			uint32_t spaces;
		};

		state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end);
	}

	namespace symbol
	{
		struct ctx_t
		{
		};

		state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end);
	}

} }