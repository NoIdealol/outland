#pragma once
#include "lexer.h"

namespace outland { namespace lexer { namespace identifier
{
	enum : size_t
	{
		max_size = 1024,
	};

	struct ctx_t
	{
		uint32_t	codepoints;
		size_t		size;
		char8_t		buffer[max_size];
	};

	state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end);

} } }