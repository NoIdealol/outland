#pragma once

namespace outland { namespace lexer { namespace utf8
{
	inline bool is_character(char8_t ch)
	{
		return (u8'_' <= ch && u8'z' >= ch) || (u8'A' <= ch && u8'Z' >= ch) || (0x80 & ch);
	}

	inline size_t codepoint_size(char8_t ch)
	{
		if ((0b10000000 & ch) == 0)
			return 1;
		if ((0b11100000 & ch) == 0b11000000)
			return 2;
		if ((0b11110000 & ch) == 0b11100000)
			return 3;

		O_ASSERT((0b11111000 & ch) == 0b11110000);
		return 4;
	}

	inline bool is_whitespace(char8_t ch)
	{
		return (ch == ' ') || (ch == '\t');
	}

	inline bool is_digit(char8_t ch)
	{
		return (u8'0' <= ch && u8'9' >= ch);
	}

	inline bool is_control(char8_t ch)
	{
		return (0x00 <= ch && 0x1F >= ch) || (0x7F == ch);
	}

	inline bool is_newline(char8_t ch)
	{
		return (u8'\n' == ch) || (u8'\r' == ch);
	}

} } }