#include "pch.h"
#include "comment.h"
#include "utf8.h"

namespace outland { namespace lexer { namespace comment
{
	state_t parse_single_line_lf_cr(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		if (str == end)
			return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_single_line_lf_cr>);

		if (u8'\r' == *str)
			return store_comment(lexer, str + 1, ctx->codepoints + 1, 1);
		else
			return store_comment(lexer, str, ctx->codepoints, 1);
	}

	state_t parse_single_line_cr_lf(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		if (str == end)
			return store_state(lexer, str, state_call_t::object_call<ctx_t, parse_single_line_cr_lf>);

		if (u8'\n' == *str)
			return store_comment(lexer, str + 1, ctx->codepoints + 1, 1);
		else
			return store_comment(lexer, str, ctx->codepoints, 1);
	}

	state_t parse_single_line(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		while (true)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, &parse_single_line>);

			if (utf8::is_control(*str))
			{
				if(u8'\0' == *str)
					return store_comment(lexer, str, ctx->codepoints, 0);
				if (utf8::is_newline(*str))
				{
					++(ctx->codepoints);

					if (u8'\r' == *str)
						return parse_single_line_cr_lf(ctx, lexer, str + 1, end);
					else
						return parse_single_line_lf_cr(ctx, lexer, str + 1, end);
				}
				if(!utf8::is_whitespace(*str))
					return store_error(lexer, str, "unsuported control character");
			}

			++(ctx->codepoints);
			str += utf8::codepoint_size(*str);
		}
	}
	
	state_t parse_multi_line(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		while (true)
		{
			if (str == end)
				return store_state(lexer, str, state_call_t::object_call<ctx_t, &parse_multi_line>);

			switch (ctx->last)
			{
			case u8'\r':
				++(ctx->lines);
				ctx->codepoints = 0;
				ctx->last = u8'\0';
				if (u8'\n' == *str)
				{
					++str;
					continue;
				}
				break;
			case u8'\n':
				++(ctx->lines);
				ctx->codepoints = 0;
				ctx->last = u8'\0';
				if (u8'\r' == *str)
				{
					++str;
					continue;
				}
				break;
			case u8'*':
				if (u8'/' == *str)
				{
					return store_comment(lexer, str + 1, ctx->codepoints + 1, ctx->lines);
				}
				ctx->last = u8'\0';
				break;
			default:
				O_ASSERT(u8'\0' == ctx->last);
				break;
			}

			if (utf8::is_control(*str))
			{
				if (u8'\0' == *str)
					return store_error(lexer, str, "unclosed comment");
				if (utf8::is_newline(*str))
					ctx->last = *str;
				else if (!utf8::is_whitespace(*str))
					return store_error(lexer, str, "unsuported control character");
			}
			else if (u8'*' == *str)
			{
				ctx->last = *str;
			}

			++(ctx->codepoints);
			str += utf8::codepoint_size(*str);
		}
	}

	state_t parse_branch(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		if (str == end)
			return store_state(lexer, str, state_call_t::object_call<ctx_t, &parse_branch>);

		switch (*str)
		{
		case u8'/':
			//ctx->lines = 0;
			ctx->codepoints = 2;
			return parse_single_line(ctx, lexer, str + 1, end);
		case u8'*':
			ctx->last = u8'\0';
			ctx->lines = 0;
			ctx->codepoints = 2;
			return parse_multi_line(ctx, lexer, str + 1, end);
		default:
			return store_symbol(lexer, str, u8'/');
		}
	}

	state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end)
	{
		O_ASSERT(u8'/' == *str);
		return parse_branch(ctx, lexer, str + 1, end);
	}

} } }
