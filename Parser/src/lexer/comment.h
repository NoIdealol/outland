#pragma once
#include "lexer.h"

namespace outland { namespace lexer { namespace comment
{
	struct ctx_t
	{
		uint32_t	codepoints;
		uint32_t	lines;
		char8_t		last;
	};

	state_t parse(ctx_t* ctx, lexer_t* lexer, char8_t const* str, char8_t const* end);

} } }