#include "pch.h"
#include "main\main.h"
//#include "application.h"

#include "math\integer.h"
#include "core\hash_map.h"

#include "..\lexer\number.h"
#include "lexer\lexer.h"
#include "system\file\file.h"
#include "system\thread\thread.h"

#include "system\debug\output.h"
#include <stdio.h>
#include <cstdarg>

#include "parser\parser.h"
//#include ""


namespace outland
{
	

	//converts data to utf-8
	class atomizer_t
	{
		byte_t*	_data;
		size_t	_size;

		size_t	_head;
		size_t	_freed;
		size_t	const _packet_size;

	public:
		atomizer_t(size_t packet_size)
			: _data(nullptr)
			, _size(0)
			, _head(0)
			, _freed(0)
			, _packet_size(packet_size)
		{

		}

		void	init_data(byte_t* data, size_t size)
		{
			_data = data;
			_size = size;
			_head = 0;
			_freed = 0;
		}

		void	free_packet(size_t size)
		{
			O_ASSERT(size <= _packet_size);
			_freed += size;
			O_ASSERT(_freed <= _head);
		}

		error_t	atomize_ascii(char8_t*& str, size_t& size) //return a packet of normalized utf8 data
		{
			enum : uint8_t
			{
				_ascii_1_mask = uint8_t(128),

			};

			enum : uint64_t
			{
				_ascii_8_mask =
					(uint64_t(128) << 56) | (uint64_t(128) << 48) | (uint64_t(128) << 40) | (uint64_t(128) << 32) |
					(uint64_t(128) << 24) | (uint64_t(128) << 16) | (uint64_t(128) << 8) | (uint64_t(128) << 0),
			};

			size_t const old_ready = (_head - _freed);
			size_t to_process = m::min(_size, _packet_size - old_ready);
			size = old_ready + to_process;
			str = (char8_t*)(_data + _freed);

			byte_t* it = _data + _head;

			{
				size_t process = m::min(u::size_align(_head, 8) - _head, to_process);
				byte_t* const end = it + process;
				while (it != end)
				{
					uint8_t data = *(uint8_t*)it;
					if (_ascii_1_mask & data)
						return err_bad_data;

					++it;
				}

				_head += process;
				to_process -= process;
			}

			{
				size_t process = ((to_process >> 3) << 3);
				byte_t* const end = it + process;
				while (it != end)
				{
					uint64_t data = *(uint64_t*)it;
					if (_ascii_8_mask & data)
						return err_bad_data;

					it += 8;
				}

				_head += process;
				to_process -= process;
			}

			{
				size_t process = to_process;
				byte_t* const end = it + process;
				while (it != end)
				{
					uint8_t data = *(uint8_t*)it;
					if (_ascii_1_mask & data)
						return err_bad_data;

					++it;
				}

				_head += process;
				to_process -= process;
			}

			O_ASSERT(0 == to_process);
		
			return err_ok;
		}

	};
}



namespace outland
{
	void load_file_blocking(array<byte_t>& data, char8_t const* path)
	{
		error_t err;
		data.clear();

		os::file::queue_id queue;
		err = os::file::queue_create(queue);
		O_ASSERT(err_ok == err);

		os::file::operation_id op;
		err = os::file::operation_create(op);
		O_ASSERT(err_ok == err);

		os::file_id file;
		err = os::file::open(file, queue, os::file::open_t::existing, os::file::access_read, path);
		O_ASSERT(err_ok == err);

		os::file::request_t req;
		req.file = file;
		req.user = nullptr;
		req.where = 0;
		err = os::file::size(file, req.size);
		O_ASSERT(err_ok == err);
		err = data.resize(req.size);
		O_ASSERT(err_ok == err);
		req.buffer = data.data();

		os::file::operation_set_request(op, req);
		err = os::file::read(op);
		O_ASSERT(err_ok == err);

		size_t results_size = 0;
		os::file::result_t results_array[1];

		while (0 == results_size)
		{
			os::file::queue_process(queue, results_array, 1, results_size);
			os::thread::sleep(1);
		}

		err = results_array[0].error;
		O_ASSERT(err_ok == err);

		os::file::close(file);
		os::file::operation_destroy(op);
		os::file::queue_destroy(queue);
	}

	
	void table_dump(void* ctx, char8_t const* str, size_t len, size_t reuse)
	{
		char8_t buffer[1024];
		snprintf(buffer, sizeof(buffer), "table entry: %s, %u\n", str, reuse);
		os::debug_string(buffer);

		*(size_t*)ctx += len;
	}

	namespace parser
	{
		syntax_t* do_test(lexer::string_table_t* string_table, parser::symbol_table_t* symbol_table, lexer::token_t const* token_array, size_t token_size, allocator_t* allocator, scratch_allocator_t* syntax_scratch);
	}

	uint64_t sc_buffer[16];

	uint8_t main(main_args_t const& main_args)
	{
		error_t err;
	
		scratch_allocator_t* scratch;
		{
			scratch_allocator::create_t cr;
			cr.init_buffer = nullptr;
			cr.init_size = 0;
			cr.page_size = 1024 * 1024 * 64;
			cr.page_source = main_args.allocator;
			scratch_allocator::create(scratch, cr, sc_buffer);
		}	

		lexer::string_table_t* string_table;
		{
			memory_info_t info;
			lexer::string_table::memory_info(info);
			lexer::string_table::create_t cr;
			cr.allocator = scratch/*main_args.allocator*/;

			lexer::string_table::create(string_table, cr, main_args.allocator->alloc(info.size, info.align));
		}

		lexer::lexer_t* lexer;
		{
			memory_info_t info;
			lexer::memory_info(info);
			lexer::create_t cr;
			cr.table = string_table;
			lexer::create(lexer, cr, main_args.allocator->alloc(info.size, info.align));
		}

		parser::symbol_table_t symbol_table = { scratch/*main_args.allocator*/ };
		err = parser::tables_init(string_table, &symbol_table);
		O_ASSERT(err_ok == err);


		static lexer::token_t tokens[1024 * 4];
		size_t token_size = 0;
			array<byte_t> data = { main_args.allocator };
			//load_file_blocking(data, "E:/test_lang.txt");
			
			load_file_blocking(data, "E:/Projects/test_lang.txt");
			
			//load_file_blocking(data, "E:/Projects/Outland/Parser/src/main/main.cpp");
			
			
		{
			size_t parsed;
			size_t lexed;
			err = lexer::parse_packet(lexer, (char8_t*)data.data(), data.size(), parsed, tokens, u::array_size(tokens), lexed);
			O_ASSERT(err_ok == err);
			token_size += lexed;
			err = lexer::parse_finish(lexer, tokens + lexed, u::array_size(tokens) - lexed, lexed);
			O_ASSERT(err_ok == err);
			token_size += lexed;

			size_t sum = 0;
			lexer::string_table::dump(string_table, &sum, &table_dump);
		}
		
		os::time_t t_beg = os::time::now_high(os::time::nano_second);

		auto syntax = parser::do_test(string_table, &symbol_table, tokens, token_size, scratch/*main_args.allocator*/, scratch);

		os::time_t t_end = os::time::now_high(os::time::nano_second);
		

		os::time_t t_sum = t_end - t_beg;
		char8_t buffer[128];
		snprintf(buffer, sizeof(buffer), "time: %d \n", t_sum);
		os::debug_string(buffer);
		

		parser::output_t o;
		o.sym_tbl = &symbol_table;
		o.str_tbl = string_table;
		o.level = 0;
		o.print = &os::debug_string;

		parser::print(&o, syntax);
		
	
		return 0;
	}


}