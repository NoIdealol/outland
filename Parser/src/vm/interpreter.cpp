#include "pch.h"
#include "assembly.h"

namespace outland { namespace vm
{
	struct object_t
	{
		void*	vtable;
	};
	using object_id = object_t*;

	union register_t
	{
		register_t*		reg;
		object_id		obj;
		byte_t const*	dta;
		int64_t			i64;
		float			flt;
		double			dbl;
		//uint64_t		uint64;
	};

	struct call_t
	{
		byte_t const*	rip;
		register_t*		rbp;
		byte_t*			rdp;
		call_t*			rcp;
	};

	struct context_t
	{
		register_t*		rsp;
		register_t*		rbp;
		byte_t*			rdp;
		call_t*			rcp;
		register_t		rax;
		register_t		stack[1024 - 5 * sizeof(void*)];
	};



	namespace instr
	{
		void add_i64(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			rax.i64 = rsp->i64 + rax.i64;
			++rsp;
			++rip;
		}

		void sub_i64(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			rax.i64 = rsp->i64 - rax.i64;
			++rsp;
			++rip;
		}

		void mul_i64(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			rax.i64 = rsp->i64 * rax.i64;
			++rsp;
			++rip;
		}

		void div_i64(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			rax.i64 = rsp->i64 / rax.i64;
			++rsp;
			++rip;
		}

		void cmp_i64(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			rax.i64 = rsp->i64 == rax.i64;
			++rsp;
			++rip;
		}

		void push8_i64(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			--rsp;
			*rsp = rax;
			rax.i64 = (int8_t)rip[1];
			rip += 2;
		}

		void commit(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			rsp[-1] = rax;
			++rip;
		}

		void copy(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			--rsp;
			*rsp = rax;
			++rip;
		}

		void copy_reg(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			--rsp;
			*rsp = rax;
			rax = rbp[(int8_t)rip[1]];
			rip += 2;
		}

		void set_reg(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			rbp[(int8_t)rip[1]] = rax;
			rax = *rsp;
			++rsp;
			rip += 2;
		}

		/*void load8_reg(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t const*& rdp, register_t& rax)
		{
			--rsp;
			*rsp = rax;
			rax = ((register_t*)(rbp[(int8_t)rip[1]].obj + 1))[rip[2]];
			rip += 3;
		}

		void store8_reg(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t const*& rdp, register_t& rax)
		{
			((register_t*)(rbp[(int8_t)rip[1]].obj + 1))[rip[2]] = rax;
			rax = *rsp;
			++rsp;
			rip += 3;
		}*/

		void load8(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			//--rsp;
			//*rsp = rax;
			rax = ((register_t*)(rax.obj + 1))[rip[1]];
			rip += 2;
		}

		void store8(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			((register_t*)(rsp->obj + 1))[rip[1]] = rax;
			rax = rsp[1];
			rsp += 2;
			rip += 2;
		}

		/*void access8(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t const*& rdp, register_t& rax)
		{
			rax = ((register_t*)(rax.obj + 1))[rip[1]];
			rip += 2;
		}*/


		void call_scr(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			auto* addr = (int32_t const*)u::mem_align(rip + 1, 4);
			auto* call = (call_t*)u::mem_align(rdp, alignof(call_t));
			call->rdp = rdp;
			call->rcp = rcp;
			call->rip = (byte_t const*)(addr + 1);
			call->rbp = rbp;

			rbp = rsp - 1; //rbp is positive into params + possible ret ptr
			rsp[-1] = rax; //commit reg
			rcp = call;
			rdp = (byte_t*)(rcp + 1);
			//rip = ctx->prg + (((static_cast<uint32_t>(rip[2])) | (static_cast<uint32_t>(rip[1]) << 8)) << 2);
			rip += *addr;

			
			/*rsp[-1] = rax;
			rsp[-2].dta = rdp;
			rsp[-3].dta = rip + 4;
			rax.reg = rbp;
			rsp[-4] = rax; //commit reg
			rbp = rsp - 1; //rbp is positive into params + possible ret ptr
			rsp -= 3;

			rip = ctx->prg +
				(((static_cast<uint32_t>(rip[3])) |
				(static_cast<uint32_t>(rip[2]) << 8) |
				(static_cast<uint32_t>(rip[1]) << 16)) << 2);*/
		}

		void ret_void(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			//pop param count in rip[1]
			rsp = rbp + rcp->rip[0] + 1;
			rax = rsp[-1]; //restore rax
			rdp = rcp->rdp;
			rip = rcp->rip + 1;
			rbp = rcp->rbp;
			rcp = rcp->rcp;

			/*//pop param count in rip[1]
			rsp = rbp + 1 + rip[1]; 
			rax = rbp[rip[1]]; //restore rax
			rdp = rbp[-1].dta;
			rip = rbp[-2].dta;
			rbp = rbp[-3].reg;*/
		}

		void ret_val(context_t* ctx, byte_t const*& rip, register_t*& rsp, register_t*& rbp, byte_t*& rdp, call_t*& rcp, register_t& rax)
		{
			//pop param count in rip[1]
			rsp = rbp + rcp->rip[0];
			//rax contains return value
			rdp = rcp->rdp;
			rip = rcp->rip + 1;
			rbp = rcp->rbp;
			rcp = rcp->rcp;

			/*//pop param count in rip[1]
			rsp = rbp + rip[1]; 
			//rax contains return value
			rdp = rbp[-1].dta;
			rip = rbp[-2].dta;
			rbp = rbp[-3].reg;*/
		}
	}


#define CASE_INSTRUCTION(NAME) case asm_##NAME: goto _##NAME;
#define GOTO_INSTRUCTION() \
	switch (*rip & 0b1111) \
	{ \
		INSTRUCTION(CASE_INSTRUCTION) \
	}

#define EXEC_INSTRUCTION(NAME) \
_##NAME: \
instr::NAME(ctx, rip, rsp, rbp, rdp, rcp, rax);

	void execute(context_t* ctx, byte_t const* rip)
	{	
		/*register_t* rsp = ctx->rsp;
		register_t* rbp = ctx->rbp;
		byte_t* rdp = ctx->rdp;
		call_t* rcp = ctx->rcp;
		register_t rax = ctx->rax;

		GOTO_INSTRUCTION();

		EXEC_INSTRUCTION(add_i64); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(sub_i64); GOTO_INSTRUCTION()
		EXEC_INSTRUCTION(mul_i64); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(div_i64); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(cmp_i64); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(push8_i64); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(commit); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(copy); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(copy_reg); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(set_reg); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(load8); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(store8); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(call_scr); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(ret_void); GOTO_INSTRUCTION();
		EXEC_INSTRUCTION(ret_val); GOTO_INSTRUCTION();

		_exit:
		{
			O_ASSERT(rbp == ctx->rbp);
			ctx->rsp = rsp;
			ctx->rdp = rdp;
			ctx->rax = rax;
			return;
		}*/

	}

	void push_args(context_t* ctx, register_t const* arg_data, size_t arg_size)
	{
		ctx->rsp[-1] = ctx->rax;
		ctx->rsp -= arg_size;
		ctx->rax = arg_data[0];
		++arg_data;
		--arg_size;
		for (size_t i = 0; i < arg_size; ++i)
		{
			ctx->rsp[i] = arg_data[i];
		}
	}

	register_t pop_ret(context_t* ctx)
	{
		register_t rax = ctx->rax;
		ctx->rax = ctx->rsp[0];
		--(ctx->rsp);

		return rax;
	}

} }

outland::int32_t teeest(outland::int32_t par1, outland::int32_t par2)
{
	return 0;

#if 0
	alignas(4)
	outland::byte_t const prg[] =
	{
		0, 0, 0,
		outland::vm::asm_call_scr, 11, 0, 0, 0, 2,
		outland::vm::asm_exit,
		//outland::vm::asm_copy_reg, 0,
		//outland::vm::asm_copy_reg, 1,
		outland::vm::asm_mul_i64,
		outland::vm::asm_ret_val,
		outland::vm::asm_NOP, //nop
		outland::vm::asm_NOP, //nop
		//outland::vm::asm_copy_reg, 0,
		outland::vm::asm_copy_reg, 1,
		outland::vm::asm_load8, 1,
		outland::vm::asm_add_i64,
		outland::vm::asm_push8_i64, 110,
		outland::vm::asm_call_scr, outland::vm::asm_NOP, outland::vm::asm_NOP, -11, -1, -1, -1, 2,
		outland::vm::asm_ret_val,
	};

	outland::vm::context_t ctx;
	outland::u::mem_clr(&ctx, sizeof(ctx));
	ctx.rbp = nullptr;
	ctx.rsp = ctx.stack + outland::u::array_size(ctx.stack);
	ctx.rdp = (outland::byte_t*)ctx.stack; //not needed now
	ctx.rcp = nullptr;

	
	/*outland::byte_t const thunk[] =
	{
		outland::vm::asm_call_scr, 0, 1, 2,
		outland::vm::asm_exit
	};*/

	struct my_obj_t : outland::vm::object_t
	{
		outland::vm::register_t	m0;
		outland::vm::register_t	m1;
	};

	my_obj_t obj;
	obj.m1.i64 = par1;

	outland::vm::register_t args[2];
	args[0].i64 = par2;
	args[1].obj = &obj;
	outland::vm::push_args(&ctx, args, outland::u::array_size(args));
	outland::vm::execute(&ctx, prg + 3);
	outland::vm::register_t ret = outland::vm::pop_ret(&ctx);
	return ret.i64;
#endif
}