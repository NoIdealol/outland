#pragma once

namespace outland { namespace vm
{

#define INSTRUCTION(NAME) \
NAME(noop) \
NAME(add_i64) \
NAME(sub_i64) \
NAME(mul_i64) \
NAME(div_i64) \
NAME(and_i64) \
NAME(or_i64) \
NAME(xor_i64) \
NAME(neg_i64) \
NAME(bs_l_i64) \
NAME(bs_r_i64) \
NAME(cmp_eq_i64) \
NAME(cmp_lt_i64) \
NAME(push_i64) \
NAME(load_i64) \
NAME(store_i64) \
NAME(lea_rbp) \
NAME(lea_rip) \
NAME(copy) \
NAME(scr_call) \
NAME(nat_call) \
NAME(ret_void) \
NAME(ret_reg) \
NAME(exit) \


#define ENUM_INSTRUCTION(NAME) asm_##NAME,
	enum : byte_t
	{
		INSTRUCTION(ENUM_INSTRUCTION)
		ENUM_INSTRUCTION(NOP = 0xFF)
	};

} }
