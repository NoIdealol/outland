#include "pch.h"
#include "assembly.h"

#include "math\integer.h"

namespace outland { namespace vm
{
	static void print_add_i64(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_sub_i64(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_mul_i64(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_div_i64(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_cmp_i64(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_push8_i64(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_commit(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_copy(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_copy_reg(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_set_reg(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_load8(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_store8(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_call_scr(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_ret_void(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_ret_val(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_exit(byte_t const*& prg, char8_t* out)
	{
		*out = '\0';
	}

	static void print_asm(byte_t const* prg, char8_t* out)
	{
		
#define CASE_INSTRUCTION(NAME) case asm_##NAME: u::str_copy(out, #NAME); print_##NAME(prg, out + u::str_len(#NAME)); break;
		switch (*prg)
		{
			//INSTRUCTION(CASE_INSTRUCTION)
			//CASE_INSTRUCTION(NOP = 0xFF)
		default:
			break;
		}
	}


#if 0
	using namespace lang;
	using namespace parser;

	struct link_t
	{
		size_t						offset;
		symbol_table::symbol_id		symbol;
	};

	struct type_bank_t
	{
		syntax::type_name_t	tn_void;
		syntax::type_name_t	tn_int64;
	};

	class ctx_t
	{
	public:
		array<byte_t>					_code;
		array<link_t>					_link;
		symbol_table_t*					_symbol;
		deque<syntax::func_t*>			_deferred;
		type_bank_t						_type_bank;

		ctx_t(symbol_table_t* symbol_table, allocator_t* allocator)
			: _code(allocator)
			, _link(allocator)
			, _deferred(allocator)
			, _symbol(symbol_table)
		{
			_type_bank.tn_void.vtable = syntax::id_type_name;
			_type_bank.tn_void.symbol = symbol_void;

			_type_bank.tn_int64.vtable = syntax::id_type_name;
			_type_bank.tn_int64.symbol = symbol_int64;
		}
	};

	namespace expression_compiler
	{
		using flags_t = uint32_t;
		enum flag_t : flags_t
		{
			flag_lvalue		= 0x1, //locator value
			flag_rvalue		= 0x2, //return value
			//flag_const		= 0x4, //only with lvalue, constant/literal
			flag_ptr		= 0x8, //only with rvalue; rvalues must colapse pointer syntax to this flag
		};

		struct type_t
		{
			flags_t				flags;
			syntax::syntax_t*	type;
		};

		error_t	compile(ctx_t* ctx, syntax::syntax_t* s, type_t& t);
		error_t	_compile(ctx_t* ctx, syntax::syntax_t* s, type_t& t);
		error_t	_compile(ctx_t* ctx, syntax::op_add_t* s, type_t& t);
		error_t	_compile(ctx_t* ctx, syntax::func_t* s, type_t& t);
		error_t	_compile(ctx_t* ctx, syntax::value_name_t* s, type_t& t);
		error_t	_compile(ctx_t* ctx, syntax::value_braces_t* s, type_t& t);
		error_t	_compile(ctx_t* ctx, syntax::literal_integer_t* s, type_t& t);

		static integral_t integral_number(syntax::syntax_t* type)
		{
			switch (type->vtable)
			{
			case syntax::id_type_name:
			{
				auto* tn = (syntax::type_name_t*)type;
				switch (tn->symbol)
				{
				case symbol_int8: return integral_int8;
				case symbol_int16: return integral_int16;
				case symbol_int32: return integral_int32;
				case symbol_int64: return integral_int64;
				case symbol_fixed32: return integral_fixed32;
				case symbol_float32: return integral_float32;
				case symbol_float64: return integral_float64;
				default:
					return integral_void;
				}
			}
			default:
				return integral_void;
			}
		}

		static error_t to_number_rvalue(ctx_t* ctx, integral_t integral)
		{
			switch (integral)
			{
			case integral_int8: return err_not_implemented;
			case integral_int16: return err_not_implemented;
			case integral_int32: return err_not_implemented;
			case integral_int64: return ctx->_code.push_back(vm::asm_load_i64);
			case integral_fixed32: return err_not_implemented;
			case integral_float32: return err_not_implemented;
			case integral_float64: return err_not_implemented;
			default:
				O_ASSERT(false);
				return err_unknown;
			}
		}

		static error_t emit_link(ctx_t* ctx, symbol_table::symbol_id symbol)
		{
			error_t err;
			size_t const padding = m::align(ctx->_code.size(), sizeof(int32_t)) - ctx->_code.size();
			err = ctx->_code.push_back_array(padding, vm::asm_noop);
			if (err)
				return err;

			err = ctx->_code.push_back_array(sizeof(int32_t), 0);
			if (err)
				return err;

			link_t link;
			link.offset = ctx->_code.size();
			link.symbol = symbol;

			err = ctx->_link.push_back(link);
			if (err)
				return err;

			return err_ok;
		}

		static error_t emit_uint16(ctx_t* ctx, uint16_t value)
		{
			error_t err;
			size_t const padding = m::align(ctx->_code.size(), sizeof(uint16_t)) - ctx->_code.size();
			err = ctx->_code.push_back_array(padding, vm::asm_noop);
			if (err)
				return err;

			err = ctx->_code.push_back_array(sizeof(uint16_t), 0);
			if (err)
				return err;

			u::mem_copy(ctx->_code.end() - sizeof(uint16_t), &value, sizeof(uint16_t));

			return err_ok;
		}

		static error_t emit_int64(ctx_t* ctx, int64_t value)
		{
			error_t err;
			size_t const padding = m::align(ctx->_code.size(), sizeof(int64_t)) - ctx->_code.size();
			err = ctx->_code.push_back_array(padding, vm::asm_noop);
			if (err)
				return err;

			err = ctx->_code.push_back_array(sizeof(int64_t), 0);
			if (err)
				return err;

			u::mem_copy(ctx->_code.end() - sizeof(int64_t), &value, sizeof(int64_t));

			return err_ok;
		}

		error_t	compile(ctx_t* ctx, syntax::syntax_t* s, type_t& t)
		{
#define COMP_EXPR_OPERATOR(NAME, DUMMY) case syntax::id_##NAME: return _compile(ctx, (syntax::NAME##_t*)s, t);
#define COMP_EXPR_VALUE(NAME) case syntax::id_##NAME: return _compile(ctx, (syntax::NAME##_t*)s, t);

			O_ASSERT(s);
			switch (s->vtable)
			{
				LANG_OPERATOR_BINARY(COMP_EXPR_OPERATOR)
					LANG_OPERATOR_PREFIX(COMP_EXPR_OPERATOR)
					LANG_OPERATOR_POSTFIX(COMP_EXPR_OPERATOR)
					COMP_EXPR_VALUE(func) //lambda
					COMP_EXPR_VALUE(value_name)
					COMP_EXPR_VALUE(value_braces)
					COMP_EXPR_VALUE(literal_integer)
					COMP_EXPR_VALUE(literal_string)
			default:
				return _compile(ctx, s, t);
			}

#undef COMP_EXPR_OPERATOR
#undef COMP_EXPR_VALUE
		}

		error_t	_compile(ctx_t* ctx, syntax::syntax_t* s, type_t& t)
		{
			O_ASSERT(false);
			return err_bad_data;
		}

		error_t	_compile(ctx_t* ctx, syntax::op_add_t* s, type_t& t)
		{
			error_t err;

			type_t t1;
			err = compile(ctx, s->left_side, t1);
			if (err)
				return err;

			integral_t i1 = integral_number(t1.type);
			if (!i1)
			{
				return err_bad_data;
			}

			if (flag_lvalue & t1.flags)
			{
				err = to_number_rvalue(ctx, i1);
				if (err)
					return err;
			}

			type_t t2;
			err = compile(ctx, s->right_side, t2);
			if (err)
				return err;

			integral_t i2 = integral_number(t2.type);
			if (!i2)
			{
				return err_bad_data;
			}

			if (flag_lvalue & t2.flags)
			{
				err = to_number_rvalue(ctx, i2);
				if (err)
					return err;
			}

			if (i1 != i2)
			{
				return err_bad_data;
			}

			t.type = t1.type;
			t.flags = flag_rvalue;

			switch (i1)
			{
			case integral_int8: return err_not_implemented;
			case integral_int16: return err_not_implemented;
			case integral_int32: return err_not_implemented;
			case integral_int64: return ctx->_code.push_back(vm::asm_add_i64);
			case integral_fixed32: return err_not_implemented;
			case integral_float32: return err_not_implemented;
			case integral_float64: return err_not_implemented;
			default:
				O_ASSERT(false);
				return err_unknown;
			}
		}

		error_t	_compile(ctx_t* ctx, syntax::func_t* s, type_t& t)
		{
			error_t err;

			symbol_table::set_data(ctx->_symbol, s->symbol, s->type);

			err = ctx->_code.push_back(vm::asm_lea_rip);
			if (err)
				return err;

			err = emit_link(ctx, s->symbol);
			if (err)
				return err;

			err = ctx->_deferred.push_back(s);
			if (err)
				return err;
			
			t.type = s->type;
			t.flags = flag_lvalue;// | flag_const;

			return err_ok;
		}

		error_t	_compile(ctx_t* ctx, syntax::value_name_t* s, type_t& t)
		{
			error_t err;

			auto space = symbol_table::get_space(ctx->_symbol, s->symbol);
			if (space && !symbol_table::get_name(ctx->_symbol, space))
			{
				//local var in fnc
				auto addr = (uint16_t)symbol_table::get_addr(ctx->_symbol, s->symbol);

				err = ctx->_code.push_back(vm::asm_lea_rbp);
				if (err)
					return err;

				err = emit_uint16(ctx, addr);
				if (err)
					return err;
			}
			else
			{
				//global symbol
				err = ctx->_code.push_back(vm::asm_lea_rip);
				if (err)
					return err;

				err = emit_link(ctx, s->symbol);
				if (err)
					return err;
			}
			//todo: ensure type data is there
			t.type = (syntax::syntax_t*)symbol_table::get_data(ctx->_symbol, s->symbol);
			t.flags = flag_lvalue;
			O_ASSERT(t.type);
			/*if (syntax::id_type_func == t.type->vtable)
				t.flags |= flag_const;*/

			return err_ok;
		}

		error_t	_compile(ctx_t* ctx, syntax::value_braces_t* s, type_t& t)
		{
			return compile(ctx, s->expression, t);
		}

		error_t	_compile(ctx_t* ctx, syntax::literal_integer_t* s, type_t& t)
		{
			error_t err;

			err = ctx->_code.push_back(vm::asm_push_i64);
			if (err)
				return err;

			err = emit_int64(ctx, s->value); //todo: value is unsigned .. numerical parsing
			if (err)
				return err;

			t.type = &ctx->_type_bank.tn_int64;
			t.flags = flag_rvalue;
			O_ASSERT(t.type);

			return err_ok;
		}
	}

	enum ctrlp_t
	{
		ctrlp_error, //keep this first
		ctrlp_closed,
		ctrlp_open,
	};

	class compilation_unit_t
	{
		struct link_t
		{
			size_t						offset;
			symbol_table::symbol_id		symbol;
		};

	private:
		array<byte_t>					_code;
		array<link_t>					_link;
		symbol_table_t*					_symbol;
		deque<syntax::func_t*>			_deferred;
		uint8_t							_reg_size;

	public:
		compilation_unit_t(allocator_t* allocator)
			: _code(allocator)
			, _link(allocator)
			, _deferred(allocator)
		{
		}

		~compilation_unit_t()
		{
		}

		bool	type_is_void(syntax::syntax_t* s);
		bool	type_is_same(syntax::syntax_t* s1, syntax::syntax_t* s2);
		bool	type_is_func(syntax::syntax_t* s);
		bool	type_is_uint8(syntax::syntax_t* s);

		bool compile_function(syntax::func_t* s);
		//void compile_lambda(syntax::func_t* s);
		ctrlp_t compile_function_statement(syntax::syntax_t* s, syntax::type_func_t* t);
		ctrlp_t compile_function_body(syntax::func_body_t* s, syntax::type_func_t* t);
		ctrlp_t compile_function_func(syntax::func_t* s, syntax::type_func_t* t);
		ctrlp_t compile_function_variable(syntax::variable_t* s, syntax::type_func_t* t);
		ctrlp_t compile_function_if_then(syntax::if_then_t* s, syntax::type_func_t* t);
		ctrlp_t compile_function_if_then_else(syntax::if_then_else_t* s, syntax::type_func_t* t);

		syntax::syntax_t*	compile_expression(syntax::syntax_t* s);
		syntax::syntax_t*	_compile_expression(syntax::syntax_t* s);
		syntax::syntax_t*	_compile_expression(syntax::op_add_t* s);
		syntax::syntax_t*	_compile_expression(syntax::func_t* s);
		syntax::syntax_t*	_compile_expression(syntax::value_name_t* s);
		syntax::syntax_t*	_compile_expression(syntax::value_braces_t* s);
		syntax::syntax_t*	_compile_expression(syntax::literal_integer_t* s);
	};

	syntax::syntax_t*	compilation_unit_t::compile_expression(syntax::syntax_t* s)
	{
#define COMP_EXPR_OPERATOR(NAME, DUMMY) case syntax::id_##NAME: return _compile_expression((syntax::NAME##_t*)s);
#define COMP_EXPR_VALUE(NAME) case syntax::id_##NAME: return _compile_expression((syntax::NAME##_t*)s);

		O_ASSERT(s);
		switch (s->vtable)
		{
		LANG_OPERATOR_BINARY(COMP_EXPR_OPERATOR)
		LANG_OPERATOR_PREFIX(COMP_EXPR_OPERATOR)
		LANG_OPERATOR_POSTFIX(COMP_EXPR_OPERATOR)
		COMP_EXPR_VALUE(func) //lambda
		COMP_EXPR_VALUE(value_name)
		COMP_EXPR_VALUE(value_braces)
		COMP_EXPR_VALUE(literal_integer)
		COMP_EXPR_VALUE(literal_string)
		default:
			return _compile_expression(s);
		}

#undef COMP_EXPR_OPERATOR
#undef COMP_EXPR_VALUE
	}

	syntax::syntax_t*	compilation_unit_t::_compile_expression(syntax::syntax_t* s)
	{
		O_ASSERT(s);
		return nullptr;
	}

	/*enum integral_num_t : uint8_t
	{
		integral_INVALID,
		integral_int8,
		integral_uint8,
		integral_int16,
		integral_uint16,
		integral_int32,
		integral_uint32,
		integral_int64,
		integral_uint64,
		integral_fixed32,
		integral_float32,
		integral_float64,
	};*/

	static integral_num_t integral_num(syntax::syntax_t* type)
	{
		

		switch (type->vtable)
		{
		case syntax::id_type_name:
		{
			auto* tn = (syntax::type_name_t*)type;
			switch (tn->symbol)
			{
			case symbol_int32: return integral_int32;
			case symbol_int64: return integral_int64;
			case symbol_fixed32: return integral_fixed32;
			case symbol_float32: return integral_float32;
			case symbol_float64: return integral_float64;
			default:
				return integral_INVALID;
			}
		}
		default:
			return integral_INVALID;
		}
	}

	
	syntax::syntax_t* compilation_unit_t::_compile_expression(syntax::op_add_t* s)
	{
		syntax::syntax_t* t2 = compile_expression(s->right_side);
		syntax::syntax_t* t1 = compile_expression(s->left_side);

		if (nullptr == t1 || nullptr == t2)
			return nullptr;

		integral_num_t r2 = integral_num(t2);
		integral_num_t r1 = integral_num(t1);
		if (r2 != r1)
			return nullptr; //todo report error?

		switch (r1)
		{
		case outland::vm::integral_INVALID:
			return nullptr;
		case outland::vm::integral_int8:
			return nullptr;
		case outland::vm::integral_uint8:
			return nullptr;
		case outland::vm::integral_int16:
			return nullptr;
		case outland::vm::integral_uint16:
			return nullptr;
		case outland::vm::integral_int32:
			return nullptr;
		case outland::vm::integral_uint32:
			return nullptr;
		case outland::vm::integral_int64:
			_code.push_back(vm::asm_add_i64); //todo: err
			return t2;
		case outland::vm::integral_uint64:
			return nullptr;
		case outland::vm::integral_fixed32:
			return nullptr;
		case outland::vm::integral_float32:
			return nullptr;
		case outland::vm::integral_float64:
			return nullptr;
		default:
			return nullptr;
		}
	}

	syntax::syntax_t* compilation_unit_t::_compile_expression(syntax::func_t* s)
	{
		switch (compile_function_func(s, nullptr))
		{
		case ctrlp_error:
			return nullptr;
		case ctrlp_closed:
			O_ASSERT(false);
			return nullptr;
		case ctrlp_open:
			break;
		default:
			O_ASSERT(false);
			return nullptr;
		}

		_code.push_back(vm::asm_lea_rip);
		_code.push_back_array(m::align(_code.size(), 4) - _code.size(), 0); //garbage memory
		
		link_t link;
		link.symbol = s->symbol;
		link.offset = _code.size();
		_link.push_back(link);

		_code.push_back_array(4, 0); //garbage memory

		return s->type;
	}

	syntax::syntax_t*	compilation_unit_t::_compile_expression(syntax::value_name_t* s)
	{
		auto* type = (syntax::syntax_t*)symbol_table::get_data(_symbol, s->symbol);
		if (type_is_func(type))
		{
			_code.push_back(vm::asm_lea_rip);
			_code.push_back_array(m::align(_code.size(), 4) - _code.size(), 0); //garbage memory

			link_t link;
			link.symbol = s->symbol;
			link.offset = _code.size();
			_link.push_back(link);

			_code.push_back_array(4, 0); //garbage memory

		}
		else
		{
			symbol_table::symbol_id space = symbol_table::get_space(_symbol, s->symbol);
			if (!space || symbol_table::get_name(_symbol, space))
			{
				//global
				_code.push_back(vm::asm_lea_rip);
				_code.push_back_array(m::align(_code.size(), 4) - _code.size(), 0); //garbage memory

				link_t link;
				link.symbol = s->symbol;
				link.offset = _code.size();
				_link.push_back(link);

				_code.push_back_array(4, 0); //garbage memory

			}
			else
			{
				//local
				auto addr = symbol_table::get_addr(_symbol, s->symbol);
				_code.push_back(vm::asm_lea_rbp8);
				_code.push_back((int8_t)addr);
			}
		}

		return type;
	}

	syntax::syntax_t*	compilation_unit_t::_compile_expression(syntax::value_braces_t* s)
	{

	}

	syntax::syntax_t*	compilation_unit_t::_compile_expression(syntax::literal_integer_t* s)
	{

	}

	bool compilation_unit_t::compile_function(syntax::func_t* s)
	{
		//error_t err;

		/*link_t link;
		link.offset = _code.size();
		link.symbol = s->symbol;
		err = _link.push_back(link);*/

		//store func type
		symbol_table::set_data(_symbol, s->symbol, s->type);

		//store register index and type
		for (size_t i = 0; i < s->type->args.size; ++i)
		{
			auto* arg = (syntax::func_arg_t*)s->type->args.data[i];
			if (type_is_void(s->type))
			{
				return false;
			}

			if (type_is_func(s->type))
			{
				return false;
			}

			if (arg->symbol) //may be unnamed or empty body
			{
				symbol_table::set_addr(_symbol, arg->symbol, (int32_t)(i * sizeof(void*)));
				symbol_table::set_data(_symbol, arg->symbol, arg->type);
			}
		}

		switch (compile_function_body(s->body, s->type))
		{
		case ctrlp_error:
			return false;
		case ctrlp_closed:
			return true;
		case ctrlp_open:
			return false;
		default:
			O_ASSERT(false);
			return false;
		}
	}

	ctrlp_t compilation_unit_t::compile_function_statement(syntax::syntax_t* s, syntax::type_func_t* t)
	{
		switch (s->vtable)
		{
		//Define value
		case syntax::id_variable:
			return compile_function_variable((syntax::variable_t*)s, t);
		case syntax::id_func: //or lambda, skip lambda, as it is an unused expression
		{
			auto* f = (syntax::func_t*)s;
			if (symbol_table::get_name(_symbol, f->symbol))
				return compile_function_func(f, t);
			else
				return ctrlp_open; //lambda, skip, cannot be referenced
		}
		//Scope
		case syntax::id_func_body:
		{
			uint8_t const reg_frame = _reg_size;
			switch (compile_function_body((syntax::func_body_t*)s, t))
			{
			case ctrlp_error:
				return ctrlp_error;
			case ctrlp_closed:
				return ctrlp_closed;
			case ctrlp_open:
				if (reg_frame != _reg_size)
				{
					//todo pop reg, err handling
					_code.push_back(vm::asm_pop8);
					_code.push_back(_reg_size - reg_frame);
					_reg_size = reg_frame;
				}
				return ctrlp_open;
			default:
				O_ASSERT(false);
				return ctrlp_error;
			}
		}
		//Branching
		case syntax::id_if_then:
			return compile_function_if_then((syntax::if_then_t*)s, t);
		case syntax::id_if_then_else:
			return compile_function_if_then_else((syntax::if_then_else_t*)s, t);
		//Expression statement
		//Expression value
		//case syntax::id_func: lambda already handled in Define value
		case syntax::id_value_name:
		case syntax::id_literal_integer:
		case syntax::id_literal_string:
			return ctrlp_open;; //skip, unused values
		//Expression braces
		case syntax::id_value_braces:
			return compile_function_statement(((syntax::value_braces_t*)s)->expression, t);
		//Expression operator
#define COMP_EXPR_OPERATOR(NAME, DUMMY) case syntax::id_##NAME:
		LANG_OPERATOR_BINARY(COMP_EXPR_OPERATOR)
		LANG_OPERATOR_PREFIX(COMP_EXPR_OPERATOR)
		LANG_OPERATOR_POSTFIX(COMP_EXPR_OPERATOR)
		{
			syntax::syntax_t* rt = compile_expression(s);
			if (!rt)
			{
				//todo err report
				return ctrlp_error;
			}
			if (!type_is_void(rt))
			{
				//todo pop reg, err handling
				_code.push_back(vm::asm_pop);
				_code.push_back(1);
			}

			return ctrlp_open;
		}
#undef COMP_EXPR_OPERATOR

		default:
			O_ASSERT(false);
			return ctrlp_error;
		}
	}

	ctrlp_t compilation_unit_t::compile_function_body(syntax::func_body_t* s, syntax::type_func_t* t)
	{
		ctrlp_t ctrlp = ctrlp_open;
		for (size_t i = 0; i < s->statements.size; ++i)
		{
			auto* st = s->statements.data[i];
			ctrlp = compile_function_statement(st, t);
			switch (ctrlp)
			{
			case ctrlp_error:
				return ctrlp_error;
			default:
				break;
			}
		}

		return ctrlp;
	}

	ctrlp_t compilation_unit_t::compile_function_func(syntax::func_t* s, syntax::type_func_t* t)
	{
		//store func type
		symbol_table::set_data(_symbol, s->symbol, s->type);
		_deferred.push_back(s);

		return ctrlp_open;
	}

	ctrlp_t compilation_unit_t::compile_function_variable(syntax::variable_t* s, syntax::type_func_t* t)
	{
		if (type_is_void(s->type))
		{
			return ctrlp_error;
		}
		
		if (type_is_func(s->type))
		{
			return ctrlp_error;
		}

		++_reg_size;

		symbol_table::set_data(_symbol, s->symbol, s->type);
		symbol_table::set_addr(_symbol, s->symbol, -(int8_t)_reg_size);

		if (s->value)
		{
			auto* rt = compile_expression(s->value);
			if (!rt)
				return ctrlp_error;

			if (!type_is_same(s->type, rt))
			{
				return ctrlp_error;
			}
		}
		else
		{
			_code.push_back(vm::asm_push8_i64);
			_code.push_back(0); //garbage memory
		}

		_code.push_back(vm::asm_commit);

		return ctrlp_open;
	}

	ctrlp_t compilation_unit_t::compile_function_if_then(syntax::if_then_t* s, syntax::type_func_t* t)
	{
		auto* rt = compile_expression(s->condition);
		if (!type_is_uint8(rt))
		{
			return ctrlp_error;
		}

		size_t rip = _code.size();
		_code.push_back(vm::asm_jmp_not);
		size_t addr = m::align(_code.size(), 2);
		_code.push_back_array(3, 0); //garbage memory

		if(!compile_function_statement(s->then_statement, t))
			return ctrlp_error;

		size_t delta = _code.size() - rip;
		if(c::max_int16 < delta)
		{
			return ctrlp_error;
		}

		*(int16_t*)(_code.data() + addr) = (int16_t)(delta);

		return ctrlp_open;
	}

	ctrlp_t compilation_unit_t::compile_function_if_then_else(syntax::if_then_else_t* s, syntax::type_func_t* t)
	{
		auto* rt = compile_expression(s->condition);
		if (!type_is_uint8(rt))
		{
			return ctrlp_error;
		}

		size_t rip1 = _code.size();
		_code.push_back(vm::asm_jmp_if);
		size_t addr1 = m::align(_code.size(), 2);
		_code.push_back_array(3, 0); //garbage memory

		ctrlp_t ctrlp_1 = compile_function_statement(s->else_statement, t);
		if (!ctrlp_1)
			return ctrlp_error;

		size_t rip2 = _code.size();
		_code.push_back(vm::asm_jmp);
		size_t addr2 = m::align(_code.size(), 2);
		_code.push_back_array(3, 0); //garbage memory

		size_t delta1 = _code.size() - rip1;
		if (c::max_int16 < delta1)
		{
			return ctrlp_error;
		}
		*(int16_t*)(_code.data() + addr1) = (int16_t)(delta1);

		ctrlp_t ctrlp_2 = compile_function_statement(s->then_statement, t);
		if (!ctrlp_2)
			return ctrlp_error;

		size_t delta2 = _code.size() - rip2;
		if (c::max_int16 < delta2)
		{
			return ctrlp_error;
		}
		*(int16_t*)(_code.data() + addr2) = (int16_t)(delta2);

		if (ctrlp_closed == ctrlp_1 && ctrlp_closed == ctrlp_2)
			return ctrlp_closed;

		return ctrlp_open;
	}


#endif
} }
