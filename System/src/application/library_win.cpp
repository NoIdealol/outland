#include "pch.h"
#include "library_win.h"

#define O_LIBRARY_HANDLE_NULL NULL

namespace outland { namespace  os
{

	//--------------------------------------------------
	error_t library::load(library_id& lib, const char8_t* name)
	{
		stringw_t wstr = { app::allocator };
		error_t err = utf8_to_wchar(name, wstr);
		if (err)
			return err;

		path_to_system(wstr.data());

		HMODULE module = LoadLibraryW(wstr.data());
		if (O_LIBRARY_HANDLE_NULL == module)
		{
			auto err = GetLastError();
			switch (err)
			{
			case ERROR_FILE_NOT_FOUND:
			case ERROR_PATH_NOT_FOUND:
				return err_not_found;
			case ERROR_TOO_MANY_OPEN_FILES:
				return err_out_of_handles;
			default:
				return err_unknown;
			}
		}

		lib = reinterpret_cast<library_id>(module);

		return err_ok;
	}

	//--------------------------------------------------
	error_t library::free(library_id lib)
	{
		BOOL result = FreeLibrary(reinterpret_cast<HMODULE>(lib));
		if (FALSE == result)
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	library::function_t	library::find(library_id lib, const char8_t* name)
	{
		FARPROC proc = GetProcAddress(reinterpret_cast<HMODULE>(lib), name);
		return reinterpret_cast<function_t>(proc);
	}

	//--------------------------------------------------
	HMODULE get_native_handle(library_id library)
	{
		return reinterpret_cast<HMODULE>(library);
	}

} }