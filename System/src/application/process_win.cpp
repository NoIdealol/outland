#include "pch.h"
#include "process_win.h"

namespace outland { namespace  os
{
	//--------------------------------------------------
	process::id_t	process::id()
	{
		return GetCurrentProcessId();
	}

	//--------------------------------------------------
	process::id_t	process::id(process_id process)
	{
		return GetProcessId(get_native_handle(process));
	}

	//--------------------------------------------------
	error_t	process::create(process_id& process, const char8_t* name, const char8_t* cmd_line)
	{
		STARTUPINFOW si;
		PROCESS_INFORMATION pi;
		error_t err;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));

		stringw_t wcmd = { app::allocator };
		err = utf8_to_wchar(cmd_line, wcmd);
		if (err)
			return err;

		stringw_t wstr = { app::allocator };
		err = utf8_to_wchar(name, wstr);
		if (err)
			return err;

		path_to_system(wstr.data());

		// Start the child process_t. 
		if (!CreateProcessW(
			wstr.data(),	// No module name (use command line)
			wcmd.data(),	// Command line
			NULL,           // Process handle not inheritable
			NULL,           // Thread handle not inheritable
			FALSE,          // Set handle inheritance to FALSE
			0,              // No creation flags
			NULL,           // Use parent's environment block
			NULL,           // Use parent's starting directory 
			&si,            // Pointer to STARTUPINFO structure
			&pi)            // Pointer to PROCESS_INFORMATION structure
			)
		{
			auto err = GetLastError();
			switch (err)
			{
			case ERROR_FILE_NOT_FOUND:
			case ERROR_PATH_NOT_FOUND:
				return err_not_found;
			case ERROR_TOO_MANY_OPEN_FILES:
				return err_out_of_handles;
			default:
				return err_unknown;
			}
		}

		// Close process_t and thread handles. 
		process = reinterpret_cast<process_id>(pi.hProcess);
		CloseHandle(pi.hThread);

		return err_ok;
	}

	//--------------------------------------------------
	error_t	process::join(process_id process)
	{
		switch (WaitForSingleObject(get_native_handle(process), INFINITE))
		{
		case WAIT_OBJECT_0:
			break;
		case WAIT_TIMEOUT:
		case WAIT_FAILED:
		default:
			return err_unknown;
		}

		if (0 == CloseHandle(get_native_handle(process)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	process::detach(process_id process)
	{
		if (0 == CloseHandle(get_native_handle(process)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	process::terminate(process_id process)
	{
		if (FALSE == TerminateProcess(get_native_handle(process), -1))
			return err_unknown;

		if (0 == CloseHandle(get_native_handle(process)))
			return err_unknown;

		return err_ok;
	}
	
	//--------------------------------------------------
	HANDLE get_native_handle(process_id process)
	{
		return reinterpret_cast<HANDLE>(process);
	}
} }
