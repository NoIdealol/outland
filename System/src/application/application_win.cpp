#include "pch.h"
#include "application_win.h"

namespace outland { namespace  os
{
	namespace app
	{
		namespace g
		{
			static process_id			process;
			static library_id			library;
			static allocator_win_t		allocator;
		}

		//--------------------------------------------------
		allocator_t* const	allocator = &g::allocator;
		process_id const&	process = g::process;
		library_id const&	library = g::library;

		//--------------------------------------------------
		void	init_os(HINSTANCE module)
		{
			g::library = reinterpret_cast<library_id>(module);
			g::process = reinterpret_cast<process_id>(GetCurrentProcess());
		}

		//--------------------------------------------------
		void	clean_os()
		{

		}

		//--------------------------------------------------
		void*	allocator_win_t::alloc(size_t size, size_t align, size_t* capacity)
		{
			void* mem = _aligned_malloc(size, align);
			if (capacity)
				*capacity = size;
			return mem;
		}

		//--------------------------------------------------
		void	allocator_win_t::free(void* memory, size_t size)
		{
			if (!memory)
				return;

			_aligned_free(memory);
		}

		//--------------------------------------------------
		uint8_t run(application_t* application, size_t arg_count, char8_t* cmd_line[])
		{
			uint8_t exit_code = c::max_uint8;
			MSG msg;
			bool run = application->on_init(arg_count, cmd_line);;
			if (!run)
				return exit_code;

			while (run)
			{
				if (application->on_idle())
				{
					BOOL ret;
					while (0 != (ret = GetMessageW(&msg, NULL, 0, 0)))
					{
						if (-1 == ret)
						{
							//error
							run = false;
							break;
						}				

						TranslateMessage(&msg);
						DispatchMessageW(&msg);
					}

					if (WM_QUIT == msg.message)
					{
						exit_code = (uint8_t)msg.wParam;
						run = false;
					}
				}
				else
				{
					while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
					{
						if (WM_QUIT == msg.message)
						{
							exit_code = (uint8_t)msg.wParam;
							run = false;
							break;
						}

						TranslateMessage(&msg);
						DispatchMessageW(&msg);
					}
				}
			}

			exit_code = application->on_exit(exit_code);
			
			return exit_code;
		}
	}

	//--------------------------------------------------
	void	app::exit(uint8_t code)
	{
		PostQuitMessage(code);
	}
		
		
		
	

} }