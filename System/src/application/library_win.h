#pragma once
#include "system\application\library.h"

namespace outland { namespace  os
{

	HMODULE get_native_handle(library_id library);

} }
