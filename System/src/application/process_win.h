#pragma once
#include "system\application\process.h"

namespace outland { namespace  os
{
#define O_PROCESS_HANDLE_NULL NULL

	HANDLE get_native_handle(process_id process);

} }
