#pragma once
#include "system\application\application.h"
#include "process_win.h"
#include "library_win.h"

namespace outland { namespace  os
{
	namespace app
	{
		//--------------------------------------------------
		class allocator_win_t : public allocator_t
		{
		public:
			void*	alloc(size_t size, size_t align, size_t* capacity) final;
			void	free(void* memory, size_t size) final;
		};

		//--------------------------------------------------
		void	init_os(HINSTANCE module);
		void	clean_os();
	}

} }