#include "pch.h"
#include "system\application\time.h"

namespace outland { namespace  os
{
	//--------------------------------------------------
	static LARGE_INTEGER init_high_res_clock(LARGE_INTEGER& ticks_per_sec)
	{
		BOOL result;
		result = QueryPerformanceFrequency(&ticks_per_sec);
		//O_ASSERT(result == TRUE);

		LARGE_INTEGER ticks_at_start;
		ticks_at_start.QuadPart = 0;
		result = QueryPerformanceCounter(&ticks_at_start);
		//O_ASSERT(result == TRUE);

		return ticks_at_start;
	}

	namespace g
	{
		static LARGE_INTEGER	_ticks_per_sec;
		static LARGE_INTEGER	_ticks_at_start;
	}

	//--------------------------------------------------
	void init_time()
	{
		g::_ticks_at_start = init_high_res_clock(g::_ticks_per_sec);
	}

	//--------------------------------------------------
	time_t	time::now_high(resolution_t resolution)
	{
		LARGE_INTEGER current;
		current.QuadPart = 0;

		BOOL result = QueryPerformanceCounter(&current);
			
		//O_ASSERT(TRUE == result);

		time_t div = (current.QuadPart - g::_ticks_at_start.QuadPart) / g::_ticks_per_sec.QuadPart;
		time_t mod = (current.QuadPart - g::_ticks_at_start.QuadPart) % g::_ticks_per_sec.QuadPart;
		return (div * resolution) + (mod * resolution) / g::_ticks_per_sec.QuadPart;
	}

	//--------------------------------------------------
	time_t	time::now_low(resolution_t resolution)
	{
		return (GetTickCount64() * resolution) / milli_second;
	}

	//--------------------------------------------------
	time_t	time::convert(time_t time, resolution_t resolution_from, resolution_t resolution_to)
	{
		time_t div = time / resolution_from;
		time_t mod = time % resolution_from;
		return (div * resolution_to) + (mod * resolution_to) / resolution_from;
	}

} }