#include "pch.h"
#include "system\hardware\processor.h"

namespace outland { namespace  os
{
	
	error_t processor::querry_info(processor_t& processor)
	{
		processor.cache_line_size = 0;
		processor.core_count_logical = 0;
		processor.core_count_physical = 0;

		{
			DWORD buffer_size = 0;

			if (FALSE == GetLogicalProcessorInformation(NULL, &buffer_size))
			{
				if (ERROR_INSUFFICIENT_BUFFER != GetLastError())
					return err_unknown;
			}
			else
			{
				return err_unknown;
			}

			array<byte_t*> byte_arr = { os::app::allocator };
			error_t err = byte_arr.resize(buffer_size);
			if (err)
				return err;

			SYSTEM_LOGICAL_PROCESSOR_INFORMATION* buffer = reinterpret_cast<SYSTEM_LOGICAL_PROCESSOR_INFORMATION*>(byte_arr.data());

			if (FALSE == GetLogicalProcessorInformation(buffer, &buffer_size))
				return err_unknown;

			for (DWORD i = 0; i != buffer_size / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION); ++i)
			{
				if (buffer[i].Relationship == RelationCache && buffer[i].Cache.Level == 1) {
					processor.cache_line_size = (size_t)buffer[i].Cache.LineSize;
					break;
				}
			}
		}

		{
			SYSTEM_INFO sysinfo = { 0 };
			GetSystemInfo(&sysinfo);
			processor.core_count_logical = (size_t)sysinfo.dwNumberOfProcessors;
		}

		return err_ok;
	}
	
} }
