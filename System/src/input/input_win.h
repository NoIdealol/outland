#pragma once
#include "system\input\input.h"
#include "hid_usage.h"
#include "keyboard_win.h"
#include "mouse_win.h"
#include "gamepad_win.h"

namespace outland { namespace  os
{	
	class raw_input_t
	{
	private:
		input_t*				_input;
		array<raw_keyboard_t>	_keyboard_list = { app::allocator };
		array<raw_mouse_t>		_mouse_list = { app::allocator };
		array<raw_gamepad_t>	_gamepad_list = { app::allocator };

		const wchar_t*			_class_name = L"OUTLAND_ENGINE_INPUT";
		HWND					_hwnd = NULL;
		
	public:
		raw_input_t(input_t* input, error_t& err);

		static LRESULT CALLBACK window_callback(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		
		error_t window_register();
		error_t window_create();
		void window_destroy();
		void window_unregister();

		bool devices_register(hid::usage_t usage);
		void devices_unregister(hid::usage_t usage);

		error_t get_hid_caps(hid::device_id device, hid::device_caps_t& caps);
	};
	
} }