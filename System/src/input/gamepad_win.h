#pragma once

#include "system\input\gamepad.h"
#include "system\input\input.h"

//driver system lib
#include <Hidsdi.h>


namespace outland { namespace os
{
	//--------------------------------------------------
	class raw_gamepad_t : public hid::device_t
	{
	private:
		enum : size_t
		{
			max_switch_count = 16, //reserver some more
			max_value_count = 16, //reserver some more
			max_index_count = max_switch_count + max_value_count, //reserve some more

			value_index_stick_left_x = 0,
			value_index_stick_left_y,
			value_index_stick_right_x,
			value_index_stick_right_y,
			value_index_lever_left,
			value_index_lever_right,
			value_index_hat,

			value_index_count,
		};

		enum : hid::index_t
		{
			hid_index_stick_left_x,
			hid_index_stick_left_y,
			hid_index_stick_right_x,
			hid_index_stick_right_y,

			hid_index_lever_left,
			hid_index_lever_right,

			hid_index_hat,
		};

		enum feature_t
		{
			feature_switch,
			feature_lever,
			feature_stick,
			feature_hat,
		};

		struct index_t
		{
			uint32_t	index;
			feature_t	feature;
			uint32_t	bit_count;
		};

	private:
		HANDLE						_device_handle;
		RID_DEVICE_INFO_HID			_device_info;

		PHIDP_PREPARSED_DATA		_hid_data;
		size_t						_hid_size;

		index_t		_hid_index[max_index_count];

		hid::report_t	_hid_state_switch[max_switch_count];
		hid::report_t	_hid_state_value[max_value_count];
		
		array<byte_t*>				_input_buffer = { app::allocator };

	public:
		raw_gamepad_t(HANDLE device, RID_DEVICE_INFO_HID& info);
		raw_gamepad_t(raw_gamepad_t&& other) = default;
		~raw_gamepad_t();

		raw_gamepad_t& operator = (raw_gamepad_t&& other) = default;

		error_t create(input_t* input);
		void destroy(input_t* input);


		void on_input(HRAWINPUT h_raw_input, input_t* input);

		inline HANDLE handle() { return _device_handle; }

		error_t get_hid_caps(hid::device_caps_t& caps);
	};

} }