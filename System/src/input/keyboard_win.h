#pragma once

#include "system\input\keyboard.h"
#include "system\input\input.h"

namespace outland { namespace os
{
	//--------------------------------------------------
	class raw_keyboard_t : public hid::device_t
	{
	public:
		enum : hid::index_t
		{
			hid_index_key_A,
			hid_index_key_B,
			hid_index_key_C,
			hid_index_key_D,
			hid_index_key_E,
			hid_index_key_F,
			hid_index_key_G,
			hid_index_key_H,
			hid_index_key_I,
			hid_index_key_J,
			hid_index_key_K,
			hid_index_key_L,
			hid_index_key_M,
			hid_index_key_N,
			hid_index_key_O,
			hid_index_key_P,
			hid_index_key_Q,
			hid_index_key_R,
			hid_index_key_S,
			hid_index_key_T,
			hid_index_key_U,
			hid_index_key_V,
			hid_index_key_W,
			hid_index_key_X,
			hid_index_key_Y,
			hid_index_key_Z,
			hid_index_key_1,
			hid_index_key_2,
			hid_index_key_3,
			hid_index_key_4,
			hid_index_key_5,
			hid_index_key_6,
			hid_index_key_7,
			hid_index_key_8,
			hid_index_key_9,
			hid_index_key_0,
			hid_index_key_enter,
			hid_index_key_escape,
			hid_index_key_backspace,
			hid_index_key_tab,
			hid_index_key_space,
			hid_index_key_minus,
			hid_index_key_equals,
			hid_index_key_bracket_left,
			hid_index_key_bracket_right,
			hid_index_key_backslash,
			hid_index_key_semicolon,
			hid_index_key_quote,
			hid_index_key_tilde,
			hid_index_key_comma,
			hid_index_key_period,
			hid_index_key_slash,
			hid_index_key_caps_lock,
			hid_index_key_F1,
			hid_index_key_F2,
			hid_index_key_F3,
			hid_index_key_F4,
			hid_index_key_F5,
			hid_index_key_F6,
			hid_index_key_F7,
			hid_index_key_F8,
			hid_index_key_F9,
			hid_index_key_F10,
			hid_index_key_F11,
			hid_index_key_F12,
			hid_index_key_print_screen,
			hid_index_key_scroll_lock,
			hid_index_key_pause,
			hid_index_key_insert,
			hid_index_key_home,
			hid_index_key_page_up,
			hid_index_key_delete,
			hid_index_key_end,
			hid_index_key_page_down,
			hid_index_key_right,
			hid_index_key_left,
			hid_index_key_down,
			hid_index_key_up,
			hid_index_key_num_lock,
			hid_index_key_num_divide,
			hid_index_key_num_multiply,
			hid_index_key_num_minus,
			hid_index_key_num_plus,
			hid_index_key_num_enter,
			hid_index_key_num_1,
			hid_index_key_num_2,
			hid_index_key_num_3,
			hid_index_key_num_4,
			hid_index_key_num_5,
			hid_index_key_num_6,
			hid_index_key_num_7,
			hid_index_key_num_8,
			hid_index_key_num_9,
			hid_index_key_num_0,
			hid_index_key_num_period,
			hid_index_key_backslash_102,
			hid_index_key_F13,
			hid_index_key_F14,
			hid_index_key_F15,
			hid_index_key_F16,
			hid_index_key_F17,
			hid_index_key_F18,
			hid_index_key_F19,
			hid_index_key_F20,
			hid_index_key_F21,
			hid_index_key_F22,
			hid_index_key_F23,
			hid_index_key_F24,
			hid_index_key_help,
			hid_index_key_menu,
			hid_index_key_control_left,
			hid_index_key_shift_left,
			hid_index_key_alt_left,
			hid_index_key_win_left,
			hid_index_key_control_right,
			hid_index_key_shift_right,
			hid_index_key_alt_right,
			hid_index_key_win_right,

			hid_index_count,
		};
	private:
		HANDLE						_device_handle;
		RID_DEVICE_INFO_KEYBOARD	_device_info;
		bool						_is_pause_c1_s1_read;

	public:
		raw_keyboard_t(HANDLE device, RID_DEVICE_INFO_KEYBOARD& info);
		~raw_keyboard_t();

		error_t create(input_t* input);
		void destroy(input_t* input);

		void on_input(HRAWINPUT h_raw_input, input_t* input);

		inline HANDLE handle() { return _device_handle; }

		error_t get_hid_caps(hid::device_caps_t& caps);
	};

	namespace hid { namespace keyboard
	{
		//--------------------------------------------------
		using virtual_key_t = uint8_t;

		//--------------------------------------------------
		hid::usage_t vk_to_key(virtual_key_t vk);
	} }

} }
