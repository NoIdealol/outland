#include "pch.h"
#include "gamepad_win.h"
#include "hid_usage.h"

//driver system lib
#pragma comment(lib, "hid.lib")

namespace outland { namespace os
{
	//--------------------------------------------------
	raw_gamepad_t::raw_gamepad_t(HANDLE device, RID_DEVICE_INFO_HID& info)
		: _device_handle(device)
		, _device_info(info)
		, _hid_data(nullptr)
	{
		device_t::device = static_cast<hid::device_id>(reinterpret_cast<uintptr_t>(device));
		device_t::usage = hid::gamepad::device;
		device_t::appdata = nullptr;
	}

	//--------------------------------------------------
	raw_gamepad_t::~raw_gamepad_t()
	{
	
	}

	//--------------------------------------------------
	error_t raw_gamepad_t::create(input_t* input)
	{
		error_t err = _input_buffer.reserve(128);
		if (err)
			return err;

		BOOL res = FALSE;
		UINT size = 0;
		res = GetRawInputDeviceInfoW(_device_handle, RIDI_PREPARSEDDATA, NULL, &size);
		if (res != 0)
			return err_unknown;
		if (size == 0)
			return err_unknown;

		_hid_size = size;
		_hid_data = reinterpret_cast<PHIDP_PREPARSED_DATA>(app::allocator->alloc(_hid_size, alignof(uintptr_t))); //alloc
		if (nullptr == _hid_data)
			return err_out_of_memory;

		res = GetRawInputDeviceInfoW(_device_handle, RIDI_PREPARSEDDATA, _hid_data, &size);
		if (res == ((UINT)-1) || res == 0)
		{
			app::allocator->free(_hid_data, _hid_size);
			return err_unknown;
		}

		//get caps
		HIDP_CAPS caps;
		res = HIDP_STATUS_SUCCESS == HidP_GetCaps(_hid_data, &caps);
		if (FALSE == res)
		{
			app::allocator->free(_hid_data, _hid_size);
			return err_unknown;
		}

		//get button caps
		USHORT button_caps_count = caps.NumberInputButtonCaps;
		if (0 == button_caps_count || max_switch_count < button_caps_count)
		{
			app::allocator->free(_hid_data, _hid_size);
			return err_unknown;
		}

		HIDP_BUTTON_CAPS button_caps[max_switch_count];
		res = HIDP_STATUS_SUCCESS == HidP_GetButtonCaps(
			HidP_Input,
			button_caps,
			&button_caps_count,
			_hid_data);

		if (FALSE == res)
		{
			app::allocator->free(_hid_data, _hid_size);
			return err_unknown;
		}

		auto button_init = [this](size_t hid_index, uint32_t feature_index)
		{
			_hid_index[hid_index].index = feature_index;
			_hid_index[hid_index].feature = feature_switch;
			_hid_index[hid_index].bit_count = 1;

			//init value
			_hid_state_switch[feature_index].value_new = 0;
			_hid_state_switch[feature_index].value_old = 0;
			_hid_state_switch[feature_index].index = value_index_count + feature_index;
		};

		//parse button caps
		for (size_t i = 0; i < button_caps_count; ++i)
		{
			if ((USHORT)hid::usage_page::button == button_caps[i].UsagePage)
			{
				if (button_caps[i].IsRange)
				{
					const auto& range = button_caps[i].Range;
					for (USAGE usage = range.UsageMin; usage <= range.UsageMax; ++usage)
					{
						if ((USAGE)hid::usage_button::button_1 <= usage && (USAGE)hid::usage_button::button_20 >= usage)
						{
							uint32_t feature_index = usage - (USAGE)hid::usage_button::button_1;
							size_t hid_index = (usage - range.UsageMin) + range.DataIndexMin;

							button_init(hid_index, feature_index);
						}
					}

				}
				else
				{
					USAGE usage = button_caps[i].NotRange.Usage;
					USHORT dataIndex = button_caps[i].NotRange.DataIndex;

					if ((USAGE)hid::usage_button::button_1 <= usage && (USAGE)hid::usage_button::button_20 >= usage)
					{
						uint32_t feature_index = usage - (USAGE)hid::usage_button::button_1;
						size_t hid_index = dataIndex;

						button_init(hid_index, feature_index);
					}
				}
			}
		}

		//get value caps
		USHORT value_caps_count = caps.NumberInputValueCaps;
		if (0 == value_caps_count || max_value_count < value_caps_count)
		{
			app::allocator->free(_hid_data, _hid_size);
			return err_unknown;
		}

		HIDP_VALUE_CAPS value_caps[max_value_count];
		res = HIDP_STATUS_SUCCESS == HidP_GetValueCaps(
			HidP_Input,
			value_caps,
			&value_caps_count,
			_hid_data);

		if (FALSE == res)
		{
			app::allocator->free(_hid_data, _hid_size);
			return err_unknown;
		}

		_hid_state_value[value_index_stick_left_x].value_new = 0;
		_hid_state_value[value_index_stick_left_x].value_old = 0;
		_hid_state_value[value_index_stick_left_x].index = hid_index_stick_left_x;

		_hid_state_value[value_index_stick_left_y].value_new = 0;
		_hid_state_value[value_index_stick_left_y].value_old = 0;
		_hid_state_value[value_index_stick_left_y].index = hid_index_stick_left_y;
		
		_hid_state_value[value_index_stick_right_x].value_new = 0;
		_hid_state_value[value_index_stick_right_x].value_old = 0;
		_hid_state_value[value_index_stick_right_x].index = hid_index_stick_right_x;
		
		_hid_state_value[value_index_stick_right_y].value_new = 0;
		_hid_state_value[value_index_stick_right_y].value_old = 0;
		_hid_state_value[value_index_stick_right_y].index = hid_index_stick_right_y;

		_hid_state_value[value_index_lever_left].value_new = 0;
		_hid_state_value[value_index_lever_left].value_old = 0;
		_hid_state_value[value_index_lever_left].index = hid_index_lever_left;
		
		_hid_state_value[value_index_lever_right].value_new = 0;
		_hid_state_value[value_index_lever_right].value_old = 0;
		_hid_state_value[value_index_lever_right].index = hid_index_lever_right;
		
		_hid_state_value[value_index_hat].value_new = hid::hat_switch::angle_none;
		_hid_state_value[value_index_hat].value_old = hid::hat_switch::angle_none;
		_hid_state_value[value_index_hat].index = hid_index_hat;

		auto value_init = [this](size_t hid_index, uint32_t bit_count, USAGE usage)
		{
			switch (usage)
			{
			case hid::usage_generic_desktop::x:
			{
				_hid_index[hid_index].index = value_index_stick_left_x;
				_hid_index[hid_index].feature = feature_stick;
				_hid_index[hid_index].bit_count = bit_count;
				break;
			}
			case hid::usage_generic_desktop::y:
			{
				_hid_index[hid_index].index = value_index_stick_left_y;
				_hid_index[hid_index].feature = feature_stick;
				_hid_index[hid_index].bit_count = bit_count;			
				break;
			}
			case hid::usage_generic_desktop::r_x:
			{
				_hid_index[hid_index].index = value_index_stick_right_x;
				_hid_index[hid_index].feature = feature_stick;
				_hid_index[hid_index].bit_count = bit_count;
				break;
			}
			case hid::usage_generic_desktop::r_y:
			{
				_hid_index[hid_index].index = value_index_stick_right_y;
				_hid_index[hid_index].feature = feature_stick;
				_hid_index[hid_index].bit_count = bit_count;
				break;
			}
			case hid::usage_generic_desktop::z: //on pc, trigger axis is merged into one, centered
			{
				_hid_index[hid_index].index = value_index_lever_left;
				_hid_index[hid_index].feature = feature_lever;
				_hid_index[hid_index].bit_count = bit_count;
				break;
			}
			case hid::usage_generic_desktop::hat_switch:
			{
				_hid_index[hid_index].index = value_index_hat;
				_hid_index[hid_index].feature = feature_hat;
				_hid_index[hid_index].bit_count = bit_count;
				break;
			}
			default:
				break;
			}
		};

		//parse value caps	
		for (size_t i = 0; i < value_caps_count; ++i)
		{
			if ((USHORT)hid::usage_page::generic_desktop == value_caps[i].UsagePage)
			{
				if (value_caps[i].IsRange)
				{
					const auto& range = value_caps[i].Range;
					for (USAGE usage = range.UsageMin; usage <= range.UsageMax; ++usage)
					{
						size_t hid_index = (usage - range.UsageMin) + range.DataIndexMin;
						uint32_t bit_count = value_caps[i].BitSize;

						value_init(hid_index, bit_count, usage);
					}
				}
				else
				{
					USAGE usage = value_caps[i].NotRange.Usage;
					size_t hid_index = value_caps[i].NotRange.DataIndex;
					uint32_t bit_count = value_caps[i].BitSize;

					value_init(hid_index, bit_count, usage);
				}
			}
		}

		device_t::appdata = input->on_device_added(*this);

		return err_ok;
	}

	//--------------------------------------------------
	void raw_gamepad_t::destroy(input_t* input)
	{
		input->on_device_removed(*this);

		app::allocator->free(_hid_data, _hid_size);
	}

	//--------------------------------------------------
	void raw_gamepad_t::on_input(HRAWINPUT h_raw_input, input_t* input)
	{
		UINT size = 0;
		UINT res = GetRawInputData(h_raw_input, RID_INPUT, 0, &size, sizeof(RAWINPUTHEADER));
		if (res != 0)
			return;
		if (size > _input_buffer.size())
		{
			if (_input_buffer.resize(size))
				return;
		}

		PRAWINPUT raw_input = reinterpret_cast<PRAWINPUT>(_input_buffer.data());
		res = GetRawInputData(h_raw_input, RID_INPUT, raw_input, &size, sizeof(RAWINPUTHEADER));
		if ((res == ((UINT)-1)) || (res == 0) || (raw_input->header.dwType != RIM_TYPEHID))
			return;

		HIDP_DATA raw_data[max_index_count];
		ULONG raw_data_count = max_index_count;
		bool raw_data_result = HIDP_STATUS_SUCCESS == HidP_GetData(
			HidP_Input,
			raw_data,
			&raw_data_count,
			_hid_data,
			(PCHAR)raw_input->data.hid.bRawData,
			raw_input->data.hid.dwSizeHid
		);

		if (!raw_data_result)
			return;

		//OS values to hid values
		static uint8_t const hat_switch_mapping[] =
		{
			hid::hat_switch::angle_0,
			hid::hat_switch::angle_45,
			hid::hat_switch::angle_90,
			hid::hat_switch::angle_135,
			hid::hat_switch::angle_180,
			hid::hat_switch::angle_225,
			hid::hat_switch::angle_270,
			hid::hat_switch::angle_315,
			hid::hat_switch::angle_none,
		};

		//update state
		for (size_t i = 0; i < raw_data_count; ++i)
		{
			HIDP_DATA& data = raw_data[i];
			
			index_t& index = _hid_index[data.DataIndex];
			switch (index.feature)
			{
			case feature_switch:
			{
				_hid_state_switch[index.index].value_old = _hid_state_switch[index.index].value_new;
				_hid_state_switch[index.index].value_new = data.On != FALSE;
				break;
			}
			case feature_hat:
			{	
				_hid_state_value[index.index].value_old = _hid_state_value[index.index].value_new;
				_hid_state_value[index.index].value_new = hat_switch_mapping[min(data.RawValue, 8)];
				break;
			}
			case feature_lever:
			{
				//on pc lt and rt are compacted into Z
				int64_t value = data.RawValue;
				value *= c::max_uint16;
				value /= ((uint64_t(1) << index.bit_count) - 1);
				value += c::min_int16;
				value = max(0, value);

				_hid_state_value[index.index].value_old = _hid_state_value[index.index].value_new;
				_hid_state_value[index.index].value_new = (int32_t)value;
				break;
			}
			case feature_stick:
			{
				int64_t value = data.RawValue;
				value *= c::max_uint16;
				value /= ((uint64_t(1) << index.bit_count) - 1);
				value += c::min_int16;

				_hid_state_value[index.index].value_old = _hid_state_value[index.index].value_new;
				_hid_state_value[index.index].value_new = (int32_t)value;
				break;
			}
			default:
				break;
			}
		}

		hid::report_t	report_array[max_index_count];
		size_t			report_count = 0;

		//send updates in order
		for (size_t i = 0; i < value_index_count; ++i)
		{
			if (_hid_state_value[i].value_new != _hid_state_value[i].value_old)
			{
				report_array[report_count] = _hid_state_value[i];
				++report_count;

				_hid_state_value[i].value_old = _hid_state_value[i].value_new;
			}
		}

		for (size_t i = 0; i < max_switch_count; ++i)
		{
			if (_hid_state_switch[i].value_new != _hid_state_switch[i].value_old)
			{
				report_array[report_count] = _hid_state_switch[i];
				++report_count;

				_hid_state_switch[i].value_old = _hid_state_switch[i].value_new;
			}
		}

		input->on_input(report_array, report_count, *this);
	}

	//--------------------------------------------------
	error_t raw_gamepad_t::get_hid_caps(hid::device_caps_t& caps)
	{
		if (nullptr == caps.feature_array)
		{
			caps.stick_count = 4;
			caps.lever_count = 2;
			caps.hat_count = 1;
			caps.range_count = 0;
			caps.button_count = 10;
			caps.feature_count = 17;
		}
		else
		{
			if (17 != caps.feature_count)
				return err_invalid_parameter;

			caps.feature_array[hid_index_stick_left_x].usage = hid::gamepad::stick_left_x;
			caps.feature_array[hid_index_stick_left_x].index = hid_index_stick_left_x;
			caps.feature_array[hid_index_stick_left_x].feature = hid::feature_t::stick;

			caps.feature_array[hid_index_stick_left_y].usage = hid::gamepad::stick_left_y;
			caps.feature_array[hid_index_stick_left_y].index = hid_index_stick_left_y;
			caps.feature_array[hid_index_stick_left_y].feature = hid::feature_t::stick;

			caps.feature_array[hid_index_stick_right_x].usage = hid::gamepad::stick_right_x;
			caps.feature_array[hid_index_stick_right_x].index = hid_index_stick_right_x;
			caps.feature_array[hid_index_stick_right_x].feature = hid::feature_t::stick;

			caps.feature_array[hid_index_stick_right_y].usage = hid::gamepad::stick_right_y;
			caps.feature_array[hid_index_stick_right_y].index = hid_index_stick_right_y;
			caps.feature_array[hid_index_stick_right_y].feature = hid::feature_t::stick;

			caps.feature_array[hid_index_lever_left].usage = hid::gamepad::trigger_left;
			caps.feature_array[hid_index_lever_left].index = hid_index_lever_left;
			caps.feature_array[hid_index_lever_left].feature = hid::feature_t::lever;

			caps.feature_array[hid_index_lever_right].usage = hid::gamepad::trigger_right;
			caps.feature_array[hid_index_lever_right].index = hid_index_lever_right;
			caps.feature_array[hid_index_lever_right].feature = hid::feature_t::lever;

			caps.feature_array[hid_index_hat].usage = hid::gamepad::hat_switch;
			caps.feature_array[hid_index_hat].index = hid_index_hat;
			caps.feature_array[hid_index_hat].feature = hid::feature_t::hat;

			for (hid::index_t i = hid_index_hat + 1; i < (hid_index_hat + 1 + 10); ++i)
			{
				caps.feature_array[i].index = i;
				caps.feature_array[i].feature = hid::feature_t::button;
			}

			caps.feature_array[hid_index_hat + 1 + 0].usage = hid::gamepad::button_A;
			caps.feature_array[hid_index_hat + 1 + 1].usage = hid::gamepad::button_B;
			caps.feature_array[hid_index_hat + 1 + 2].usage = hid::gamepad::button_X;
			caps.feature_array[hid_index_hat + 1 + 3].usage = hid::gamepad::button_Y;
			caps.feature_array[hid_index_hat + 1 + 4].usage = hid::gamepad::button_bumper_left;
			caps.feature_array[hid_index_hat + 1 + 5].usage = hid::gamepad::button_bumper_right;
			caps.feature_array[hid_index_hat + 1 + 6].usage = hid::gamepad::button_start;
			caps.feature_array[hid_index_hat + 1 + 7].usage = hid::gamepad::button_back;
			caps.feature_array[hid_index_hat + 1 + 8].usage = hid::gamepad::button_stick_left;
			caps.feature_array[hid_index_hat + 1 + 9].usage = hid::gamepad::button_stick_right;
		}

		return err_ok;
	}

} }