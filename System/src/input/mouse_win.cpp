#include "pch.h"
#include "mouse_win.h"

namespace outland { namespace os
{
	//--------------------------------------------------
	raw_mouse_t::raw_mouse_t(HANDLE device, RID_DEVICE_INFO_MOUSE& info)
		: _device_handle(device)
		, _device_info(info)
	{
		device_t::device = static_cast<hid::device_id>(reinterpret_cast<uintptr_t>(device));
		device_t::usage = hid::mouse::device;
		device_t::appdata = nullptr;
	}

	//--------------------------------------------------
	raw_mouse_t::~raw_mouse_t()
	{
		
	}

	//--------------------------------------------------
	error_t raw_mouse_t::create(input_t* input)
	{
		device_t::appdata = input->on_device_added(*this);
		return err_ok;
	}

	//--------------------------------------------------
	void raw_mouse_t::destroy(input_t* input)
	{
		input->on_device_removed(*this);
	}

	//--------------------------------------------------
	void raw_mouse_t::on_input(HRAWINPUT h_raw_input, input_t* input)
	{
		RAWINPUT rawinput;
		UINT size = sizeof(RAWINPUT);
		UINT res = GetRawInputData(h_raw_input, RID_INPUT, &rawinput, &size, sizeof(RAWINPUTHEADER));
		if (res == ((UINT)-1) || (res == 0) || rawinput.header.dwType != RIM_TYPEMOUSE)
			return;

		hid::report_t	report_array[16];
		size_t			report_count = 0;

		auto& ms = rawinput.data.mouse;
		if (MOUSE_MOVE_RELATIVE == ms.usFlags) //flags, but relative is zero :(
		{
			report_array[report_count].value_new = ms.lLastX;
			report_array[report_count].value_old = 0;
			report_array[report_count].index = hid_index_position_x;
			++report_count;

			report_array[report_count].value_new = ms.lLastY;
			report_array[report_count].value_old = 0;
			report_array[report_count].index = hid_index_position_y;
			++report_count;
		}

		if (ms.usButtonFlags & RI_MOUSE_WHEEL)
		{
			report_array[report_count].value_new = ms.usButtonData;
			report_array[report_count].value_old = 0;
			report_array[report_count].index = hid_index_wheel;
			++report_count;
		}

		struct button_mapping_t
		{
			USHORT			raw_mask;
			hid::usage_t	usage;
			hid::value_t	value_new;
			hid::value_t	value_old;
		};

		static const button_mapping_t button_mapping[] =
		{
			{ RI_MOUSE_BUTTON_1_DOWN ,	hid::mouse::button_1, 1 , 0},
			{ RI_MOUSE_BUTTON_1_UP ,	hid::mouse::button_1, 0 , 1},

			{ RI_MOUSE_BUTTON_2_DOWN ,	hid::mouse::button_2, 1 , 0},
			{ RI_MOUSE_BUTTON_2_UP ,	hid::mouse::button_2, 0 , 1},

			{ RI_MOUSE_BUTTON_3_DOWN ,	hid::mouse::button_3, 1 , 0},
			{ RI_MOUSE_BUTTON_3_UP ,	hid::mouse::button_3, 0 , 1},

			{ RI_MOUSE_BUTTON_4_DOWN ,	hid::mouse::button_4, 1 , 0},
			{ RI_MOUSE_BUTTON_4_UP ,	hid::mouse::button_4, 0 , 1},

			{ RI_MOUSE_BUTTON_5_DOWN ,	hid::mouse::button_5, 1 , 0},
			{ RI_MOUSE_BUTTON_5_UP ,	hid::mouse::button_5, 0 , 1},
		};

		for (button_mapping_t const& it : button_mapping)
		{
			if (ms.usButtonFlags & it.raw_mask)
			{
				report_array[report_count].value_new = it.value_new;
				report_array[report_count].value_old = it.value_old;
				report_array[report_count].index = it.usage - hid::mouse::button_1 + hid_index_button1;
				++report_count;
			}
		}

		input->on_input(report_array, report_count, *this);
	}

	//--------------------------------------------------
	error_t raw_mouse_t::get_hid_caps(hid::device_caps_t& caps)
	{
		if (nullptr == caps.feature_array)
		{
			caps.range_count = 3;
			caps.button_count = 5;
			caps.feature_count = hid_index_count;

			caps.stick_count = 0;
			caps.lever_count = 0;
			caps.hat_count = 0;
		}
		else
		{

			if (hid_index_count != caps.feature_count)
				return err_invalid_parameter;

			caps.feature_array[hid_index_position_x].usage = hid::mouse::position_x;
			caps.feature_array[hid_index_position_x].index = hid_index_position_x;
			caps.feature_array[hid_index_position_x].feature = hid::feature_t::range;

			caps.feature_array[hid_index_position_y].usage = hid::mouse::position_y;
			caps.feature_array[hid_index_position_y].index = hid_index_position_y;
			caps.feature_array[hid_index_position_y].feature = hid::feature_t::range;

			caps.feature_array[hid_index_wheel].usage = hid::mouse::wheel;
			caps.feature_array[hid_index_wheel].index = hid_index_wheel;
			caps.feature_array[hid_index_wheel].feature = hid::feature_t::range;

			caps.feature_array[hid_index_button1].usage = hid::mouse::button_1;
			caps.feature_array[hid_index_button1].index = hid_index_button1;
			caps.feature_array[hid_index_button1].feature = hid::feature_t::button;

			caps.feature_array[hid_index_button2].usage = hid::mouse::button_2;
			caps.feature_array[hid_index_button2].index = hid_index_button2;
			caps.feature_array[hid_index_button2].feature = hid::feature_t::button;

			caps.feature_array[hid_index_button3].usage = hid::mouse::button_3;
			caps.feature_array[hid_index_button3].index = hid_index_button3;
			caps.feature_array[hid_index_button3].feature = hid::feature_t::button;

			caps.feature_array[hid_index_button4].usage = hid::mouse::button_4;
			caps.feature_array[hid_index_button4].index = hid_index_button4;
			caps.feature_array[hid_index_button4].feature = hid::feature_t::button;

			caps.feature_array[hid_index_button5].usage = hid::mouse::button_5;
			caps.feature_array[hid_index_button5].index = hid_index_button5;
			caps.feature_array[hid_index_button5].feature = hid::feature_t::button;
		}

		return err_ok;
	}

} }