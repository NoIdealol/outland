#include "pch.h"
#include "input_win.h"
#include "application\library_win.h"
#include "keyboard_win.h"
#include "mouse_win.h"

namespace outland { namespace  os
{
	static_assert(sizeof(HANDLE) == sizeof(hid::device_id), "bad handle");

	namespace g
	{
		static raw_input_t*	raw_input = nullptr;
	}

	//--------------------------------------------------
	raw_input_t::raw_input_t(input_t* input, error_t& err)
		: _input(input)
	{
		err = err_ok;

		err |= _keyboard_list.reserve(4);
		err |= _mouse_list.reserve(4);
		err |= _gamepad_list.reserve(8);
	}

	//--------------------------------------------------
	bool raw_input_t::devices_register(hid::usage_t usage)
	{
		RAWINPUTDEVICE device;
		device.usUsagePage = (USHORT)(hid::usage_page::generic_desktop);
		device.usUsage = (USHORT)usage;
		device.dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
		device.hwndTarget = _hwnd;

		return TRUE == RegisterRawInputDevices(&device, 1, sizeof(device));
	}

	//--------------------------------------------------
	void raw_input_t::devices_unregister(hid::usage_t usage)
	{
		RAWINPUTDEVICE device;
		device.usUsagePage = (USHORT)(hid::usage_page::generic_desktop);
		device.usUsage = (USHORT)usage;
		device.dwFlags = RIDEV_REMOVE;
		device.hwndTarget = _hwnd;
		RegisterRawInputDevices(&device, 1, sizeof(device));
	}

	//--------------------------------------------------
	error_t raw_input_t::window_register()
	{
		SetLastError(0);

		HICON hIcon = NULL;
		HICON hIconSm = NULL;
		HBRUSH hbrBackground = reinterpret_cast<HBRUSH>(COLOR_3DDKSHADOW + 1); //NULL;//

		WNDCLASSEXW wcex;
		wcex.cbSize = sizeof(WNDCLASSEXW);
		wcex.style = 0;//CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = &raw_input_t::window_callback;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = get_native_handle(app::library);
		wcex.hIcon = hIcon;
		wcex.hIconSm = hIconSm;
		wcex.hCursor = NULL;
		wcex.hbrBackground = hbrBackground;
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = g::raw_input->_class_name;

		ATOM window = RegisterClassExW(&wcex);
		if (NULL == window)
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t raw_input_t::window_create()
	{
		_hwnd = CreateWindowExW(
			WS_EX_TOOLWINDOW,
			_class_name,
			nullptr,
			WS_POPUP,
			0,
			0,
			100,
			100,
			HWND_MESSAGE,
			NULL,
			get_native_handle(app::library),
			NULL);

		if (NULL == _hwnd)
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	void raw_input_t::window_destroy()
	{
		DestroyWindow(_hwnd);
	}

	//--------------------------------------------------
	void raw_input_t::window_unregister()
	{
		UnregisterClassW(_class_name, get_native_handle(app::library));
	}


	//--------------------------------------------------
	error_t input::create(input_t* input)
	{
		if (nullptr != g::raw_input)
			return false;

		error_t err = err_ok;
		g::raw_input = mem<raw_input_t>::alloc(app::allocator, input, err);
		if (nullptr == g::raw_input)
			return err_out_of_memory;

		if (err || (err = g::raw_input->window_register()))
			goto clean_mem;

		if (err = g::raw_input->window_create())
			goto clean_window_class;

		return err;

	clean_window_class:
		g::raw_input->window_unregister();
	clean_mem:	
		mem<raw_input_t>::free(app::allocator, g::raw_input);
		g::raw_input = nullptr;

		return err;
	}

	//--------------------------------------------------
	void input::destroy()
	{
		if (nullptr == g::raw_input)
			return;

		g::raw_input->window_destroy();
		g::raw_input->window_unregister();
	
		mem<raw_input_t>::free(app::allocator, g::raw_input);
		g::raw_input = nullptr;
	}

	//--------------------------------------------------
	bool input::register_hid(hid::usage_t usage)
	{
		if (nullptr == g::raw_input)
			return false;

		switch (usage)
		{
		case hid::mouse::device:
		case hid::keyboard::device:
		case hid::gamepad::device:
			return g::raw_input->devices_register(usage & c::max_uint16);
		default:
			return false;
		}
	}

	//--------------------------------------------------
	void input::unregister_hid(hid::usage_t usage)
	{
		if (nullptr == g::raw_input)
			return;

		switch (usage)
		{
		case hid::mouse::device:
		case hid::keyboard::device:
		case hid::gamepad::device:
			g::raw_input->devices_unregister(usage & c::max_uint16);
			break;
		default:
			return;
		}
	}

	//--------------------------------------------------
	error_t input::get_hid_caps(hid::device_id device, hid::device_caps_t& caps)
	{
		return g::raw_input->get_hid_caps(device, caps);
	}

	//--------------------------------------------------
	error_t raw_input_t::get_hid_caps(hid::device_id device, hid::device_caps_t& caps)
	{
		for (auto& it : _keyboard_list)
		{
			if (reinterpret_cast<HANDLE>(device) == it.handle())
				return it.get_hid_caps(caps);
		}

		for (auto& it : _mouse_list)
		{
			if (reinterpret_cast<HANDLE>(device) == it.handle())
				return it.get_hid_caps(caps);
		}

		for (auto& it : _gamepad_list)
		{
			if (reinterpret_cast<HANDLE>(device) == it.handle())
				return it.get_hid_caps(caps);
		}

		return err_not_found;
	}

	//--------------------------------------------------
	LRESULT CALLBACK raw_input_t::window_callback(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_INPUT:
		{
			RAWINPUTHEADER header;
			UINT size = sizeof(RAWINPUTHEADER);
			UINT res = GetRawInputData((HRAWINPUT)lParam, RID_HEADER, &header, &size, size);
			if (res != (UINT)-1 && res != 0)
			{
				HANDLE hDevice = header.hDevice;

				switch (header.dwType)
				{
				case RIM_TYPEKEYBOARD:
				{
					for (auto& it : g::raw_input->_keyboard_list)
					{
						if (it.handle() == hDevice)
						{
							it.on_input((HRAWINPUT)lParam, g::raw_input->_input);
							break;
						}
					}
					break;
				}
				case RIM_TYPEMOUSE:
				{
					for (auto& it : g::raw_input->_mouse_list)
					{
						if (it.handle() == hDevice)
						{
							it.on_input((HRAWINPUT)lParam, g::raw_input->_input);
							break;
						}
					}
					break;
				}
				case RIM_TYPEHID: //human interface device -> only gamepad is registered
				{
					for (auto& it : g::raw_input->_gamepad_list)
					{
						if (it.handle() == hDevice)
						{
							it.on_input((HRAWINPUT)lParam, g::raw_input->_input);
							break;
						}
					}
					break;
				}
				default:
					break; //return def proc to cleanup
				}
			}
			DefWindowProcW(hWnd, msg, wParam, lParam); //clean the msg
			return 0;
		}
		case WM_INPUT_DEVICE_CHANGE:
		{
			HANDLE hDevice = (HANDLE)lParam;
			switch (wParam)
			{
			case GIDC_ARRIVAL:
			{
				RID_DEVICE_INFO info;
				info.cbSize = sizeof(RID_DEVICE_INFO);
				UINT infoSize = sizeof(RID_DEVICE_INFO);
				UINT res = GetRawInputDeviceInfoW(hDevice, RIDI_DEVICEINFO, &info, &infoSize);
				if (res != (UINT)-1 && res != 0)
				{
					switch (info.dwType)
					{
					case RIM_TYPEHID:
					{
						switch (info.hid.usUsage)
						{
						case hid::usage_generic_desktop::gamepad:
						{
							//we dont care about memory here
							if (err_ok == g::raw_input->_gamepad_list.push_back(hDevice, info.hid))
							{
								if (err_ok != g::raw_input->_gamepad_list.last().create(g::raw_input->_input))
									g::raw_input->_gamepad_list.pop_back();
							}
							break;
						}
						default:
							break;
						}
						break;
					}
					case RIM_TYPEKEYBOARD:
					{
						if (err_ok == g::raw_input->_keyboard_list.push_back(hDevice, info.keyboard))
						{
							if (err_ok != g::raw_input->_keyboard_list.last().create(g::raw_input->_input))
								g::raw_input->_keyboard_list.pop_back();
						}
						break;
					}
					case RIM_TYPEMOUSE:
					{
						if (err_ok == g::raw_input->_mouse_list.push_back(hDevice, info.mouse))
						{
							if (err_ok != g::raw_input->_mouse_list.last().create(g::raw_input->_input))
								g::raw_input->_mouse_list.pop_back();
						}
						break;
					}
					default:
						break;
					}
				}
				break;
			}
			case GIDC_REMOVAL:
			{
				for (auto it = g::raw_input->_keyboard_list.begin(); it != g::raw_input->_keyboard_list.end(); ++it)
				{
					if (it->handle() == hDevice)
					{
						it->destroy(g::raw_input->_input);
						g::raw_input->_keyboard_list.remove_swap(it);
						break;
					}
				}

				for (auto it = g::raw_input->_mouse_list.begin(); it != g::raw_input->_mouse_list.end(); ++it)
				{
					if (it->handle() == hDevice)
					{
						it->destroy(g::raw_input->_input);
						g::raw_input->_mouse_list.remove_swap(it);
						break;
					}
				}

				for (auto it = g::raw_input->_gamepad_list.begin(); it != g::raw_input->_gamepad_list.end(); ++it)
				{
					if (it->handle() == hDevice)
					{
						it->destroy(g::raw_input->_input);
						g::raw_input->_gamepad_list.remove_swap(it);
						break;
					}
				}

				break;
			}
			default:
				break;
			}
			DefWindowProcW(hWnd, msg, wParam, lParam);
			return 0;
		}
		//case WM_INPUTLANGCHANGE:
			//return 0;
		default:
			break;
		}

		return DefWindowProcW(hWnd, msg, wParam, lParam);
	}

} }