#pragma once

#include "system\input\mouse.h"
#include "system\input\input.h"

namespace outland { namespace os
{
	//--------------------------------------------------
	class raw_mouse_t : public hid::device_t
	{
	private:
		enum : hid::index_t
		{
			hid_index_position_x,
			hid_index_position_y,
			hid_index_wheel,
			hid_index_button1,
			hid_index_button2,
			hid_index_button3,
			hid_index_button4,
			hid_index_button5,

			hid_index_count,
		};
	private:
		HANDLE						_device_handle;
		RID_DEVICE_INFO_MOUSE		_device_info;

	public:
		raw_mouse_t(HANDLE device, RID_DEVICE_INFO_MOUSE& info);
		~raw_mouse_t();

		error_t	create(input_t* input);
		void	destroy(input_t* input);

		void on_input(HRAWINPUT h_raw_input, input_t* input);

		inline HANDLE handle() { return _device_handle; }

		error_t get_hid_caps(hid::device_caps_t& caps);
	};

} }