#pragma once
#include "pch.h"
#include "keyboard_win.h"

namespace outland { namespace os
{
	
	/*
	The scancode values come from:
	- http://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/scancode.doc (March 16, 2000).
	- http://www.computer-engineering.org/ps2keyboard/scancodes1.html
	- using MapVirtualKeyEx( VK_*, MAPVK_VK_TO_VSC_EX, 0 ) with the english us keyboard layout
	- reading win32 WM_INPUT keyboard messages.
	*/

	//--------------------------------------------------
	enum scancode_win_t : uint8_t
	{
		sc_escape = 0x01,
		sc_1 = 0x02,
		sc_2 = 0x03,
		sc_3 = 0x04,
		sc_4 = 0x05,
		sc_5 = 0x06,
		sc_6 = 0x07,
		sc_7 = 0x08,
		sc_8 = 0x09,
		sc_9 = 0x0A,
		sc_0 = 0x0B,
		sc_minus = 0x0C,
		sc_equals = 0x0D,
		sc_backspace = 0x0E,
		sc_tab = 0x0F,
		sc_q = 0x10,
		sc_w = 0x11,
		sc_e = 0x12,
		sc_r = 0x13,
		sc_t = 0x14,
		sc_y = 0x15,
		sc_u = 0x16,
		sc_i = 0x17,
		sc_o = 0x18,
		sc_p = 0x19,
		sc_bracket_left = 0x1A,
		sc_bracket_right = 0x1B,
		sc_enter = 0x1C,
		sc_control_left = 0x1D,
		sc_a = 0x1E,
		sc_s = 0x1F,
		sc_d = 0x20,
		sc_f = 0x21,
		sc_g = 0x22,
		sc_h = 0x23,
		sc_j = 0x24,
		sc_k = 0x25,
		sc_l = 0x26,
		sc_semicolon = 0x27,
		sc_quote = 0x28,
		sc_tilde = 0x29,
		sc_shift_left = 0x2A,
		sc_backslash = 0x2B,
		sc_z = 0x2C,
		sc_x = 0x2D,
		sc_c = 0x2E,
		sc_v = 0x2F,
		sc_b = 0x30,
		sc_n = 0x31,
		sc_m = 0x32,
		sc_comma = 0x33,
		sc_preiod = 0x34,
		sc_slash = 0x35,
		sc_shift_right = 0x36,
		sc_num_multiply = 0x37,
		sc_alt_left = 0x38,
		sc_space = 0x39,
		sc_capslock = 0x3A,
		sc_f1 = 0x3B,
		sc_f2 = 0x3C,
		sc_f3 = 0x3D,
		sc_f4 = 0x3E,
		sc_f5 = 0x3F,
		sc_f6 = 0x40,
		sc_f7 = 0x41,
		sc_f8 = 0x42,
		sc_f9 = 0x43,
		sc_f10 = 0x44,
		sc_numlock = 0x45, //should be 0xE045, but rawinput doesnt indicate it
		sc_scrolllock = 0x46,
		sc_num_7 = 0x47,
		sc_num_8 = 0x48,
		sc_num_9 = 0x49,
		sc_num_minus = 0x4A,
		sc_num_4 = 0x4B,
		sc_num_5 = 0x4C,
		sc_num_6 = 0x4D,
		sc_num_plus = 0x4E,
		sc_num_1 = 0x4F,
		sc_num_2 = 0x50,
		sc_num_3 = 0x51,
		sc_num_0 = 0x52,
		sc_num_period = 0x53,
		sc_printscreen_v2 = 0x54, /* Alt + print screen. MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54. */
		sc_backslash_102 = 0x56, /* Key between the left shift and Z. */
		sc_f11 = 0x57,
		sc_f12 = 0x58,
		sc_help = 0x63,
		sc_f13 = 0x64,
		sc_f14 = 0x65,
		sc_f15 = 0x66,
		sc_f16 = 0x67,
		sc_f17 = 0x68,
		sc_f18 = 0x69,
		sc_f19 = 0x6a,
		sc_f20 = 0x6b,
		sc_f21 = 0x6c,
		sc_f22 = 0x6d,
		sc_f23 = 0x6e,
		sc_f24 = 0x76,
		sc_pause_v1_s2 = 0x45,
	};

	//--------------------------------------------------
	enum scancode_ignore_win_t : uint8_t
	{
		sc_ignore_s1 = 0x2A,
		sc_ignore_s2 = 0xAA,
		sc_ignore_s3 = 0xB6,
		sc_ignore_s4 = 0x36,
	};

	//--------------------------------------------------
	enum scancode_e0_win_t : uint8_t
	{
		sc_numpad_enter = 0x1C,
		sc_control_right = 0x1D,
		sc_numpad_divide = 0x35,
		sc_printscreen_v1 = 0x37, //sc_ignore_s1, 0xE037 sends two codes, the first can be ignored
		sc_alt_right = 0x38,
		sc_pause_v2 = 0x46, /* CTRL + Pause */
		sc_home = 0x47,
		sc_arrow_up = 0x48,
		sc_page_up = 0x49,
		sc_arrow_left = 0x4B,
		sc_arrow_right = 0x4D,
		sc_end = 0x4F,
		sc_arrow_down = 0x50,
		sc_page_down = 0x51,
		sc_insert = 0x52,
		sc_delete = 0x53,
		sc_win_left = 0x5B, //win button
		sc_win_right = 0x5C,
		sc_menu = 0x5D, //menu near win

		/*
		sc_printScreen:
		- make: 0xE02A 0xE037
		- break: 0xE0B7 0xE0AA
		- MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54; remap to print screen maybe?
		- There is no VK_KEYDOWN with VK_SNAPSHOT.
		*/
	};

	//--------------------------------------------------
	enum scancode_e1_win_t : uint8_t
	{
		sc_pause_v1_s1 = 0x1D,
		/*
		sc_pause:
		- make: 0xE11D 45 0xE19D C5
		- make in raw input: 0xE11D 0x45
		- break: none
		- No repeat when you hold the key down
		- There are no break so I don't know how the key down/up is expected to work. Raw input sends "keydown" and "keyup" messages, and it appears that the keyup message is sent directly after the keydown message (you can't hold the key down) so depending on when GetMessage or PeekMessage will return messages, you may get both a keydown and keyup message "at the same time". If you use VK messages most of the time you only get keydown messages, but some times you get keyup messages too.
		- when pressed at the same time as one or both control keys, generates a 0xE046 (sc_cancel) and the string for that scancode is "break".
		*/
	};

	//--------------------------------------------------
	using scancode_t = uint8_t;
	using scancode_ex_t = uint8_t;
	enum : scancode_ex_t
	{
		sc_ex_00 = 0,
		sc_ex_E0 = 1,
		sc_ex_E1 = 2,
	};

	class raw_keyboard_mapper_t
	{
		hid::usage_t _sc_to_hid[2][c::max_uint8];
		hid::index_t _hid_to_index[256];

	public:
		raw_keyboard_mapper_t()
		{
			for (auto& ex : _sc_to_hid)
			{
				for (auto& sc : ex)
				{
					sc = hid::keyboard::key_none;
				}
			}

			for (auto& idx : _hid_to_index)
			{
				idx = raw_keyboard_t::hid_index_count;
			}


			_sc_to_hid[sc_ex_00][sc_escape] = hid::keyboard::key_escape;
			_sc_to_hid[sc_ex_00][sc_1] = hid::keyboard::key_1;
			_sc_to_hid[sc_ex_00][sc_2] = hid::keyboard::key_2;
			_sc_to_hid[sc_ex_00][sc_3] = hid::keyboard::key_3;
			_sc_to_hid[sc_ex_00][sc_4] = hid::keyboard::key_4;
			_sc_to_hid[sc_ex_00][sc_5] = hid::keyboard::key_5;
			_sc_to_hid[sc_ex_00][sc_6] = hid::keyboard::key_6;
			_sc_to_hid[sc_ex_00][sc_7] = hid::keyboard::key_7;
			_sc_to_hid[sc_ex_00][sc_8] = hid::keyboard::key_8;
			_sc_to_hid[sc_ex_00][sc_9] = hid::keyboard::key_9;
			_sc_to_hid[sc_ex_00][sc_0] = hid::keyboard::key_0;
			_sc_to_hid[sc_ex_00][sc_minus] = hid::keyboard::key_minus;
			_sc_to_hid[sc_ex_00][sc_equals] = hid::keyboard::key_equals;
			_sc_to_hid[sc_ex_00][sc_backspace] = hid::keyboard::key_backspace;
			_sc_to_hid[sc_ex_00][sc_tab] = hid::keyboard::key_tab;
			_sc_to_hid[sc_ex_00][sc_q] = hid::keyboard::key_Q;
			_sc_to_hid[sc_ex_00][sc_w] = hid::keyboard::key_W;
			_sc_to_hid[sc_ex_00][sc_e] = hid::keyboard::key_E;
			_sc_to_hid[sc_ex_00][sc_r] = hid::keyboard::key_R;
			_sc_to_hid[sc_ex_00][sc_t] = hid::keyboard::key_T;
			_sc_to_hid[sc_ex_00][sc_y] = hid::keyboard::key_Y;
			_sc_to_hid[sc_ex_00][sc_u] = hid::keyboard::key_U;
			_sc_to_hid[sc_ex_00][sc_i] = hid::keyboard::key_I;
			_sc_to_hid[sc_ex_00][sc_o] = hid::keyboard::key_O;
			_sc_to_hid[sc_ex_00][sc_p] = hid::keyboard::key_P;
			_sc_to_hid[sc_ex_00][sc_bracket_left] = hid::keyboard::key_bracket_left;
			_sc_to_hid[sc_ex_00][sc_bracket_right] = hid::keyboard::key_bracket_right;
			_sc_to_hid[sc_ex_00][sc_enter] = hid::keyboard::key_enter;
			_sc_to_hid[sc_ex_00][sc_control_left] = hid::keyboard::key_control_left;
			_sc_to_hid[sc_ex_00][sc_a] = hid::keyboard::key_A;
			_sc_to_hid[sc_ex_00][sc_s] = hid::keyboard::key_S;
			_sc_to_hid[sc_ex_00][sc_d] = hid::keyboard::key_D;
			_sc_to_hid[sc_ex_00][sc_f] = hid::keyboard::key_F;
			_sc_to_hid[sc_ex_00][sc_g] = hid::keyboard::key_G;
			_sc_to_hid[sc_ex_00][sc_h] = hid::keyboard::key_H;
			_sc_to_hid[sc_ex_00][sc_j] = hid::keyboard::key_J;
			_sc_to_hid[sc_ex_00][sc_k] = hid::keyboard::key_K;
			_sc_to_hid[sc_ex_00][sc_l] = hid::keyboard::key_L;
			_sc_to_hid[sc_ex_00][sc_semicolon] = hid::keyboard::key_semicolon;
			_sc_to_hid[sc_ex_00][sc_quote] = hid::keyboard::key_quote;
			_sc_to_hid[sc_ex_00][sc_tilde] = hid::keyboard::key_tilde;
			_sc_to_hid[sc_ex_00][sc_shift_left] = hid::keyboard::key_shift_left;
			_sc_to_hid[sc_ex_00][sc_backslash] = hid::keyboard::key_backslash;
			_sc_to_hid[sc_ex_00][sc_z] = hid::keyboard::key_Z;
			_sc_to_hid[sc_ex_00][sc_x] = hid::keyboard::key_X;
			_sc_to_hid[sc_ex_00][sc_c] = hid::keyboard::key_C;
			_sc_to_hid[sc_ex_00][sc_v] = hid::keyboard::key_V;
			_sc_to_hid[sc_ex_00][sc_b] = hid::keyboard::key_B;
			_sc_to_hid[sc_ex_00][sc_n] = hid::keyboard::key_N;
			_sc_to_hid[sc_ex_00][sc_m] = hid::keyboard::key_M;
			_sc_to_hid[sc_ex_00][sc_comma] = hid::keyboard::key_comma;
			_sc_to_hid[sc_ex_00][sc_preiod] = hid::keyboard::key_period;
			_sc_to_hid[sc_ex_00][sc_slash] = hid::keyboard::key_slash;
			_sc_to_hid[sc_ex_00][sc_shift_right] = hid::keyboard::key_shift_right;
			_sc_to_hid[sc_ex_00][sc_num_multiply] = hid::keyboard::key_num_multiply;
			_sc_to_hid[sc_ex_00][sc_alt_left] = hid::keyboard::key_alt_left;
			_sc_to_hid[sc_ex_00][sc_space] = hid::keyboard::key_space;
			_sc_to_hid[sc_ex_00][sc_capslock] = hid::keyboard::key_caps_lock;
			_sc_to_hid[sc_ex_00][sc_f1] = hid::keyboard::key_F1;
			_sc_to_hid[sc_ex_00][sc_f2] = hid::keyboard::key_F2;
			_sc_to_hid[sc_ex_00][sc_f3] = hid::keyboard::key_F3;
			_sc_to_hid[sc_ex_00][sc_f4] = hid::keyboard::key_F4;
			_sc_to_hid[sc_ex_00][sc_f5] = hid::keyboard::key_F5;
			_sc_to_hid[sc_ex_00][sc_f6] = hid::keyboard::key_F6;
			_sc_to_hid[sc_ex_00][sc_f7] = hid::keyboard::key_F7;
			_sc_to_hid[sc_ex_00][sc_f8] = hid::keyboard::key_F8;
			_sc_to_hid[sc_ex_00][sc_f9] = hid::keyboard::key_F9;
			_sc_to_hid[sc_ex_00][sc_f10] = hid::keyboard::key_F10;
			_sc_to_hid[sc_ex_00][sc_numlock] = hid::keyboard::key_num_lock;
			_sc_to_hid[sc_ex_00][sc_scrolllock] = hid::keyboard::key_scroll_lock;
			_sc_to_hid[sc_ex_00][sc_num_7] = hid::keyboard::key_num_7;
			_sc_to_hid[sc_ex_00][sc_num_8] = hid::keyboard::key_num_8;
			_sc_to_hid[sc_ex_00][sc_num_9] = hid::keyboard::key_num_9;
			_sc_to_hid[sc_ex_00][sc_num_minus] = hid::keyboard::key_num_minus;
			_sc_to_hid[sc_ex_00][sc_num_4] = hid::keyboard::key_num_4;
			_sc_to_hid[sc_ex_00][sc_num_5] = hid::keyboard::key_num_5;
			_sc_to_hid[sc_ex_00][sc_num_6] = hid::keyboard::key_num_6;
			_sc_to_hid[sc_ex_00][sc_num_plus] = hid::keyboard::key_num_plus;
			_sc_to_hid[sc_ex_00][sc_num_1] = hid::keyboard::key_num_1;
			_sc_to_hid[sc_ex_00][sc_num_2] = hid::keyboard::key_num_2;
			_sc_to_hid[sc_ex_00][sc_num_3] = hid::keyboard::key_num_3;
			_sc_to_hid[sc_ex_00][sc_num_0] = hid::keyboard::key_num_0;
			_sc_to_hid[sc_ex_00][sc_num_period] = hid::keyboard::key_num_period;
			_sc_to_hid[sc_ex_00][sc_printscreen_v2] = hid::keyboard::key_print_screen; /* Alt + print screen. MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54. */
			_sc_to_hid[sc_ex_00][sc_backslash_102] = hid::keyboard::key_backslash_102; /* Key between the left shift and Z. */
			_sc_to_hid[sc_ex_00][sc_f11] = hid::keyboard::key_F11;
			_sc_to_hid[sc_ex_00][sc_f12] = hid::keyboard::key_F12;
			_sc_to_hid[sc_ex_00][sc_help] = hid::keyboard::key_help;
			_sc_to_hid[sc_ex_00][sc_f13] = hid::keyboard::key_F13;
			_sc_to_hid[sc_ex_00][sc_f14] = hid::keyboard::key_F14;
			_sc_to_hid[sc_ex_00][sc_f15] = hid::keyboard::key_F15;
			_sc_to_hid[sc_ex_00][sc_f16] = hid::keyboard::key_F16;
			_sc_to_hid[sc_ex_00][sc_f17] = hid::keyboard::key_F17;
			_sc_to_hid[sc_ex_00][sc_f18] = hid::keyboard::key_F18;
			_sc_to_hid[sc_ex_00][sc_f19] = hid::keyboard::key_F19;
			_sc_to_hid[sc_ex_00][sc_f20] = hid::keyboard::key_F20;
			_sc_to_hid[sc_ex_00][sc_f21] = hid::keyboard::key_F21;
			_sc_to_hid[sc_ex_00][sc_f22] = hid::keyboard::key_F22;
			_sc_to_hid[sc_ex_00][sc_f23] = hid::keyboard::key_F23;
			_sc_to_hid[sc_ex_00][sc_f24] = hid::keyboard::key_F24;
			
			_sc_to_hid[sc_ex_E0][sc_numpad_enter] = hid::keyboard::key_num_enter;
			_sc_to_hid[sc_ex_E0][sc_control_right] = hid::keyboard::key_control_right;
			_sc_to_hid[sc_ex_E0][sc_numpad_divide] = hid::keyboard::key_num_divide;
			_sc_to_hid[sc_ex_E0][sc_printscreen_v1] = hid::keyboard::key_print_screen;
			_sc_to_hid[sc_ex_E0][sc_alt_right] = hid::keyboard::key_alt_right;
			_sc_to_hid[sc_ex_E0][sc_pause_v2] = hid::keyboard::key_pause;
			_sc_to_hid[sc_ex_E0][sc_home] = hid::keyboard::key_home;
			_sc_to_hid[sc_ex_E0][sc_arrow_up] = hid::keyboard::key_up;
			_sc_to_hid[sc_ex_E0][sc_page_up] = hid::keyboard::key_page_up;
			_sc_to_hid[sc_ex_E0][sc_arrow_left] = hid::keyboard::key_left;
			_sc_to_hid[sc_ex_E0][sc_arrow_right] = hid::keyboard::key_right;
			_sc_to_hid[sc_ex_E0][sc_end] = hid::keyboard::key_end;
			_sc_to_hid[sc_ex_E0][sc_arrow_down] = hid::keyboard::key_down;
			_sc_to_hid[sc_ex_E0][sc_page_down] = hid::keyboard::key_page_down;
			_sc_to_hid[sc_ex_E0][sc_insert] = hid::keyboard::key_insert;
			_sc_to_hid[sc_ex_E0][sc_delete] = hid::keyboard::key_delete;
			_sc_to_hid[sc_ex_E0][sc_win_left] = hid::keyboard::key_win_left;
			_sc_to_hid[sc_ex_E0][sc_win_right] = hid::keyboard::key_win_right;
			_sc_to_hid[sc_ex_E0][sc_menu] = hid::keyboard::key_menu;

			//_hid_to_index
			_hid_to_index[0xFF & hid::keyboard::key_A				] = raw_keyboard_t::hid_index_key_A;
			_hid_to_index[0xFF & hid::keyboard::key_B				] = raw_keyboard_t::hid_index_key_B;
			_hid_to_index[0xFF & hid::keyboard::key_C				] = raw_keyboard_t::hid_index_key_C;
			_hid_to_index[0xFF & hid::keyboard::key_D				] = raw_keyboard_t::hid_index_key_D;
			_hid_to_index[0xFF & hid::keyboard::key_E				] = raw_keyboard_t::hid_index_key_E;
			_hid_to_index[0xFF & hid::keyboard::key_F				] = raw_keyboard_t::hid_index_key_F;
			_hid_to_index[0xFF & hid::keyboard::key_G				] = raw_keyboard_t::hid_index_key_G;
			_hid_to_index[0xFF & hid::keyboard::key_H				] = raw_keyboard_t::hid_index_key_H;
			_hid_to_index[0xFF & hid::keyboard::key_I				] = raw_keyboard_t::hid_index_key_I;
			_hid_to_index[0xFF & hid::keyboard::key_J				] = raw_keyboard_t::hid_index_key_J;
			_hid_to_index[0xFF & hid::keyboard::key_K				] = raw_keyboard_t::hid_index_key_K;
			_hid_to_index[0xFF & hid::keyboard::key_L				] = raw_keyboard_t::hid_index_key_L;
			_hid_to_index[0xFF & hid::keyboard::key_M				] = raw_keyboard_t::hid_index_key_M;
			_hid_to_index[0xFF & hid::keyboard::key_N				] = raw_keyboard_t::hid_index_key_N;
			_hid_to_index[0xFF & hid::keyboard::key_O				] = raw_keyboard_t::hid_index_key_O;
			_hid_to_index[0xFF & hid::keyboard::key_P				] = raw_keyboard_t::hid_index_key_P;
			_hid_to_index[0xFF & hid::keyboard::key_Q				] = raw_keyboard_t::hid_index_key_Q;
			_hid_to_index[0xFF & hid::keyboard::key_R				] = raw_keyboard_t::hid_index_key_R;
			_hid_to_index[0xFF & hid::keyboard::key_S				] = raw_keyboard_t::hid_index_key_S;
			_hid_to_index[0xFF & hid::keyboard::key_T				] = raw_keyboard_t::hid_index_key_T;
			_hid_to_index[0xFF & hid::keyboard::key_U				] = raw_keyboard_t::hid_index_key_U;
			_hid_to_index[0xFF & hid::keyboard::key_V				] = raw_keyboard_t::hid_index_key_V;
			_hid_to_index[0xFF & hid::keyboard::key_W				] = raw_keyboard_t::hid_index_key_W;
			_hid_to_index[0xFF & hid::keyboard::key_X				] = raw_keyboard_t::hid_index_key_X;
			_hid_to_index[0xFF & hid::keyboard::key_Y				] = raw_keyboard_t::hid_index_key_Y;
			_hid_to_index[0xFF & hid::keyboard::key_Z				] = raw_keyboard_t::hid_index_key_Z;
			_hid_to_index[0xFF & hid::keyboard::key_1				] = raw_keyboard_t::hid_index_key_1;
			_hid_to_index[0xFF & hid::keyboard::key_2				] = raw_keyboard_t::hid_index_key_2;
			_hid_to_index[0xFF & hid::keyboard::key_3				] = raw_keyboard_t::hid_index_key_3;
			_hid_to_index[0xFF & hid::keyboard::key_4				] = raw_keyboard_t::hid_index_key_4;
			_hid_to_index[0xFF & hid::keyboard::key_5				] = raw_keyboard_t::hid_index_key_5;
			_hid_to_index[0xFF & hid::keyboard::key_6				] = raw_keyboard_t::hid_index_key_6;
			_hid_to_index[0xFF & hid::keyboard::key_7				] = raw_keyboard_t::hid_index_key_7;
			_hid_to_index[0xFF & hid::keyboard::key_8				] = raw_keyboard_t::hid_index_key_8;
			_hid_to_index[0xFF & hid::keyboard::key_9				] = raw_keyboard_t::hid_index_key_9;
			_hid_to_index[0xFF & hid::keyboard::key_0				] = raw_keyboard_t::hid_index_key_0;
			_hid_to_index[0xFF & hid::keyboard::key_enter			] = raw_keyboard_t::hid_index_key_enter;
			_hid_to_index[0xFF & hid::keyboard::key_escape			] = raw_keyboard_t::hid_index_key_escape;
			_hid_to_index[0xFF & hid::keyboard::key_backspace		] = raw_keyboard_t::hid_index_key_backspace;
			_hid_to_index[0xFF & hid::keyboard::key_tab				] = raw_keyboard_t::hid_index_key_tab;
			_hid_to_index[0xFF & hid::keyboard::key_space			] = raw_keyboard_t::hid_index_key_space;
			_hid_to_index[0xFF & hid::keyboard::key_minus			] = raw_keyboard_t::hid_index_key_minus;
			_hid_to_index[0xFF & hid::keyboard::key_equals			] = raw_keyboard_t::hid_index_key_equals;
			_hid_to_index[0xFF & hid::keyboard::key_bracket_left	] = raw_keyboard_t::hid_index_key_bracket_left;
			_hid_to_index[0xFF & hid::keyboard::key_bracket_right	] = raw_keyboard_t::hid_index_key_bracket_right;
			_hid_to_index[0xFF & hid::keyboard::key_backslash		] = raw_keyboard_t::hid_index_key_backslash;
			_hid_to_index[0xFF & hid::keyboard::key_semicolon		] = raw_keyboard_t::hid_index_key_semicolon;
			_hid_to_index[0xFF & hid::keyboard::key_quote			] = raw_keyboard_t::hid_index_key_quote;
			_hid_to_index[0xFF & hid::keyboard::key_tilde			] = raw_keyboard_t::hid_index_key_tilde;
			_hid_to_index[0xFF & hid::keyboard::key_comma			] = raw_keyboard_t::hid_index_key_comma;
			_hid_to_index[0xFF & hid::keyboard::key_period			] = raw_keyboard_t::hid_index_key_period;
			_hid_to_index[0xFF & hid::keyboard::key_slash			] = raw_keyboard_t::hid_index_key_slash;
			_hid_to_index[0xFF & hid::keyboard::key_caps_lock		] = raw_keyboard_t::hid_index_key_caps_lock;
			_hid_to_index[0xFF & hid::keyboard::key_F1				] = raw_keyboard_t::hid_index_key_F1;
			_hid_to_index[0xFF & hid::keyboard::key_F2				] = raw_keyboard_t::hid_index_key_F2;
			_hid_to_index[0xFF & hid::keyboard::key_F3				] = raw_keyboard_t::hid_index_key_F3;
			_hid_to_index[0xFF & hid::keyboard::key_F4				] = raw_keyboard_t::hid_index_key_F4;
			_hid_to_index[0xFF & hid::keyboard::key_F5				] = raw_keyboard_t::hid_index_key_F5;
			_hid_to_index[0xFF & hid::keyboard::key_F6				] = raw_keyboard_t::hid_index_key_F6;
			_hid_to_index[0xFF & hid::keyboard::key_F7				] = raw_keyboard_t::hid_index_key_F7;
			_hid_to_index[0xFF & hid::keyboard::key_F8				] = raw_keyboard_t::hid_index_key_F8;
			_hid_to_index[0xFF & hid::keyboard::key_F9				] = raw_keyboard_t::hid_index_key_F9;
			_hid_to_index[0xFF & hid::keyboard::key_F10				] = raw_keyboard_t::hid_index_key_F10;
			_hid_to_index[0xFF & hid::keyboard::key_F11				] = raw_keyboard_t::hid_index_key_F11;
			_hid_to_index[0xFF & hid::keyboard::key_F12				] = raw_keyboard_t::hid_index_key_F12;
			_hid_to_index[0xFF & hid::keyboard::key_print_screen	] = raw_keyboard_t::hid_index_key_print_screen;
			_hid_to_index[0xFF & hid::keyboard::key_scroll_lock		] = raw_keyboard_t::hid_index_key_scroll_lock;
			_hid_to_index[0xFF & hid::keyboard::key_pause			] = raw_keyboard_t::hid_index_key_pause;
			_hid_to_index[0xFF & hid::keyboard::key_insert			] = raw_keyboard_t::hid_index_key_insert;
			_hid_to_index[0xFF & hid::keyboard::key_home			] = raw_keyboard_t::hid_index_key_home;
			_hid_to_index[0xFF & hid::keyboard::key_page_up			] = raw_keyboard_t::hid_index_key_page_up;
			_hid_to_index[0xFF & hid::keyboard::key_delete			] = raw_keyboard_t::hid_index_key_delete;
			_hid_to_index[0xFF & hid::keyboard::key_end				] = raw_keyboard_t::hid_index_key_end;
			_hid_to_index[0xFF & hid::keyboard::key_page_down		] = raw_keyboard_t::hid_index_key_page_down;
			_hid_to_index[0xFF & hid::keyboard::key_right			] = raw_keyboard_t::hid_index_key_right;
			_hid_to_index[0xFF & hid::keyboard::key_left			] = raw_keyboard_t::hid_index_key_left;
			_hid_to_index[0xFF & hid::keyboard::key_down			] = raw_keyboard_t::hid_index_key_down;
			_hid_to_index[0xFF & hid::keyboard::key_up				] = raw_keyboard_t::hid_index_key_up;
			_hid_to_index[0xFF & hid::keyboard::key_num_lock		] = raw_keyboard_t::hid_index_key_num_lock;
			_hid_to_index[0xFF & hid::keyboard::key_num_divide		] = raw_keyboard_t::hid_index_key_num_divide;
			_hid_to_index[0xFF & hid::keyboard::key_num_multiply	] = raw_keyboard_t::hid_index_key_num_multiply;
			_hid_to_index[0xFF & hid::keyboard::key_num_minus		] = raw_keyboard_t::hid_index_key_num_minus;
			_hid_to_index[0xFF & hid::keyboard::key_num_plus		] = raw_keyboard_t::hid_index_key_num_plus;
			_hid_to_index[0xFF & hid::keyboard::key_num_enter		] = raw_keyboard_t::hid_index_key_num_enter;
			_hid_to_index[0xFF & hid::keyboard::key_num_1			] = raw_keyboard_t::hid_index_key_num_1;
			_hid_to_index[0xFF & hid::keyboard::key_num_2			] = raw_keyboard_t::hid_index_key_num_2;
			_hid_to_index[0xFF & hid::keyboard::key_num_3			] = raw_keyboard_t::hid_index_key_num_3;
			_hid_to_index[0xFF & hid::keyboard::key_num_4			] = raw_keyboard_t::hid_index_key_num_4;
			_hid_to_index[0xFF & hid::keyboard::key_num_5			] = raw_keyboard_t::hid_index_key_num_5;
			_hid_to_index[0xFF & hid::keyboard::key_num_6			] = raw_keyboard_t::hid_index_key_num_6;
			_hid_to_index[0xFF & hid::keyboard::key_num_7			] = raw_keyboard_t::hid_index_key_num_7;
			_hid_to_index[0xFF & hid::keyboard::key_num_8			] = raw_keyboard_t::hid_index_key_num_8;
			_hid_to_index[0xFF & hid::keyboard::key_num_9			] = raw_keyboard_t::hid_index_key_num_9;
			_hid_to_index[0xFF & hid::keyboard::key_num_0			] = raw_keyboard_t::hid_index_key_num_0;
			_hid_to_index[0xFF & hid::keyboard::key_num_period		] = raw_keyboard_t::hid_index_key_num_period;
			_hid_to_index[0xFF & hid::keyboard::key_backslash_102	] = raw_keyboard_t::hid_index_key_backslash_102;
			_hid_to_index[0xFF & hid::keyboard::key_F13				] = raw_keyboard_t::hid_index_key_F13;
			_hid_to_index[0xFF & hid::keyboard::key_F14				] = raw_keyboard_t::hid_index_key_F14;
			_hid_to_index[0xFF & hid::keyboard::key_F15				] = raw_keyboard_t::hid_index_key_F15;
			_hid_to_index[0xFF & hid::keyboard::key_F16				] = raw_keyboard_t::hid_index_key_F16;
			_hid_to_index[0xFF & hid::keyboard::key_F17				] = raw_keyboard_t::hid_index_key_F17;
			_hid_to_index[0xFF & hid::keyboard::key_F18				] = raw_keyboard_t::hid_index_key_F18;
			_hid_to_index[0xFF & hid::keyboard::key_F19				] = raw_keyboard_t::hid_index_key_F19;
			_hid_to_index[0xFF & hid::keyboard::key_F20				] = raw_keyboard_t::hid_index_key_F20;
			_hid_to_index[0xFF & hid::keyboard::key_F21				] = raw_keyboard_t::hid_index_key_F21;
			_hid_to_index[0xFF & hid::keyboard::key_F22				] = raw_keyboard_t::hid_index_key_F22;
			_hid_to_index[0xFF & hid::keyboard::key_F23				] = raw_keyboard_t::hid_index_key_F23;
			_hid_to_index[0xFF & hid::keyboard::key_F24				] = raw_keyboard_t::hid_index_key_F24;
			_hid_to_index[0xFF & hid::keyboard::key_help			] = raw_keyboard_t::hid_index_key_help;
			_hid_to_index[0xFF & hid::keyboard::key_menu			] = raw_keyboard_t::hid_index_key_menu;
			_hid_to_index[0xFF & hid::keyboard::key_control_left	] = raw_keyboard_t::hid_index_key_control_left;
			_hid_to_index[0xFF & hid::keyboard::key_shift_left		] = raw_keyboard_t::hid_index_key_shift_left;
			_hid_to_index[0xFF & hid::keyboard::key_alt_left		] = raw_keyboard_t::hid_index_key_alt_left;
			_hid_to_index[0xFF & hid::keyboard::key_win_left		] = raw_keyboard_t::hid_index_key_win_left;
			_hid_to_index[0xFF & hid::keyboard::key_control_right	] = raw_keyboard_t::hid_index_key_control_right;
			_hid_to_index[0xFF & hid::keyboard::key_shift_right		] = raw_keyboard_t::hid_index_key_shift_right;
			_hid_to_index[0xFF & hid::keyboard::key_alt_right		] = raw_keyboard_t::hid_index_key_alt_right;
			_hid_to_index[0xFF & hid::keyboard::key_win_right		] = raw_keyboard_t::hid_index_key_win_right;

		}

		hid::usage_t sc_to_key(scancode_ex_t ex, scancode_t sc)
		{
			return _sc_to_hid[ex][sc];
		}

		hid::index_t get_index(hid::usage_t usage)
		{
			return _hid_to_index[usage & 0xFF];
		}

		int key_to_sc(hid::usage_t key)
		{
			switch (key)
			{
			case hid::keyboard::key_escape:			return sc_escape;
			case hid::keyboard::key_1:				return sc_1;
			case hid::keyboard::key_2:				return sc_2;
			case hid::keyboard::key_3:				return sc_3;
			case hid::keyboard::key_4:				return sc_4;
			case hid::keyboard::key_5:				return sc_5;
			case hid::keyboard::key_6:				return sc_6;
			case hid::keyboard::key_7:				return sc_7;
			case hid::keyboard::key_8:				return sc_8;
			case hid::keyboard::key_9:				return sc_9;
			case hid::keyboard::key_0:				return sc_0;
			case hid::keyboard::key_minus:			return sc_minus;
			case hid::keyboard::key_equals:			return sc_equals;
			case hid::keyboard::key_backspace:		return sc_backspace;
			case hid::keyboard::key_tab:			return sc_tab;
			case hid::keyboard::key_Q:				return sc_q;
			case hid::keyboard::key_W:				return sc_w;
			case hid::keyboard::key_E:				return sc_e;
			case hid::keyboard::key_R:				return sc_r;
			case hid::keyboard::key_T:				return sc_t;
			case hid::keyboard::key_Y:				return sc_y;
			case hid::keyboard::key_U:				return sc_u;
			case hid::keyboard::key_I:				return sc_i;
			case hid::keyboard::key_O:				return sc_o;
			case hid::keyboard::key_P:				return sc_p;
			case hid::keyboard::key_bracket_left:	return sc_bracket_left;
			case hid::keyboard::key_bracket_right:	return sc_bracket_right;
			case hid::keyboard::key_enter:			return sc_enter;
			case hid::keyboard::key_control_left:	return sc_control_left;
			case hid::keyboard::key_A:				return sc_a;
			case hid::keyboard::key_S:				return sc_s;
			case hid::keyboard::key_D:				return sc_d;
			case hid::keyboard::key_F:				return sc_f;
			case hid::keyboard::key_G:				return sc_g;
			case hid::keyboard::key_H:				return sc_h;
			case hid::keyboard::key_J:				return sc_j;
			case hid::keyboard::key_K:				return sc_k;
			case hid::keyboard::key_L:				return sc_l;
			case hid::keyboard::key_semicolon:		return sc_semicolon;
			case hid::keyboard::key_quote:			return sc_quote;
			case hid::keyboard::key_tilde:			return sc_tilde;
			case hid::keyboard::key_shift_left:		return sc_shift_left;
			case hid::keyboard::key_backslash:		return sc_backslash;
			case hid::keyboard::key_Z:				return sc_z;
			case hid::keyboard::key_X:				return sc_x;
			case hid::keyboard::key_C:				return sc_c;
			case hid::keyboard::key_V:				return sc_v;
			case hid::keyboard::key_B:				return sc_b;
			case hid::keyboard::key_N:				return sc_n;
			case hid::keyboard::key_M:				return sc_m;
			case hid::keyboard::key_comma:			return sc_comma;
			case hid::keyboard::key_period:			return sc_preiod;
			case hid::keyboard::key_slash:			return sc_slash;
			case hid::keyboard::key_shift_right:	return sc_shift_right;
			case hid::keyboard::key_num_multiply:	return sc_num_multiply;
			case hid::keyboard::key_alt_left:		return sc_alt_left;
			case hid::keyboard::key_space:			return sc_space;
			case hid::keyboard::key_caps_lock:		return sc_capslock;
			case hid::keyboard::key_F1:				return sc_f1;
			case hid::keyboard::key_F2:				return sc_f2;
			case hid::keyboard::key_F3:				return sc_f3;
			case hid::keyboard::key_F4:				return sc_f4;
			case hid::keyboard::key_F5:				return sc_f5;
			case hid::keyboard::key_F6:				return sc_f6;
			case hid::keyboard::key_F7:				return sc_f7;
			case hid::keyboard::key_F8:				return sc_f8;
			case hid::keyboard::key_F9:				return sc_f9;
			case hid::keyboard::key_F10:			return sc_f10;
			case hid::keyboard::key_num_lock:		return sc_numlock | 0x100; //set extended bit for true numlock
			case hid::keyboard::key_scroll_lock:	return sc_scrolllock;
			case hid::keyboard::key_num_7:			return sc_num_7;
			case hid::keyboard::key_num_8:			return sc_num_8;
			case hid::keyboard::key_num_9:			return sc_num_9;
			case hid::keyboard::key_num_minus:		return sc_num_minus;
			case hid::keyboard::key_num_4:			return sc_num_4;
			case hid::keyboard::key_num_5:			return sc_num_5;
			case hid::keyboard::key_num_6:			return sc_num_6;
			case hid::keyboard::key_num_plus:		return sc_num_plus;
			case hid::keyboard::key_num_1:			return sc_num_1;
			case hid::keyboard::key_num_2:			return sc_num_2;
			case hid::keyboard::key_num_3:			return sc_num_3;
			case hid::keyboard::key_num_0:			return sc_num_0;
			case hid::keyboard::key_num_period:		return sc_num_period;
				//case hid::keyboard::key_print_screen:	return sc_printscreen_v2; //v2 is not compatible
			case hid::keyboard::key_backslash_102:	return sc_backslash_102;
			case hid::keyboard::key_F11:			return sc_f11;
			case hid::keyboard::key_F12:			return sc_f12;
			case hid::keyboard::key_help:			return sc_help;
			case hid::keyboard::key_F13:			return sc_f13;
			case hid::keyboard::key_F14:			return sc_f14;
			case hid::keyboard::key_F15:			return sc_f15;
			case hid::keyboard::key_F16:			return sc_f16;
			case hid::keyboard::key_F17:			return sc_f17;
			case hid::keyboard::key_F18:			return sc_f18;
			case hid::keyboard::key_F19:			return sc_f19;
			case hid::keyboard::key_F20:			return sc_f20;
			case hid::keyboard::key_F21:			return sc_f21;
			case hid::keyboard::key_F22:			return sc_f22;
			case hid::keyboard::key_F23:			return sc_f23;
			case hid::keyboard::key_F24:			return sc_f24;

			case hid::keyboard::key_num_enter:		return 0x100 | sc_numpad_enter;
			case hid::keyboard::key_control_right:	return 0x100 | sc_control_right;
			case hid::keyboard::key_num_divide:		return 0x100 | sc_numpad_divide;
			case hid::keyboard::key_print_screen:	return 0x100 | sc_printscreen_v1;
			case hid::keyboard::key_alt_right:		return 0x100 | sc_alt_right;
			case hid::keyboard::key_pause:			return sc_pause_v1_s2; //this is the true pause
			case hid::keyboard::key_home:			return 0x100 | sc_home;
			case hid::keyboard::key_up:				return 0x100 | sc_arrow_up;
			case hid::keyboard::key_page_up:		return 0x100 | sc_page_up;
			case hid::keyboard::key_left:			return 0x100 | sc_arrow_left;
			case hid::keyboard::key_right:			return 0x100 | sc_arrow_right;
			case hid::keyboard::key_end:			return 0x100 | sc_end;
			case hid::keyboard::key_down:			return 0x100 | sc_arrow_down;
			case hid::keyboard::key_page_down:		return 0x100 | sc_page_down;
			case hid::keyboard::key_insert:			return 0x100 | sc_insert;
			case hid::keyboard::key_delete:			return 0x100 | sc_delete;
			case hid::keyboard::key_win_left:		return 0x100 | sc_win_left;
			case hid::keyboard::key_win_right:		return 0x100 | sc_win_right;
			case hid::keyboard::key_menu:			return 0x100 | sc_menu;
			default:
				return 0;
			}
		}
	};

	namespace g
	{
		static raw_keyboard_mapper_t mapper;
	}

	//--------------------------------------------------
	error_t hid::keyboard::get_key_name(hid::usage_t key, string8_t& utf8)
	{
		int sc = g::mapper.key_to_sc(key);
		unsigned int result = 0;
		unsigned int lParam = sc;
		lParam = lParam << 16;
		
		wchar_t	buffer_w[128];
		result = GetKeyNameTextW(lParam, buffer_w, (int)u::array_size(buffer_w));
		if (result == 0)
			return err_unknown;
	
		return wchar_to_utf8(buffer_w, utf8);
	}

	//--------------------------------------------------
	hid::usage_t hid::keyboard::vk_to_key(virtual_key_t vk)
	{
		/*
		* VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
		* 0x40 : unassigned
		* VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
		*/

		if ('1' <= vk && '9' >= vk)
			return hid::keyboard::key_1 + (vk - '1');
		if ('0' == vk)
			return hid::keyboard::key_0;

		if ('A' <= vk && 'Z' >= vk)
			return hid::keyboard::key_A + (vk - 'A');
	
		switch(vk)
		{
		//case VK_BACK:           return sc_back;
		case VK_TAB:           return hid::keyboard::key_tab;
	
		//case VK_CLEAR:          return hid::keyboard::key_clear;
		case VK_RETURN:        return hid::keyboard::key_enter;
		
		case VK_SHIFT:         return hid::keyboard::key_shift_left;
		case VK_CONTROL:       return hid::keyboard::key_control_left;
		case VK_MENU:          return hid::keyboard::key_alt_left;
		case VK_PAUSE:         return hid::keyboard::key_pause;
		/*case VK_CAPITAL:        0x14
		case 
		case VK_KANA           0x15
		case VK_HANGEUL        0x15  // old name - should be here for compatibility
		case VK_HANGUL         0x15
		case VK_JUNJA          0x17
		case VK_FINAL          0x18
		case VK_HANJA          0x19
		case VK_KANJI          0x19
		case */
		case VK_ESCAPE:        return hid::keyboard::key_escape;
	
		/*case VK_CONVERT        0x1C
		case VK_NONCONVERT     0x1D
		case VK_ACCEPT         0x1E
		case VK_MODECHANGE     0x1F*/
		
		case VK_SPACE:         return hid::keyboard::key_space;
		//case VK_PRIOR          0x21
		//case VK_NEXT           0x22
		case VK_END:           return hid::keyboard::key_end;
		case VK_HOME:          return hid::keyboard::key_home;
		case VK_LEFT:          return hid::keyboard::key_left;
		case VK_UP:            return hid::keyboard::key_up;
		case VK_RIGHT:         return hid::keyboard::key_right;
		case VK_DOWN:          return hid::keyboard::key_down;
		//case VK_SELECT         0x29
		//case VK_PRINT          0x2A
		//case VK_EXECUTE        0x2B
		case VK_SNAPSHOT:      return hid::keyboard::key_print_screen;
		case VK_INSERT:        return hid::keyboard::key_insert;
		case VK_DELETE:        return hid::keyboard::key_delete;
		case VK_HELP:          return hid::keyboard::key_help;
		 
		/*
		case * VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
		case * 0x40 : unassigned
		case * VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
		case */
		 
		case VK_LWIN:          return hid::keyboard::key_win_left;
		case VK_RWIN:          return hid::keyboard::key_win_right;
		case VK_APPS:          return hid::keyboard::key_menu;
		
		/*
		case * 0x5E : reserved
		case */
		
		//case VK_SLEEP          0x5F
		 
		case VK_NUMPAD0:       return hid::keyboard::key_num_0;
		case VK_NUMPAD1:       return hid::keyboard::key_num_1;
		case VK_NUMPAD2:       return hid::keyboard::key_num_2; 
		case VK_NUMPAD3:       return hid::keyboard::key_num_3; 
		case VK_NUMPAD4:       return hid::keyboard::key_num_4; 
		case VK_NUMPAD5:       return hid::keyboard::key_num_5; 
		case VK_NUMPAD6:       return hid::keyboard::key_num_6; 
		case VK_NUMPAD7:       return hid::keyboard::key_num_7; 
		case VK_NUMPAD8:       return hid::keyboard::key_num_8; 
		case VK_NUMPAD9:       return hid::keyboard::key_num_9; 
		case VK_MULTIPLY:      return hid::keyboard::key_num_multiply;
		case VK_ADD:           return hid::keyboard::key_num_plus;
		//case VK_SEPARATOR:   return hid::keyboard::key_num_period;
		case VK_SUBTRACT:      return hid::keyboard::key_num_minus;
		case VK_DECIMAL:       return hid::keyboard::key_num_period;
		case VK_DIVIDE:        return hid::keyboard::key_num_divide;
		case VK_F1:            return hid::keyboard::key_F1;
		case VK_F2:            return hid::keyboard::key_F2;
		case VK_F3:            return hid::keyboard::key_F3;
		case VK_F4:            return hid::keyboard::key_F4;
		case VK_F5:            return hid::keyboard::key_F5;
		case VK_F6:            return hid::keyboard::key_F6;
		case VK_F7:            return hid::keyboard::key_F7;
		case VK_F8:            return hid::keyboard::key_F8;
		case VK_F9:            return hid::keyboard::key_F9;
		case VK_F10:           return hid::keyboard::key_F10;
		case VK_F11:           return hid::keyboard::key_F11;
		case VK_F12:           return hid::keyboard::key_F12;
		case VK_F13:           return hid::keyboard::key_F13;
		case VK_F14:           return hid::keyboard::key_F14;
		case VK_F15:           return hid::keyboard::key_F15;
		case VK_F16:           return hid::keyboard::key_F16;
		case VK_F17:           return hid::keyboard::key_F17;
		case VK_F18:           return hid::keyboard::key_F18;
		case VK_F19:           return hid::keyboard::key_F19;
		case VK_F20:           return hid::keyboard::key_F20;
		case VK_F21:           return hid::keyboard::key_F21;
		case VK_F22:           return hid::keyboard::key_F22;
		case VK_F23:           return hid::keyboard::key_F23;
		case VK_F24:           return hid::keyboard::key_F24;
		/*
		case * 0x88 - 0x8F : unassigned
		case */
		case VK_NUMLOCK:       return hid::keyboard::key_num_lock;
		case VK_SCROLL:        return hid::keyboard::key_scroll_lock;
		default:			   return hid::keyboard::key_none;
		}
	}

	//--------------------------------------------------
	raw_keyboard_t::raw_keyboard_t(HANDLE device, RID_DEVICE_INFO_KEYBOARD& info)
		: _device_handle(device)
		, _device_info(info)
		, _is_pause_c1_s1_read(false)
	{
		device_t::device = static_cast<hid::device_id>(reinterpret_cast<uintptr_t>(device));
		device_t::usage = hid::keyboard::device;
		device_t::appdata = nullptr;
	}

	//--------------------------------------------------
	raw_keyboard_t::~raw_keyboard_t()
	{
		
	}

	//--------------------------------------------------
	error_t raw_keyboard_t::create(input_t* input)
	{
		device_t::appdata = input->on_device_added(*this);
			
		return err_ok;
	}

	//--------------------------------------------------
	void raw_keyboard_t::destroy(input_t* input)
	{
		input->on_device_removed(*this);
	}

	//--------------------------------------------------
	void raw_keyboard_t::on_input(HRAWINPUT h_raw_input, input_t* input)
	{
		hid::report_t report;

		RAWINPUT rawinput;
		UINT size = sizeof(RAWINPUT);
		UINT res = GetRawInputData(h_raw_input, RID_INPUT, &rawinput, &size, sizeof(RAWINPUTHEADER));
		if (res == ((UINT)-1) || (res == 0) || rawinput.header.dwType != RIM_TYPEKEYBOARD)
			return;

		report.value_new = ((rawinput.data.keyboard.Flags & RI_KEY_BREAK) == RI_KEY_MAKE);
		report.value_old = ((rawinput.data.keyboard.Flags & RI_KEY_BREAK) != RI_KEY_MAKE);

		bool isE0 = ((rawinput.data.keyboard.Flags & RI_KEY_E0) != 0);
		bool isE1 = ((rawinput.data.keyboard.Flags & RI_KEY_E1) != 0);

		scancode_ex_t ex = sc_ex_00;
		if (isE0)
			ex |= sc_ex_E0;
		if (isE1)
			ex |= sc_ex_E1;

		scancode_t sc = static_cast<scancode_t>(rawinput.data.keyboard.MakeCode);
		if (rawinput.data.keyboard.MakeCode != sc)
			return;		
		
		/* The pause scancode is in 2 parts: a WM_INPUT with 0xE11D and one WM_INPUT with 0x45. */
		if (_is_pause_c1_s1_read) //handle pause chain sequence
		{
			if (sc_pause_v1_s2 == sc && sc_ex_00 == ex)
			{
				sc = sc_pause_v2; //v2 is mapped directly
				ex = sc_ex_E0;
			}
			_is_pause_c1_s1_read = false;
		}
		else if (sc_pause_v1_s1 == sc && sc_ex_E1 & ex) //init pause chain sequence
		{
			_is_pause_c1_s1_read = true;
			return;
		}

		//we support only one E1 key
		if (sc_ex_E1 & ex || sc_ignore_s1 == sc || sc_ignore_s2 == sc || sc_ignore_s3 == sc || sc_ignore_s4 == sc)
			return;

		//mapping to index
		report.index = g::mapper.get_index(g::mapper.sc_to_key(ex, sc));
		
		if (hid_index_count == report.index)
			return;

		input->on_input(&report, 1, *this);
	}

	//--------------------------------------------------
	error_t raw_keyboard_t::get_hid_caps(hid::device_caps_t& caps)
	{	
		if (nullptr == caps.feature_array)
		{
			caps.range_count = 0;
			caps.button_count = hid_index_count;
			caps.stick_count = 0;
			caps.lever_count = 0;
			caps.hat_count = 0;
			caps.feature_count = hid_index_count;

		}
		else
		{
			if (hid_index_count != caps.feature_count)
				return err_invalid_parameter;

			for (hid::index_t i = 0; i < hid_index_count; ++i)
			{
				caps.feature_array[i].index = i;
				caps.feature_array[i].feature = hid::feature_t::button;
			}

			caps.feature_array[hid_index_key_A].usage = hid::keyboard::key_A;
			caps.feature_array[hid_index_key_B].usage = hid::keyboard::key_B;
			caps.feature_array[hid_index_key_C].usage = hid::keyboard::key_C;
			caps.feature_array[hid_index_key_D].usage = hid::keyboard::key_D;
			caps.feature_array[hid_index_key_E].usage = hid::keyboard::key_E;
			caps.feature_array[hid_index_key_F].usage = hid::keyboard::key_F;
			caps.feature_array[hid_index_key_G].usage = hid::keyboard::key_G;
			caps.feature_array[hid_index_key_H].usage = hid::keyboard::key_H;
			caps.feature_array[hid_index_key_I].usage = hid::keyboard::key_I;
			caps.feature_array[hid_index_key_J].usage = hid::keyboard::key_J;
			caps.feature_array[hid_index_key_K].usage = hid::keyboard::key_K;
			caps.feature_array[hid_index_key_L].usage = hid::keyboard::key_L;
			caps.feature_array[hid_index_key_M].usage = hid::keyboard::key_M;
			caps.feature_array[hid_index_key_N].usage = hid::keyboard::key_N;
			caps.feature_array[hid_index_key_O].usage = hid::keyboard::key_O;
			caps.feature_array[hid_index_key_P].usage = hid::keyboard::key_P;
			caps.feature_array[hid_index_key_Q].usage = hid::keyboard::key_Q;
			caps.feature_array[hid_index_key_R].usage = hid::keyboard::key_R;
			caps.feature_array[hid_index_key_S].usage = hid::keyboard::key_S;
			caps.feature_array[hid_index_key_T].usage = hid::keyboard::key_T;
			caps.feature_array[hid_index_key_U].usage = hid::keyboard::key_U;
			caps.feature_array[hid_index_key_V].usage = hid::keyboard::key_V;
			caps.feature_array[hid_index_key_W].usage = hid::keyboard::key_W;
			caps.feature_array[hid_index_key_X].usage = hid::keyboard::key_X;
			caps.feature_array[hid_index_key_Y].usage = hid::keyboard::key_Y;
			caps.feature_array[hid_index_key_Z].usage = hid::keyboard::key_Z;
			caps.feature_array[hid_index_key_1].usage = hid::keyboard::key_1;
			caps.feature_array[hid_index_key_2].usage = hid::keyboard::key_2;
			caps.feature_array[hid_index_key_3].usage = hid::keyboard::key_3;
			caps.feature_array[hid_index_key_4].usage = hid::keyboard::key_4;
			caps.feature_array[hid_index_key_5].usage = hid::keyboard::key_5;
			caps.feature_array[hid_index_key_6].usage = hid::keyboard::key_6;
			caps.feature_array[hid_index_key_7].usage = hid::keyboard::key_7;
			caps.feature_array[hid_index_key_8].usage = hid::keyboard::key_8;
			caps.feature_array[hid_index_key_9].usage = hid::keyboard::key_9;
			caps.feature_array[hid_index_key_0].usage = hid::keyboard::key_0;
			caps.feature_array[hid_index_key_enter].usage = hid::keyboard::key_enter;
			caps.feature_array[hid_index_key_escape].usage = hid::keyboard::key_escape;
			caps.feature_array[hid_index_key_backspace].usage = hid::keyboard::key_backspace;
			caps.feature_array[hid_index_key_tab].usage = hid::keyboard::key_tab;
			caps.feature_array[hid_index_key_space].usage = hid::keyboard::key_space;
			caps.feature_array[hid_index_key_minus].usage = hid::keyboard::key_minus;
			caps.feature_array[hid_index_key_equals].usage = hid::keyboard::key_equals;
			caps.feature_array[hid_index_key_bracket_left].usage = hid::keyboard::key_bracket_left;
			caps.feature_array[hid_index_key_bracket_right].usage = hid::keyboard::key_bracket_right;
			caps.feature_array[hid_index_key_backslash].usage = hid::keyboard::key_backslash;
			caps.feature_array[hid_index_key_semicolon].usage = hid::keyboard::key_semicolon;
			caps.feature_array[hid_index_key_quote].usage = hid::keyboard::key_quote;
			caps.feature_array[hid_index_key_tilde].usage = hid::keyboard::key_tilde;
			caps.feature_array[hid_index_key_comma].usage = hid::keyboard::key_comma;
			caps.feature_array[hid_index_key_period].usage = hid::keyboard::key_period;
			caps.feature_array[hid_index_key_slash].usage = hid::keyboard::key_slash;
			caps.feature_array[hid_index_key_caps_lock].usage = hid::keyboard::key_caps_lock;
			caps.feature_array[hid_index_key_F1].usage = hid::keyboard::key_F1;
			caps.feature_array[hid_index_key_F2].usage = hid::keyboard::key_F2;
			caps.feature_array[hid_index_key_F3].usage = hid::keyboard::key_F3;
			caps.feature_array[hid_index_key_F4].usage = hid::keyboard::key_F4;
			caps.feature_array[hid_index_key_F5].usage = hid::keyboard::key_F5;
			caps.feature_array[hid_index_key_F6].usage = hid::keyboard::key_F6;
			caps.feature_array[hid_index_key_F7].usage = hid::keyboard::key_F7;
			caps.feature_array[hid_index_key_F8].usage = hid::keyboard::key_F8;
			caps.feature_array[hid_index_key_F9].usage = hid::keyboard::key_F9;
			caps.feature_array[hid_index_key_F10].usage = hid::keyboard::key_F10;
			caps.feature_array[hid_index_key_F11].usage = hid::keyboard::key_F11;
			caps.feature_array[hid_index_key_F12].usage = hid::keyboard::key_F12;
			caps.feature_array[hid_index_key_print_screen].usage = hid::keyboard::key_print_screen;
			caps.feature_array[hid_index_key_scroll_lock].usage = hid::keyboard::key_scroll_lock;
			caps.feature_array[hid_index_key_pause].usage = hid::keyboard::key_pause;
			caps.feature_array[hid_index_key_insert].usage = hid::keyboard::key_insert;
			caps.feature_array[hid_index_key_home].usage = hid::keyboard::key_home;
			caps.feature_array[hid_index_key_page_up].usage = hid::keyboard::key_page_up;
			caps.feature_array[hid_index_key_delete].usage = hid::keyboard::key_delete;
			caps.feature_array[hid_index_key_end].usage = hid::keyboard::key_end;
			caps.feature_array[hid_index_key_page_down].usage = hid::keyboard::key_page_down;
			caps.feature_array[hid_index_key_right].usage = hid::keyboard::key_right;
			caps.feature_array[hid_index_key_left].usage = hid::keyboard::key_left;
			caps.feature_array[hid_index_key_down].usage = hid::keyboard::key_down;
			caps.feature_array[hid_index_key_up].usage = hid::keyboard::key_up;
			caps.feature_array[hid_index_key_num_lock].usage = hid::keyboard::key_num_lock;
			caps.feature_array[hid_index_key_num_divide].usage = hid::keyboard::key_num_divide;
			caps.feature_array[hid_index_key_num_multiply].usage = hid::keyboard::key_num_multiply;
			caps.feature_array[hid_index_key_num_minus].usage = hid::keyboard::key_num_minus;
			caps.feature_array[hid_index_key_num_plus].usage = hid::keyboard::key_num_plus;
			caps.feature_array[hid_index_key_num_enter].usage = hid::keyboard::key_num_enter;
			caps.feature_array[hid_index_key_num_1].usage = hid::keyboard::key_num_1;
			caps.feature_array[hid_index_key_num_2].usage = hid::keyboard::key_num_2;
			caps.feature_array[hid_index_key_num_3].usage = hid::keyboard::key_num_3;
			caps.feature_array[hid_index_key_num_4].usage = hid::keyboard::key_num_4;
			caps.feature_array[hid_index_key_num_5].usage = hid::keyboard::key_num_5;
			caps.feature_array[hid_index_key_num_6].usage = hid::keyboard::key_num_6;
			caps.feature_array[hid_index_key_num_7].usage = hid::keyboard::key_num_7;
			caps.feature_array[hid_index_key_num_8].usage = hid::keyboard::key_num_8;
			caps.feature_array[hid_index_key_num_9].usage = hid::keyboard::key_num_9;
			caps.feature_array[hid_index_key_num_0].usage = hid::keyboard::key_num_0;
			caps.feature_array[hid_index_key_num_period].usage = hid::keyboard::key_num_period;
			caps.feature_array[hid_index_key_backslash_102].usage = hid::keyboard::key_backslash_102;
			caps.feature_array[hid_index_key_F13].usage = hid::keyboard::key_F13;
			caps.feature_array[hid_index_key_F14].usage = hid::keyboard::key_F14;
			caps.feature_array[hid_index_key_F15].usage = hid::keyboard::key_F15;
			caps.feature_array[hid_index_key_F16].usage = hid::keyboard::key_F16;
			caps.feature_array[hid_index_key_F17].usage = hid::keyboard::key_F17;
			caps.feature_array[hid_index_key_F18].usage = hid::keyboard::key_F18;
			caps.feature_array[hid_index_key_F19].usage = hid::keyboard::key_F19;
			caps.feature_array[hid_index_key_F20].usage = hid::keyboard::key_F20;
			caps.feature_array[hid_index_key_F21].usage = hid::keyboard::key_F21;
			caps.feature_array[hid_index_key_F22].usage = hid::keyboard::key_F22;
			caps.feature_array[hid_index_key_F23].usage = hid::keyboard::key_F23;
			caps.feature_array[hid_index_key_F24].usage = hid::keyboard::key_F24;
			caps.feature_array[hid_index_key_help].usage = hid::keyboard::key_help;
			caps.feature_array[hid_index_key_menu].usage = hid::keyboard::key_menu;
			caps.feature_array[hid_index_key_control_left].usage = hid::keyboard::key_control_left;
			caps.feature_array[hid_index_key_shift_left].usage = hid::keyboard::key_shift_left;
			caps.feature_array[hid_index_key_alt_left].usage = hid::keyboard::key_alt_left;
			caps.feature_array[hid_index_key_win_left].usage = hid::keyboard::key_win_left;
			caps.feature_array[hid_index_key_control_right].usage = hid::keyboard::key_control_right;
			caps.feature_array[hid_index_key_shift_right].usage = hid::keyboard::key_shift_right;
			caps.feature_array[hid_index_key_alt_right].usage = hid::keyboard::key_alt_right;
			caps.feature_array[hid_index_key_win_right].usage = hid::keyboard::key_win_right;

		}

		return err_ok;
	}

} }