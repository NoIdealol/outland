#include "pch.h"
#include "main_win.h"

#if O_CRT
/*
//--------------------------------------------------
extern "C" int mainCRTStartup()
{
	ExitProcess(crt_main(0, nullptr));
	return 0;
}
*/
//--------------------------------------------------
extern "C" int WinMainCRTStartup()
{
	ExitProcess(crt_main(0, nullptr));
	return 0;
}

//--------------------------------------------------
extern "C" int __cdecl _purecall(void)
{ 
	//O_ASSERT(false);
	return 0;
}

//--------------------------------------------------
extern "C" int _fltused = 0x9875;

//--------------------------------------------------
void __cdecl operator delete(void* mem, size_t size)
{

}

#pragma function(memset, memcmp, memcpy)

//--------------------------------------------------
extern "C"
void* __cdecl memmove(
	void*       _Dst,
	void const* _Src,
	size_t      _Size
)
{
	//O_ASSERT(false);
	return nullptr;
}

//--------------------------------------------------
extern "C"
void* __cdecl memset(
	void*  _Dst,
	int    _Val,
	size_t size
)
{
	outland::uint8_t value8 = _Val;
	outland::uint32_t value32 = value8;
	value32 |= value32 << 24 | value32 << 16 | value32 << 8;

	outland::byte_t* aligner = reinterpret_cast<outland::byte_t*>(_Dst);
	void* dst = outland::u::mem_align(_Dst, alignof(outland::uint32_t));

	while ((aligner != dst) && (size >= sizeof(outland::uint8_t)))
	{
		size -= sizeof(outland::uint8_t);
		*aligner = value8;
		++aligner;
	}

	while (size >= sizeof(outland::uint32_t))
	{
		size -= sizeof(outland::uint32_t);
		*reinterpret_cast<outland::uint32_t*>(dst) = value32;
		dst = reinterpret_cast<outland::byte_t*>(dst) + sizeof(outland::uint32_t);
	}

	while (size >= sizeof(outland::uint8_t))
	{
		size -= sizeof(outland::uint8_t);
		*reinterpret_cast<outland::uint8_t*>(dst) = value8;
		dst = reinterpret_cast<outland::byte_t*>(dst) + sizeof(outland::uint8_t);
	}

	return _Dst;
}

//--------------------------------------------------
extern "C"
int __cdecl memcmp(
	void const* _Buf1,
	void const* _Buf2,
	size_t      _Size
)
{
	//todo: optimize for word size

	if (0 == _Size)
		return 0;

	outland::byte_t const* buf1 = reinterpret_cast<outland::byte_t const*>(_Buf1);
	outland::byte_t const* buf2 = reinterpret_cast<outland::byte_t const*>(_Buf2);
	
	while (--_Size && *buf1 == *buf2)
	{
		++buf1;
		++buf2;
	}

	return *buf2 - *buf1;
}

//--------------------------------------------------
extern "C"
void* __cdecl memcpy(
	void* _Dst,
	void const* _Src,
	size_t      _Size
)
{
	//todo: optimize for word size

	outland::byte_t const* src = reinterpret_cast<outland::byte_t const*>(_Src);
	outland::byte_t* dst = reinterpret_cast<outland::byte_t*>(_Dst);

	while (_Size)
	{
		--_Size;
		dst[_Size] = src[_Size];
	}

	return _Dst;
}

//--------------------------------------------------
_ACRTIMP
void __cdecl _aligned_free(
	_Pre_maybenull_ _Post_invalid_ void* _Block
)
{
	outland::byte_t* mem_aligned = reinterpret_cast<outland::byte_t*>(_Block);
	size_t diff = *reinterpret_cast<size_t*>(mem_aligned - sizeof(size_t));
	outland::byte_t* mem = mem_aligned - diff;

	HeapFree(GetProcessHeap(), 0, mem);
}

//--------------------------------------------------
_Check_return_ _Ret_maybenull_ _Post_writable_byte_size_(_Size)
_ACRTIMP _CRTALLOCATOR _CRTRESTRICT
void* __cdecl _aligned_malloc(
	_In_ _CRT_GUARDOVERFLOW size_t _Size,
	_In_                    size_t _Alignment
)
{
	size_t extra = _Alignment + sizeof(size_t);
	outland::byte_t* mem = reinterpret_cast<outland::byte_t*>(HeapAlloc(GetProcessHeap(), 0, _Size + extra));
	outland::byte_t* mem_aligned = reinterpret_cast<outland::byte_t*>(outland::u::mem_align(mem + sizeof(size_t), _Alignment));
	size_t diff = (mem_aligned - mem);
	*reinterpret_cast<size_t*>(mem_aligned - sizeof(size_t)) = diff;

	return mem_aligned;
}

#endif