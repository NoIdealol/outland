#include "pch.h"
#include "main_win.h"
#include "application\application_win.h"

namespace outland { namespace os
{
	void init_time();

	//--------------------------------------------------
	static error_t get_error(DWORD err)
	{
		switch (err)
		{
		case ERROR_INSUFFICIENT_BUFFER://.A supplied buffer size was not large enough, or it was incorrectly set to NULL.
			return err_out_of_memory;
		case ERROR_NO_UNICODE_TRANSLATION://
			return err_bad_data;
		case ERROR_INVALID_FLAGS://.The values supplied for flags were not valid.
		case ERROR_INVALID_PARAMETER://.Any of the parameter values was invalid.
		default:
			return err_unknown;
		}
	}

	//--------------------------------------------------
	error_t wchar_to_utf8(wchar_t const* wstr, string8_t& utf8)
	{
		//returns num of bytes required
		int chars_req = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, nullptr, 0, NULL, NULL);
		if (1 > chars_req)
		{
			DWORD err = GetLastError();
			return get_error(err);
		}

		error_t err = utf8.resize(chars_req - 1);
		if (err)
			return err;

		//returns num of bytes written
		if (chars_req != WideCharToMultiByte(CP_UTF8, 0, wstr, -1, utf8.data(), chars_req, NULL, NULL))
		{
			utf8.clear();
			DWORD err = GetLastError();
			return get_error(err);
		}

		return err_ok;
	}

	//--------------------------------------------------
	error_t utf8_to_wchar(char8_t const* utf8, stringw_t& wstr)
	{
		//returns num of bytes required
		int chars_req = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, nullptr, 0);
		if (1 > chars_req)
		{
			DWORD err = GetLastError();
			return get_error(err);
		}

		error_t err = wstr.resize(chars_req - 1);
		if (err)
			return err;

		//returns num of bytes written
		if(chars_req != MultiByteToWideChar(CP_UTF8, 0, utf8, -1, wstr.data(), chars_req))
		{
			wstr.clear();
			DWORD err = GetLastError();
			return get_error(err);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void path_to_system(wchar_t* wstr)
	{
		while (*wstr)
		{
			if (L'/' == *wstr)
				*wstr = L'\\';
			++wstr;
		}
	}

} }

#if !O_CRT
//--------------------------------------------------
int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
)
{
	outland::uint8_t ret = 0;

	outland::os::init_time();

	outland::os::app::init_os(hInstance);

	outland::main_args_t main_args;
	main_args.allocator = outland::os::app::allocator;
	main_args.cmd_count = __argc;
	main_args.cmd_line = __argv;

	ret = outland::main(main_args);

	outland::os::app::clean_os();

	return ret;
}
#else
//--------------------------------------------------
outland::uint8_t crt_main(size_t arg_count, outland::char8_t* cmd_line[])
{
	outland::uint8_t ret = 0;

	outland::os::init_time();

	outland::os::app::init_os(GetModuleHandle(NULL));

	outland::main_args_t main_args;
	main_args.allocator = outland::os::app::allocator;
	main_args.cmd_count = arg_count;
	main_args.cmd_line = cmd_line;

	ret = outland::main(main_args);

	outland::os::app::clean_os();

	return ret;
}
#endif
