#pragma once

#include <Windows.h>

#include "main\outland.h"

#include "system\application\application.h"

#include "config.h"

namespace outland { namespace os
{

	error_t wchar_to_utf8(wchar_t const* wstr, string8_t& utf8);
	error_t utf8_to_wchar(char8_t const* utf8, stringw_t& wstr);
	void path_to_system(wchar_t* wstr);

	namespace app
	{
		extern allocator_t* const	allocator;
		extern process_id const&	process;
		extern library_id const&	library;
	}

} }