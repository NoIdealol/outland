#pragma once
#include "system\thread\thread.h"

namespace outland { namespace os
{
	//--------------------------------------------------
	HANDLE get_native_handle(thread_id thread);

} }
