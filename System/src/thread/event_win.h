#pragma once
#include "system\thread\event.h"

namespace outland { namespace  os
{
	//--------------------------------------------------
	HANDLE get_native_handle(event_id event);

} }
