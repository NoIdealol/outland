#include "pch.h"
#include "mutex_win.h"

namespace outland { namespace os
{

	//--------------------------------------------------
	error_t	mutex::create(mutex_id& mutex, size_t spin_count)
	{
		mutex_id m = mem<mutex_t>::alloc(app::allocator);
		if (nullptr == m)
			return err_out_of_handles;

		BOOL ret = InitializeCriticalSectionAndSpinCount(m, (DWORD)spin_count);
		if (FALSE == ret)
		{
			mem<mutex_t>::free(app::allocator, m);
			return err_unknown;
		}

		mutex = m;
		return err_ok;
	}

	//--------------------------------------------------
	void	mutex::destroy(mutex_id mutex)
	{
		DeleteCriticalSection(mutex);
		mem<mutex_t>::free(app::allocator, mutex);
	}

	//--------------------------------------------------
	void	mutex::lock(mutex_id mutex)
	{
		EnterCriticalSection(mutex);
	}

	//--------------------------------------------------
	void	mutex::unlock(mutex_id mutex)
	{
		LeaveCriticalSection(mutex);
	}

	//--------------------------------------------------
	bool	mutex::try_lock(mutex_id mutex)
	{
		return TRUE == TryEnterCriticalSection(mutex);
	}

	//--------------------------------------------------
	CRITICAL_SECTION* get_native_handle(mutex_id mutex)
	{
		return mutex;
	}

} }
