#include "pch.h"
#include "thread_win.h"
#include "memory\scratch.h"

namespace outland { namespace os
{
#define O_THREAD_HANDLE_NULL NULL
	
	struct thread_data_t
	{
		thread::main_t::function_t	function;
		thread::main_t::instance_t	instance;
		char8_t*					name_data;
		size_t						name_size;
	};

	using thread_mem_t = mem<thread_data_t, char8_t[]>;

#if !O_CRT
	//--------------------------------------------------
	static void thread_t_set_name_MSVC(char8_t const* name)
	{
		const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)  
		typedef struct tagTHREADNAME_INFO
		{
			DWORD dwType; // Must be 0x1000.  
			LPCSTR szName; // Pointer to name (in user addr space).  
			DWORD dwThreadID; // Thread ID (-1=caller thread).  
			DWORD dwFlags; // Reserved for future use, must be zero.  
		} THREADNAME_INFO;
#pragma pack(pop)  

		DWORD dwThreadID = GetCurrentThreadId();
		const char* threadName = name;

		THREADNAME_INFO info;
		info.dwType = 0x1000;
		info.szName = threadName;
		info.dwThreadID = dwThreadID;
		info.dwFlags = 0;

#pragma warning(push)  
#pragma warning(disable: 6320 6322)  
		__try
		{
			RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
		}
#pragma warning(pop)  
	}
#endif
	//--------------------------------------------------
	static void thread_t_set_name_windows10(char8_t const* name)
	{
		using SetThreadDescription = HRESULT(WINAPI*)(HANDLE hThread, PCWSTR lpThreadDescription);

		HMODULE h_module = ::GetModuleHandleW(L"Kernel32.dll");
		if (NULL != h_module)
		{
			FARPROC farproc = ::GetProcAddress(h_module, "SetThreadDescription");
			if (NULL != farproc)
			{
				byte_t buffer[1024];
				memory_info_t memory_info;
				scratch_allocator::memory_info(memory_info);
				size_t buffer_offset = u::mem_align_offset(buffer, memory_info.align);

				if (sizeof(buffer) < (buffer_offset + memory_info.size))
				{
					O_ASSERT(false);
					return;
				}

				scratch_allocator::create_t create_info;
				create_info.page_source = app::allocator;
				create_info.page_size = sizeof(buffer);
				create_info.init_buffer = buffer + (buffer_offset + memory_info.size);
				create_info.init_size = sizeof(buffer) - (buffer_offset + memory_info.size);

				scratch_allocator_t* scratch;
				scratch_allocator::create(scratch, create_info, buffer + buffer_offset);

				{
					stringw_t wname = { scratch };

					if (err_ok == utf8_to_wchar(name, wname))
					{
						SetThreadDescription set_thread_description = reinterpret_cast<SetThreadDescription>(farproc);
						set_thread_description(::GetCurrentThread(), wname.data());
						//ignore return, we cannot do anything anyway
					}
				}

				scratch_allocator::destroy(scratch);
			}
		}
	}

	//--------------------------------------------------
	static void thread_t_set_name(char8_t const* name)
	{
		//the old style for visual studio, from MSDN
#if !O_CRT
		thread_t_set_name_MSVC(name);
#endif
		//new style, probably not properly supported yet, if ever ...
		thread_t_set_name_windows10(name);
	}

	//--------------------------------------------------
	static unsigned long _stdcall thread_t_main(void* user_data)
	{
		thread::main_t::function_t	function = nullptr;
		thread::main_t::instance_t	instance = nullptr;
		
		{
			thread_data_t* data = static_cast<thread_data_t*>(user_data);
			function = data->function;
			instance = data->instance;

			thread_t_set_name(data->name_data);

			thread_mem_t::array_size_t sizes;
			sizes.at<1>() = data->name_size + 1;

			thread_mem_t::alloc_t alloc;
			alloc.at<0>() = data;
			alloc.at<1>() = data->name_data;

			thread_mem_t::free(app::allocator, alloc, sizes);
		}

		return function(instance);
	}

	//--------------------------------------------------
	thread::id_t	thread::id()
	{
		return static_cast<id_t>(GetCurrentThreadId());
	}

	//--------------------------------------------------
	thread::id_t	thread::id(thread_id thread)
	{
		return static_cast<id_t>(GetThreadId(get_native_handle(thread)));
	}

	//--------------------------------------------------
	error_t	thread::create(thread_id& thread, main_t main, char8_t const* name, size_t stack_size)
	{
		if (nullptr == name)
			name = "O_THREAD";

		thread_mem_t::array_size_t sizes;
		sizes.at<1>() = u::str_len(name) + 1;

		thread_mem_t::alloc_t alloc = thread_mem_t::alloc(app::allocator, sizes);
		thread_data_t* data = alloc.at<0>();
		if (nullptr == data)
			return err_out_of_handles;

		u::mem_copy(alloc.at<1>(), name, sizes.at<1>());

		data->name_data = alloc.at<1>();
		data->name_size = sizes.at<1>() - 1;
		data->function = main.get_function();
		data->instance = main.get_instance();

		DWORD id;
		HANDLE t = CreateThread(
			NULL, //default securit
			stack_size, //default stack size
			&thread_t_main, //starting function
			data, //starting fnc argument
			0, //no flags
			&id); //thread_t ID

		if (O_THREAD_HANDLE_NULL == t)
		{
			thread_mem_t::free(app::allocator, alloc, sizes);

			auto err = GetLastError();
			switch (err)
			{
			case ERROR_TOO_MANY_OPEN_FILES:
				return err_out_of_handles;
			default:
				return err_unknown;
			}
		}

		thread = reinterpret_cast<thread_id>(t);
		return err_ok;
	}

	//--------------------------------------------------
	error_t thread::join(thread_id thread)
	{
		switch (WaitForSingleObject(get_native_handle(thread), INFINITE))
		{
		case WAIT_OBJECT_0:
			break;
		case WAIT_TIMEOUT:
		case WAIT_FAILED:
		default:
			return err_unknown;
		}

		if (0 == CloseHandle(get_native_handle(thread)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	thread::detach(thread_id thread)
	{
		if (0 == CloseHandle(get_native_handle(thread)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	thread::terminate(thread_id thread)
	{
		if (FALSE == TerminateThread(get_native_handle(thread), -1))
			return err_unknown;

		return err_ok;
	}
	
	//--------------------------------------------------
	void	thread::sleep(time_t time_ms)
	{
		Sleep(static_cast<DWORD>(time_ms));
	}

	//--------------------------------------------------
	void	thread::yield()
	{
		YieldProcessor();
	}

	//--------------------------------------------------
	HANDLE get_native_handle(thread_id thread)
	{
		return reinterpret_cast<HANDLE>(thread);
	}

} }
