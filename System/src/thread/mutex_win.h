#pragma once
#include "system\thread\mutex.h"

namespace outland { namespace os
{
	//--------------------------------------------------
	struct mutex_t : public CRITICAL_SECTION
	{
	};

	//--------------------------------------------------
	CRITICAL_SECTION* get_native_handle(mutex_id mutex);

} }