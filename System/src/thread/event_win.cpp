#include "pch.h"
#include "event_win.h"


namespace outland { namespace  os
{
	//--------------------------------------------------
	HANDLE get_native_handle(event_id event)
	{
		return event;
	}

	//--------------------------------------------------
	error_t	event::create(event_id& event)
	{
		HANDLE handle = CreateEventW(NULL, TRUE, FALSE, NULL);
		if (NULL == handle)
		{
			auto err = GetLastError();
			switch (err)
			{
			case ERROR_TOO_MANY_OPEN_FILES:
				return err_out_of_handles;
			default:
				return err_unknown;
			}
		}

		event = reinterpret_cast<event_id>(handle);
		return err_ok;
	}

	//--------------------------------------------------
	void event::destroy(event_id event)
	{
		BOOL result = CloseHandle(get_native_handle(event));
	}

	//--------------------------------------------------
	void	event::set(event_id event)
	{
		BOOL result = SetEvent(get_native_handle(event));
		//O_ASSERT(FALSE != result);
	}

	//--------------------------------------------------
	void	event::reset(event_id event)
	{
		BOOL result = ResetEvent(get_native_handle(event));
		//O_ASSERT(FALSE != result);
	}

	//--------------------------------------------------
	void	event::wait(event_id event)
	{
		auto result = WaitForSingleObject(get_native_handle(event), INFINITE);
		//O_ASSERT(result == WAIT_OBJECT_0);
	}


} }
