#include "pch.h"
#include "atomic_win.h"

namespace outland { namespace  os
{

	//--------------------------------------------------
	std::memory_order to_std_order(memory::order order)
	{
		switch (order)
		{
		case outland::os::memory::order_flush:
			return std::memory_order_release;
		case outland::os::memory::order_evict:
			return std::memory_order_acquire;
		case outland::os::memory::order_strict:
			return std::memory_order_acq_rel;
		case outland::os::memory::order_relaxed:
			return std::memory_order_relaxed;
		default:
			O_ASSERT(false);
			return std::memory_order_acq_rel;
		}
	}

	//--------------------------------------------------
	void memory::barrier(memory::order order)
	{
		std::atomic_thread_fence(to_std_order(order));
	}

	//--------------------------------------------------
	int32_t	atomic::read(atomic_int32_id atom, memory::order order)
	{
		return atom->var.load(to_std_order(order));
	}

	//--------------------------------------------------
	int32_t	atomic::exchange(atomic_int32_id atom, int32_t value, memory::order order)
	{
		return atom->var.exchange(value, to_std_order(order));
	}

	//--------------------------------------------------
	int32_t	atomic::increment(atomic_int32_id atom, int32_t value, memory::order order)
	{
		return atom->var.fetch_add(value, to_std_order(order));
	}

	//--------------------------------------------------
	int32_t	atomic::decrement(atomic_int32_id atom, int32_t value, memory::order order)
	{
		return atom->var.fetch_sub(value, to_std_order(order));
	}

	//--------------------------------------------------
	void	atomic::write(atomic_int32_id atom, int32_t value, memory::order order)
	{
		atom->var.store(value, to_std_order(order));
	}


	//--------------------------------------------------
	uint32_t	atomic::read(atomic_uint32_id atom, memory::order order)
	{
		return atom->var.load(to_std_order(order));
	}

	//--------------------------------------------------
	uint32_t	atomic::exchange(atomic_uint32_id atom, uint32_t value, memory::order order)
	{
		return atom->var.exchange(value, to_std_order(order));
	}

	//--------------------------------------------------
	uint32_t	atomic::increment(atomic_uint32_id atom, uint32_t value, memory::order order)
	{
		return atom->var.fetch_add(value, to_std_order(order));
	}

	//--------------------------------------------------
	uint32_t	atomic::decrement(atomic_uint32_id atom, uint32_t value, memory::order order)
	{
		return atom->var.fetch_sub(value, to_std_order(order));
	}

	//--------------------------------------------------
	void		atomic::write(atomic_uint32_id atom, uint32_t value, memory::order order)
	{
		atom->var.store(value, to_std_order(order));
	}





	//--------------------------------------------------
	int64_t	atomic::read(atomic_int64_id atom, memory::order order)
	{
		return atom->var.load(to_std_order(order));
	}

	//--------------------------------------------------
	int64_t	atomic::exchange(atomic_int64_id atom, int64_t value, memory::order order)
	{
		return atom->var.exchange(value, to_std_order(order));
	}

	//--------------------------------------------------
	int64_t	atomic::increment(atomic_int64_id atom, int64_t value, memory::order order)
	{
		return atom->var.fetch_add(value, to_std_order(order));
	}

	//--------------------------------------------------
	int64_t	atomic::decrement(atomic_int64_id atom, int64_t value, memory::order order)
	{
		return atom->var.fetch_sub(value, to_std_order(order));
	}

	//--------------------------------------------------
	void	atomic::write(atomic_int64_id atom, int64_t value, memory::order order)
	{
		atom->var.store(value, to_std_order(order));
	}



	//--------------------------------------------------
	uint64_t	atomic::read(atomic_uint64_id atom, memory::order order)
	{
		return atom->var.load(to_std_order(order));
	}

	//--------------------------------------------------
	uint64_t	atomic::exchange(atomic_uint64_id atom, uint64_t value, memory::order order)
	{
		return atom->var.exchange(value, to_std_order(order));
	}

	//--------------------------------------------------
	uint64_t	atomic::increment(atomic_uint64_id atom, uint64_t value, memory::order order)
	{
		return atom->var.fetch_add(value, to_std_order(order));
	}

	//--------------------------------------------------
	uint64_t	atomic::decrement(atomic_uint64_id atom, uint64_t value, memory::order order)
	{
		return atom->var.fetch_sub(value, to_std_order(order));
	}

	//--------------------------------------------------
	void		atomic::write(atomic_uint64_id atom, uint64_t value, memory::order order)
	{
		atom->var.store(value, to_std_order(order));
	}



	//--------------------------------------------------
	void	atomic::memory_info_int32(memory_info_t& info)
	{
		info.align = alignof(atomic_int32_t);
		info.size = sizeof(atomic_int32_t);
	}

	//--------------------------------------------------
	void	atomic::create(atomic_int32_id& atom, int32_t value, void* memory)
	{
		atom = O_CREATE(atomic_int32_t, memory);
		atom->var = value;
	}

	//--------------------------------------------------
	void	atomic::destroy(atomic_int32_id& atom)
	{
		O_DESTROY(atomic_int32_t, atom);
	}

	//--------------------------------------------------
	void	atomic::memory_info_uint32(memory_info_t& info)
	{
		info.align = alignof(atomic_uint32_t);
		info.size = sizeof(atomic_uint32_t);
	}

	//--------------------------------------------------
	void	atomic::create(atomic_uint32_id& atom, uint32_t value, void* memory)
	{
		{
			atom = O_CREATE(atomic_uint32_t, memory);
			atom->var = value;
		}
	}

	//--------------------------------------------------
	void	atomic::destroy(atomic_uint32_id& atom)
	{
		O_DESTROY(atomic_uint32_t, atom);
	}

	//--------------------------------------------------
	void	atomic::memory_info_int64(memory_info_t& info)
	{
		info.align = alignof(atomic_int64_t);
		info.size = sizeof(atomic_int64_t);
	}

	//--------------------------------------------------
	void	atomic::create(atomic_int64_id& atom, int64_t value, void* memory)
	{
		{
			atom = O_CREATE(atomic_int64_t, memory);
			atom->var = value;
		}
	}

	//--------------------------------------------------
	void	atomic::destroy(atomic_int64_id& atom)
	{
		O_DESTROY(atomic_int64_t, atom);
	}

	//--------------------------------------------------
	void	atomic::memory_info_uint64(memory_info_t& info)
	{
		info.align = alignof(atomic_uint64_t);
		info.size = sizeof(atomic_uint64_t);
	}

	//--------------------------------------------------
	void	atomic::create(atomic_uint64_id& atom, uint64_t value, void* memory)
	{
		atom = O_CREATE(atomic_uint64_t, memory);
		atom->var = value;
	}

	//--------------------------------------------------
	void	atomic::destroy(atomic_uint64_id& atom)
	{
		O_DESTROY(atomic_uint64_t, atom);
	}
	
} }