#pragma once
#include "system\thread\atomic.h"
#include <atomic>

namespace outland { namespace  os
{
	//--------------------------------------------------
	struct atomic_int32_t
	{
		std::atomic_int32_t		var;
	};

	struct atomic_uint32_t
	{
		std::atomic_uint32_t	var;
	};

	struct atomic_int64_t
	{
		std::atomic_int64_t		var;
	};

	struct atomic_uint64_t
	{
		std::atomic_uint64_t	var;
	};

	struct atomic_ptr_t
	{
		std::atomic<void*>		var;
	};

} }
