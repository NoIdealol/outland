#pragma once
#include "system\file\file.h"

namespace outland { namespace  os
{
	namespace file
	{
		struct operation_t : OVERLAPPED
		{
			request_t	request;
		};
	}
	HANDLE get_native_handle(file_id file);
	HANDLE get_native_handle(file::queue_id file_queue);

}}