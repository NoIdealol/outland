#pragma once
#include "pch.h"
#include "file_win.h"

namespace outland { namespace  os
{

	//--------------------------------------------------
	enum : ULONG_PTR
	{
		key_file,
	};

	//--------------------------------------------------
	static error_t get_error(DWORD err)
	{
		switch (err)
		{
			case ERROR_OUTOFMEMORY:
				return err_out_of_memory;
			case ERROR_ACCESS_DENIED:
				return err_access_denied;
			case ERROR_TOO_MANY_OPEN_FILES:
				return err_out_of_handles;
			case ERROR_FILE_NOT_FOUND:
			case ERROR_PATH_NOT_FOUND:
				return err_not_found;
			case ERROR_ALREADY_EXISTS:
				return err_already_exists;
			default:
				return err_unknown;
		};
	}

	//--------------------------------------------------
	error_t	file::open(file_id& file, queue_id q, open_t open, access_t access, const char8_t* path)
	{
		stringw_t wstr = { app::allocator };
		error_t err = utf8_to_wchar(path, wstr);
		if (err)
			return err;

		path_to_system(wstr.data());

		DWORD acces = 0;
		if (access & access_read)
			acces |= GENERIC_READ;
		if (access & access_write)
			acces |= GENERIC_WRITE;

		DWORD flagsAndAttributes = FILE_ATTRIBUTE_NORMAL;
		flagsAndAttributes |= FILE_FLAG_OVERLAPPED;

		DWORD creation = 0;
		switch (open)
		{
		case outland::os::file::open_t::existing:
			creation = OPEN_EXISTING;
			break;
		case outland::os::file::open_t::try_new:
			creation = CREATE_NEW;
			break;
		case outland::os::file::open_t::overwrite:
			creation = CREATE_ALWAYS;
			break;
		case outland::os::file::open_t::always:
			creation = OPEN_ALWAYS;
			break;
		default:
			return err_invalid_parameter;
		}

		HANDLE hFile = CreateFileW(wstr.data(), acces, FILE_SHARE_READ, NULL, creation, flagsAndAttributes, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			auto err = GetLastError();
			return get_error(err);
		}

		if (NULL == CreateIoCompletionPort(
			hFile,
			get_native_handle(q),
			key_file, //completion key,
			0) //ignored
		)
		{
			CloseHandle(hFile);
			return err_unknown;
		}

		file = reinterpret_cast<file_id>(hFile);
		return err_ok;
	}

	//--------------------------------------------------
	error_t	file::close(file_id file)
	{
		if (FALSE == CloseHandle(get_native_handle(file)))
		{
			auto err = GetLastError();
			return get_error(err);
		}

		return err_ok;
	}

	//--------------------------------------------------
	error_t	file::remove(const char8_t* path)
	{
		stringw_t wstr = { app::allocator };
		error_t err = utf8_to_wchar(path, wstr);
		if (err)
			return err;

		path_to_system(wstr.data());

		if (TRUE == DeleteFileW(wstr.data()))
			return err_ok;

		return get_error(GetLastError());
	}

	//--------------------------------------------------
	error_t file::size(file_id file, filesize_t& size)
	{
		LARGE_INTEGER li;
		if (FALSE == GetFileSizeEx(get_native_handle(file), &li))
		{
			auto err = GetLastError();
			return get_error(err);
		}

		size = static_cast<filesize_t>(li.QuadPart);

		return err_ok;
	}

	//--------------------------------------------------
	error_t file::resize(file_id file, filesize_t size)
	{
		LARGE_INTEGER li;
		li.QuadPart = size;
		if (FALSE == SetFilePointerEx(get_native_handle(file), li, NULL, FILE_BEGIN))
		{
			auto err = GetLastError();
			return get_error(err);
		}

		if (FALSE == SetEndOfFile(get_native_handle(file)))
		{
			auto err = GetLastError();
			return get_error(err);
		}
		return err_ok;
	}

	//--------------------------------------------------
	error_t	file::read(operation_id operation)
	{
		operation->hEvent = NULL;
		operation->Internal = 0;
		operation->InternalHigh = 0;
		operation->Offset = (DWORD)(operation->request.where & 0xFFffFFff);
		operation->OffsetHigh = (DWORD)(operation->request.where >> 32);

		BOOL result = ReadFile(
			get_native_handle(operation->request.file),
			operation->request.buffer,
			static_cast<DWORD>(operation->request.size),
			nullptr, 
			operation);

		if (FALSE == result)
		{
			auto err = GetLastError();
			if (ERROR_IO_PENDING == err)
				return err_ok;

			return get_error(err);
		}
		else
		{
			return err_ok;
		}

		return err_unknown;
	}

	//--------------------------------------------------
	error_t	file::write(operation_id operation)
	{
		operation->hEvent = NULL;
		operation->Internal = 0;
		operation->InternalHigh = 0;
		operation->Offset = (DWORD)(operation->request.where & 0xFFffFFff);
		operation->OffsetHigh = (DWORD)(operation->request.where >> 32);

		BOOL result = WriteFile(
			get_native_handle(operation->request.file),
			operation->request.buffer,
			static_cast<DWORD>(operation->request.size),
			nullptr,
			operation);

		if (FALSE == result)
		{
			auto err = GetLastError();
			if (ERROR_IO_PENDING == err)
				return err_ok;

			return get_error(err);
		}
		else
		{
			return err_ok;
		}

		return err_unknown;
	}

	//--------------------------------------------------
	error_t	file::folder_create(const char8_t* path)
	{
		stringw_t wstr = { app::allocator };
		error_t err = utf8_to_wchar(path, wstr);
		if (err)
			return err;

		path_to_system(wstr.data());

		BOOL result = CreateDirectoryW(wstr.data(), NULL);
		if (result == TRUE)
		{
			return err_ok;
		}
		else
		{
			auto err = GetLastError();
			return get_error(err);
		}
	}

	//--------------------------------------------------
	error_t	file::folder_remove(const char8_t* path)
	{
		BOOL result = RemoveDirectoryA(path);
		if (result == TRUE)
		{
			return err_ok;
		}
		else
		{
			auto err = GetLastError();
			return get_error(err);
		}
	}

	//--------------------------------------------------
	error_t	file::queue_create(queue_id& q)
	{
		HANDLE hPort = CreateIoCompletionPort(
			INVALID_HANDLE_VALUE, //create new port
			NULL, //create new port
			NULL, //ignored, so use NULL
			0 //as many concurrently running threads as there are processors in the system
		);

		if (NULL == hPort)
		{
			auto err = GetLastError();
			return get_error(err);
		}

		q = reinterpret_cast<queue_id>(hPort);
		return err_ok;
	}

	//--------------------------------------------------
	error_t	file::queue_destroy(queue_id q)
	{
		if (FALSE == CloseHandle(get_native_handle(q)))
		{
			auto err = GetLastError();
			return get_error(err);
		}

		return err_ok;
	}

	//--------------------------------------------------
	void	file::queue_process(queue_id q, result_t* results_data, size_t results_size, size_t& results_processed)
	{
		OVERLAPPED_ENTRY overlapped[64] = { 0 };
		ULONG count = 0;

		results_processed = 0;

		while (results_size)
		{
			ULONG can_process = static_cast<ULONG>(u::array_size(overlapped) > results_size ? results_size : u::array_size(overlapped));

			BOOL result = GetQueuedCompletionStatusEx(
				get_native_handle(q),
				overlapped,
				can_process,
				&count,
				0/*INFINITE*/,
				FALSE);
			
			if (result != FALSE)
			{
				//we got a packet
				for (ULONG i = 0; i < count; ++i)
				{
					switch (overlapped[i].lpCompletionKey)
					{
					case key_file:
					{
						results_data->operation = static_cast<operation_id>(overlapped[i].lpOverlapped);
						results_data->transfered = static_cast<size_t>(overlapped[i].dwNumberOfBytesTransferred);

						DWORD bytes_transfered = 0;
						result = GetOverlappedResult(
							get_native_handle(results_data->operation->request.file), //file
							results_data->operation, //overlapped
							&bytes_transfered, 
							FALSE //nonblocking
						);

						O_ASSERT(results_data->transfered == static_cast<size_t>(bytes_transfered));

						if (0 != result)
						{
							results_data->error = err_ok;
						}
						else
						{
							auto err = GetLastError();
							switch (err)
							{
							case ERROR_OPERATION_ABORTED:
								//break;
							case ERROR_HANDLE_EOF:
								//break;
							default:
								results_data->error = err_unknown;
								break;
							}
						}

						++results_data;
						--results_size;
						++results_processed;
						break;
					}
					default:
						O_ASSERT(false);
						break; //should not happen, skip
					}
				}
			}
			else
			{
				auto err = GetLastError();
				//packet did not dequeue
				switch (err)
				{
				case ERROR_ABANDONED_WAIT_0:
					return; //the port was closed, error
				case ERROR_NO_MORE_ITEMS:
					return; //empty
				default:
					return; //timeout probably
				}
				
			}
		}
	}

	//--------------------------------------------------
	error_t	file::operation_create(operation_id& op)
	{
		op = mem<operation_t>::alloc(app::allocator);
		if (nullptr == op)
			return err_out_of_memory;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	file::operation_destroy(operation_id op)
	{
		mem<operation_t>::free(app::allocator, op);

		return err_ok;
	}

	//--------------------------------------------------
	void	file::operation_set_request(operation_id op, request_t const& request)
	{
		op->request = request;
	}

	//--------------------------------------------------
	void	file::operation_get_request(operation_id op, request_t& request)
	{
		request = op->request;
	}

	//--------------------------------------------------
	error_t	file::attributes_set(attributes_t attr, const char8_t* path)
	{
		stringw_t wstr = { app::allocator };
		error_t err = utf8_to_wchar(path, wstr);
		if (err)
			return err;

		path_to_system(wstr.data());

		DWORD sysAttr = 0;
		if (attr & att_read_only)
			sysAttr |= FILE_ATTRIBUTE_READONLY;
		if (attr & att_hidden)
			sysAttr |= FILE_ATTRIBUTE_HIDDEN;

		BOOL result = SetFileAttributesW(wstr.data(), sysAttr);
		if (TRUE == result)
		{
			return err_ok;
		}
		else
		{
			auto err = GetLastError();
			return get_error(err);
		}
	}

	//--------------------------------------------------
	error_t	file::attributes_get(attributes_t& attr, const char8_t* path)
	{
		stringw_t wstr = { app::allocator };
		error_t err = utf8_to_wchar(path, wstr);
		if (err)
			return err;

		path_to_system(wstr.data());


		DWORD sysAttr = GetFileAttributesW(wstr.data());
		if (INVALID_FILE_ATTRIBUTES != sysAttr)
		{
			attr = 0;
			if (sysAttr & FILE_ATTRIBUTE_DIRECTORY)
				attr |= att_folder;
			if (sysAttr & FILE_ATTRIBUTE_READONLY)
				attr |= att_read_only;
			if (sysAttr & FILE_ATTRIBUTE_HIDDEN)
				attr |= att_hidden;
			if (sysAttr & FILE_ATTRIBUTE_SYSTEM)
				attr |= att_system;

			return err_ok;
		}
		else
		{
			auto err = GetLastError();
			return get_error(err);
		}
	}

	//--------------------------------------------------
	HANDLE get_native_handle(file_id file)
	{
		return reinterpret_cast<HANDLE>(file);
	}

	//--------------------------------------------------
	HANDLE get_native_handle(file::queue_id file_queue)
	{
		return reinterpret_cast<HANDLE>(file_queue);
	}

}}