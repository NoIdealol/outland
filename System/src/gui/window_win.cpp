#include "pch.h"
#include "window_win.h"
#include "input\keyboard_win.h"
#include "surface_win.h"
#include "application\library_win.h"

namespace outland { namespace  os
{
	namespace g
	{
		enum : size_t
		{
			max_class_name_len = 32
		};
		static const wchar_t		class_window[] = L"OUTLAND_ENGINE_WINDOW";
		static const wchar_t		class_shadow[] = L"OUTLAND_ENGINE_SHADOW";
		window_manager_t*			manager = nullptr;
	}

#define O_WINDOW_HANDLE_NULL NULL
	
	enum : UINT
	{
		WM_THIS_DESTROY_LATER = WM_USER, //window wide msg

		//WM_APP_MSG = WM_APP, //app wide msg
	};


	//--------------------------------------------------
	window_id const window::null = get_interface_handle(O_WINDOW_HANDLE_NULL);

	//--------------------------------------------------
	HWND get_native_handle(window_id window)
	{
		return reinterpret_cast<HWND>(window);
	}

	//--------------------------------------------------
	window_id get_interface_handle(HWND window)
	{
		return static_cast<window_id>(reinterpret_cast<uintptr_t>(window));
	}

	//--------------------------------------------------
	static window_id hwnd_to_window(HWND hWnd)
	{
		if (hWnd == O_WINDOW_HANDLE_NULL)
			return window::null;
	
		wchar_t buffer[g::max_class_name_len];

		int len = GetClassNameW(
			hWnd,
			buffer,
			(int)g::max_class_name_len);		

		if ((len != u::str_len(g::class_window)) && (len != u::str_len(g::class_shadow)))
			return window::null;

		if ((!u::str_cmp(g::class_window, buffer)) && (!u::str_cmp(g::class_shadow, buffer)))
			return window::null;

		return get_interface_handle(hWnd);
	}

	void rect_to_native(RECT& rc_nat, os::rect_t const& rc_int)
	{
		rc_nat.left = rc_int.base.x;
		rc_nat.top = rc_int.base.y;
		rc_nat.right = rc_int.base.x + rc_int.size.x;
		rc_nat.bottom = rc_int.base.y + rc_int.size.y;
	}

	void rect_to_interface(os::rect_t& rc_int, RECT& rc_nat)
	{
		rc_int.base.x = rc_nat.left;
		rc_int.base.y = rc_nat.top;
		rc_int.size.x = rc_nat.right - rc_nat.left;
		rc_int.size.y = rc_nat.bottom - rc_nat.top;
	}

	//--------------------------------------------------
	error_t window::get_native_style(window_style_t& ws, window_id parent, window::style_t style)
	{
		ws.class_name = nullptr;
		ws.style_win = 0;
		ws.style_ex = 0;

		if (style & st_child)
			ws.style_win |= WS_CHILD;
		else if (style & st_standard)
			ws.style_win |= WS_CAPTION;
		else
			ws.style_win |= WS_POPUP;

		if ((style & st_child) && (style & st_standard))
			return err_invalid_parameter;

		if ((style & st_child) && null == parent)
			return err_invalid_parameter;


		if (style & st_top_tool)
			ws.style_ex |= WS_EX_TOOLWINDOW;
		if (style & st_top_app)
			ws.style_ex |= WS_EX_APPWINDOW;

		if ((style & st_top_tool) && (style & st_top_app))
			return err_invalid_parameter;


		if (style & st_top_most)
			ws.style_ex |= WS_EX_TOPMOST;

		if ((style & st_child) && (style & st_top_most))
			return err_invalid_parameter;


		if (style & st_file_drop)
			ws.style_ex |= WS_EX_ACCEPTFILES;

		//for basic windows
		if (style & st_caption)
			ws.style_win |= WS_CAPTION;
		if (style & st_border)
			ws.style_win |= WS_BORDER;

		if (style & st_size_frame)
			ws.style_win |= WS_THICKFRAME;
		if (style & st_client_border)
			ws.style_ex |= WS_EX_CLIENTEDGE;

		if (style & st_system_menu)
			ws.style_win |= WS_SYSMENU;
		if (style & st_menu_minimize)
			ws.style_win |= WS_MINIMIZEBOX;
		if (style & st_menu_maximize)
			ws.style_win |= WS_MAXIMIZEBOX;

		if (style & st_shadow) //THIS IS A CLASS STYLE WTF!!! 
			ws.class_name = g::class_shadow;
		else
			ws.class_name = g::class_window;

		if ((style & st_shadow) && ((style & st_standard) || (style & st_child)))
			return err_invalid_parameter;

		return err_ok;
	}
	
	//////////////////////////////////////////////////
	// GUI
	//////////////////////////////////////////////////

	//--------------------------------------------------
	error_t window::init()
	{
		error_t err;
		g::manager = mem<window_manager_t>::alloc(app::allocator, err);
		if (nullptr == g::manager)
			return err_out_of_memory;
		
		if (err)
		{
			mem<window_manager_t>::free(app::allocator, g::manager);
			g::manager = nullptr;
			return err;
		}

		SetLastError(0);

		HICON hIcon = NULL;
		HICON hIconSm = NULL;
		HBRUSH hbrBackground = reinterpret_cast<HBRUSH>(COLOR_3DDKSHADOW + 1); //NULL;//

		WNDCLASSEXW wcex;
		wcex.cbSize = sizeof(WNDCLASSEXW);
		wcex.lpfnWndProc = &window_manager_t::window_callback;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = get_native_handle(app::library);
		wcex.hIcon = hIcon;
		wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground = hbrBackground;
		wcex.lpszMenuName = NULL;
		wcex.hIconSm = hIconSm;
		
		wcex.style = 0;//CS_HREDRAW | CS_VREDRAW;
		wcex.lpszClassName = g::class_window;
		ATOM atom_window = RegisterClassExW(&wcex);

		wcex.style = CS_DROPSHADOW;//CS_HREDRAW | CS_VREDRAW;
		wcex.lpszClassName = g::class_shadow;
		ATOM atom_shadow = RegisterClassExW(&wcex);
		if ((NULL == atom_window) || (NULL == atom_shadow))
		{
			if (NULL != atom_window)
				UnregisterClassW(g::class_window, get_native_handle(app::library));

			if (NULL != atom_shadow)
				UnregisterClassW(g::class_shadow, get_native_handle(app::library));

			return err_unknown;
		}

		return err_ok;
	}

	//--------------------------------------------------
	void window::clean()
	{
		if (nullptr == g::manager)
			return;

		mem<window_manager_t>::free(app::allocator, g::manager);
		g::manager = nullptr;

		UnregisterClassW(g::class_window, get_native_handle(app::library));
		UnregisterClassW(g::class_shadow, get_native_handle(app::library));
	}

	//--------------------------------------------------
	error_t window::mouse_capture(window_id window)
	{
		if (O_WINDOW_HANDLE_NULL == window)
			return err_invalid_handle;

		SetCapture(get_native_handle(window));

		return err_ok;
	}

	//--------------------------------------------------
	void window::mouse_release()
	{
		ReleaseCapture();
	}

	//////////////////////////////////////////////////
	// WINDOW
	//////////////////////////////////////////////////

	//--------------------------------------------------
	error_t window::get_client_rect(window_id window, rect_t& client_rect)
	{
		POINT pt_cli;
		pt_cli.x = 0;
		pt_cli.y = 0;
		if (FALSE == ClientToScreen(get_native_handle(window), &pt_cli))
			return err_unknown;

		RECT rc_cli;
		if (FALSE == GetClientRect(get_native_handle(window), &rc_cli))
			return err_unknown;

		RECT rc_win;
		if (FALSE == GetWindowRect(get_native_handle(window), &rc_win))
			return err_unknown;

		rc_cli.left = pt_cli.x - rc_win.left;
		rc_cli.top = pt_cli.y - rc_win.top;

		rect_to_interface(client_rect, rc_cli);

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::get_client_size(window_id window, point_t& client_size)
	{
		RECT rc_cli;
		if (FALSE == GetClientRect(get_native_handle(window), &rc_cli))
			return err_unknown;

		client_size.x = rc_cli.right;
		client_size.y = rc_cli.bottom;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::set_text(window_id window, const char8_t* utf8)
	{
		stringw_t wstr = { app::allocator };
		error_t err = utf8_to_wchar(utf8, wstr);
		if (err)
			return err;

		if (FALSE == SetWindowTextW(get_native_handle(window), wstr.data()))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::set_cursor(window_id window, cursor_id cursor)
	{
		//LONG_PTR userData = GetWindowLongPtr((*it)->hwnd, GWLP_USERDATA);
		//Window_win* windowPtr = reinterpret_cast<Window_win*>(userData);
		SetCursor(get_native_handle(cursor));

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::resize(window_id window, const point_t& size)
	{
		RECT sysRect;
		BOOL err = GetWindowRect(get_native_handle(window), &sysRect);
		if (FALSE == err)
			return err_unknown;

		//do nothing when size did not change
		if (((sysRect.right - sysRect.left) == size.x) &&
			((sysRect.bottom - sysRect.top) == size.y))
			return err_ok;

		if (FALSE == SetWindowPos(get_native_handle(window), NULL, 0, 0, size.x, size.y, SWP_NOZORDER | SWP_NOMOVE))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::move(window_id window, const point_t& base)
	{
		RECT sysRect;
		BOOL err = GetWindowRect(get_native_handle(window), &sysRect);
		if (FALSE == err)
			return err_unknown;

		//do nothing when size did not change
		if ((sysRect.top == base.y) &&
			(sysRect.left == base.x))
			return err_ok;

		if (FALSE == SetWindowPos(get_native_handle(window), NULL, base.x, base.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::repaint(window_id window, const rect_t& client_rect)
	{
		RECT rect;
		rect_to_native(rect, client_rect);
		
		if (FALSE == InvalidateRect(get_native_handle(window), &rect, FALSE))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::repaint_frame(window_id window)
	{
		RECT rcWindow;
		BOOL err = GetWindowRect(get_native_handle(window), &rcWindow); //in screen coords
		if (FALSE == err)
			return err_unknown;

		POINT pointClient;
		pointClient.x = 0;
		pointClient.y = 0;
		err = ClientToScreen(get_native_handle(window), &pointClient); //in screen coords
		if (FALSE == err)
			return err_unknown;

		//to win coords
		pointClient.x -= rcWindow.left;
		pointClient.y -= rcWindow.top;

		RECT rcClient;
		err = GetClientRect(get_native_handle(window), &rcClient); //this is in client coords
		if (FALSE == err)
			return err_unknown;

		RECT rcWindowLocal; //in win coords
		rcWindowLocal.left = 0;
		rcWindowLocal.top = 0;
		rcWindowLocal.right = rcWindow.right - rcWindow.left;
		rcWindowLocal.bottom = rcWindow.bottom - rcWindow.top;
		//transform to client coords
		rcWindowLocal.left -= pointClient.x;
		rcWindowLocal.right -= pointClient.x;
		rcWindowLocal.top -= pointClient.y;
		rcWindowLocal.bottom -= pointClient.y;

		HRGN clipFrame = CreateRectRgnIndirect(&rcWindowLocal);
		HRGN clipClient = CreateRectRgnIndirect(&rcClient);

		error_t ret = err_unknown;

		if (NULL != clipFrame && NULL != clipClient)
		{
			int regionResult = CombineRgn(clipFrame, clipFrame, clipClient, RGN_DIFF);

			if (regionResult != ERROR)
			{
				//this is in client coords!!!!
				err = RedrawWindow(get_native_handle(window), NULL, clipFrame, RDW_FRAME | RDW_INVALIDATE);
				if (FALSE != err)
					ret = err_ok;
			}
		}

		if (NULL != clipFrame)
			err = DeleteObject(clipFrame);
		if (NULL != clipClient)
			err = DeleteObject(clipClient);

		return ret;
	}

	//--------------------------------------------------
	error_t window::minimize(window_id window)
	{
		if (FALSE == ShowWindow(get_native_handle(window), SW_MINIMIZE))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::maximize(window_id window)
	{
		if (FALSE == ShowWindow(get_native_handle(window), SW_MAXIMIZE))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::restore(window_id window)
	{
		if (FALSE == ShowWindow(get_native_handle(window), SW_RESTORE))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::show(window_id window)
	{
		if (FALSE == ShowWindow(get_native_handle(window), SW_SHOW))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::hide(window_id window)
	{
		if (FALSE == ShowWindow(get_native_handle(window), SW_HIDE))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::activate(window_id window)
	{
		if(NULL == SetActiveWindow(get_native_handle(window)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::focus(window_id window)
	{
		if (NULL == SetFocus(get_native_handle(window)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::drag(window_id window)
	{
		//To indicate that you want to move the dialog by using the mouse, you must specify SC_MOVE|0x0002.
		if (0 != SendMessageW(get_native_handle(window), WM_SYSCOMMAND, SC_MOVE | 0x0002, 0))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::parent_set(window_id window, window_id parent)
	{
		if (NULL == SetParent(get_native_handle(window), get_native_handle(parent)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	window_id window::window_at(const point_t& point)
	{
		POINT p;
		p.x = point.x;
		p.y = point.y;
		HWND hwnd = WindowFromPoint(p);
		return hwnd_to_window(hwnd);
	}

	//--------------------------------------------------
	error_t window::mouse_leave_register(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_LEAVE;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::mouse_leave_frame_register(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_LEAVE | TME_NONCLIENT;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::mouse_leave_cancel(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_LEAVE | TME_CANCEL;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::mouse_leave_frame_cancel(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_LEAVE | TME_NONCLIENT | TME_CANCEL;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::mouse_hover_register(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_HOVER;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::mouse_hover_frame_register(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_HOVER | TME_NONCLIENT;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::mouse_hover_cancel(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_HOVER | TME_CANCEL;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::mouse_hover_frame_cancel(window_id window)
	{
		TRACKMOUSEEVENT track;
		track.cbSize = sizeof(TRACKMOUSEEVENT);
		track.dwFlags = TME_HOVER | TME_NONCLIENT | TME_CANCEL;
		track.hwndTrack = get_native_handle(window);
		track.dwHoverTime = HOVER_DEFAULT;

		if (FALSE == TrackMouseEvent(&track))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::calc_window_rect(rect_t& rect, window_id parent, style_t style)
	{
		error_t err;

		window_style_t ws;
		err = get_native_style(ws, parent, style);
		if (err)
			return err;

		RECT rc;
		rect_to_native(rc, rect);
		
		if (FALSE == AdjustWindowRectEx(&rc, ws.style_win, FALSE, ws.style_ex))
			return err_unknown;

		rect_to_interface(rect, rc);

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::create(window_id& window, window_t* callback, const rect_t& window_rect, const char8_t* name, window_id parent, style_t style)
	{
		error_t err;

		window_style_t ws;
		err = get_native_style(ws, parent, style);
		if (err)
			return err;

		HWND hwnd_parent = O_WINDOW_HANDLE_NULL;
		if (parent)
			hwnd_parent = get_native_handle(parent);

		stringw_t namew = { app::allocator };
		err = utf8_to_wchar(name, namew);
		if (err)
			return err;

		g::manager->window_create_begin(callback);

		HWND hwnd = CreateWindowExW(
			ws.style_ex,
			ws.class_name,
			namew.data(),
			ws.style_win,
			window_rect.base.x,
			window_rect.base.y,
			window_rect.size.x,
			window_rect.size.y,
			hwnd_parent,
			NULL,
			get_native_handle(app::library),
			callback);

		if (O_WINDOW_HANDLE_NULL == hwnd)
		{
			g::manager->window_create_fail();
			return err_unknown;
		}
		else
		{
			window::show(get_interface_handle(hwnd)); //creation callback is done, ready to display
			window = get_interface_handle(hwnd);
			return err_ok;
		}
	}

	//--------------------------------------------------
	error_t window::destroy(window_id window)
	{
		if (FALSE == DestroyWindow(get_native_handle(window)))
			return err_unknown;

		return err_ok;
	}

	//--------------------------------------------------
	error_t window::destroy_later(window_id window)
	{
		if (FALSE == PostMessageW(get_native_handle(window), WM_THIS_DESTROY_LATER, 0, 0))
			return err_unknown;

		return err_ok;
	}

	//////////////////////////////////////////////////
	// SYSTEM CALLBACK
	//////////////////////////////////////////////////

	//--------------------------------------------------
	window_manager_t::window_manager_t(error_t& err)
		: _data_array(app::allocator)
	{
		err = _data_array.push_back();
	}

	//--------------------------------------------------
	window_t*	window_manager_t::window_get(HWND hWnd)
	{
		LONG_PTR user_data = GetWindowLongPtrW(hWnd, GWLP_USERDATA);
		return _data_array[user_data].callback;
	}

	//--------------------------------------------------
	error_t		window_manager_t::window_create_begin(window_t* win)
	{
		error_t err;
		err = _data_array.push_back();
		if (err)
			return err;
		
		_data_array.first().callback = win;

		return err_ok;
	}

	//--------------------------------------------------
	void		window_manager_t::window_create_fail()
	{
		_data_array.first().callback = nullptr;
		_data_array.pop_back();
	}

	//--------------------------------------------------
	void		window_manager_t::window_create_end(HWND hWnd)
	{
		SetWindowLongPtrW(hWnd, GWLP_USERDATA, LONG_PTR(_data_array.size() - 1));

		_data_array.last().callback = _data_array.first().callback;
		_data_array.last().hwnd = hWnd;

		_data_array.first().callback = nullptr;
	}

	//--------------------------------------------------
	void		window_manager_t::window_destroy(HWND hWnd)
	{
		//get and clear destroyed window
		LONG_PTR user_data = GetWindowLongPtrW(hWnd, GWLP_USERDATA);
		SetWindowLongPtrW(hWnd, GWLP_USERDATA, 0);

		//if not last, swap last with destroyed
		if ((_data_array.size() - 1) != user_data)
		{
			SetWindowLongPtrW(_data_array.last().hwnd, GWLP_USERDATA, user_data);
			_data_array[user_data] = _data_array.last();
		}

		_data_array.pop_back();
	}

	//--------------------------------------------------
	LRESULT CALLBACK window_manager_t::window_callback(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		//OutputDebugStringA(msg_to_string(msg));
		//OutputDebugStringA("\n");

		window_t* callback = g::manager->window_get(hWnd);

		DWORD window_style = (DWORD)GetWindowLong(hWnd, GWL_STYLE);
		DWORD window_style_ex = (DWORD)GetWindowLong(hWnd, GWL_EXSTYLE);

#define check_style(style) (style == (window_style & style))
#define check_style_ex(style) (style == (window_style_ex & style))

		//WS_EX_CLIENTEDGE
		bool is_managed = !(
			(check_style(WS_POPUP)) || 
			(check_style(WS_CHILD) && !check_style(WS_BORDER) && !check_style_ex(WS_EX_CLIENTEDGE))
			);

		switch (msg)
		{
		case WM_NCCREATE:
		{
			LPCREATESTRUCTW create_struct = reinterpret_cast<LPCREATESTRUCTW>(lParam);
			//callback = static_cast<window_t*>(create_struct->lpCreateParams);
			//set window user data
			//SetWindowLongPtrW(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(callback));

			g::manager->window_create_end(hWnd);


			if (is_managed)
				break;

			//set the task bar name
			SetWindowTextW(hWnd, create_struct->lpszName);

			return TRUE;
		}
		case WM_CREATE:
		{
			callback->on_create(get_interface_handle(hWnd));
			return 0;
		}
		case WM_CLOSE:
		{
			callback->on_close(get_interface_handle(hWnd));
			return 0;
		}
		case WM_DESTROY:
		{
			callback->on_destroy(get_interface_handle(hWnd));
			return 0;
		}
		case WM_NCDESTROY:
		{
			g::manager->window_destroy(hWnd);
			//SetWindowLongPtrW(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(nullptr));
			if (is_managed)
				break;

			return 0;
		}
		case WM_WINDOWPOSCHANGING:
		{
			WINDOWPOS* win_pos = (WINDOWPOS*)lParam;
			bool no_size = 0 != (win_pos->flags & SWP_NOSIZE);
			bool no_move = 0 != (win_pos->flags & SWP_NOMOVE);

			if (!(no_size && no_size))
			{
				rect_t rect;
				rect.base.x = win_pos->x;
				rect.base.y = win_pos->y;
				rect.size.x = win_pos->cx;
				rect.size.y = win_pos->cy;

				if (no_size)
				{
					RECT rc;
					GetWindowRect(hWnd, &rc);
					rect.size.x = rc.right - rc.left;
					rect.size.y = rc.bottom - rc.top;

					callback->on_frame_size_move(get_interface_handle(hWnd), rect);

					if ((rect.size.x != rc.right - rc.left) || (rect.size.y != rc.bottom - rc.top))
					{
						win_pos->flags &= ~(UINT)(SWP_NOSIZE);
						win_pos->cx = rect.size.x;
						win_pos->cy = rect.size.y;
					}

					win_pos->x = rect.base.x;
					win_pos->y = rect.base.y;
				}
				else if (no_move)
				{
					RECT rc;
					GetWindowRect(hWnd, &rc);
					rect.base.x = rc.left;
					rect.base.y = rc.top;

					callback->on_frame_size_move(get_interface_handle(hWnd), rect);

					if ((rect.base.x != rc.left) || (rect.base.y != rc.top))
					{
						win_pos->flags &= ~(UINT)(SWP_NOMOVE);
						win_pos->x = rect.base.x;
						win_pos->y = rect.base.y;
					}

					win_pos->cx = rect.size.x;
					win_pos->cy = rect.size.y;
				}
				else
				{
					callback->on_frame_size_move(get_interface_handle(hWnd), rect);

					win_pos->x = rect.base.x;
					win_pos->y = rect.base.y;
					win_pos->cx = rect.size.x;
					win_pos->cy = rect.size.y;
				}
			}
			return 0;
		}
		/*case WM_WINDOWPOSCHANGING:
		{
			WINDOWPOS* winPos = (WINDOWPOS*)lParam;
			//winPos->flags |= SWP_NOCOPYBITS;
			if ((winPos->flags & SWP_NOSIZE) == 0)
			{
				point_t min_size(100, 100);
				point_t max_size(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
				callback->calc_min_size(min_size);
				callback->calc_max_size(max_size);

				//handle implementation errors
				max_size.x = max(max_size.x, min_size.x);
				max_size.y = max(max_size.y, min_size.y);
				//adjust win rect
				winPos->cx = min(max(min_size.x, winPos->cx), max_size.x);
				winPos->cy = min(max(min_size.y, winPos->cy), max_size.y);

				//calc client rect with new size
				rect_t client_rect;
				client_rect.base.x = 0;
				client_rect.base.y = 0;
				client_rect.size.x = winPos->cx;
				client_rect.size.y = winPos->cy;

				callback->calc_client_rect(client_rect);
				
				//post resize
				callback->on_resize(point_t(winPos->cx, winPos->cy), client_rect);
			}

			//blocking WM_GETMINMAXINFO:
			return 0;
		}*/
		/*case WM_WINDOWPOSCHANGED: //we ignore WM_SIZE client change
		{
			WINDOWPOS* winPos = (WINDOWPOS*)lParam;
			if ((winPos->flags & SWP_NOSIZE) == 0)
			{
				winPos->
				RECT winRect; //in screen
				GetWindowRect(hWnd, &winRect);
				POINT pointClient;
				pointClient.x = 0;
				pointClient.y = 0;
				ClientToScreen(hWnd, &pointClient);
				pointClient.x -= winRect.left;
				pointClient.y -= winRect.top;

				RECT clientRect;
				GetClientRect(hWnd, &clientRect);

				//call resize in NC pain because microsoft decided its fun to call NCPAINT before telling you about resize
				Other(windowPtr)->_callback->onResize(
				Size(winPos->cx, winPos->cy),
				Rect(pointClient.x, pointClient.y, clientRect.right - clientRect.left, clientRect.bottom - clientRect.top)
				);
			}
			break; //default processing so we get WM_MOVE and WM_SIZE
		}*/
		case WM_MOVE:
		{
			callback->on_move(get_interface_handle(hWnd), point_t((int)(short)LOWORD(lParam), (int)(short)HIWORD(lParam)));
			return 0;
		}
		case WM_SIZE:
		{
			switch (wParam)
			{
			case SIZE_MAXIMIZED:
				callback->on_maximize(get_interface_handle(hWnd), point_t((int)(short)LOWORD(lParam), (int)(short)HIWORD(lParam)));
				break;
			case SIZE_MINIMIZED:
				callback->on_minimize(get_interface_handle(hWnd));
				break;
			case SIZE_RESTORED: //this is a regular resize
				callback->on_resize(get_interface_handle(hWnd), point_t((int)(short)LOWORD(lParam), (int)(short)HIWORD(lParam)));
				break;
			default:
				break;
			}
			return 0;
		}
		case WM_ENTERSIZEMOVE:
		{
			callback->on_tracking_begin(get_interface_handle(hWnd));
			return 0;
		}
		case WM_EXITSIZEMOVE:
		{
			callback->on_tracking_end(get_interface_handle(hWnd));
			return 0;
		}
		case WM_GETMINMAXINFO:
		{
			MINMAXINFO* info = (MINMAXINFO*)lParam;
			//point_t min_track_size(info->ptMinTrackSize.x, info->ptMinTrackSize.y);
			//point_t max_track_size(info->ptMaxTrackSize.x, info->ptMaxTrackSize.y);
			rect_t maximized;
			maximized.base.x = info->ptMaxPosition.x;
			maximized.base.y = info->ptMaxPosition.y;
			maximized.size.x = info->ptMaxSize.x;
			maximized.size.y = info->ptMaxSize.y;


			//callback->on_frame_min_track_size(get_interface_handle(hWnd), min_track_size);
			//callback->on_frame_max_track_size(get_interface_handle(hWnd), max_track_size);
			if (!is_managed)
				callback->on_frame_maximized(get_interface_handle(hWnd), maximized);

			//info->ptMinTrackSize.x = (LONG)min_track_size.x;
			//info->ptMinTrackSize.y = (LONG)min_track_size.y;
			//info->ptMaxTrackSize.x = (LONG)max_track_size.x;
			//info->ptMaxTrackSize.y = (LONG)max_track_size.y;
			info->ptMaxPosition.x = (LONG)maximized.base.x;
			info->ptMaxPosition.y = (LONG)maximized.base.y;
			info->ptMaxSize.x = (LONG)maximized.size.x;
			info->ptMaxSize.y = (LONG)maximized.size.y;

			return 0;
		}
		case WM_SIZING:
		{
			RECT* prc = (RECT*)(lParam);
			rect_t rc;

			rect_to_interface(rc, *prc);
		
			callback->on_tracking(get_interface_handle(hWnd), rc);

			rect_to_native(*prc, rc);
		
			return TRUE;
		}
		case WM_MOVING:
		{
			RECT* prc = (RECT*)(lParam);
			rect_t rc;

			rect_to_interface(rc, *prc);

			callback->on_tracking(get_interface_handle(hWnd), rc);

			rect_to_native(*prc, rc);
			
			return TRUE;
		}
		case WM_NCCALCSIZE:
		{
			if (is_managed)
				break;

			if (wParam == TRUE)
			{
				NCCALCSIZE_PARAMS* params = reinterpret_cast<NCCALCSIZE_PARAMS*>(lParam);
				rect_t rect;
				rect.base.x = 0;
				rect.base.y = 0;
				rect.size.x = params->rgrc[0].right - params->rgrc[0].left,
				rect.size.y = params->rgrc[0].bottom - params->rgrc[0].top;

				callback->on_frame_client_rect(get_interface_handle(hWnd), rect);

				params->rgrc[0].left += rect.base.x;
				params->rgrc[0].top += rect.base.y;
				params->rgrc[0].right = params->rgrc[0].left + rect.size.x;
				params->rgrc[0].bottom = params->rgrc[0].top + rect.size.y;

				//return WVR_VREDRAW | WVR_HREDRAW;
			}
			else
			{
				RECT* params = reinterpret_cast<RECT*>(lParam);
				rect_t rect;
				rect.base.x = 0,
				rect.base.y = 0,
				rect.size.x = params->right - params->left,
				rect.size.y = params->bottom - params->top;

				callback->on_frame_client_rect(get_interface_handle(hWnd), rect);

				params->left += rect.base.x;
				params->top += rect.base.y;
				params->right = params->left + rect.size.x;
				params->bottom = params->top + rect.size.y;
			}

			return 0;
		}
		case WM_NCHITTEST:
		{
			if (is_managed)
				break;

			window::frame_t frame = window::frame_t::client;
			RECT win_rect;
			if (FALSE != GetWindowRect(hWnd, &win_rect))
			{
				callback->on_frame_hit(get_interface_handle(hWnd),
					point_t((SHORT)LOWORD(lParam) - win_rect.left, 
							(SHORT)HIWORD(lParam) - win_rect.top), 
					frame);
				
				switch (frame)
				{
				case window::frame_t::top:
					return HTTOP;
				case window::frame_t::left:
					return HTLEFT;
				case window::frame_t::right:
					return HTRIGHT;
				case window::frame_t::bottom:
					return HTBOTTOM;
				case window::frame_t::top_left:
					return HTTOPLEFT;
				case window::frame_t::top_right:
					return HTTOPRIGHT;
				case window::frame_t::bottom_left:
					return HTBOTTOMLEFT;
				case window::frame_t::bottom_right:
					return HTBOTTOMRIGHT;
				case window::frame_t::caption: //for window movement
					return HTCAPTION;
				case window::frame_t::client:
					return HTCLIENT;
				default:
					return HTCLIENT;
				}
			}
			else
			{
				return HTCLIENT;
			}
		}
		//we dont supress mouse click activation
		case WM_MOUSEACTIVATE:
		{
			return MA_ACTIVATE;
			//return MA_NOACTIVATE;
		}
		case WM_ACTIVATE:
		{	
			if (LOWORD(wParam) == WA_INACTIVE)
			{			
				callback->on_deactivate(get_interface_handle(hWnd), hwnd_to_window((HWND)lParam));
			}
			else
			{
				callback->on_activate(get_interface_handle(hWnd), hwnd_to_window((HWND)lParam));
			}


			return 0;
		}
		case WM_THIS_DESTROY_LATER:
		{
			DestroyWindow(hWnd);
			return 0;
		}
		case WM_KILLFOCUS:
		{
			callback->on_focus_lost(get_interface_handle(hWnd), hwnd_to_window((HWND)wParam));
			return 0;
		}
		case WM_SETFOCUS:
		{
			callback->on_focus_gained(get_interface_handle(hWnd), hwnd_to_window((HWND)wParam));
			return 0;
		}
		case WM_NCACTIVATE:
		{
			if (is_managed)
				break;

			if (TRUE == wParam)
			{
				callback->on_frame_activate(get_interface_handle(hWnd));
			}
			else
			{
				callback->on_frame_deactivate(get_interface_handle(hWnd));
			}

			return TRUE; //accept deactivation
		}
		case WM_SETCURSOR:
		{
			if (reinterpret_cast<HWND>(wParam) != hWnd)
				return FALSE;

			os::cursor_id cursor = NULL;

			DWORD hitTest = LOWORD(lParam);
			switch (hitTest)
			{
			case HTCLIENT:
			{
 				callback->on_mouse_cursor(get_interface_handle(hWnd), cursor);
				if (NULL != cursor)
				{
					SetCursor(get_native_handle(cursor));
					return TRUE;
				}
				break;
			}
			default:
				break;
			}
			break;
		}
		case WM_CAPTURECHANGED:
		{
			callback->on_mouse_capture_lost(get_interface_handle(hWnd));
			return 0;
		}
		case WM_MOUSELEAVE:
		{
			callback->on_mouse_leave(get_interface_handle(hWnd));
			return 0;
		}
		case WM_NCMOUSELEAVE:
		{
			if (is_managed)
				break;

			callback->on_frame_mouse_leave(get_interface_handle(hWnd));
			return 0;
		}
		case WM_MOUSEHOVER:
		{
			callback->on_mouse_hover(get_interface_handle(hWnd), point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_NCMOUSEHOVER:
		{
			if (is_managed)
				break;

			callback->on_frame_mouse_hover(get_interface_handle(hWnd), point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_MOUSEMOVE:
		{	
			callback->on_mouse_move(get_interface_handle(hWnd), point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_NCMOUSEMOVE:
		{
			if (is_managed)
				break;

			RECT rc;
			if (GetWindowRect(hWnd, &rc) != FALSE)
			{
				callback->on_frame_mouse_move(get_interface_handle(hWnd),
					point_t((SHORT)LOWORD(lParam) - rc.left, (SHORT)HIWORD(lParam) - rc.top)
				);
			}

			return 0;
		}
		
		case WM_MOUSEWHEEL:
		{
			POINT point;
			point.x = (SHORT)LOWORD(lParam);
			point.y = (SHORT)HIWORD(lParam);
			ScreenToClient(hWnd, &point);
			
			RECT rect;
			GetClientRect(hWnd, &rect);

			int32_t wheel = GET_WHEEL_DELTA_WPARAM(wParam);
			wheel = (wheel * (1 << 16)) / WHEEL_DELTA;

			if (point.x >= rect.left && point.x < rect.right &&
				point.y >= rect.top && point.y < rect.bottom)
			{
				callback->on_mouse_wheel(get_interface_handle(hWnd), wheel, point_t(point.x, point.y));
			}
			else
			{
				point.x = (SHORT)LOWORD(lParam);
				point.y = (SHORT)HIWORD(lParam);
				GetWindowRect(hWnd, &rect);
				point.x -= rect.left;
				point.y -= rect.top;

				callback->on_frame_mouse_wheel(get_interface_handle(hWnd), wheel, point_t(point.x, point.y));
			}

			return 0;
		}

		case WM_LBUTTONDOWN:
		{
			callback->on_mouse_down(get_interface_handle(hWnd), hid::mouse::button_1, point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_LBUTTONUP:
		{
			callback->on_mouse_up(get_interface_handle(hWnd), hid::mouse::button_1, point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_MBUTTONDOWN:
		{
			callback->on_mouse_down(get_interface_handle(hWnd), hid::mouse::button_3, point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_MBUTTONUP:
		{
			callback->on_mouse_up(get_interface_handle(hWnd), hid::mouse::button_3, point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_RBUTTONDOWN:
		{
			callback->on_mouse_down(get_interface_handle(hWnd), hid::mouse::button_2, point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_RBUTTONUP:
		{
			callback->on_mouse_up(get_interface_handle(hWnd), hid::mouse::button_2, point_t((SHORT)LOWORD(lParam), (SHORT)HIWORD(lParam)));
			return 0;
		}
		case WM_KEYDOWN:
		{
			callback->on_key_down(get_interface_handle(hWnd), hid::keyboard::vk_to_key((hid::keyboard::virtual_key_t)wParam));
			return 0;
		}
		case WM_KEYUP:
		{
			callback->on_key_up(get_interface_handle(hWnd), hid::keyboard::vk_to_key((hid::keyboard::virtual_key_t)wParam));
			return 0;
		}
		case WM_CHAR:
		{
			//small object optimization means no allocations :) 
			string8_t temp = { app::allocator };
			wchar_t	input[2];
			input[0] = (wchar_t)wParam;
			input[1] = L'\0';

			if (!wchar_to_utf8(input, temp))
			{
				callback->on_write(get_interface_handle(hWnd), temp.data());
			}

			return FALSE;
		}

		case WM_NCPAINT:
		{
			if (is_managed)
				break;

			enum : WPARAM
			{
				clear_all = 1,
			};

			enum : LRESULT
			{
				ret_value = 0,
			};
			
			surface_t surface;

			surface.hwnd = hWnd;

			if ((clear_all == wParam) || (NULL == (HRGN)wParam))
			{
				if (FALSE == GetWindowRect(hWnd, &surface.rect))
					return ret_value;

				surface.rect.right -= surface.rect.left;
				surface.rect.bottom -= surface.rect.top;
				surface.rect.left = 0;
				surface.rect.top = 0;
			}
			else
			{
				if (0 == GetRgnBox((HRGN)wParam, &surface.rect))
					return ret_value;
			}

			HDC hdc = GetWindowDC(hWnd);
			if (NULL == hdc)
				return ret_value;

			surface.hdc_dev = hdc;
			surface.hdc_mem = CreateCompatibleDC(hdc);
			
			if (NULL != surface.hdc_mem)
			{
				callback->on_frame_paint(get_interface_handle(hWnd), &surface);

				DeleteDC(surface.hdc_mem);
			}

			ReleaseDC(hWnd, hdc);

			return ret_value;

			/*enum : WPARAM
			{
				CLEAR_WHOLE_NC_AREA = 1,
			};

			enum : LRESULT
			{
				ret_value = 0,
			};

			RECT rcWindow;
			if (FALSE == GetWindowRect(hWnd, &rcWindow))
				return ret_value;

			if (wParam != CLEAR_WHOLE_NC_AREA && (HRGN)wParam != NULL)
			{
				//we need this in window coords
				int complexity = OffsetRgn((HRGN)wParam, -rcWindow.left, -rcWindow.top);
				if (ERROR == complexity)
				{
					wParam = CLEAR_WHOLE_NC_AREA;
				}
			}

			{
				RECT clip_rect;
				HDC hdc = GetWindowDC(hWnd);
				if(NULL == hdc)
					return ret_value;

				HRGN clipFrame = NULL;
				HRGN clipClient = NULL;
				if (wParam == CLEAR_WHOLE_NC_AREA || (HRGN)wParam == NULL)
				{
					POINT pointClient;
					pointClient.x = 0;
					pointClient.y = 0;
					ClientToScreen(hWnd, &pointClient);
					pointClient.x -= rcWindow.left;
					pointClient.y -= rcWindow.top;

					RECT rcClient; //we need this in window coords
					GetClientRect(hWnd, &rcClient);
					rcClient.left += pointClient.x;
					rcClient.right += pointClient.x;
					rcClient.top += pointClient.y;
					rcClient.bottom += pointClient.y;

					//clipping client rect
					RECT rcWindowLocal; //we need this in window coords
					rcWindowLocal.left = 0;
					rcWindowLocal.top = 0;
					rcWindowLocal.right = rcWindow.right - rcWindow.left;
					rcWindowLocal.bottom = rcWindow.bottom - rcWindow.top;

					clipFrame = CreateRectRgnIndirect(&rcWindowLocal);
					clipClient = CreateRectRgnIndirect(&rcClient);
					int regionResult = CombineRgn(clipFrame, clipFrame, clipClient, RGN_DIFF);
					if (regionResult != ERROR)
					{
						GetRgnBox(clipFrame, &clip_rect);
						regionResult = SelectClipRgn(hdc, clipFrame);
						if (ERROR == regionResult)
						{
							DeleteObject(clipFrame);
							DeleteObject(clipClient);
							return ret_value;
						}
					}
					else
					{
						DeleteObject(clipFrame);
						DeleteObject(clipClient);
						return ret_value;
					}
				}
				else
				{
					GetRgnBox((HRGN)wParam, &clip_rect);
					int regionResult = SelectClipRgn(hdc, (HRGN)wParam);
					if (ERROR == regionResult)
						return ret_value;
				}

				surface_t surface;
				surface.hdc = hdc;
				surface.rect = clip_rect;
				surface.hwnd = hWnd;

				callback->on_paint_frame(window_win, &surface);
			
				//cleanup
				//SelectClipRgn(hdc, NULL); //not needed winapi makes a copy
				ReleaseDC(hWnd, hdc);
				if (wParam == CLEAR_WHOLE_NC_AREA || (HRGN)wParam == NULL)
				{
					DeleteObject(clipFrame);
					DeleteObject(clipClient);
				}
			}
			
			return ret_value;*/
		}
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			surface_t surface;
			surface.hdc_dev = BeginPaint(hWnd, &ps);
			surface.rect = ps.rcPaint;
			surface.hwnd = hWnd;

			if (surface.hdc_dev != NULL)
			{
				surface.hdc_mem = CreateCompatibleDC(surface.hdc_dev);

				if (NULL != surface.hdc_mem)
				{
					callback->on_paint(get_interface_handle(hWnd), &surface);

					DeleteDC(surface.hdc_mem);
				}

				EndPaint(hWnd, &ps);
			}
			
			return 0;
		}
		case WM_ERASEBKGND:
		{
			//ValidateRect(hWnd, NULL); //validate whole client area
			return TRUE; //no flickering please
		}
		
		default:
			break;
		}

		return DefWindowProcW(hWnd, msg, wParam, lParam);
	}

	const char8_t* msg_to_string(UINT msg)
	{
#define O_WIN_MSG_TEXT(code, msg) case code: return msg;
		switch (msg)
		{
		O_WIN_MSG_TEXT(0x0000, "WM_NULL					")
		O_WIN_MSG_TEXT(0x0001, "WM_CREATE				")
		O_WIN_MSG_TEXT(0x0002, "WM_DESTROY				")
		O_WIN_MSG_TEXT(0x0003, "WM_MOVE					")
		O_WIN_MSG_TEXT(0x0005, "WM_SIZE					")
		O_WIN_MSG_TEXT(0x0006, "WM_ACTIVATE				")
		O_WIN_MSG_TEXT(0x0007, "WM_SETFOCUS				")
		O_WIN_MSG_TEXT(0x0008, "WM_KILLFOCUS			")
		O_WIN_MSG_TEXT(0x000a, "WM_ENABLE				")
		O_WIN_MSG_TEXT(0x000b, "WM_SETREDRAW			")
		O_WIN_MSG_TEXT(0x000c, "WM_SETTEXT				")
		O_WIN_MSG_TEXT(0x000d, "WM_GETTEXT				")
		O_WIN_MSG_TEXT(0x000e, "WM_GETTEXTLENGTH		")
		O_WIN_MSG_TEXT(0x000f, "WM_PAINT				")
		O_WIN_MSG_TEXT(0x0010, "WM_CLOSE				")
		O_WIN_MSG_TEXT(0x0011, "WM_QUERYENDSESSION		")
		O_WIN_MSG_TEXT(0x0012, "WM_QUIT					")
		O_WIN_MSG_TEXT(0x0013, "WM_QUERYOPEN			")
		O_WIN_MSG_TEXT(0x0014, "WM_ERASEBKGND			")
		O_WIN_MSG_TEXT(0x0015, "WM_SYSCOLORCHANGE		")
		O_WIN_MSG_TEXT(0x0016, "WM_ENDSESSION			")
		O_WIN_MSG_TEXT(0x0018, "WM_SHOWWINDOW			")
		O_WIN_MSG_TEXT(0x0019, "WM_CTLCOLOR				")
		O_WIN_MSG_TEXT(0x001a, "WM_WININICHANGE			")
		O_WIN_MSG_TEXT(0x001b, "WM_DEVMODECHANGE		")
		O_WIN_MSG_TEXT(0x001c, "WM_ACTIVATEAPP			")
		O_WIN_MSG_TEXT(0x001d, "WM_FONTCHANGE			")
		O_WIN_MSG_TEXT(0x001e, "WM_TIMECHANGE			")
		O_WIN_MSG_TEXT(0x001f, "WM_CANCELMODE			")
		O_WIN_MSG_TEXT(0x0020, "WM_SETCURSOR			")
		O_WIN_MSG_TEXT(0x0021, "WM_MOUSEACTIVATE		")
		O_WIN_MSG_TEXT(0x0022, "WM_CHILDACTIVATE		")
		O_WIN_MSG_TEXT(0x0023, "WM_QUEUESYNC			")
		O_WIN_MSG_TEXT(0x0024, "WM_GETMINMAXINFO		")
		O_WIN_MSG_TEXT(0x0026, "WM_PAINTICON			")
		O_WIN_MSG_TEXT(0x0027, "WM_ICONERASEBKGND		")
		O_WIN_MSG_TEXT(0x0028, "WM_NEXTDLGCTL			")
		O_WIN_MSG_TEXT(0x002a, "WM_SPOOLERSTATUS		")
		O_WIN_MSG_TEXT(0x002b, "WM_DRAWITEM				")
		O_WIN_MSG_TEXT(0x002c, "WM_MEASUREITEM			")
		O_WIN_MSG_TEXT(0x002d, "WM_DELETEITEM			")
		O_WIN_MSG_TEXT(0x002e, "WM_VKEYTOITEM			")
		O_WIN_MSG_TEXT(0x002f, "WM_CHARTOITEM			")
		O_WIN_MSG_TEXT(0x0030, "WM_SETFONT				")
		O_WIN_MSG_TEXT(0x0031, "WM_GETFONT				")
		O_WIN_MSG_TEXT(0x0032, "WM_SETHOTKEY			")
		O_WIN_MSG_TEXT(0x0033, "WM_GETHOTKEY			")
		O_WIN_MSG_TEXT(0x0037, "WM_QUERYDRAGICON		")
		O_WIN_MSG_TEXT(0x0039, "WM_COMPAREITEM			")
		O_WIN_MSG_TEXT(0x003d, "WM_GETOBJECT			")
		O_WIN_MSG_TEXT(0x0041, "WM_COMPACTING			")
		O_WIN_MSG_TEXT(0x0044, "WM_COMMNOTIFY			")
		O_WIN_MSG_TEXT(0x0046, "WM_WINDOWPOSCHANGING	")
		O_WIN_MSG_TEXT(0x0047, "WM_WINDOWPOSCHANGED		")
		O_WIN_MSG_TEXT(0x0048, "WM_POWER				")
		O_WIN_MSG_TEXT(0x0049, "WM_COPYGLOBALDATA		")
		O_WIN_MSG_TEXT(0x004a, "WM_COPYDATA				")
		O_WIN_MSG_TEXT(0x004b, "WM_CANCELJOURNAL		")
		O_WIN_MSG_TEXT(0x004e, "WM_NOTIFY				")
		O_WIN_MSG_TEXT(0x0050, "WM_INPUTLANGCHANGEREQUEST")
		O_WIN_MSG_TEXT(0x0051, "WM_INPUTLANGCHANGE		")
		O_WIN_MSG_TEXT(0x0052, "WM_TCARD				")
		O_WIN_MSG_TEXT(0x0053, "WM_HELP					")
		O_WIN_MSG_TEXT(0x0054, "WM_USERCHANGED			")
		O_WIN_MSG_TEXT(0x0055, "WM_NOTIFYFORMAT			")
		O_WIN_MSG_TEXT(0x007b, "WM_CONTEXTMENU			")
		O_WIN_MSG_TEXT(0x007c, "WM_STYLECHANGING		")
		O_WIN_MSG_TEXT(0x007d, "WM_STYLECHANGED			")
		O_WIN_MSG_TEXT(0x007e, "WM_DISPLAYCHANGE		")
		O_WIN_MSG_TEXT(0x007f, "WM_GETICON				")
		O_WIN_MSG_TEXT(0x0080, "WM_SETICON				")
		O_WIN_MSG_TEXT(0x0081, "WM_NCCREATE				")
		O_WIN_MSG_TEXT(0x0082, "WM_NCDESTROY			")
		O_WIN_MSG_TEXT(0x0083, "WM_NCCALCSIZE			")
		O_WIN_MSG_TEXT(0x0084, "WM_NCHITTEST			")
		O_WIN_MSG_TEXT(0x0085, "WM_NCPAINT				")
		O_WIN_MSG_TEXT(0x0086, "WM_NCACTIVATE			")
		O_WIN_MSG_TEXT(0x0087, "WM_GETDLGCODE			")
		O_WIN_MSG_TEXT(0x0088, "WM_SYNCPAINT			")
		O_WIN_MSG_TEXT(0x00a0, "WM_NCMOUSEMOVE			")
		O_WIN_MSG_TEXT(0x00a1, "WM_NCLBUTTONDOWN		")
		O_WIN_MSG_TEXT(0x00a2, "WM_NCLBUTTONUP			")
		O_WIN_MSG_TEXT(0x00a3, "WM_NCLBUTTONDBLCLK		")
		O_WIN_MSG_TEXT(0x00a4, "WM_NCRBUTTONDOWN		")
		O_WIN_MSG_TEXT(0x00a5, "WM_NCRBUTTONUP			")
		O_WIN_MSG_TEXT(0x00a6, "WM_NCRBUTTONDBLCLK		")
		O_WIN_MSG_TEXT(0x00a7, "WM_NCMBUTTONDOWN		")
		O_WIN_MSG_TEXT(0x00a8, "WM_NCMBUTTONUP			")
		O_WIN_MSG_TEXT(0x00a9, "WM_NCMBUTTONDBLCLK		")
		O_WIN_MSG_TEXT(0x00ab, "WM_NCXBUTTONDOWN		")
		O_WIN_MSG_TEXT(0x00ac, "WM_NCXBUTTONUP			")
		O_WIN_MSG_TEXT(0x00ad, "WM_NCXBUTTONDBLCLK		")
		O_WIN_MSG_TEXT(0x00b0, "EM_GETSEL				")
		O_WIN_MSG_TEXT(0x00b1, "EM_SETSEL				")
		O_WIN_MSG_TEXT(0x00b2, "EM_GETRECT				")
		O_WIN_MSG_TEXT(0x00b3, "EM_SETRECT				")
		O_WIN_MSG_TEXT(0x00b4, "EM_SETRECTNP			")
		O_WIN_MSG_TEXT(0x00b5, "EM_SCROLL				")
		O_WIN_MSG_TEXT(0x00b6, "EM_LINESCROLL			")
		O_WIN_MSG_TEXT(0x00b7, "EM_SCROLLCARET			")
		O_WIN_MSG_TEXT(0x00b8, "EM_GETMODIFY			")
		O_WIN_MSG_TEXT(0x00b9, "EM_SETMODIFY			")
		O_WIN_MSG_TEXT(0x00ba, "EM_GETLINECOUNT			")
		O_WIN_MSG_TEXT(0x00bb, "EM_LINEINDEX			")
		O_WIN_MSG_TEXT(0x00bc, "EM_SETHANDLE			")
		O_WIN_MSG_TEXT(0x00bd, "EM_GETHANDLE			")
		O_WIN_MSG_TEXT(0x00be, "EM_GETTHUMB				")
		O_WIN_MSG_TEXT(0x00c1, "EM_LINELENGTH			")
		O_WIN_MSG_TEXT(0x00c2, "EM_REPLACESEL			")
		O_WIN_MSG_TEXT(0x00c3, "EM_SETFONT				")
		O_WIN_MSG_TEXT(0x00c4, "EM_GETLINE				")
		//O_WIN_MSG_TEXT(0x00c5		,"EM_LIMITTEXT		")
		O_WIN_MSG_TEXT(0x00c5, "EM_SETLIMITTEXT			")
		O_WIN_MSG_TEXT(0x00c6, "EM_CANUNDO				")
		O_WIN_MSG_TEXT(0x00c7, "EM_UNDO					")
		O_WIN_MSG_TEXT(0x00c8, "EM_FMTLINES				")
		O_WIN_MSG_TEXT(0x00c9, "EM_LINEFROMCHAR			")
		O_WIN_MSG_TEXT(0x00ca, "EM_SETWORDBREAK			")
		O_WIN_MSG_TEXT(0x00cb, "EM_SETTABSTOPS			")
		O_WIN_MSG_TEXT(0x00cc, "EM_SETPASSWORDCHAR		")
		O_WIN_MSG_TEXT(0x00cd, "EM_EMPTYUNDOBUFFER		")
		O_WIN_MSG_TEXT(0x00ce, "EM_GETFIRSTVISIBLELINE	")
		O_WIN_MSG_TEXT(0x00cf, "EM_SETREADONLY			")
		O_WIN_MSG_TEXT(0x00d0, "EM_SETWORDBREAKPROC		")
		O_WIN_MSG_TEXT(0x00d1, "EM_GETWORDBREAKPROC		")
		O_WIN_MSG_TEXT(0x00d2, "EM_GETPASSWORDCHAR		")
		O_WIN_MSG_TEXT(0x00d3, "EM_SETMARGINS			")
		O_WIN_MSG_TEXT(0x00d4, "EM_GETMARGINS			")
		O_WIN_MSG_TEXT(0x00d5, "EM_GETLIMITTEXT			")
		O_WIN_MSG_TEXT(0x00d6, "EM_POSFROMCHAR			")
		O_WIN_MSG_TEXT(0x00d7, "EM_CHARFROMPOS			")
		O_WIN_MSG_TEXT(0x00d8, "EM_SETIMESTATUS			")
		O_WIN_MSG_TEXT(0x00d9, "EM_GETIMESTATUS			")
		O_WIN_MSG_TEXT(0x00e0, "SBM_SETPOS				")
		O_WIN_MSG_TEXT(0x00e1, "SBM_GETPOS				")
		O_WIN_MSG_TEXT(0x00e2, "SBM_SETRANGE			")
		O_WIN_MSG_TEXT(0x00e3, "SBM_GETRANGE			")
		O_WIN_MSG_TEXT(0x00e4, "SBM_ENABLE_ARROWS		")
		O_WIN_MSG_TEXT(0x00e6, "SBM_SETRANGEREDRAW		")
		O_WIN_MSG_TEXT(0x00e9, "SBM_SETSCROLLINFO		")
		O_WIN_MSG_TEXT(0x00ea, "SBM_GETSCROLLINFO		")
		O_WIN_MSG_TEXT(0x00eb, "SBM_GETSCROLLBARINFO	")
		O_WIN_MSG_TEXT(0x00f0, "BM_GETCHECK				")
		O_WIN_MSG_TEXT(0x00f1, "BM_SETCHECK				")
		O_WIN_MSG_TEXT(0x00f2, "BM_GETSTATE				")
		O_WIN_MSG_TEXT(0x00f3, "BM_SETSTATE				")
		O_WIN_MSG_TEXT(0x00f4, "BM_SETSTYLE				")
		O_WIN_MSG_TEXT(0x00f5, "BM_CLICK				")
		O_WIN_MSG_TEXT(0x00f6, "BM_GETIMAGE				")
		O_WIN_MSG_TEXT(0x00f7, "BM_SETIMAGE				")
		O_WIN_MSG_TEXT(0x00f8, "BM_SETDONTCLICK			")
		O_WIN_MSG_TEXT(0x00ff, "WM_INPUT				")
		O_WIN_MSG_TEXT(0x0100, "WM_KEYDOWN				")
		//O_WIN_MSG_TEXT(0x0100		,"WM_KEYFIRST		")
		O_WIN_MSG_TEXT(0x0101, "WM_KEYUP				")
		O_WIN_MSG_TEXT(0x0102, "WM_CHAR					")
		O_WIN_MSG_TEXT(0x0103, "WM_DEADCHAR				")
		O_WIN_MSG_TEXT(0x0104, "WM_SYSKEYDOWN			")
		O_WIN_MSG_TEXT(0x0105, "WM_SYSKEYUP				")
		O_WIN_MSG_TEXT(0x0106, "WM_SYSCHAR				")
		O_WIN_MSG_TEXT(0x0107, "WM_SYSDEADCHAR			")
		O_WIN_MSG_TEXT(0x0108, "WM_KEYLAST				")
		O_WIN_MSG_TEXT(0x0109, "WM_UNICHAR				")
		//O_WIN_MSG_TEXT(0x0109		,"WM_WNT_CONVERTREQU")
		O_WIN_MSG_TEXT(0x010a, "WM_CONVERTREQUEST		")
		O_WIN_MSG_TEXT(0x010b, "WM_CONVERTRESULT		")
		O_WIN_MSG_TEXT(0x010c, "WM_INTERIM				")
		O_WIN_MSG_TEXT(0x010d, "WM_IME_STARTCOMPOSITION	")
		O_WIN_MSG_TEXT(0x010e, "WM_IME_ENDCOMPOSITION	")
		O_WIN_MSG_TEXT(0x010f, "WM_IME_COMPOSITION		")
		//O_WIN_MSG_TEXT(0x010f		,"WM_IME_KEYLAST	")
		O_WIN_MSG_TEXT(0x0110, "WM_INITDIALOG			")
		O_WIN_MSG_TEXT(0x0111, "WM_COMMAND				")
		O_WIN_MSG_TEXT(0x0112, "WM_SYSCOMMAND			")
		O_WIN_MSG_TEXT(0x0113, "WM_TIMER				")
		O_WIN_MSG_TEXT(0x0114, "WM_HSCROLL				")
		O_WIN_MSG_TEXT(0x0115, "WM_VSCROLL				")
		O_WIN_MSG_TEXT(0x0116, "WM_INITMENU				")
		O_WIN_MSG_TEXT(0x0117, "WM_INITMENUPOPUP		")
		O_WIN_MSG_TEXT(0x0118, "WM_SYSTIMER				")
		O_WIN_MSG_TEXT(0x011f, "WM_MENUSELECT			")
		O_WIN_MSG_TEXT(0x0120, "WM_MENUCHAR				")
		O_WIN_MSG_TEXT(0x0121, "WM_ENTERIDLE			")
		O_WIN_MSG_TEXT(0x0122, "WM_MENURBUTTONUP		")
		O_WIN_MSG_TEXT(0x0123, "WM_MENUDRAG				")
		O_WIN_MSG_TEXT(0x0124, "WM_MENUGETOBJECT		")
		O_WIN_MSG_TEXT(0x0125, "WM_UNINITMENUPOPUP		")
		O_WIN_MSG_TEXT(0x0126, "WM_MENUCOMMAND			")
		O_WIN_MSG_TEXT(0x0127, "WM_CHANGEUISTATE		")
		O_WIN_MSG_TEXT(0x0128, "WM_UPDATEUISTATE		")
		O_WIN_MSG_TEXT(0x0129, "WM_QUERYUISTATE			")
		O_WIN_MSG_TEXT(0x0132, "WM_CTLCOLORMSGBOX		")
		O_WIN_MSG_TEXT(0x0133, "WM_CTLCOLOREDIT			")
		O_WIN_MSG_TEXT(0x0134, "WM_CTLCOLORLISTBOX		")
		O_WIN_MSG_TEXT(0x0135, "WM_CTLCOLORBTN			")
		O_WIN_MSG_TEXT(0x0136, "WM_CTLCOLORDLG			")
		O_WIN_MSG_TEXT(0x0137, "WM_CTLCOLORSCROLLBAR	")
		O_WIN_MSG_TEXT(0x0138, "WM_CTLCOLORSTATIC		")
		//O_WIN_MSG_TEXT(0x0200		,"WM_MOUSEFIRST		")
		O_WIN_MSG_TEXT(0x0200, "WM_MOUSEMOVE			")
		O_WIN_MSG_TEXT(0x0201, "WM_LBUTTONDOWN			")
		O_WIN_MSG_TEXT(0x0202, "WM_LBUTTONUP			")
		O_WIN_MSG_TEXT(0x0203, "WM_LBUTTONDBLCLK		")
		O_WIN_MSG_TEXT(0x0204, "WM_RBUTTONDOWN			")
		O_WIN_MSG_TEXT(0x0205, "WM_RBUTTONUP			")
		O_WIN_MSG_TEXT(0x0206, "WM_RBUTTONDBLCLK		")
		O_WIN_MSG_TEXT(0x0207, "WM_MBUTTONDOWN			")
		O_WIN_MSG_TEXT(0x0208, "WM_MBUTTONUP			")
		O_WIN_MSG_TEXT(0x0209, "WM_MBUTTONDBLCLK		")
		//O_WIN_MSG_TEXT(0x0209		,"WM_MOUSELAST		")
		O_WIN_MSG_TEXT(0x020a, "WM_MOUSEWHEEL			")
		O_WIN_MSG_TEXT(0x020b, "WM_XBUTTONDOWN			")
		O_WIN_MSG_TEXT(0x020c, "WM_XBUTTONUP			")
		O_WIN_MSG_TEXT(0x020d, "WM_XBUTTONDBLCLK		")
		O_WIN_MSG_TEXT(0x0210, "WM_PARENTNOTIFY			")
		O_WIN_MSG_TEXT(0x0211, "WM_ENTERMENULOOP		")
		O_WIN_MSG_TEXT(0x0212, "WM_EXITMENULOOP			")
		O_WIN_MSG_TEXT(0x0213, "WM_NEXTMENU				")
		O_WIN_MSG_TEXT(0x0214, "WM_SIZING				")
		O_WIN_MSG_TEXT(0x0215, "WM_CAPTURECHANGED		")
		O_WIN_MSG_TEXT(0x0216, "WM_MOVING				")
		O_WIN_MSG_TEXT(0x0218, "WM_POWERBROADCAST		")
		O_WIN_MSG_TEXT(0x0219, "WM_DEVICECHANGE			")
		O_WIN_MSG_TEXT(0x0220, "WM_MDICREATE			")
		O_WIN_MSG_TEXT(0x0221, "WM_MDIDESTROY			")
		O_WIN_MSG_TEXT(0x0222, "WM_MDIACTIVATE			")
		O_WIN_MSG_TEXT(0x0223, "WM_MDIRESTORE			")
		O_WIN_MSG_TEXT(0x0224, "WM_MDINEXT				")
		O_WIN_MSG_TEXT(0x0225, "WM_MDIMAXIMIZE			")
		O_WIN_MSG_TEXT(0x0226, "WM_MDITILE				")
		O_WIN_MSG_TEXT(0x0227, "WM_MDICASCADE			")
		O_WIN_MSG_TEXT(0x0228, "WM_MDIICONARRANGE		")
		O_WIN_MSG_TEXT(0x0229, "WM_MDIGETACTIVE			")
		O_WIN_MSG_TEXT(0x0230, "WM_MDISETMENU			")
		O_WIN_MSG_TEXT(0x0231, "WM_ENTERSIZEMOVE		")
		O_WIN_MSG_TEXT(0x0232, "WM_EXITSIZEMOVE			")
		O_WIN_MSG_TEXT(0x0233, "WM_DROPFILES			")
		O_WIN_MSG_TEXT(0x0234, "WM_MDIREFRESHMENU		")
		O_WIN_MSG_TEXT(0x0280, "WM_IME_REPORT			")
		O_WIN_MSG_TEXT(0x0281, "WM_IME_SETCONTEXT		")
		O_WIN_MSG_TEXT(0x0282, "WM_IME_NOTIFY			")
		O_WIN_MSG_TEXT(0x0283, "WM_IME_CONTROL			")
		O_WIN_MSG_TEXT(0x0284, "WM_IME_COMPOSITIONFULL	")
		O_WIN_MSG_TEXT(0x0285, "WM_IME_SELECT			")
		O_WIN_MSG_TEXT(0x0286, "WM_IME_CHAR				")
		O_WIN_MSG_TEXT(0x0288, "WM_IME_REQUEST			")
		//O_WIN_MSG_TEXT(0x0290		,"WM_IMEKEYDOWN		")
		O_WIN_MSG_TEXT(0x0290, "WM_IME_KEYDOWN			")
		//O_WIN_MSG_TEXT(0x0291		,"WM_IMEKEYUP		")
		O_WIN_MSG_TEXT(0x0291, "WM_IME_KEYUP			")
		O_WIN_MSG_TEXT(0x02a0, "WM_NCMOUSEHOVER			")
		O_WIN_MSG_TEXT(0x02a1, "WM_MOUSEHOVER			")
		O_WIN_MSG_TEXT(0x02a2, "WM_NCMOUSELEAVE			")
		O_WIN_MSG_TEXT(0x02a3, "WM_MOUSELEAVE			")
		O_WIN_MSG_TEXT(0x0300, "WM_CUT					")
		O_WIN_MSG_TEXT(0x0301, "WM_COPY					")
		O_WIN_MSG_TEXT(0x0302, "WM_PASTE				")
		O_WIN_MSG_TEXT(0x0303, "WM_CLEAR				")
		O_WIN_MSG_TEXT(0x0304, "WM_UNDO					")
		O_WIN_MSG_TEXT(0x0305, "WM_RENDERFORMAT			")
		O_WIN_MSG_TEXT(0x0306, "WM_RENDERALLFORMATS		")
		O_WIN_MSG_TEXT(0x0307, "WM_DESTROYCLIPBOARD		")
		O_WIN_MSG_TEXT(0x0308, "WM_DRAWCLIPBOARD		")
		O_WIN_MSG_TEXT(0x0309, "WM_PAINTCLIPBOARD		")
		O_WIN_MSG_TEXT(0x030a, "WM_VSCROLLCLIPBOARD		")
		O_WIN_MSG_TEXT(0x030b, "WM_SIZECLIPBOARD		")
		O_WIN_MSG_TEXT(0x030c, "WM_ASKCBFORMATNAME		")
		O_WIN_MSG_TEXT(0x030d, "WM_CHANGECBCHAIN		")
		O_WIN_MSG_TEXT(0x030e, "WM_HSCROLLCLIPBOARD		")
		O_WIN_MSG_TEXT(0x030f, "WM_QUERYNEWPALETTE		")
		O_WIN_MSG_TEXT(0x0310, "WM_PALETTEISCHANGING	")
		O_WIN_MSG_TEXT(0x0311, "WM_PALETTECHANGED		")
		O_WIN_MSG_TEXT(0x0312, "WM_HOTKEY				")
		O_WIN_MSG_TEXT(0x0317, "WM_PRINT				")
		O_WIN_MSG_TEXT(0x0318, "WM_PRINTCLIENT			")
		O_WIN_MSG_TEXT(0x0319, "WM_APPCOMMAND			")
		O_WIN_MSG_TEXT(0x0358, "WM_HANDHELDFIRST		")
		O_WIN_MSG_TEXT(0x035f, "WM_HANDHELDLAST			")
		O_WIN_MSG_TEXT(0x0360, "WM_AFXFIRST				")
		O_WIN_MSG_TEXT(0x037f, "WM_AFXLAST				")
		O_WIN_MSG_TEXT(0x0380, "WM_PENWINFIRST			")
		O_WIN_MSG_TEXT(0x0381, "WM_RCRESULT				")
		O_WIN_MSG_TEXT(0x0382, "WM_HOOKRCRESULT			")
		//O_WIN_MSG_TEXT(0x0383		,"WM_GLOBALRCCHANGE	")
		O_WIN_MSG_TEXT(0x0383, "WM_PENMISCINFO			")
		O_WIN_MSG_TEXT(0x0384, "WM_SKB					")
		//O_WIN_MSG_TEXT(0x0385		,"WM_HEDITCTL		")
		O_WIN_MSG_TEXT(0x0385, "WM_PENCTL				")
		O_WIN_MSG_TEXT(0x0386, "WM_PENMISC				")
		O_WIN_MSG_TEXT(0x0387, "WM_CTLINIT				")
		O_WIN_MSG_TEXT(0x0388, "WM_PENEVENT				")
		O_WIN_MSG_TEXT(0x038f, "WM_PENWINLAST			")
		//O_WIN_MSG_TEXT(0x0400		,"DDM_SETFMT							")
		//O_WIN_MSG_TEXT(0x0400		,"DM_GETDEFID							")
		//O_WIN_MSG_TEXT(0x0400		,"NIN_SELECT							")
		//O_WIN_MSG_TEXT(0x0400		,"TBM_GETPOS							")
		//O_WIN_MSG_TEXT(0x0400		,"WM_PSD_PAGESETUPDLG					")
		O_WIN_MSG_TEXT(0x0400, "WM_USER")
		
		default:
			return "unknown msg";
		};
	}

} }
