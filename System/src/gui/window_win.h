#pragma once
#include "system\gui\gui.h"
#include "system\gui\window.h"
#include "gui_win.h"

namespace outland { namespace  os
{
	//--------------------------------------------------
	class window_manager_t
	{
	private:
		struct data_t
		{
			window_t*	callback;
			HWND		hwnd;
		};

	private:
		array<data_t>	_data_array;

	public:
		static LRESULT CALLBACK window_callback(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

		window_manager_t(error_t& err);
		window_t*	window_get(HWND hWnd);
		error_t		window_create_begin(window_t* win);
		void		window_create_fail();
		void		window_create_end(HWND hWnd);
		void		window_destroy(HWND hWnd);
	};

	//--------------------------------------------------
	struct window_style_t
	{
		const wchar_t* class_name;
		DWORD style_win;
		DWORD style_ex;
	};


	//--------------------------------------------------
	HWND get_native_handle(window_id window);
	window_id get_interface_handle(HWND window);


	//--------------------------------------------------
	const char8_t* msg_to_string(UINT msg);

	namespace window
	{
		error_t	init();
		void	clean();

		error_t get_native_style(window_style_t& ws, window_id parent, window::style_t style);
	}

} }
