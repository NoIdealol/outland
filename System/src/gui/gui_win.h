#pragma once
#include "system\gui\gui.h"

#include "window_win.h"
#include "surface_win.h"

namespace outland { namespace  os
{
	//--------------------------------------------------
	HCURSOR get_native_handle(cursor_id cursor);

} }