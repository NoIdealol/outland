#include "pch.h"
#include "gui_win.h"


namespace outland { namespace  os
{
	//--------------------------------------------------
	HCURSOR get_native_handle(cursor_id cursor)
	{
		return reinterpret_cast<HCURSOR>(cursor);
	}

	//--------------------------------------------------
	void cursor::hide()
	{
		ShowCursor(FALSE);
	}

	//--------------------------------------------------
	void cursor::show()
	{
		ShowCursor(TRUE);
	}

	//--------------------------------------------------
	error_t cursor::create(cursor_id& cursor, size_t width, size_t height, color_t* pixels, size_t hotspot_x, size_t hotspot_y)
	{
		return err_not_implemented;
	}

	//--------------------------------------------------
	void cursor::destroy(cursor_id cursor)
	{

	}

	//--------------------------------------------------
	error_t gui::create()
	{
		error_t err;

		err = surface::init();
		if (err)
			return err;

		err = texture::init();
		if (err)
		{
			surface::clean();
			return err;
		}

		err = window::init();
		if (err)
		{
			texture::clean();
			surface::clean();
			return err;
		}

		return err_ok;
	}

	//--------------------------------------------------
	void gui::destroy()
	{
		window::clean();
		texture::clean();
		surface::clean();
	}

} }