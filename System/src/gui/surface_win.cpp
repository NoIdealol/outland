#include "pch.h"
#include "surface_win.h"
#include "window_win.h"

namespace outland { namespace  os
{
	namespace g
	{
		surface_manager_t* surface_manager = nullptr;
		texture_manager_t* texture_manager = nullptr;
	}

	texture_id const texture::null = nullptr;

	//--------------------------------------------------
	inline COLORREF to_gdi_color(color_t color) 
	{
		return ((color >> 16) & 0x000000FF) | (color & 0x0000FF00) | ((color & 0x000000FF) << 16);
	}

	//--------------------------------------------------
	error_t bitmap_info_get(BITMAPINFO& bitmap_info, bitmap_t const& bitmap)
	{
		ZeroMemory(&bitmap_info, sizeof(bitmap_info));
		bitmap_info.bmiHeader.biSize = sizeof(bitmap_info.bmiHeader);
		bitmap_info.bmiHeader.biWidth = (LONG)bitmap.width;
		bitmap_info.bmiHeader.biHeight = -(LONG)bitmap.height;
		bitmap_info.bmiHeader.biPlanes = 1;
		switch (bitmap.format)
		{
		case bitmap_t::b8_g8_r8:
			bitmap_info.bmiHeader.biBitCount = 24;
			break;
		case bitmap_t::b8_g8_r8_x8:
			bitmap_info.bmiHeader.biBitCount = 32;
			break;
		default:
			return err_bad_data;
		}

		enum : size_t
		{
			mask_low = 0b11,
			mask_all = c::max_size - mask_low,
		};

		if (bitmap.scanline != (((size_t)(bitmap.width * bitmap_info.bmiHeader.biBitCount / 8) + mask_low) & mask_all))
			return err_bad_data;

		return err_ok;
	}

	//////////////////////////////////////////////////
	// TEXTURE
	//////////////////////////////////////////////////

	//--------------------------------------------------
	error_t	texture::init()
	{
		g::texture_manager = mem<texture_manager_t>::alloc(app::allocator);
		if (nullptr == g::texture_manager)
			return err_out_of_memory;

		return err_ok;
	}

	//--------------------------------------------------
	void	texture::clean()
	{
		if (nullptr == g::texture_manager)
			return;

		mem<texture_manager_t>::free(app::allocator, g::texture_manager);
		g::texture_manager = nullptr;
	}

	//--------------------------------------------------
	error_t		texture::create_ddb(texture_id& texture, bitmap_t const& bitmap)
	{
		error_t err;
		texture_id t;

		

		err = g::texture_manager->create(t);
		if (err)
			return err;

		t->hbitmap = NULL;
		t->bitmap = bitmap;
		t->bitmap.pixels = nullptr;
		t->bitmap.format = bitmap_t::device_dependant_bitmap;
		t->bitmap.scanline = 0;

		HDC screen_dc = GetDC(NULL);
		if (NULL == screen_dc)
			goto _error;

		t->hbitmap = CreateCompatibleBitmap(screen_dc, (int)bitmap.width, (int)bitmap.height);	
		if (NULL == t->hbitmap)
			goto _error;		

		if (nullptr != bitmap.pixels)
		{
			BITMAPINFO bitmap_info;
			err = bitmap_info_get(bitmap_info, bitmap);
			if (err)
				goto _error;

			int lines_set = SetDIBits(
				screen_dc,
				t->hbitmap,
				0,
				(UINT)bitmap.height,
				bitmap.pixels,
				&bitmap_info,
				DIB_RGB_COLORS);

			if (lines_set != bitmap.height)
				goto _error;
		}

		ReleaseDC(NULL, screen_dc);
		texture = t;
		return err_ok;

	_error:
		if (NULL != screen_dc)
			ReleaseDC(NULL, screen_dc);
		
		if (NULL != t->hbitmap)
			DeleteObject(t->hbitmap);

		g::texture_manager->destroy(t);

		return err_unknown;
	}

	//--------------------------------------------------
	error_t		texture::create_dib(texture_id& texture, bitmap_t const& bitmap)
	{
		error_t err;
		texture_id t;

		BITMAPINFO bitmap_info;
		err = bitmap_info_get(bitmap_info, bitmap);
		if (err)
			return err;

		err = g::texture_manager->create(t);
		if (err)
			return err;

		t->hbitmap = NULL;
		t->bitmap = bitmap;
		t->bitmap.pixels = nullptr;

		HDC screen_dc = GetDC(NULL);
		if (NULL == screen_dc)
			goto _error;

		void* pbits;
		t->hbitmap = CreateDIBSection(screen_dc, &bitmap_info, DIB_RGB_COLORS, &pbits, NULL, NULL);
		if (NULL == t->hbitmap)
			goto _error;

		t->bitmap.pixels = (byte_t*)pbits;

		if (nullptr != bitmap.pixels)
			memcpy(pbits, bitmap.pixels, bitmap.scanline * bitmap.height);

		ReleaseDC(NULL, screen_dc);
		texture = t;
		return err_ok;

	_error:
		if (NULL != screen_dc)
			ReleaseDC(NULL, screen_dc);

		if (NULL != t->hbitmap)
			DeleteObject(t->hbitmap);

		g::texture_manager->destroy(t);

		return err_unknown;
	}

	//--------------------------------------------------
	void		texture::destroy(texture_id texture)
	{
		DeleteObject(texture->hbitmap);
		g::texture_manager->destroy(texture);
	}

	//--------------------------------------------------
	void		texture::get_bitmap(texture_id texture, bitmap_t& bitmap)
	{
		bitmap = texture->bitmap;
		bitmap.pixels = nullptr;
	}

	//--------------------------------------------------
	error_t		texture::lock(texture_id texture, bitmap_t& bitmap)
	{
		if (nullptr == texture->bitmap.pixels)
			err_invalid_parameter;

		GdiFlush();

		bitmap = texture->bitmap;

		return err_ok;
	}

	//--------------------------------------------------
	void		texture::unlock(texture_id texture)
	{

	}

	//////////////////////////////////////////////////
	// TEXTURE MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	texture_manager_t::texture_manager_t()
		: _texture_used_count(0)
	{
		for (size_t i = 0; i < max_texture_count; ++i)
		{
			_texture_id_array[i] = &_texture_array[i];
		}
	}

	//--------------------------------------------------
	texture_manager_t::~texture_manager_t()
	{

	}

	//--------------------------------------------------
	error_t	texture_manager_t::create(texture_id& texture)
	{
		if (max_texture_count == _texture_used_count)
			return err_out_of_handles;

		texture = _texture_id_array[_texture_used_count];
		++_texture_used_count;

		return err_ok;
	}

	//--------------------------------------------------
	void	texture_manager_t::destroy(texture_id texture)
	{
		_texture_id_array[_texture_used_count] = texture;
		--_texture_used_count;
	}

	//////////////////////////////////////////////////
	// SURFACE MANAGER
	//////////////////////////////////////////////////

	//--------------------------------------------------
	surface_manager_t::surface_manager_t()
		: _surface_used_count(0)
	{
		for (size_t i = 0; i < max_surface_count; ++i)
		{
			_surface_id_array[i] = &_surface_array[i];
		}
	}

	//--------------------------------------------------
	surface_manager_t::~surface_manager_t()
	{

	}

	//--------------------------------------------------
	error_t	surface_manager_t::create(surface_id& surface)
	{
		if (max_surface_count == _surface_used_count)
			return err_out_of_handles;

		surface_id s = _surface_id_array[_surface_used_count];

		++_surface_used_count;
		surface = s;

		return err_ok;
	}

	//--------------------------------------------------
	void	surface_manager_t::destroy(surface_id surface)
	{
		_surface_id_array[_surface_used_count] = surface;
		--_surface_used_count;
	}

	//////////////////////////////////////////////////
	// SURFACE
	//////////////////////////////////////////////////

	//--------------------------------------------------
	error_t	surface::init()
	{
		g::surface_manager = mem<surface_manager_t>::alloc(app::allocator);
		if (nullptr == g::surface_manager)
			return err_out_of_memory;

		return err_ok;
	}

	//--------------------------------------------------
	void	surface::clean()
	{
		if (nullptr == g::surface_manager)
			return;

		mem<surface_manager_t>::free(app::allocator, g::surface_manager);
		g::surface_manager = nullptr;
	}

	//--------------------------------------------------
	error_t	surface::draw_begin(surface_id& surface, texture_id texture)
	{
		surface_id s;
		error_t err;
		
		err = g::surface_manager->create(s);
		if (err)
			return err;

		s->hdc_dev = CreateCompatibleDC(NULL);
		if (NULL == s->hdc_dev)
		{
			g::surface_manager->destroy(s);
			return err_unknown;
		}

		s->hdc_mem = CreateCompatibleDC(s->hdc_dev);
		if (NULL == s->hdc_mem)
		{
			DeleteDC(s->hdc_dev);
			g::surface_manager->destroy(s);
			return err_unknown;
		}

		s->hwnd = NULL;
		s->hbitmap1x1 = SelectObject(s->hdc_dev, texture->hbitmap);
		s->rect.left = 0;
		s->rect.top = 0;
		s->rect.right = (LONG)texture->bitmap.width;
		s->rect.bottom = (LONG)texture->bitmap.height;

		surface = s;

		return err_ok;
	}

	//--------------------------------------------------
	error_t	surface::draw_begin(surface_id& surface, window_id window)
	{
		surface_id s;
		error_t err;
		point_t client_size;

		err = window::get_client_size(window, client_size);
		if (err)
			return err;

		err = g::surface_manager->create(s);
		if (err)
			return err;

		s->hdc_dev = GetDC(get_native_handle(window));
		if (NULL == s->hdc_dev)
		{
			g::surface_manager->destroy(s);
			return err_unknown;
		}

		s->hdc_mem = CreateCompatibleDC(s->hdc_dev);
		if (NULL == s->hdc_mem)
		{
			ReleaseDC(get_native_handle(window), s->hdc_dev);
			g::surface_manager->destroy(s);
			return err_unknown;
		}

		s->hwnd = get_native_handle(window);
		s->hbitmap1x1 = NULL;
		s->rect.left = 0;
		s->rect.top = 0;
		s->rect.right = (LONG)client_size.x;
		s->rect.bottom = (LONG)client_size.y;

		surface = s;

		return err_ok;
	}

	//--------------------------------------------------
	void	surface::draw_end(surface_id surface)
	{
		if (NULL != surface->hbitmap1x1)
		{
			SelectObject(surface->hdc_dev, surface->hbitmap1x1);

			DeleteDC(surface->hdc_mem);
			DeleteDC(surface->hdc_dev);

			g::surface_manager->destroy(surface);
		}

		if (NULL != surface->hwnd)
		{
			DeleteDC(surface->hdc_mem);
			ReleaseDC(surface->hwnd, surface->hdc_dev);

			g::surface_manager->destroy(surface);
		}
	}

	//--------------------------------------------------
	void surface::clear(surface_id surface, color_t color)
	{
		HGDIOBJ dc_brush = GetStockObject(DC_BRUSH);
		HGDIOBJ old_brush = SelectObject(surface->hdc_dev, dc_brush);
		SetDCBrushColor(surface->hdc_dev, to_gdi_color(color));

		FillRect(surface->hdc_dev, &surface->rect, (HBRUSH)dc_brush);
		
		SelectObject(surface->hdc_dev, old_brush);
	}

	//--------------------------------------------------
	void surface::draw_pixel(surface_id surface, const point_t& position, color_t color)
	{
		SetPixel(surface->hdc_dev, position.x, position.y, to_gdi_color(color));
	}

	//--------------------------------------------------
	void surface::draw_line(surface_id surface, const point_t& p1, const point_t& p2, color_t color)
	{
		BOOL res;	
		//COLORREF old_color = SetDCPenColor(surface->hdc_dev, to_gdi_color(color));
		//HPEN pen = CreatePen(PS_SOLID, 0, to_gdi_color(color));

		HGDIOBJ old_pen = SelectObject(surface->hdc_dev, GetStockObject(DC_PEN));
		SetDCPenColor(surface->hdc_dev, to_gdi_color(color));

		res = MoveToEx(surface->hdc_dev, p1.x, p1.y, NULL);
		res = LineTo(surface->hdc_dev, p2.x, p2.y);

		SelectObject(surface->hdc_dev, old_pen);
		//DeleteObject(pen);
	}

	//--------------------------------------------------
	void surface::draw_ellipse(surface_id surface, const rect_t& dst_rect, color_t color)
	{
		BOOL res;
		//COLORREF old_color = SetDCPenColor(surface->hdc_dev, to_gdi_color(color));
		//HPEN pen = CreatePen(PS_SOLID, 0, to_gdi_color(color));

		HGDIOBJ old_pen = SelectObject(surface->hdc_dev, GetStockObject(DC_PEN));
		SetDCPenColor(surface->hdc_dev, to_gdi_color(color));

		HGDIOBJ old_brush = SelectObject(surface->hdc_dev, GetStockObject(NULL_BRUSH));

		res = Ellipse(surface->hdc_dev, 
			dst_rect.base.x,
			dst_rect.base.y,
			dst_rect.base.x + dst_rect.size.x,
			dst_rect.base.y + dst_rect.size.y);

		SelectObject(surface->hdc_dev, old_brush);

		SelectObject(surface->hdc_dev, old_pen);
		//DeleteObject(pen);
	}

	//--------------------------------------------------
	void surface::draw_rect(surface_id surface, const rect_t& dst_rect, color_t color)
	{
		RECT rect;
		rect.left = dst_rect.base.x;
		rect.top = dst_rect.base.y;
		rect.right = dst_rect.base.x + dst_rect.size.x;
		rect.bottom = dst_rect.base.y + dst_rect.size.y;

		//HBRUSH brush = CreateSolidBrush(to_gdi_color(color));
		//FillRect(surface->hdc_dev, &rect, brush);
		//DeleteObject(brush);

		HGDIOBJ old_brush = SelectObject(surface->hdc_dev, GetStockObject(NULL_BRUSH));
		HGDIOBJ old_pen = SelectObject(surface->hdc_dev, GetStockObject(DC_PEN));
		SetDCPenColor(surface->hdc_dev, to_gdi_color(color));

		Rectangle(surface->hdc_dev,
			dst_rect.base.x,
			dst_rect.base.y,
			dst_rect.base.x + dst_rect.size.x,
			dst_rect.base.y + dst_rect.size.y
		);

		SelectObject(surface->hdc_dev, old_pen);
		SelectObject(surface->hdc_dev, old_brush);
	}

	//--------------------------------------------------
	void surface::fill_rect(surface_id surface, const rect_t& dst_rect, color_t color)
	{
		RECT rect;
		rect.left = dst_rect.base.x;
		rect.top = dst_rect.base.y;
		rect.right = dst_rect.base.x + dst_rect.size.x;
		rect.bottom = dst_rect.base.y + dst_rect.size.y;
	
		//HBRUSH brush = CreateSolidBrush(to_gdi_color(color));
		//FillRect(surface->hdc_dev, &rect, brush);
		//DeleteObject(brush);

		HGDIOBJ dc_brush = GetStockObject(DC_BRUSH);
		HGDIOBJ old_brush = SelectObject(surface->hdc_dev, dc_brush);
		SetDCBrushColor(surface->hdc_dev, to_gdi_color(color));

		FillRect(surface->hdc_dev, &rect, (HBRUSH)dc_brush);
		/*Rectangle(surface->hdc_dev, 
			dst_rect.base.x, 
			dst_rect.base.y,
			dst_rect.base.x + dst_rect.size.x, 
			dst_rect.base.y + dst_rect.size.y
		);*/

		SelectObject(surface->hdc_dev, old_brush);
	}

	//--------------------------------------------------
	void surface::draw_bitmap(surface_id surface, const rect_t& dst_rect, const point_t& src_base, const bitmap_t& bitmap)
	{
		BITMAPINFO bitmap_info;
		if (bitmap_info_get(bitmap_info, bitmap))
			return;

		//DWORD paint_w = m::max(m::min(ps.rcPaint.right - ps.rcPaint.left, (long)texture->width() - ps.rcPaint.left), (long)0);
		//DWORD paint_h = m::max(m::min(ps.rcPaint.bottom - ps.rcPaint.top, (long)texture->height() - ps.rcPaint.top), (long)0);
		//if (paint_w != 0 && paint_h != 0)
		//{
		auto result = SetDIBitsToDevice(
			surface->hdc_dev,
			dst_rect.base.x, //ps.rcPaint.left,
			dst_rect.base.y, //ps.rcPaint.top,
			dst_rect.size.x, //paint_w,
			dst_rect.size.y, //paint_h,
			src_base.x, //ps.rcPaint.left,
			(UINT)(bitmap.height - (src_base.y + dst_rect.size.y)), //texture->height() - (ps.rcPaint.bottom), //y is reversed and specifies y offset from bot
			0, //starts with first scan line
			(int)bitmap.height, //texture->height(), //scan line count in data bytes
			bitmap.pixels, //texture->texels(), //data bytes
			&bitmap_info,
			DIB_RGB_COLORS);
			//O_ASSERT(result != 0);
		//}
	}

	//--------------------------------------------------
	void surface::draw_bitmap(surface_id surface, const rect_t& dst_rect, const rect_t& src_rect, const bitmap_t& bitmap)
	{
		BITMAPINFO bitmap_info;
		if (bitmap_info_get(bitmap_info, bitmap))
			return;

		
		SetStretchBltMode(surface->hdc_dev, HALFTONE);
		SetBrushOrgEx(surface->hdc_dev, 0, 0, NULL);


		auto result = StretchDIBits(
			surface->hdc_dev,
			dst_rect.base.x, //ps.rcPaint.left,
			dst_rect.base.y, //ps.rcPaint.top,
			dst_rect.size.x, //paint_w,
			dst_rect.size.y, //paint_h,
			src_rect.base.x, //ps.rcPaint.left,
			src_rect.base.y,
			src_rect.size.x,
			src_rect.size.y,
			bitmap.pixels, //texture->texels(), //data bytes
			&bitmap_info,
			DIB_RGB_COLORS,
			SRCCOPY);
	}

	//--------------------------------------------------
	void surface::draw_texture(surface_id surface, const rect_t& dst_rect, const point_t& src_base, texture_id texture)
	{
		HGDIOBJ old_bitmap = SelectObject(surface->hdc_mem, texture->hbitmap);
		BOOL res = BitBlt(
			surface->hdc_dev,
			dst_rect.base.x,
			dst_rect.base.y,
			dst_rect.size.x,
			dst_rect.size.y,
			surface->hdc_mem,
			src_base.x,
			src_base.y,
			SRCCOPY);

		SelectObject(surface->hdc_mem, old_bitmap);
	}

	//--------------------------------------------------
	void surface::draw_texture(surface_id surface, const rect_t& dst_rect, const rect_t& src_rect, texture_id texture)
	{
		SetStretchBltMode(surface->hdc_dev, HALFTONE);
		SetBrushOrgEx(surface->hdc_dev, 0, 0, NULL);

		SetStretchBltMode(surface->hdc_mem, HALFTONE);
		SetBrushOrgEx(surface->hdc_mem, 0, 0, NULL);

		HGDIOBJ old_bitmap = SelectObject(surface->hdc_mem, texture->hbitmap);
		BOOL res = StretchBlt(
			surface->hdc_dev,
			dst_rect.base.x,
			dst_rect.base.y,
			dst_rect.size.x,
			dst_rect.size.y,
			surface->hdc_mem,
			src_rect.base.x,
			src_rect.base.y,
			src_rect.size.x,
			src_rect.size.y,
			SRCCOPY);

		SelectObject(surface->hdc_mem, old_bitmap);
	}

} }