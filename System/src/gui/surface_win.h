#pragma once
#include "system\gui\surface.h"

namespace outland { namespace  os
{
	

	//////////////////////////////////////////////////
	// SURFACE
	//////////////////////////////////////////////////

	namespace surface
	{
		error_t	init();
		void	clean();
	}

	struct surface_t
	{
		HWND	hwnd;
		HDC		hdc_dev;
		HDC		hdc_mem;
		RECT	rect;

		HGDIOBJ	hbitmap1x1;
	};

	//////////////////////////////////////////////////
	// SURFACE MANAGER
	//////////////////////////////////////////////////

	class surface_manager_t
	{
	private:
		enum : size_t
		{
			max_surface_count = 4,
		};

		surface_t	_surface_array[max_surface_count];
		surface_id	_surface_id_array[max_surface_count];
		size_t		_surface_used_count;

	public:
		surface_manager_t();
		~surface_manager_t();

		error_t	create(surface_id& surface);
		void	destroy(surface_id surface);
	};

	//////////////////////////////////////////////////
	// TEXTURE
	//////////////////////////////////////////////////

	namespace texture
	{
		error_t	init();
		void	clean();
	}

	struct texture_t
	{
		HBITMAP		hbitmap;
		bitmap_t	bitmap;
	};

	//////////////////////////////////////////////////
	// TEXTURE MANAGER
	//////////////////////////////////////////////////

	class texture_manager_t
	{
	private:
		enum : size_t
		{
			max_texture_count = 32,
		};

		texture_t	_texture_array[max_texture_count];
		texture_id	_texture_id_array[max_texture_count];
		size_t		_texture_used_count;

	public:
		texture_manager_t();
		~texture_manager_t();

		error_t	create(texture_id& texture);
		void	destroy(texture_id texture);
	};

} }