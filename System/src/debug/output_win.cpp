#include "pch.h"
#include "system\debug\output.h"

namespace outland { namespace  os
{

	void debug_string(const char8_t* str)
	{
		OutputDebugStringA(str);
	}

} }