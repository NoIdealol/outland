#include "pch.h"
#include "system\debug\stack_trace.h"

#if O_ENABLE_STACK_TRACE

#include <Dbghelp.h>
#pragma comment(lib, "Dbghelp.lib")

namespace outland { namespace  os
{	
	//--------------------------------------------------
	error_t stack_trace::init()
	{
		DWORD sym_options = SymSetOptions(SYMOPT_LOAD_LINES);
		BOOL init = SymInitialize(GetCurrentProcess(), NULL, TRUE);
		return FALSE == init;
	}

	//--------------------------------------------------
	size_t	stack_trace::capture(stack_trace_id* trace, size_t count, size_t skip, uint32_t& hash)
	{
		ULONG stack_hash = 0;
		WORD stack_res = CaptureStackBackTrace(skip, count, trace, &stack_hash);
		hash = stack_hash;
		return stack_res;
	}

	//--------------------------------------------------
	error_t stack_trace::info(stack_trace_id trace, string8_t& name, size_t& line)
	{
		IMAGEHLP_LINE64 trace_info = { 0 };
		trace_info.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

		DWORD line_displacement = 0;
		BOOL ret = SymGetLineFromAddr64(GetCurrentProcess(), (DWORD64)trace, &line_displacement, &trace_info);
		if (FALSE == ret)
			return err_unknown;

		error_t err = name = trace_info.FileName;
		if (err)
			return err;

		line = trace_info.LineNumber;

		return err_ok;
	}

	//--------------------------------------------------
	error_t stack_trace::clean()
	{
		BOOL cleanup = SymCleanup(GetCurrentProcess());
		return FALSE == cleanup;
	}

} }

#else

namespace outland { namespace  os
{	
	//--------------------------------------------------
	error_t stack_trace::init()
	{
		return err_unknown;
	}

	//--------------------------------------------------
	size_t	stack_trace::capture(stack_trace_id* trace, size_t count, size_t skip, uint32_t& hash)
	{
		return err_unknown;
	}

	//--------------------------------------------------
	error_t stack_trace::info(stack_trace_id trace, string8_t& name, size_t& line)
	{
		return err_unknown;
	}

	//--------------------------------------------------
	error_t stack_trace::clean()
	{
		return err_unknown;
	}

} }

#endif